﻿using DNBTools;
using DNBTools.DataClasses;
using DNBTools.Nautilus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NautTools
{
    public partial class MainForm : Form
    {
        private LimsCommunication _limsCommunication;
        private string _operatorId;
        private List<BiobankEntity> _entities;

        public MainForm(LimsCommunication limsCommunication, List<BiobankEntity> data, string operatorId, ref StartupForm startForm)
        {
            InitializeComponent();
            try
            {
                _entities = data;
                _operatorId = operatorId;
                _limsCommunication = limsCommunication;
                SysLog.Log(string.Format("Mainform started with {0} aliquots. ", data.Count), LogLevel.Info);
                // Test
                SysLog.Log(string.Format("LimsID {0} LogicalID {1} ", data[0].LimsId, data[0].LogicalId), LogLevel.Info);
                lblAliquotsChoosen.Text = string.Format("(Antal aliquots valgt: {0})", data.Count);
                lblAliquotsChoosen.Refresh();
                _stateLabel.Text = "OK";
                startForm.Close();
                this.Cursor = Cursors.Default;
                //FillList();
            }
            catch (Exception ex)
            {
                SysLog.Log("Fejl ved opstart: " + ex.Message, LogLevel.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            UpdateUI(false);
            try
            {
                this.Cursor = Cursors.WaitCursor;
                string comment = txtComments.Text.Trim();
                if (comment == string.Empty)
                {
                    DialogResult res= MessageBox.Show("Skal kommentaren slettes?","AddComments", MessageBoxButtons.YesNo);
                    if (res == DialogResult.No)
                        return;
                }
                if (_limsCommunication.UpdateManyTubesWithComments(_entities, comment, rbComment.Checked, _operatorId, _stateLabel, pbStatus))
                {
                    _stateLabel.Text = string.Format("Opdateret {0} Aliquots", _entities.Count);
                }
                else
                {
                    _stateLabel.Text = "Der skete en fejl under opdateringen. Kontakt IT afdelingen.";
                }

                //int x = 1;
                //foreach (var i in _entities)
                //{
                //    string newComment= _limsCommunication.UpdateTubeWithComment(i.LogicalId, txtComments.Text.Trim(), rbComment.Checked);
                //    //UpdateList(new ListInfo() { ExternalRef = i.LogicalId, Comment = newComment });
                //    _stateLabel.Text = string.Format("Opdaterer {0} ud af {1}", x, _entities.Count);
                //    _stateLabel.Refresh();
                //    x++;
                //}
               // FillList();
            }
            catch (Exception ex)
            {
                SysLog.Log("Fejl ved opdatering af kommentar: " + ex.Message, LogLevel.Error);
            }
            this.Cursor = Cursors.Default;
            UpdateUI(true);
        }

        private void UpdateList(ListInfo updateEntry)
        {
            lstTubes.BeginUpdate();
            ListBox.ObjectCollection items = lstTubes.Items;
            int count = items.Count;

            for (int i = 0; i < count; i++)
            {
                ListInfo li= (ListInfo)items[i];
                if (li.ExternalRef == updateEntry.ExternalRef)
                    li.Comment = updateEntry.Comment;
            }
            lstTubes.EndUpdate();
            lstTubes.Refresh();
          
        }

        private void FillList()
        {
            lstTubes.Items.Clear();
            foreach (var i in _entities)
            {
                ListInfo lf = new ListInfo();
                lf.ExternalRef = i.LogicalId;
                lf.Comment = _limsCommunication.GetTubeComment(i.LogicalId);
                lstTubes.Items.Add(lf);
            }
        }

        private void UpdateUI(bool active)
        {
            btnSave.Enabled = active;

            //if (active && txtComments.Text == "")
            //{
            //    btnSave.Enabled = false;
            //    _stateLabel.Text = "Beskrivelse er ikke udfyldt";
            //}
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            
        }

        private void cbDefinedText_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox myCmb = (ComboBox)sender;
            string selected = myCmb.GetItemText(myCmb.SelectedItem);
            if (selected != string.Empty)
                txtComments.Text += selected;
        }
    }
}
