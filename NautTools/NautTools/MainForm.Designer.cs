﻿namespace NautTools
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._stateLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtComments = new System.Windows.Forms.TextBox();
            this.lstTubes = new System.Windows.Forms.ListBox();
            this.rbComment = new System.Windows.Forms.RadioButton();
            this.rbErstatKommentar = new System.Windows.Forms.RadioButton();
            this.lblAliquotsChoosen = new System.Windows.Forms.Label();
            this.cbDefinedText = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pbStatus = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // _stateLabel
            // 
            this._stateLabel.AutoSize = true;
            this._stateLabel.Location = new System.Drawing.Point(73, 347);
            this._stateLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this._stateLabel.Name = "_stateLabel";
            this._stateLabel.Size = new System.Drawing.Size(46, 17);
            this._stateLabel.TabIndex = 20;
            this._stateLabel.Text = "label5";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 347);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 17);
            this.label4.TabIndex = 19;
            this.label4.Text = "Status:";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(626, 409);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 18;
            this.btnCancel.Text = "Afslut";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(471, 409);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 28);
            this.btnSave.TabIndex = 17;
            this.btnSave.Text = "Gem";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 27);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 17);
            this.label1.TabIndex = 16;
            this.label1.Text = "Indtast kommentar";
            // 
            // txtComments
            // 
            this.txtComments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComments.Location = new System.Drawing.Point(16, 133);
            this.txtComments.Margin = new System.Windows.Forms.Padding(4);
            this.txtComments.Multiline = true;
            this.txtComments.Name = "txtComments";
            this.txtComments.Size = new System.Drawing.Size(712, 99);
            this.txtComments.TabIndex = 15;
            // 
            // lstTubes
            // 
            this.lstTubes.Enabled = false;
            this.lstTubes.FormattingEnabled = true;
            this.lstTubes.ItemHeight = 16;
            this.lstTubes.Location = new System.Drawing.Point(972, 113);
            this.lstTubes.Margin = new System.Windows.Forms.Padding(4);
            this.lstTubes.Name = "lstTubes";
            this.lstTubes.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lstTubes.Size = new System.Drawing.Size(313, 260);
            this.lstTubes.TabIndex = 21;
            this.lstTubes.Visible = false;
            // 
            // rbComment
            // 
            this.rbComment.AutoSize = true;
            this.rbComment.Checked = true;
            this.rbComment.Location = new System.Drawing.Point(16, 267);
            this.rbComment.Margin = new System.Windows.Forms.Padding(4);
            this.rbComment.Name = "rbComment";
            this.rbComment.Size = new System.Drawing.Size(133, 21);
            this.rbComment.TabIndex = 22;
            this.rbComment.TabStop = true;
            this.rbComment.Text = "Tilføj kommentar";
            this.rbComment.UseVisualStyleBackColor = true;
            // 
            // rbErstatKommentar
            // 
            this.rbErstatKommentar.AutoSize = true;
            this.rbErstatKommentar.Location = new System.Drawing.Point(16, 295);
            this.rbErstatKommentar.Margin = new System.Windows.Forms.Padding(4);
            this.rbErstatKommentar.Name = "rbErstatKommentar";
            this.rbErstatKommentar.Size = new System.Drawing.Size(140, 21);
            this.rbErstatKommentar.TabIndex = 23;
            this.rbErstatKommentar.Text = "Erstat kommentar";
            this.rbErstatKommentar.UseVisualStyleBackColor = true;
            // 
            // lblAliquotsChoosen
            // 
            this.lblAliquotsChoosen.AutoSize = true;
            this.lblAliquotsChoosen.Location = new System.Drawing.Point(145, 27);
            this.lblAliquotsChoosen.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAliquotsChoosen.Name = "lblAliquotsChoosen";
            this.lblAliquotsChoosen.Size = new System.Drawing.Size(106, 17);
            this.lblAliquotsChoosen.TabIndex = 24;
            this.lblAliquotsChoosen.Text = "( Antal valgt: x )";
            // 
            // cbDefinedText
            // 
            this.cbDefinedText.FormattingEnabled = true;
            this.cbDefinedText.Items.AddRange(new object[] {
            "Manuelt disposed [oprydning]"});
            this.cbDefinedText.Location = new System.Drawing.Point(209, 86);
            this.cbDefinedText.Name = "cbDefinedText";
            this.cbDefinedText.Size = new System.Drawing.Size(510, 24);
            this.cbDefinedText.TabIndex = 25;
            this.cbDefinedText.SelectedIndexChanged += new System.EventHandler(this.cbDefinedText_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 17);
            this.label2.TabIndex = 26;
            this.label2.Text = "Pre-defineret text:";
            // 
            // pbStatus
            // 
            this.pbStatus.Location = new System.Drawing.Point(16, 415);
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Size = new System.Drawing.Size(370, 23);
            this.pbStatus.TabIndex = 27;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 453);
            this.Controls.Add(this.pbStatus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbDefinedText);
            this.Controls.Add(this.lblAliquotsChoosen);
            this.Controls.Add(this.rbErstatKommentar);
            this.Controls.Add(this.rbComment);
            this.Controls.Add(this.lstTubes);
            this.Controls.Add(this._stateLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtComments);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(760, 500);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tilføj kommentar";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _stateLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtComments;
        private System.Windows.Forms.ListBox lstTubes;
        private System.Windows.Forms.RadioButton rbComment;
        private System.Windows.Forms.RadioButton rbErstatKommentar;
        private System.Windows.Forms.Label lblAliquotsChoosen;
        private System.Windows.Forms.ComboBox cbDefinedText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ProgressBar pbStatus;
    }
}