﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using DNBTools;
using DNBTools.Kiwi;
using DNBTools.Nautilus;
using DNBTools.DataClasses;

namespace NautTools
{
    public partial class NautilusUserControl : KiwiUserControl
    {
        private List<BiobankEntity> _entities;
        private List<string> _kiwi05List;
        private List<string> _kiwi075List;
        private List<string> _kiwi10List;
        private List<string> _kiwi14List;
        private List<string> _bothList;

        public NautilusUserControl()
        {
            InitializeComponent();
        }

        public void Initialize(List<BiobankEntity> entities, Configuration configuration, LimsCommunication limsCommunication, KiwiCommunication kiwiCommunication)
        {
            base.Initialize(configuration, limsCommunication, kiwiCommunication);
            _entities = entities;
            _bothList = new List<string>();
            SysLog.Log("Nautilus Usercontrol started", LogLevel.Info);
        }

        internal void EnableUI(bool enable)
        {
            _allRadioButton.Enabled = enable;
            _differentRadioButton.Enabled = enable;
            _updateButton.Enabled = enable;
            Refresh();
        }

        private void NautilusUserControl_VisibleChanged(object sender, EventArgs e)
        {
            _allRadioButton.Enabled = false;
            _differentRadioButton.Enabled = false;
        }

        private void _updateButton_Click(object sender, EventArgs e)
        {
            EnableUI(false);
            _kiwi14List = new List<string>();
            _kiwi10List = new List<string>();
            _kiwi075List = new List<string>();
            _kiwi05List = new List<string>();
            _bothList = new List<string>();
            _kiwiListBox.Items.Clear();
            _nautilusListBox.Items.Clear();
            _bothListBox.Items.Clear();
            _allRadioButton.Checked = true;
            _kiwiCountLabel.Text = "Antal: ";
            _nautilusCountLabel.Text = "Antal: ";
            _bothCountLabel.Text = "Antal: ";
            _stateLabel.Text = "Status: Henter information fra Kiwi (1,4mL)";
            Refresh();
            KiwiCommunication kiwiCommunication = new KiwiCommunication(_configuration.Config14.StoreName, _configuration.Config14.StoreAddress, _configuration.Config14.StorePort);
            Tuple<string, string> response = kiwiCommunication.GetPartitionPlatesList(NautToolsEntityExtension.OrderId++, _configuration.OperatorKiwiName, _configuration.Config14.Partitions[0]);
            Tuple<string, string> status = kiwiCommunication.StatusFromResponse(response.Item2);
            if (status.Item1 == "ERR")
            {
                MessageBox.Show("Kunne ikke hente information fra Kiwi: " + status.Item2);
                EnableUI(true);
                return;
            }
            _stateLabel.Text = "Status: Behandler information fra Kiwi (1,4mL)";
            Refresh();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(response.Item2);
            XmlNodeList plateNodes = doc.SelectNodes("/STXRequest/Plates");
            foreach (XmlNode plateNode in plateNodes)
            {
                string barcode = plateNode.SelectSingleNode("PltBCR").InnerXml;
                if (!String.IsNullOrWhiteSpace(barcode))
                {
                    _kiwi14List.Add(barcode);
                }
            }
            _stateLabel.Text = "Status: Henter information fra Kiwi (1,0mL)";
            Refresh();
            kiwiCommunication = new KiwiCommunication(_configuration.Config10.StoreName, _configuration.Config10.StoreAddress, _configuration.Config10.StorePort);
            response = kiwiCommunication.GetPartitionPlatesList(NautToolsEntityExtension.OrderId++, _configuration.OperatorKiwiName, _configuration.Config10.Partitions[0]);
            status = kiwiCommunication.StatusFromResponse(response.Item2);
            if (status.Item1 == "ERR")
            {
                MessageBox.Show("Kunne ikke hente information fra Kiwi: " + status.Item2);
                EnableUI(true);
                return;
            }
            _stateLabel.Text = "Status: Behandler information fra Kiwi (1,0mL)";
            Refresh();
            doc = new XmlDocument();
            doc.LoadXml(response.Item2);
            plateNodes = doc.SelectNodes("/STXRequest/Plates");
            foreach (XmlNode plateNode in plateNodes)
            {
                string barcode = plateNode.SelectSingleNode("PltBCR").InnerXml;
                if (!String.IsNullOrWhiteSpace(barcode))
                {
                    _kiwi10List.Add(barcode);
                }
            }
            _stateLabel.Text = "Status: Henter information fra Kiwi (0,75mL)";
            Refresh();
            kiwiCommunication = new KiwiCommunication(_configuration.Config075.StoreName, _configuration.Config075.StoreAddress, _configuration.Config075.StorePort);
            response = kiwiCommunication.GetPartitionPlatesList(NautToolsEntityExtension.OrderId++, _configuration.OperatorKiwiName, _configuration.Config075.Partitions[0]);
            status = kiwiCommunication.StatusFromResponse(response.Item2);
            if (status.Item1 == "ERR")
            {
                MessageBox.Show("Kunne ikke hente information fra Kiwi: " + status.Item2);
                EnableUI(true);
                return;
            }
            _stateLabel.Text = "Status: Behandler information fra Kiwi (0,75mL)";
            Refresh();
            doc = new XmlDocument();
            doc.LoadXml(response.Item2);
            plateNodes = doc.SelectNodes("/STXRequest/Plates");
            foreach (XmlNode plateNode in plateNodes)
            {
                string barcode = plateNode.SelectSingleNode("PltBCR").InnerXml;
                if (!String.IsNullOrWhiteSpace(barcode))
                {
                    _kiwi075List.Add(barcode);
                }
            }
            _stateLabel.Text = "Status: Henter information fra Kiwi (0,5mL)";
            Refresh();
            kiwiCommunication = new KiwiCommunication(_configuration.Config05.StoreName, _configuration.Config05.StoreAddress, _configuration.Config05.StorePort);
            response = kiwiCommunication.GetPartitionPlatesList(NautToolsEntityExtension.OrderId++, _configuration.OperatorKiwiName, _configuration.Config05.Partitions[0]);
            status = kiwiCommunication.StatusFromResponse(response.Item2);
            if (status.Item1 == "ERR")
            {
                MessageBox.Show("Kunne ikke hente information fra Kiwi: " + status.Item2);
                EnableUI(true);
                return;
            }
            _stateLabel.Text = "Status: Behandler information fra Kiwi (0,5mL)";
            Refresh();
            doc = new XmlDocument();
            doc.LoadXml(response.Item2);
            plateNodes = doc.SelectNodes("/STXRequest/Plates");
            foreach (XmlNode plateNode in plateNodes)
            {
                string barcode = plateNode.SelectSingleNode("PltBCR").InnerXml;
                if (!String.IsNullOrWhiteSpace(barcode))
                {
                    _kiwi05List.Add(barcode);
                }
            }
            List<string> kiwiPlates = new List<string>();
            kiwiPlates.AddRange(_kiwi14List.ToArray());
            kiwiPlates.AddRange(_kiwi10List.ToArray());
            kiwiPlates.AddRange(_kiwi075List.ToArray());
            kiwiPlates.AddRange(_kiwi05List.ToArray());
            kiwiPlates = kiwiPlates.Distinct().ToList();
            _stateLabel.Text = "Status: Henter information fra Nautilus";
            List<string> nautilusPlates = _limsCommunication.LocationGetPlates(_limsCommunication.LocationGetId(LimsCommunication.KiwiLocationName));
            _stateLabel.Text = "Status: Opdaterer UI";
            Refresh();
            List<string> kiwiList = kiwiPlates.Except(nautilusPlates).ToList();
            kiwiList.Sort();
            _kiwiListBox.Items.AddRange(kiwiList.ToArray());
            List<string> nautilusList = nautilusPlates.Except(kiwiPlates).ToList();
            nautilusList.Sort();
            _nautilusListBox.Items.AddRange(nautilusList.ToArray());
            _bothList = kiwiPlates.Intersect(nautilusPlates).ToList();
            _bothList.Sort();
            _bothListBox.Items.AddRange(_bothList.ToArray());
            _stateLabel.Text = "Status: Klar ";
            _kiwiCountLabel.Text = "Antal: " + _kiwiListBox.Items.Count;
            _nautilusCountLabel.Text = "Antal: " + _nautilusListBox.Items.Count;
            _bothCountLabel.Text = "Antal: " + _bothListBox.Items.Count;
            EnableUI(true);
        }

        private void _differentRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            EnableUI(false);
            _bothListBox.Items.Clear();
            try
            {
                if (_differentRadioButton.Checked)
                {
                    List<string> barcodes = new List<string>();
                    for (int i = 0; i < _bothList.Count; i++ )
                    {
                        _stateLabel.Text = "Status: Sammenligner plader (" + i + "/" + _bothList.Count + ")";
                        _stateLabel.Refresh();
                        if (CompareInSystems(_bothList[i]) != null)
                        {
                            barcodes.Add(_bothList[i]);
                        }
                    }
                    _bothListBox.Items.AddRange(barcodes.ToArray());
                }
                else
                {
                    _bothListBox.Items.AddRange(_bothList.ToArray());
                }
                _bothCountLabel.Text = "Antal: " + _bothListBox.Items.Count;
            }
            catch (Exception ex)
            {
                SysLog.Log("RB: " + ex.Message, LogLevel.Error);
                MessageBox.Show(ex.Message);
                _bothCountLabel.Text = "Error!";
            }
            _stateLabel.Text = "Status: Klar";
            EnableUI(true);
            Refresh();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="plateBC"></param>
        /// <returns>Null if plate is identical in the two systems. Otherwise a list of <kiwi pos, kiwi barcode, nautilus pos, nautilus barcode> tuples.</returns>
        private List<Tuple<string, string, string, string>> CompareInSystems(string plateBC)
        {
            string storeName, storeAddess;
            int storePort;
            if (_kiwi14List.Contains(plateBC))
            {
                storeName = _configuration.Config14.StoreName;
                storeAddess = _configuration.Config14.StoreAddress;
                storePort = _configuration.Config14.StorePort;
            }
            else if (_kiwi10List.Contains(plateBC))
            {
                storeName = _configuration.Config10.StoreName;
                storeAddess = _configuration.Config10.StoreAddress;
                storePort = _configuration.Config10.StorePort;
            }
            else if (_kiwi075List.Contains(plateBC))
            {
                storeName = _configuration.Config075.StoreName;
                storeAddess = _configuration.Config075.StoreAddress;
                storePort = _configuration.Config075.StorePort;
            }
            else
            {
                storeName = _configuration.Config05.StoreName;
                storeAddess = _configuration.Config05.StoreAddress;
                storePort = _configuration.Config05.StorePort;
            }
            KiwiCommunication kiwiCommunication = new KiwiCommunication(storeName, storeAddess, storePort);
            Tuple<string, string> response = kiwiCommunication.GetPlateInfo(NautToolsEntityExtension.OrderId++, _configuration.OperatorKiwiName, plateBC);
            Tuple<string, string> status = kiwiCommunication.StatusFromResponse(response.Item2);
            if (status.Item1 == "ERR")
            {
                EnableUI(true);
                SysLog.Log("Kunne ikke hente information fra Kiwi: " + status.Item2, LogLevel.Error);
                throw new Exception("Kunne ikke hente information fra Kiwi: " + status.Item2);
            }
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(response.Item2);
            XmlNodeList nodes = doc.SelectNodes("/STXRequest/Plates/PltTPos[PTb/TbBCR]");
            Dictionary<string, string> kiwiDictionary = new Dictionary<string, string>();
            foreach(XmlNode node in nodes)
            {
                string pos = node.SelectSingleNode("PYA").InnerText + node.SelectSingleNode("PX").InnerText;
                string barcode = node.SelectSingleNode("PTb/TbBCR").InnerText;
                kiwiDictionary.Add(pos, barcode);
            }
            string plateId = _limsCommunication.PlateGetId(plateBC);
            List<Tuple<int, string, string, string, string>> nautilusDatas = _limsCommunication.LookupSampleDataForCarrier(plateId);
            Dictionary<string, string> nautilusDictionary = new Dictionary<string, string>();
            foreach(Tuple<int, string, string, string, string> nautilusData in nautilusDatas)
            {
                nautilusDictionary.Add(nautilusData.Item4 + nautilusData.Item5.TrimStart('0'), nautilusData.Item2);
            }
            if (kiwiDictionary.Keys.Except(nautilusDictionary.Keys).Any() || nautilusDictionary.Keys.Except(kiwiDictionary.Keys).Any())
            {
                return MakeDifferenceList(kiwiDictionary, nautilusDictionary);
            }
            foreach (string key in kiwiDictionary.Keys)
            {
                if(kiwiDictionary[key] != nautilusDictionary[key])
                {
                    return MakeDifferenceList(kiwiDictionary, nautilusDictionary);
                }
            }
            return null;
        }

        private List<Tuple<string, string, string, string>> MakeDifferenceList(Dictionary<string, string> kiwiDictionary, Dictionary<string, string> nautilusDictionary)
        {
            List<string> positions = kiwiDictionary.Keys.Union(nautilusDictionary.Keys).ToList();
            positions.Sort(
                delegate(String one, String two)
                {
                    if (one == two) { return 0; }
                    string letterOne = one.Substring(0, 1);
                    string letterTwo = two.Substring(0, 1);
                    if (letterOne != letterTwo) { return letterOne.CompareTo(letterTwo); }
                    int numberOne = Convert.ToInt32(one.Substring(1));
                    int numberTwo = Convert.ToInt32(two.Substring(1));
                    return numberOne.CompareTo(numberTwo);
                }
            );
            List<Tuple<string, string, string, string>> retval = new List<Tuple<string, string, string, string>>();
            foreach(string position in positions)
            {
                string kpos, kbc, npos, nbc;
                kpos = kbc = npos = nbc = "";
                if(kiwiDictionary.ContainsKey(position))
                {
                    kpos = position;
                    kbc = kiwiDictionary[position];
                }
                if(nautilusDictionary.ContainsKey(position))
                {
                    npos = position;
                    nbc = nautilusDictionary[position];
                }
                retval.Add(new Tuple<string, string, string, string>(kpos, kbc, npos, nbc));
            }            
            return retval;
        }

        private void _bothListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(_differentRadioButton.Checked)
            {
                List<Tuple<string, string, string, string>> data = CompareInSystems(_bothListBox.SelectedItem.ToString());
                CompareForm form = new CompareForm(data);
                form.ShowDialog();
            }
        }
    }
}
