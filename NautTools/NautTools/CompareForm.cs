﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KiwiExtension
{
    public partial class CompareForm : Form
    {
        private List<Tuple<string, string, string, string>> _data;

        public CompareForm(List<Tuple<string, string, string, string>> data)
        {
            _data = data;
            InitializeComponent();
        }

        private void CompareForm_Load(object sender, EventArgs e)
        {
            foreach(Tuple<string, string, string, string> data in _data)
            {
                _dataGridView.Rows.Add(data.Item1, data.Item2, data.Item3, data.Item4);
                if (data.Item2 != data.Item4) { _dataGridView.Rows[_dataGridView.Rows.Count-1].DefaultCellStyle.BackColor = Color.Salmon; }
            }
        }
    }
}
