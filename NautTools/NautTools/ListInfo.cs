﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NautTools
{
    public class ListInfo
    {
        public ListInfo()
        { }
        private string _ExternalRef = string.Empty;
        public string ExternalRef { get { return _ExternalRef; } set { _ExternalRef = value; } }
        private string _Comment = string.Empty;
        public string Comment { get { return _Comment; } set { _Comment = value; } }

        public override string ToString()
        {
            return _ExternalRef + " - " + _Comment;
        }
    }
}
