﻿using DNBTools;
using DNBTools.Nautilus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NautTools
{
    public class DBAccess
    {
        NautEntities myDB = new NautEntities();

        private LimsCommunication _limsCommunication;
        public DBAccess(LimsCommunication limsCommunication)
        {

        }

        public bool UpdateAliquotsWithNewComments(AliInfo myInfo)
        {
            try
            {
                
                ALIQUOT myAliquot = myDB.ALIQUOT.FirstOrDefault(p => p.ALIQUOT_ID == myInfo.Aliquot_ID);
                if (myAliquot != null)
                {
                    SysLog.Log("Fandt Aliquot: " + myAliquot.EXTERNAL_REFERENCE, LogLevel.Info);
                    myAliquot.STORAGE = myInfo.Storage;
                    SysLog.Log("Opdaterede Aliquot med kommentaren: " + myAliquot.STORAGE, LogLevel.Info);
                    //ALIQUOT_USER myDisposedAliquots = myDB.ALIQUOT_USER.FirstOrDefault(d => d.ALIQUOT_ID == myInfo.Aliquot_ID);

                    //// Dispose or not
                    //if (myAliquot.CONDITION == "U" && myAliquot.LOCATION_ID == 5948 && myDisposedAliquots.U_DISPOSED == "T" && myInfo.Disposed == false)
                    //{
                    //    // Un-dispose
                    //    myDisposedAliquots.U_DISPOSED = null;

                    //    // We need to find the correct location for the aliquot by using the one from the plate
                    //    if (myInfo.Plate_ID == null)
                    //    {
                    //        myAliquot.LOCATION_ID = null;
                    //    }
                    //    else
                    //    {
                    //        PLATE myPlate = myDB.PLATE.FirstOrDefault(p => p.PLATE_ID == myInfo.Plate_ID);
                    //        if (myPlate != null)
                    //        {
                    //            myAliquot.LOCATION_ID = myPlate.LOCATION_ID;
                    //        }
                    //    }
                    //    myAliquot.CONDITION = null;
                    //}
                    //if (myInfo.Disposed && myAliquot.LOCATION_ID != 5948 && myDisposedAliquots.U_DISPOSED != "T")
                    //{
                    //    myAliquot.LOCATION_ID = 5948;
                    //    myAliquot.CONDITION = "U";
                    //    myDisposedAliquots.U_DISPOSED = "T";
                    //}
                    myDB.SaveChanges();
                    SysLog.Log("Data saved", LogLevel.Info);
                }
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("UpdateAliquotsWithNewComments: {0}", ex.Message), LogLevel.Error);
                return false;
            }
            return true;
        }
        
        public ALIQUOT GetAliquotFromExRef(string exRef)
        {
            ALIQUOT newAli = new ALIQUOT();
            try
            {
                newAli = myDB.ALIQUOT.FirstOrDefault(p => p.EXTERNAL_REFERENCE == exRef);
            }
            catch (Exception ex)
            {

                throw;
            }
            return newAli;
        }
    }

}
