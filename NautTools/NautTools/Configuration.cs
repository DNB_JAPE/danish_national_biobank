﻿using DNBTools;
using DNBTools.DataClasses;
using DNBTools.Nautilus;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace NautTools
{
    public class Configuration
    {
        internal string Mode { get; set; }
        internal string NautConStr { get; set; }
        internal ContainerTypeConfiguration Config05 { get; set; }
        internal ContainerTypeConfiguration Config075 { get; set; }
        internal ContainerTypeConfiguration Config10 { get; set; }
        internal ContainerTypeConfiguration Config14 { get; set; }
        internal ContainerTypeConfiguration DefaultConfig { get; set; }
        internal string OperatorKiwiName { get; set; }
        internal string SysLogIP { get; set; }
        internal string SysLogName { get; set; }
        internal int DebugLevel { get; set; }
        internal bool SysLogEnabled { get; set; }

        public Configuration()
        {
            try
            {
                string location = Path.Combine(Application.StartupPath, "DNB_Extensions", "NautTools");
                UriBuilder uri = new UriBuilder(location);
                string path = Uri.UnescapeDataString(uri.Path);
                XmlDocument configurationDoc = new XmlDocument();
                configurationDoc.Load(Path.Combine(path, "Configuration.xml"));
                SysLogIP = configurationDoc.SelectSingleNode("/Configuration/SysLog/IP").InnerXml;
                SysLogEnabled = Convert.ToBoolean(configurationDoc.SelectSingleNode("/Configuration/SysLog/Enabled").InnerXml);
                SysLogName = configurationDoc.SelectSingleNode("/Configuration/SysLog/Name").InnerXml;
                DebugLevel = Convert.ToInt16(configurationDoc.SelectSingleNode("/Configuration/SysLog/DebugLevel").InnerXml);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Fejl ved indlæsning af Syslog configuration: " + ex.Message);
            }
        }

        public Configuration(string containerTypeId)
        {
            string location = Path.Combine(Application.StartupPath, "DNB_Extensions", "Kiwi");
            UriBuilder uri = new UriBuilder(location);
            string path = Uri.UnescapeDataString(uri.Path);
            XmlDocument configurationDoc = new XmlDocument();
            configurationDoc.Load(Path.Combine(path, "Configuration.xml"));
            
            Mode = configurationDoc.SelectSingleNode("/Configuration/Mode").InnerXml;
            if (NautilusTools.IsProduction())
            {
                NautConStr = configurationDoc.SelectSingleNode("/Configuration/Connections/NautConStrProd").InnerXml;
            }
            else
            {
                NautConStr = configurationDoc.SelectSingleNode("/Configuration/Connections/NautConStrTest").InnerXml;
            }
            XmlNode connectionNode = configurationDoc.SelectSingleNode("/Configuration/Connections/Right");
            XmlNode dataNode = configurationDoc.SelectSingleNode("/Configuration/Data/ContainerType05");
            Config05 = new ContainerTypeConfiguration("41", connectionNode, dataNode);
            connectionNode = configurationDoc.SelectSingleNode("/Configuration/Connections/Right");
            dataNode = configurationDoc.SelectSingleNode("/Configuration/Data/ContainerType075");
            Config075 = new ContainerTypeConfiguration("21", connectionNode, dataNode);
            connectionNode = configurationDoc.SelectSingleNode("/Configuration/Connections/Left");
            dataNode = configurationDoc.SelectSingleNode("/Configuration/Data/ContainerType10");
            Config10 = new ContainerTypeConfiguration("43", connectionNode, dataNode);
            connectionNode = configurationDoc.SelectSingleNode("/Configuration/Connections/Left");
            dataNode = configurationDoc.SelectSingleNode("/Configuration/Data/ContainerType14");
            Config14 = new ContainerTypeConfiguration("42", connectionNode, dataNode);
            switch (containerTypeId)
            {
                case "41":
                    DefaultConfig = Config05;
                    break;
                case "21":
                    DefaultConfig = Config075;
                    break;
                case "43":
                    DefaultConfig = Config10;
                    break;
                case "42":
                    DefaultConfig = Config14;
                    break;
            }
        }

        public void InitializeOperator(string operatorLimsName)
        {
            try
            {
                string location = Path.Combine(Application.StartupPath, "Extensions", "Kiwi");
                UriBuilder uri = new UriBuilder(location);
                string path = Uri.UnescapeDataString(uri.Path);
                XmlDocument configurationDoc = new XmlDocument();
                configurationDoc.Load(Path.Combine(path, "Configuration.xml"));
                XmlNode operatorNode = configurationDoc.SelectSingleNode("/Configuration/Users/User[@LimsName='" + operatorLimsName + "']");
                OperatorKiwiName = operatorNode.Attributes["KiwiName"].Value;
                SysLog.Log(string.Format("Operator ID: {0}", OperatorKiwiName), LogLevel.Info);
            }
            catch
            {
                OperatorKiwiName = operatorLimsName;
            }
        }
    }

    public class ContainerTypeConfiguration
    {
        internal string ContainerTypeId { get; set; }
        internal string StoreName { get; set; }
        internal string StoreAddress { get; set; }
        internal int StorePort { get; set; }
        internal string[] Partitions { get; set; }
        internal string TransferStation { get; set; }
        internal string Buffer { get; set; }

        public ContainerTypeConfiguration(string containerTypeId, XmlNode connectionNode, XmlNode dataNode)
        {
            ContainerTypeId = containerTypeId;
            StoreName = connectionNode.SelectSingleNode("StoreName").InnerXml;
            StoreAddress = connectionNode.SelectSingleNode("StoreAddress").InnerXml;
            StorePort = Convert.ToInt32(connectionNode.SelectSingleNode("StorePort").InnerXml);
            Partitions = dataNode.SelectSingleNode("Partition").InnerXml.Split(',');
            TransferStation = dataNode.SelectSingleNode("TransferStation").InnerXml;
            Buffer = dataNode.SelectSingleNode("Buffer").InnerXml;
        }
    }
}
