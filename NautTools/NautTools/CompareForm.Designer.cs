﻿namespace KiwiExtension
{
    partial class CompareForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._dataGridView = new System.Windows.Forms.DataGridView();
            this._kiwiPosColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._kiwiBCColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._nautilusPosColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._nautilusBCColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // _dataGridView
            // 
            this._dataGridView.AllowUserToAddRows = false;
            this._dataGridView.AllowUserToDeleteRows = false;
            this._dataGridView.AllowUserToResizeRows = false;
            this._dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._kiwiPosColumn,
            this._kiwiBCColumn,
            this._nautilusPosColumn,
            this._nautilusBCColumn});
            this._dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dataGridView.Location = new System.Drawing.Point(0, 0);
            this._dataGridView.Name = "_dataGridView";
            this._dataGridView.RowHeadersVisible = false;
            this._dataGridView.Size = new System.Drawing.Size(604, 595);
            this._dataGridView.TabIndex = 0;
            // 
            // _kiwiPosColumn
            // 
            this._kiwiPosColumn.HeaderText = "Kiwi Position";
            this._kiwiPosColumn.Name = "_kiwiPosColumn";
            this._kiwiPosColumn.ReadOnly = true;
            // 
            // _kiwiBCColumn
            // 
            this._kiwiBCColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._kiwiBCColumn.HeaderText = "Kiwi Stregkode";
            this._kiwiBCColumn.Name = "_kiwiBCColumn";
            this._kiwiBCColumn.ReadOnly = true;
            // 
            // _nautilusPosColumn
            // 
            this._nautilusPosColumn.HeaderText = "Nautilus Position";
            this._nautilusPosColumn.Name = "_nautilusPosColumn";
            this._nautilusPosColumn.ReadOnly = true;
            // 
            // _nautilusBCColumn
            // 
            this._nautilusBCColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._nautilusBCColumn.HeaderText = "Nautilus Stregkode";
            this._nautilusBCColumn.Name = "_nautilusBCColumn";
            this._nautilusBCColumn.ReadOnly = true;
            // 
            // CompareForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 595);
            this.Controls.Add(this._dataGridView);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CompareForm";
            this.ShowIcon = false;
            this.Text = "Sammenlign Plade i Kiwi og Nautilus";
            this.Load += new System.EventHandler(this.CompareForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._dataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView _dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn _kiwiPosColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _kiwiBCColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _nautilusPosColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _nautilusBCColumn;
    }
}