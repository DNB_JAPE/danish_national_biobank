﻿using DNBTools;
using DNBTools.DataClasses;
using DNBTools.Kiwi;
using DNBTools.Nautilus;
using LSEXT;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Xml;
using Thermo.Nautilus.Extensions.UserInterface;

namespace NautTools
{
    /// <summary>
    /// Extension for sending orders to Kiwi robot freezer.
    /// </summary>
   // [Guid("d5a850c3-6b6f-47c5-addc-61eeb2df5711")]
    public class NautToolsEntityExtension : EntityExtension, IVersion
    {
        public static int OrderId;
        private string _logFile;
        /// <summary>
        /// Version must match the one registered in Nautilus in order for extension to be loaded.
        /// </summary>
        /// <returns></returns>
        /// 
        public int GetVersion()
        {
            return 2;
        }

        /// <summary>
        /// Called by Lims to decide if the extension should be listed in the right click menu.
        /// </summary>
        /// <param name="Parameters">Lims parameters (not used).</param>
        /// <returns>ExecuteExtension.exEnabled - Always allow this extension to start.</returns>
        public override EntityExtension.enumExtensionVisibility CanExecuteExtension()
        {

            return EntityExtension.enumExtensionVisibility.Enabled;
        }

        //public ExecuteExtension CanExecute(ref IExtensionParameters Parameters)
        //{
        //    return LSEXT.ExecuteExtension.exEnabled;
        //}

        /// <summary>
        /// Called by Lims to activate the extension.
        /// </summary>
        //public override void ExecuteExtension()
        //{
        //}

        public override void ExecuteExtension()
        {
            LimsCommunication limsCommunication = null;
            List<BiobankEntity> entities = new List<BiobankEntity>();
            string operatorId;
            StartupForm startForm = new StartupForm();
            startForm.Show();

            // Read SysLog configuration
            Configuration syslogConfig;
            try
            {
                syslogConfig = new Configuration();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Kunne ikke indlæse konfigurationen til Syslog: " + ex.Message);
                return;
            }
            SysLog.EnableLog(syslogConfig.SysLogIP, syslogConfig.SysLogName, syslogConfig.SysLogEnabled, syslogConfig.DebugLevel);

            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            string configurationPath = Path.GetDirectoryName(path);
            _logFile = Path.Combine(configurationPath, (DateTime.Today.ToShortDateString() + ".log"));
            string containerTypeId = string.Empty;
            
            try
            {
                SysLog.Log(string.Format("Operator: {0} with role: {1} and sessionID {2}", Parameters["OPERATOR_NAME"], Parameters["ROLE_NAME"], Parameters["SESSION_ID"]), LogLevel.Info);

                XmlDocument configurationDoc = new XmlDocument();

                configurationDoc.Load(Path.Combine(configurationPath, "Configuration.xml"));

                string logDir = configurationPath;// configurationDoc.SelectSingleNode("/Configuration/LogDir").InnerXml;
                SysLog.Log("Starter extension", LogLevel.Info);

                OrderId = 1;
                // Read entities from Nautilus
                
                try
                {
                    SysLog.Log(string.Format("Extension activated. {0} parameters in list.", Parameters.Count), LogLevel.Info);
                    ADODB.Recordset records = (ADODB.Recordset)Parameters["RECORDS"];
                    while (!records.EOF)
                    {
                        foreach (ADODB.Field field in records.Fields)
                        {
                            BiobankEntity entity;
                            string id = field.Value.ToString();
                            if (field.Name.Equals("ALIQUOT_ID"))
                            {
                                Thermo.Nautilus.Extensions.Model.Aliquot nautilusAliquot = Nautilus.Aliquots[id, "ALIQUOT_ID"];
                                string barcode = nautilusAliquot["EXTERNAL_REFERENCE"].ToString();
                                List<string> containerTypes = new List<string> { "41", "21", "42", "43" };
                                //if (nautilusAliquot["CONTAINER_TYPE_ID"] == null || !containerTypes.Contains(nautilusAliquot["CONTAINER_TYPE_ID"].ToString()))
                                //{
                                //    SysLog.Log(string.Format("Ugyldig container type id for: {0}", barcode), LogLevel.Alert);
                                //    MessageBox.Show("Ugyldig container type id for: " + barcode);
                                //    return;
                                //}
                                //else if (containerTypeId != string.Empty && containerTypeId != nautilusAliquot["CONTAINER_TYPE_ID"].ToString())
                                //{
                                //    SysLog.Log("Ikke alle prøver er af samme type", LogLevel.Alert);
                                //    MessageBox.Show("Ikke alle prøver er af samme type");
                                //    return;
                                //}
                                //else
                                //{
                                //    containerTypeId = nautilusAliquot["CONTAINER_TYPE_ID"].ToString();
                                //}
                                if (nautilusAliquot["CONTAINER_TYPE_ID"] != null && containerTypes.Contains(nautilusAliquot["CONTAINER_TYPE_ID"].ToString()))
                                    containerTypeId = nautilusAliquot["CONTAINER_TYPE_ID"].ToString();
                                entity = new Sample(id, barcode);
                            }
                            else if (field.Name.Equals("PLATE_ID"))
                            {
                                Thermo.Nautilus.Extensions.Model.Plate nautilusPlate = Nautilus.Plates[id, "PLATE_ID"];
                                string barcode = nautilusPlate["EXTERNAL_REFERENCE"].ToString();
                                entity = new Plate(id, barcode);
                            }
                            else
                            {
                                SysLog.Log("Nautilus objekt er hverken prøve eller plade", LogLevel.Alert);
                                MessageBox.Show("Nautilus objekt er hverken prøve eller plade");
                                return;
                            }
                            entities.Add(entity);
                        }
                        records.MoveNext();
                    }
                    if (entities.Count == 0)
                    {
                        SysLog.Log("Vælg venligst prøver eller plader inden extensionen åbnes", LogLevel.Alert);
                        MessageBox.Show("Vælg venligst prøver eller plader inden extensionen åbnes");
                        return;
                    }
                    if (entities[0] is Plate)
                    {
                        SysLog.Log("Kun Aliqouts!", LogLevel.Alert);
                        MessageBox.Show("Kun Aliqouts!");
                    }
                    operatorId = Parameters["OPERATOR_ID"].ToString();
                    SysLog.Log("Containertype valgt: " + containerTypeId, LogLevel.Info);
                }
                catch (Exception ex)
                {
                    SysLog.Log(string.Format("Kunne ikke læse data fra Nautilus: {0} ", ex.Message), LogLevel.Error);
                    MessageBox.Show("Kunne ikke læse data fra Nautilus: " + ex.Message);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                Log("Fejl ved opstart af extension: " + ex.Message);
            }
            
            // Read configuration
            SysLog.Log("Read configuration", LogLevel.Info);
            Configuration configuration;
            try
            {
                configuration = new Configuration(containerTypeId);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Kunne ikke indlæse konfigurationen: {0} ", ex.Message), LogLevel.Error);
                MessageBox.Show("Kunne ikke indlæse konfigurationen: " + ex.Message);
                return;
            }
            SysLog.Log("Create Lims communication med " + configuration.NautConStr, LogLevel.Info);

            try
            {
                limsCommunication = new LimsCommunication(configuration.NautConStr);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Kunne ikke Oprette forbindelse til Nautilus DB: {0} ", ex.Message), LogLevel.Error);
                MessageBox.Show("Kunne ikke Oprette forbindelse til Nautilus DB: " + ex.Message);
                return;
            }

            operatorId = limsCommunication.OperatorGetInitials(Parameters["OPERATOR_ID"]);
            SysLog.Log("Operator: " + operatorId, LogLevel.Info);

            MainForm myMain = new MainForm(limsCommunication, entities, operatorId, ref startForm);
            //startForm.Close();
            myMain.ShowDialog();

            Parameters["REFRESH"] = true;
            
            //kiwiCommunication.Dispose();
            limsCommunication.Dispose();
            SysLog.CloseSyslog();
        }

        /// <summary>
        /// Log message
        /// </summary>
        /// <param name="message"></param>
        internal static void Log(string message)
        {
            try
            {

            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            string configurationPath = Path.GetDirectoryName(path);
            string logFile = Path.Combine(configurationPath, (DateTime.Today.ToShortDateString() + ".log"));
            File.AppendAllText(logFile, DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + " " + message + Environment.NewLine);
            }
            catch (Exception ex)
            {
                SysLog.Log("Error in Log: " + ex.Message, LogLevel.Error);
            }

        }
    }
}
