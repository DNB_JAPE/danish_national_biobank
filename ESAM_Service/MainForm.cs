﻿using DNBTools;
using System;
using System.Windows.Forms;

namespace ESAMService
{
	public partial class MainForm : Form
	{
        SysLog myLog;
		public MainForm(SysLog mySysLog)
		{
			InitializeComponent();
            myLog = mySysLog;
		}

        string lastLog = string.Empty;

        delegate void UpdateListboxDelegate(string text);

        public void LogMessages(string log)
        {
            lastLog = log;
            if (this.listBox1.InvokeRequired)
            {
                UpdateListboxDelegate d = new UpdateListboxDelegate(LogMessages);
                this.Invoke(d, new object[] { log });
            }
            else
            {
                listBox1.Items.Insert(0, string.Format("{0} - {1}", DateTime.Now.ToString("HH:mm:ss"), log));
                if (listBox1.Items.Count > 300)
                    listBox1.Items.RemoveAt(300);
            }
        }

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void storeBoxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSetup_StoreBox frm = new FrmSetup_StoreBox();
            frm.ShowDialog();
        }

        private void webToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSetup_Databases frm = new FrmSetup_Databases(myLog);
            frm.ShowDialog();
        }
    }
}
