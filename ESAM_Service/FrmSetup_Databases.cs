﻿using DNBTools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IdentityModel;
using SimpleImpersonation;

namespace ESAMService
{
    public partial class FrmSetup_Databases : Form
    {
        string destinationDBName;
        string destinationDBServerName;
        string destinationDBUserName;
        string destinationDBUserPassword;
        string destinationDomain;

        string webDBName;
        string webDBServerName;
        string webDBUserName;
        string webDBUserPassword;
        string webSqlGetChanges;
        Int32 delayTime = 2;
        bool isADUser = true;
        bool useImpersonation = false;

        string webConnectionsString;
        string homeConnectionString;
        SysLog myLog;
        public FrmSetup_Databases(SysLog mySysLog)
        {
            InitializeComponent();
            myLog = mySysLog;
            ReadSetup();
        }

        void WriteSetup()
        {
            Configuration myConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            destinationDBName = myConfig.AppSettings.Settings["DestinationDBName"].Value = this.txtHomeDBName.Text;
            destinationDBServerName = myConfig.AppSettings.Settings["DestinationDBServerName"].Value = txtHomeDBServerName.Text;
            destinationDBUserName = myConfig.AppSettings.Settings["DestinationDBUserName"].Value = txtHomeDBUserName.Text;
            destinationDBUserPassword = myConfig.AppSettings.Settings["DestinationDBUserPassword"].Value = txtHomeDBUserPassword.Text;
            isADUser = Convert.ToBoolean(myConfig.AppSettings.Settings["IsADUser"].Value = chkAD.Checked.ToString());
            // Web database
            webDBName = myConfig.AppSettings.Settings["WebDBName"].Value = txtDBName.Text;
            webDBServerName = myConfig.AppSettings.Settings["WebDBServerName"].Value = txtServerName.Text;
            webDBUserName = myConfig.AppSettings.Settings["WebDBUserName"].Value = txtUserName.Text;
            webDBUserPassword = myConfig.AppSettings.Settings["WebDBUserPassword"].Value = txtPassword.Text;
            webSqlGetChanges= myConfig.AppSettings.Settings["WebSqlGetChanges"].Value = txtwebSqlGetChanges.Text;
            delayTime = Convert.ToInt32(myConfig.AppSettings.Settings["WaitTimeSeconds"].Value = cboDelay.Text);

            myConfig.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("AppSettings");
            CreateConnectionStrings();
            myLog.Log("Saving config", LogLevel.Info);
        }

        void ReadSetup()
        {
            // Own Database
            destinationDBName = ConfigurationManager.AppSettings["DestinationDBName"];
            destinationDBServerName = ConfigurationManager.AppSettings["DestinationDBServerName"];
            destinationDBUserName = ConfigurationManager.AppSettings["DestinationDBUserName"];
            destinationDBUserPassword = ConfigurationManager.AppSettings["DestinationDBUserPassword"];
            destinationDomain = ConfigurationManager.AppSettings["DestinationDomain"];
            // Web database
            webDBName = ConfigurationManager.AppSettings["WebDBName"];
            webDBServerName = ConfigurationManager.AppSettings["WebDBServerName"];
            webDBUserName = ConfigurationManager.AppSettings["WebDBUserName"];
            webDBUserPassword = ConfigurationManager.AppSettings["WebDBUserPassword"];
            webSqlGetChanges = ConfigurationManager.AppSettings["WebSqlGetChanges"];
            delayTime = Convert.ToInt32(ConfigurationManager.AppSettings["WaitTimeSeconds"]);
            isADUser = Convert.ToBoolean(ConfigurationManager.AppSettings["IsADUser"]);
            bool useImpersonation = Convert.ToBoolean(ConfigurationManager.AppSettings["DoImpersonate"]);

            this.txtHomeDBName.Text = destinationDBName;
            this.txtHomeDBServerName.Text = destinationDBServerName;
            this.txtHomeDBUserName.Text = destinationDBUserName;
            this.txtHomeDBUserPassword.Text = destinationDBUserPassword;
            this.chkAD.Checked = isADUser;

            this.txtServerName.Text = webDBServerName;
            this.txtDBName.Text = webDBName;
            this.txtUserName.Text = webDBUserName;
            this.txtPassword.Text = webDBUserPassword;
            this.txtwebSqlGetChanges.Text = webSqlGetChanges;
            this.cboDelay.Text = delayTime.ToString();
            
            CreateConnectionStrings();
            myLog.Log("Reading config", LogLevel.Info);
        }

        void CreateConnectionStrings()
        {
            if (isADUser)
                homeConnectionString = string.Format("Data Source= {0}; Initial Catalog={1}; Integrated Security=SSPI;", destinationDBServerName, destinationDBName);
            else
                homeConnectionString = string.Format("Data Source= {0}; Initial Catalog={1}; User Id={2}; Password={3};", destinationDBServerName, destinationDBName, destinationDBUserName, destinationDBUserPassword);

            webConnectionsString = string.Format("Data Source= {0}; Initial Catalog={1}; User Id={2}; Password={3};", webDBServerName, webDBName, webDBUserName, webDBUserPassword);
        }

        //public SqlConnection ConnectToDb(string ConnectionString)
        //{
        //    try
        //    {
        //        var conn = new SqlConnection();
        //        conn.ConnectionString = ConnectionString;
        //        conn.Open();
        //        return conn;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(string.Format("Error {0}", ex.ToString()));
        //        throw;
        //    }
        //}

        #region Buttons

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            myLog.Log("Testing Destination Database connection", LogLevel.Info);
            //if (isADUser)
            if (useImpersonation)
            {
                var credentials = new UserCredentials(destinationDomain, destinationDBUserName, destinationDBUserPassword);
                Impersonation.RunAsUser(credentials, LogonType.Interactive, () =>
                 {
                     TestDb(homeConnectionString);
                 });
            }
            else
                TestDb(homeConnectionString);

            myLog.Log("Testing Web Database connection", LogLevel.Info);
            TestDb(webConnectionsString);
        }

        void TestDb(string myConnectionString)
        {
            try
            {
                SqlConnection testDB = SQLTools.ConnectToDb(myConnectionString);
                if (testDB.State != ConnectionState.Open)
                    myLog.Log(string.Format("Connectionstate is: {0}", testDB.State), LogLevel.Info);
                else
                    myLog.Log("Test DB Connection is OK!", LogLevel.Info);
                testDB.Close();
            }
            catch (Exception ex)
            {
                this.tsStatus.Text=string.Format("Error: Couldn't connect because: {0}", ex.ToString());
                //myLog.Log(this.tsStatus.Text, LogLevel.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                // Save data
                WriteSetup();
                this.tsStatus.Text = "Data saved";
            }
            catch (Exception ex)
            {
                this.tsStatus.Text = "Error when saving: " + ex.ToString();
                myLog.Log(this.tsStatus.Text, LogLevel.Error);
            }
        }

        #endregion Buttons

        private void tsStatus_TextChanged(object sender, EventArgs e)
        {
            myLog.Log(this.tsStatus.Text, LogLevel.Debug);
        }
    }
}
