﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESAMService
{
    public class BasicQueueFeactures : ConfigurationElement
    {
        [ConfigurationProperty("HostName", DefaultValue = "", IsRequired = true)]
        public string HostName
        {
            get
            {
                return (string)this["HostName"];
            }
        }

        [ConfigurationProperty("QueueName", DefaultValue = "", IsRequired = true)]
        public string QueueName
        {
            get
            {
                return (string)this["QueueName"];
            }
        }

        [ConfigurationProperty("UserName", DefaultValue = "", IsRequired = true)]
        public string UserName
        {
            get
            {
                return (string)this["UserName"];
            }
        }

        [ConfigurationProperty("Password", DefaultValue = "", IsRequired = true)]
        public string Password
        {
            get
            {
                return (string)this["Password"];
            }
        }
    }

    public class ActionQueueFeatures : BasicQueueFeactures
    {
        [ConfigurationProperty("DelayTimeSeconds", DefaultValue = "11", IsRequired = true)]
        public string DelayTimeSeconds
        {
            get
            {
                return (string)this["DelayTimeSeconds"];
            }
        }

        [ConfigurationProperty("Durable", DefaultValue = "false", IsRequired = true)]
        public string Durable
        {
            get
            {
                return (string)this["Durable"];
            }
        }

        [ConfigurationProperty("DeleteAfterSeconds", DefaultValue = "0", IsRequired = false)]
        public string DeleteAfterSeconds
        {
            get
            {
                return (string)this["DeleteAfterSeconds"];
            }
        }
    }

    public class LogQueueFeatures : BasicQueueFeactures
    {
        [ConfigurationProperty("isListening", DefaultValue = "false", IsRequired = true)]
        public string isListening
        {
            get
            {
                return (string)this["isListening"];
            }
        }

        [ConfigurationProperty("Durable", DefaultValue = "false", IsRequired = true)]
        public string Durable
        {
            get
            {
                return (string)this["Durable"];
            }
        }
    }

    public class QueueSettings : ConfigurationSection
    {
        [ConfigurationProperty("LogQueueSetting")]
        public LogQueueFeatures LogQueueFeatures
        {
            get
            {
                return (LogQueueFeatures)this["LogQueueSetting"];
            }
            set
            {
                value = (LogQueueFeatures)this["LogQueueSetting"];
            }
        }

        [ConfigurationProperty("ActionQueueSetting")]
        public ActionQueueFeatures ActionQueueFeatures
        {
            get
            {
                return (ActionQueueFeatures)this["ActionQueueSetting"];
            }

            set
            {
                value = (ActionQueueFeatures)this["ActionQueueSetting"];
            }
        }

    }

    public class ServiceInfoSettings : ConfigurationSection
    {
        [ConfigurationProperty("ServiceInfoSetting")]
        public ServiceInfoFeatures ServiceInfoFeatures
        {
            get
            {
                return (ServiceInfoFeatures)this["ServiceInfoSetting"];
            }

            set
            {
                value = (ServiceInfoFeatures)this["ServiceInfoSetting"];
            }
        }
    }
}
