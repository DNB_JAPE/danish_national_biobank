﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESAMService
{
    [JsonObject(MemberSerialization.OptIn)]
    public class LogData
    {
        public enum LogLevel
        {
            Info = 0,
            Debug = 1,
            Error = 2,
            Critical = 3
        }

        public LogData()
        { }

        private string _Version = "1.0";
        [JsonProperty]
        public string Version { get { return _Version; } set { _Version = value; } }

        private string _Message = string.Empty;
        [JsonProperty]
        public string Message { get { return _Message; } set { _Message = value; } }

        private LogLevel _Level = LogLevel.Info;
        [JsonProperty]
        public LogLevel Level { get { return _Level; } set { _Level = value; } }

        private string _Name = string.Empty;
        [JsonProperty]
        public string Name { get { return _Name; } set { _Name = value; } }

        private DateTime _Time;
        [JsonProperty]
        public DateTime Time { get { return _Time; } set { _Time = value; } }

        private string _TCPIP = string.Empty;
        [JsonProperty]
        public string TCPIP { get { return _TCPIP; } set { _TCPIP = value; } }
    }
}
