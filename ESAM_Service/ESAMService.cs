﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace ESAMService
{
	public partial class ESAMService : ServiceBase
	{
		public ESAMService()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			Program.OnStart(args);
		}

		protected override void OnStop()
		{
			Program.OnStop();
		}
	}
}
