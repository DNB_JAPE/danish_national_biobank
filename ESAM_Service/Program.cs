﻿using System;
using System.IO;
using System.ServiceProcess;
using System.Threading;
using System.Windows.Forms;
using System.Configuration;
using DNBTools;

namespace ESAMService
{
	static public class Program
	{
		//static System.Threading.Timer theTimer;
		//static int lineNumber = 0;
		static bool serviceWasRunning = false;
		static MainForm mainForm;
        static MyWebManager myWebManager;
        //static MyDatabase myDatabase;
        //static MyStoreBox myStoreBox;
        static SysLog myLog;
        //static string sysLogServer;
        //static string sysLogName;
        //static bool syslogEnabled;
        static int debugLevel = 3;
        //static QueueManager myQueuesManager;
        
        [STAThread]
		static void Main(string[] args)
		{
            ReadAndSetup();
            Queues.InitQueue();
            myLog.Log("ESAM Service started", LogLevel.Info);
			if (Environment.UserInteractive)
			{
                myLog.Log("ESAM Service started with GUI", LogLevel.Info);

                
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                mainForm = new MainForm(myLog);


                DisableServiceIfRunning();
				OnStart(args);
				//Application.EnableVisualStyles();
				//Application.SetCompatibleTextRenderingDefault(false);
				//mainForm = new MainForm(myLog);
                Application.Run(mainForm);
				OnStop();
               
				EnableServiceIfWasRunning();
			}
			else
			{
                myLog.Log("ESAM Service started as service", LogLevel.Info);

                ServiceBase.Run(new ESAMService());
			}
		}

       

        private static void ReadAndSetup()
        {
            debugLevel = Convert.ToInt16(ConfigurationManager.AppSettings["DebugLevel"]);

            myLog = new SysLog();
            myLog.LogReceivedEvent += MyLog_LogReceivedEvent;
        }

        private static void MyLog_LogReceivedEvent(object sender, LogReceivedEventArgs args)
        {
            if (mainForm != null)
            mainForm.LogMessages(args.LogData);
        }

        static internal void OnStart(string[] args)
		{
            //	theTimer = new System.Threading.Timer(tickHandler, null, 1000, 2000);
            myLog.Log("ESAM Service start", LogLevel.Info);

            myWebManager = new MyWebManager(myLog);
        }

        static internal void OnStop()
		{
            myLog.Log("ESAM Service stop", LogLevel.Info);
            myWebManager.Stop();
            myWebManager.Dispose();
		}

		///// (summary)
		///// Write a line to the log file.  For simplicity, assume no race conditions.
		///// Modified to also display the line in the GUI if it is running.
		///// (/summary)
		///// (param name="state")(/param)
		//static private void tickHandler(object state)
		//{

		//	string line = string.Format("Line {0}{1}", ++lineNumber, Environment.NewLine);
		//	if (Environment.UserInteractive)
		//	{
		//		MethodInvoker action = () => 
		//		{
		//			mainForm.listBox1.Items.Add(line);
		//			mainForm.listBox1.TopIndex = mainForm.listBox1.Items.Count - 1;
		//		};
		//		mainForm.BeginInvoke(action);
		//	}
		//	File.AppendAllText(@"C:\ESAMService.txt", line);
		//}

        #region Service
        /// (summary)
        /// If the service is running, stop it.
        /// (/summary)
        private static void DisableServiceIfRunning()
		{
			try
			{
                myLog.Log("ESAM Service disabled", LogLevel.Info);
                //
                // ServiceController will throw System.InvalidOperationException if service not found.
                //
                ServiceController sc = new ServiceController("ESAMService");
				sc.Stop();
				serviceWasRunning = true;
				Thread.Sleep(1000);  // wait for service to stop
			}
			catch (Exception)
			{
			}
		}

		/// (summary)
		/// If the service was running, start it up again.
		/// (/summary)
		private static void EnableServiceIfWasRunning()
		{
			if (serviceWasRunning)
			{
				try
				{
                    myLog.Log("ESAM Service enabled", LogLevel.Info);
                    //
                    // ServiceController will throw System.InvalidOperationException if service not found.
                    //
                    ServiceController sc = new ServiceController("ESAMService");
					sc.Start();
				}
				catch (Exception)
				{
				}
			}
		}
        #endregion Service
    }

    internal static class Queues
    {
        public static QueueManager myQueueManager;
        //public static DNB_LogEntities myDb = new DNB_LogEntities();

        public static void InitQueue()
        {
            myQueueManager = new QueueManager();
        }
    }
}
