﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ESAMService
{
    static class SQLTools
    {
        public static SqlConnection ConnectToDb(string ConnectionString)
        {
            try
            {
                var conn = new SqlConnection();
                conn.ConnectionString = ConnectionString;
                conn.Open();
                return conn;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
