﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Dynamic;
using DNBTools;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Net;
using System.Runtime.Remoting.Contexts;
using System.Configuration;
using System.ComponentModel;

namespace ESAMService
{
    public class MyStoreBox : IDisposable
    {
        SysLog myLog;
        HttpClient client = new HttpClient();
        int debugLevel = 3;

        public MyStoreBox(SysLog _myLog)
        {
            debugLevel = Convert.ToInt32(ConfigurationManager.AppSettings["DebugLevel"]);
            myLog = _myLog;
        }

        public bool InitStoreBoxClient()
        {
            client = GetStoreBoxHttpClient();
            myLog.Log("StoreBox HttpClient Initialized ", LogLevel.Info);
            return true;
        }

        /// <summary>
        /// 1. Step
        /// Returns the email validation id from StoreBox to be used
        /// </summary>
        /// <param name="myUser"></param>
        /// <returns>EmailVerificationID</returns>
        public string LinkUser(UserData myUser)
        {
            string sb_Response = string.Empty;
            InitStoreBoxClient();
            string json = ConvertUserDataToJSONstring(myUser);
            if (debugLevel > 2)
                myLog.Log(string.Format("JSON file: {0}", json), LogLevel.Info);
            try
            {
                var task = client.PostAsync(@"/receipt-data/v1/storebox-users/link", new StringContent(json, Encoding.UTF8, "application/json"));
                task.Wait();
                var response = task.Result;
                LogRequest(response);
                if (response.IsSuccessStatusCode)
                {
                    var readTask = response.Content.ReadAsStringAsync();
                    readTask.Wait();
                    var dataObj = readTask.Result;

                    SBLinkResponse sbResponse = JsonConvert.DeserializeObject<SBLinkResponse>(dataObj);
                    myLog.Log(string.Format("Response: {0} on {1}", sbResponse.emailValidationId, json), LogLevel.Info);
                    sb_Response = sbResponse.emailValidationId;
                }
                else
                {
                    myLog.Log(string.Format("Response: {0} on {1}", response.StatusCode, json), LogLevel.Warning);
                    Console.WriteLine(" {0} ({1}", response.StatusCode, response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                myLog.Log(string.Format("Error in Link User: [{0}] - [{1}]", ex.ToString(), ex.ToString()), LogLevel.Error);
            }
            return sb_Response;
        }

        /// <summary>
        /// 2. step
        /// Storebox receives temporary id and pin
        /// This will return the StoreboxID which will later be used 
        /// to receive receipts from Storeboxfor for the user.
        /// </summary>
        /// <param name="myUser"></param>
        /// <returns>StoreboxID</returns>
        public string VerifyUser(UserData myUser)
        {
            string sb_Response = string.Empty;
            try
            {
                PrepareClientForGet();

                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(string.Format("{0}/receipt-data/v1/storebox-users/link/{1}/{2}", TargetUri, myUser.EmailVerificationID, myUser.EmailPIN)),
                    Method = HttpMethod.Get
                    
                };
                request.Headers.Authorization = new AuthenticationHeaderValue(ApiKey);

                var task = client.SendAsync(request);

                task.Wait();
                var response = task.Result;
                LogRequest(response);
                if (response.IsSuccessStatusCode)
                {
                    var readTask = response.Content.ReadAsStringAsync();
                    readTask.Wait();
                    var dataObj = readTask.Result;
                    SBVerifyResponse sbResponse = JsonConvert.DeserializeObject<SBVerifyResponse>(dataObj);
                    sb_Response = sbResponse.userId;
                    myLog.Log(string.Format("Returned UserID: {0} ", sb_Response), LogLevel.Info);
                }
                else//sb_Response = JsonConvert.DeserializeObject<SBVerifyResponse>(dataObj);
                {
                    myLog.Log(string.Format("Response status: {0} - [{1}]", response.StatusCode, response.ReasonPhrase), LogLevel.Info);
                    Console.WriteLine("Response status: {0} - [{1}]", response.StatusCode, response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                myLog.Log(string.Format("Error in Verify User: [{0}] - [{1}]", ex.ToString(), ex.ToString()), LogLevel.Error);
            }

            return sb_Response;
        }

        public SBAllReceiptsResponse GetAllReceiptsForUser(UserData myUser)
        {
            //.path("/receipt-data/v1/users/dxvnpp0bxn0o4s3hiz9ymsx6a799kelw/receipts") //all receipts, general view
            SBAllReceiptsResponse sb_Response = new SBAllReceiptsResponse();
            try
            {
                PrepareClientForGet();

                SBAllReceiptRequest req = new SBAllReceiptRequest();
                req.folderId = "inbox";
                req.orderBy = "total";
                req.direction = "asc";
                req.search = "B";
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(string.Format("{0}/receipt-data/v1/users/{1}/receipts?{2}", TargetUri, myUser.StoreBoxID, req.GetSearch())),
                    Method = HttpMethod.Get
                };
                request.Headers.Authorization = new AuthenticationHeaderValue(ApiKey);

                var task = client.SendAsync(request);
                task.Wait();
                var response = task.Result;
                LogRequest(response);
                if (response.IsSuccessStatusCode)
                {
                    var readTask = response.Content.ReadAsStringAsync();
                    readTask.Wait();
                    var dataObj = readTask.Result;
                    SBAllReceiptsResponse sbResponse = JsonConvert.DeserializeObject<SBAllReceiptsResponse>(dataObj);
                    sb_Response = sbResponse;
                }
                else
                {
                    myLog.Log(string.Format("Response status: {0} - [{1}]", response.StatusCode, response.ReasonPhrase), LogLevel.Info);
                    Console.WriteLine("Response status: {0} - [{1}]", response.StatusCode, response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                myLog.Log(string.Format("Error in GetAllReceiptsForUser: [{0}] - [{1}]", ex.ToString(), ex.ToString()), LogLevel.Error);
            }
            return sb_Response;
        }

        public List<Tuple<string,string>> GetReceiptDetailsInJSON(UserData myUser, Receipt[] myReceipts)
        {
            List<Tuple<string,string>> receiptDetailsList = new List<Tuple<string, string>>();
            try
            {
                PrepareClientForGet();
                foreach (Receipt receipt in myReceipts)
                {
                    try
                    {
                        SBGetReceiptDetailsRequest req = new SBGetReceiptDetailsRequest()
                        {
                            userID = myUser.StoreBoxID,
                            receiptID = receipt.ReceiptID
                        };

                        var request = new HttpRequestMessage()
                        {
                            RequestUri = new Uri(string.Format("{0}/receipt-data/v1/users/{1}/receipts/{2}", TargetUri, myUser.StoreBoxID, req.receiptID)),
                            Method = HttpMethod.Get
                        };
                        request.Headers.Authorization = new AuthenticationHeaderValue(ApiKey);

                        var task = client.SendAsync(request);
                        task.Wait();
                        var response = task.Result;
                        LogRequest(response);
                        if (response.IsSuccessStatusCode)
                        {
                            var readTask = response.Content.ReadAsStringAsync();
                            readTask.Wait();
                            var dataObj = readTask.Result;

                            receiptDetailsList.Add(new Tuple<string, string>(dataObj.ToString(), receipt.ReceiptID));
                        }
                        else
                        {
                            myLog.Log(string.Format("Response status: {0} - [{1}]", response.StatusCode, response.ReasonPhrase), LogLevel.Info);
                            Console.WriteLine("Response status: {0} - [{1}]", response.StatusCode, response.ReasonPhrase);
                        }
                    }
                    catch (Exception ex)
                    {
                        myLog.Log(string.Format("Error in GetReceiptDetailsForReceipt: [{0}] - [{1}] Trying to continue...", ex.ToString(), ex.ToString()), LogLevel.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                myLog.Log(string.Format("Error in GetReceiptDetailsForUser: [{0}] - [{1}]", ex.ToString(), ex.ToString()), LogLevel.Error);
            }
            return receiptDetailsList;
        }

        public List<ReceiptDetails> GetReceiptDetails(UserData myUser, Receipt[] myReceipts)
        {
            List<ReceiptDetails> receiptDetailsList = new List<ReceiptDetails>();
            try
            {
                PrepareClientForGet();
                foreach (Receipt receipt in myReceipts)
                {
                    try
                    {
                        SBGetReceiptDetailsRequest req = new SBGetReceiptDetailsRequest()
                        {
                            userID = myUser.StoreBoxID,
                            receiptID = receipt.ReceiptID
                        };

                        var request = new HttpRequestMessage()
                        {
                            RequestUri = new Uri(string.Format("{0}/receipt-data/v1/users/{1}/receipts/{2}", TargetUri, myUser.StoreBoxID, req.receiptID)),
                            Method = HttpMethod.Get
                        };
                        request.Headers.Authorization = new AuthenticationHeaderValue(ApiKey);

                        var task = client.SendAsync(request);
                        task.Wait();
                        var response = task.Result;
                        LogRequest(response);
                        if (response.IsSuccessStatusCode)
                        {
                            var readTask = response.Content.ReadAsStringAsync();
                            readTask.Wait();
                            var dataObj = readTask.Result;
                            ReceiptDetails sbResponse = JsonConvert.DeserializeObject<ReceiptDetails>(dataObj);
                            receiptDetailsList.Add(sbResponse);
                        }
                        else
                        {
                            myLog.Log(string.Format("Response status: {0} - [{1}]", response.StatusCode, response.ReasonPhrase), LogLevel.Info);
                            Console.WriteLine("Response status: {0} - [{1}]", response.StatusCode, response.ReasonPhrase);
                        }
                    }
                    catch (Exception ex)
                    {
                        myLog.Log(string.Format("Error in GetReceiptDetailsForReceipt: [{0}] - [{1}] Trying to continue...", ex.ToString(), ex.ToString()), LogLevel.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                myLog.Log(string.Format("Error in GetReceiptDetailsForUser: [{0}] - [{1}]", ex.ToString(), ex.ToString()), LogLevel.Error);
            }
            return receiptDetailsList;
        }

        private void PrepareClientForGet()
        {
            client.DefaultRequestHeaders.Accept.Clear();

            // Have to remove the field ContentType from InvalidFields because it will fail if not.
            var field = typeof(System.Net.Http.Headers.HttpRequestHeaders).GetField("invalidHeaders", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static) ?? typeof(System.Net.Http.Headers.HttpRequestHeaders).GetField("s_invalidHeaders", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
            if (field != null)
            {
                var invalidFields = (HashSet<string>)field.GetValue(null);
                invalidFields.Remove("Content-Type");
            }
            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(ApiKey);
        }

        private string ConvertUserDataToJSONstring(UserData myUser)
        {
            dynamic myMainJSON = new ExpandoObject();
            dynamic myMailJSON = new ExpandoObject();
            dynamic myMobileJSON = new ExpandoObject();
            myMailJSON.value = myUser.Email;
            myMailJSON.sendOtp = (Tools.IsNullOrEmpty(myUser.EmailPIN)); // If true it will send a new code to the email address.
            myMailJSON.length = 4;
            myMainJSON.email = myMailJSON;
            myMobileJSON.value = myUser.MobileNo;
            myMobileJSON.sendOtp = false;
            myMainJSON.mobileNumber = myMobileJSON;
            myMainJSON.language = "da";
            return JsonConvert.SerializeObject(myMainJSON);
        }

        private HttpClient GetStoreBoxHttpClient(string Path)
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(new Uri(TargetUri), Path);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(ApiKey);
            return client;
        }

        private HttpClient GetStoreBoxHttpClient()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(TargetUri);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(ApiKey);
            return client;
        }

        /// <summary>
        /// Just for testing purpose :-)
        /// </summary>
        /// <param name="me"></param>
        /// <returns></returns>
        public bool CheckUser(UserData me)
        {
            if (me.StoreBoxID != string.Empty)
            {
                // Get receipts
                SBAllReceiptsResponse myReceipts = GetAllReceiptsForUser(me);
            }
            else
            {
                if ((me.EmailVerificationID == null) || me.EmailVerificationID == string.Empty)
                {   // Not registered yet
                    string tmpID = LinkUser(me);
                    if (tmpID != null && tmpID != string.Empty)
                    {
                        me.EmailVerificationID = tmpID;
                    }
                    else
                    {
                        Console.WriteLine("Didn't get the email verification ID :-( Check data...");
                    }
                }
                else
                {
                    if (me.EmailPIN == null || me.EmailPIN == string.Empty)
                    {
                        // Waiting for PIN code...
                    }
                    else
                    {
                        me.StoreBoxID = VerifyUser(me);
                    }
                }
            }
            return true;
        }

        private void LogRequest(HttpResponseMessage response)
        {
            if (debugLevel > 2)
            {
                myLog.Log(string.Format("Request Message: {0}", response.RequestMessage), LogLevel.Info);
                //myLog.Log(string.Format("Message Content: {0}", response.Content), LogLevel.Info);
            }
            Console.WriteLine("Request Message;- \n\n" + response.RequestMessage);
            Console.WriteLine("Message Content;- \n\n" + response.Content);
        }

        #region Properties

        private bool _isTestMode = true;
        /// <summary>
        /// Default value=True
        /// </summary>
        public bool isTestMode { get { return _isTestMode; } set { _isTestMode = value; } }

        private string _TargetUri = "https://rda.storebox.com";
        private string _TargetTestUri = "https://test-rda.storebox.com";
        /// <summary>
        /// Target Uri for JSON com.
        /// When isTestMode it returns "https://test-rda.storebox.com"
        /// else "https://rda.storebox.com"
        /// </summary>
        public string TargetUri
        {
            get
            {
                //if (_isTestMode)
                //    return _TargetTestUri;
                //else
                //    return _TargetUri;
                return (_isTestMode ? _TargetTestUri : _TargetUri);
            }
            set { _TargetUri = value; }
        }

        private string _AuthorizationName = string.Empty;

        //public string AuthorizationName
        //{
        //    get { return _AuthorizationName; }
        //    set { _AuthorizationName = value; }
        //}

        private string _ApiKey = string.Empty;

        public string ApiKey
        {
            get { return _ApiKey; }
            set { _ApiKey = value; }
        }

        #endregion Properties

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    client.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~myStoreBox() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }

    [JsonObject(MemberSerialization.OptIn)]
    public class UserData
    {
        public UserData()
        { }

        private string _UserID = string.Empty;
        [JsonProperty]
        public string UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _NemID = string.Empty;
        [JsonProperty]
        public string NemID
        {
            get { return _NemID; }
            set { _NemID = value; }
        }

        private string _Email = string.Empty;
        [JsonProperty]
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        private string _MobileNo = string.Empty;
        [JsonProperty]
        public string MobileNo
        {
            get { return _MobileNo; }
            set { _MobileNo = value; }
        }

        private string _StoreBoxID = string.Empty;
        [JsonProperty]
        public string StoreBoxID
        {
            get { return _StoreBoxID; }
            set { _StoreBoxID = value; }
        }

        private string _EmailVerificationID = string.Empty;
        [JsonProperty]
        public string EmailVerificationID
        {
            get { return _EmailVerificationID; }
            set { _EmailVerificationID = value; }
        }

        private string _EmailPIN = string.Empty;
        [JsonProperty]
        public string EmailPIN
        {
            get { return _EmailPIN; }
            set { _EmailPIN = value; }
        }
        /// <summary>
        /// Returns this object as JSON
        /// </summary>
        public string JSON_Rep
        {
            get
            {
                return JsonConvert.SerializeObject(this);
            }
        }

        // Following properties won't be made available to JSON object

        private bool _Active = true;
        public bool Active
        {
            get { return _Active; }
            set { _Active = value; }
        }

    }

    public class SBLinkResponse
    {
        public string emailValidationId { get; set; }

        public Uri HeadersLocation { get; set; }
    }

    public class SBVerifyResponse
    {
        public string userId { get; set; }
    }

    public class PublicContentHandler : HttpClientHandler
    {
        public PublicContentHandler()
        {
            AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
        }
    }

    public class SBAllReceiptRequest
    {
        public SBAllReceiptRequest()
        { }

        public string GetSearch()
        {
            string res = string.Empty;
            res = "folderId=" + folderId;
            res += (orderBy == string.Empty ? "" : "&orderBy=" + orderBy);
            res += (direction == string.Empty ? "" : "&direction=" + direction);
            res += (search == string.Empty ? "" : "&search=" + search);
            res += (offset == null ? "" : "&offset=" + offset.ToString());
            res += (limit == null ? "" : "&limit=" + limit.ToString());
            res += (merchantId == string.Empty ? "" : "&merchantId=" + merchantId);
            res += (startDate == string.Empty ? "" : "&startDate=" + startDate);
            res += (endDate == string.Empty ? "" : "&endDate=" + endDate);
            res += (startGrandTotal == null ? "" : "&startGrandTotal=" + startGrandTotal.ToString());
            res += (endGrandTotal == null ? "" : "&endGrandTotal=" + endGrandTotal.ToString());
            return res;
        }
        private string _folderId = "inbox";
        /// <summary>
        /// string M The folder to list
        /// </summary>
        public string folderId { get { return _folderId; } set { _folderId = value; } }            // folderId string M The folder to list

        private string _orderBy = string.Empty;
        /// <summary>
        /// string M One of store, date, total
        /// </summary>
        public string orderBy { get { return _orderBy; } set { _orderBy = value; } }             // orderBy string M One of store, date, total

        private string _direction = string.Empty;
        /// <summary>
        /// string M One of asc, desc
        /// </summary>
        public string direction { get { return _direction; } set { _direction = value; } }           // direction string M One of asc, desc

        private string _search = string.Empty;
        /// <summary>
        /// string O A term to search for
        /// </summary>
        public string search { get { return _search; } set { _search = value; } }              // search string O A term to search for

        private int? _offset = null;
        /// <summary>
        /// integer O Start at this result index
        /// </summary>
        public int? offset { get { return _offset; } set { _offset = value; } }                 // offset integer O Start at this result index

        private int? _limit = null;
        /// <summary>
        /// integer O The number of results to fetch
        /// </summary>
        public int? limit { get { return _limit; } set { _limit = value; } }                  // limit integer O The number of results to fetch

        private string _merchantId = string.Empty;
        /// <summary>
        /// string O The merchant to fetch
        /// </summary>
        public string merchantId { get { return _merchantId; } set { _merchantId = value; } }          // merchantId string O The merchant to fetch

        private string _startDate = string.Empty;
        /// <summary>
        /// string O Date search range start in ISO - 8601 format.Example: 2015 - 06 - 29T15:50:12 % 2B02:00
        /// </summary>
        public string startDate { get { return _startDate; } set { _startDate = value; } }           // startDate string O Date search range start in ISO - 8601 format.Example: 2015 - 06 - 29T15:50:12 % 2B02:00

        private string _endDate = string.Empty;
        /// <summary>
        /// string O Date search range end in ISO - 8601 format
        /// </summary>
        public string endDate { get { return _endDate; } set { _endDate = value; } }             // endDate string O Date search range end in ISO - 8601 format

        private decimal? _startGrandTotal = null;
        /// <summary>
        /// decimal O Grandtotal search range start.Example: 150.50
        /// </summary>
        public decimal? startGrandTotal { get { return _startGrandTotal; } set { _startGrandTotal = value; } }    // startGrandTotal decimal O Grandtotal search range start.Example: 150.50

        private decimal? _endGrandTotal = null;
        /// <summary>
        /// decimal O Grandtotal search range end
        /// </summary>
        public decimal? endGrandTotal { get { return _endGrandTotal; } set { _endGrandTotal = value; } }      // endGrandTotal decimal O Grandtotal search range end

    }

    public class SBAllReceiptsResponse
    {
        public int totalSize { get; set; }          //integer M The total number of results
        public Receipt[] receipts { get; set; }     //receipts array M The result of the call
    }
    public class Receipt
    {
        public string ReceiptID { get; set; }       // Receipt ID
        public string storeName { get; set; }       //  string M Store name
        public string purchaseDate { get; set; }    // string M Date of purchas
        public decimal grandTotal { get; set; }     // number M The grand total of the receipt
        public string copiedFromUser { get; set; }  // string O If the receipt has been copied from another user, this field contains that users userId.
        public Logo logo { get; set; }              // object O The logo of the merchant.If there is any.
    }

    #region ReceiptDetails

    public class SBGetReceiptDetailsRequest
    {
        public string userID { get; set; }
        public string receiptID { get; set; }
    }

    public class ReceiptDetails
    {
        public string receiptId { get; set; }           // The unique identifier for the Storebox receipt
        public string merchantId { get; set; }          // The identifier of the merchant or retailer chain where the receipt origins
        public string purchaseDateTime { get; set; }    // The timestamp of the transaction in ISO 8601 format
        public GrandTotal grandTotal { get; set; }
        public Merchant merchant { get; set; }
        public ReceiptLines[] receiptLines { get; set; }
        public Payments[] payments { get; set; }
        public Barcode barcode { get; set; }
        public CopyReceiptHistory[] copyReceiptHistory { get; set; }
    }

    public class GrandTotal
    {
        public decimal value { get; set; }   // The total charge price of the receipt
        public decimal vat { get; set; }     // number M The full vat of the receipt
    }

    public class Merchant
    {
        public string merchantId { get; set; }          // The identifier of the merchant or retailer chain where the receipt origins

        public string name { get; set; }
        public string logoFilename { get; set; }
        public string addressLine1 { get; set; }
        public string addressLine2 { get; set; }
        public string zipCode { get; set; }
        public string city { get; set; }
        public string phoneNumber { get; set; }
    }

    public class ReceiptLines
    {
        public string name { get; set; }
        public string description { get; set; }
        public ItemPrice itemPrice { get; set; }
        public ItemPrice totalPrice { get; set; }
        public decimal amount { get; set; }

    }

    public class ItemPrice
    {
        public decimal value { get; set; }
        public decimal vat { get; set; }
    }
    public class Payments
    {
        public string paymentType { get; set; }
        public decimal priceValue { get; set; }
        public string priceCurrency { get; set; }
    }

    public class Barcode
    {
        public string type { get; set; }
        public string value { get; set; }
        public string displayValue { get; set; }
    }
    
    public class CopyReceiptHistory
    {
        public CopiedFromUser copiedFromUser { get; set; }
        public CopiedToUserIDs[] copiedToUserIds { get; set; }
    }

    public class CopiedFromUser
    {
        public string userId{ get; set; }
        public string dateOfCopy { get; set; }
    }

    public class CopiedToUserIDs
    { }
    public class Logo
    {
        public string name { get; set; }            // string M The name of the logo
        public string version { get; set; }         // string M The logoversion

    }

    #endregion ReceiptDetails
}
