﻿using DNBTools;
using ESAMService.DMZ;
using ESAMService.Helper;
using SimpleImpersonation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESAMService
{
    public class MyDatabase
    {
        string destinationDBName;
        string destinationDBServerName;
        string destinationDBUserName;
        string destinationDBUserPassword;
        string destinationConnectionString;
        string destinationDomain;

        bool useImpersonation = false;
        bool isADUser = true;
        int debugLevel = 3;
        SysLog myLog;
        myParticipantHelper myParticipants;
        myReceiptsHelper myReceipts;

        public MyDatabase(SysLog mySysLog)
        {
            myLog = mySysLog;
            ReadSetup();
            myParticipants = new myParticipantHelper(mySysLog);
            myReceipts = new myReceiptsHelper(mySysLog);
        }

        void ReadSetup()
        {
            destinationDBName = ConfigurationManager.AppSettings["DestinationDBName"];
            destinationDBServerName = ConfigurationManager.AppSettings["DestinationDBServerName"];
            destinationDBUserName = ConfigurationManager.AppSettings["DestinationDBUserName"];
            destinationDBUserPassword = ConfigurationManager.AppSettings["DestinationDBUserPassword"];
            destinationDomain= ConfigurationManager.AppSettings["DestinationDomain"];
            isADUser = Convert.ToBoolean(ConfigurationManager.AppSettings["IsDestinationADUser"]);
            useImpersonation = Convert.ToBoolean(ConfigurationManager.AppSettings["DoImpersonate"]);

            if (isADUser)
                destinationConnectionString = string.Format("Data Source= {0}; Initial Catalog={1}; Integrated Security=SSPI;", destinationDBServerName, destinationDBName);
            else
            {
                //destinationConnectionString = string.Format("Data Source= {0}; Initial Catalog={1}; User Id={2}; Password={3};", destinationDBServerName, destinationDBName, destinationDBUserName, destinationDBUserPassword);
                // Needed to change this because serviceuser has access this way. 
                destinationConnectionString = string.Format("Data Source= {0}; Initial Catalog={1}; Integrated Security=SSPI;", destinationDBServerName, destinationDBName);
            }
            debugLevel = Convert.ToInt32(ConfigurationManager.AppSettings["DebugLevel"]);

            if (debugLevel > 2)
            {
                myLog.Log(string.Format("LAN DB Service started. Con.string: {0}", destinationConnectionString), LogLevel.Info);
            }
        }
 
        public bool AddUserData(List<Users> newUsers)
        {
            bool l_Reply = false;
            try
            {
                using (SqlConnection myDB = SQLTools.ConnectToDb(destinationConnectionString))
                {
                    if (myDB.State == System.Data.ConnectionState.Open)
                    {
                        foreach (Users users in newUsers)
                        {
                            string insertUsers = "INSERT INTO ESAM_LANDING.USERS (Email, NemId, Phone, StoreboxEmail, StoreboxPhone, RegistrationDate, Active, new, " +
                                "AcceptUseData, ParticipateInProject, ContactMe, SendMeNewsLetter, ValidationCode, AuthenticationInfo, StoreboxConsent, ProjectConsent) "
                                + "VALUES('" + (users.Email == null ? string.Empty : users.Email) + "', '" + (users.NemId == null ? string.Empty : users.NemId) +
                                "', '" + (users.Phone == null ? string.Empty : users.Phone) + "', '" + (users.StoreboxEmail == null ? string.Empty : users.StoreboxEmail) +
                                "', '" + (users.StoreboxPhone == null ? string.Empty : users.StoreboxPhone) +
                                "', '" + (users.RegistrationDate == null ? string.Empty : ((DateTime)users.RegistrationDate).ToString("s")) +
                                "', " + ((users.Active == null || (bool)users.Active == false) ? "0" : "1") +
                                ", 1, " + ((users.AcceptUseData) == null ? "0" : ((bool)users.AcceptUseData ? "1" : "0")) +
                                ", " + ((users.ParticipateInProject) == null ? "0" : ((bool)users.ParticipateInProject ? "1" : "0")) +
                                ", " + (users.ContactMe == null ? "0" : ((bool)users.ContactMe ? "1" : "0")) +
                                ", " + (users.SendMeNewsLetter == null ? "0" : ((bool)users.SendMeNewsLetter ? "1" : "0")) +
                                ", '" + (users.ValidationCode == null ? string.Empty : users.ValidationCode) +
                                "', '" + (users.AuthenticationInfo == null ? string.Empty : users.AuthenticationInfo) +
                                "', '" + (users.StoreboxConsent == null ? "0" : users.StoreboxConsent.ToString()) +
                                "', '" + (users.ProjectConsent == null ? string.Empty : users.ProjectConsent) + "')";

                            using (SqlCommand cmd = new SqlCommand(insertUsers, myDB))
                            {
                                cmd.ExecuteNonQuery();
                            }

                            //if (isADUser)
                            if (useImpersonation)
                            {
                                var credentials = new UserCredentials(destinationDomain, destinationDBUserName, destinationDBUserPassword);
                                Impersonation.RunAsUser(credentials, LogonType.Interactive, () =>
                                {
                                    myParticipants.UpdateParticipantsFromDMZ(users);
                                });
                            }
                            else
                                myParticipants.UpdateParticipantsFromDMZ(users);

                            l_Reply = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                myLog.Log("Service: Error when adding data from Web: " + ex.ToString(), LogLevel.Error);
                l_Reply = false;
            }
            return l_Reply;
        }

        public bool AddReceipts(SBAllReceiptsResponse userReceipts, UserData user)
        {
            bool l_Reply = false;
            try
            {
                //if (isADUser)
                if (useImpersonation)
                {
                    var credentials = new UserCredentials(destinationDomain, destinationDBUserName, destinationDBUserPassword);
                    Impersonation.RunAsUser(credentials, LogonType.Interactive, () =>
                    {
                        l_Reply = myReceipts.AddReceiptsForUser(userReceipts, myParticipants.GetParticipantID(user));
                    });
                }
                else
                    l_Reply = myReceipts.AddReceiptsForUser(userReceipts, myParticipants.GetParticipantID(user));

                //l_Reply = myReceipts.AddReceiptsForUser(userReceipts, myParticipants.GetParticipantID(user));
            }
            catch (Exception ex)
            {
                myLog.Log(string.Format("Error in saving receipts: [{0}] - [{1}]", ex.ToString(), ex.ToString()), LogLevel.Error);
            }
            return l_Reply;
        }

        /// <summary>
        /// Returning Tuple<JSON string with details, ReceiptID>
        /// </summary>
        /// <param name="userReceiptDetails"></param>
        /// <param name="user"></param>
        public void AddReceiptDetails(List<Tuple<string,string>> userReceiptDetails, UserData user)
        {
            try
            {
                //if (isADUser)
                if (useImpersonation)
                {
                    var credentials = new UserCredentials(destinationDomain, destinationDBUserName, destinationDBUserPassword);
                    Impersonation.RunAsUser(credentials, LogonType.Interactive, () =>
                    {
                        myReceipts.AddReceiptDetailsForUser(userReceiptDetails, user);
                    });
                }
                else
                    myReceipts.AddReceiptDetailsForUser(userReceiptDetails, user);
            }
            catch (Exception ex)
            {
                myLog.Log(string.Format("Error in saving receiptdetails: [{0}] - [{1}]", ex.ToString(), ex.ToString()), LogLevel.Error);
            }
        }

        public void AddReceiptDetails(List<ReceiptDetails> userReceiptDetails, UserData user)
        {
            try
            {
                //if (isADUser)
                if (useImpersonation)
                {
                    var credentials = new UserCredentials(destinationDomain, destinationDBUserName, destinationDBUserPassword);
                    Impersonation.RunAsUser(credentials, LogonType.Interactive, () =>
                    {
                        myReceipts.AddReceiptDetailsForUser(userReceiptDetails, user);
                    });
                }
                else
                    myReceipts.AddReceiptDetailsForUser(userReceiptDetails, user);
            }
            catch (Exception ex)
            {
                myLog.Log(string.Format("Error in saving receiptdetails: [{0}] - [{1}]", ex.ToString(), ex.ToString()), LogLevel.Error);
            }
        }

        public bool UpdateUserSB(UserData myUser)
        {
            bool l_Reply = false;
            try
            {
                if (myUser.EmailVerificationID == string.Empty && myUser.StoreBoxID == string.Empty && myUser.EmailPIN == string.Empty)
                {
                    if (debugLevel > 2)
                        myLog.Log("No data to save", LogLevel.Info);
                }
                else
                {
                    //if (isADUser)
                    if (useImpersonation)
                    {
                        var credentials = new UserCredentials(destinationDomain, destinationDBUserName, destinationDBUserPassword);
                        Impersonation.RunAsUser(credentials, LogonType.Interactive, () =>
                        {
                            myParticipants.UpdateParticipantSBInfo(myUser);
                        });
                    }
                    else
                        myParticipants.UpdateParticipantSBInfo(myUser);
                    l_Reply = true;
                }
            }
            catch (Exception ex)
            {
                myLog.Log("Service: Error when updating user data: " + ex.ToString(), LogLevel.Error);
                l_Reply = false;
            }
            return l_Reply;
        }

        /// <summary>
        /// Deactivates user from NemID
        /// Sets active bit to 0
        /// </summary>
        /// <param name="nemID"></param>
        /// <returns></returns>
        public bool DeactivateUser(string nemID)
        {
            bool l_Reply = false;
            try
            {
                using (SqlConnection myDB = SQLTools.ConnectToDb(destinationConnectionString))
                {
                    if (myDB.State == System.Data.ConnectionState.Open)
                    {
                        string updateUser = "UPDATE ESAM_LANDING.USERS SET Active = 0 " +
                            "WHERE NemId='@id'";

                        using (SqlCommand cmd = new SqlCommand(updateUser, myDB))
                        {
                            cmd.Parameters.AddWithValue("@id", nemID);
                            cmd.ExecuteNonQuery();
                            l_Reply = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                myLog.Log(string.Format("Service: Error when deactivating user[{0}]: {1}", nemID, ex.ToString()), LogLevel.Error);
                l_Reply = false;
            }
            return l_Reply;

        }
    }
}
