﻿using DNBTools;
using ESAMService.DMZ;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESAMService
{
    public class MyWebDatabase
    {
        string webDBName;
        string webDBServerName;
        string webDBUserName;
        string webDBUserPassword;
        string webConnectionsString;
        string webSqlGetChanges;
        int debugLevel = 3;
        SysLog myLog;
        ESAM_EXT_TESTEntities myDB = new ESAM_EXT_TESTEntities();

        public MyWebDatabase(SysLog mySysLog)
        {
            myLog = mySysLog;
            ReadSetup();
        }

        void ReadSetup()
        {
            
            webDBName = ConfigurationManager.AppSettings["WebDBName"];
            webDBServerName = ConfigurationManager.AppSettings["WebDBServerName"];
            webDBUserName = ConfigurationManager.AppSettings["WebDBUserName"];
            webDBUserPassword = ConfigurationManager.AppSettings["WebDBUserPassword"];
            // Obsolete...
            webSqlGetChanges = ConfigurationManager.AppSettings["webSqlGetChanges"];
            webConnectionsString = string.Format("Data Source= {0}; Initial Catalog={1}; User Id={2}; Password={3};", webDBServerName, webDBName, webDBUserName, webDBUserPassword);
            debugLevel = Convert.ToInt32(ConfigurationManager.AppSettings["DebugLevel"]);
            if (debugLevel > 2)
            {
                myLog.Log(string.Format("DMZ DB Service started. Con.string: {0}", webConnectionsString), LogLevel.Info);
            }
        }

        //public bool AnyChanges()
        //{
        //    bool l_Reply = false;
        //    try
        //    {
        //        DateTime startTime = DateTime.Now;

        //        //int noOfUsers = myDB.Users.Where(u => u.Active && u.NemId != null).Count();
        //        int noOfUsers = myDB.Users.Count();
        //        l_Reply = noOfUsers > 0;
        //        if (l_Reply)
        //            myLog.Log(string.Format("DMZ: Received: {0} new users ", noOfUsers), LogLevel.Info);
        //        else
        //            myLog.Log("DMZ: No users in database.", LogLevel.Info);

        //        myLog.Log(string.Format("DMZ: It took {0} ms to get the data via Entity", (DateTime.Now - startTime).TotalMilliseconds), LogLevel.Info);

        //        startTime = DateTime.Now;

        //        //noOfUsers = myDB.Users.Count(u => u.Active && u.NemId != null);
        //        noOfUsers = myDB.Users.Count();
        //        l_Reply = noOfUsers > 0;
        //        if (l_Reply)
        //            myLog.Log(string.Format("DMZ: Received: {0} new users ", noOfUsers), LogLevel.Info);
        //        else
        //            myLog.Log("DMZ: No users in database.", LogLevel.Info);

        //        myLog.Log(string.Format("DMZ Count: It took {0} ms to get the data via Entity", (DateTime.Now - startTime).TotalMilliseconds), LogLevel.Info);


        //        startTime = DateTime.Now;
        //        using (SqlConnection myDB = SQLTools.ConnectToDb(webConnectionsString))
        //        {
        //            noOfUsers = 0;
        //            if (myDB.State == System.Data.ConnectionState.Open)
        //            {
        //                using (SqlCommand cmd = new SqlCommand(webSqlGetChanges, myDB))
        //                {
        //                    using (SqlDataReader reader = cmd.ExecuteReader())
        //                    {
        //                        while (reader.Read())
        //                        {
        //                            if (debugLevel > 2)
        //                            {
        //                                noOfUsers = reader.GetInt32(0);
        //                                myLog.Log(string.Format("DMZ: Received: {0} new users ", noOfUsers), LogLevel.Info);
        //                            }
        //                        }
        //                        l_Reply = noOfUsers > 0;
        //                        if (!l_Reply)
        //                            myLog.Log("DMZ: No users in database.", LogLevel.Info);
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                myLog.Log("DMZ database did not open.", LogLevel.Info);
        //            }
        //            myDB.Close();
        //            myLog.Log(string.Format("DMZ: It took {0} ms to get the data via SQL", (DateTime.Now - startTime).TotalMilliseconds), LogLevel.Info);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //    return l_Reply;
        //}

        public List<Users> GetChanges()
        {
            List<Users> newUsers = new List<Users>();
            try
            {
                newUsers = myDB.Users.Include("UserDisease").ToList();
            }
            catch (Exception ex)
            {
                myLog.Log("Error when getting changes from DMZ-DB: " + ex.ToString(), LogLevel.Error);
            }
            return newUsers;
        }

        public bool DeletetransferedUsersRecords(List<Users> myUsers)
        {
            bool l_Reply = false;
            try
            {
                foreach (Users users in myUsers)
                {
                    Users removeMe = myDB.Users.Where(x => x.Id == users.Id).First();
                    if (debugLevel > 2)
                        myLog.Log("Removing user from DMZ DB: " + removeMe.Email, LogLevel.Debug);

                    List<UserDisease> userDiseases = myDB.UserDisease.Where(p1 => p1.UserId == removeMe.Id).ToList();
                    foreach (UserDisease u in userDiseases)
                    {
                        if (debugLevel > 2)
                            myLog.Log("Removing user disease from DMZ DB: " + u.Disease, LogLevel.Debug);
                        myDB.UserDisease.Remove(u);
                    }
                    myDB.Users.Remove(removeMe);
                    myDB.SaveChanges();
                }
                l_Reply = true;
            }
            catch (Exception ex)
            {
                myLog.Log("Error when deleting users from WebDB: " + ex.ToString(), LogLevel.Error);
                l_Reply = false;
            }
            return l_Reply;
        }
    }
}

