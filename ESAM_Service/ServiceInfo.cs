﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ESAMService
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ServiceInfo
    {
        ServiceInfo _me;

        public ServiceInfo()
        {
            _ProductVersion = GetVersion();
            _IP = GetLocalIpAddress();
        }

        public ServiceInfo(ServiceInfoSettings value)
        {
            try
            {
                _me = new ServiceInfo();
                _me.Name = value.ServiceInfoFeatures.Name;
                //_me.IP = GetLocalIpAddress();
                _me.ServiceDisplayName = value.ServiceInfoFeatures.ServiceDisplayName;
                _me.Port = value.ServiceInfoFeatures.Port;
                _me.UriAddress = value.ServiceInfoFeatures.UriAddress;
            }
            catch (Exception ex)
            {
                string err = ex.ToString();
            }
        }


        public string GetLocalIpAddress()
        {
            UnicastIPAddressInformation mostSuitableIp = null;

            var networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();

            foreach (var network in networkInterfaces)
            {
                if (network.OperationalStatus != OperationalStatus.Up)
                    continue;

                var properties = network.GetIPProperties();

                if (properties.GatewayAddresses.Count == 0)
                    continue;

                foreach (var address in properties.UnicastAddresses)
                {
                    if (address.Address.AddressFamily != AddressFamily.InterNetwork)
                        continue;

                    if (IPAddress.IsLoopback(address.Address))
                        continue;

                    if (!address.IsDnsEligible)
                    {
                        if (mostSuitableIp == null)
                            mostSuitableIp = address;
                        continue;
                    }

                    // The best IP is the IP got from DHCP server
                    if (address.PrefixOrigin != PrefixOrigin.Dhcp)
                    {
                        if (mostSuitableIp == null || !mostSuitableIp.IsDnsEligible)
                            mostSuitableIp = address;
                        continue;
                    }

                    return address.Address.ToString();
                }
            }

            return mostSuitableIp != null
                ? mostSuitableIp.Address.ToString()
                : "";
        }

        string _Name = string.Empty;
        [JsonProperty]
        public string Name { get { return _Name; } set { _Name = value; } }

        private string _ServiceDisplayName = string.Empty;
        [JsonProperty]
        public string ServiceDisplayName { get { return _ServiceDisplayName; } set { _ServiceDisplayName = value; } }

        string _IP = string.Empty;
        [JsonProperty]
        public string IP { get { return _IP; } set { _IP = value; } }

        int _Port = 8080;
        [JsonProperty]
        public int Port { get { return _Port; } set { _Port = value; } }

        Uri _UriAddress;
        [JsonProperty]
        public Uri UriAddress { get { return _UriAddress; } set { _UriAddress = value; } }

        public static explicit operator ServiceInfo(ServiceInfoSettings v)
        {
            ServiceInfo s = new ServiceInfo(v);
            return s._me;
        }

        private string _Version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
        [JsonProperty]
        public string Version { get { return _Version; } set { _Version = value; } }

        private string _FileVersion = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion;
        [JsonProperty]
        public string FileVersion { get { return _FileVersion; } set { _FileVersion = value; } }

        private string _ProductVersion = string.Empty;

        private string GetVersion()
        {
            FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);
            string l_Reply = string.Format("Version: {0}.{1}.{2}.{3}",
            myFileVersionInfo.FileMajorPart,
            myFileVersionInfo.FileMinorPart,
            myFileVersionInfo.FileBuildPart,
            myFileVersionInfo.FilePrivatePart);

            return l_Reply;
        }
        [JsonProperty]
        public string ProductVersion
        {
            get
            {
                if (_ProductVersion == string.Empty)
                    _ProductVersion = GetVersion();
                return _ProductVersion;
            }
            set { _ProductVersion = value; }
        }

        public override string ToString()
        {
            return string.Format("Name: {0}, IP: {1}, Port: {2}, UriAddress: {3}, Version: {4}, FileVersion: {5}, ProductVersion: {6}", Name, IP, Port, UriAddress, Version, FileVersion, ProductVersion);
        }
    }

    public class ServiceInfoFeatures : ConfigurationElement
    {
        [ConfigurationProperty("Name", DefaultValue = "", IsRequired = true)]
        public string Name
        {
            get { return (string)this["Name"]; }
            set { this["Name"] = value; }
        }


        [ConfigurationProperty("ServiceDisplayName", DefaultValue = "", IsRequired = true)]
        public string ServiceDisplayName
        {
            get { return (string)this["ServiceDisplayName"]; }
            set { this["ServiceDisplayName"] = value; }
        }

        //[ConfigurationProperty("IP", DefaultValue = "", IsRequired = true)]
        //public string IP
        //{
        //    get { return (string)this["IP"]; }
        //    set { this["IP"] = value; }
        //}

        [ConfigurationProperty("Port", DefaultValue = "8081", IsRequired = true)]
        public int Port
        {
            get { return (int)this["Port"]; }
            set { this["Port"] = value; }
        }

        [ConfigurationProperty("UriAddress", DefaultValue = "", IsRequired = false)]
        public Uri UriAddress
        {
            get { return (Uri)this["UriAddress"]; }
            set { this["UriAddress"] = value; }
        }

    }
}
