﻿using DNBTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESAMService.Helper{
    public class myReceiptsHelper
    {
        SysLog myLog;
        LAN.lan_db_Entities1 myLAN;
        public myReceiptsHelper(SysLog Log)
        {
            myLog = Log;
            myLAN = new LAN.lan_db_Entities1();
        }

        /// <summary>
        /// Adding new receipts for userID
        /// </summary>
        /// <param name="userReceipts"></param>
        /// <param name="userID"></param>
        public bool AddReceiptsForUser(SBAllReceiptsResponse userReceipts, Guid? userID)
        {
            bool l_Reply = false;
            bool addedReceipts = false;
            try
            {
                myLog.Log(string.Format("Checking for new receipts for UserID: {0}. Found {1} in StoreBox", 
                    userID, userReceipts.receipts.Count()), LogLevel.Debug);
                if (userID == null || userID == (new Guid()))
                {
                    myLog.Log("No UserID when adding receipts...", LogLevel.Error);
                }
                else
                {
                    LAN.Receipts myReceipt;

                    foreach (Receipt i in userReceipts.receipts)
                    {
                        if (myLAN.Receipts.Count() > 0)
                        {
                            myReceipt = myLAN.Receipts.Where(r => r.ReceiptID == i.ReceiptID).FirstOrDefault();
                        }
                        else
                            myReceipt = new LAN.Receipts();

                        if (myReceipt == null || (myReceipt.StoreName == null && myReceipt.GrandTotal == 0))
                        {
                            myReceipt = new LAN.Receipts()
                            {
                                UserID = (Guid)userID,
                                ReceiptID = i.ReceiptID,
                                CopiedFromUser = i.copiedFromUser,
                                GrandTotal = i.grandTotal,
                                PurchaseDate = i.purchaseDate,
                                StoreName = i.storeName
                            };
                            myLAN.Receipts.Add(myReceipt);
                            myLAN.SaveChanges();
                            myLog.Log(string.Format("Added receipt from {0} for UserID {1}", myReceipt.StoreName, userID), LogLevel.Debug);
                            addedReceipts = true;
                        }
                    }
                    if (!addedReceipts)
                    {
                        myLog.Log("No new receipts for UserID: " + userID, LogLevel.Info);
                    }
                    l_Reply = true;
                }
            }
            catch (Exception ex)
            {
               
                myLog.Log(string.Format("Error when adding receipts: {0} Error: {1} ", userID, ex.ToString()), LogLevel.Error);

                l_Reply = false;
            }
            return l_Reply;
        }

        public void AddReceiptDetailsForUser(List<Tuple<string,string>> userReceiptDetails, UserData user)
        {
            try
            {
                myLog.Log(string.Format("Checking for new receiptdetails for UserID: {0}. Found {1} in StoreBox",
                   user.UserID, userReceiptDetails.Count()), LogLevel.Debug);
                foreach (Tuple<string,string> s in userReceiptDetails)
                {
                    LAN.ReceiptDetails rd = new LAN.ReceiptDetails()
                    {
                        ReceiptDetails1 = s.Item1,
                        ReceiptID=s.Item2
                    };

                    myLAN.ReceiptDetails.Add(rd);
                    myLAN.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                myLog.Log(string.Format("Error when adding receiptdetails: {0} Error: {1} ", user.UserID, ex.ToString()), LogLevel.Error);
            }
        }

        public void AddReceiptDetailsForUser(List<ReceiptDetails> userReceiptDetails, UserData user)
        {
            try
            {
                myLog.Log(string.Format("Checking for new receiptdetails for UserID: {0}. Found {1} in StoreBox",
                   user.UserID, userReceiptDetails.Count()), LogLevel.Debug);

            }
            catch (Exception ex)
            {
                myLog.Log(string.Format("Error when adding receiptdetails: {0} Error: {1} ", user.UserID, ex.ToString()), LogLevel.Error);
            }
        }
    }
}
