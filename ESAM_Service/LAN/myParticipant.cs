﻿using DNBTools;
using SimpleImpersonation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESAMService.Helper
{
    public class myParticipantHelper
    {
        SysLog myLog;
        LAN.lan_db_Entities1 myLAN;

        public myParticipantHelper(SysLog Log)
        {
            myLog = Log;
            myLAN = new LAN.lan_db_Entities1();
        }

        public List<UserData> GetParticipantList()
        {
            List<UserData> myParticipants = new List<UserData>();
            try
            {
                bool useImpersonation = Convert.ToBoolean(ConfigurationManager.AppSettings["DoImpersonate"]);

                //if (Convert.ToBoolean(ConfigurationManager.AppSettings["IsADUser"]))
                if (useImpersonation)
                {
                    string destinationDBUserName = ConfigurationManager.AppSettings["DestinationDBUserName"];
                    string destinationDBUserPassword = ConfigurationManager.AppSettings["DestinationDBUserPassword"];
                    string destinationDomain = ConfigurationManager.AppSettings["DestinationDomain"];
                    var credentials = new UserCredentials(destinationDomain, destinationDBUserName, destinationDBUserPassword);
                    Impersonation.RunAsUser(credentials, LogonType.Interactive, () =>
                    {
                        try
                        {
                            List<LAN.Participant> newP = myLAN.Participant.ToList();
                            myLog.Log(string.Format("LAN: {0} participants in database.", newP.Count()), LogLevel.Info);
                            foreach (LAN.Participant p in newP)
                            {
                                myParticipants.Add(ConvertParticipantToUserData(p));
                            }
                        }
                        catch (Exception ex)
                        {
                            myLog.Log("Service: Error when getting participant data from LAN DB: " + ex.ToString(), LogLevel.Error);
                        }
                    });
                }
                else
                {
                    try
                    {
                        List<LAN.Participant> newP = myLAN.Participant.ToList();
                        myLog.Log(string.Format("LAN: {0} participants in database.", newP.Count()), LogLevel.Info);
                        foreach (LAN.Participant p in newP)
                        {
                            myParticipants.Add(ConvertParticipantToUserData(p));
                        }
                    }
                    catch (Exception ex)
                    {
                        myLog.Log("Service: Error when getting participant data from LAN DB: " + ex.ToString(), LogLevel.Error);
                    }
                }
            }
            catch (Exception x)
            {
                myLog.Log("Service: Error when impersonating for getting participant data from LAN DB: " + x.Message, LogLevel.Error);
            }

            return myParticipants;
        }

        public Guid? GetParticipantID(UserData user)
        {
            LAN.Participant participant = myLAN.Participant.Where(p => p.NemId == user.NemID).First();
            if (participant != null)
                return participant.ID;
            else
                return null;
        }

        public void UpdateParticipantsFromDMZ(DMZ.Users user)
        {
            try
            {
                Guid participantID;
                if (!Tools.IsNullOrEmpty(user.NemId))
                {
                    if (myLAN.Participant.Where(x => x.NemId == user.NemId).Count() > 0)
                    {
                        participantID = UpdateParticipant(user);
                    }
                    else
                    {
                        LAN.Participant newParticipant = new LAN.Participant();
                        newParticipant = user;
                        newParticipant.ID = participantID = Guid.NewGuid();
                        myLAN.Participant.Add(newParticipant);
                        myLAN.SaveChanges();
                    }
                    if (participantID != Guid.Empty)
                    {

                        // Check if user has entered diseases.
                        if (user.UserDisease.Count > 0)
                        {
                            myDisease myD = new myDisease(myLog);
                            myD.CheckUserForDiseases(user, participantID);
                        }
                    }
                    else
                    {
                        myLog.Log(string.Format("Could not find GUID for participant. NemID: {0}", user.NemId), LogLevel.Debug);
                    }
                }
                else
                {
                    if (!Tools.IsNullOrEmpty(user.StoreboxEmail))
                    {
                        if (myLAN.Participant.Where(x => x.StoreboxEmail == user.StoreboxEmail).Count() > 0)
                        {
                            UpdateParticipantFromEmailWithValidationCode(user);
                        }
                        else {
                            myLog.Log("No Storebox email...", LogLevel.Debug);
                        }
                    }
                    else
                        myLog.Log("No NemID", LogLevel.Debug);
                }
            }
            catch (Exception ex)
            {
                myLog.Log(string.Format("Error when Updating/creating participant. Error: {0}", ex.ToString()), LogLevel.Error);
            }
        }

        /// <summary>
        /// Updates user via StoreBox Email
        /// Is only used when user enters Email PIN 
        /// directly from link without registration first.
        /// </summary>
        /// <param name="user"></param>
        public void UpdateParticipantFromEmailWithValidationCode(DMZ.Users user)
        {
            try
            {
                LAN.Participant updateUser = myLAN.Participant.Where(x => x.StoreboxEmail == user.StoreboxEmail).SingleOrDefault();
                if (updateUser != null)
                {
                    if (!Tools.IsNullOrEmpty(user.ValidationCode))
                    {
                        updateUser.ValidationCode = user.ValidationCode;
                        myLAN.SaveChanges();
                    }
                }
                else
                    myLog.Log(string.Format("User {0} not found??", user.StoreboxEmail), LogLevel.Error);
            }
            catch (Exception ex)
            {
                myLog.Log(string.Format("Error when Updating participant with validation code. Error: {0}", ex.ToString()), LogLevel.Error);
            }
        }

        public Guid UpdateParticipant(DMZ.Users user)
        {
            Guid participantID = new Guid();
            try
            {
                LAN.Participant updateUser = myLAN.Participant.Where(x => x.NemId == user.NemId).SingleOrDefault();
                if (!Tools.IsNullOrEmpty(user.AcceptUseData)) updateUser.AcceptUseData = user.AcceptUseData;
                if (!Tools.IsNullOrEmpty(user.Active)) updateUser.Active = user.Active;
                if (!Tools.IsNullOrEmpty(user.AuthenticationInfo)) updateUser.AuthenticationInfo = user.AuthenticationInfo;
                if (!Tools.IsNullOrEmpty(user.ContactMe)) updateUser.ContactMe = user.ContactMe;
                if (!Tools.IsNullOrEmpty(user.Email)) updateUser.Email = user.Email;
                if (!Tools.IsNullOrEmpty(user.ParticipateInProject)) updateUser.ParticipateInProject = user.ParticipateInProject;
                if (!Tools.IsNullOrEmpty(user.RegistrationDate)) updateUser.RegistrationDate = user.RegistrationDate;
                if (!Tools.IsNullOrEmpty(user.SendMeNewsLetter)) updateUser.SendMeNewsletter = user.SendMeNewsLetter;
                if (!Tools.IsNullOrEmpty(user.StoreboxEmail)) updateUser.StoreboxEmail = user.StoreboxEmail;
                if (!Tools.IsNullOrEmpty(user.StoreboxPhone)) updateUser.StoreboxPhone = user.StoreboxPhone;
                if (!Tools.IsNullOrEmpty(user.ValidationCode)) updateUser.ValidationCode = user.ValidationCode;
                if (!Tools.IsNullOrEmpty(user.StoreboxConsent)) updateUser.StoreboxConsent = user.StoreboxConsent;
                if (!Tools.IsNullOrEmpty(user.ProjectConsent)) updateUser.ProjectConsent = user.ProjectConsent;
                participantID = updateUser.ID;
                myLAN.SaveChanges();
            }
            catch (Exception ex)
            {
                myLog.Log(string.Format("Error when Updating participant. Error: {0}", ex.ToString()), LogLevel.Error);
            }
            return participantID;
        }

        public bool UpdateParticipantSBInfo(UserData myUser)
        {
            bool l_Reply = false;
            try
            {
                LAN.Participant myP = myLAN.Participant.Where(p => p.NemId == myUser.NemID).First();
                if (myP != null)
                {
                    if (!Tools.IsNullOrEmpty(myUser.EmailPIN))
                        myP.ValidationCode = myUser.EmailPIN;
                    if (!Tools.IsNullOrEmpty(myUser.EmailVerificationID))
                        myP.EmailVerificationID = myUser.EmailVerificationID;
                    if (!Tools.IsNullOrEmpty(myUser.StoreBoxID))
                        myP.StoreboxID = myUser.StoreBoxID;
                    myLAN.SaveChanges();
                    l_Reply = true;
                }
                else
                    myLog.Log("Couldn't find user: " + myUser.UserID, LogLevel.Error);
            }
            catch (Exception ex)
            {
                myLog.Log(string.Format("Error when searching for participant: {0} Error: {1} ", myUser.UserID, ex.ToString()), LogLevel.Error);
            }
            return l_Reply;
        }
        /// <summary>
        /// UserData is mainly for communicating with StoreBox
        /// Therefore the field Email is converted from StoreBoxEmail
        /// </summary>
        /// <param name="myParticipant"></param>
        /// <returns></returns>
        public UserData ConvertParticipantToUserData(LAN.Participant myParticipant)
        {
            UserData myUser = new UserData()
            {
                Active = (bool)myParticipant.Active,
                Email = myParticipant.StoreboxEmail, // Is mainly used to communicate with storebox.
                EmailPIN = myParticipant.ValidationCode,
                EmailVerificationID = myParticipant.EmailVerificationID,
                MobileNo = myParticipant.StoreboxPhone,
                NemID = myParticipant.NemId,
                StoreBoxID = myParticipant.StoreboxID,
                UserID = myParticipant.ID.ToString()
            };

            return myUser;
        }
    }
    //LAN.Users lanUser = new LAN.Users()
    //{
    //    AcceptUseData = users.AcceptUseData,
    //    Active = users.Active,
    //    AuthenticationInfo = users.AuthenticationInfo,
    //    ContactMe = users.ContactMe,
    //    Email = users.Email,
    //    NemId = users.NemId,
    //    ParticipateInProject = users.ParticipateInProject,
    //    SendMeNewsLetter = users.SendMeNewsLetter,
    //    Phone = users.Phone,
    //    RegistrationDate = users.RegistrationDate,
    //    StoreboxEmail = users.StoreboxEmail,
    //    StoreboxPhone = users.StoreboxPhone,
    //    ValidationCode = users.ValidationCode
    //};
}
namespace ESAMService.LAN
{
    public partial class Participant
    {
        public static implicit operator Participant(DMZ.Users v)
        {
            return new Participant
            {
                AcceptUseData = v.AcceptUseData,
                Active = v.Active,
                AuthenticationInfo = v.AuthenticationInfo,
                ContactMe = v.ContactMe,
                //CPR
                Email = v.Email,
                //EmailVerificationID Will be collected during storebox update from LAN
                NemId = v.NemId,
                ParticipateInProject = v.ParticipateInProject,
                RegistrationDate = v.RegistrationDate,
                SendMeNewsletter = v.SendMeNewsLetter,
                StoreboxEmail = v.StoreboxEmail,
                //StoreboxID Will be collected during storebox update from LAN
                StoreboxPhone = v.StoreboxPhone,
                ValidationCode = v.ValidationCode,
                StoreboxConsent = v.StoreboxConsent,
                ProjectConsent = v.ProjectConsent
            };
        }
    }
}
