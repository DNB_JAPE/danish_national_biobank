﻿using DNBTools;
using ESAMService.DMZ;
using ESAMService;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESAMService.Helper
{
    public class myDisease
    {
        SysLog myLog;
        LAN.lan_db_Entities1 myLAN;

        public myDisease(SysLog Log)
        {
            myLog = Log;
            myLAN = new LAN.lan_db_Entities1();
        }
        public void CheckUserForDiseases(Users user, Guid participantID)
        {
            foreach (DMZ.UserDisease userDisease in user.UserDisease)
            {
                LAN.UserDisease userDiseaseLAN = myLAN.UserDisease
                    .Where(x => x.UserDiseaseID == userDisease.DiseaseId && x.ParticipantID == participantID)
                    .SingleOrDefault();
                if (userDiseaseLAN == null)
                {
                    string myTitle = string.Empty;
                    string myLang = string.Empty;
                    using (DMZ.ESAM_EXT_TESTEntities myDMZDB = new ESAM_EXT_TESTEntities())
                    {
                        var title = myDMZDB.DiseaseTranslation
                            .Where(d => d.DiseaseId == userDisease.DiseaseId)
                            .First().Title;
                        var languag = myDMZDB.DiseaseTranslation
                            .Where(d => d.DiseaseId == userDisease.DiseaseId)
                            .First().Language.Name;


                        myTitle = title.ToString();// dis.Name;
                        myLang = languag.ToString();// dis.Language;
                    };
                    LAN.Disease lanDisease = myLAN.Disease.Where(x => x.Name == myTitle).SingleOrDefault();

                    if (lanDisease == null) CreateDisease(userDisease, myTitle, myLang);

                    userDiseaseLAN = new LAN.UserDisease();
                    userDiseaseLAN.UserDiseaseID = userDisease.DiseaseId;
                    userDiseaseLAN.ParticipantID = participantID;
                    myLAN.UserDisease.Add(userDiseaseLAN);
                    myLAN.SaveChanges();
                }
            }
        }
      
        public void CreateDisease(UserDisease userDisease, string Title, string Language)
        {
            LAN.Disease lanDisease = new LAN.Disease();
            // Create it
            lanDisease.DiseaseID = userDisease.DiseaseId;

            lanDisease.Name = Title;
            lanDisease.Language = Language;

            myLAN.Disease.Add(lanDisease);
            myLAN.SaveChanges();
        }

        //public void UpdateUserDisease(UserDisease disease)
        //{

        //}
        //public bool doesDiseaseExist(int id, SqlConnection myDB)
        //{
        //    try
        //    {
        //        if (myDB.State == System.Data.ConnectionState.Open)
        //        {
        //            string search = string.Format("SELECT ID " +
        //               "FROM ESAM_PD.Disease WHERE DiseaseID = {0}", id);
        //            using (SqlCommand cmd = new SqlCommand(search, myDB))
        //            {
        //                using (SqlDataReader reader = cmd.ExecuteReader())
        //                {
        //                    while (reader.Read())
        //                    {
        //                        return true;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        myLog.Log("Service: Error when searching for disease: " + ex.ToString(), LogLevel.Error);
        //        throw;
        //    }
        //    return false;
        //}

    }
    public class pInfo
    {
        public string Name { get; set; }
        public string Language { get; set; }
    }
}
