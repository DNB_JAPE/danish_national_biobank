﻿using DNBTools;
using ESAMService.DMZ;
using ESAMService.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ESAMService
{
    public class MyWebManager:IDisposable
    {
        Thread mainThread;
        Thread storeBoxThread;

        bool isActive = true;
        
        MyDatabase myDatabase;
        MyWebDatabase myWebDatabase;
        MyStoreBox myStoreBox;

        SysLog myLog;
        string status;

        public event EventHandler StatusChangedEvent;

        private string ApiKey = string.Empty;
        int debugLevel = 3;

        public MyWebManager(SysLog mySyslog)
        {
            ReadSetup();
            myLog = mySyslog;
            mySyslog.Log("ESAM Manager started", LogLevel.Info);

            myDatabase = new MyDatabase(myLog);
            myStoreBox = new MyStoreBox(myLog);
            myStoreBox.isTestMode = false;
            // API Key
            //SocaYiWXZo33D6pF4uWMtYtbp1CWSZ2z
            //
            myStoreBox.ApiKey = ApiKey;         
            myStoreBox.InitStoreBoxClient();

            myWebDatabase = new MyWebDatabase(myLog);
           
            mainThread = new Thread(() => SurvejlanceThread());
            mainThread.IsBackground = true;
            mainThread.Start();

            storeBoxThread = new Thread(() => StoreBoxThread());
            storeBoxThread.IsBackground = true;
            storeBoxThread.Start();
        }

        public SysLog MySysLog {
            get { return myLog; }
            set { myLog = value; }
        }

        public string Status
        {
            get { return status; }
            private set
            {
                status = value;
            }
        }

        void RaiseStatusChangedEvent(EventArgs e)
        {
            EventHandler handler = StatusChangedEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        
        #region Public Functions

        public void Stop()
        {
            isActive = false;
            myLog.Log("WebManager ending", LogLevel.Info);
        }

        #endregion Public Functions
        
        /// <summary>
        /// Handling StoreBox data
        /// </summary>
        private void StoreBoxThread()
        {
            myLog.Log("Service: StoreBox thread started", LogLevel.Info);
            while (isActive)
            {
                try
                {
                    // Waiting twice as long as the DMZ thread
                    Thread.Sleep(serviceWaitTime * 2000);   
                    
                    // Waiting for Survejlance thread to
                    // finish transfering users and making 
                    // preliminary checks
                    while (isChecking)
                    {
                        Thread.Sleep(300);
                    }
                    UpdateStoreboxData();
                    
                }
                catch (Exception ex)
                {
                    myLog.Log("Service: Error in StoreBoxThread: " + ex.ToString(), LogLevel.Error);
                }
            }
        }

        /// <summary>
        /// Checking DMZ Database for new entries.
        /// Entries will be deleted from DMZ after transfer.
        /// </summary>
        bool isChecking = false;

        /// <summary>
        /// Monitoring DMZ database 
        /// </summary>
        private void SurvejlanceThread()
        {
            myLog.Log("Service: WebManager thread started", LogLevel.Info);

            while (isActive)
            {
                try
                {
                    Thread.Sleep(serviceWaitTime * 1000);
                    // Check Web if any new data has arrived
                    // TODO Monitor var
                    isChecking = true;
                    bool newUsersArrived = CheckWebSiteForChanges();
                    isChecking = false;
                }
                catch (Exception ex)
                {
                    myLog.Log("Service: Error in WebManager: " + ex.ToString(), LogLevel.Error);
                }
            }
            myLog.Log("Service: WebManager thread ended", LogLevel.Info);
        }
       
        DateTime LastFailureTime;

        /// <summary>
        /// Check for arrival of new users in DMZ.
        /// When new user(s) arrive, it copies them to the LAN database
        /// and deletes the DMZ user(s) after transfer
        /// </summary>
        /// <returns></returns>
        bool CheckWebSiteForChanges()
        {
            bool l_Reply = false;
            try
            {
                if (((DateTime.Now - LastFailureTime).TotalSeconds) > 10)
                {
                    List<Users> newUsers = myWebDatabase.GetChanges();
                    if (newUsers != null && newUsers.Count > 0)
                    {
                        if (debugLevel > 2)
                            myLog.Log(string.Format("Service: WebManager found {0} records", newUsers.Count), LogLevel.Debug);

                        if (myDatabase.AddUserData(newUsers))
                        {
                            // Delete user records in DMZ
                            myWebDatabase.DeletetransferedUsersRecords(newUsers);

                        }
                        else
                        {
                            myLog.Log("Service: WebManager couldn't add userdata nor check Storebox info.", LogLevel.Critical);
                        }
                    }
                    else
                    {
                        if (debugLevel > 2)
                            myLog.Log("Service: No new records", LogLevel.Debug);
                    }
                }
            }
            catch (Exception ex)
            {
                myLog.Log("Service: Error when checking data from Web: " + ex.ToString(), LogLevel.Error);
                l_Reply = false;
                LastFailureTime = DateTime.Now;
            }
            return l_Reply;
        }

        int serviceWaitTime = 2;

        /// <summary>
        /// Reading setup data for Wait Time 
        /// and API key for StoreBox
        /// </summary>
        void ReadSetup()
        {
            serviceWaitTime = Convert.ToInt32(ConfigurationManager.AppSettings["WaitTimeSeconds"]);
            ApiKey = ConfigurationManager.AppSettings["ApiKey"];
            debugLevel = Convert.ToInt32(ConfigurationManager.AppSettings["DebugLevel"]);
        }

        #region StoreBox

        /// <summary>
        /// Getting list of all users and handle
        /// their StoreBox data if needed
        /// </summary>
        private void UpdateStoreboxData()
        {
            List<UserData> newUsers = new List<UserData>();
            try
            {
                myLog.Log("Service SB: Check StoreBox Info", LogLevel.Info);
                myParticipantHelper myParticipants = new myParticipantHelper(myLog);
                newUsers = myParticipants.GetParticipantList();
                //newUsers = myDatabase.GetUserList();
                if (newUsers != null && newUsers.Count > 0)
                {
                    foreach (UserData user in newUsers)
                    {
                        if (Tools.IsNullOrEmpty(user.MobileNo))
                        {
                            myLog.Log("Service SB: Can't collect data without mobile no. for UserID: " + user.UserID, LogLevel.Error);
                        }
                        else
                        {
                            HandleStoreboxUser(user);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                myLog.Log("Service SB: Error in CheckForNewUsers: " + ex.ToString(), LogLevel.Error);
            }

        }

        /// <summary>
        /// Handle specific user's StoreBox data
        /// Registrating and getting StoreBox ID
        /// </summary>
        /// <param name="user"></param>
        private void HandleStoreboxUser(UserData user)
        {
            if (!Tools.IsNullOrEmpty(user.Email) && !Tools.IsNullOrEmpty(user.NemID))
            {
                if (user.Active == false)
                {
                    HandleWithdrawalOfConsent(user);
                }
                else
                {
                    if (Tools.IsNullOrEmpty(user.StoreBoxID))
                    {
                        if (Tools.IsNullOrEmpty(user.EmailVerificationID) && Tools.IsNullOrEmpty(user.EmailPIN ))
                        {
                            HandleNewUserWithoutPIN(user);
                        }
                        else
                        {
                            if (!Tools.IsNullOrEmpty( user.EmailPIN))
                            {
                                if (Tools.IsNullOrEmpty(user.EmailVerificationID))
                                {
                                    HandleNewUserWithPIN(user);
                                }
                                else
                                {
                                    HandleNewUserWithPINAndEmailVerificationID(user);
                                }
                            }
                            else
                            {
                                // Waiting for PIN
                                myLog.Log(string.Format("Waiting for PIN UserID {0}", user.UserID), LogLevel.Info);
                            }
                        }
                    }
                    else
                    {
                        // Get all Recipts for user
                        // with correct info
                        myLog.Log("Collecting receipts for user: " + user.Email, LogLevel.Info);
                        SBAllReceiptsResponse userReceipts = myStoreBox.GetAllReceiptsForUser(user);
                        if (userReceipts != null && userReceipts.receipts.Count() > 0)
                        {
                            myLog.Log(string.Format("User {0} har {1} receipts in Storebox", user.Email, userReceipts.receipts.Count()), LogLevel.Info);
                            if (myDatabase.AddReceipts(userReceipts, user))
                            {
                                List<Tuple<string, string>> receiptDetails = myStoreBox.GetReceiptDetailsInJSON(user, userReceipts.receipts);
                                myDatabase.AddReceiptDetails(receiptDetails, user);
                            }
                            else
                            {
                                myLog.Log("Problems when trying to receive receipts. Check log.", LogLevel.Warning);
                            }
                        }
                    }
                }
            }
            else
            {
                myLog.Log("No email or nemid for UserID: " + user.UserID, LogLevel.Error);
            }
        }
        
        /// <summary>
        /// User entered Email PIN
        /// </summary>
        /// <param name="user"></param>
        private void HandleEmailPINArrived(UserData me)
        {
            // User entered Email PIN
            myLog.Log("Service: StoreBox User entered Email PIN", LogLevel.Info);
           // myDatabase.UpdateUserSB(me);

            if (me != null)
            {
                me.StoreBoxID = myStoreBox.VerifyUser(me);
                if (me.StoreBoxID != string.Empty)
                {
                    myLog.Log("Service: StoreBox User Got StoreBoxID: " + me.StoreBoxID, LogLevel.Info);
                    myDatabase.UpdateUserSB(me);
                }
                else
                {
                    myLog.Log(string.Format("Service: StoreBox User [{0}] didn't get StoreBoxID", me.UserID), LogLevel.Info);
                }
            }
        }

        /// <summary>
        /// User wants to withdraw consent
        /// </summary>
        /// <param name="user"></param>
        private void HandleWithdrawalOfConsent(UserData user)
        {
            // User wants to withdraw consent
            myLog.Log("Service: StoreBox User wants to withdraw consent", LogLevel.Info);
            if (myDatabase.DeactivateUser(user.NemID))
            {
                // TODO: Should we send email??
                myLog.Log(string.Format("Service: StoreBox User [{0}] withdrew consent", user.NemID), LogLevel.Info);
            }
            else
            {
                myLog.Log(string.Format("Service: StoreBox User [{0}] couldn't withdraw consent", user.NemID), LogLevel.Warning);
            }
        }

        /// <summary>
        /// User wants to withdraw consent
        /// </summary>
        /// <param name="user"></param>
        private void HandleWithdrawalOfConsent(Users user)
        {
            // User wants to withdraw consent
            myLog.Log("Service: StoreBox User wants to withdraw consent", LogLevel.Info);
            if (myDatabase.DeactivateUser(user.NemId))
            {
                // TODO: Should we send email and/or delete database data??
                myLog.Log(string.Format("Service: StoreBox User [{0}] withdrew consent", user.NemId), LogLevel.Info);
            }
            else
            {
                myLog.Log(string.Format("Service: StoreBox User [{0}] couldn't withdraw consent", user.NemId), LogLevel.Warning);
            }
        }

        private void  HandleNewUserWithoutPIN(UserData me)
        {
            // New user without Storebox PIN
            myLog.Log("Service: New user without Storebox PIN: " + me.Email, LogLevel.Info);
            me.EmailVerificationID = myStoreBox.LinkUser(me);
            myDatabase.UpdateUserSB(me);
        }

        /// <summary>
        /// New user with Storebox PIN
        /// </summary>
        /// <param name="me"></param>
        private void HandleNewUserWithPIN(UserData me)
        {
            // New user with Storebox PIN
            myLog.Log("Service: New user with Storebox Email and PIN: " + me.Email, LogLevel.Info);
            me.EmailVerificationID = myStoreBox.LinkUser(me);
            if (me.EmailVerificationID != null)
            {
                myLog.Log("Service: New user with Storebox Email: Got EmailVerificationID!" + me.Email, LogLevel.Info);
                myDatabase.UpdateUserSB(me);
            }
            else
            {
                myLog.Log("Service: New user with Storebox Email: Didn't get EmailVerificationID... See log for errors." + me.Email, LogLevel.Warning);
            }

        }

        private void HandleNewUserWithPINAndEmailVerificationID(UserData me)
        {
            // New user with PIN and EmailVerificationID
            myLog.Log("Service: New user with Storebox PIN and EmailVerificationID: " + me.Email, LogLevel.Info);
            me.StoreBoxID = myStoreBox.VerifyUser(me);
            if (me.StoreBoxID != null)
            {
                myLog.Log("Service: New user with Storebox PIN and EmailVerificationID: Got StoreBoxID! " + me.Email, LogLevel.Info);
                myDatabase.UpdateUserSB(me);
            }
            else
            {
                myLog.Log("Service: New user with Storebox PIN and EmailVerificationID: Didn't get StoreBoxID... " + me.Email, LogLevel.Warning);
            }
        }

        #endregion StoreBox

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                isActive = false;
                if (disposing)
                {
                    if (mainThread != null)
                    {
                        mainThread.Join();
                        mainThread.Abort();
                        mainThread = null;
                    }
                    if (storeBoxThread != null)
                    {
                        storeBoxThread.Join();
                        storeBoxThread.Abort();
                        storeBoxThread = null;
                    }
                }
                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~myWeb() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}


// EGO ID:  1cswez933zhapacevynbzn4khu32utyw
// "userId" : "1cswez933zhapacevynbzn4khu32utyw"

// DET VIRKER :-)
////UserData me = new UserData() {
////    Email = "esat@ssi.dk",
////    MobileNo = "4525618121",
////    EmailVerificationID = "u3ee8zsj19jrx5i6q7fxpdt6uzghndmy",
////    EmailPIN = "2420",
////    StoreBoxID = "1cswez933zhapacevynbzn4khu32utyw"
////};
////// Test JSON
////string testJson = me.JSON_Rep;
////// Returns {"UserID":"","NemID":"","Email":"esat@ssi.dk","MobileNo":"4525618121","StoreBoxID":"1cswez933zhapacevynbzn4khu32utyw","EmailVerificationID":"u3ee8zsj19jrx5i6q7fxpdt6uzghndmy","EmailPIN":"2420"}
////myStoreBox.CheckUser(me);

// TEST

