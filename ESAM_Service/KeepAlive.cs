﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ESAMService
{
    public class KeepAlive : IDisposable
    {
        bool isRunning = true;


        public KeepAlive()
        {

        }

        public KeepAlive(MQueue myQueue, ServiceInfo myInfo)
        {

            Thread myMainThread = new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                Console.WriteLine("Started KeepAlive thread");

                string mes = string.Empty;
                while (isRunning)
                {
                    mes = "I'm alive :-) " + DateTime.Now;
                    Console.WriteLine(mes);
                    myQueue.Send(JsonConvert.SerializeObject(myInfo));
                    Thread.Sleep(myQueue.DelayTimeInSeconds * 1000);
                }
            });

            myMainThread.Start();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    isRunning = false;
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~KeepAlive() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}
