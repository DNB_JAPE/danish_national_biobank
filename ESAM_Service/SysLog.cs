﻿//****************************************************
// SysLog
//
// This class connects to a Syslog server and sends UDP log-packages 
// to the server.
//
// Created:     2017-08-31 ESAT
// Modified:    2017-08-31
//
// Syslog needs 2 variables:
// SysLogIP     :   IP address for Syslog server
// SysLogName   :   The name for the logging system 
//                  that will apear in  the log string.
using ESAMService;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace DNBTools
{
    public enum LogLevel
    {
        Emergency = 0,
        Alert = 1,
        Critical = 2,
        Error = 3,
        Warning = 4,
        Notice = 5,
        Info = 6,
        Debug = 7,
        NoSource = 184
    } 

    public class SysLog
    {
        UdpClient m_syslogsock = null;
        string m_SyslogIP = "";
        string m_SourceId = "";
        bool m_Enable = false;
        int m_MaxLogQueue = 100;
        Queue m_LogQueue = null;
        Thread m_SenderThread = null;
        bool m_StopTheThread = false;
        bool m_TemporaryDisabled = false;
        bool m_ThreadAliveMessage = false;
        bool m_TimeStamp = false;
        bool m_onlyUseQueues = false;

        int m_DebugLevel = 4;

        string sStartSyslogMessage = "Starting Syslog thread";
        string sStartMessage = "DNB Syslog starting";

        public delegate void LogReceivedDelegate(object sender, LogReceivedEventArgs args);
        public event LogReceivedDelegate LogReceivedEvent;


        public SysLog()
        {
            ReadAndSetup();
        }

        private void ReadAndSetup()
        {
            m_SyslogIP = ConfigurationManager.AppSettings["SyslogIP"];
            m_SourceId = ConfigurationManager.AppSettings["SyslogName"];
            m_Enable = Convert.ToBoolean(ConfigurationManager.AppSettings["SyslogEnabled"]);
            m_DebugLevel = Convert.ToInt16(ConfigurationManager.AppSettings["DebugLevel"]);
            EnableLog(m_SyslogIP, m_SourceId, m_Enable, m_DebugLevel);
            m_onlyUseQueues= Convert.ToBoolean(ConfigurationManager.AppSettings["OnlyUseQueues"]);
        }

        public string SourceId
        {
            get { return m_SourceId; }
            set
            {
                m_SourceId = value.Trim();
                if (m_SourceId.Length != 0)
                    m_SourceId = m_SourceId + " ";
            }
        }

        public int DebugLevel
        {
            get { return m_DebugLevel; }
            set { m_DebugLevel = value; }
        }

        public bool TemporaryDisabled
        {
            set { m_TemporaryDisabled = value; }
        }

        public void EnableLog(string aSyslogIP, string aSourceID, bool enableLog, int debugLevel)
        {
            m_SyslogIP = aSyslogIP;
            m_SourceId = string.Format("[{0}] ", aSourceID);
            m_TimeStamp = true;
            m_MaxLogQueue = 100;
            try
            {
                m_Enable = enableLog;
                m_DebugLevel = debugLevel;
                m_syslogsock = new UdpClient(m_SyslogIP, 514);
            }
            catch (Exception)
            {
                m_syslogsock = null;
            }
            //MessageBox.Show(string.Format("IP: {0}, SyslogEnabled: {1} Name: {2} DebugLevel: {3}", m_SyslogIP, m_Enable, m_SourceId, m_DebugLevel));
            EnableLog(m_Enable);
            Log(sStartMessage, LogLevel.Info);
        }

        public void EnableLog(bool enable = true, bool threadAliveMessage = true, bool timeStamp = true, Int32 maxLogQueue = 100)
        {
            try
            {

                m_LogQueue = new Queue();
                m_SenderThread = new Thread(new ThreadStart(SenderThread));
                if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                    SetAsBackGround(m_SenderThread);
                m_SenderThread.Start();

                if (m_syslogsock == null)
                {
                    m_Enable = enable;
                    m_SourceId = Environment.GetEnvironmentVariable("SysLogName");
                    if (m_SourceId == null) m_SourceId = "DNBLog";
                    m_MaxLogQueue = maxLogQueue;
                    m_ThreadAliveMessage = threadAliveMessage;
                    m_TimeStamp = timeStamp;
                    //m_SyslogIP = sysLogIP;
                    m_SyslogIP = Environment.GetEnvironmentVariable("SysLogIP");
                    if (m_SyslogIP == null) m_SyslogIP = "127.0.0.1";
                    m_SourceId = m_SourceId.Trim();
                    if (m_SourceId.Length != 0) m_SourceId = m_SourceId + " ";

                    if (m_Enable)
                    {
                        if (m_SyslogIP != null)
                        {
                            m_syslogsock = new UdpClient(m_SyslogIP, 514);
                            Log(sStartMessage, LogLevel.Info);
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        public bool ThreadAliveMessage
        {
            get { return m_ThreadAliveMessage; }
            set { m_ThreadAliveMessage = value; }
        }

        public void CloseSyslog()
        {
            m_StopTheThread = true;
        }

        private void SetAsBackGround(Thread AThread)
        {
            AThread.IsBackground = true;
        }



        public void SenderThread()
        {
            byte[] data = null;
            int l_RunNotify = 0;
            Log( sStartSyslogMessage, LogLevel.Debug);
            while (!m_StopTheThread)
            {
                try
                {
                    Thread.Sleep(50);
                    if (m_TemporaryDisabled) continue;
                    // Check if previous data has been sent.
                    l_RunNotify++;
                    if (data == null)
                    {
                        try
                        {
                            Monitor.Enter(m_LogQueue);
                            if (m_LogQueue.Count > 0)
                            {
                                l_RunNotify = 0;
                                data = (byte[])m_LogQueue.Dequeue();
                            }
                        }
                        finally
                        {
                            Monitor.Exit(m_LogQueue);
                        }
                    }

                    if (m_ThreadAliveMessage && (l_RunNotify > 6000))
                    {
                        data = Encoding.GetEncoding(1252).GetBytes(String.Format("<{0}> {1} Syslog thread active.\n", LogLevel.NoSource | LogLevel.Warning, m_SourceId));
                        l_RunNotify = 0;
                    }
                    if (data != null)
                    {
                        if (m_syslogsock == null) m_syslogsock = new UdpClient(m_SyslogIP, 514);
                        m_syslogsock.Send(data, data.Length);
                        data = null;
                    }
                }
                catch (Exception)
                {
                    try
                    {
                        m_syslogsock.Close();
                    }
                    catch (Exception)
                    {
                    }
                    m_syslogsock = null;
                }

            }
        }

        public void Log(string Message, LogLevel logLevel)
        {
            if (Message == sStartSyslogMessage) return;

            if (m_onlyUseQueues)
            {
                Queues.myQueueManager.Log(Message, ConvertToDNBLogLevel(logLevel));
                return;
            }

            try
            {
                LogReceivedEvent?.Invoke(this, new LogReceivedEventArgs() { LogData = Message, Level = logLevel });

                int Lev = (int)logLevel;

                if (!m_Enable) EnableLog();
                if ((Lev & ~0x07) == 0) Lev |= (int)LogLevel.NoSource;          //If no facility/source, default to NoSource
                if (m_TimeStamp) Message = string.Format("{0} - {1}", DateTime.Now.ToString("ss.ffff"), Message);
                byte[] data = Encoding.GetEncoding(1252).GetBytes(String.Format("<{0}> {1}{2}\n", Lev, m_SourceId, Message));
                Monitor.Enter(m_LogQueue);
                try
                {
                    if (m_LogQueue.Count == (m_MaxLogQueue - 5))
                    {
                        data = Encoding.GetEncoding(1252).GetBytes(String.Format("<{0}> {1}Log queue is allmost full...\n", Lev, m_SourceId));
                        m_LogQueue.Enqueue(data);
                    }
                    else if (m_LogQueue.Count < m_MaxLogQueue)
                    {
                        m_LogQueue.Enqueue(data);
                    }
                }
                finally
                {
                    Monitor.Exit(m_LogQueue);
                }
            }
            catch (Exception y)
            {
                string t = y.Message;
            }
        }

        public LogData.LogLevel ConvertToDNBLogLevel(LogLevel oldLevel)
        {
            LogData.LogLevel newLevel;

            switch (oldLevel)
            {
                case LogLevel.Emergency:
                    newLevel = LogData.LogLevel.Critical;
                    break;
                case LogLevel.Alert:
                    newLevel = LogData.LogLevel.Error;
                    break;
                case LogLevel.Critical:
                    newLevel = LogData.LogLevel.Critical;
                    break;
                case LogLevel.Error:
                    newLevel = LogData.LogLevel.Error;
                    break;
                case LogLevel.Warning:
                    newLevel = LogData.LogLevel.Info;
                    break;
                case LogLevel.Notice:
                    newLevel = LogData.LogLevel.Info;
                    break;
                case LogLevel.Info:
                    newLevel = LogData.LogLevel.Info;
                    break;
                case LogLevel.Debug:
                    newLevel = LogData.LogLevel.Debug;
                    break;
                case LogLevel.NoSource:
                    newLevel = LogData.LogLevel.Error;
                    break;
                default:
                    newLevel = LogData.LogLevel.Debug;
                    break;
            }
            return newLevel;
        }
    }
    public class LogReceivedEventArgs : EventArgs
    {
        public string LogData { get; set; }
        public LogLevel Level { get; set; }
    }
}
