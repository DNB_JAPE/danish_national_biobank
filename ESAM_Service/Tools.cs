﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESAMService
{
    public static class Tools
    {
        public static bool IsNullOrEmpty(object target)
        {
            if (target == null)
                return true;
            else
            {
                if (target.GetType() != typeof(string))
                {
                    return (target == null);
                }
                else
                {
                    string test = (string)target;
                    if ((target == null) || (test == string.Empty))
                        return true;
                    else
                        return false;
                }
            }
        }
    }
}
