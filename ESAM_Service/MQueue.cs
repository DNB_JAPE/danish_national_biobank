﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ESAMService
{
    public class MQueue : INotifyPropertyChanged, IDisposable
    {

        public MQueue()
        {

        }

        #region Properties

        string _hostName = "localhost";

        /// <summary>
        /// Hostname for messageQueue Broker
        /// </summary>
        public string HostName
        {
            get { return _hostName; }
            set { _hostName = value; }
        }

        string _queueName = string.Empty;
        public string QueueName
        {
            get { return _queueName; }
            set { _queueName = value; }
        }

        private string _UserName;
        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

        private string _Password;
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }

        private int _DelayTimeInSeconds = 10;
        public int DelayTimeInSeconds
        {
            get { return _DelayTimeInSeconds; }
            set { _DelayTimeInSeconds = value; }
        }

        private bool _isListening = false;
        public bool isListening
        {
            get { return _isListening; }
            set { _isListening = value; }
        }

        private bool _Durable = false;
        public bool Durable
        {
            get { return _Durable; }
            set { _Durable = value; }
        }

        private int _DeleteAfterSeconds = 0;
        public int DeleteAfterSeconds
        {
            get { return _DeleteAfterSeconds; }
            set { _DeleteAfterSeconds = value; }
        }

        public IModel Channel
        {
            get { return channel; }
        }

        #endregion Properties

        #region Action

        public bool StartQueue()
        {
            var factory = new ConnectionFactory() { HostName = HostName };
            factory.UserName = this.UserName;
            factory.Password = this.Password;

            connection = factory.CreateConnection();
            channel = connection.CreateModel();

            return true;
        }

        public bool StartQueue(string hostName = "localhost")
        {
            var factory = new ConnectionFactory() { HostName = hostName };
            factory.UserName = this.UserName;
            factory.Password = this.Password;

            connection = factory.CreateConnection();
            channel = connection.CreateModel();

            return true;
        }

        public bool StopQueue()
        {
            try
            {
                channel.Close();
                connection.Close();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public void Send(string QueueName, string Message)  //, string NewHostName = "localhost"
        {
            try
            {
                if (connection == null)
                    StartQueue();
                if (DeleteAfterSeconds > 0)
                {
                    var args = new Dictionary<string, object>();
                    args.Add("x-message-ttl", DeleteAfterSeconds * 1000);
                    channel.QueueDeclare(queue: QueueName, durable: Durable, exclusive: false, autoDelete: false, arguments: args);
                }
                else
                    channel.QueueDeclare(queue: QueueName, durable: Durable, exclusive: false, autoDelete: false, arguments: null);

                var body = Encoding.UTF8.GetBytes(Message);

                IBasicProperties props = channel.CreateBasicProperties();
                props.Persistent = Durable;

                channel.BasicPublish(exchange: "", routingKey: QueueName, basicProperties: props, body: body);
            }
            catch (Exception ex)
            {
                Debug.Print("Error when sending: " + ex.ToString());
            }
        }

        public void Send(string Message)
        {
            Send(QueueName, Message);//, HostName
        }


        EventingBasicConsumer myConsumer;
        IModel channel;
        IConnection connection;

        public void Listen(string QueueName, string NewHostName = "localhost")
        {
            try
            {
                string receivedMssage = string.Empty;

                var factory = new ConnectionFactory() { HostName = NewHostName };
                factory.UserName = this.UserName;
                factory.Password = this.Password;

                //using (connection = factory.CreateConnection())
                //using (channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: QueueName,
                                         durable: Durable,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);

                    myConsumer = new EventingBasicConsumer(channel);
                    myConsumer.Received += onConsumer_Received;

                    channel.BasicConsume(queue: QueueName, autoAck: true, consumer: myConsumer);
                    while (isListening)
                    {
                        Thread.Sleep(500);
                        RaiseEventIfAny();
                    }
                    channel.Close();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                Debug.Print("Error when waiting for log: " + ex.ToString());
            }
        }

        Queue<EventReceivedArgs> myEvents = new Queue<EventReceivedArgs>();

        private void onConsumer_Received(object sender, BasicDeliverEventArgs e)
        {
            var body = e.Body;
            var message = Encoding.UTF8.GetString(body);
            var consumerTag = e.ConsumerTag;
            myEvents.Enqueue(new EventReceivedArgs()
            {
                Message = message,
                ConsumerTag = e.ConsumerTag,
                Exchange = e.Exchange,
                Redelivered = e.Redelivered,
                RoutingKey = e.RoutingKey
            });

            // What to do

            Debug.Print("Received: " + message);
        }

        private void RaiseEventIfAny()
        {
            if (myEvents.Count > 0)
            {
                if (OnReceivedMessage(myEvents.Peek()))
                    myEvents.Dequeue();
            }
            //OnReceivedMessage(new EventReceivedArgs()
            //{
            //    Message = message,
            //    ConsumerTag = e.ConsumerTag,
            //    Exchange = e.Exchange,
            //    Redelivered = e.Redelivered,
            //    RoutingKey = e.RoutingKey
            //});

        }

        public void Listen()
        {
            Listen(this.QueueName, this.HostName);
        }

        public event EventHandler<EventReceivedArgs> OnMessageReceived;

        protected virtual bool OnReceivedMessage(EventReceivedArgs e)
        {
            bool l_Reply = false;
            EventHandler<EventReceivedArgs> handler = OnMessageReceived;
            if (handler != null)
            {
                handler(this, e);
                l_Reply = true;
            }
            return l_Reply;
        }

        #endregion Action

        public event PropertyChangedEventHandler PropertyChanged;

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    StopQueue();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~MQueue() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }

    public class EventReceivedArgs : EventArgs
    {
        private string _message = string.Empty;
        public string Message { get { return _message; } set { _message = value; } }

        private string _ConsumerTag = string.Empty;
        //
        // Summary:
        //     The consumer tag of the consumer that the message was delivered to.
        public string ConsumerTag { get { return _ConsumerTag; } set { _ConsumerTag = value; } }

        //private ulong _DeliveryTag = 0;
        ////
        //// Summary:
        ////     The delivery tag for this delivery. See IModel.BasicAck.
        //public ulong DeliveryTag { get { return _DeliveryTag; } set { _DeliveryTag = value; } }

        private string _Exchange = string.Empty;
        //
        // Summary:
        //     The exchange the message was originally published to.
        public string Exchange { get { return _Exchange; } set { _Exchange = value; } }

        private bool _Redelivered = false;
        //
        // Summary:
        //     The AMQP "redelivered" flag.
        public bool Redelivered { get { return _Redelivered; } set { _Redelivered = value; } }

        private string _RoutingKey = string.Empty;
        //
        // Summary:
        //     The routing key used when the message was originally published.
        public string RoutingKey { get { return _RoutingKey; } set { _RoutingKey = value; } }

        private DateTime _ReceivedTime;
        public DateTime ReceivedTime { get { return _ReceivedTime; } set { _ReceivedTime = value; } }
    }
}
