﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static ESAMService.LogData;

namespace ESAMService
{
    public class QueueManager : IDisposable
    {
        MQueue myLogQueue;
        //MQueue myTestLogQueue;
        MQueue myActionQueue;
        KeepAlive myHeartBeat;
        //ServiceInfo myServiceInfo = new ServiceInfo();
        List<MQueue> myQueues = new List<MQueue>();
        public event EventHandler<EventReceivedArgs> ReceivedLogMessage;

        public QueueManager()
        {
            ReadSetup();
            InitInitialQueues();
        }

        private void ReadSetup()
        {
            try
            {
                var serviceInfoSettings = ConfigurationManager.GetSection("ServiceInfoSettings") as ServiceInfoSettings;
                if (serviceInfoSettings == null)
                {
                    // TODO 
                    Configuration myConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    myConfig.Sections.Add("ServiceInfoSettings", new ServiceInfoSettings());
                    myConfig.Save();
                }
                else
                {
                    try
                    {
                        _myInfo = (ServiceInfo)serviceInfoSettings;
                    }
                    catch (Exception x)
                    {
                        // TODO
                    }
                }
                // Get data for HostName of Queues
                var queueSettings = ConfigurationManager.GetSection("QueueSettings") as QueueSettings;
                if (queueSettings == null)
                {
                    // Create settings with default values
                }
                else
                {
                    this.myLogQueueSettings = queueSettings.LogQueueFeatures;
                    this.myActionQueueSettings = queueSettings.ActionQueueFeatures;
                }
            }
            catch (Exception ex)
            {
                // TODO
                throw;
            }

        }

        //Thread listenLogThread;
        private void InitInitialQueues()
        {
            try
            {
                this.myLogQueue = new MQueue()
                {
                    UserName = this.myLogQueueSettings.UserName,
                    Password = this.myLogQueueSettings.Password,
                    HostName = this.myLogQueueSettings.HostName,
                    QueueName = this.myLogQueueSettings.QueueName,
                    Durable = Convert.ToBoolean(this.myLogQueueSettings.Durable),
                    isListening = Convert.ToBoolean(this.myLogQueueSettings.isListening)
                };
                this.myLogQueue.StartQueue();
                myQueues.Add(myLogQueue);

                this.myActionQueue = new MQueue()
                {
                    UserName = this.myActionQueueSettings.UserName,
                    Password = this.myActionQueueSettings.Password,
                    HostName = this.myActionQueueSettings.HostName,
                    QueueName = this.myActionQueueSettings.QueueName,
                    DelayTimeInSeconds = Convert.ToInt32(this.myActionQueueSettings.DelayTimeSeconds),
                    Durable = Convert.ToBoolean(this.myActionQueueSettings.Durable),
                    DeleteAfterSeconds = Convert.ToInt32(this.myActionQueueSettings.DeleteAfterSeconds)
                };
                this.myActionQueue.StartQueue();
                myQueues.Add(myActionQueue);

                Log("Initialized Queues", LogLevel.Info);

                myHeartBeat = new KeepAlive(this.myActionQueue, myInfo);
            }
            catch (Exception ex)
            {
                string err = ex.ToString();
                // TODO
            }
        }

        private void MyLogQueue_OnMessageReceived(object sender, EventReceivedArgs e)
        {
            EventHandler<EventReceivedArgs> handler = ReceivedLogMessage;
            EventReceivedArgs a = new EventReceivedArgs();
            a.Message = e.Message;
            a.ReceivedTime = DateTime.Now;

            if (handler != null)
            {
                handler(this, a);
            }
        }

        List<string> ReceivedMessageList = new List<string>();

        //public void StartListening()
        //{
        //    listenLogThread.Start();
        //}


        #region Properties

        ServiceInfo _myInfo;
        public ServiceInfo myInfo
        {
            get { return _myInfo; }
            set { _myInfo = value; }
        }

        #endregion Properties

        //public void Log(string message)
        //{
        //    this.myLogQueue.Send(message + " - " + DateTime.Now);
        //}

        public void Log(string message, LogLevel level)
        {
            LogData myLog = new LogData()
            {
                Message = message,
                Level = level,
                Name = _myInfo.Name,
                Time = DateTime.Now,
                TCPIP = _myInfo.IP
            };
            this.myLogQueue.Send(JsonConvert.SerializeObject(myLog));
        }
        private LogQueueFeatures _myLogQueue;
        public LogQueueFeatures myLogQueueSettings { get { return _myLogQueue; } set { _myLogQueue = value; } }

        private ActionQueueFeatures _myActionQueue;
        public ActionQueueFeatures myActionQueueSettings { get { return _myActionQueue; } set { _myActionQueue = value; } }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    //if (listenLogThread != null)
                    //{
                    //    listenLogThread.Abort();
                    //    listenLogThread.Join();
                    //    listenLogThread = null;
                    //}
                    this.myLogQueue.StopQueue();
                    this.myActionQueue.StopQueue();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~QueueManager() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }

}
