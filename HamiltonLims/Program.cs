﻿using DNBTools.Nautilus;
using System;
using System.Configuration;
using System.IO;
using System.Reflection;

namespace HamiltonLims
{
    class Program
    {
        static void Main(string[] args)
        {
            if((args[0].ToUpper() == "/C" || args[0].ToUpper() == "-C") && args.Length == 3)
            {
                Environment.ExitCode = CheckSampleExistence(args[1], args[2]) ? 0 : 1;
            }
        }

        private static bool CheckSampleExistence(string sampleBarcode, string sampleTubeType)
        {
            try
            {
                Log("Check for sample existence: " + sampleBarcode + "/" + sampleTubeType);
                LimsCommunication limsCommunication = new LimsCommunication(ConfigurationManager.ConnectionStrings["Nautilus"].ConnectionString);
                return limsCommunication.SampleExist(sampleBarcode, sampleTubeType);
            }
            catch(Exception ex)
            {
                Log("Failed to check for sample existence: " + ex.Message);
                return false;
            }
        }

        private static void Log(string message)
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            string configurationPath = Path.GetDirectoryName(path);
            string logFile = Path.Combine(configurationPath, DateTime.Today.ToString("yyyy-MM-dd") + ".log");
            File.AppendAllText(logFile, DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + " " + message + Environment.NewLine);
        }
    }
}