﻿using DAL.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using Core.Models;
using System.Data.Entity;

namespace Core.Operations
{
    public class WebOperations : AbstractOperations
    {

        public DAL.Context.Language getCurrentLanguage()
        {
            return Uow.LanguageRepository.Get(z => z.Name == CultureInfo.CurrentCulture.Name).FirstOrDefault();
        }
        public IEnumerable<InfoTranslation> getTextPages()
        {
            var language = getCurrentLanguage();
            if (language == null)
            {
                language = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            }
            return Uow.InfoTranslationRepository.Query.Include(x => x.Info.Sections.SectionTranslation).Include(x=>x.Language).Include(x => x.File).Where(x => x.Language.Id == language.Id && x.Info.Sections.SectionTypes.Name == "Text Page" && x.Info.Published==true).AsEnumerable();
        }
        public string getLogo()
        {
            return Uow.SettingsRepository.Get(x => x.Slug == "logo_url").FirstOrDefault().Value;
        }
        public List<InfoAdditionalTranslate> getQuestionnaire()
        {
            var language = getCurrentLanguage();
            if (language == null)
            {
                language = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            }
            var info = Uow.InfoRepository.Get(x => x.InfoAdditional.FirstOrDefault() !=null && x.Active == true).LastOrDefault();
            return Uow.InfoAdditionalTranslateRepository.Query.Include(x => x.InfoAdditional.Info.InfoTranslation).Where(x => x.Language.Id == language.Id && x.InfoAdditional.Info.Id == info.Id).ToList();
        }
        public IQueryable<InfoTranslation> getNewsPostPages(string sectionSlug,bool isSectionSlug)
        {
            var b = getHomeNewsBlock();
            var language = getCurrentLanguage();
            if (language == null)
            {
                language = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            }
            int section = 0;
            if (isSectionSlug)
            {
                section = Uow.SectionsRepository.Get(x => x.Slug == sectionSlug).FirstOrDefault().Id;
                return Uow.InfoTranslationRepository.Query.Include(x => x.Info.Sections.SectionTranslation).Include(x=>x.Language).Include(x => x.File).Where(x => x.Language.Id == language.Id && x.Info.Sections.SectionTypes.Name == "Post" && x.Info.SectionId == section && x.Info.Published == true && x.Info.ImportantType== b);
            }
            else
            {
                
                var a = Uow.InfoTranslationRepository.Query.Include(x => x.Info.Sections.SectionTranslation).Include(x => x.Language).Include(x => x.File).Where(x => x.Language.Id == language.Id && x.Info.Sections.SectionTypes.Name == "Post" && x.Info.Published == true && x.Info.ImportantType == b);

                // section = Uow.InfoTranslationRepository.Get(x => x.Slug == sectionSlug).FirstOrDefault().Info.SectionId;
                return Uow.InfoTranslationRepository.Query.Include(x => x.Info.Sections.SectionTranslation).Include(x => x.Language).Include(x => x.File).Where(x => x.Language.Id == language.Id && x.Info.Sections.SectionTypes.Name == "Post" && x.Info.Published == true && x.Info.ImportantType == b);
            }
           
        }
        public IQueryable<InfoTranslation> getSlidePostPages(string sectionSlug, bool isSectionSlug)
        {
            var b = getSliderWhoisRunningthisstudy();
            var language = getCurrentLanguage();
            if (language == null)
            {
                language = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            }
            int section = 0;
            if (isSectionSlug)
            {
                section = Uow.SectionsRepository.Get(x => x.Slug == sectionSlug).FirstOrDefault().Id;
                return Uow.InfoTranslationRepository.Query.Include(x => x.Info.Sections.SectionTranslation).Include(x => x.Language).Include(x => x.File).Where(x => x.Language.Id == language.Id && x.Info.Sections.SectionTypes.Name == "Post" && x.Info.SectionId == section && x.Info.Published == true && x.Info.ImportantType == b);
            }
            else
            {
                return Uow.InfoTranslationRepository.Query.Include(x => x.Info.Sections.SectionTranslation).Include(x => x.Language).Include(x => x.File).Where(x => x.Language.Id == language.Id && x.Info.Sections.SectionTypes.Name == "Post" && x.Info.Published == true && x.Info.ImportantType == b);
            }

        }
        public InfoTranslation GetContactPage()
        {
            var language = getCurrentLanguage();
            if (language == null)
            {
                language = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            }
            return Uow.InfoTranslationRepository.Query.Include(x => x.Info).Where(x => x.Language.Id == language.Id && x.Info.Sections.SectionTypes.Name == "Contact Page" && x.Info.Active==true).FirstOrDefault();
        }
        public IEnumerable<InfoTranslation> GetPublicationPage(string sectionSlug,bool isSectionSlug)
        {
            var language = getCurrentLanguage();
            if (language == null)
            {
                language = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            }
            int section = 0;
            if (isSectionSlug)
            {
                section = Uow.SectionsRepository.Get(x => x.Slug == sectionSlug).FirstOrDefault().Id;
                var result =  Uow.InfoTranslationRepository.Query.Include(x => x.Info).Include(x => x.File).Include(x => x.Info.InfoFiles).Where(x => x.Language.Id == language.Id && x.Info.Sections.SectionTypes.Name == "Publications" && x.Info.SectionId == section && x.Info.Published == true).AsEnumerable();
                return result;
            }
            else
            {
                //section = Uow.InfoTranslationRepository.Get(x => x.Slug == sectionSlug).FirstOrDefault().Info.SectionId;
                var result = Uow.InfoTranslationRepository.Query.Include(x => x.Info).Include(x => x.File).Include(x => x.Info.InfoFiles).Where(x => x.Language.Id == language.Id && x.Info.Sections.SectionTypes.Name == "Publications" && x.Info.Published == true).AsEnumerable();
                return result;
            }
            
        }

        public int ValidateStoreboxAccount(string email,string nemid)
        {
            // ESAT 20181120
            //Users newUser = new Users();
            //newUser.StoreboxEmail = email;
            //newUser.NemId = nemid;
            //newUser.Active = true;
            //Uow.UsersRepository.Insert(newUser);
            //return Uow.UsersRepository.Query.Where(x => x.StoreboxEmail == email && x.NemId != nemid && x.Active == true).Count();
            return 0;
        }


        public List<Settings> GetSettings()
        {
            return Uow.SettingsRepository.Get(x => x.Editable == true).ToList();
        }
        public IEnumerable<GalleryGroupTranslate> getGalleries()
        {
            var language = getCurrentLanguage();
            if (language == null)
            {
                language = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            }
            //file migvaklda
            var a = Uow.GalleryGroupTranslateRepository.Query.Include(x => x.GalleryGroup.Sections.SectionTranslation).Include(x => x.Language).Include(x => x.GalleryGroup.Gallery).Where(x => x.Language.Id == language.Id && x.GalleryGroup.Sections.SectionTypes.Name == "Photo Gallery Page" && x.GalleryGroup.Active == true).AsEnumerable();
            return a;
        }
        public List<int> MiddleMenuItems()
        {
            var menu = Uow.MenuTypesRepository.Get(x => x.Name == "Middle Menu").FirstOrDefault();
            return Uow.SectionToMenuRepository.Get(x => x.MenuTypes.Id == menu.Id).Select(x => x.SectionId).ToList();
        }
        public List<int> TopMenuItems()
        {
            var menu = Uow.MenuTypesRepository.Get(x => x.Name == "Top Menu").FirstOrDefault();
            return Uow.SectionToMenuRepository.Get(x => x.MenuTypes.Id == menu.Id).Select(x => x.SectionId).ToList();
        }
        public List<int> MainMenuItems()
        {
            var menu = Uow.MenuTypesRepository.Get(x => x.Name == "Main Menu").FirstOrDefault();
            return Uow.SectionToMenuRepository.Get(x => x.MenuTypes.Id == menu.Id).Select(x => x.SectionId).ToList();
        }
        public List<int> BottomMenuItems()
        {
            var menu = Uow.MenuTypesRepository.Get(x => x.Name == "Bottom Menu").FirstOrDefault();
            return Uow.SectionToMenuRepository.Get(x => x.MenuTypes.Id == menu.Id).Select(x => x.SectionId).ToList();
        }
        public int getHomeNewsBlock()
        {
            return Uow.SettingsRepository.Get(x => x.Description == "Home News Block").FirstOrDefault().Id;
        }
        public int getSliderWhoisRunningthisstudy()
        {
            return Uow.SettingsRepository.Get(x => x.Description == "Slider (Who is Running this study)").FirstOrDefault().Id;
        }
        public List<BannerTranslation> getSliderBanner()
        {
            var language = getCurrentLanguage();
            if (language == null)
            {
                language = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            }
            return Uow.BannerTranslationRepository.Query.Include(x => x.File).Where(x => x.Banner.Settings.Description == "Slider" && x.Language.Id == language.Id && x.Banner.Active == true).ToList();
        }
        public List<InfoTranslation> getInfos()
        {
            var language = getCurrentLanguage();
            if (language == null)
            {
                language = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            }
            return Uow.InfoTranslationRepository.Query.Include(x => x.Info.Sections.SectionTranslation).Include(x => x.Language).Include(x => x.File).Where(x => x.Language.Id == language.Id && x.Info.Active == true).ToList();
        }
        public List<SectionTranslation> getSections()
        {
            var language = getCurrentLanguage();
            if (language == null)
            {
                language = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            }
            return Uow.SectionTranslationsRepository.Query.Include(x => x.Sections.Info).Where(x => x.Language.Id == language.Id && x.Sections.active == true).OrderBy(x => x.Sections.sort).ToList();

        }
        public List<SectionTranslation> getSectionsForREgistrationPage()
        {
            var language = getCurrentLanguage();
            if (language == null)
            {
                language = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            }
            var textType = Uow.SectionTypesRepository.Get(x => x.Name == "Text Page").FirstOrDefault();
            var questionbaireType = Uow.SectionTypesRepository.Get(x => x.Name == "Questionnaire").FirstOrDefault();
            return Uow.SectionTranslationsRepository.Query.Include(x => x.Sections.Info).Where(x => x.Language.Id == language.Id && x.Sections.active == true &&(x.Sections.SectionTypes.Id == textType.Id || x.Sections.SectionTypes.Id == questionbaireType.Id)).OrderBy(x => x.Sections.sort).ToList();
        }
        public List<DiseaseTranslation> getDiseases()
        {
            var language = getCurrentLanguage();
            if (language == null)
            {
                language = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            }
            return Uow.DiseaseTranslationRepository.Query.Include(x => x.Disease).Where(x => x.Language.Id == language.Id && x.Disease.Active == true).ToList();
        }
        public string getAction(string structure, string info, out string infoSlug, out string structureSlug)
        {
            var language = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            var infotranslation = Uow.InfoTranslationRepository.Get(x => x.Slug == info && x.Language.Id == language.Id).FirstOrDefault();
            if (infotranslation == null)
            {
                int n;
                bool isNumeric = int.TryParse(info, out n);
                if (isNumeric)
                {
                    var gallery = Uow.GalleryGroupRepository.Get(Convert.ToInt32(info));
                    if (gallery != null)
                    {
                        var section = Uow.SectionsRepository.Get(x => x.Id == gallery.SectionId, null, "SectionTypes").FirstOrDefault();
                        if (section == null)
                        {
                            infoSlug = null;
                            structureSlug = null;
                            return "home";
                        }
                        else if (section.Slug != structure)
                        {
                            infoSlug = null;
                            structureSlug = null;
                            return "home";
                        }
                        else
                        {
                            infoSlug = info;
                            structureSlug = structure;
                            return section.SectionTypes.Action;
                        }
                    }
                }
                else
                {
                    string slug = structure + "/" + info;
                    var sectionSlug = Uow.SectionsRepository.Get(x => x.Slug == slug, null, "SectionTypes").FirstOrDefault();
                    if (sectionSlug != null)
                    {
                        structureSlug = slug;
                        infoSlug = null;
                        return sectionSlug.SectionTypes.Action;
                    }
                }
                infoSlug = null;
                structureSlug = null;
                return "home";
            }
            else
            {
                var infos = Uow.InfoRepository.Get(infotranslation.InfoId);
                var section = Uow.SectionsRepository.Get(x => x.Id == infos.SectionId, null, "SectionTypes").FirstOrDefault();
                if (section == null)
                {
                    infoSlug = null;
                    structureSlug = null;
                    return "home";
                }
                else if (section.Slug != structure)
                {
                    infoSlug = null;
                    structureSlug = null;
                    return "home";
                }
                else
                {
                    infoSlug = info;
                    structureSlug = structure;
                    return section.SectionTypes.Action;
                }
            }
        }
        public string getAction(string Slug, out string structureSlug, out string infoSlug)
        {
            var language = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            var infotranslation = Uow.InfoTranslationRepository.Get(x => x.Slug == Slug && x.Language.Id == language.Id).FirstOrDefault();
            if (infotranslation == null)
            {
                var sectionSlug = Uow.SectionsRepository.Get(x => x.Slug == Slug, null, "SectionTypes").FirstOrDefault();
                if (sectionSlug != null)
                {
                    structureSlug = Slug;
                    infoSlug = null;
                    return sectionSlug.SectionTypes.Action;
                }
                structureSlug = "home";
                infoSlug = null;
                return "home";
            }
            else
            {
                var info = Uow.InfoRepository.Get(infotranslation.InfoId);
                var section = Uow.SectionsRepository.Get(x => x.Id == info.SectionId, null, "SectionTypes").FirstOrDefault();
                if (section == null)
                {
                    structureSlug = "home";
                    infoSlug = null;
                    return "home";
                }
                else
                {
                    infoSlug = Slug;
                    structureSlug = null;
                    return section.SectionTypes.Action;
                }
            }
        }
        public bool Subscribe(string email)
        {
            var language = getCurrentLanguage();
            if (language == null)
            {
                language = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            }
            var subscribe = Uow.SubscribesRepository.Get(x => x.Email == email).FirstOrDefault();
            if (subscribe != null)
            {
                return true;
            }
            else
            {
                Subscribes sub = new Subscribes();
                sub.Email = email;
                sub.Language = language;
                Uow.SubscribesRepository.Insert(sub);
                return false;
            }
        }
        
        public registrationReturn registration(RegistrationModel model)
        {
            var language = getCurrentLanguage();
            if (language == null)
            {
                language = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            }
            var translation = Uow.TranslationRepository.Get(x => x.Language.Id == language.Id).FirstOrDefault();
            // Need to change all validation because DB will allways be empty...
            //if (!string.IsNullOrEmpty(model.email))
            //{
            //    var emailValidation = Uow.UsersRepository.Get(x => x.Email == model.email && x.Active == true).FirstOrDefault();
            //    if (emailValidation != null)
            //    {
            //        return new registrationReturn() { id = 0, error = translation.EmailExists, errorType = "email" };
            //    }
            //}
            //if (!string.IsNullOrEmpty(model.phone))
            //{
            //    var phoneValidation = Uow.UsersRepository.Get(x => x.Phone == model.phone && x.Active == true).FirstOrDefault();
            //    if (phoneValidation != null)
            //    {
            //        return new registrationReturn() { id = 0, error = translation.UniquePhone, errorType = "phone" };
            //    }
            //}
            //if(!string.IsNullOrEmpty(model.storeboxPhone))
            //{
            //    var storeboxPhoneValidation = Uow.UsersRepository.Get(x => x.StoreboxPhone == model.storeboxPhone && x.Active == true).FirstOrDefault();
            //    if (storeboxPhoneValidation != null)
            //    {
            //        return new registrationReturn() { id = 0, error = translation.StoreboxUniquePhone, errorType = "storeboxphone" };
            //    }
            //}
            //if (!string.IsNullOrEmpty(model.storeboxEmail))
            //{
            //    var storeboxValidation = Uow.UsersRepository.Get(x => x.StoreboxEmail == model.storeboxEmail && x.Active == true).FirstOrDefault();
            //    if (storeboxValidation != null)
            //    {
            //        return new registrationReturn() { id = 0, error = translation.StoreboxEmailAllreadyRegistered, errorType = "storeboxemail" };
            //    }
            //}
            if (string.IsNullOrEmpty(model.nemid))
            {
                return new registrationReturn() { id = -1, error = " ", errorType = "nemid" };
            }
            //else if(Uow.UsersRepository.Get(x => x.NemId == model.nemid && x.Active == true).FirstOrDefault() !=null)
            //{
            //    return new registrationReturn() { id = 0, error = translation.NemidAllreadyRegistered, errorType = "nemid" };
            //}
            Users user = new Users();
            user.AcceptUseData = model.acceptDedidentifedDataAccessed;
            user.Active = true;
            user.ContactMe = model.contatMeIfProjectExpanded;
            user.Email = model.email;
            user.SendMeNewsLetter = model.sendNewsletters;
            user.Phone = model.phone;
            user.RegistrationDate = DateTime.Now;
            user.AuthenticationInfo = model.AuthenticationInfo;

            if (model.StoreboxConsentWithField || model.StoreboxConsentWithoutField)
            {
                user.StoreboxConsent = 1;
                if (model.StoreboxConsentWithField_Field.Trim() != string.Empty)
                    user.ProjectConsent = model.StoreboxConsentWithField_Field;
            }

            if (model.storeboxEmail !=null && model.storeboxPhone !=null)
            {
                user.StoreboxEmail = model.storeboxEmail;
                user.StoreboxPhone = model.storeboxPhone;
            }

            user.NemId = model.nemid;
            user.ParticipateInProject = model.Participate_text;
            
            Uow.UsersRepository.Insert(user);
            if(model.selecteddisease!=null)
            {
            foreach(var item in model.selecteddisease)
                {
                    Uow.UserDiseaseRepository.Insert(new UserDisease() { Users = user, DiseaseId = item });
                }
            }
            if(model.Unlisteddisease!=null)
            {
            foreach(var item in model.Unlisteddisease)
                {
                    var diseaseTranslation = new DiseaseTranslation();
                    var diseaseTranslationdk = new DiseaseTranslation();
                    var disease = new Disease();
                    disease.Active = false;
                    Uow.DiseaseRepository.Insert(disease);
                    diseaseTranslation.Language = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault(); ;
                    diseaseTranslation.Title = item;
                    diseaseTranslation.Disease = disease;
                    diseaseTranslationdk.Language = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
                    diseaseTranslationdk.Title = item;
                    diseaseTranslationdk.Disease = disease;
                    Uow.DiseaseTranslationRepository.Insert(diseaseTranslation);
                    Uow.DiseaseTranslationRepository.Insert(diseaseTranslationdk);
                    Uow.UserDiseaseRepository.Insert(new UserDisease() { Users = user, Disease = disease });
                }
            }
            return new registrationReturn() { id = user.Id, error = "" };
        }

        public bool Insert(string SBemail, string validation_code)
        {
            try
            {
                Users newUser = new Users();
                newUser.StoreboxEmail = SBemail;
                newUser.ValidationCode = validation_code;
                newUser.Active = true;
                Uow.UsersRepository.Insert(newUser);
                return true;
            }
            catch
            {
                return false;
            }
        }

        //public bool Insert(int Id, string validation_code)
        //{
        //    try
        //    {
        //        Users newUser = new Users();
        //        //newUser.OldID = Id;
        //        newUser.ValidationCode = validation_code;
        //        Uow.UsersRepository.Insert(newUser);
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

        public bool Update(int Id, string validation_code)
        {
            try
            {
                var user = Uow.UsersRepository.Get(Id);
                user.ValidationCode = validation_code;
                Uow.UsersRepository.Update(user);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(string storebox_account_id, string validation_code, string nemid)
        {
            try
            {
                Users user = new Users();
                // ESAT 20181120
                //var user = Uow.UsersRepository.Query.Where(x=>x.NemId == nemid && x.Active == true).FirstOrDefault();
                user.ValidationCode = validation_code;
                user.StoreboxEmail = storebox_account_id;
                user.Active = true;
                user.NemId = nemid;
                // ESAT 20181120
                //Uow.UsersRepository.Update(user);
                Uow.UsersRepository.Insert(user);
                return true;
            }
            catch
            {
                return false;
            }

        }

        public UserModel GetRecieptPageInfo(string nemid)
        {
            try
            {
                var user = Uow.UsersRepository.Query.Where(x=>x.NemId == nemid && x.Active==true).FirstOrDefault();
                if (user == null)
                {
                    return new UserModel()
                    {
                        NemId = nemid,
                        StoreboxEmail = "",
                        ValidationCode = ""
                    };
                }
                else
                {
                    return new UserModel()
                    {
                        Id = user.Id,
                        StoreboxEmail = user.StoreboxEmail,
                        ValidationCode = user.ValidationCode
                    };
                }
            }
            catch
            {
                return null;
            }
            
        }

        public bool UpdateByStorebox(string storbox_account_id)
        {
            try
            {
                var user = Uow.UsersRepository.Query.Where(x=>x.Email == storbox_account_id).FirstOrDefault();
                if(user !=null)
                {
                    user.Active = true;
                    Uow.UsersRepository.Update(user);
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
            
        }


        public int GenerateKey(string Email)
        {
            try
            {
                var user = Uow.UsersRepository.Query.Where(x => x.Email == Email).FirstOrDefault();
                if (user != null)
                {
                    if((bool)user.Active)
                    {
                        return 1;
                    }
                    Random rnd = new Random();
                    int code = rnd.Next(100000, 999999);
                    return code;
                }
                return 0;
            }
            catch
            {
                return 0;
            }
        }

        public List<int> BannerToSection(int id)
        {
            var bannertosection = Uow.BannerToSectionRepository.Get(x => x.SectionId == id);
            List<int> relation = new List<int>();
            foreach(var item in bannertosection)
            {
                relation.Add(item.BannerId);
            }
            return relation;
        }
        public BannerTranslation getHeaderBanner()
        {
            var language = getCurrentLanguage();
            if (language == null)
            {
                language = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            }
            return Uow.BannerTranslationRepository.Query.Include(x => x.Banner).Include(x => x.File).Where(x => x.Banner.Settings.Value == "2" && x.Language.Id == language.Id && x.Banner.Active == true).FirstOrDefault();
        }
        public BannerTranslation getFooterBanner()
        {

            var language = getCurrentLanguage();
            if (language == null)
            {
                language = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            }
            return Uow.BannerTranslationRepository.Query.Include(x => x.Banner).Include(x => x.File).Where(x => x.Banner.Settings.Value == "1" && x.Language.Id == language.Id && x.Banner.Active==true).FirstOrDefault();
        }
        public int homeSectionId()
        {
            return Uow.SectionsRepository.Get(x => x.SectionTypes.Name == "Home Page").FirstOrDefault().Id;
        }
        public int getCurrentSectionId(string slug)
        {
            return Uow.SectionsRepository.Get(x => x.Slug == slug).FirstOrDefault().Id;
        }
        public int getCurrentSectionByInfoSlugId(string slug)
        {
            return Uow.InfoTranslationRepository.Get(x => x.Slug == slug).FirstOrDefault().Info.SectionId;
        }
        public bool DeleteUser(string nemid)
        {
            // Create new user with nemid and active=false
            Users newUser = new Users() { NemId = nemid, Active = false };
            Uow.UsersRepository.Insert(newUser);
            return true;
            //var user = Uow.UsersRepository.Get(x => x.NemId == nemid && x.Active == true);
            //if(user.Count() > 0)
            //{
            //    foreach(var u in user)
            //    {
            //        u.Active = false;
            //        Uow.UsersRepository.Update(u);
            //    }
            //    return true;
            //}
            //return false;
        }
        public string SectionSlug(string slug)
        {
            var language = getCurrentLanguage();
            if (language == null)
            {
                language = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            }
            var section = Uow.SectionTranslationsRepository.Get(x => x.Sections.Slug == slug && x.Language.Id == language.Id).FirstOrDefault();
            if(section != null)
            {
                return section.Name;
            }
            return "";
        }
    }
}
