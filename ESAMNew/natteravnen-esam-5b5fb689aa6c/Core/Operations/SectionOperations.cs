﻿using DAL.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Core.Models;
using System.Globalization;

namespace Core.Operations
{
    public class SectionOperations : AbstractOperations
    {
        public IEnumerable<SectionTranslation> GetAll()
        {
            var language = Uow.LanguageRepository.Get(z => z.Name == CultureInfo.CurrentCulture.Name).FirstOrDefault();
            if (language == null)
                return Uow.SectionTranslationsRepository.Get(x=>x.LanguageId == 1, null, "Sections");
            return Uow.SectionTranslationsRepository.Get(x=>x.Language.Id == language.Id, null, "Sections");
        }
        public IEnumerable<SectionTranslation> GetByTypeName(string type)
        {
            return Uow.SectionTranslationsRepository.Get(x => x.Sections.active == true && x.LanguageId == 1 && x.Sections.SectionTypes.Name == type, null, "Sections");
        }
        public void updateSection(List<Sections> section)
        {
            foreach (var item in section)
            {
                var updateSection = Uow.SectionsRepository.Get(x => x.Id == item.Id).FirstOrDefault();
                updateSection.ParentId = item.ParentId;
                updateSection.sort = item.sort;
                updateSection.Slug = item.Slug;
                Uow.SectionsRepository.Update(updateSection);
            }
        }

        public string GetSectionSlug(int Id)
        {
            return Uow.SectionsRepository.Get(x => x.Id == Id).FirstOrDefault().Slug;
        }
        public void createPage(Section section)
        {
            SectionTranslation _sectionTranslation = new SectionTranslation();
            SectionTranslation _sectionTranslationdk = new SectionTranslation();
            Sections _section = new Sections();
            int sort;
            if (section.ParentId != null)
            {
                int parentId = (int)section.ParentId;
                sort = Uow.SectionsRepository.Get(x => x.Id == parentId).FirstOrDefault().sort;
                string parentSlug = GetSectionSlug(parentId);
                int count = parentSlug.Count(f => f == '/');
                if (count >= 4)
                {
                    section.ParentId = null;
                }
                else
                {
                    section.Slug = parentSlug + "/" + section.Slug;
                }

            } else
            {
                sort = Uow.SectionsRepository.Query.Max(x => x.sort);
            }
            _section.ParentId = section.ParentId;
            _section.TypeId = section.TypeId;
            _section.Style = section.Style;
            _section.icon = section.Icon;
            _section.active = section.Active;
            _section.sort = sort;
            _section.Style = section.Style;
            _section.Slug = section.Slug;

            Uow.SectionsRepository.Insert(_section);

            _sectionTranslation.Description = section.Description;
            _sectionTranslation.Name = section.Name;
            _sectionTranslation.Sections = _section;
            _sectionTranslation.Language = Uow.LanguageRepository.Get(x => x.Name == "en-US").FirstOrDefault();

            Uow.SectionTranslationsRepository.Insert(_sectionTranslation);
            if (section.Namedk != null && section.Descriptiondk != null)
            {
                _sectionTranslationdk.Name = section.Namedk;
                _sectionTranslationdk.Description = section.Descriptiondk;
                _sectionTranslationdk.Language = Uow.LanguageRepository.Get(x => x.Name == "da-DK").FirstOrDefault();
                _sectionTranslationdk.Sections = _section;
                Uow.SectionTranslationsRepository.Insert(_sectionTranslationdk);
            }
            if (section.MenuTypes != null)
            {
                foreach (var item in section.MenuTypes)
                {
                    Uow.SectionToMenuRepository.Insert(new SectionToMenu() { Sections = _section, MenuId = item });
                }
            }

        }
        public bool DeleteImage(string name)
        {
            var file = Uow.FileRepository.Get(x => x.FileName == name).FirstOrDefault();
            bool Info = Uow.InfoTranslationRepository.Get().Where(i => i.File == file).Any();
            bool Banner = Uow.BannerTranslationRepository.Get().Where(i => i.File == file).Any();
            if(Info||Banner)
            {
                return false;
            }
            try
            {
                Uow.FileRepository.Delete(file);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteFile(string name)
        {
            var file = Uow.InfoFileRepository.Get(x => x.Name == name).FirstOrDefault();
            bool Info = Uow.InfoToInfoFilesRepository.Get().Where(i => i.InfoFiles.Id == file.Id).Any();
            if (Info)
            {
                return false;
            }
            try
            {
                Uow.InfoFileRepository.Delete(file);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public IEnumerable<MenuTypes> GetMenues()
        {
            return Uow.MenuTypesRepository.GetAll();
        }
        public IEnumerable<SectionTypes> GetSectionTypes()
        {
            return Uow.SectionTypesRepository.GetAll();
        }


        public bool DeletePage(int id)
        {
            try
            {
                bool sectionRelationShip = Uow.SectionsRepository.Get().Where(i => i.ParentId == id).Any();
                bool bannerTosection = Uow.BannerToSectionRepository.Get().Where(x => x.SectionId == id).Any();
                bool galleryGroup = Uow.GalleryGroupRepository.Get().Where(x => x.SectionId == id).Any();
                bool info = Uow.InfoRepository.Get().Where(x => x.SectionId == id).Any();
                if (sectionRelationShip || bannerTosection || galleryGroup || info)
                {
                    return false;
                }
                var sectionTranslation = Uow.SectionTranslationsRepository.Get(x => x.SectionId == id);
                foreach (var item in sectionTranslation)
                {
                    Uow.SectionTranslationsRepository.Delete(item);
                }
                var sectionToMenu = Uow.SectionToMenuRepository.Get(x => x.SectionId == id);
                if (sectionToMenu != null)
                {
                    foreach (var item in sectionToMenu)
                    {
                        Uow.SectionToMenuRepository.Delete(item);
                    }
                }
                var section = Uow.SectionsRepository.Get(id);

                Uow.SectionsRepository.Delete(section);
                return true;
            } catch
            {
                return false;
            }
        }
        public Section GetSection(int SectionId)
        {
            Section section = new Section();
            List<int> MenuTypesList = new List<int>();

            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var English = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();

            var sectionTranslationById = Uow.SectionTranslationsRepository.Get(x => x.SectionId == SectionId && x.Language.Id == English.Id).FirstOrDefault();
            var menuTypes = Uow.SectionToMenuRepository.Get(x => x.SectionId == SectionId);
            var DkSections = Uow.SectionTranslationsRepository.Get(x => x.SectionId == SectionId && x.Language.Id == Dk.Id).FirstOrDefault();
            if (DkSections != null)
            {
                section.Namedk = DkSections.Name;
                section.Descriptiondk = DkSections.Description;
                section.SectionTranslationIddk = DkSections.Id;
            }
            string slug = sectionTranslationById.Sections.Slug;
            var i = slug.LastIndexOf("/");
            if (i > 0)
            {
                i = i + 1;
                slug = slug.Substring(i, slug.Length - i);
            }
            section.Id = sectionTranslationById.SectionId;
            section.Name = sectionTranslationById.Name;
            section.Active = sectionTranslationById.Sections.active;
            section.Description = sectionTranslationById.Description;
            section.Icon = sectionTranslationById.Sections.icon;
            section.Style = sectionTranslationById.Sections.Style;
            section.TypeId = sectionTranslationById.Sections.TypeId;
            section.ParentId = sectionTranslationById.Sections.ParentId;
            section.Sort = sectionTranslationById.Id;
            section.Slug = slug;
            section.SectionTranslationId = sectionTranslationById.Id;
            foreach (var item in menuTypes)
            {
                MenuTypesList.Add(item.MenuId);
            }
            section.MenuTypes = MenuTypesList;

            return section;
        }


        public void UpdatePage(Section section)
        {
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var English = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            var _sectionTranslationEn = Uow.SectionTranslationsRepository.Get(x => x.Id == section.SectionTranslationId).FirstOrDefault();
            var _section = Uow.SectionsRepository.Get(x => x.Id == section.Id).FirstOrDefault();
            int sort;
            if (section.ParentId != null)
            {
                int parentId = (int)section.ParentId;
                // sort = Uow.SectionsRepository.Get(x => x.Id == parentId).FirstOrDefault().sort;
                string parentSlug = GetSectionSlug(parentId);
                int count = parentSlug.Count(f => f == '/');
                if (count >= 4)
                {
                    section.ParentId = null;
                }
                else
                {
                    section.Slug = parentSlug + "/" + section.Slug;
                }

            }
            else
            {
                sort = Uow.SectionsRepository.Query.Max(x => x.sort);
            }

            string slug = _section.Slug;
            var i = slug.LastIndexOf("/");
            if (i > 0)
            {
                i = i + 1;
                slug = slug.Substring(i, slug.Length - i);
            }
            _section.ParentId = section.ParentId;
            _section.TypeId = section.TypeId;
            _section.Style = section.Style;
            _section.icon = section.Icon;
            _section.active = section.Active;
            _section.Style = section.Style;
            _section.Slug = slug + "/" + section.Slug;

            Uow.SectionsRepository.Update(_section);

            _sectionTranslationEn.Description = section.Description;
            _sectionTranslationEn.Name = section.Name;
            _sectionTranslationEn.Sections = _section;

            Uow.SectionTranslationsRepository.Update(_sectionTranslationEn);
            if (section.Namedk != null && section.Descriptiondk != null)
            {
                var _sectionTranslationdk = Uow.SectionTranslationsRepository.Get(x => x.Id == section.SectionTranslationIddk).FirstOrDefault();
                if (_sectionTranslationdk == null)
                {
                    _sectionTranslationdk = new SectionTranslation();
                    _sectionTranslationdk.Name = section.Namedk;
                    _sectionTranslationdk.Description = section.Descriptiondk;
                    _sectionTranslationdk.Language = Dk;
                    _sectionTranslationdk.Sections = _section;
                    Uow.SectionTranslationsRepository.Insert(_sectionTranslationdk);
                }
                _sectionTranslationdk.Name = section.Namedk;
                _sectionTranslationdk.Description = section.Descriptiondk;
                _sectionTranslationdk.Sections = _section;
                Uow.SectionTranslationsRepository.Update(_sectionTranslationdk);
            }
            var sectionToMenu = Uow.SectionToMenuRepository.Get(x => x.SectionId == section.Id);
            if (sectionToMenu != null)
            {
                foreach (var item in sectionToMenu)
                {
                    Uow.SectionToMenuRepository.Delete(item);
                }
            }
            if (section.MenuTypes != null)
            {
                foreach (var item in section.MenuTypes)
                {
                    Uow.SectionToMenuRepository.Insert(new SectionToMenu() { Sections = _section, MenuId = item });
                }
            }
        }
        public bool CheckFileName(string filename)
        {
            var file = Uow.FileRepository.Get(x => x.FileName == filename).FirstOrDefault();
            if (file == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool CheckGalleryCoverFileName(string filename)
        {
            var file = Uow.GalleryRepository.Get(x => x.File.FileName == filename).FirstOrDefault();
            if (file == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool CheckFileNameInfoFilesTable(string filename)
        {
            var file = Uow.InfoFileRepository.Get(x => x.Name == filename).FirstOrDefault();
            if (file == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void UploadFile(string name, string extension,string ImgResolution,bool resolution)
        {
            if(resolution)
            {
                Uow.FileRepository.Insert(new File() { FileName = name, Extension = extension,ImgResolution = ImgResolution });
            }
            else
            {
                Uow.FileRepository.Insert(new File() { FileName = name, Extension = extension });
            }
           
        }
        public void UploadFile(string name, string extension, string description)
        {
            Uow.InfoFileRepository.Insert(new InfoFiles() { Name = name, extension = extension, Desctiption = description });
        }
        public File GetFile(string name, string extension)
        {
            return Uow.FileRepository.Get(x => x.FileName == name && x.Extension == extension).FirstOrDefault();
        }
        public InfoFiles GetFile(string name, string extension, string description)
        {
            return Uow.InfoFileRepository.Get(x => x.Name == name && x.extension == extension).FirstOrDefault();
        }
        public IEnumerable<File> GetImages()
        {
            return Uow.FileRepository.Get(x => x.Extension == ".jpg" || x.Extension == ".png" || x.Extension == ".PNG" || x.Extension == ".jpeg" || x.Extension == ".JPEG" || x.Extension == ".svg" || x.Extension == ".gif").OrderByDescending(x=>x.Id);
        }
        public IEnumerable<File> GetImages(string Resolution)
        {
            return Uow.FileRepository.Get(x => (x.Extension == ".png" || x.Extension == ".PNG" || x.Extension == ".jpg" || x.Extension == ".jpeg" || x.Extension == ".JPEG" || x.Extension == ".svg" || x.Extension == ".gif")&& x.ImgResolution == Resolution).ToList();
        }
        public IEnumerable<InfoFiles> GetFiles()
        {
            return Uow.InfoFileRepository.Get().OrderByDescending(x=>x.Id);
        }
    }
}
