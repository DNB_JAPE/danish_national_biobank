﻿using Core.Models;
using DAL.Context;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Core.Operations
{
    public class InfoManagmentOperations : AbstractOperations
    {


        SlugGenerator slugGenerator = new SlugGenerator();
        #region ContactPage
        public IEnumerable<InfoTranslation> GetAllContactPage(int id)
        {
            var language = Uow.LanguageRepository.Get(z => z.Name == CultureInfo.CurrentCulture.Name).FirstOrDefault();
            var type = Uow.SectionTypesRepository.Get(x => x.Name == "Contact Page").FirstOrDefault();
            if (id > 0)
            {
                if (language == null)
                {
                    return Uow.InfoTranslationRepository.Query.Include(x => x.Info).Where(x => x.Info.Sections.SectionTypes.Id == type.Id && x.Info.SectionId == id);
                }
                return Uow.InfoTranslationRepository.Query.Include(x => x.Info).Where(x => x.Info.Sections.SectionTypes.Id == type.Id && x.Info.SectionId == id && x.Language.Id == language.Id);
            }
            if (language == null)
            {
                return Uow.InfoTranslationRepository.Query.Include(x => x.Info).Where(x => x.Info.Sections.SectionTypes.Id == type.Id);
            }
            return Uow.InfoTranslationRepository.Query.Include(x => x.Info).Where(x => x.Info.Sections.SectionTypes.Id == type.Id && x.Language.Id == language.Id);
        }
        public void CreateContactPage(ContactModel model)
        {
            InfoTranslation infoTranslation = new InfoTranslation();
            InfoTranslation infoTranslationdk = new InfoTranslation();
            Info info = new Info();

            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();

            info.SectionId = model.SectionId;
            info.Active = model.Published;
            info.Sort = Uow.InfoRepository.Query.Max(x => x.Sort);
            info.Published = model.Published;
            info.Important = model.Published;
            info.Slider = true;
            info.ConnectedGall = 1;
            info.ImportantType = 1;
            info.Date = DateTime.Now;
            info.Lat = model.Lat;
            info.Lng = model.Long;

            Uow.InfoRepository.Insert(info);
            string slug = "";
            if (model.Title != null)
            {
                slug = slugGenerator.GenerateSlug(model.Title);
            }
            else if(model.Titledk != null)
            {
                slug = slugGenerator.GenerateSlug(model.Titledk);
            }
            if (model.Address!=null && model.Title!=null)
            { 
                infoTranslation.Content = model.Text;
                infoTranslation.Description = model.Address;
                infoTranslation.Title = model.Title;
                infoTranslation.Url = model.Phone;
                infoTranslation.Thumbnail = model.Email;
                infoTranslation.Info = info;
                infoTranslation.Slug = slug;
                infoTranslation.Language = En;
                Uow.InfoTranslationRepository.Insert(infoTranslation);
            }
            if (model.Addressdk != null && model.Titledk != null)
            {
                infoTranslationdk.Content = model.Textdk;
                infoTranslationdk.Description = model.Addressdk;
                infoTranslationdk.Title = model.Titledk;
                infoTranslationdk.Url = model.Phonedk;
                infoTranslationdk.Thumbnail = model.Emaildk;
                infoTranslationdk.Info = info;
                infoTranslationdk.Slug = slug;
                infoTranslationdk.Language = Dk;
                Uow.InfoTranslationRepository.Insert(infoTranslationdk);
            }
        }

        public ContactModel GetContactPageModel(int id)
        {
            ContactModel model = new ContactModel();
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();


            var modelTranslation = Uow.InfoTranslationRepository.Get(x => x.InfoId == id && x.Language.Id == En.Id).FirstOrDefault();
            var modelTranslationdk = Uow.InfoTranslationRepository.Get(x => x.InfoId == id && x.Language.Id == Dk.Id).FirstOrDefault();
            var infoModel = Uow.InfoRepository.Get(id);
            if (modelTranslation != null)
            {
                model.Title = modelTranslation.Title;
                model.Phone = modelTranslation.Url;
                model.Address = modelTranslation.Description;
                model.Text = modelTranslation.Content;
                model.Email = modelTranslation.Thumbnail;
            }
            if (modelTranslationdk != null)
            {
                model.Titledk = modelTranslationdk.Title;
                model.Phonedk = modelTranslationdk.Url;
                model.Addressdk = modelTranslationdk.Description;
                model.Textdk = modelTranslationdk.Content;
                model.Emaildk = modelTranslationdk.Thumbnail;
            }
            model.SectionId = infoModel.SectionId;
            model.Id = infoModel.Id;
            model.Published = infoModel.Published;
            model.Lat = infoModel.Lat;
            model.Long = infoModel.Lng;
            return model;
        }
        public ContactModel GetContactPageModel()
        {
            ContactModel model = new ContactModel();
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();

            var infoModel = Uow.InfoRepository.Get(x=>x.Sections.SectionTypes.Name== "Contact Page").FirstOrDefault();
            var modelTranslation = Uow.InfoTranslationRepository.Get(x => x.Info.Id == infoModel.Id && x.Language.Id == En.Id).FirstOrDefault();
            var modelTranslationdk = Uow.InfoTranslationRepository.Get(x => x.Info.Id == infoModel.Id && x.Language.Id == Dk.Id).FirstOrDefault();
            if (modelTranslation != null)
            {
                model.Title = modelTranslation.Title;
                model.Phone = modelTranslation.Url;
                model.Address = modelTranslation.Description;
                model.Text = modelTranslation.Content;
                model.Email = modelTranslation.Thumbnail;
            }
            if (modelTranslationdk != null)
            {
                model.Titledk = modelTranslationdk.Title;
                model.Phonedk = modelTranslationdk.Url;
                model.Addressdk = modelTranslationdk.Description;
                model.Textdk = modelTranslationdk.Content;
                model.Emaildk = modelTranslationdk.Thumbnail;
            }
            model.SectionId = infoModel.SectionId;
            model.Id = infoModel.Id;
            model.Published = infoModel.Published;
            model.Lat = infoModel.Lat;
            model.Long = infoModel.Lng;
            return model;
        }
        public void UpdateContactPage(ContactModel model)
        {
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            InfoTranslation infoTranslation = Uow.InfoTranslationRepository.Get(x=>x.InfoId == model.Id && x.Language.Id == En.Id).FirstOrDefault();
            InfoTranslation infoTranslationdk = Uow.InfoTranslationRepository.Get(x => x.InfoId == model.Id && x.Language.Id == Dk.Id).FirstOrDefault();
            Info info = Uow.InfoRepository.Get(model.Id);

            
            info.SectionId = model.SectionId;
            info.Active = model.Published;
            info.Sort = Uow.InfoRepository.Query.Max(x => x.Sort);
            info.Published = model.Published;
            info.Important = model.Published;
            info.ConnectedGall = 1;
            info.ImportantType = 1;
            info.Lat = model.Lat;
            info.Lng = model.Long;

            Uow.InfoRepository.Update(info);
            string slug = "";
            if (model.Address != null && model.Title != null && infoTranslation != null)
            {
                slug = infoTranslation.Slug;
                infoTranslation.Content = model.Text;
                infoTranslation.Description = model.Address;
                infoTranslation.Title = model.Title;
                infoTranslation.Url = model.Phone;
                infoTranslation.Thumbnail = model.Email;
                Uow.InfoTranslationRepository.Update(infoTranslation);
            }
            if (model.Addressdk != null && model.Titledk != null && infoTranslationdk != null)
            {
                if(string.IsNullOrEmpty(slug))
                {
                    slug = infoTranslationdk.Slug;
                }
                infoTranslationdk.Content = model.Textdk;
                infoTranslationdk.Description = model.Addressdk;
                infoTranslationdk.Title = model.Titledk;
                infoTranslationdk.Url = model.Phonedk;
                infoTranslationdk.Thumbnail = model.Emaildk;
                infoTranslationdk.Info = info;
                Uow.InfoTranslationRepository.Update(infoTranslationdk);
            }
            if (model.Address != null && model.Title != null && infoTranslation == null)
            {
                if (string.IsNullOrEmpty(slug))
                {
                    slug = slugGenerator.GenerateSlug(model.Title);
                }
                InfoTranslation infoTranlation = new InfoTranslation();
                infoTranlation.Content = model.Text;
                infoTranlation.Description = model.Address;
                infoTranlation.Title = model.Title;
                infoTranlation.Url = model.Phone;
                infoTranlation.Thumbnail = model.Email;
                infoTranlation.Info = info;
                infoTranlation.Slug = slug;
                infoTranlation.Language = Dk;
                Uow.InfoTranslationRepository.Insert(infoTranlation);
            }
            else
            {
                if (infoTranslation != null && model.Address == null && model.Title == null)
                {
                    Uow.InfoTranslationRepository.Delete(infoTranslation);
                }
            }
            if (model.Addressdk != null && model.Titledk != null && infoTranslationdk == null)
            {
                if (string.IsNullOrEmpty(slug))
                {
                    slug = slugGenerator.GenerateSlug(model.Titledk);
                }
                InfoTranslation infoTranlation = new InfoTranslation();
                infoTranlation.Content = model.Textdk;
                infoTranlation.Description = model.Addressdk;
                infoTranlation.Title = model.Titledk;
                infoTranlation.Url = model.Phonedk;
                infoTranlation.Thumbnail = model.Emaildk;
                infoTranlation.Info = info;
                infoTranlation.Slug = slug;
                infoTranlation.Language = Dk;
                Uow.InfoTranslationRepository.Insert(infoTranlation);
            }
            else
            {
                if(infoTranslationdk!=null && model.Addressdk == null && model.Titledk == null)
                {
                    Uow.InfoTranslationRepository.Delete(infoTranslationdk);
                }
            }
        }

        public void DeleteContactPage(int Id)
        {
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            InfoTranslation infoTranslation = Uow.InfoTranslationRepository.Get(x => x.InfoId == Id && x.Language.Id == En.Id).FirstOrDefault();
            InfoTranslation infoTranslationdk = Uow.InfoTranslationRepository.Get(x => x.InfoId == Id && x.Language.Id == Dk.Id).FirstOrDefault();
            Info info = Uow.InfoRepository.Get(Id);

            if(infoTranslation!=null)
            {
                Uow.InfoTranslationRepository.Delete(infoTranslation);
            }
            if (infoTranslationdk != null)
            {
                Uow.InfoTranslationRepository.Delete(infoTranslationdk);
            }
            Uow.InfoRepository.Delete(info);
        }
        #endregion
        #region Publications
        public IEnumerable<InfoTranslation> GetAllPublications(int id)
        {
            var language = Uow.LanguageRepository.Get(z => z.Name == CultureInfo.CurrentCulture.Name).FirstOrDefault();
            var type = Uow.SectionTypesRepository.Get(x => x.Name == "Publications").FirstOrDefault();
            if (id > 0)
            {
                if (language == null)
                {
                    return Uow.InfoTranslationRepository.Query.Include(x => x.Info).Where(x => x.Info.Sections.SectionTypes.Id == type.Id && x.Info.SectionId == id);
                }
                return Uow.InfoTranslationRepository.Query.Include(x => x.Info).Where(x => x.Info.Sections.SectionTypes.Id == type.Id && x.Info.SectionId == id && x.Language.Id == language.Id);
            }
            if (language == null)
            {
                return Uow.InfoTranslationRepository.Query.Include(x => x.Info).Where(x => x.Info.Sections.SectionTypes.Id == type.Id);
            }
            return Uow.InfoTranslationRepository.Query.Include(x => x.Info).Where(x => x.Info.Sections.SectionTypes.Id == type.Id && x.Language.Id == language.Id);

        }
        public void CreatePublication(PublicationModel model)
        {
            InfoTranslation infoTranslation = new InfoTranslation();
            InfoTranslation infoTranslationdk = new InfoTranslation();
            Info info = new Info();

            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();

            info.SectionId = model.Section;
            info.Date = DateTime.Now;
            info.Active = model.Published;
            info.Sort = Uow.InfoRepository.Query.Max(x => x.Sort);
            info.Published = model.Published;
            info.Important = model.Published;
            info.Slider = true;
            info.ConnectedGall = 1;
            info.ImportantType = 1;
            var infoFile = Uow.InfoFileRepository.Get(model.FileId);
            if (infoFile != null)
            {
                info.InfoFilesId = model.FileId;
            }
            Uow.InfoRepository.Insert(info);
            string slug = "";
            if (model.Title != null)
            {
                slug = slugGenerator.GenerateSlug(model.Title);
            }
            else if (model.Titledk != null)
            {
                slug = slugGenerator.GenerateSlug(model.Titledk);
            }

            infoTranslation.Title = model.Title;
            infoTranslation.Info = info;
            infoTranslation.Slug = slug;
            infoTranslation.FileId = (model.ImageId == 0) ? (long?)null : model.ImageId;
            infoTranslation.Language = En;
            infoTranslation.Url = model.URL;
            Uow.InfoTranslationRepository.Insert(infoTranslation);

            if (model.Titledk != null)
            {
                infoTranslationdk.Title = model.Titledk;
                infoTranslationdk.Info = info;
                infoTranslationdk.Slug = slug;
                infoTranslationdk.FileId = (model.ImageId == 0) ? (long?)null : model.ImageId;
                infoTranslationdk.Language = Dk;
                infoTranslationdk.Url = model.URLdk;
                Uow.InfoTranslationRepository.Insert(infoTranslationdk);
            }

            //var InfoFiles = model.FileId.Distinct();
            //foreach(var item in InfoFiles)
            //{
            //    Uow.InfoToInfoFilesRepository.Insert(new InfoToInfoFiles() { InfoFileId = item, Info = info });
            //}
        }

        public PublicationModel GetPublicationsModel(int id)
        {
            PublicationModel model = new PublicationModel();
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();


            var modelTranslation = Uow.InfoTranslationRepository.Get(x => x.InfoId == id && x.Language.Id == En.Id).FirstOrDefault();
            var modelTranslationdk = Uow.InfoTranslationRepository.Get(x => x.InfoId == id && x.Language.Id == Dk.Id).FirstOrDefault();
            var infoModel = Uow.InfoRepository.Get(id);
            if (modelTranslation != null)
            {
                model.Title = modelTranslation.Title;
                model.URL = modelTranslation.Url;
                model.ImageId = (modelTranslation.FileId == null) ? 0 : (long)modelTranslation.FileId;
            }
            if (modelTranslationdk != null)
            {
                model.Titledk = modelTranslationdk.Title;
                model.URLdk = modelTranslationdk.Url;
            }
            model.Section = infoModel.SectionId;
            model.Date = infoModel.Date;
            model.Id = infoModel.Id;
            model.Published = infoModel.Published;
            model.FileId = (infoModel.InfoFilesId==null)?0:(int)infoModel.InfoFilesId;
            return model;
        }
        public void UpdatePublication(PublicationModel model)
        {
            var InfoModel = Uow.InfoRepository.Get(model.Id);
            var infoFile = Uow.InfoFileRepository.Get(model.FileId);
            if(infoFile!=null)
            {
                InfoModel.InfoFilesId = model.FileId;
            }
            InfoModel.Published = model.Published;
            InfoModel.SectionId = model.Section;
            InfoModel.Date = model.Date;
            InfoModel.StartDate = model.Date;
            Uow.InfoRepository.Update(InfoModel);

            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            var textTranslation = Uow.InfoTranslationRepository.Get(x => x.InfoId == model.Id && x.Language.Id == En.Id).FirstOrDefault();
            var textTranslationdk = Uow.InfoTranslationRepository.Get(x => x.InfoId == model.Id && x.Language.Id == Dk.Id).FirstOrDefault();
            string slug = "";
            if (model.Title != null && textTranslation != null)
            {
                slug = textTranslation.Slug;
                textTranslation.Title = model.Title;
                textTranslation.FileId = (model.ImageId == 0) ? (long?)null : model.ImageId;
                textTranslation.Url = model.URL;
                Uow.InfoTranslationRepository.Update(textTranslation);
            }
            if (textTranslationdk != null && model.Titledk != null)
            {
                slug = textTranslation.Slug;
                textTranslationdk.Title = model.Titledk;
                textTranslationdk.FileId = (model.ImageId == 0) ? (long?)null : model.ImageId;
                textTranslationdk.Url = model.URLdk;
                Uow.InfoTranslationRepository.Update(textTranslation);
            }
            if(model.Title!=null && textTranslation == null)
            {
                if(string.IsNullOrEmpty(slug))
                {
                    slug = slugGenerator.GenerateSlug(model.Title);
                }
                InfoTranslation newTextTranslation = new InfoTranslation();
                newTextTranslation.Title = model.Title;
                newTextTranslation.Info = InfoModel;
                newTextTranslation.Slug = slug;
                newTextTranslation.FileId = (model.ImageId == 0) ? (long?)null : model.ImageId;
                newTextTranslation.Language = En;
                Uow.InfoTranslationRepository.Insert(newTextTranslation);
            }
            if (model.Titledk != null && textTranslationdk == null)
            {
                if (string.IsNullOrEmpty(slug))
                {
                    slug = slugGenerator.GenerateSlug(model.Titledk);
                }
                InfoTranslation newTextTranslation = new InfoTranslation();
                newTextTranslation.Title = model.Titledk;
                newTextTranslation.Info = InfoModel;
                newTextTranslation.Slug = slug;
                newTextTranslation.FileId = (model.ImageId == 0) ? (long?)null : model.ImageId;
                newTextTranslation.Language = Dk;
                Uow.InfoTranslationRepository.Insert(newTextTranslation);
            }
            if(model.Titledk == null && textTranslationdk != null)
            {
                Uow.InfoTranslationRepository.Delete(textTranslationdk);
            }
            if (model.Title == null && textTranslation != null)
            {
                Uow.InfoTranslationRepository.Delete(textTranslation);
            }
        }
        public void DeletePublication(int id)
        {
            var InfoModel = Uow.InfoRepository.Get(id);

            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();

            var textTranslation = Uow.InfoTranslationRepository.Get(x => x.InfoId == id && x.Language.Id == En.Id).FirstOrDefault();
            var textTranslationdk = Uow.InfoTranslationRepository.Get(x => x.InfoId == id && x.Language.Id == Dk.Id).FirstOrDefault();
            if (textTranslation != null)
            {
                Uow.InfoTranslationRepository.Delete(textTranslation);
            }
            if (textTranslationdk != null)
            {
                Uow.InfoTranslationRepository.Delete(textTranslationdk);
            }
            var infoToInfoFiles = Uow.InfoToInfoFilesRepository.Get(x => x.Info.Id == InfoModel.Id);
            foreach(var item in infoToInfoFiles)
            {
                Uow.InfoToInfoFilesRepository.Delete(item);
            }
            Uow.InfoRepository.Delete(InfoModel);
        }

        #endregion
        #region PostPage
        public IEnumerable<Settings> getSettingForPostPage()
        {
            return Uow.SettingsRepository.Get(x => x.Slug == "important_post_type");
        }
        public int GetTypeValueBySettingsId(int id)
        {
            return Convert.ToInt32(Uow.SettingsRepository.Get(id).Value);
        }
        public IEnumerable<InfoTranslation> GetAllPostPage(int id)
        {
            var language = Uow.LanguageRepository.Get(z => z.Name == CultureInfo.CurrentCulture.Name).FirstOrDefault();
            var type = Uow.SectionTypesRepository.Get(x => x.Name == "Post").FirstOrDefault();
            if(id>0)
            {
                if (language == null)
                {
                    return Uow.InfoTranslationRepository.Query.Include(x => x.Info).Where(x => x.Info.Sections.SectionTypes.Id == type.Id && x.Info.ImportantType == id);
                }
                return Uow.InfoTranslationRepository.Query.Include(x => x.Info).Where(x => x.Info.Sections.SectionTypes.Id == type.Id && x.Info.ImportantType == id &&x.Language.Id == language.Id);
            }
            if (language == null)
            {
                return Uow.InfoTranslationRepository.Query.Include(x => x.Info).Where(x => x.Info.Sections.SectionTypes.Id == type.Id);
            }
            return Uow.InfoTranslationRepository.Query.Include(x => x.Info).Where(x => x.Info.Sections.SectionTypes.Id == type.Id && x.Language.Id == language.Id);
        }


        public void CreatePostPage(PostPageModel model)
        {
            InfoTranslation infoTranslation = new InfoTranslation();
            InfoTranslation infoTranslationdk = new InfoTranslation();
            Info info = new Info();

            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();

            info.SectionId = model.Section;
            info.Date = model.Date;
            info.Active = model.Published;
            info.Sort = Uow.InfoRepository.Query.Max(x => x.Sort);
            info.Published = model.Published;
            info.Important = model.Published;
            info.ImportantType = model.DislplayIn;
            info.StartDate = model.Date;
            info.Slider = true;
            info.ConnectedGall = 1;

            Uow.InfoRepository.Insert(info);

            string slug = "";
            if (model.Title != null)
            {
                slug = slugGenerator.GenerateSlug(model.Title);
            }
            if (model.Titledk != null)
            {
                slug = slugGenerator.GenerateSlug(model.Title);
            }
            infoTranslation.Content = model.Content;
            infoTranslation.Description = model.Description;
            infoTranslation.Title = model.Title;
            infoTranslation.Info = info;
            infoTranslation.Slug = slug;
            infoTranslation.FileId = (model.FileId == 0) ? (long?)null : model.FileId;
            infoTranslation.Language = En;
            Uow.InfoTranslationRepository.Insert(infoTranslation);
            
            if (model.Titledk != null)
            {
                infoTranslationdk.Content = model.Contentdk;
                infoTranslationdk.Description = model.Descriptiondk;
                infoTranslationdk.Title = model.Titledk;
                infoTranslationdk.Info = info;
                infoTranslationdk.Slug = slug;
                infoTranslationdk.FileId = (model.FileId == 0) ? (long?)null : model.FileId;
                infoTranslationdk.Language = Dk;
                Uow.InfoTranslationRepository.Insert(infoTranslationdk);
            }
        }
        public PostPageModel GetPostPageModel(int id)
        {
            PostPageModel model = new PostPageModel();
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();

            var modelTranslation = Uow.InfoTranslationRepository.Get(x => x.InfoId == id && x.Language.Id == En.Id).FirstOrDefault();
            var modelTranslationdk = Uow.InfoTranslationRepository.Get(x => x.InfoId == id && x.Language.Id == Dk.Id).FirstOrDefault();
            var infoModel = Uow.InfoRepository.Get(id);
            if (modelTranslation != null)
            {
                model.Content = modelTranslation.Content;
                model.Description = modelTranslation.Description;
                model.Title = modelTranslation.Title;
                model.FileId = (modelTranslation.FileId == null) ? 0 : (long)modelTranslation.FileId;
            }
            if (modelTranslationdk != null)
            {
                model.Titledk = modelTranslationdk.Title;
                model.Descriptiondk = modelTranslationdk.Description;
                model.Contentdk = modelTranslationdk.Content;
            }
            model.Section = infoModel.SectionId;
            model.Date = infoModel.Date;
            model.Id = infoModel.Id;
            model.Published = infoModel.Published;
            model.DislplayIn = infoModel.ImportantType;
            return model;
        }

        public void UpdatePostPage(PostPageModel model)
        {
            var InfoModel = Uow.InfoRepository.Get(model.Id);
            InfoModel.Published = model.Published;
            InfoModel.SectionId = model.Section;
            InfoModel.Date = model.Date;
            InfoModel.StartDate = model.Date;
            Uow.InfoRepository.Update(InfoModel);

            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();

            var textTranslation = Uow.InfoTranslationRepository.Get(x => x.InfoId == model.Id && x.Language.Id == En.Id).FirstOrDefault();
            var textTranslationdk = Uow.InfoTranslationRepository.Get(x => x.InfoId == model.Id && x.Language.Id == Dk.Id).FirstOrDefault();
            string slug = "";
            if (textTranslation != null && model.Title!=null)
            {
                slug = textTranslation.Slug;
                textTranslation.Title = model.Title;
                textTranslation.Description = model.Description;
                textTranslation.Content = model.Content;
                textTranslation.FileId = (model.FileId == 0) ? (long?)null : model.FileId;
                Uow.InfoTranslationRepository.Update(textTranslation);
            }
            if (textTranslationdk != null && model.Titledk != null)
            {
                if(string.IsNullOrEmpty(slug))
                {
                    slug = textTranslation.Slug;
                }
                textTranslationdk.Title = model.Titledk;
                textTranslationdk.Description = model.Descriptiondk;
                textTranslationdk.Content = model.Contentdk;
                textTranslationdk.FileId = (model.FileId == 0) ? (long?)null : model.FileId;
                Uow.InfoTranslationRepository.Update(textTranslation);
            }
            if (textTranslation == null && model.Title != null)
            {
                if (string.IsNullOrEmpty(slug))
                {
                    slug = slugGenerator.GenerateSlug(model.Title);
                }
                InfoTranslation newTextTranslation = new InfoTranslation();
                newTextTranslation.Content = model.Content;
                newTextTranslation.Description = model.Description;
                newTextTranslation.Title = model.Title;
                newTextTranslation.Info = InfoModel;
                newTextTranslation.Slug = slug;
                newTextTranslation.FileId = (model.FileId == 0) ? (long?)null : model.FileId;
                newTextTranslation.Language = En;
                Uow.InfoTranslationRepository.Insert(newTextTranslation);
            }
            else
            {
                if(textTranslation != null && model.Title == null)
                {
                    Uow.InfoTranslationRepository.Delete(textTranslation);
                }
            }
            if (textTranslationdk == null && model.Titledk != null)
            {
                if (string.IsNullOrEmpty(slug))
                {
                    slug = slugGenerator.GenerateSlug(model.Titledk);
                }
                InfoTranslation newTextTranslation = new InfoTranslation();
                newTextTranslation.Content = model.Contentdk;
                newTextTranslation.Description = model.Descriptiondk;
                newTextTranslation.Title = model.Titledk;
                newTextTranslation.Info = InfoModel;
                newTextTranslation.Slug = slug;
                newTextTranslation.FileId = (model.FileId == 0) ? (long?)null : model.FileId;
                newTextTranslation.Language = Dk;
                Uow.InfoTranslationRepository.Insert(newTextTranslation);
            }
            else
            {
                if (textTranslationdk != null && model.Titledk == null)
                {
                    Uow.InfoTranslationRepository.Delete(textTranslationdk);
                }
            }
        }
        public void DeletePostPage(int id)
        {
            var InfoModel = Uow.InfoRepository.Get(id);

            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();

            var textTranslation = Uow.InfoTranslationRepository.Get(x => x.InfoId == id && x.Language.Id == En.Id).FirstOrDefault();
            var textTranslationdk = Uow.InfoTranslationRepository.Get(x => x.InfoId == id && x.Language.Id == Dk.Id).FirstOrDefault();
            if (textTranslation != null)
            {
                Uow.InfoTranslationRepository.Delete(textTranslation);
            }
            if (textTranslationdk != null)
            {
                Uow.InfoTranslationRepository.Delete(textTranslationdk);
            }
            Uow.InfoRepository.Delete(InfoModel);
        }

        #endregion
        #region TextPage
        public IEnumerable<InfoTranslation> GetAllTextPage(int id)
        {
            var language = Uow.LanguageRepository.Get(z => z.Name == CultureInfo.CurrentCulture.Name).FirstOrDefault();
            var type = Uow.SectionTypesRepository.Get(x => x.Name == "Text Page").FirstOrDefault();
            if (id > 0)
            {
                if (language == null)
                {
                    return Uow.InfoTranslationRepository.Query.Include(x => x.Info).Where(x => x.Info.Sections.SectionTypes.Id == type.Id && x.Info.SectionId == id);
                }
                return Uow.InfoTranslationRepository.Query.Include(x => x.Info).Where(x => x.Info.Sections.SectionTypes.Id == type.Id && x.Info.SectionId == id && x.Language.Id == language.Id);
            }
            if (language == null)
            {
                return Uow.InfoTranslationRepository.Query.Include(x => x.Info).Where(x => x.Info.Sections.SectionTypes.Id == type.Id);
            }
            return Uow.InfoTranslationRepository.Query.Include(x => x.Info).Where(x => x.Info.Sections.SectionTypes.Id == type.Id && x.Language.Id == language.Id);

        }
        public bool checkTextPageSection()
        {
            var section = Uow.SectionsRepository.Get(x => x.SectionTypes.Name == "Text Page").FirstOrDefault();
            if (section == null)
            {
                return false;
            }
            return true;
        }
        public void CreateTextPage(TextPageModel model)
        {
            InfoTranslation infoTranslation = new InfoTranslation();
            InfoTranslation infoTranslationdk = new InfoTranslation();
            Info info = new Info();

            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();

            info.SectionId = model.Section;
            info.Date = DateTime.Now;
            info.Active = model.Published;
            info.Sort = Uow.InfoRepository.Query.Max(x => x.Sort);
            info.Published = model.Published;
            info.Important = model.Published;
            info.Slider = true;
            info.ConnectedGall = 1;
            info.ImportantType = 1;

            Uow.InfoRepository.Insert(info);

            string slug = "";
            if (model.Title != null)
            {
                slug = slugGenerator.GenerateSlug(model.Title);
                infoTranslation.Content = model.Content;
                infoTranslation.Description = model.Description;
                infoTranslation.Title = model.Title;
                infoTranslation.Info = info;
                infoTranslation.Slug = slug;
                infoTranslation.FileId = (model.FileId == 0) ? (long?)null : model.FileId;
                infoTranslation.Language = En;
                Uow.InfoTranslationRepository.Insert(infoTranslation);
            }
            if (model.Titledk != null)
            {
                if(string.IsNullOrEmpty(slug))
                {
                    slug = slugGenerator.GenerateSlug(model.Titledk);
                }
                slug = slugGenerator.GenerateSlug(model.Titledk);
                infoTranslationdk.Content = model.Contentdk;
                infoTranslationdk.Description = model.Descriptiondk;
                infoTranslationdk.Title = model.Titledk;
                infoTranslationdk.Info = info;
                infoTranslationdk.Slug = slug;
                infoTranslationdk.FileId = (model.FileId == 0) ? (long?)null : model.FileId;
                infoTranslationdk.Language = Dk;
                Uow.InfoTranslationRepository.Insert(infoTranslationdk);
            }
        }
        public TextPageModel GetTextPageModel(int id)
        {
            TextPageModel model = new TextPageModel();
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();

            var modelTranslation = Uow.InfoTranslationRepository.Get(x => x.InfoId == id && x.Language.Id == En.Id).FirstOrDefault();
            var modelTranslationdk = Uow.InfoTranslationRepository.Get(x => x.InfoId == id && x.Language.Id == Dk.Id).FirstOrDefault();
            var infoModel = Uow.InfoRepository.Get(id);
            if (modelTranslation!=null)
            {
                model.Content = modelTranslation.Content;
                model.Description = modelTranslation.Description;
                model.Title = modelTranslation.Title;
                model.FileId = (modelTranslation.FileId==null)?0:(long)modelTranslation.FileId;
            }
            if(modelTranslationdk!=null)
            {
                model.Titledk = modelTranslationdk.Title;
                model.Descriptiondk = modelTranslationdk.Description;
                model.Contentdk = modelTranslationdk.Content;
            }
            model.Section = infoModel.SectionId;
            model.Id = infoModel.Id;
            model.Published = infoModel.Published;
            return model;
        }
        public void UpdateTextPage(TextPageModel model)
        {
            var InfoModel =  Uow.InfoRepository.Get(model.Id);
            InfoModel.Published = model.Published;
            InfoModel.SectionId = model.Section;
            Uow.InfoRepository.Update(InfoModel);

            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();

            var textTranslation = Uow.InfoTranslationRepository.Get(x => x.InfoId == model.Id && x.Language.Id == En.Id).FirstOrDefault();
            var textTranslationdk = Uow.InfoTranslationRepository.Get(x => x.InfoId == model.Id && x.Language.Id == Dk.Id).FirstOrDefault();
            string slug = "";
            if (textTranslation!=null)
            {
                if(model.Title!=null)
                {
                    slug = textTranslation.Slug;
                    textTranslation.Title = model.Title;
                    textTranslation.Description = model.Description;
                    textTranslation.Content = model.Content;
                    textTranslation.FileId = (model.FileId==0)?(long?)null:model.FileId;
                    Uow.InfoTranslationRepository.Update(textTranslation);
                }
                else
                {
                    Uow.InfoTranslationRepository.Delete(textTranslation);
                }

            }
            if (textTranslationdk != null)
            {
                if (model.Titledk != null)
                {
                    if(string.IsNullOrEmpty(slug))
                    {
                        slug = textTranslationdk.Slug;
                    }
                    textTranslationdk.Title = model.Titledk;
                    textTranslationdk.Description = model.Descriptiondk;
                    textTranslationdk.Content = model.Contentdk;
                    textTranslationdk.FileId = (model.FileId == 0) ? (long?)null : model.FileId;
                    Uow.InfoTranslationRepository.Update(textTranslationdk);
                }
                else
                {
                    Uow.InfoTranslationRepository.Delete(textTranslationdk);
                }

            }
            if (textTranslation == null && model.Title != null)
            {
                if (string.IsNullOrEmpty(slug))
                {
                    slug = slugGenerator.GenerateSlug(model.Titledk);
                }
                InfoTranslation newTextTranslation = new InfoTranslation();
                newTextTranslation.Content = model.Content;
                newTextTranslation.Description = model.Description;
                newTextTranslation.Title = model.Title;
                newTextTranslation.Info = InfoModel;
                newTextTranslation.Slug = slug;
                newTextTranslation.FileId = (model.FileId == 0) ? (long?)null : model.FileId;
                newTextTranslation.Language = Dk;
                Uow.InfoTranslationRepository.Insert(newTextTranslation);
            }
            if (textTranslationdk ==  null && model.Titledk != null)
            {
                if (string.IsNullOrEmpty(slug))
                {
                    slug = slugGenerator.GenerateSlug(model.Titledk);
                }
                InfoTranslation newTextTranslation = new InfoTranslation();
                newTextTranslation.Content = model.Contentdk;
                newTextTranslation.Description = model.Descriptiondk;
                newTextTranslation.Title = model.Titledk;
                newTextTranslation.Info = InfoModel;
                newTextTranslation.Slug = slug;
                newTextTranslation.FileId = (model.FileId == 0) ? (long?)null : model.FileId;
                newTextTranslation.Language = Dk;
                Uow.InfoTranslationRepository.Insert(newTextTranslation);
            }
        }
        public bool DeleteTextPage(int id)
        {
            bool infoAdditional = Uow.InfoAdditionalRepository.Get().Where(i => i.InfoId == id).Any();
            if(infoAdditional)
            {
                return false;
            }
    
            var InfoModel = Uow.InfoRepository.Get(id);

            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();

            var textTranslation = Uow.InfoTranslationRepository.Get(x => x.InfoId == id && x.Language.Id == En.Id).FirstOrDefault();
            var textTranslationdk = Uow.InfoTranslationRepository.Get(x => x.InfoId == id && x.Language.Id == Dk.Id).FirstOrDefault();
            if (textTranslation != null)
            {
                Uow.InfoTranslationRepository.Delete(textTranslation);
            }
            if (textTranslationdk != null)
            {
                Uow.InfoTranslationRepository.Delete(textTranslationdk);
            }
            Uow.InfoRepository.Delete(InfoModel);
            return true;
        }
    }
    #endregion


        
}
