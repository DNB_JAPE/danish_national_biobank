﻿using Core.Models;
using DAL.Context;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Core.Operations
{
    public class ChronicDiseaseoperations : AbstractOperations
    {
        public IEnumerable<DiseaseTranslation> GetDisease()
        {
            var language = Uow.LanguageRepository.Get(z => z.Name == CultureInfo.CurrentCulture.Name).FirstOrDefault();
            if(language!=null)
            {
                return Uow.DiseaseTranslationRepository.Get(x => x.Language.Id == language.Id);
            }
            return Uow.DiseaseTranslationRepository.GetAll();

        }
        public void CreateDisease(DiseaseModel model)
        {
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            DiseaseTranslation diseaseTranslation;
            Disease disease = new Disease();

            disease.Active = model.Active;
            Uow.DiseaseRepository.Insert(disease);

            if(model.Title!=null)
            {
                diseaseTranslation = new DiseaseTranslation();
                diseaseTranslation.Title = model.Title;
                diseaseTranslation.Disease = disease;
                diseaseTranslation.Language = En;
                Uow.DiseaseTranslationRepository.Insert(diseaseTranslation);
            }
            if (model.Title != null)
            {
                diseaseTranslation = new DiseaseTranslation();
                diseaseTranslation.Title = model.Title;
                diseaseTranslation.Disease = disease;
                diseaseTranslation.Language = Dk;
                Uow.DiseaseTranslationRepository.Insert(diseaseTranslation);
            }
        }
        public DiseaseModel GetDiseaseModel(int Id)
        {
            DiseaseModel model = new DiseaseModel();
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            var disease = Uow.DiseaseRepository.Get(Id);

            var diseaseTranslation = Uow.DiseaseTranslationRepository.Get(x => x.Disease.Id == disease.Id && x.Language.Id == En.Id).FirstOrDefault();
            var diseaseTranslationdk = Uow.DiseaseTranslationRepository.Get(x => x.Disease.Id == disease.Id && x.Language.Id == Dk.Id).FirstOrDefault();
            model.Active = disease.Active;
            model.Id = disease.Id;
            if(diseaseTranslation!=null)
            {
                model.Title = diseaseTranslation.Title;
            }
            if (diseaseTranslationdk != null)
            {
                model.Titledk = diseaseTranslationdk.Title;
            }
            return model;
        }
        public void UpdateDisease(DiseaseModel model)
        {
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            var disease = Uow.DiseaseRepository.Get(model.Id);
            var diseaseTranslation = Uow.DiseaseTranslationRepository.Get(x => x.Disease.Id == disease.Id && x.Language.Id == En.Id).FirstOrDefault();
            var diseaseTranslationdk = Uow.DiseaseTranslationRepository.Get(x => x.Disease.Id == disease.Id && x.Language.Id == Dk.Id).FirstOrDefault();


            disease.Active = model.Active;
            Uow.DiseaseRepository.Update(disease);

            if(model.Title!=null)
            {
                if(diseaseTranslation!=null)
                {
                    diseaseTranslation.Title = model.Title;
                    Uow.DiseaseTranslationRepository.Update(diseaseTranslation);
                }
                else
                {
                    diseaseTranslation = new DiseaseTranslation();
                    diseaseTranslation.Title = model.Title;
                    diseaseTranslation.Language = En;
                    diseaseTranslation.Disease = disease;
                    Uow.DiseaseTranslationRepository.Insert(diseaseTranslation);
                }
            }
            else
            {
                if(diseaseTranslation!=null)
                {
                    Uow.DiseaseTranslationRepository.Delete(diseaseTranslation);
                }
            }
            if (model.Titledk != null)
            {
                if (diseaseTranslationdk != null)
                {
                    diseaseTranslationdk.Title = model.Titledk;
                    Uow.DiseaseTranslationRepository.Update(diseaseTranslationdk);
                }
                else
                {
                    diseaseTranslationdk = new DiseaseTranslation();
                    diseaseTranslationdk.Title = model.Titledk;
                    diseaseTranslationdk.Language = Dk;
                    diseaseTranslationdk.Disease = disease;
                    Uow.DiseaseTranslationRepository.Insert(diseaseTranslationdk);
                }
            }
            else
            {
                if (diseaseTranslationdk != null)
                {
                    Uow.DiseaseTranslationRepository.Delete(diseaseTranslationdk);
                }
            }
        }
        public bool DeleteDisease(int Id)
        {
            bool user = Uow.UserDiseaseRepository.Get(x => x.DiseaseId == Id).Any();
            if (user)
            {
                return false;
            }else
            {
                var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
                var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
                var disease = Uow.DiseaseRepository.Get(Id);
                var diseaseTranslation = Uow.DiseaseTranslationRepository.Get(x => x.Disease.Id == disease.Id && x.Language.Id == En.Id).FirstOrDefault();
                var diseaseTranslationdk = Uow.DiseaseTranslationRepository.Get(x => x.Disease.Id == disease.Id && x.Language.Id == Dk.Id).FirstOrDefault();

                if (diseaseTranslation != null)
                {
                    Uow.DiseaseTranslationRepository.Delete(diseaseTranslation);
                }
                if (diseaseTranslationdk != null)
                {
                    Uow.DiseaseTranslationRepository.Delete(diseaseTranslationdk);
                }
                Uow.DiseaseRepository.Delete(disease);
                return true;
            }
           
        }
    }
}
