﻿using Core.Models;
using DAL.Context;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Core.Operations
{
    public class GalleyOperations:AbstractOperations
    {
        public IEnumerable<GalleryGroupTranslate> GetAll(int id)
        {
            var language = Uow.LanguageRepository.Get(z => z.Name == CultureInfo.CurrentCulture.Name).FirstOrDefault();
            var type = Uow.SectionTypesRepository.Get(x => x.Name == "Photo Gallery Page").FirstOrDefault();
            if (id > 0)
            {
                if (language == null)
                {
                    return Uow.GalleryGroupTranslateRepository.Query.Include(x => x.GalleryGroup).Where(x => x.GalleryGroup.Sections.SectionTypes.Id == type.Id && x.GalleryGroup.SectionId == id);
                }
                return Uow.GalleryGroupTranslateRepository.Query.Include(x => x.GalleryGroup).Where(x => x.GalleryGroup.Sections.SectionTypes.Id == type.Id && x.GalleryGroup.SectionId == id && x.Language.Id == language.Id);
            }
            else if (language == null)
            {
                return Uow.GalleryGroupTranslateRepository.Query.Include(x => x.GalleryGroup).Where(x => x.GalleryGroup.Sections.SectionTypes.Id == type.Id);
            }
            else
            {
                return Uow.GalleryGroupTranslateRepository.Query.Include(x => x.GalleryGroup).Where(x => x.GalleryGroup.Sections.SectionTypes.Id == type.Id && x.Language.Id == language.Id);
            }
        }

        public bool checkFileName(string name)
        {
            var gallery = Uow.GalleryRepository.Get(x => x.File.FileName == name).FirstOrDefault();
            if(gallery==null)
            {
                return false;
            }
            return true;
        }


        public void CreateGallery(GalleryModel model)
        {
            GalleryGroupTranslate galleryGroupTranslate = new GalleryGroupTranslate();
            GalleryGroupTranslate galleryGroupTranslatedk = new GalleryGroupTranslate();
            GalleryGroup galleryGroup = new GalleryGroup();
            List<Gallery> galleryList = new List<Gallery>();
            List<GalleryTranslation> GalleryTranslationList = new List<GalleryTranslation>();
            Gallery gallery;
            GalleryTranslation galleryTranslation;


            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();

            galleryGroup.SectionId = model.Section;
            galleryGroup.Date = model.Date;
            galleryGroup.Active = model.Published;
            galleryGroup.Sort = 1;

            Uow.GalleryGroupRepository.Insert(galleryGroup);

            if(model.Title!=null)
            {
                galleryGroupTranslate.Language = En;
                galleryGroupTranslate.Title = model.Title;
                galleryGroupTranslate.Description = model.Description;
                galleryGroupTranslate.GalleryGroup = galleryGroup;
                galleryGroupTranslate.thumbnail = model.CoverImage;
                Uow.GalleryGroupTranslateRepository.Insert(galleryGroupTranslate);
            }
            if(model.Titledk != null)
            {
                galleryGroupTranslatedk.Language = Dk;
                galleryGroupTranslatedk.Title = model.Titledk;
                galleryGroupTranslatedk.Description = model.Descriptiondk;
                galleryGroupTranslatedk.GalleryGroup = galleryGroup;
                galleryGroupTranslatedk.thumbnail = model.CoverImagedk;
                Uow.GalleryGroupTranslateRepository.Insert(galleryGroupTranslatedk);
            }
            if (model.GelleryImages != null)
            {
                for (int i = 0; i < model.GelleryImages.Count(); i++)
                {
                    gallery = new Gallery();
                    galleryTranslation = new GalleryTranslation();
                    gallery.GalleryGroup = galleryGroup;
                    gallery.Top = true;
                    gallery.Display = false;
                    if (model.GelleryImagesPublished.Contains(model.GelleryImages[i]))
                    {
                        gallery.Display = true;
                    }
                    gallery.Date = model.GelleryImagesDate[i];
                    gallery.FileId = model.GelleryImages[i];

                    Uow.GalleryRepository.Insert(gallery);

                    galleryTranslation.GalleryId = gallery.Id;
                    galleryTranslation.Language = En;

                    Uow.GalleryTranslationRepository.Insert(galleryTranslation);
                }
            }
        }
        public GalleryModel GetGalleryModel(int id)
        {
            GalleryModel model = new GalleryModel();
            List<long> galleryImages = new List<long>();
            List<DateTime> galleryImagesDate = new List<DateTime>();
            List<long> galleryImagesPublished = new List<long>();
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            var galleryGroup = Uow.GalleryGroupRepository.Get(id);
            var galleryTranslation = Uow.GalleryGroupTranslateRepository.Get(x => x.GalleryGroup.Id == galleryGroup.Id && x.Language.Id == En.Id).FirstOrDefault();
            var galleryTranslationdk = Uow.GalleryGroupTranslateRepository.Get(x => x.GalleryGroup.Id == galleryGroup.Id && x.Language.Id == Dk.Id).FirstOrDefault();
            var gallery = Uow.GalleryRepository.Get(x => x.GalleryGroup.Id == galleryGroup.Id).AsEnumerable();

            

            model.Id = galleryGroup.Id;
            model.Section = galleryGroup.SectionId;
            model.Published = galleryGroup.Active;
            model.Date = galleryGroup.Date;
           if(galleryTranslation!=null)
           {
                model.Title = galleryTranslation.Title;
                model.Description = galleryTranslation.Description;
                model.CoverImage = galleryTranslation.thumbnail;
           }
           if(galleryTranslationdk!=null)
            {
                model.Titledk = galleryTranslationdk.Title;
                model.Descriptiondk = galleryTranslationdk.Description;
                model.CoverImagedk = galleryTranslationdk.thumbnail;
            }
           if(gallery!=null)
            {
                foreach(var item in gallery)
                {
                    galleryImages.Add((long)item.FileId);
                    galleryImagesDate.Add(item.Date);
                    if(item.Display==true)
                    {
                        galleryImagesPublished.Add((long)item.FileId);
                    }
                }
                model.GelleryImages = galleryImages;
                model.GelleryImagesDate = galleryImagesDate;
                model.GelleryImagesPublished = galleryImagesPublished;
            }
            

            return model;
        }


        public void UpdateGallery(GalleryModel model)
        {
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();

            GalleryGroupTranslate galleryGroupTranslate = Uow.GalleryGroupTranslateRepository.Get(x=>x.GalleryGroupId==model.Id && x.Language.Id == En.Id).FirstOrDefault();
            GalleryGroupTranslate galleryGroupTranslatedk = Uow.GalleryGroupTranslateRepository.Get(x => x.GalleryGroupId == model.Id && x.Language.Id == Dk.Id).FirstOrDefault();
            GalleryGroup galleryGroup = Uow.GalleryGroupRepository.Get(model.Id);
            List<Gallery> galleryList = new List<Gallery>();
            List<GalleryTranslation> GalleryTranslationList = new List<GalleryTranslation>();
            Gallery gallery;
            GalleryTranslation galleryTranslation;
            var existedGallery = Uow.GalleryRepository.Get(x => x.GalleryGroup.Id == galleryGroup.Id);
            //     var existedGalleryTranslation = Uow.GalleryTranslationRepository.Get(x => x.Gallery == existedGallery).AsEnumerable();
            foreach (var item in existedGallery)
            {
                var existedGalleryTranslation = Uow.GalleryTranslationRepository.Get(x => x.Gallery.Id == item.Id).AsEnumerable();
                foreach (var item1 in existedGalleryTranslation)
                {
                    Uow.GalleryTranslationRepository.Delete(item1);
                }
                Uow.GalleryRepository.Delete(item);

            }


            galleryGroup.SectionId = model.Section;
            galleryGroup.Date = model.Date;
            galleryGroup.Active = model.Published;
            galleryGroup.Sort = 1;//Uow.GalleryGroupRepository.Get().Max(x => x.Sort);

            Uow.GalleryGroupRepository.Update(galleryGroup);
            if(galleryGroupTranslate==null)
            {
                if (model.Title != null)
                {
                    galleryGroupTranslate = new GalleryGroupTranslate();
                    galleryGroupTranslate.Language = En;
                    galleryGroupTranslate.Title = model.Title;
                    galleryGroupTranslate.Description = model.Description;
                    galleryGroupTranslate.GalleryGroup = galleryGroup;
                    galleryGroupTranslate.thumbnail = model.CoverImage;
                    Uow.GalleryGroupTranslateRepository.Insert(galleryGroupTranslate);
                }
            }
            else
            {
                if(model.Title==null)
                {
                    Uow.GalleryGroupTranslateRepository.Delete(galleryGroupTranslate);
                }
                else
                {
                    galleryGroupTranslate.thumbnail = model.CoverImage;
                    galleryGroupTranslate.Title = model.Title;
                    galleryGroupTranslate.Description = model.Description;
                    Uow.GalleryGroupTranslateRepository.Update(galleryGroupTranslate);
                }
            }
            if(galleryGroupTranslatedk==null)
            {
                if (model.Titledk != null)
                {
                    galleryGroupTranslatedk = new GalleryGroupTranslate();
                    galleryGroupTranslatedk.Language = Dk;
                    galleryGroupTranslatedk.Title = model.Title;
                    galleryGroupTranslatedk.Description = model.Descriptiondk;
                    galleryGroupTranslatedk.GalleryGroup = galleryGroup;
                    galleryGroupTranslatedk.thumbnail = model.CoverImagedk;
                    Uow.GalleryGroupTranslateRepository.Insert(galleryGroupTranslatedk);
                }
            }
            else
            {
                if (model.Titledk == null)
                {
                    Uow.GalleryGroupTranslateRepository.Delete(galleryGroupTranslatedk);
                }
                else
                {
                    galleryGroupTranslatedk.Title = model.Titledk;
                    galleryGroupTranslatedk.Description = model.Descriptiondk;
                    galleryGroupTranslatedk.thumbnail = model.CoverImagedk;
                    Uow.GalleryGroupTranslateRepository.Update(galleryGroupTranslatedk);
                }
            }
            List<long> listOpen = new List<long>();
            if (model.GelleryImages != null)
            {
                for (int i = 0; i < model.GelleryImages.Count(); i++)
                {
                    if (listOpen.Contains(model.GelleryImages[i]))
                        continue;
                    listOpen.Add(model.GelleryImages[i]);
                    gallery = new Gallery();
                    galleryTranslation = new GalleryTranslation();
                    gallery.GalleryGroup = galleryGroup;
                    gallery.Top = true;
                    gallery.Display = false;
                    if (model.GelleryImagesPublished.Contains(model.GelleryImages[i]))
                    {
                        gallery.Display = true;
                    }
                    gallery.Date = model.GelleryImagesDate[i];
                    gallery.FileId = model.GelleryImages[i];

                    Uow.GalleryRepository.Insert(gallery);

                    galleryTranslation.GalleryId = gallery.Id;
                    galleryTranslation.Language = En;

                    Uow.GalleryTranslationRepository.Insert(galleryTranslation);
                }
            }
        }

        public void DeleteGallery(int Id)
        {
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();

            GalleryGroupTranslate galleryGroupTranslate = Uow.GalleryGroupTranslateRepository.Get(x => x.GalleryGroupId == Id && x.Language.Id == En.Id).FirstOrDefault();
            GalleryGroupTranslate galleryGroupTranslatedk = Uow.GalleryGroupTranslateRepository.Get(x => x.GalleryGroupId == Id && x.Language.Id == Dk.Id).FirstOrDefault();
            GalleryGroup galleryGroup = Uow.GalleryGroupRepository.Get(Id);
            List<Gallery> galleryList = new List<Gallery>();
            List<GalleryTranslation> GalleryTranslationList = new List<GalleryTranslation>();
            var existedGallery = Uow.GalleryRepository.Get(x => x.GalleryGroup.Id == galleryGroup.Id);
            if(existedGallery!=null)
            {
             foreach (var item in existedGallery)
                {
                    var existedGalleryTranslation = Uow.GalleryTranslationRepository.Get(x => x.Gallery.Id == item.Id).AsEnumerable();
                    foreach (var item1 in existedGalleryTranslation)
                    {
                        Uow.GalleryTranslationRepository.Delete(item1);
                    }
                    Uow.GalleryRepository.Delete(item);

                }
            }
          
            if(galleryGroupTranslate!=null)
            {
                Uow.GalleryGroupTranslateRepository.Delete(galleryGroupTranslate);
            }
            if(galleryGroupTranslatedk!=null)
            {
                Uow.GalleryGroupTranslateRepository.Delete(galleryGroupTranslatedk);
            }
            Uow.GalleryGroupRepository.Delete(galleryGroup);
        }
    }
}
