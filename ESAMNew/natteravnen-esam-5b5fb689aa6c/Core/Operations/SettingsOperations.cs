﻿using Core.Models;
using DAL.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DAL;

namespace Core.Operations
{
    public class SettingsOperations : AbstractOperations
    {
        public SettingsModel getSettings()
        {
            SettingsModel model = new SettingsModel();
            List<int> Id = new List<int>();
            List<string> Description = new List<string>();
            List<string> Value = new List<string>();
            List<bool> Html = new List<bool>();
            List<string> Slug = new List<string>();

            var settings = Uow.SettingsRepository.Get(x => x.Editable == true);
            foreach (var item in settings)
            {
                Id.Add(item.Id);
                Description.Add(item.Description);
                Value.Add(item.Value);
                Html.Add(item.Html);
                Slug.Add(item.Slug);
            }
            model.Id = Id;
            model.Description = Description;
            model.Value = Value;
            model.Html = Html;
            model.Slug = Slug;
            return model; 
        }
        public List<Settings> GetSettings()
        {
            return Uow.SettingsRepository.Get(x => x.Editable == true).ToList();
        }
        public List<Settings> GetSettingsForApi()
        {
            return Uow.SettingsRepository.Get().ToList();
        }
        public string GetSettingByDescription(string description)
        {
            return GetUow().SettingsRepository.Query.Where(x => x.Description == description).FirstOrDefault().Value;
        }
        public void UpdateSettings(SettingsModel model)
        {
            for(int i=0;i<model.Id.Count;i++)
            {
                var updt = Uow.SettingsRepository.Get(model.Id[i]);
                updt.Value = model.Value[i];
                Uow.SettingsRepository.Update(updt);
            }
        }
    }
}
