﻿using Core.Models;
using DAL.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Core.Operations
{
    public class QuestionnaireOperations : AbstractOperations
    {
        SlugGenerator slugGenerator = new SlugGenerator();
        public List<QuestionnaireList> GetQuestionnaireListFullModel()
        {
            var language = Uow.LanguageRepository.Get(z => z.Name == CultureInfo.CurrentCulture.Name).FirstOrDefault();
            List<QuestionnaireList> model = new List<QuestionnaireList>();
            var type = Uow.SectionTypesRepository.Get(x => x.Name == "Questionnaire").FirstOrDefault();
            List<InfoTranslation> InfoAdditionalTranslation =   Uow.InfoTranslationRepository.Query.Include(x=>x.Info).Where(x => x.Language.Id == language.Id && x.Info.Sections.SectionTypes.Id == type.Id).OrderByDescending(x=>x.Id).ToList();
            foreach(var item in InfoAdditionalTranslation)
            {
                var Title = item.Title;
                var sectionName = Uow.SectionTranslationsRepository.Get(x => x.Language.Id == language.Id && x.SectionId == item.Info.SectionId).FirstOrDefault();
                string Page = "";
                if(sectionName!=null)
                {
                    Page = sectionName.Name;
                }
                model.Add(new QuestionnaireList() { Title = Title, Page = Page, Id = item.InfoId });
            }
            return model;
        }
        public bool checkQuestionnaireSection()
        {
            var section = Uow.SectionsRepository.Get(x => x.SectionTypes.Name == "Questionnaire").FirstOrDefault();
            if(section==null)
            {
                return false;
            }
            return true;
        }
        public void CreateQuestionarie(QuestionnaireModel model)
        {
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            InfoAdditionalTranslate questionnaireTranslate;
            InfoAdditional questionnaire = new InfoAdditional();
            InfoTranslation InfoTranslation;
            Info info = new Info();
            info.Active = model.Published;
            info.Date = DateTime.Now;
            info.StartDate = DateTime.Now;
            info.Sort = 1;
            info.Sections = Uow.SectionsRepository.Get(x => x.SectionTypes.Name == "Questionnaire").FirstOrDefault();
            info.Important = model.Published;
            info.Slider = true;
            info.ConnectedGall = 1;
            info.ImportantType = 1;
            info.Published = model.Published;

            Uow.InfoRepository.Insert(info);

            string slug = "";
            if (model.Title!=null)
            {
                slug = slugGenerator.GenerateSlug(model.Title);
                InfoTranslation = new InfoTranslation();
                InfoTranslation.Title = model.Title;
                InfoTranslation.Info = info;
                InfoTranslation.Language = En;
                InfoTranslation.Slug = slug;
                Uow.InfoTranslationRepository.Insert(InfoTranslation);
            }
            if(model.Titledk!=null)
            {
                if(string.IsNullOrEmpty(slug))
                {
                    slug= slugGenerator.GenerateSlug(model.Titledk);
                }
                InfoTranslation = new InfoTranslation();
                InfoTranslation.Title = model.Titledk;
                InfoTranslation.Info = info;
                InfoTranslation.Language = Dk;
                InfoTranslation.Slug = slug;
                Uow.InfoTranslationRepository.Insert(InfoTranslation);
            }
            questionnaire.Info = info;
            Uow.InfoAdditionalRepository.Insert(questionnaire);
            if(model.Question[0] != null)
            {
                for(int i=0;i< model.Question.Count(); i++)
                {
                    questionnaireTranslate = new InfoAdditionalTranslate();
                    questionnaireTranslate.Language = En;
                    questionnaireTranslate.Question = model.Question[i];
                    questionnaireTranslate.Param_1 = model.param_1[i];
                    questionnaireTranslate.Param_2 = model.param_2[i];
                    questionnaireTranslate.Param_3 = model.param_3[i];
                    questionnaireTranslate.Param_4 = model.param_4[i];
                    questionnaireTranslate.InfoAdditional = questionnaire;
                    questionnaireTranslate.Value = model.Answers[i].ToString();
                    Uow.InfoAdditionalTranslateRepository.Insert(questionnaireTranslate);
                }
            }
            if (model.Questiondk[0]!= null)
            {
                for (int i = 0; i < model.Questiondk.Count(); i++)
                {
                    questionnaireTranslate = new InfoAdditionalTranslate();
                    questionnaireTranslate.Language = Dk;
                    questionnaireTranslate.Question = model.Questiondk[i];
                    questionnaireTranslate.Param_1 = model.param_1dk[i];
                    questionnaireTranslate.Param_2 = model.param_2dk[i];
                    questionnaireTranslate.Param_3 = model.param_3dk[i];
                    questionnaireTranslate.Param_4 = model.param_4dk[i];
                    questionnaireTranslate.InfoAdditional = questionnaire;
                    questionnaireTranslate.Value = model.Answersdk[i].ToString();
                    Uow.InfoAdditionalTranslateRepository.Insert(questionnaireTranslate);
                }
            }
        }
        public QuestionnaireModel GetQuestionnaireModel(int id)
        {
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            QuestionnaireModel model = new QuestionnaireModel();
            List<string> param_1,param_2,param_3,param_4, Question;
            List<int> Answers;

            var infoTranslation = Uow.InfoTranslationRepository.Get(x => x.InfoId == id && x.Language.Id == En.Id).FirstOrDefault();
            var infoTranslationdk = Uow.InfoTranslationRepository.Get(x => x.InfoId == id && x.Language.Id == Dk.Id).FirstOrDefault();
            var infoAdditional = Uow.InfoAdditionalRepository.Query.Include(x=>x.Info).Where(x => x.InfoId == id).FirstOrDefault();

            if(infoTranslationdk!=null)
            {
                model.Titledk = infoTranslationdk.Title;
            }
            if (infoTranslation != null)
            {
                model.Title = infoTranslation.Title;
            }
            model.Published = infoAdditional.Info.Active;
            model.InfoId = infoAdditional.InfoId;

            var infoAdditionalTranslation = Uow.InfoAdditionalTranslateRepository.Get(x => x.InfoAdditional.Id == infoAdditional.Id && x.Language.Id == En.Id);
            if(infoAdditionalTranslation!=null)
            {
                param_1 = new List<string>();
                param_2 = new List<string>();
                param_3 = new List<string>();
                param_4 = new List<string>();
                Question = new List<string>();
                Answers = new List<int>();
                foreach (var item in infoAdditionalTranslation)
                {
                    param_1.Add(item.Param_1);
                    param_2.Add(item.Param_2);
                    param_3.Add(item.Param_3);
                    param_4.Add(item.Param_4);
                    Question.Add(item.Question);
                    Answers.Add(Convert.ToInt32(item.Value));
                }
                model.param_1 = param_1;
                model.param_2 = param_2;
                model.param_3 = param_3;
                model.param_4 = param_4;
                model.Question = Question;
                model.Answers = Answers;
            }
            var infoAdditionalTranslationdk = Uow.InfoAdditionalTranslateRepository.Get(x => x.InfoAdditional.Id == infoAdditional.Id && x.Language.Id == Dk.Id);
            if (infoAdditionalTranslationdk != null)
            {
                param_1 = new List<string>();
                param_2 = new List<string>();
                param_3 = new List<string>();
                param_4 = new List<string>();
                Question = new List<string>();
                Answers = new List<int>();
                foreach (var item in infoAdditionalTranslationdk)
                {
                    param_1.Add(item.Param_1);
                    param_2.Add(item.Param_2);
                    param_3.Add(item.Param_3);
                    param_4.Add(item.Param_4);
                    Question.Add(item.Question);
                    Answers.Add(Convert.ToInt32(item.Value));
                }
                model.param_1dk = param_1;
                model.param_2dk = param_2;
                model.param_3dk = param_3;
                model.param_4dk = param_4;
                model.Questiondk = Question;
                model.Answersdk = Answers;
            }
            return model;
        }
        public void UpdateQuestionnaire(QuestionnaireModel model)
        {
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            InfoAdditionalTranslate questionnaireTranslate;
            InfoAdditional questionnaire = Uow.InfoAdditionalRepository.Get(x=>x.InfoId==model.InfoId).FirstOrDefault();
            InfoTranslation InfoTranslation;
            Info info = Uow.InfoRepository.Get(model.InfoId);
            info.Active = model.Published;
            info.Sections = Uow.SectionsRepository.Get(x => x.SectionTypes.Name == "Questionnaire").FirstOrDefault();
            info.Important = model.Published;
            info.Published = model.Published;

            Uow.InfoRepository.Update(info);
            string slug = "";
            if (model.Title != null)
            {
                InfoTranslation = Uow.InfoTranslationRepository.Get(x => x.Language.Id == En.Id && x.Info.Id == info.Id).FirstOrDefault();
                if (InfoTranslation != null)
                {
                    slug = InfoTranslation.Slug;
                    InfoTranslation.Title = model.Title;
                    Uow.InfoTranslationRepository.Update(InfoTranslation);
                }
                else
                {
                    InfoTranslation = Uow.InfoTranslationRepository.Get(x => x.Language.Id == En.Id && x.Info.Id == info.Id).FirstOrDefault();
                    if (InfoTranslation != null)
                    {
                        slug = InfoTranslation.Slug;
                    }
                    else
                    {
                        slug = slugGenerator.GenerateSlug(model.Title);
                    }
                    InfoTranslation = new InfoTranslation();
                    InfoTranslation.Title = model.Title;
                    InfoTranslation.Info = info;
                    InfoTranslation.Language = En;
                    InfoTranslation.Slug = slug;
                    Uow.InfoTranslationRepository.Insert(InfoTranslation);
                }
            }
            else
            {
                InfoTranslation = Uow.InfoTranslationRepository.Get(x => x.Language.Id == En.Id && x.Info.Id == info.Id).FirstOrDefault();
                if(InfoTranslation !=null)
                {
                    Uow.InfoTranslationRepository.Delete(InfoTranslation);
                }
            }
            if (model.Titledk != null)
            {
                InfoTranslation = Uow.InfoTranslationRepository.Get(x => x.Language.Id == Dk.Id && x.Info.Id == info.Id).FirstOrDefault();
                if (InfoTranslation != null)
                {
                    slug = InfoTranslation.Slug;
                    InfoTranslation.Title = model.Titledk;
                    Uow.InfoTranslationRepository.Update(InfoTranslation);
                }
                else
                {
                    if(string.IsNullOrEmpty(slug))
                    {
                        InfoTranslation = Uow.InfoTranslationRepository.Get(x => x.Language.Id == En.Id && x.Info.Id == info.Id).FirstOrDefault();
                        if (InfoTranslation != null)
                        {
                            slug = InfoTranslation.Slug;
                        }
                        else
                        {
                            slug = slugGenerator.GenerateSlug(model.Titledk);
                        }
                    }
                   
                    InfoTranslation = new InfoTranslation();
                    InfoTranslation.Title = model.Titledk;
                    InfoTranslation.Info = info;
                    InfoTranslation.Language = Dk;
                    InfoTranslation.Slug = slug;
                    Uow.InfoTranslationRepository.Insert(InfoTranslation);
                }
            }
            else
            {
                InfoTranslation = Uow.InfoTranslationRepository.Get(x => x.Language.Id == Dk.Id && x.Info.Id == info.Id).FirstOrDefault();
                if (InfoTranslation != null)
                {
                    Uow.InfoTranslationRepository.Delete(InfoTranslation);
                }
            }
            if(questionnaire==null)
            {
                questionnaire = new InfoAdditional();
                questionnaire.Info = info;
                Uow.InfoAdditionalRepository.Insert(questionnaire);
            }
            else
            {
                questionnaire.Info = info;
                Uow.InfoAdditionalRepository.Update(questionnaire);
            }

            var QuestionEn = Uow.InfoAdditionalTranslateRepository.Get(x => x.InfoAdditional.Id == questionnaire.Id && x.Language.Id == En.Id);
            if(QuestionEn!=null)
            {
                foreach(var item in QuestionEn)
                {
                    Uow.InfoAdditionalTranslateRepository.Delete(item);
                }
            }

            var QuestionDk = Uow.InfoAdditionalTranslateRepository.Get(x => x.InfoAdditional.Id == questionnaire.Id && x.Language.Id == Dk.Id);
            if (QuestionDk != null)
            {
                foreach (var item in QuestionDk)
                {
                    Uow.InfoAdditionalTranslateRepository.Delete(item);
                }
            }

            if (model.Question != null)
            {
                for (int i = 0; i < model.Question.Count(); i++)
                {
                    questionnaireTranslate = new InfoAdditionalTranslate();
                    questionnaireTranslate.Language = En;
                    questionnaireTranslate.Question = model.Question[i];
                    questionnaireTranslate.Param_1 = model.param_1[i];
                    questionnaireTranslate.Param_2 = model.param_2[i];
                    questionnaireTranslate.Param_3 = model.param_3[i];
                    questionnaireTranslate.Param_4 = model.param_4[i];
                    questionnaireTranslate.InfoAdditional = questionnaire;
                    questionnaireTranslate.Value = model.Answers[i].ToString();
                    Uow.InfoAdditionalTranslateRepository.Insert(questionnaireTranslate);
                }
            }
            if (model.Questiondk != null)
            {
                for (int i = 0; i < model.Questiondk.Count(); i++)
                {
                    questionnaireTranslate = new InfoAdditionalTranslate();
                    questionnaireTranslate.Language = Dk;
                    questionnaireTranslate.Question = model.Questiondk[i];
                    questionnaireTranslate.Param_1 = model.param_1dk[i];
                    questionnaireTranslate.Param_2 = model.param_2dk[i];
                    questionnaireTranslate.Param_3 = model.param_3dk[i];
                    questionnaireTranslate.Param_4 = model.param_4dk[i];
                    questionnaireTranslate.InfoAdditional = questionnaire;
                    questionnaireTranslate.Value = model.Answersdk[i].ToString();
                    Uow.InfoAdditionalTranslateRepository.Insert(questionnaireTranslate);
                }
            }
        }
        public void DeleteQuestionnaire(int Id)
        {
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            InfoAdditional questionnaire = Uow.InfoAdditionalRepository.Get(x => x.InfoId == Id).FirstOrDefault();
            InfoTranslation InfoTranslation = Uow.InfoTranslationRepository.Get(x=>x.InfoId==Id && x.Language.Id == En.Id).FirstOrDefault();
            InfoTranslation InfoTranslationdk = Uow.InfoTranslationRepository.Get(x => x.InfoId == Id && x.Language.Id == Dk.Id).FirstOrDefault();
            Info info = Uow.InfoRepository.Get(Id);
            
            var QuestionEn = Uow.InfoAdditionalTranslateRepository.Get(x => x.InfoAdditional.Id == questionnaire.Id && x.Language.Id == En.Id);
            if (QuestionEn != null)
            {
                foreach (var item in QuestionEn)
                {
                    Uow.InfoAdditionalTranslateRepository.Delete(item);
                }
            }

            var QuestionDk = Uow.InfoAdditionalTranslateRepository.Get(x => x.InfoAdditional.Id == questionnaire.Id && x.Language.Id == Dk.Id);
            if (QuestionDk != null)
            {
                foreach (var item in QuestionDk)
                {
                    Uow.InfoAdditionalTranslateRepository.Delete(item);
                }
            }
            Uow.InfoAdditionalRepository.Delete(questionnaire);
            Uow.InfoTranslationRepository.Delete(InfoTranslation);
            Uow.InfoTranslationRepository.Delete(InfoTranslationdk);
            Uow.InfoRepository.Delete(info);
        }
    }
}
