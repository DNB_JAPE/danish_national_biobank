﻿using Core.Models;
using DAL.Context;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Core.Operations
{
    public class TranslateOperation : AbstractOperations
    {
        public Translation getTranslate()
        {
            var language = Uow.LanguageRepository.Get(z => z.Name == CultureInfo.CurrentCulture.Name).FirstOrDefault();
            return Uow.TranslationRepository.Get(x => x.Language.Id == language.Id).FirstOrDefault();
        }
        public TranslationModel GetTranslations()
        {
            TranslationModel model = new TranslationModel();
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-Us").FirstOrDefault();

            var translationen = Uow.TranslationRepository.Get(x => x.Language.Id == En.Id).FirstOrDefault();
            foreach(var item in model.GetType().GetProperties().Where(p => !p.Name.EndsWith("dk")))
            {
                item.SetValue(model,translationen.GetType().GetProperty(item.Name).GetValue(translationen, null));
            }

            var translationdk = Uow.TranslationRepository.Get(x => x.Language.Id == Dk.Id).FirstOrDefault();
            foreach (var item in model.GetType().GetProperties().Where(p => p.Name.EndsWith("dk")))
            {
                item.SetValue(model, translationdk.GetType().GetProperty(item.Name.Replace("dk", "")).GetValue(translationdk, null));
            }

            return model;
        }
        public void UpdateTranslation(TranslationModel model)
        {
            var translationen = Uow.TranslationRepository.Get(model.Id);
            foreach (var item in model.GetType().GetProperties().Where(p => !p.Name.EndsWith("dk")))
            {
                translationen.GetType().GetProperty(item.Name.Replace("dk", "")).SetValue(translationen, item.GetValue(model,null));
            }
            Uow.TranslationRepository.Update(translationen);

            var translationdk = Uow.TranslationRepository.Get(model.Iddk);
            foreach (var item in model.GetType().GetProperties().Where(p => p.Name.EndsWith("dk")))
            {
                translationdk.GetType().GetProperty(item.Name.Replace("dk", "")).SetValue(translationdk, item.GetValue(model, null));
            }
            Uow.TranslationRepository.Update(translationdk);
        }
  
    }
}
