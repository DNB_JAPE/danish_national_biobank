﻿using Core.Models;
using DAL.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using ThenInclude;
using Newtonsoft.Json;

namespace Core.Operations
{
    public class UserOperations : AbstractOperations
    {
        public DAL.Context.Language getCurrentLanguage()
        {
            return Uow.LanguageRepository.Get(z => z.Name == CultureInfo.CurrentCulture.Name).FirstOrDefault();
        }
        //public void DeleteUser(int id)
        //{
        //    var user = Uow.UsersRepository.Get(id);
        //    if(user!=null)
        //    {
        //        var userDisease = Uow.UserDiseaseRepository.Get(x => x.Users.Id == user.Id).ToList();
        //        if(userDisease!=null)
        //        {
        //            foreach(var item in userDisease)
        //            {
        //                Uow.UserDiseaseRepository.Delete(item);
        //            }
        //        }
        //        Uow.UsersRepository.Delete(user);
        //    }
        //}
        //public bool DeleteUserWithApi(int id)
        //{
        //    var user = Uow.UsersRepository.Get(id);
        //    if (user != null)
        //    {
        //        var userDisease = Uow.UserDiseaseRepository.Get(x => x.Users.Id == user.Id).ToList();
        //        if (userDisease != null)
        //        {
        //            foreach (var item in userDisease)
        //            {
        //                Uow.UserDiseaseRepository.Delete(item);
        //            }
        //        }
        //        Uow.UsersRepository.Delete(user);
        //        return true;
        //    }
        //    return false;

        //}
        public List<Users> GetUsers()
        {
            return Uow.UsersRepository.GetAll().ToList();
        }

        public Users GetUserById(int id)
        {
            return Uow.UsersRepository.Get(id);
        }


        public void UpdateUser(Users Model)
        {
            var user = Uow.UsersRepository.Get(Model.Id);
            user.AcceptUseData = Model.AcceptUseData;
            user.Active = Model.Active;
            user.AuthenticationInfo = Model.AuthenticationInfo;
            user.ContactMe = Model.ContactMe;
            user.Email = Model.Email;
            user.NemId = Model.NemId;
            user.ParticipateInProject = Model.ParticipateInProject;
            user.Phone = Model.Phone;
            user.SendMeNewsLetter = Model.SendMeNewsLetter;
            user.StoreboxEmail = Model.StoreboxEmail;
            user.StoreboxPhone = Model.StoreboxPhone;
            user.UserDisease = Model.UserDisease;
            user.ValidationCode = Model.ValidationCode;

            Uow.UsersRepository.Update(user);
        }

        //public List<UserModel> GetUserModel()
        //{
        //    var language = getCurrentLanguage();
        //    if (language == null)
        //    {
        //        language = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
        //    }
        //    List<UserModel> UserList = new List<UserModel>();
        //    List<Diseases> disease;
        //    UserModel model;
        //    var users = Uow.UsersRepository.GetAll().ToList();
        //    foreach(var item in users)
        //    {
        //        model = new UserModel();
        //        model.Id = item.Id;
        //        model.ParticipateInProject = item.ParticipateInProject;
        //        model.Active = item.Active;
        //        model.ContactMe = item.ContactMe;
        //        model.Email = item.Email;
        //        model.NemId = item.NemId;
        //        //model.New = item.NewsLetter;
        //        model.SendMeNewsLetter = item.SendMeNewsLetter;
        //        model.Phone = item.Phone;
        //        model.RegistrationDate = item.RegistrationDate;
        //        model.AcceptUseData = item.AcceptUseData;
        //        model.StoreboxEmail = item.StoreboxEmail;
        //        model.StoreboxPhone = item.StoreboxPhone;
        //        model.AuthenticationInfo = item.AuthenticationInfo;
        //        model.ValidationCode = item.ValidationCode;

        //        var diseases = Uow.UserDiseaseRepository.Query.Include(x => x.Disease.DiseaseTranslation).Where(x => x.Users.Id == item.Id).ToList();
        //        disease = new List<Diseases>();
        //        foreach (var dis in diseases)
        //        {
        //            disease.Add(new Diseases() { Id = dis.DiseaseId, Disease = (dis.Disease.DiseaseTranslation.Where(x => x.Language.Id == language.Id).FirstOrDefault()!=null?dis.Disease.DiseaseTranslation.Where(x => x.Language.Id == language.Id).FirstOrDefault().Title:"")});
        //        }
        //        model.Diseases = disease;
        //        UserList.Add(model);
        //    }
        //    return UserList;
        //}
        public bool checkKey(string id)
        {
            var key = Uow.SettingsRepository.Get(x => x.Value == id&& x.Slug== "api_key").FirstOrDefault();
            if(key==null)
            {
                return false;
            }
            return true;
        }
        public string validationkey()
        {
            return Uow.SettingsRepository.Get(x=>x.Slug == "api_key").FirstOrDefault().Value;
        }

    }
}
