﻿using Core.Models;
using DAL.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Core.Operations
{
    public class BannerOperations:AbstractOperations
    {
        public IEnumerable<BannerTranslation> GetAllBanner(int id)
        {
            var language = Uow.LanguageRepository.Get(z => z.Name == CultureInfo.CurrentCulture.Name).FirstOrDefault();
            var type = Uow.SettingsRepository.Get(id);
            if (id > 0)
            {
                if (language == null)
                {
                   return Uow.BannerTranslationRepository.Query.Include(x => x.Banner).Where(x => x.Banner.Settings.Id == type.Id);
                }
                return Uow.BannerTranslationRepository.Query.Include(x => x.Banner).Where(x => x.Banner.Settings.Id == type.Id && x.Language.Id == language.Id);
            }
            if (language == null)
            {
                return Uow.BannerTranslationRepository.Query.Include(x => x.Banner).AsEnumerable();
            }
            return Uow.BannerTranslationRepository.Query.Include(x => x.Banner).Where(x => x.Language.Id == language.Id);
        }
        public IEnumerable<Settings> GetTypes()
        {
            return Uow.SettingsRepository.Get(x => x.Slug == "banner_type");
        }
        public int GetTypeValueByBannerId(int id)
        {
            return Convert.ToInt32(Uow.BannerRepository.Get(id).Settings.Value);
        }
        public int GetTypeValueBySettingsId(int id)
        {
            return Convert.ToInt32(Uow.SettingsRepository.Get(id).Value);
        }
        public void CreateBanner(BannerModel model)
        {
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();


            Banner banner = new Banner();
            BannerToSection bannerTosection = new BannerToSection();

            banner.Active = model.Active;
            banner.Target = model.Target;
            banner.TypeId = model.Type;
            banner.AllSections = true;
            banner.Sort = 1;
            banner.Undeletable = false;

            Uow.BannerRepository.Insert(banner);

            if(model.Titledk!=null)
            {
                BannerTranslation bannerTranslation = new BannerTranslation();
                bannerTranslation.Banner = banner;
                bannerTranslation.Title = model.Titledk;
                bannerTranslation.Language = Dk;
                bannerTranslation.Url = model.Urldk;
                bannerTranslation.FileId = (model.FileIddk == 0) ? (long?)null : model.FileIddk;
                Uow.BannerTranslationRepository.Insert(bannerTranslation);
            }
            if (model.Title != null)
            {
                BannerTranslation bannerTranslation = new BannerTranslation();
                bannerTranslation.Banner = banner;
                bannerTranslation.Title = model.Title;
                bannerTranslation.Language = En;
                bannerTranslation.Url = model.Url;
                bannerTranslation.FileId = (model.FileId == 0) ? (long?)null : model.FileId;
                Uow.BannerTranslationRepository.Insert(bannerTranslation);
            }
            if(model.DisplayBannerIn!=null)
            {
                foreach(var item in model.DisplayBannerIn)
                {
                    Uow.BannerToSectionRepository.Insert(new BannerToSection() { Banner = banner, SectionId = item });
                }
            }
        }
        public BannerModel GetBannerModel(int id)
        {
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            BannerModel model = new BannerModel();
            BannerTranslation bannerTranslation;
            Banner banner = Uow.BannerRepository.Get(x => x.Id == id).FirstOrDefault();
            List<int> sectionList = new List<int>();
            var bannerToSection = Uow.BannerToSectionRepository.Get(x => x.Banner.Id == banner.Id);

            if((bannerTranslation = Uow.BannerTranslationRepository.Get(x=>x.Banner.Id == banner.Id && x.Language.Id == Dk.Id).FirstOrDefault())!=null)
            {
                model.Titledk = bannerTranslation.Title;
                model.Urldk = bannerTranslation.Url;
                model.FileIddk = (bannerTranslation.FileId == null) ? 0 : (long)bannerTranslation.FileId;
            }
            if ((bannerTranslation = Uow.BannerTranslationRepository.Get(x => x.Banner.Id == banner.Id && x.Language.Id == En.Id).FirstOrDefault()) != null)
            {
                model.Title = bannerTranslation.Title;
                model.Url = bannerTranslation.Url;
                model.FileId = (bannerTranslation.FileId==null)?0:(long)bannerTranslation.FileId;
            }
            model.Id = banner.Id;
            model.Type = banner.TypeId;
            model.Target = banner.Target;
            model.Active = banner.Active;
            foreach(var item in bannerToSection)
            {
                sectionList.Add(item.SectionId);
            }
            model.DisplayBannerIn = sectionList;
            return model; 
        }
        public void UpdateBanner(BannerModel model)
        {
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();

            Banner banner = Uow.BannerRepository.Get(model.Id);
            BannerToSection bannerTosectionNew = new BannerToSection();
            var bannerTosection = Uow.BannerToSectionRepository.Get(x => x.Banner.Id == banner.Id).AsEnumerable();
            foreach(var Item in bannerTosection)
            {
                Uow.BannerToSectionRepository.Delete(Item);
            }
            banner.Active = model.Active;
            banner.Target = model.Target;
            banner.TypeId = model.Type;
            banner.AllSections = true;
            banner.Sort = 1;
            banner.Undeletable = false;

            Uow.BannerRepository.Update(banner);

            if (model.Titledk != null)
            {
                BannerTranslation bannerTranslation = Uow.BannerTranslationRepository.Get(x => x.Banner.Id == banner.Id && x.Language.Id == Dk.Id).FirstOrDefault();
                if(bannerTranslation==null)
                {
                    bannerTranslation = new BannerTranslation();
                    bannerTranslation.Banner = banner;
                    bannerTranslation.Title = model.Titledk;
                    bannerTranslation.Language = Dk;
                    bannerTranslation.Url = model.Urldk;
                    bannerTranslation.FileId = (model.FileIddk == 0) ? (long?)null : model.FileIddk;
                    Uow.BannerTranslationRepository.Insert(bannerTranslation);
                }
                else
                {
                    bannerTranslation.Title = model.Titledk;
                    bannerTranslation.Url = model.Urldk;
                    bannerTranslation.FileId = (model.FileIddk == 0) ? (long?)null : model.FileIddk;
                    Uow.BannerTranslationRepository.Update(bannerTranslation);
                }
            }
            else
            {
                BannerTranslation bannerTranslation = Uow.BannerTranslationRepository.Get(x => x.Banner.Id == banner.Id && x.Language.Id == Dk.Id).FirstOrDefault();
                if (bannerTranslation != null)
                {
                    Uow.BannerTranslationRepository.Delete(bannerTranslation);
                }
            }
            if (model.Title != null)
            {
                BannerTranslation bannerTranslation = Uow.BannerTranslationRepository.Get(x => x.Banner.Id == banner.Id && x.Language.Id == En.Id).FirstOrDefault();
                if (bannerTranslation == null)
                {
                    bannerTranslation = new BannerTranslation();
                    bannerTranslation.Banner = banner;
                    bannerTranslation.Title = model.Title;
                    bannerTranslation.Language = En;
                    bannerTranslation.Url = model.Url;
                    bannerTranslation.FileId = (model.FileId == 0) ? (long?)null : model.FileId;
                    Uow.BannerTranslationRepository.Insert(bannerTranslation);
                }
                else
                {
                    bannerTranslation.Title = model.Title;
                    bannerTranslation.Url = model.Url;
                    bannerTranslation.FileId = (model.FileId == 0) ? (long?)null : model.FileId;
                    Uow.BannerTranslationRepository.Update(bannerTranslation);
                }
            }
            else
            {
                BannerTranslation bannerTranslation = Uow.BannerTranslationRepository.Get(x => x.Banner.Id == banner.Id && x.Language.Id == En.Id).FirstOrDefault();
                if (bannerTranslation != null)
                {
                    Uow.BannerTranslationRepository.Delete(bannerTranslation);
                }
            }
            if (model.DisplayBannerIn != null)
            {
                foreach (var item in model.DisplayBannerIn)
                {
                    Uow.BannerToSectionRepository.Insert(new BannerToSection() { Banner = banner, SectionId = item });
                }
            }
        }
        public void DeleteBanner(int id)
        {
            var Dk = Uow.LanguageRepository.Get(z => z.Name == "da-DK").FirstOrDefault();
            var En = Uow.LanguageRepository.Get(z => z.Name == "en-US").FirstOrDefault();
            Banner banner = Uow.BannerRepository.Get(id);
            BannerTranslation bannerTranslation = Uow.BannerTranslationRepository.Get(x => x.Banner.Id == banner.Id && x.Language.Id == En.Id).FirstOrDefault();
            BannerTranslation bannerTranslationdk = Uow.BannerTranslationRepository.Get(x => x.Banner.Id == banner.Id && x.Language.Id == Dk.Id).FirstOrDefault();
            var bannerTosection = Uow.BannerToSectionRepository.Get(x => x.Banner.Id == banner.Id).AsEnumerable();
            foreach (var Item in bannerTosection)
            {
                Uow.BannerToSectionRepository.Delete(Item);
            }
            if(bannerTranslation!=null)
            {
                Uow.BannerTranslationRepository.Delete(bannerTranslation);
            }
            if (bannerTranslationdk != null)
            {
                Uow.BannerTranslationRepository.Delete(bannerTranslationdk);
            }
            Uow.BannerRepository.Delete(banner);
        }
    }
}
