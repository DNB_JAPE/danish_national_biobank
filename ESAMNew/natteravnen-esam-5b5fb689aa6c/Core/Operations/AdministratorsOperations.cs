﻿using DAL.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Globalization;
using System.Data.Entity;

namespace Core.Operations
{
    public class AdministratorsOperations : AbstractOperations
    {
        public IEnumerable<Administrators> GetAdministratorsList()
        {
            return Uow.AdministratorsRepository.Query.Include(x => x.AdministratorGroups);
        }
        public Administrators CheckUsers(string userName, string password)
        {
            var user = Uow.AdministratorsRepository.Get(x => x.Password == password && x.Name == userName).FirstOrDefault();
            return user;
        }
        public string GetSessionUserName(int Id)
        {
            return Uow.AdministratorsRepository.Get(x => x.Id == Id).FirstOrDefault().Name;
        }
        public bool CreateAdministrator(Administrators administrator)
        {
            var user = Uow.AdministratorsRepository.Get(x=>x.Email==administrator.Email|| x.Name==administrator.Name).FirstOrDefault();
            if(user==null)
            {
                Uow.AdministratorsRepository.Insert(administrator);
                return true;
            }
            else
            {
                return false;
            }
        }
        public Administrators GetAdministratorById(int Id)
        {
            return Uow.AdministratorsRepository.Get(x => x.Id == Id).FirstOrDefault();
        }
        public bool UpdateAdministrator(Administrators administrator)
        {
            try
            {
                var user = Uow.AdministratorsRepository.Get(x => x.Email.Equals(administrator.Email) && x.Id != administrator.Id || x.Name.Equals(administrator.Name)  &&  x.Id != administrator.Id).FirstOrDefault();
                if (user == null)
                {
                    Uow.AdministratorsRepository.Update(administrator);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        public void ResetPassword(int id)
        {
            var user = Uow.AdministratorsRepository.Get(id);
            user.Password = MD5Hash("111111");
            Uow.AdministratorsRepository.Update(user);
        }
        public void DeleteAdministrator(int Id)
        {
            Uow.AdministratorsRepository.Delete(Uow.AdministratorsRepository.Get(x=>x.Id==Id).FirstOrDefault());
        }
    
        public bool CheckPassword(int id,string password)
        {
            string passHash = MD5Hash(password);
            var user = Uow.AdministratorsRepository.Get(x => x.Id == id && x.Password == passHash).FirstOrDefault();
            if(user==null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void changePassword(int id, string password, string newpassword)
        {
            var user = Uow.AdministratorsRepository.Get(id);
            string passHash = MD5Hash(newpassword);
            user.Password = passHash;
            Uow.AdministratorsRepository.Update(user);
        }
        public static string MD5Hash(string input)
        {
            byte[] data = System.Security.Cryptography.MD5.Create().ComputeHash(System.Text.Encoding.UTF8.GetBytes(input));
            string md5 = "";
            for (int i = 0; i < data.Length; i++)
            {
                md5 += data[i].ToString("x2");
            }
            return md5;
        }
    }

}
