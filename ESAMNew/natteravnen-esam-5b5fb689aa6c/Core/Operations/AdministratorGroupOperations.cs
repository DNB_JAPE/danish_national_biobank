﻿using DAL.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Data.Entity;
using Core.Models;

namespace Core.Operations
{
    public class AdministratorGroupOperations : AbstractOperations
    {
        public IEnumerable<AdministratorGroups> Get()
        {
            return Uow.AdministratorGroupsRepository.GetAll();
        }
        public bool CreateGroup(AdministratorGroupsModel administratorGroups)
        {
            var group = Uow.AdministratorGroupsRepository.Get(x => x.Name == administratorGroups.Name && x.Description == administratorGroups.Description).FirstOrDefault();
            if(group==null)
            {
                AdministratorGroups model = new AdministratorGroups();
                model.Name = administratorGroups.Name;
                model.Description = administratorGroups.Description;
                Uow.AdministratorGroupsRepository.Insert(model);
                if(administratorGroups.pages!=null)
                {
                    foreach(var item in administratorGroups.pages)
                    {
                        Uow.AdministratorGroupsPermissionsRepository.Insert(new AdministratorGroupsPermissions() { PageId = item, AdministratorGroups = model });
                    }
                }
                
                return true;
            }
            else
            {
                return false;
            }
        }
        public AdministratorGroupsModel GetGroupById(int Id)
        {
            var AdministratorGroups= Uow.AdministratorGroupsRepository.Get(Id);
            var relation = Uow.AdministratorGroupsPermissionsRepository.Get(x => x.GroupId == Id);
            List<int> pages = new List<int>();
            foreach(var item in relation)
            {
                pages.Add(item.PageId);
            }
            AdministratorGroupsModel model = new AdministratorGroupsModel();
            model.Name = AdministratorGroups.Name;
            model.Description = AdministratorGroups.Description;
            model.Id = AdministratorGroups.Id;
            model.pages = pages;
            return model;
        }
        public bool UpdateGroup (AdministratorGroupsModel administratorGroups)
        {
            try
            {
                var model = Uow.AdministratorGroupsRepository.Get(administratorGroups.Id);
                if(administratorGroups.Name==null && administratorGroups.Description ==null)
                {
                    return false;
                }
                model.Name = administratorGroups.Name;
                model.Description = administratorGroups.Description;
                Uow.AdministratorGroupsRepository.Update(model);
                var relation = Uow.AdministratorGroupsPermissionsRepository.Query.Include(x => x.AdministratorGroups).Include(x => x.AdministratorGroups).ToList();
                foreach (var item in relation)
                {
                    if(item.GroupId==model.Id)
                    {
                        Uow.AdministratorGroupsPermissionsRepository.Delete(item);
                    }
                }
                if (administratorGroups.pages != null)
                {
                    foreach (var item in administratorGroups.pages)
                    {
                        Uow.AdministratorGroupsPermissionsRepository.Insert(new AdministratorGroupsPermissions() { PageId = item, AdministratorGroups = model });
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteGroup(int Id)
        {
            var Group = Uow.AdministratorGroupsRepository.Query.Include(x=>x.Administrators).Where(x=>x.Id==Id).FirstOrDefault();
            if (Group.Administrators.Count>0)
            {
                return false;
            }
            else
            {
                var relation = Uow.AdministratorGroupsPermissionsRepository.Query.Include(x => x.AdministratorGroups).Include(x => x.AdministratorGroups).ToList();
                foreach (var item in relation)
                {
                    if (item.GroupId == Group.Id)
                    {
                        Uow.AdministratorGroupsPermissionsRepository.Delete(item);
                    }
                }
                Uow.AdministratorGroupsRepository.Delete(Group);
                return true;
            }
        }
        public List<Pages> GetPages()
        {
            return Uow.PagesRepository.Get(x=>x.Active==true).ToList();
        }
        public List<int> GetSessionPermisions(int administratiorId)
        {
            var group = Uow.AdministratorsRepository.Get(administratiorId);
            List<int> permisions = new List<int>();
            var per = Uow.AdministratorGroupsPermissionsRepository.Get(x => x.GroupId == group.GroupId).ToList();
            foreach(var item  in per)
            {
                permisions.Add(item.PageId);
            }
            return permisions;
        }
        public bool CheckPermision(string Controller, List<int> permission)
        {
            var page = Uow.PagesRepository.Get(x => x.Controller == Controller).FirstOrDefault();
            if(!permission.Contains(page.Id))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
