﻿using DAL.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Data.Entity;
using System.Globalization;

namespace Core.Operations
{ 
    public class PageOperaions : AbstractOperations
    {
        public  IEnumerable<PagesTranslation> GetPageList()
        {
            var language =  Uow.LanguageRepository.Get(z => z.Name == CultureInfo.CurrentCulture.Name).FirstOrDefault();
            return  Uow.PagesTranslationRepository.Query.Where(x => x.languageId == language.Id && x.Pages.Active == true && x.Pages.GroupName == "0").OrderBy(x => x.Pages.Sort).AsEnumerable();
        }
        public IEnumerable<PagesTranslation> GetByGroup(string GroupName)
        {
            var language = Uow.LanguageRepository.Get(z => z.Name == CultureInfo.CurrentCulture.Name).FirstOrDefault();
            return Uow.PagesTranslationRepository.Query.Where(x => x.languageId == language.Id && x.Pages.Active == true && x.Pages.GroupName == GroupName).OrderBy(x => x.Pages.Sort).AsEnumerable();
        }
        public IEnumerable<string> GetCategories()
        {

            return Uow.PagesRepository.Get(x => x.Active == true && x.GroupName !="0").Select(x => x.GroupName).Distinct().AsEnumerable();
        }
        public string[] getRoute(string name)
        {
            string[] a = new string[2];
            var slug =  Uow.SectionsRepository.Get(x => x.Slug == name).FirstOrDefault();
            var type = Uow.SectionTypesRepository.Get(slug.TypeId);
            a[0] = type.Controller;
            a[1] = type.Action;
            return a;
        }
    }
}
