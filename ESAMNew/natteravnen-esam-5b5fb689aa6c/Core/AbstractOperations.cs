﻿using DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    public abstract class AbstractOperations
    {
        public Uow Uow
        {
            get
            {
                if (_Uow == null)
                {
                    _Uow = new Uow();
                }
                return _Uow;
            }
        }
        private Uow _Uow;

        public static Uow GetUow()
        {
            return new Uow();
        }
    }
}
