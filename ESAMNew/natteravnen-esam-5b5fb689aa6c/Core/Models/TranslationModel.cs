﻿using System.Web.Mvc;

namespace Core.Models
{
    public class TranslationModel
    {
         public int Id { get; set; }

         [AllowHtml] public string WebsiteTitle { get; set; }
        // [AllowHtml] public string WebsiteCopyright { get; set; }
        // [AllowHtml] public string CreatedBy { get; set; }
         [AllowHtml] public string ReadMore { get; set; }
         //[AllowHtml] public string UnderConstruction { get; set; }
         //[AllowHtml] public string UnderConstructionText { get; set; }
         [AllowHtml] public string Username { get; set; }
         [AllowHtml] public string Password { get; set; }
         [AllowHtml] public string Name { get; set; }
         [AllowHtml] public string Email { get; set; }
         [AllowHtml] public string Phone { get; set; }
         [AllowHtml] public string Subject { get; set; }
         [AllowHtml] public string Message { get; set; }
         [AllowHtml] public string Send { get; set; }
         [AllowHtml] public string LatestNews { get; set; }
         //[AllowHtml] public string CharSize { get; set; }
         [AllowHtml] public string Back { get; set; }
         [AllowHtml] public string ContinueReading { get; set; }
         [AllowHtml] public string EmailNotify { get; set; }
         [AllowHtml] public string NameNotify { get; set; }
         [AllowHtml] public string RequiredNotify { get; set; }
         [AllowHtml] public string RegistrationDone { get; set; }
         [AllowHtml] public string AllreadyExists { get; set; }
         [AllowHtml] public string MailSendNotify { get; set; }
         [AllowHtml] public string EmailError { get; set; }
         [AllowHtml] public string MailSended { get; set; }
         [AllowHtml] public string SandingFaild { get; set; }
         [AllowHtml] public string CompliteFields { get; set; }
         [AllowHtml] public string MorePosts { get; set; }
         [AllowHtml] public string RequiredName { get; set; }
         [AllowHtml] public string RequiredPhone { get; set; }
         [AllowHtml] public string RequiredType { get; set; }
         [AllowHtml] public string RequiredMessage { get; set; }
         [AllowHtml] public string RequiredAddress { get; set; }
         //[AllowHtml] public string RequiredDesc { get; set; }
         [AllowHtml] public string View { get; set; }
         [AllowHtml] public string Publications { get; set; }
         [AllowHtml] public string ContactUs { get; set; }
         [AllowHtml] public string EmailUniqueError { get; set; }
         [AllowHtml] public string EmailFormatError { get; set; }
         [AllowHtml] public string PhoneError { get; set; }
         [AllowHtml] public string MoreInfo { get; set; }
         [AllowHtml] public string Subscribe { get; set; }
         [AllowHtml] public string Registration { get; set; }
         [AllowHtml] public string MessageError { get; set; }
         [AllowHtml] public string AddressError { get; set; }
        // [AllowHtml] public string CaptchaError { get; set; }
         [AllowHtml] public string Continue { get; set; }
         [AllowHtml] public string Add { get; set; }
         [AllowHtml] public string Next { get; set; }
         [AllowHtml] public string SelectDisease { get; set; }
         [AllowHtml] public string NoDisease { get; set; }
         [AllowHtml] public string IncorectAnswer { get; set; }
         [AllowHtml] public string ConsentMessage { get; set; }
         [AllowHtml] public string Approval { get; set; }
         [AllowHtml] public string ContactInfo { get; set; }
         [AllowHtml] public string EmailAgain { get; set; }
         [AllowHtml] public string PhoneAgain { get; set; }
         [AllowHtml] public string StoreboxAccount { get; set; }
         [AllowHtml] public string Yes { get; set; }
         [AllowHtml] public string SameAsAbove { get; set; }
         [AllowHtml] public string StoreboxEmail { get; set; }
         [AllowHtml] public string StoreboxEmailAgain { get; set; }
         [AllowHtml] public string StoreboxTel { get; set; }
         [AllowHtml] public string StoreboxTelAgain { get; set; }
         [AllowHtml] public string No { get; set; }
         [AllowHtml] public string StoreboxEmailNotification { get; set; }
         [AllowHtml] public string Consent { get; set; }
         [AllowHtml] public string ParticipateText { get; set; }
         [AllowHtml] public string AcceptText { get; set; }
         [AllowHtml] public string ContactMe { get; set; }
         [AllowHtml] public string NewslatterText { get; set; }
         [AllowHtml] public string RequestNemid { get; set; }
         [AllowHtml] public string QuestionnaireError { get; set; }
         [AllowHtml] public string QuestionnaireSuccess { get; set; }
         [AllowHtml] public string ErrorTitle { get; set; }
         [AllowHtml] public string StartAgain { get; set; }
         [AllowHtml] public string WindrawSuccess { get; set; }
         [AllowHtml] public string WindrawEmailNotFound { get; set; }
         [AllowHtml] public string EmailEmpty { get; set; }
         [AllowHtml] public string EmailExists { get; set; }
         [AllowHtml] public string SubscribeKeySent { get; set; }
         [AllowHtml] public string SubscriptionSuccess { get; set; }
         [AllowHtml] public string StoreboxEmptyEmail { get; set; }
         [AllowHtml] public string VerifyNemid { get; set; }
         [AllowHtml] public string PhoneDoesntMatch { get; set; }
         [AllowHtml] public string EmailDoesntMatch1 { get; set; }
         [AllowHtml] public string EmailDoesntMatch { get; set; }
         [AllowHtml] public string QuestionaryIncorrect { get; set; }
         [AllowHtml] public string UniquePhone { get; set; }
         [AllowHtml] public string StoreboxUniquePhone { get; set; }
         [AllowHtml] public string ConsentEnroll { get; set; }
         [AllowHtml] public string UnderstandRisk { get; set; }
         [AllowHtml] public string PerformTasks { get; set; }
         [AllowHtml] public string AskTasks { get; set; }
         [AllowHtml] public string TrackHealth { get; set; }
         [AllowHtml] public string UseData { get; set; }
         [AllowHtml] public string MakeDiscoveries { get; set; }
         [AllowHtml] public string MakeBreakthroughs { get; set; }
         [AllowHtml] public string WhoIsRunning { get; set; }
         [AllowHtml] public string StudySponsored { get; set; }
         [AllowHtml] public string AboutStudy { get; set; }
         [AllowHtml] public string HowWorks { get; set; }
         [AllowHtml] public string Participate { get; set; }
         [AllowHtml] public string SignNemid { get; set; }
         [AllowHtml] public string EnterContacts { get; set; }
         [AllowHtml] public string AuthenticateNemid { get; set; }
         [AllowHtml] public string Remove { get; set; }
         [AllowHtml] public string CheckQuestionarie { get; set; }
         [AllowHtml] public string ValidationCode { get; set; }

         [AllowHtml] public string AccountAllredyActivated { get; set; }
         [AllowHtml] public string PleaseCheckEmail { get; set; }
         [AllowHtml] public string ValidationCodeIncorrect { get; set; }
         [AllowHtml] public string GetValidationCode { get; set; }

         [AllowHtml] public string RepeatEmail { get; set; }
         [AllowHtml] public string RepeatPhone { get; set; }
         [AllowHtml] public string Submit { get; set; }
         [AllowHtml] public string StorboxAccountId { get; set; }
         [AllowHtml] public string QuestionError { get; set; }
         [AllowHtml] public string PhoneEmpty { get; set; }
         [AllowHtml] public string UnknownRegistrationError { get; set; }
         [AllowHtml] public string DiseaseError { get; set; }

        [AllowHtml] public string NemidAllreadyRegistered { get; set; }
        [AllowHtml] public string StoreboxEmailAllreadyRegistered { get; set; }
        [AllowHtml]
        public string MessageIfStoreboxAccountExists { get; set; }
        [AllowHtml]
        public string MessageIfStoreboxDoesntAccountExists { get; set; }
        [AllowHtml]
        public string PleaseAcceptConditions { get; set; }
        [AllowHtml] public string NemidNotFound { get; set; }


        public int Iddk { get; set; }
         [AllowHtml] public string WebsiteTitledk { get; set; }
        // [AllowHtml] public string WebsiteCopyrightdk { get; set; }
         //[AllowHtml] public string CreatedBydk { get; set; }
         [AllowHtml] public string ReadMoredk { get; set; }
         //[AllowHtml] public string UnderConstructiondk { get; set; }
         //[AllowHtml] public string UnderConstructionTextdk { get; set; }
         [AllowHtml] public string Usernamedk { get; set; }
         [AllowHtml] public string Passworddk { get; set; }
         [AllowHtml] public string Namedk { get; set; }
         [AllowHtml] public string Emaildk { get; set; }
         [AllowHtml] public string Phonedk { get; set; }
         [AllowHtml] public string Subjectdk { get; set; }
         [AllowHtml] public string Messagedk { get; set; }
         [AllowHtml] public string Senddk { get; set; }
         [AllowHtml] public string LatestNewsdk { get; set; }
      //   [AllowHtml] public string CharSizedk { get; set; }
         [AllowHtml] public string Backdk { get; set; }
         [AllowHtml] public string ContinueReadingdk { get; set; }
         [AllowHtml] public string EmailNotifydk { get; set; }
         [AllowHtml] public string NameNotifydk { get; set; }
         [AllowHtml] public string RequiredNotifydk { get; set; }
         [AllowHtml] public string RegistrationDonedk { get; set; }
         [AllowHtml] public string AllreadyExistsdk { get; set; }
         [AllowHtml] public string MailSendNotifydk { get; set; }
         [AllowHtml] public string EmailErrordk { get; set; }
         [AllowHtml] public string MailSendeddk { get; set; }
         [AllowHtml] public string SandingFailddk { get; set; }
         [AllowHtml] public string CompliteFieldsdk { get; set; }
         [AllowHtml] public string MorePostsdk { get; set; }
         [AllowHtml] public string RequiredNamedk { get; set; }
         [AllowHtml] public string RequiredPhonedk { get; set; }
         [AllowHtml] public string RequiredTypedk { get; set; }
         [AllowHtml] public string RequiredMessagedk { get; set; }
         [AllowHtml] public string RequiredAddressdk { get; set; }
      //   [AllowHtml] public string RequiredDescdk { get; set; }
         [AllowHtml] public string Viewdk { get; set; }
         [AllowHtml] public string Publicationsdk { get; set; }
         [AllowHtml] public string ContactUsdk { get; set; }
         [AllowHtml] public string EmailUniqueErrordk { get; set; }
         [AllowHtml] public string EmailFormatErrordk { get; set; }
         [AllowHtml] public string PhoneErrordk { get; set; }
         [AllowHtml] public string MoreInfodk { get; set; }
         [AllowHtml] public string Subscribedk { get; set; }
         [AllowHtml] public string Registrationdk { get; set; }
         [AllowHtml] public string MessageErrordk { get; set; }
         [AllowHtml] public string AddressErrordk { get; set; }
        // [AllowHtml] public string CaptchaErrordk { get; set; }
         [AllowHtml] public string Continuedk { get; set; }
         [AllowHtml] public string Adddk { get; set; }
         [AllowHtml] public string Nextdk { get; set; }
         [AllowHtml] public string SelectDiseasedk { get; set; }
         [AllowHtml] public string NoDiseasedk { get; set; }
         [AllowHtml] public string IncorectAnswerdk { get; set; }
         [AllowHtml] public string ConsentMessagedk { get; set; }
         [AllowHtml] public string Approvaldk { get; set; }
         [AllowHtml] public string ContactInfodk { get; set; }
         [AllowHtml] public string EmailAgaindk { get; set; }
         [AllowHtml] public string PhoneAgaindk { get; set; }
         [AllowHtml] public string StoreboxAccountdk { get; set; }
         [AllowHtml] public string Yesdk { get; set; }
         [AllowHtml] public string SameAsAbovedk { get; set; }
         [AllowHtml] public string StoreboxEmaildk { get; set; }
         [AllowHtml] public string StoreboxEmailAgaindk { get; set; }
         [AllowHtml] public string StoreboxTeldk { get; set; }
         [AllowHtml] public string StoreboxTelAgaindk { get; set; }
         [AllowHtml] public string Nodk { get; set; }
         [AllowHtml] public string StoreboxEmailNotificationdk { get; set; }
         [AllowHtml] public string Consentdk { get; set; }
         [AllowHtml] public string ParticipateTextdk { get; set; }
         [AllowHtml] public string AcceptTextdk { get; set; }
         [AllowHtml] public string ContactMedk { get; set; }
         [AllowHtml] public string NewslatterTextdk { get; set; }
         [AllowHtml] public string RequestNemiddk { get; set; }
         [AllowHtml] public string QuestionnaireErrordk { get; set; }
         [AllowHtml] public string QuestionnaireSuccessdk { get; set; }
         [AllowHtml] public string ErrorTitledk { get; set; }
         [AllowHtml] public string StartAgaindk { get; set; }
         [AllowHtml] public string WindrawSuccessdk { get; set; }
         [AllowHtml] public string WindrawEmailNotFounddk { get; set; }
         [AllowHtml] public string EmailEmptydk { get; set; }
         [AllowHtml] public string EmailExistsdk { get; set; }
         [AllowHtml] public string SubscribeKeySentdk { get; set; }
         [AllowHtml] public string SubscriptionSuccessdk { get; set; }
         [AllowHtml] public string StoreboxEmptyEmaildk { get; set; }
         [AllowHtml] public string VerifyNemiddk { get; set; }
         [AllowHtml] public string PhoneDoesntMatchdk { get; set; }
         [AllowHtml] public string EmailDoesntMatch1dk { get; set; }
         [AllowHtml] public string EmailDoesntMatchdk { get; set; }
         [AllowHtml] public string QuestionaryIncorrectdk { get; set; }
         [AllowHtml] public string UniquePhonedk { get; set; }
         [AllowHtml] public string StoreboxUniquePhonedk { get; set; }
         [AllowHtml] public string ConsentEnrolldk { get; set; }
         [AllowHtml] public string UnderstandRiskdk { get; set; }
         [AllowHtml] public string PerformTasksdk { get; set; }
         [AllowHtml] public string AskTasksdk { get; set; }
         [AllowHtml] public string TrackHealthdk { get; set; }
         [AllowHtml] public string UseDatadk { get; set; }
         [AllowHtml] public string MakeDiscoveriesdk { get; set; }
         [AllowHtml] public string MakeBreakthroughsdk { get; set; }
         [AllowHtml] public string WhoIsRunningdk { get; set; }
         [AllowHtml] public string StudySponsoreddk { get; set; }
         [AllowHtml] public string AboutStudydk { get; set; }
         [AllowHtml] public string HowWorksdk { get; set; }
         [AllowHtml] public string Participatedk { get; set; }
         [AllowHtml] public string SignNemiddk { get; set; }
         [AllowHtml] public string EnterContactsdk { get; set; }
         [AllowHtml] public string AuthenticateNemiddk { get; set; }
         [AllowHtml] public string Removedk { get; set; }
         [AllowHtml] public string CheckQuestionariedk { get; set; }
         [AllowHtml] public string ValidationCodedk { get; set; }
         [AllowHtml] public string AccountAllredyActivateddk{ get; set; }
         [AllowHtml] public string PleaseCheckEmaildk { get; set; }
         [AllowHtml] public string ValidationCodeIncorrectdk { get; set; }
         [AllowHtml] public string GetValidationCodedk { get; set; }
         [AllowHtml] public string RepeatEmaildk { get; set; }
         [AllowHtml] public string RepeatPhonedk { get; set; }
         [AllowHtml] public string Submitdk { get; set; }
         [AllowHtml] public string StorboxAccountIddk { get; set; }
         [AllowHtml] public string QuestionErrordk { get; set; }
         [AllowHtml] public string PhoneEmptydk { get; set; }
         [AllowHtml] public string UnknownRegistrationErrordk { get; set; }
         [AllowHtml] public string DiseaseErrordk { get; set; }
        [AllowHtml] public string NemidAllreadyRegistereddk { get; set; }
        [AllowHtml] public string StoreboxEmailAllreadyRegistereddk { get; set; }
        [AllowHtml]
        public string MessageIfStoreboxAccountExistsdk { get; set; }
        [AllowHtml]
        public string MessageIfStoreboxDoesntAccountExistsdk { get; set; }
        [AllowHtml]
        public string PleaseAcceptConditionsdk { get; set; }
        [AllowHtml] public string NemidNotFounddk { get; set; }

        [AllowHtml] public string StoreboxConsentWithField { get; set; }
        [AllowHtml] public string StoreboxConsentWithoutField { get; set; }
    }
}
