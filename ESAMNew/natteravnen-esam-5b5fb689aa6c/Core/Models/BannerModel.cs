﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace Core.Models
{
    public class BannerModel
    {
        public int Id { get; set; }
        public long FileId { get; set; }
        public string Url { get; set; }
        public int Type { get; set; }
        [AllowHtml]
        public string Title { get; set; }
        public string Urldk { get; set; }
        [AllowHtml]
        public string Titledk { get; set; }
        public long FileIddk { get; set; }
        public string Target { get; set; }
        public List<int> DisplayBannerIn { get; set; }
        public bool Active { get; set; }
    }
}
