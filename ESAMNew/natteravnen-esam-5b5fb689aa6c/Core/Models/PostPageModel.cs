﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace Core.Models
{
    public class PostPageModel
    {
        public int Id { get; set; }
        public int Section { get; set; }
        [AllowHtml]
        public string Content { get; set; }
        [AllowHtml]
        public string Title { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        [AllowHtml]
        public string Contentdk { get; set; }
        [AllowHtml]
        public string Titledk { get; set; }
        [AllowHtml]
        public string Descriptiondk { get; set; }
        public bool Published { get; set; }
        public long FileId { get; set; }
        public int DislplayIn { get; set; }
        public DateTime Date { get; set; }
    }
}
