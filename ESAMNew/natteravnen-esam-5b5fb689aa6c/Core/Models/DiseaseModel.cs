﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models
{
    public class DiseaseModel
    {
        public int Id { get; set; }
        public bool Active { get; set; }
        public string Title { get; set; }
        public string Titledk { get; set; }
    }
}
