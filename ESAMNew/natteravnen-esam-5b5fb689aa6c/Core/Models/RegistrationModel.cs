﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace Core.Models
{
    public class RegistrationModel
    {
        public bool HavePrioritzedDiseases { get; set; }
        public List<int> selecteddisease { get; set; }
        public List<string> Unlisteddisease { get; set; }
        public string nemid { get; set; }
        public string email { get; set; }
        public string emailAgain { get; set; }
        public string phone { get; set; }
        public string phoneAgain { get; set; }
        public string storeboxEmail { get; set; }
        public string storeboxPhone { get; set; }
        public bool Participate_text { get; set; }
        public bool acceptDedidentifedDataAccessed { get; set; }
        public bool contatMeIfProjectExpanded { get; set; }
        public bool sendNewsletters { get; set; }
        public string username { get; set; }
        public string validation_code { get; set; }
        [AllowHtml]
        public string AuthenticationInfo { get; set; }
        public bool StoreboxConsentWithField { get; set; }
        public string StoreboxConsentWithField_Field { get; set; }
        public bool StoreboxConsentWithoutField { get; set; }
    }
    public class registrationReturn
    {
        public int id { get; set; }
        public string error { get; set; }
        public string errorType { get; set; }
    }
}
