﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models
{

    public class QuestionnaireList
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Page { get; set; }
    }
    public class QuestionnaireModel
    {
        public int InfoId { get; set; }
        public string Title { get; set; }
        public List<string> Question { get; set; }
        public List<string> param_1 { get; set; }
        public List<string> param_2 { get; set; }
        public List<string> param_3 { get; set; }
        public List<string> param_4 { get; set; }
        public List<int> Answers { get; set; }
        public string Titledk { get; set; }
        public List<string> Questiondk { get; set; }
        public List<string> param_1dk { get; set; }
        public List<string> param_2dk { get; set; }
        public List<string> param_3dk { get; set; }
        public List<string> param_4dk { get; set; }
        public List<int> Answersdk { get; set; }
        public bool Published { get; set; }
    }
}
