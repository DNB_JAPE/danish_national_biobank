﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace Core.Models
{
    public class ContactModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        [AllowHtml]
        public string Text { get; set; }
        public string Titledk { get; set; }
        public string Addressdk { get; set; }
        public string Phonedk { get; set; }
        public string Emaildk { get; set; }
        [AllowHtml]
        public string Textdk { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public bool Published { get; set; }
        public int SectionId { get; set; }
    }
}
