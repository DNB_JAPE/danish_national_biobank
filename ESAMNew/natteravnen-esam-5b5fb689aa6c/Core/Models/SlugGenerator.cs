﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
namespace Core.Models
{
    public class SlugGenerator : AbstractOperations
    {
        public bool checkSlug(string slug)
        {
            var check = Uow.InfoTranslationRepository.Get(x => x.Slug == slug).FirstOrDefault();
            if (check == null)
            {
                return false;
            }
            return true;
        }
        public string GenerateSlug(string Title)
        {
            string pattern = @" ";
            String[] elements = Regex.Split(Title, pattern);
            string slug = DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            int index = 1;
            foreach (var item in elements)
            {
                if (index >5)
                {
                    break;
                }
                string val ="";
                foreach (char c in item)
                {
                    if (!char.IsPunctuation(c))
                        val+=c;
                }
                if(val.Equals(""))
                {

                }
                else
                {
                    slug += "-" + val;
                }
                index++;
            }
            while(checkSlug(slug))
            {
                string v = Random32();
                slug += MD5Hash(v);
            }

            return slug;
        }
        public static string MD5Hash(string input)
        {
            byte[] data = System.Security.Cryptography.MD5.Create().ComputeHash(System.Text.Encoding.UTF8.GetBytes(input));
            string md5 = "";
            for (int i = 0; i < data.Length; i++)
            {
                md5 += data[i].ToString("x2");
            }
            return md5;
        }
        public static string Random32()
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}
