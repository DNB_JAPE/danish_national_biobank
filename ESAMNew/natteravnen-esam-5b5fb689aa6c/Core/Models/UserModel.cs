﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models
{
    public class Diseases
    {
        public int Id { get; set; }
        public string Disease { get; set; }
    }
    public class UserModel
    {
        public int Id { get; set; }
        //public string Name { get; set; }
        public string Email { get; set; }
        //public int? Age { get; set; }
        public string NemId { get; set; }
        //public int? Region { get; set; }
        public string Phone { get; set; }
        public string StoreboxEmail { get; set; }
        public string StoreboxPhone { get; set; }
        public DateTime RegistrationDate { get; set; }
        public bool Active { get; set; }
        //public bool New { get; set; }
        public bool? ParticipateInProject { get; set; }
        public bool? AcceptUseData { get; set; }
        public bool? ContactMe { get; set; }
        public bool? SendMeNewsLetter { get; set; }
        //public string UsernameEmail { get; set; }
        public string ValidationCode { get; set; }
        // public string StoreboxAccount { get; set; }
        public List<Diseases> Diseases { get; set; }
        public object AuthenticationInfo { get; set; }
    }
}
