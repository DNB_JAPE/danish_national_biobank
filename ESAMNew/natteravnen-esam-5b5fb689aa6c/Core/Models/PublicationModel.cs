﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace Core.Models
{
    public class PublicationModel
    {
        public int Id { get; set; }
        public int Section { get; set; }
        [AllowHtml]
        public string Title { get; set; }
        [AllowHtml]
        public string Titledk { get; set; }
        public bool Published { get; set; }
        public long ImageId { get; set; }
        public string URL { get; set; }
        public string URLdk { get; set; }
        public DateTime Date { get; set; }
        public int FileId { get; set; }
    }
}
