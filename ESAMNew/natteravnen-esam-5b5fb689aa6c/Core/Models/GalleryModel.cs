﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace Core.Models
{

    public class GalleryModel
    {
        public int Id { get; set; }
        public int Section { get; set; }
        [AllowHtml]
        public string Title { get; set; }
        [AllowHtml]
        public string Titledk { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        [AllowHtml]
        public string Descriptiondk { get; set; }
        public bool Published { get; set; }
        public string CoverImage { get; set; }
        public string CoverImagedk { get; set; }
        public DateTime Date { get; set; }
        public List<long> GelleryImages { get; set; }
        public List<DateTime> GelleryImagesDate { get; set; }
        public List<long> GelleryImagesPublished { get; set; }
    }
}
