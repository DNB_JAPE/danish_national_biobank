﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models
{
    public class AdministratorGroupsModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<int> pages { get; set; }
    }
}
