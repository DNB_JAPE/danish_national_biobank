﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models
{
    public class Object
    {
        public int id { get; set; }
        public string className { get; set; }
        public string Description { get; set; }
    }
    public class Templates
    {
        public List<Object> main = new List<Object>() {
            new Object() { id = 1, className = "icon-th-list", Description = "Default" },
            new Object() { id = 2, className = "icon-th-large", Description = "Questionnaire" }
        };
        public List<Object> text = new List<Object>() {
            new Object() { id = 1, className = "icon-th-list", Description = "Default" },
            new Object() { id = 2, className = "icon-th-large", Description = "Pin to Questionnaire" },
            new Object() { id = 3, className = "icon-th-large", Description = "Pin to Questionnaire | with disease combo" },
            new Object() { id = 4, className = "icon-th-large", Description = "Pin to Questionnaire | Final page" },
            new Object() { id = 5, className = "icon-th-large", Description = "Pin to Questionnaire | with Slider" }
        };
        public List<Object> news = new List<Object>() {
            new Object() { id = 1, className = "icon-th-list", Description = "Default" },
            new Object() { id = 2, className = "icon-th-large", Description = "Questionnaire" },
        };
        public List<Object> list = new List<Object>() {
            new Object() { id = 1, className = "icon-th-list", Description = "With Date" },
            new Object() { id = 2, className = "icon-th-large", Description = "Questionnaire" },
        };
        public List<Object> gallery = new List<Object>(){
            new Object() { id = 1, className = "icon-th-list", Description = "Default" },
            new Object() { id = 2, className = "icon-th-large", Description = "Questionnaire" },
        };
        public List<Object> video = new List<Object>() {
            new Object() { id = 1, className = "icon-th-list", Description = "Default" },
            new Object() { id = 2, className = "icon-th-large", Description = "Questionnaire" },
        };
        public List<Object> Questionnaire = new List<Object>() {
            new Object() { id = 1, className = "icon-th-list", Description = "Default" },
            new Object() { id = 2, className = "icon-th-large", Description = "Questionnaire" },
        };
    }
   
}
