﻿using System.Collections.Generic;

namespace Core.Models
{
    public class Section
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public int TypeId { get; set; }
        public int Style { get; set; }
        public string Icon { get; set; }
        public bool Active { get; set; }
        public int Sort { get; set; }
        public int SectionTranslationId { get; set; }
        public int SectionTranslationIddk { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Namedk { get; set; }
        public string Descriptiondk { get; set; }
        public string Slug { get; set; }
        public List<int> MenuTypes{get;set;}

    }
}
