﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models
{
    public class SettingsModel
    {
        public List<int> Id { get; set; }
        public List<string> Description { get; set; }
        public List<string> Value { get; set; }
        public List<string> Slug { get; set; }
        public List<bool> Html { get; set; }
    }
}
