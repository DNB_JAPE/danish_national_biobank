﻿using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using Newtonsoft.Json;
using SPS.Client.Api.Extensions;
using SPS.Core.Api.Frame.NemID;
using IdeaCms.Filters;

namespace IdeaCms.Controllers
{
    [Localization]
    public class SecureAreaController : Controller
    {
        public JsonResult ShowResult()
        {
            ViewBag.Maintitle = "Result";
            var res = Session["NemIDResult"] as NemIDFlowResult;
            if (res == null)
            {
                FormsAuthentication.SignOut();
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            var result= JsonConvert.SerializeObject(res, Formatting.Indented);
            return Json(result,JsonRequestBehavior.AllowGet);
        }
    }
}