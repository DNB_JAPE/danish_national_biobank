using Core.Operations;
using IdeaCms.Filters;
using IdeaCms.Helpers;
using System.Web.Mvc;

namespace IdeaCms.Controllers
{
    [Localization]
    public class ManageController : Controller
    {
        AdministratorsOperations administratorOperations = new AdministratorsOperations();
        AdministratorGroupOperations administratorGroupOperations = new AdministratorGroupOperations();
        [Authorisation]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            if(Session["User"] != null)
            {
                return RedirectToAction("Control", "Structure");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Login(string userName, string password)
        {
            string HashPassword = SecurityHelper.MD5Hash(password);
            var CheckUser = administratorOperations.CheckUsers(userName, HashPassword);
            if(CheckUser==null)
            {
                return View();
            }
            else
            {
                Session["user"] = CheckUser.Id;
                return RedirectToAction("Control", "Structure");
            }
        }
        [HttpGet]
        public ActionResult Logout()
        {
            Session.Remove("User");
            return View("Login");
        }
    }
}