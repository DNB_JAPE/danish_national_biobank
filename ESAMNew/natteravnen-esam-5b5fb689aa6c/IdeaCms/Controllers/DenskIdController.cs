﻿using System.Configuration;
using System.Web.Hosting;
using System.Web.Mvc;
using SPS.Client.Frame;
using SPS.Client.Frame.NemId;
using IdeaCms.Filters;

namespace IdeaCms.Controllers
{
    [Localization]
    public class DenskIdController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Logon()
        {
            ViewBag.Maintitle = "NemID Logon";
            UpdateViewBagWithNemIdParameters(new NemIDFrameBuilder());
            return View();
        }

        public ActionResult SignPdf()
        {
            ViewBag.Maintitle = "NemID Signature (PDF)";
            var path = ConfigurationManager.AppSettings["testPdfPath"];
            if (!System.IO.File.Exists(path))
            {
                path = HostingEnvironment.MapPath(ConfigurationManager.AppSettings["testPdfPath"]);
            }
            var frameBuilder = new NemIDSignFrameBuilder()
                .SignPdf(System.IO.File.ReadAllBytes(path));

            UpdateViewBagWithNemIdParameters(frameBuilder);

            return View();
        }

        public ActionResult SignText()
        {
            ViewBag.Maintitle = "NemID Signature (Text)";
            const string messageToBeSigned = "IT-løsning fra Signaturgruppen\nLicens og assistance\n\n\nDer tilbydes kontrakt mellem parterne...";
            UpdateViewBagWithNemIdParameters(new NemIDSignFrameBuilder().SignText(messageToBeSigned));

            return View();
        }

        private void UpdateViewBagWithNemIdParameters(FrameBuilder frameBuilder)
        {
           // parent.postMessage('obj.AuthenticationInfo.Pid', 'http://localhost:50448/');
            var v = Url.Action("HandleBackendResponse", "Home");
            string test = frameBuilder.GetScript(v);

            var targetSite = ConfigurationManager.AppSettings["targetSite"];
            // window.alert(obj.message); 
            string replace = test.Replace("document.body.appendChild(f);", "document.body.appendChild(f); $.post('"+ v + "', {result: message.content}, function(data) {var obj = JSON.parse(data); if(obj.error1){ window.parent.document.dispatchEvent(new CustomEvent('nemidEventer', { cancel: true })); } else {  var iframe = document.getElementById('IFrameId'); iframe.src = iframe.src; var AuthenticationInfo = obj.AuthenticationInfo; parent.postMessage(AuthenticationInfo, '" + targetSite + "');}});");
            replace = replace.Replace("f.submit();", "");
            ViewBag.NemIdParameterBlock = replace;
            ViewBag.IframeUrl = frameBuilder.FrameUrl;
        }
    }
}