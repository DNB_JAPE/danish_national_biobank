﻿using System.Web.Mvc;
using System.Web.Security;
using SPS.Client.Api.Exceptions;
using SPS.Client.Validation.NemID;
using IdeaCms.Filters;
using System;

namespace IdeaCms.Controllers
{
    [Localization]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Maintitle = "Welcome";
            return View();
        }

        public ActionResult ex(ValidationException vEx)
        {
            return View("~/Views/Error/OnValidationException.cshtml", vEx);
        }

        /// <summary>
        /// Handles the SPS.Client response. Alternatively, use a try/catch(ValidationException)
        /// The new NemIDFlowValidator().Validate(result) call throws a SPS.Client.Api.Exceptions.ValidationException
        /// if not SUCCESS.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public ActionResult HandleBackendResponse(string result)
        {
            try
            {
                var nemidValidationResult = new NemIDFlowValidator().Validate(result);
                //No exception => SUCCESS. Log in user, get info from response.
                Session["NemIDResult"] = nemidValidationResult;
                LoginUserWithFormsAuthentication(nemidValidationResult.AuthenticationInfo.SubjectSerialNumber);
                return RedirectToAction("ShowResult", "SecureArea");
            }
            catch (ValidationException vEx)
            {
                //Log vEx.ErrorMessage - this is intended for you logs
                //vEx.FlowErrorCode is the NemID ErrorCode
                //vEx.UserMessage can be shown to the user
                string error = "{\"error1\":\"true\", \"message\":\"" + vEx.ErrorMessage + "\"}";
                
                return Json(error, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("ex", "Home",vEx);
            }

        }

        private static void LoginUserWithFormsAuthentication(string uniqueNemIdUserIdentitier)
        {
            FormsAuthentication.SetAuthCookie(uniqueNemIdUserIdentitier, false);
        }
    }
}