using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Operations;
using DAL.Context;
using IdeaCms.Models;
using IdeaCms.Filters;
using System.Globalization;
using Core.Models;
using System.Text;
using System.Web.Mvc;

namespace IdeaCms.Controllers.ManageControllers
{
    [Authorisation]
    [Permisions]
    [Localization]
    public class StructureController : Controller
    {
        SectionOperations sectionOperation = new SectionOperations();
        List<Sections> sectionList = new List<Sections>();
        List<int> listClose = new List<int>();

        public ActionResult Control()
        {
            return View(sectionOperation.GetAll().OrderBy(x => x.Sections.sort));
        }

        [HttpPost]
        public JsonResult Control(List<StructureListModel> model)
        {
            string slug;
            int sort = 1;
            foreach (var item in model)
            {
                foreach (var lst in listClose)
                {
                    if (lst == item.id)
                    {
                        break;
                    }
                }
                slug = sectionOperation.GetSectionSlug(item.id);
                var i = slug.LastIndexOf("/");
                if (i > 0)
                {
                    i = i + 1;
                    slug = slug.Substring(i, slug.Length - i);
                }
                sectionList.Add(new Sections() { Id = item.id, ParentId = null, sort = sort, Slug = slug });
                sort++;
                listClose.Add(item.id);
                if (item.children != null)
                {
                    foreach (var item2 in item.children)
                    {
                        foreach (var lst in listClose)
                        {
                            if (lst == item2.id)
                            {
                                break;
                            }
                        }

                        string slug1 = sectionOperation.GetSectionSlug(item2.id);
                        i = slug1.LastIndexOf("/");
                        if (i > 0)
                        {
                            i = i + 1;
                            slug1 = slug1.Substring(i, slug1.Length - i);
                        }
                        slug1 = slug + "/" + slug1;
                        sectionList.Add(new Sections() { Id = item2.id, ParentId = item.id, sort = sort, Slug = slug1 });
                        sort++;
                        listClose.Add(item2.id);
                        if (item2.children != null)
                        {
                            foreach (var item3 in item2.children)
                            {
                                foreach (var lst in listClose)
                                {
                                    if (lst == item3.id)
                                    {
                                        break;
                                    }
                                }
                                string slug2 = sectionOperation.GetSectionSlug(item3.id);
                                i = slug2.LastIndexOf("/");
                                if (i > 0)
                                {
                                    i = i + 1;
                                    slug2 = slug2.Substring(i, (slug2.Length) - i);
                                }
                                slug2 = slug1 + "/" + slug2;
                                sectionList.Add(new Sections() { Id = item3.id, ParentId = item2.id, sort = sort, Slug = slug2 });
                                sort++;
                                listClose.Add(item3.id);
                                if (item3.children != null)
                                {
                                    foreach (var item4 in item3.children)
                                    {
                                        foreach (var lst in listClose)
                                        {
                                            if (lst == item4.id)
                                            {
                                                break;
                                            }
                                        }
                                        string slug3 = sectionOperation.GetSectionSlug(item4.id);
                                        i = slug3.LastIndexOf("/");
                                        if (i > 0)
                                        {
                                            i = i + 1;
                                            slug3 = slug3.Substring(i, (slug3.Length) - i);
                                        }
                                        slug3 = slug2 + "/" + slug3;
                                        sectionList.Add(new Sections() { Id = item4.id, ParentId = item3.id, sort = sort, Slug = slug3 });
                                        sort++;
                                        listClose.Add(item4.id);
                                        if (item4.children != null)
                                        {
                                            foreach (var item5 in item4.children)
                                            {
                                                foreach (var lst in listClose)
                                                {
                                                    if (lst == item5.id)
                                                    {
                                                        break;
                                                    }
                                                }
                                                string slug4 = sectionOperation.GetSectionSlug(item5.id);
                                                i = slug4.LastIndexOf("/");
                                                if (i > 0)
                                                {
                                                    i = i + 1;
                                                    slug4 = slug4.Substring(i, (slug4.Length) - i);
                                                }
                                                slug4 = slug3 + "/" + slug4;
                                                sectionList.Add(new Sections() { Id = item5.id, ParentId = item4.id, sort = sort, Slug = slug4 });
                                                sort++;
                                                listClose.Add(item5.id);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                }

            }
            sectionOperation.updateSection(sectionList);
            return Json(1);

        }
        public ActionResult Delete(int id)
        {
            var result = sectionOperation.DeletePage(id);
            if (!result)
            {
                TempData["Error"] = "Unable to delete the record";
                return RedirectToAction("Control");
            }
            return RedirectToAction("Control");
        }

        public ActionResult Createpage()
        {
            ViewData["SectionTypes"] = sectionOperation.GetSectionTypes();
            ViewData["ParentList"] = sectionOperation.GetAll();
            ViewData["Menues"] = sectionOperation.GetMenues();
            return View();
        }

        [HttpPost]
        public ActionResult Createpage(Core.Models.Section section)
        {
            sectionOperation.createPage(section);
            return RedirectToAction("Control");
        }

        public ActionResult Edit(int id)
        {
            ViewData["SectionTypes"] = sectionOperation.GetSectionTypes();
            ViewData["ParentList"] = sectionOperation.GetAll();
            ViewData["Menues"] = sectionOperation.GetMenues();
            return View(sectionOperation.GetSection(id));
        }
        [HttpPost]
        public ActionResult Edit(Core.Models.Section section)
        {
            sectionOperation.UpdatePage(section);
            return RedirectToAction("Control");
        }
        [HttpPost]
        public JsonResult CheckStyle(string Id)
        {
            Templates template = new Templates();
            switch(Id)
            {
                case "Home Page":
                    return Json(template.main);
                case "Text Page":
                    return Json(template.text);
                case "Photo Gallery Page":
                    return Json(template.gallery);
                case "List Page":
                    return Json(template.list);
                case "Post":
                    return Json(template.news);
                case "Questionnaire":
                    return Json(template.Questionnaire);
                default: return Json("0");
            }
        }
    }
}