using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdeaCms.Filters;
using Core.Operations;
using System.IO;
using IdeaCms.Helpers;
using System.Web.Mvc;
using System.Web.Hosting;
using System.Web;

namespace IdeaCms.Controllers.ManageControllers
{
    [Authorisation]
    [Permisions]
    [Localization]
    public class FileManagerController : Controller
    {
        SectionOperations sectionOperations = new SectionOperations();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Pictures()
        {
            return View(sectionOperations.GetImages());
        }
        public ActionResult Files()
        {
            return View(sectionOperations.GetFiles());
        }

        [HttpGet]
        public ActionResult DeleteFile(string id)
        {
            string webroot = HostingEnvironment.ApplicationPhysicalPath;
            var uploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "wwwroot/Uploads");
            var FileName = Path.Combine(uploads, id);
            string name = Path.GetFileNameWithoutExtension(id);
            var result =  sectionOperations.DeleteFile(name);
            if(!result)
            {
                TempData["Error"] = "Unable to delete the record";
                return RedirectToAction("Files");
            }
            System.IO.File.Delete(FileName);
            return RedirectToAction("Files");
        }

        [HttpGet]
        public ActionResult DeleteImage(string id, string ext)
        {
            string webroot = HostingEnvironment.ApplicationPhysicalPath;
            var defaultuploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath , "wwwroot/images/default");
            var standartuploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath , "wwwroot/images/standard");
            var FileName = Path.Combine(standartuploads, id + "." +ext);
            var FileName2 = Path.Combine(defaultuploads, id + "." + ext);
            var result = sectionOperations.DeleteImage(id);
            if (!result)
            {
                TempData["Error"] = "Unable to delete the record";
                return RedirectToAction("Pictures");
            }
            System.IO.File.Delete(FileName);
            System.IO.File.Delete(FileName2);
            return RedirectToAction("Pictures");
        }
        [HttpPost]
        public ActionResult UploadImage(HttpPostedFileBase file, string resolution)
        {
            if(file==null)
            {
                TempData["Error"] = "Please Browse Image";
                return RedirectToAction("Pictures");
            }
            int width = 0;
            int height = 0;
            if(resolution == "787X121")
            {
                width = 787;
                height = 121;
            }
            if (resolution == "600X105")
            {
                width = 600;
                height = 105;
            }
            if (resolution == "300X463")
            {
                width = 300;
                height = 463;
            }
            if (resolution == "600X105")
            {
                width = 600;
                height = 105;
            }
            if (resolution == "2000X704")
            {
                width = 2000;
                height = 704;
            }
            if (resolution == "1280X720")
            {
                width = 1280;
                height = 720;
            }
            bool success;
            string webroot = HostingEnvironment.ApplicationPhysicalPath;
            var defaultuploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath , "wwwroot/images/default");
            var standartuploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath , "wwwroot/images/standard");
            string ext = Path.GetExtension(file.FileName);
            string name = SecurityHelper.Random32();
            while (sectionOperations.CheckFileName(name))
            {
                name = SecurityHelper.Random32();
            }
            int quality = 100;
            string fileName = name + ext;
            string filepath = Path.Combine(defaultuploads, fileName);
            try
            {
                file.SaveAs(filepath);
                success = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                success = false;
            }
            if (success)
            {
                ImageHelper.ResizeImage(filepath, standartuploads, fileName, width, height, quality);
                sectionOperations.UploadFile(name, ext, resolution, true);
                long fileId = sectionOperations.GetFile(name, ext).Id;
                return RedirectToAction("Pictures");
            }
            else
            {
                TempData["Error"] = "Something wrong";
                return RedirectToAction("Pictures");
            }
            
        }
        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            bool success;
            string webroot = HostingEnvironment.ApplicationPhysicalPath ;
            var uploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath , "wwwroot/Uploads");
            string ext = Path.GetExtension(file.FileName);
            string description = Path.GetFileNameWithoutExtension(file.FileName);
            string name = SecurityHelper.Random32();
            while (sectionOperations.CheckFileNameInfoFilesTable(name))
            {
                name = SecurityHelper.Random32();
            }
            string fileName = name + ext;
            string filepath = Path.Combine(uploads, fileName);
            try
            {
                file.SaveAs(filepath);
                success = true;
            }
            catch
            {
                success = false;
            }
            if (success)
            {
                sectionOperations.UploadFile(name, ext, description);
                long fileId = sectionOperations.GetFile(name, ext, description).Id;
                return RedirectToAction("Files");
            }
            else
            {
                TempData["Error"] = "Something wrong";
                return RedirectToAction("Files");
            }
        }
    }
}