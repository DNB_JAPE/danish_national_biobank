using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Context;
using Core.Operations;
using IdeaCms.Filters;
using Core.Models;
using System.Web.Mvc;

namespace IdeaCms.Controllers.ManageControllers
{
    [Authorisation]
    [Permisions]
    [Localization]
    public class AdministratorGroupsController : Controller
    {
        AdministratorGroupOperations administratorGroupOperations = new AdministratorGroupOperations();
        public ActionResult AdministratorGroupsList()
        {
            return View(administratorGroupOperations.Get());
        }
        public ActionResult CreateAdministratorGroup()
        {
            ViewData["Permisions"] = administratorGroupOperations.GetPages();
            return View();
        }
        [HttpPost]
        public ActionResult CreateAdministratorGroup(AdministratorGroupsModel administratorGroups)
        {
            var createGroup = administratorGroupOperations.CreateGroup(administratorGroups);
            if (createGroup)
                return RedirectToAction("AdministratorGroupsList");
            ViewData["Permisions"] = administratorGroupOperations.GetPages();
            TempData["Error"] = "Record Allready exists";
            return View();
        }
        public ActionResult UpdateAdministratorGroup(int Id)
        {
            ViewData["Permisions"] = administratorGroupOperations.GetPages();
            return View(administratorGroupOperations.GetGroupById(Id));
        }
        [HttpPost]
        public ActionResult UpdateAdministratorGroup(AdministratorGroupsModel administratorGroup)
        {
            var result = administratorGroupOperations.UpdateGroup(administratorGroup);
            if (!result)
            {
                ViewData["Permisions"] = administratorGroupOperations.GetPages();
                return View(administratorGroupOperations.GetGroupById(administratorGroup.Id));
            }
            return RedirectToAction("AdministratorGroupsList");
        }
        public ActionResult DeleteAdministratorGroup(int Id)
        {
            var result = administratorGroupOperations.DeleteGroup(Id);
            if (!result)
            {
                TempData["Error"] = "Unable to delete the record";
                return RedirectToAction("AdministratorGroupsList", "AdministratorGroups");
            }
            return RedirectToAction("AdministratorGroupsList");
        }

    }
}