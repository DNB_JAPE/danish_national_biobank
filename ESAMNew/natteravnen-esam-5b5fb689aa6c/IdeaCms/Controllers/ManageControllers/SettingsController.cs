using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Operations;
using Core.Models;
using IdeaCms.Filters;
using System.Web.Mvc;

namespace IdeaCms.Controllers.ManageControllers
{
    [Authorisation]
    [Permisions]
    [Localization]
    public class SettingsController : Controller
    {
        SettingsOperations settingsOperations = new SettingsOperations();
        public ActionResult UpdateSettings()
        {
            return View(settingsOperations.getSettings());
        }
        [HttpPost]
        public ActionResult UpdateSettings(SettingsModel model)
        {
            settingsOperations.UpdateSettings(model);
            return View(settingsOperations.getSettings());
        }
    }
}