using Core.Operations;
using IdeaCms.Filters;
using DAL.Context;
using IdeaCms.Helpers;
using System.Web.Mvc;

namespace IdeaCms.Controllers.ManageControllers
{
    [Authorisation]
    [Localization]
    public class AdministratorsController : Controller
    {
        AdministratorsOperations administratorOperations = new AdministratorsOperations();
        AdministratorGroupOperations administratorGroupOperations = new AdministratorGroupOperations();
        [Permisions]
        public ActionResult AdministratorsList()
        {
            return View(administratorOperations.GetAdministratorsList());
        }
        [Permisions]
        public ActionResult CreateAdministrator()
        {
            ViewData["AdministratorGroups"] = administratorGroupOperations.Get();
            return View();
        }
        [Permisions]
        [HttpPost]
        public JsonResult CreateAdministrator(Administrators administrator)
        {

            administrator.Password = SecurityHelper.MD5Hash(administrator.Password);
            var createAdministrator = administratorOperations.CreateAdministrator(administrator);
            if (createAdministrator)
                return Json(0);
            return Json(1);
        }
        [Permisions]
        public ActionResult UpdateAdministrator(int id)
        {
            ViewData["AdministratorGroups"] = administratorGroupOperations.Get();
            return View(administratorOperations.GetAdministratorById(id));
        }
        [Permisions]
        [HttpPost]
        public JsonResult UpdateAdministrator(Administrators administrator)
        {
            bool result = administratorOperations.UpdateAdministrator(administrator);
            if (!result)
            {
                return Json(1);
            }
            return Json(0);
        }
        [Permisions]
        public ActionResult DeleteAdministrator(int Id)
        {
            int sessionId = (int)Session["User"];
            if(sessionId.Equals(Id))
            {
                TempData["Error"] = "Unable to delete your account";
                return RedirectToAction("AdministratorsList");
            }
            administratorOperations.DeleteAdministrator(Id);
            return RedirectToAction("AdministratorsList");
        }
        [Permisions]
        public ActionResult ResetPassword(int Id)
        {
            TempData["success"] = "Password has been reset successfully. Please change password after logging. New password is: 111111";
            administratorOperations.ResetPassword(Id);
            return RedirectToAction("AdministratorsList");
        }
        public ActionResult CurrentUser(int id)
        {
            int sessionId = (int)Session["User"];
            if (!sessionId.Equals(id))
            {
                return RedirectToAction("Control", "Structure");
            }
            return View(administratorOperations.GetAdministratorById(id));
        }

        [HttpPost]
        public ActionResult CurrentUser(Administrators model)
        {
            int sessionId = (int)Session["User"];
            if (!sessionId.Equals(model.Id))
            {
                return RedirectToAction("Control", "Structure");
            }
            bool result =  administratorOperations.UpdateAdministrator(model);
            if(!result)
            {
                TempData["Error"] = "email or username is already registered";
                return RedirectToAction("CurrentUser");
            }
            return RedirectToAction("CurrentUser");
        }
        public ActionResult ChangePassword(int id)
        {
            int sessionId = (int)Session["User"];
            if (!sessionId.Equals(id))
            {
                return RedirectToAction("Control", "Structure");
            }
            ViewBag.Id = id;
            return View();
        }
        [HttpPost]
        public ActionResult ChangePassword(int id,string password,string newpassword, string confirmpassword)
        {
            int sessionId = (int)Session["User"];
            if (!sessionId.Equals(id))
            {
                return RedirectToAction("Control", "Structure");
            }
            if(string.IsNullOrEmpty(password)|| string.IsNullOrEmpty(newpassword) || string.IsNullOrEmpty(confirmpassword) )
            {
                TempData["Error"] = "Please feel all fiald.";
                return RedirectToAction("ChangePassword");
            }
            if(!newpassword.Equals(confirmpassword))
            {
                {
                    TempData["Error"] = "Password does not match the confirm password.";
                    return RedirectToAction("ChangePassword");
                }
            }
            if(!administratorOperations.CheckPassword(id,password))
            {
                {
                    TempData["Error"] = "Old password is incorrct.";
                    return RedirectToAction("ChangePassword");
                }
            }
            administratorOperations.changePassword(id, password, newpassword);
            TempData["success"] = "password was changed successfuly.";
            return RedirectToAction("CurrentUser", new { id = id });
        }
    }
}