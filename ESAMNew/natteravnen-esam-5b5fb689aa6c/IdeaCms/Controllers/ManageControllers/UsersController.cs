using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Operations;
using IdeaCms.Filters;
using System.Web.Mvc;

namespace IdeaCms.Controllers.ManageControllers
{
    [Localization]
    public class UsersController : Controller
    {
        UserOperations useroperation = new UserOperations();

        [Authorisation]
        [Permisions]
        public ActionResult UserList()
        {
            ViewBag.ValidationKey = useroperation.validationkey();
            var language = useroperation.getCurrentLanguage();
            ViewBag.Language = language.Name;
            return View(useroperation.GetUsers());
        }

        //[Authorisation]
        //[Permisions]
        //public ActionResult UpdateUser(int id)
        //{
        //    return View(useroperation.GetUserById(id));
        //}

        [Authorisation]
        [Permisions]
        [HttpPost]
        public ActionResult UpdateUser(DAL.Context.Users model)
        {
            
            useroperation.UpdateUser(model);
            return View(useroperation.GetUserById(model.Id));
        }


        //[Authorisation]
        //[Permisions]
        //public ActionResult DeleteUser(int id)
        //{
        //    useroperation.DeleteUser(id);
        //    return RedirectToAction("UserList");
        //}
        //[HttpGet]
        //[Authorisation]
        //public ActionResult Service(string id)
        //{
        //    return Json(useroperation.GetUserModel(), JsonRequestBehavior.AllowGet);
        //}
    }
}