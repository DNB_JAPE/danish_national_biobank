using System.Collections.Generic;
using System.Linq;
using Core.Operations;
using System.Globalization;
using IdeaCms.Helpers;
using Core.Models;
using System.Threading.Tasks;
using IdeaCms.Models;
using System;
using System.Web.Mvc;
using DAL.Context;
using IdeaCms.Filters;
namespace IdeaCms.Controllers
{
    [Localization]
    public class WebController : Controller
    {
        TranslateOperation translationOperation = new TranslateOperation();
        WebOperations webOperation = new WebOperations();
        SettingsOperations settingsoperation = new SettingsOperations();
        string structureSlug;
        string infoSlug;
        public async Task<ActionResult> Index(int? page)
        {
            List<Settings> settings = settingsoperation.GetSettings();
            TempData["Translate"] = translationOperation.getTranslate();
            ViewBag.Language = CultureInfo.CurrentCulture.Name;
            string url = HttpContext.Request.Path.ToString();
            string acion = getAction(url, out structureSlug, out infoSlug);
            ViewBag.infoSlug = infoSlug;
            ViewBag.structureSlug = structureSlug;
            ViewBag.CurrentSlug = getSlug(url);
            if(structureSlug==null && infoSlug !=null)
            {
                ViewBag.SectionId = webOperation.getCurrentSectionByInfoSlugId(infoSlug);
            }
            else if(structureSlug == null)
            { 

            }
            else
            {
                ViewBag.SectionId = webOperation.getCurrentSectionId(structureSlug);
            }
            int take = Convert.ToInt32(settings.Where(x => x.Description == "Home post count").FirstOrDefault().Value);
            switch (acion)
            {
                case "PostPage":
                    int pageSize = Convert.ToInt32(settings.Where(x=>x.Description== "Post per page").FirstOrDefault().Value);
                    var PostPages = webOperation.getNewsPostPages(structureSlug, false).OrderByDescending(x => x.Id);
                    if (structureSlug != null)
                    {
                        PostPages = webOperation.getNewsPostPages(structureSlug, true).OrderByDescending(x=>x.Id);
                    }
                    else
                    {
                        PostPages = webOperation.getNewsPostPages(structureSlug, false).OrderByDescending(x => x.Id);
                    }
                    if (infoSlug == null)
                    {
                        ViewData["Title"] = webOperation.SectionSlug(structureSlug);
                    }
                    else
                    {
                        ViewData["Title"] = webOperation.getNewsPostPages(structureSlug, false).Where(x => x.Slug == infoSlug).FirstOrDefault().Title;
                    }
                    ViewData["PostPage"] = PostPages.AsEnumerable();
                    return View("PostPage", await PaginatedList<InfoTranslation>.CreateAsync(PostPages, page ?? 1, pageSize));
                case "home":
                    ViewData["PostPage"] = webOperation.getNewsPostPages(structureSlug, false).ToList().OrderByDescending(x => x.Id).Take(take);
                    ViewData["TextPage"] = webOperation.getTextPages();
                    ViewData["SliderPostPages"] = webOperation.getSlidePostPages(structureSlug, false).ToList();
                    ViewData["MiddleMenuItems"] = webOperation.MiddleMenuItems();
                    ViewData["SliderBanner"] = webOperation.getSliderBanner();
                    ViewData["Sections"] = webOperation.getSections();
                    return View("Index");
                case "PhotoGallery":
                    ViewData["Galleries"] = webOperation.getGalleries();
                    ViewBag.PhotoGallery = webOperation.SectionSlug(structureSlug);
                    return View("PhotoGallery");
                case "WindrawPage":
                    ViewData["Title"] = webOperation.SectionSlug(structureSlug);
                    return View("WindrawPage");
                case "RegistrationPage":
                    ViewData["TextPage"] = webOperation.getTextPages();
                    ViewData["SliderBanner"] = webOperation.getSliderBanner();
                    ViewData["Sections"] = webOperation.getSectionsForREgistrationPage();
                    ViewData["diseases"] = webOperation.getDiseases();
                    ViewData["questionnaire"] = webOperation.getQuestionnaire();
                    return View("RegistrationPage",new RegistrationModel());
                case "PublicationsPage":
                    if (structureSlug != null)
                    {
                        ViewData["Publications"] = webOperation.GetPublicationPage(structureSlug, true);
                    }
                    else
                    {
                        ViewData["Publications"] = webOperation.GetPublicationPage(structureSlug, true);
                    }
                    if (infoSlug == null)
                    {
                        ViewData["Title"] = webOperation.SectionSlug(structureSlug);
                    }
                    else
                    {
                        ViewData["Title"] = webOperation.GetPublicationPage(structureSlug, true).Where(x => x.Slug == infoSlug).FirstOrDefault().Title;
                    }
                    return View("PublicationsPage");
                case "ContactPage":
                    ViewData["ContactPage"] = webOperation.GetContactPage();
                    return View("ContactPage");
                case "TextPage":
                    ViewData["TextPage"] = webOperation.getTextPages();
                    if(infoSlug==null)
                    {
                        ViewData["Title"] = webOperation.SectionSlug(structureSlug);
                    }
                    else
                    {
                        var textpage = webOperation.getTextPages().Where(x => x.Slug == infoSlug).FirstOrDefault();
                        //textpage.Info.Sections.Style == 4
                        ViewData["textpg"] = textpage;
                        ViewData["Title"] = textpage.Title;
                    }
                    return View("TextPage");
                case "SubscribePage":
                    ViewData["Title"] = webOperation.SectionSlug(structureSlug);
                    return View("SubscribePage");
                default:
                    ViewBag.Language = CultureInfo.CurrentCulture.Name;
                    ViewData["TextPage"] = webOperation.getTextPages();
                    ViewData["PostPage"] = webOperation.getNewsPostPages(structureSlug, false).ToList().OrderByDescending(x => x.Id).Take(take);
                    ViewData["MiddleMenuItems"] = webOperation.MiddleMenuItems();
                    ViewData["SliderPostPages"] = webOperation.getSlidePostPages(structureSlug, false).ToList();
                    ViewData["SliderBanner"] = webOperation.getSliderBanner();
                    ViewData["Sections"] = webOperation.getSections();
                    return View("Index");
            }
        }
        public string getSlug(string slug)
        {
            int count = slug.Count(f => f == '/');
            if (count <= 1)
            {
                return "home";
            }
            else
            {
                slug = slug.Substring(7, slug.Length - 7);
                return slug;
            }
        }
        public string getAction(string slug, out string structureSlug, out string infoSlug)
        {
            if (slug.LastIndexOf("/") == slug.Length - 1)
            {
                slug = slug.Substring(0, slug.Length - 1);
            }
            int count = slug.Count(f => f == '/');
            if (count <= 1)
            {
                infoSlug = null;
                structureSlug = "home";
                return "home";
            }
            slug = slug.Substring(7, slug.Length -7 );
            count = slug.Count(f => f == '/');
            if (count > 0)
            {
                var i = slug.LastIndexOf("/");
                i++;
                string info = slug.Substring(i, slug.Length - i);
                string structure = slug.Substring(0, i - 1);
                return webOperation.getAction(structure, info, out infoSlug, out structureSlug);
            }
            else
            {
                return webOperation.getAction(slug, out structureSlug, out infoSlug);
            }
        }
        public JsonResult Subscribe(string email)
        {
            if(MailHelper.IsValidEmail(email))
            {
                bool result = webOperation.Subscribe(email);
                if (result)
                {
                    return Json("1");
                }
                return Json("0");
            }
            else
            {
                return Json("2");
            }
            
        }
        public JsonResult SendMail(string Name, string Email, string Body)
        {
            List<Settings> settings = settingsoperation.GetSettings();
            string Mail = settings.Where(x => x.Description == "Smtp Mail").FirstOrDefault().Value;
            string Password = settings.Where(x => x.Description == "Smtp Password").FirstOrDefault().Value;
            string SmtpServer = settings.Where(x => x.Description == "Smtp Server").FirstOrDefault().Value;
            int port = Convert.ToInt32(settings.Where(x => x.Description == "Port").FirstOrDefault().Value);
            if (MailHelper.SendMailToServer(port, SmtpServer, Mail,Password, Body, Email, Name))
            {
                return Json("0");
            }
            else
            {
                return Json("1");
            }
        }
        public JsonResult Registration(RegistrationModel model)
        {
            var translation =  translationOperation.getTranslate();
            IDictionary<string, string> error = new Dictionary<string, string>();
            IDictionary<string, IDictionary<string, string>> errorResult = new Dictionary<string, IDictionary<string, string>>();
            if (model.email != model.emailAgain)
            {
                error["email_unique_error"] = translation.EmailUniqueError;
                errorResult["error"] = error;
                return Json(errorResult);
            }
            if(model.phone!= model.phoneAgain)
            {
                error["phone_unique_error"] = translation.UniquePhone;
                errorResult["error"] = error;
                return Json(errorResult);
            }

            
            model.Participate_text = Request.Form["Participate_text"] == "true";
            model.contatMeIfProjectExpanded = Request.Form["contatMeIfProjectExpanded"] == "false,true";
            model.sendNewsletters= Request.Form["sendNewsletters"] == "false,true";
            model.acceptDedidentifedDataAccessed=Request.Form["acceptDedidentifedDataAccessed"] == "false,true";
            model.StoreboxConsentWithField = Request.Form["StoreboxConsentWithField"] == "false,true";
            model.StoreboxConsentWithField_Field = Request.Form["StoreboxConsentWithField_Field"];
            //model.StoreboxConsentWithoutField = Request.Form["StoreboxConsentWithoutField"] == "false,true";
            registrationReturn result = webOperation.registration(model);
            if(result.id==0)
            {
                error[result.errorType] = result.error;
                errorResult["error"] = error;
                return Json(errorResult);
            }

            IDictionary<string, object> success = new Dictionary<string, object>();
            success["error"] = false;
            success["nemid"] = model.nemid == "" || model.nemid == null ? false : true;
            success["message"] = "success";
            success["Id"] = result.id;
            return Json(success);
            
        }
        [HttpPost]
        public JsonResult DeleteUser(string NemId)
        {
            if (string.IsNullOrEmpty(NemId))
            {
                return Json("0");
            }
            var windraw =  webOperation.DeleteUser(NemId);
            if(windraw)
            {
                return Json("1");
            }
            return Json("0");
        }

        [HttpPost]
        public JsonResult AdditionalInfo(string validation_code, string storbox_account_id)
        {
            IDictionary<string, object> success = new Dictionary<string, object>();
            if (validation_code != null && storbox_account_id != string.Empty && storbox_account_id != null)
            {
                //   bool result = webOperation.Update(Id, validation_code);
                bool result = webOperation.Insert(storbox_account_id, validation_code);
                if (result)
                {
                    success["error"] = false;
                }
                else
                {
                    success["error"] = true;
                }
                return Json(success);
            }
            success["error"] = true;
            return Json(success);
        }
        //[HttpPost]
        //public JsonResult AdditionalInfo(string validation_code, int Id)
        //{
        //    IDictionary<string, object> success = new Dictionary<string, object>();
        //    if (validation_code!=null && Id != 0)
        //    {
        //        //   bool result = webOperation.Update(Id, validation_code);
        //        bool result = webOperation.Insert(Id, validation_code);
        //        if (result)
        //        {
        //            success["error"] = false;
        //        }
        //        else
        //        {
        //            success["error"] = true;
        //        }
        //        return Json(success);
        //    }
        //    success["error"] = true;
        //    return Json(success);
        //}


        [HttpPost]
        public JsonResult ValidateStoreboxAccount(string email, string nemid)
        {
           
            return Json(webOperation.ValidateStoreboxAccount(email, nemid));
        }

        [HttpPost]
        public JsonResult GetRecieptPageInfo(string nemid)
        {
            IDictionary<string, object> success = new Dictionary<string, object>();
            if (nemid != null)
            {
                UserModel result = webOperation.GetRecieptPageInfo(nemid);
                //if(result!=null && result.Id > 0)
                if (result != null)
                {
                    success["error"] = false;
                    success["storeboxEmail"] = result.StoreboxEmail;
                    success["validationCode"] = result.ValidationCode;
                }
                else
                {
                    success["error"] = true;
                }
                return Json(success);
            }
            success["error"] = true;
            return Json(success);
        }


        //[HttpPost]
        //public JsonResult UpdateStoreboxAccount(string storbox_account_id, string validation_code, string code)
        //{
        //    IDictionary<string, object> success = new Dictionary<string, object>();
        //    if (storbox_account_id != null && storbox_account_id != "" && validation_code != null && validation_code != "")
        //    {
        //        if(code == SecurityHelper.MD5Hash(validation_code))
        //        {
        //            bool result = webOperation.UpdateByStorebox(storbox_account_id);
        //            if (result)
        //            {
        //                success["error"] = false;
        //                success["dont_match"] = false;
        //            }
        //            else
        //            {
        //                success["error"] = true;
        //                success["user_not"] = true;
        //                success["fill_gaps"] = false;
        //            }
        //        }
        //        else
        //        {
        //            success["error"] = false;
        //            success["dont_match"] = true;
        //        }
        //        return Json(success);
        //    }
        //    success["error"] = true;
        //    success["fill_gaps"] = true;
        //    success["user_not"] = false;
        //    return Json(success);
        //}


        [HttpPost]
        public JsonResult UpdateStoreboxAccount(string storbox_account_id, string validation_code, string nemid)
        {
            IDictionary<string, object> success = new Dictionary<string, object>();
            var result =  webOperation.Update(storbox_account_id, validation_code, nemid);
            if (result)
            {
                success["error"] = false;
            }
            else
            {
                success["error"] = true;
            }
            return Json(success);
            success["error"] = true;
            return Json(success);
        }



        [HttpPost]
        public JsonResult GetCode(string Email)
        {
            var result = webOperation.GenerateKey(Email);
            if(result == 0)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else if(result == 1)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<Settings> settings = settingsoperation.GetSettings();
                string Mail = settings.Where(x => x.Description == "Smtp Mail").FirstOrDefault().Value;
                string Password = settings.Where(x => x.Description == "Smtp Password").FirstOrDefault().Value;
                string SmtpServer = settings.Where(x => x.Description == "Smtp Server").FirstOrDefault().Value;
                int port = Convert.ToInt32(settings.Where(x => x.Description == "Port").FirstOrDefault().Value);
                var mailSent = MailHelper.SendMailToUser(port,SmtpServer,Mail,Password,result.ToString(),Email);
                if(mailSent)
                {
                    return Json(SecurityHelper.MD5Hash(result.ToString()), JsonRequestBehavior.AllowGet);
                }
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

    }
}