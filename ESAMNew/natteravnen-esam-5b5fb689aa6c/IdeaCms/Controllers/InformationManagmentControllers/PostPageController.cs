using System;
using IdeaCms.Filters;
using Core.Operations;
using Core.Models;
using System.IO;
using IdeaCms.Helpers;
using System.Web.Mvc;
using System.Web.Hosting;
using System.Web;
using System.Globalization;

namespace IdeaCms.Controllers.InformationManagmentControllers
{
    [Authorisation]
    [Permisions]
    [Localization]
    public class PostPageController : Controller
    {
        InfoManagmentOperations infoManagmentOperation = new InfoManagmentOperations();
        SectionOperations sectionOperations = new SectionOperations();
        [HttpGet]
        public ActionResult PostPagesList(int id = 0)
        {
            ViewData["Section"] = sectionOperations.GetByTypeName("Post");
            ViewData["Types"] = infoManagmentOperation.getSettingForPostPage();
            ViewBag.Id = id;
            ViewBag.Language = CultureInfo.CurrentCulture.Name;
            return View(infoManagmentOperation.GetAllPostPage(id));
        }
        public ActionResult CreatePostPage(int id)
        {
            if (id == 0)
            {
                TempData["error"] = "please choose Dislpay";
                return RedirectToAction("PostPagesList");
            }
            string resolution = "600X800";
            int type = infoManagmentOperation.GetTypeValueBySettingsId(id);
            ViewBag.Id = id;
            if (type == 7)
            {
                resolution = "1280X720";
            }
            if (type == 8)
            {
                resolution = "600X800";
            }
            ViewData["Section"] = sectionOperations.GetByTypeName("Post");
            ViewData["Images"] = sectionOperations.GetImages(resolution);
            ViewData["Settings"] = infoManagmentOperation.getSettingForPostPage();
            return View();
        }
        [HttpPost]
        public ActionResult CreatePostPage(PostPageModel model)
        {
            infoManagmentOperation.CreatePostPage(model);
            return RedirectToAction("PostPagesList");
        }
        public ActionResult UpdatePostPage(int id)
        {
            PostPageModel model = infoManagmentOperation.GetPostPageModel(id);
            string resolution = "600X800";
            int type = infoManagmentOperation.GetTypeValueBySettingsId(model.DislplayIn);
            ViewBag.Id = model.DislplayIn;
            if (type == 7)
            {
                resolution = "1280X720";
            }
            if (type == 8)
            {
                resolution = "600X800";
            }
            ViewData["Section"] = sectionOperations.GetByTypeName("Post");
            ViewData["Images"] = sectionOperations.GetImages(resolution);
            ViewData["Settings"] = infoManagmentOperation.getSettingForPostPage();
            return View(infoManagmentOperation.GetPostPageModel(id));
        }
        [HttpPost]
        public ActionResult UpdatePostPage(PostPageModel model)
        {
            infoManagmentOperation.UpdatePostPage(model);
            return RedirectToAction("PostPagesList");
        }
        public ActionResult DeletePostPage(int id)
        {
            infoManagmentOperation.DeletePostPage(id);
            return RedirectToAction("PostPagesList");
        }
        [HttpPost]
        public JsonResult UploadImage(HttpPostedFileBase file, int Type)
        {
            int width = 600;
            int height = 800;
            int type = infoManagmentOperation.GetTypeValueBySettingsId(Type);
            string resolution = "600X800";
            if (type == 7)
            {
                width = 1280;
                height = 720;
                resolution = "1280X720";
            }
            if (type == 8)
            {
                width = 600;
                height = 800;
                resolution = "600X800";
            }
            bool success;
            string webroot = HostingEnvironment.ApplicationPhysicalPath;
            var defaultuploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "wwwroot/images/default");
            var standartuploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "wwwroot/images/standard");
            string ext = Path.GetExtension(file.FileName);
            string name = SecurityHelper.Random32();
            while (sectionOperations.CheckFileName(name))
            {
                name = SecurityHelper.Random32();
            }
            int quality = 100;

            string fileName = name + ext;
            string filepath = Path.Combine(defaultuploads, fileName);
            try
            {
                file.SaveAs(filepath);
                success = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                success = false;
            }
            if (success)
            {
                ImageHelper.ResizeImage(filepath, standartuploads, fileName, width, height, quality);
                sectionOperations.UploadFile(name, ext, resolution, true);
                long fileId = sectionOperations.GetFile(name, ext).Id;
                return Json("{\"id\":" + fileId + ",\"name\":" + "\"" + name + "\"" + ",\"extension\":" + "\"" + ext + "\"}");
            }
            else
            {
                return Json("0");
            }

        }
    }
}