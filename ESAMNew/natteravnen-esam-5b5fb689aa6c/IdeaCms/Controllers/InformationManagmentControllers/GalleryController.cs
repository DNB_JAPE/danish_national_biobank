using System;
using Core.Operations;
using IdeaCms.Filters;
using Core.Models;
using System.IO;
using IdeaCms.Helpers;
using System.Web.Mvc;
using System.Web;
using System.Web.Hosting;

namespace IdeaCms.Controllers.InformationManagmentControllers
{
    [Authorisation]
    [Permisions]
    [Localization]
    public class GalleryController : Controller
    {
       
        SectionOperations sectionOperations = new SectionOperations();
        GalleyOperations galleryOperation = new GalleyOperations();
        [HttpGet]
        public ActionResult GalleryList(int id = 0)
        {
            ViewData["Section"] = sectionOperations.GetByTypeName("Photo Gallery Page");
            return View(galleryOperation.GetAll(id));
        }
        public ActionResult CreateGallery()
        {
            ViewData["Section"] = sectionOperations.GetByTypeName("Photo Gallery Page");
            ViewData["Images"] = sectionOperations.GetImages("400X300");
            return View();
        }
        [HttpPost]
        public ActionResult CreateGallery(GalleryModel model, HttpPostedFileBase file1, HttpPostedFileBase file2)
        {
            int width = 300;
            int height = 463;
            int quality = 90;
            bool success;
            bool successdk;
            string webroot = HostingEnvironment.ApplicationPhysicalPath;
            var defaultuploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "wwwroot/images/default");
            var standartuploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "wwwroot/images/standard/cover");
            if (file1 != null)
            {

                string ext = Path.GetExtension(file1.FileName);
                string name = SecurityHelper.Random32();
                while (sectionOperations.CheckGalleryCoverFileName(name))
                {
                    name = SecurityHelper.Random32();
                }
                string fileName = name + ext;
                string filepath = Path.Combine(defaultuploads, fileName);
                try
                {
                    file1.SaveAs(filepath);
                    success = true;
                }
                catch
                {
                    success = false;
                }
                if (success)
                {
                    ImageHelper.ResizeImage(filepath, standartuploads, fileName, width, height, quality);
                    model.CoverImage = fileName;
                }
            }
            else
            {
                model.CoverImage = "gallery.png";
            }
            if (file2 != null)
            {

                string extdk = Path.GetExtension(file2.FileName);
                string namedk = SecurityHelper.Random32();
                while (sectionOperations.CheckGalleryCoverFileName(namedk))
                {
                    namedk = SecurityHelper.Random32();
                }
                string fileNamedk = namedk + extdk;
                string filepathdk = Path.Combine(defaultuploads, fileNamedk);
                try
                {
                    file2.SaveAs(filepathdk);
                    successdk = true;
                }
                catch
                {
                    successdk = false;
                }
                if (successdk)
                {
                    ImageHelper.ResizeImage(filepathdk, standartuploads, fileNamedk, width, height, quality);
                    model.CoverImagedk = fileNamedk;
                }
            }
            else
            {
                model.CoverImagedk = "gallery.png";
            }
            galleryOperation.CreateGallery(model);
            return RedirectToAction("GalleryList");
        }
        
        public ActionResult UpdateGallery(int id)
        {
            ViewData["Section"] = sectionOperations.GetByTypeName("Photo Gallery Page");
            ViewData["Images"] = sectionOperations.GetImages("400X300");
            return View(galleryOperation.GetGalleryModel(id));
        }

        [HttpPost]
        public ActionResult UpdateGallery(GalleryModel model, HttpPostedFileBase file1, HttpPostedFileBase file2)
        {
            int width = 300;
            int height = 463;
            int quality = 100;
            bool success;
            bool successdk;
            string webroot = HostingEnvironment.ApplicationPhysicalPath;
            var defaultuploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "wwwroot/images/default");
            var standartuploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "wwwroot/images/standard/cover");
            if (file1 != null)
            {
                
                string ext = Path.GetExtension(file1.FileName);
                string name = SecurityHelper.Random32();
                while(sectionOperations.CheckGalleryCoverFileName(name))
                {
                    name = SecurityHelper.Random32();
                }
                string fileName = name + ext;
                string filepath = Path.Combine(defaultuploads, fileName);
                try
                {
                    file1.SaveAs(filepath);
                    success = true;
                }
                catch
                {
                    success = false;
                }
                if (success)
                {
                    ImageHelper.ResizeImage(filepath, standartuploads, fileName, width, height, quality);
                    model.CoverImage = fileName;
                }
            }
            else if(string.IsNullOrEmpty(model.CoverImage))
            {
                model.CoverImage = "gallery.png";
            }
            if (file2 != null)
            {

                string extdk = Path.GetExtension(file2.FileName);
                string namedk = SecurityHelper.Random32();
                while (sectionOperations.CheckGalleryCoverFileName(namedk))
                {
                    namedk = SecurityHelper.Random32();
                }
                string fileNamedk = namedk + extdk;
                string filepathdk = Path.Combine(defaultuploads, fileNamedk);
                try
                {
                    file2.SaveAs(filepathdk);
                    successdk = true;
                }
                catch
                {
                    successdk = false;
                }
                if (successdk)
                {
                    ImageHelper.ResizeImage(filepathdk, standartuploads, fileNamedk, width, height, quality);
                    model.CoverImagedk = fileNamedk;
                }
            }
            else if (string.IsNullOrEmpty(model.CoverImagedk))
            {
                model.CoverImagedk = "gallery.png";
            }
            galleryOperation.UpdateGallery(model);
            return RedirectToAction("GalleryList");
        }
        public ActionResult DeleteGallery(int id)
        {
            galleryOperation.DeleteGallery(id);
            return RedirectToAction("GalleryList");
        }
        [HttpPost]
        public JsonResult UploadFileAsync(HttpPostedFileBase file)
        {
            bool success;
            string webroot = HostingEnvironment.ApplicationPhysicalPath;
            var defaultuploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "wwwroot/images/default");
            var standartuploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "wwwroot/images/standard");
            string ext = Path.GetExtension(file.FileName);
            string name = SecurityHelper.Random32();
            while (sectionOperations.CheckFileName(name))
            {
                name = SecurityHelper.Random32();
            }
            int width = 400;
            int height = 300;
            int quality = 100;

            string fileName = name + ext;
            string filepath = Path.Combine(defaultuploads, fileName);
            try
            {
                file.SaveAs(filepath);
                success = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                success = false;
                
            }
            if (success)
            {
                ImageHelper.ResizeImage(filepath, standartuploads, fileName, width, height, quality);
                sectionOperations.UploadFile(name, ext, "400X300", true);
                long fileId = sectionOperations.GetFile(name, ext).Id;
                return Json("{\"id\":" + fileId + ",\"name\":" + "\"" + name + "\"" + ",\"extension\":" + "\"" + ext + "\"}");
            }
            else
            {
                return Json("0");
            }

        }


    }
}