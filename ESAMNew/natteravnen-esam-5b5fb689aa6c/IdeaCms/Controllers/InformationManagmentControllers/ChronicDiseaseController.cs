using System;
using System.Collections.Generic;
using System.Linq;
using Core.Operations;
using Core.Models;
using IdeaCms.Filters;
using System.Web.Mvc;

namespace IdeaCms.Controllers.InformationManagmentControllers
{
    [Authorisation]
    [Permisions]
    [Localization]
    public class ChronicDiseaseController : Controller
    {
        ChronicDiseaseoperations chronicDiseaseoperation = new ChronicDiseaseoperations();
        public ActionResult ChronicDiseaseList()
        {
            return View(chronicDiseaseoperation.GetDisease());
        }
        public ActionResult CreateDisease()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateDisease(DiseaseModel model)
        {
            chronicDiseaseoperation.CreateDisease(model);
            return RedirectToAction("ChronicDiseaseList");
        }
        public ActionResult UpdateDisease(int Id)
        {
            return View(chronicDiseaseoperation.GetDiseaseModel(Id));
        }
        [HttpPost]
        public ActionResult UpdateDisease(DiseaseModel model)
        {
            chronicDiseaseoperation.UpdateDisease(model);
            return RedirectToAction("ChronicDiseaseList");
        }
        public ActionResult DeleteDisease(int Id)
        {
            bool result =  chronicDiseaseoperation.DeleteDisease(Id);
            if(result)
            {
                return RedirectToAction("ChronicDiseaseList");
            }
            else
            {
                TempData["Error"] = "Unable to delete the record";
                return RedirectToAction("ChronicDiseaseList");
            }
            
        }
    }
}