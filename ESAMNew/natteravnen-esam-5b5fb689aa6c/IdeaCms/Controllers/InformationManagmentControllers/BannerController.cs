using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Operations;
using IdeaCms.Filters;
using Core.Models;
using System.IO;
using IdeaCms.Helpers;
using System.Web.Mvc;
using System.Web.Hosting;
using System.Web;

namespace IdeaCms.Controllers.InformationManagmentControllers
{
    [Authorisation]
    [Permisions]
    [Localization]
    public class BannerController : Controller
    {
        SectionOperations sectionOperations = new SectionOperations();
        BannerOperations bannerOperation = new BannerOperations();
        public ActionResult BannerList(int id = 0)
        {
            ViewData["Types"] = bannerOperation.GetTypes();
            ViewBag.Id = id;
            return View(bannerOperation.GetAllBanner(id));
        }
        public ActionResult CreateBanner(int id)
        { 
            if(id==0)
            {
                TempData["error"] = "please choose page category";
                return RedirectToAction("BannerList");
            }
            string resolution="600X800";
            int type = bannerOperation.GetTypeValueBySettingsId(id);
            ViewBag.Id = id;
            if(type == 1)
            {
                resolution = "600X105";
            }
            if(type == 2)
            {
                resolution = "2000X704";
            }
            if(type == 3)
            {
                resolution = "1280X720";
            }
            ViewData["Types"] = bannerOperation.GetTypes();
            ViewData["Images"] = sectionOperations.GetImages(resolution);
            ViewData["Section"] = sectionOperations.GetAll();
            return View();
        }
        [HttpPost]
        public ActionResult CreateBanner(BannerModel model)
        {
            bannerOperation.CreateBanner(model);
            return RedirectToAction("BannerList");
        }
        public ActionResult UpdateBanner(int id)
        {
            BannerModel model = bannerOperation.GetBannerModel(id);
            int type = bannerOperation.GetTypeValueBySettingsId(model.Type);
            string resolution = "600X800";
            ViewBag.Id = model.Type;
            if (type==1)
            {
                resolution = "600X105";
            }
            if (type==2)
            {
                resolution = "2000X704";
            }
            if (type == 3)
            {
                resolution = "1280X720";
            }
            ViewData["Types"] = bannerOperation.GetTypes();
            ViewData["Images"] = sectionOperations.GetImages(resolution);
            ViewData["Section"] = sectionOperations.GetAll();
            return View(bannerOperation.GetBannerModel(id));
        }
        [HttpPost]
        public ActionResult UpdateBanner(BannerModel model)
        {
            bannerOperation.UpdateBanner(model);
            return RedirectToAction("BannerList");
        }
        public ActionResult DeleteBanner(int id)
        {
            bannerOperation.DeleteBanner(id);
            return RedirectToAction("BannerList");
        }
        [HttpPost]
        public JsonResult UploadImage(HttpPostedFileBase file, int Type)
        {
            int width = 600;
            int height = 800;
            int type = bannerOperation.GetTypeValueBySettingsId(Type);
            string resolution = "600X800";
            if (type == 1)
            {
                resolution = "600X105";
                width = 600;
                height = 105;
            }
            if (type == 2)
            {
                resolution = "2000X704";
                width = 2000;
                height = 704;
            }
            if (type == 3)
            {
                resolution = "1280X720";
                width = 1280;
                height = 720;
            }
            bool success;
            string webroot = HostingEnvironment.ApplicationPhysicalPath;
            var defaultuploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "wwwroot/images/default");
            var standartuploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "wwwroot/images/standard");
            string ext = Path.GetExtension(file.FileName);
            string name = SecurityHelper.Random32();
            while (sectionOperations.CheckFileName(name))
            {
                name = SecurityHelper.Random32();
            }
            int quality = 100;

            string fileName = name + ext;
            string filepath = Path.Combine(defaultuploads, fileName);
            try
            {
                file.SaveAs(filepath);
                success = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                success = false;
            }
            if (success)
            {
                ImageHelper.ResizeImage(filepath, standartuploads, fileName, width, height, quality);
                sectionOperations.UploadFile(name, ext, resolution, true);
                long fileId = sectionOperations.GetFile(name, ext).Id;
                return Json("{\"id\":" + fileId + ",\"name\":" + "\"" + name + "\"" + ",\"extension\":" + "\"" + ext + "\"}");
            }
            else
            {
                return Json("0");
            }

        }
    }
}