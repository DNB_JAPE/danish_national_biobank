﻿using Core.Models;
using Core.Operations;
using IdeaCms.Filters;
using System.Web.Mvc;

namespace IdeaCms.Controllers.InformationManagmentControllers
{
    [Authorisation]
    [Permisions]
    [Localization]
    public class ContactController : Controller
    {
        // GET: Contact
        InfoManagmentOperations infoManagmentOperation = new InfoManagmentOperations();
        SectionOperations sectionOperations = new SectionOperations();
        [HttpGet]
        public ActionResult UpdateContactPage()
        {
            ViewData["Section"] = sectionOperations.GetByTypeName("Contact Page");
            return View(infoManagmentOperation.GetContactPageModel());
        }
        [HttpPost]
        public ActionResult UpdateContactPage(ContactModel model)
        {
            infoManagmentOperation.UpdateContactPage(model);
            ViewData["Section"] = sectionOperations.GetByTypeName("Contact Page");
            return View(infoManagmentOperation.GetContactPageModel(model.Id));
        }
    }
}