using System;
using Core.Operations;
using Core.Models;
using IdeaCms.Filters;
using System.IO;
using IdeaCms.Helpers;
using System.Web.Mvc;
using System.Web;
using System.Web.Hosting;

namespace IdeaCms.Controllers.InformationManagmentControllers
{
    [Authorisation]
    [Permisions]
    [Localization]
    public class TextPageController : Controller
    {
        InfoManagmentOperations infoManagmentOperation = new InfoManagmentOperations();
        SectionOperations sectionOperations = new SectionOperations();
        [HttpGet]
        public ActionResult TextPagesList(int id = 0)
        {
            ViewData["Section"] = sectionOperations.GetByTypeName("Text Page");
            return View(infoManagmentOperation.GetAllTextPage(id));
        }
        public ActionResult CreateTextPage()
        {
            bool checkTextSection = infoManagmentOperation.checkTextPageSection();
            if (checkTextSection)
            {
                ViewData["Section"] = sectionOperations.GetByTypeName("Text Page");
                ViewData["Images"] = sectionOperations.GetImages("1280X720");
                return View();
            }
            else
            {
                TempData["Error"] = "First Create Text Page Type section";
                return RedirectToAction("TextPagesList");
            }
        }
        [HttpPost]
        public ActionResult CreateTextPage(TextPageModel model)
        {
            infoManagmentOperation.CreateTextPage(model);
            return RedirectToAction("TextPagesList");
        }
        public ActionResult UpdateTextPage(int id)
        {

            ViewData["Section"] = sectionOperations.GetByTypeName("Text Page");
            ViewData["Images"] = sectionOperations.GetImages("1280X720");
            return View(infoManagmentOperation.GetTextPageModel(id));
        }
        [HttpPost]
        public ActionResult UpdateTextPage(TextPageModel model)
        {
            infoManagmentOperation.UpdateTextPage(model);
            return RedirectToAction("TextPagesList");
        }
        public ActionResult DeleteTextPage(int id)
        {
            bool result = infoManagmentOperation.DeleteTextPage(id);
            if(!result)
            {
                TempData["Error"] = "Unable to delete the record";
                return RedirectToAction("TextPagesList");
            }
            return RedirectToAction("TextPagesList");

        }

        [HttpPost]
        public JsonResult UploadFileAsync(HttpPostedFileBase file)
        {
            bool success;
            string webroot = HostingEnvironment.ApplicationPhysicalPath;
            var defaultuploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "wwwroot/images/default");
            var standartuploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "wwwroot/images/standard");
            string ext = Path.GetExtension(file.FileName);
            string name = SecurityHelper.Random32();
            while (sectionOperations.CheckFileName(name))
            {
                name = SecurityHelper.Random32();
            }
            int width = 1280;
            int height = 720;
            int quality = 100;

            string fileName = name + ext;
            string filepath = Path.Combine(defaultuploads, fileName);
            try
            {
                file.SaveAs(filepath);
                success = true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                success = false;
            }
            if (success)
            {
                ImageHelper.ResizeImage(filepath, standartuploads, fileName, width, height, quality);
                sectionOperations.UploadFile(name, ext,"1280X720",true);
                long fileId = sectionOperations.GetFile(name, ext).Id;
                return Json("{\"id\":" + fileId + ",\"name\":" + "\"" + name + "\"" + ",\"extension\":" + "\"" + ext + "\"}");
            }
            else
            {
                return Json("0");
            }

        }


    }
}