using Core.Operations;
using Core.Models;
using IdeaCms.Filters;
using System.Web.Mvc;

namespace IdeaCms.Controllers.InformationManagmentControllers
{
    [Authorisation]
    [Permisions]
    [Localization]
    public class QuestionnaireController : Controller
    {
        QuestionnaireOperations questionnaireOperations = new QuestionnaireOperations();
        public ActionResult QuestionnaireList()
        {
            return View(questionnaireOperations.GetQuestionnaireListFullModel());
        }
        public ActionResult CreateQuestionnaire()
        {
            bool check =  questionnaireOperations.checkQuestionnaireSection();
            if(check)
            {
                return View();
            }
            else
            {
                TempData["Error"] = "First Create Questionnaire Type section";
                return RedirectToAction("QuestionnaireList");
            }
            
        }
        [HttpPost]
        public ActionResult CreateQuestionnaire(QuestionnaireModel model)
        {
            questionnaireOperations.CreateQuestionarie(model);
            return RedirectToAction("QuestionnaireList");
        }
        public ActionResult UpdateQuestionnaire(int id)
        {
            return View(questionnaireOperations.GetQuestionnaireModel(id));
        }
        [HttpPost]
        public ActionResult UpdateQuestionnaire(QuestionnaireModel model)
        {
            questionnaireOperations.UpdateQuestionnaire(model);
            return RedirectToAction("QuestionnaireList");
        }
        public ActionResult DeleteQuestionnaire(int id)
        {
            questionnaireOperations.DeleteQuestionnaire(id);
            return RedirectToAction("QuestionnaireList");
        }
    }
}