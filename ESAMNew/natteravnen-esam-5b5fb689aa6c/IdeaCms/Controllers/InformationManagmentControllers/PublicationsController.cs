using System.Threading.Tasks;
using Core.Operations;
using IdeaCms.Filters;
using Core.Models;
using System.IO;
using IdeaCms.Helpers;
using System.Web.Mvc;
using System.Web;
using System.Web.Hosting;

namespace IdeaCms.Controllers.InformationManagmentControllers
{
    [Authorisation]
    [Permisions]
    [Localization]
    public class PublicationsController : Controller
    {
        InfoManagmentOperations infoManagmentOperation = new InfoManagmentOperations();
        SectionOperations sectionOperations = new SectionOperations();
        [HttpGet]
        public ActionResult PublicationsList(int id = 0)
        {
            ViewData["Section"] = sectionOperations.GetByTypeName("Publications");
            return View(infoManagmentOperation.GetAllPublications(id));
        }
        public ActionResult CreatePublication()
        {
            ViewData["Section"] = sectionOperations.GetByTypeName("Publications");
            ViewData["Images"] = sectionOperations.GetImages("300X463");
            ViewData["Files"] = sectionOperations.GetFiles();
            return View();
        }
        [HttpPost]
        public ActionResult CreatePublication(PublicationModel model)
        {
            infoManagmentOperation.CreatePublication(model);
            ViewData["Section"] = sectionOperations.GetByTypeName("Publications");
            return RedirectToAction("PublicationsList");
        }
        public ActionResult UpdatePublication(int id)
        {
            ViewData["Section"] = sectionOperations.GetByTypeName("Publications");
            ViewData["Images"] = sectionOperations.GetImages("300X463");
            ViewData["Files"] = sectionOperations.GetFiles();
            return View(infoManagmentOperation.GetPublicationsModel(id));
        }
        [HttpPost]
        public ActionResult UpdatePublication(PublicationModel model)
        {
            infoManagmentOperation.UpdatePublication(model);
            return RedirectToAction("PublicationsList");
        }
        public ActionResult DeletePublication(int id)
        {
            infoManagmentOperation.DeletePublication(id);
            return RedirectToAction("PublicationsList");
        }



        [HttpPost]
        public async Task<JsonResult> UploadFileAsync(HttpPostedFileBase file1)
        {
            bool success;
            string webroot = HostingEnvironment.ApplicationPhysicalPath;
            var uploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "wwwroot/Uploads");
            string ext = Path.GetExtension(file1.FileName);
            string description = Path.GetFileNameWithoutExtension(file1.FileName);
            string name = SecurityHelper.Random32();
            while (sectionOperations.CheckFileNameInfoFilesTable(name))
            {
                name = SecurityHelper.Random32();
            }
            string fileName = name + ext;
            string filepath = Path.Combine(uploads, fileName);
            try
            {
                file1.SaveAs(filepath);
                success = true;
            }
            catch
            {
                success = false;
            }
            if (success)
            {
                sectionOperations.UploadFile(name, ext,description);
                long fileId = sectionOperations.GetFile(name, ext,description).Id;
                return Json("{\"id\":" + fileId + ",\"name\":" + "\"" + name + "\"" + ",\"extension\":" + "\"" + ext + "\",\"description\":\""+ description +"\"}");
            }
            else
            {
                return Json("0");
            }
        }
        [HttpPost]
        public JsonResult UploadImage(HttpPostedFileBase file)
        {
            if(file==null)
            {
                return Json("0");
            }
            bool success;
            string webroot = HostingEnvironment.ApplicationPhysicalPath;
            var defaultuploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "wwwroot/images/default");
            var standartuploads = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "wwwroot/images/standard");
            string ext = Path.GetExtension(file.FileName);
            string name = SecurityHelper.Random32();
            while (sectionOperations.CheckFileName(name))
            {
                name = SecurityHelper.Random32();
            }
            int width = 300;
            int height = 463;
            int quality = 100;

            string fileName = name + ext;
            string filepath = Path.Combine(defaultuploads, fileName);
            try
            {
                file.SaveAs(filepath);
                success = true;
            }
            catch
            {
                success = false;
            }
            if (success)
            {
                ImageHelper.ResizeImage(filepath, standartuploads, fileName, width, height, quality);
                sectionOperations.UploadFile(name, ext, "300X463", true);
                long fileId = sectionOperations.GetFile(name, ext).Id;
                return Json("{\"id\":" + fileId + ",\"name\":" + "\"" + name + "\"" + ",\"extension\":" + "\"" + ext + "\"}");
            }
            else
            {
                return Json("0");
            }

        }
    }
}