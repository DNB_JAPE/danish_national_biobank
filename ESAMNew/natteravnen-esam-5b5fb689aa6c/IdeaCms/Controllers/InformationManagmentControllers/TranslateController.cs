using Core.Operations;
using Core.Models;
using IdeaCms.Filters;
using System.Web.Mvc;
using System;
using System.Resources;

namespace IdeaCms.Controllers.InformationManagmentControllers
{
    [Authorisation]
    [Permisions]
    [Localization]
    [Serializable()]
    public class TranslateController : Controller
    {
        TranslateOperation translationOperation = new TranslateOperation();
        public ActionResult Index()
        {
            return View(translationOperation.GetTranslations());
        }
        public ActionResult UpdateTranslation(TranslationModel model)
        {
            
            translationOperation.UpdateTranslation(model);
            return RedirectToAction("Index");
        }

    }
}