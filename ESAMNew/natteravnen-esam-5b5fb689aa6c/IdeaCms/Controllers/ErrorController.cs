﻿using System.Web.Mvc;
using SPS.Client.Api.Exceptions;
using IdeaCms.Filters;

namespace IdeaCms.Controllers
{
    [Localization]
    public class ErrorController : Controller
    {
        public ActionResult OnValidationException(ValidationException validationException)
        {
            ViewBag.Maintitle = "Validation Error";
            return View();
        }
    }
}