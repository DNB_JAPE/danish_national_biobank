﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IdeaCms.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class ManageLayoute {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ManageLayoute() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("IdeaCms.Resources.ManageLayoute", typeof(ManageLayoute).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Admin.
        /// </summary>
        public static string Admin {
            get {
                return ResourceManager.GetString("Admin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to banners.
        /// </summary>
        public static string banners {
            get {
                return ResourceManager.GetString("banners", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ChronicDisease.
        /// </summary>
        public static string ChronicDisease {
            get {
                return ResourceManager.GetString("ChronicDisease", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to contact_page.
        /// </summary>
        public static string contact_page {
            get {
                return ResourceManager.GetString("contact_page", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Danish.
        /// </summary>
        public static string Danish {
            get {
                return ResourceManager.GetString("Danish", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DashBoard.
        /// </summary>
        public static string DashBoard {
            get {
                return ResourceManager.GetString("DashBoard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to English.
        /// </summary>
        public static string English {
            get {
                return ResourceManager.GetString("English", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to file_manager.
        /// </summary>
        public static string file_manager {
            get {
                return ResourceManager.GetString("file_manager", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to groups.
        /// </summary>
        public static string groups {
            get {
                return ResourceManager.GetString("groups", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hello.
        /// </summary>
        public static string Hello {
            get {
                return ResourceManager.GetString("Hello", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Information_Managment.
        /// </summary>
        public static string Information_Managment {
            get {
                return ResourceManager.GetString("Information_Managment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Language.
        /// </summary>
        public static string Language {
            get {
                return ResourceManager.GetString("Language", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Links.
        /// </summary>
        public static string Links {
            get {
                return ResourceManager.GetString("Links", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Log Out.
        /// </summary>
        public static string log_out {
            get {
                return ResourceManager.GetString("log_out", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to photo_gallery.
        /// </summary>
        public static string photo_gallery {
            get {
                return ResourceManager.GetString("photo_gallery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Posts.
        /// </summary>
        public static string Posts {
            get {
                return ResourceManager.GetString("Posts", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to publications.
        /// </summary>
        public static string publications {
            get {
                return ResourceManager.GetString("publications", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Questionnaire.
        /// </summary>
        public static string Questionnaire {
            get {
                return ResourceManager.GetString("Questionnaire", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Settings.
        /// </summary>
        public static string Settings {
            get {
                return ResourceManager.GetString("Settings", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Structure.
        /// </summary>
        public static string Structure {
            get {
                return ResourceManager.GetString("Structure", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Text_Pages.
        /// </summary>
        public static string Text_Pages {
            get {
                return ResourceManager.GetString("Text_Pages", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Translate.
        /// </summary>
        public static string Translate {
            get {
                return ResourceManager.GetString("Translate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User_List.
        /// </summary>
        public static string User_List {
            get {
                return ResourceManager.GetString("User_List", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to users.
        /// </summary>
        public static string users {
            get {
                return ResourceManager.GetString("users", resourceCulture);
            }
        }
    }
}
