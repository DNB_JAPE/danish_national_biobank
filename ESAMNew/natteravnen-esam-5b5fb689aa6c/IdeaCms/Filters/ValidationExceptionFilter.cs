﻿using System.Web.Mvc;
using SPS.Client.Api.Exceptions;

namespace SPSClientDemoMVC.Filters
{
    public class ValidationExceptionFilter : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            var vEx = filterContext.Exception as ValidationException;
            if (vEx == null) return;
            
            var result = CreateActionResult(vEx);
            filterContext.Result = result;

            //Do error logging etc..

            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
        }

        protected virtual ActionResult CreateActionResult(ValidationException vEx)
        {
            var result = new ViewResult
            {
                ViewName = "~/Views/Error/OnValidationException.cshtml",
                ViewData = new ViewDataDictionary<ValidationException>(vEx)
            };
            return result;
        }
    }
}