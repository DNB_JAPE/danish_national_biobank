﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace IdeaCms.Filters
{
    public class Localization : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
           
            var userLanguage = (string)context.RouteData.Values["Culture"];
            if (string.IsNullOrEmpty(userLanguage))
            {
                var currentRegion = System.Threading.Thread.CurrentThread.CurrentCulture;
                if(currentRegion.Name.Equals("da-DK") || currentRegion.Name.Equals("en-US"))
                {

                }
                else
                {
                    System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("da-DK");
                    System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo("da-DK");
                }
            }
            else if (userLanguage.Equals("da-DK"))
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("da-DK");
                System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo("da-DK");
            }
            else if(userLanguage.Equals("en-US"))
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            }
            else
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("da-DK");
                System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo("da-DK");
            }
            base.OnActionExecuting(context);
        }
    }
}