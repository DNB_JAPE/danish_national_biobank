﻿using IdeaCms.Helpers;
using System.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace IdeaCms.Filters
{
    public class Authorisation : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var us = context.HttpContext.Session["user"];

            if (us==null)
            {
                context.Result = new RedirectToRouteResult(
                    new RouteValueDictionary { { "Controller", "Manage" }, { "Action", "Login" } });
            }
            base.OnActionExecuting(context);
        }
    }
}
