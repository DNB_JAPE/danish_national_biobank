﻿using IdeaCms.Filters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace IdeaCms
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {

            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("da-DK");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo("da-DK");

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            // TODO Remove links to Admin section
            routes.MapRoute(
                    name: "Administrators",
                    url: "{Culture}/Manage/Administrators/{action}/{id}",
                    defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Administrators", id = UrlParameter.Optional },
                    constraints: new { Culture = "[a-z]{2}-[a-z]{2}" }
                    );
            routes.MapRoute(
                    name: "Windrow",
                    url: "wb-WD/WindrawPage/DeleteUser",
                    defaults: new { controller = "Web", action= "DeleteUser", id = UrlParameter.Optional }
                    );
            routes.MapRoute(
                    name: "Api",
                    url: "{Culture}/UserListApi/{id}",
                    defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Users", action = "Service", id = UrlParameter.Optional },
                    constraints: new { Culture = "[a-z]{2}-[a-z]{2}" }
                    );
            routes.MapRoute(
                name: "AdministratorGroups",
                url: "{Culture}/Manage/AdministratorGroups/{action}/{id}",
                defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "AdministratorGroups", id = UrlParameter.Optional },
                constraints: new { Culture = "[a-z]{2}-[a-z]{2}" }
                // constraints: new { Culture = @"E-N-D-A"}
                );
            routes.MapRoute(
                name: "nemidlogin",
                url: "{Culture}/NemId/Login",
                defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "DenskId", action = "Logon", id = UrlParameter.Optional },
                namespaces: new[] { "IdeaCms.Controllers" },
                constraints: new { Culture = "[a-z]{2}-[a-z]{2}" }
            );
            routes.MapRoute(
                name: "nemid",
                url: "{Culture}/nemid/verification",
                defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "DenskId", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "IdeaCms.Controllers" },
                constraints: new { Culture = "[a-z]{2}-[a-z]{2}" }
            );
            routes.MapRoute(
                name: "Structure",
                url: "{Culture}/Manage/Structure/{action}/{id}",
                defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Structure", id = UrlParameter.Optional },
                constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });
            routes.MapRoute(
                name: "TextPage",
                url: "{Culture}/Manage/TextPage/{action}/{id}",
                defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "TextPage", id = UrlParameter.Optional },
                constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });
            routes.MapRoute(
               name: "postPage",
               url: "{Culture}/Manage/PostPage/{action}/{id}",
               defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "PostPage", id = UrlParameter.Optional },
                constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });
            routes.MapRoute(
               name: "publications",
               url: "{Culture}/Manage/Publications/{action}/{id}",
               defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Publications", id = UrlParameter.Optional },
                constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });
            routes.MapRoute(
                 name: "gallery",
                 url: "{Culture}/Manage/Gallery/{action}/{id}",
                 defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Gallery", id = UrlParameter.Optional },
                  constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });
            routes.MapRoute(
                  name: "banner",
                  url: "{Culture}/Manage/Banner/{action}/{id}",
                  defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Banner", id = UrlParameter.Optional },
                   constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });
            routes.MapRoute(
                  name: "contact",
                  url: "{Culture}/Manage/Contact/UpdateContactPage/{id}",
                  defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Contact", action = "UpdateContactPage", id = UrlParameter.Optional },
                   constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });
            routes.MapRoute(
                  name: "questionnaire",
                  url: "{Culture}/Manage/Questionnaire/{action}/{id}",
                  defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Questionnaire", id = UrlParameter.Optional },
                   constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });
            routes.MapRoute(
                  name: "users",
                  url: "{Culture}/Manage/Users/{action}/{id}",
                  defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Users", id = UrlParameter.Optional },
                   constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });
            routes.MapRoute(
                  name: "disease",
                  url: "{Culture}/Manage/ChronicDisease/{action}/{id}",
                  defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "ChronicDisease", id = UrlParameter.Optional },
                   constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });
            routes.MapRoute(
                  name: "settings",
                  url: "{Culture}/Manage/Settings/{action}/{id}",
                  defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Settings", id = UrlParameter.Optional },
                   constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });
            routes.MapRoute(
                  name: "translate",
                  url: "{Culture}/Manage/Translate/{action}/{id}",
                  defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Translate", id = UrlParameter.Optional },
               constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });
            routes.MapRoute(
                   name: "filemanager",
                   url: "{Culture}/Manage/FileManager/{action}/{id}/{ext}",
                   defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "FileManager", id = UrlParameter.Optional, ext = UrlParameter.Optional },
                constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });

            //routes.MapRoute(
            //      name: "manageindex",
            //      url: "{Culture}/Manage/Translate/{action}/{id}",
            //      defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Manage" },
            //   constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });
            //routes.MapRoute(
            //  name: "default language router",
            //  url: "{Language}/{controller=Home}/{action=Index}/{id}", defaults:new { Language = ""});
            routes.MapRoute(
                 name: "Registration",
                 url: "{Culture}/Ajax/Registration",
                 defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Web", action = "Registration" },
                 constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });
            routes.MapRoute(
                 name: "AdditionalInfo",
                 url: "{Culture}/Ajax/AdditionalInfo",
                 defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Web", action = "AdditionalInfo" },
                 constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });
            routes.MapRoute(
                 name: "GetRecieptPageInfo",
                 url: "{Culture}/Ajax/GetRecieptPageInfo",
                 defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Web", action = "GetRecieptPageInfo" },
                 constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });

            routes.MapRoute(
                 name: "AdditionalInfoInfoTxtPage",
                 url: "{Culture}/Ajax/AdditionalInfoTxtPage",
                 defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Web", action = "UpdateStoreboxAccount" },
                 constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });

            routes.MapRoute(
                 name: "ValidateStoreboxAccount",
                 url: "{Culture}/Ajax/ValidateStoreboxAccount",
                 defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Web", action = "ValidateStoreboxAccount" },
                 constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });

            routes.MapRoute(
                 name: "GetCode",
                 url: "{Culture}/Ajax/GetCode",
                 defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Web", action = "GetCode" },
                 constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });
            routes.MapRoute(
                name: "web",
                url: "{Culture}/{*string}",
                defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Web", action = "Index" },
                constraints: new { Culture = "[a-z]{2}-[a-z]{2}" });
            
            routes.MapRoute(
                 name: "localization1",
                 url: "Manage/{controller}/{action}/{id}",
                 namespaces: new[] { "IdeaCms.Controllers" },
                 defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Structure", action = "Control", id = UrlParameter.Optional });
            

            routes.MapRoute(
                name: "default",
                url: "Manage/{action}/{id}",
                defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Manage", action = "Login", id = UrlParameter.Optional });
            routes.MapRoute(
                name: "allcases",
                url: "{*string}",
                defaults: new { Culture = CultureInfo.CurrentCulture.Name, controller = "Web", action = "Index" });
        }
    }
}
