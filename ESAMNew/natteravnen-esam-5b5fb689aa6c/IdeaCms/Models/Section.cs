﻿using DAL.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IdeaCms.Models
{
    public class Section
    {
        public int Id { get; set; }
        [Required]
        public int? ParentId { get; set; }
        [Required]
        public int TypeId { get; set; }
        public int Style { get; set; }
        [Required]
        public string Icon { get; set; }
        public bool Active { get; set; }
        public int Sort { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        public string Namedk { get; set; }
        public string Descriptiondk {get;set;}
        [Required]
        public string Slug { get; set; }
        public List<int> MenuTypes{get;set;}

    }
}
