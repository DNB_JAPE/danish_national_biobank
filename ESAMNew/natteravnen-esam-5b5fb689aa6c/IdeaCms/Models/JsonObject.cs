﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdeaCms.Models
{
    public class StructureListModel
    {
        public int id { get; set; }
        public List<StructureListModel> children { get; set; }
    }

    public class JsonObject : IEnumerable
    {
        public int id { get; set; }
        public string children { get; set; }
        public IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}