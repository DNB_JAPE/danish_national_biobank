﻿using IdeaCms.Helpers.Common;
using System;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Optimization;
using System.Web.Http;
using WebApiAuthorization;

namespace IdeaCms
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            // API authorization registration.  
            GlobalConfiguration.Configuration.MessageHandlers.Add(new AuthorizationHeaderHandler());

        }
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            
        }

    }
}
