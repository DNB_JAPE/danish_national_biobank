﻿/**
 * Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

/* exported initSample */

if ( CKEDITOR.env.ie && CKEDITOR.env.version < 9 )
	CKEDITOR.tools.enableHtml5Elements( document );

// The trick to keep the editor in the sample quite small
// unless user specified own height.
CKEDITOR.config.height = 150;
CKEDITOR.config.width = 'auto';

var initSample = ( function() {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get( 'bbcode' );

    return function () {
       

        var editorElement = CKEDITOR.document.getById('Title');
		// Depending on the wysiwygare plugin availability initialize classic or inline editor.
		if ( wysiwygareaAvailable ) {
            CKEDITOR.replace( 'Title' );
		} else {
			editorElement.setAttribute( 'contenteditable', 'true' );
            CKEDITOR.inline( 'Title' );

			// TODO we can consider displaying some info box that
			// without wysiwygarea the classic editor may not work.
        }
        var editorElementdk = CKEDITOR.document.getById('Titledk');
        // Depending on the wysiwygare plugin availability initialize classic or inline editor.
        if (wysiwygareaAvailable) {
            CKEDITOR.replace('Titledk');
        } else {
            editorElementdk.setAttribute('contenteditable', 'true');
            CKEDITOR.inline('Titledk');

            // TODO we can consider displaying some info box that
            // without wysiwygarea the classic editor may not work.
        }
        //var Description = CKEDITOR.document.getById('Description');
        //// Depending on the wysiwygare plugin availability initialize classic or inline editor.
        //if (wysiwygareaAvailable) {
        //    CKEDITOR.replace('Description');
        //} else {
        //    Description.setAttribute('contenteditable', 'true');
        //    CKEDITOR.inline('Description');

        //    // TODO we can consider displaying some info box that
        //    // without wysiwygarea the classic editor may not work.
        //}
        //var Descriptiondk = CKEDITOR.document.getById('Descriptiondk');
        //// Depending on the wysiwygare plugin availability initialize classic or inline editor.
        //if (wysiwygareaAvailable) {
        //    CKEDITOR.replace('Descriptiondk');
        //} else {
        //    Descriptiondk.setAttribute('contenteditable', 'true');
        //    CKEDITOR.inline('Descriptiondk');

        //    // TODO we can consider displaying some info box that
        //    // without wysiwygarea the classic editor may not work.
        //}
        //var Content = CKEDITOR.document.getById('Content');
        //// Depending on the wysiwygare plugin availability initialize classic or inline editor.
        //if (wysiwygareaAvailable) {
        //    CKEDITOR.replace('Content');
        //} else {
        //    Content.setAttribute('contenteditable', 'true');
        //    CKEDITOR.inline('Content');

        //    // TODO we can consider displaying some info box that
        //    // without wysiwygarea the classic editor may not work.
        //}
        //var Contentdk = CKEDITOR.document.getById('Contentdk');
        //// Depending on the wysiwygare plugin availability initialize classic or inline editor.
        //if (wysiwygareaAvailable) {
        //    CKEDITOR.replace('Contentdk');
        //} else {
        //    Contentdk.setAttribute('contenteditable', 'true');
        //    CKEDITOR.inline('Contentdk');

        //    // TODO we can consider displaying some info box that
        //    // without wysiwygarea the classic editor may not work.
        //}
	};

	function isWysiwygareaAvailable() {
		// If in development mode, then the wysiwygarea must be available.
		// Split REV into two strings so builder does not replace it :D.
		if ( CKEDITOR.revision == ( '%RE' + 'V%' ) ) {
			return true;
		}

		return !!CKEDITOR.plugins.get( 'wysiwygarea' );
	}
} )();

