(function() {
  var addDisease, validate;

  validate = {
    email: function(email) {
      var re;
      re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    },
    emptyfields: function() {
      var errors;
      if ($("input[name=contactinfo_optional]").val() === 0) {
        return false;
      }
      errors = [];
      if ($("#email").val() === "") {
        errors.push($('#email').attr('data-error-empty'));
      }
      if ($("#phone").val() === "") {
        errors.push($('#phone').attr('data-error-empty'));
      }
      return errors;
    },
    generateError: function(arr) {
      var i, len, outerWrap, val;
      outerWrap = $("<div class=\"alert alert-block alert-danger\">").append($("<ul>").css({
        'padding-left': 40,
        'font-size': 8
      })).prepend('<button type="button" class="close" data-dismiss="alert" style="font-size:20px"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>');
      for (i = 0, len = arr.length; i < len; i++) {
        val = arr[i];
        outerWrap.find('ul').append("<li>" + val + "</li>");
      }
      return outerWrap;
    },
    checkDisease: function() {
      return $("input[name=\"selectedDiseases[]\"]").length === 0 && !$('#HavePrioritzedDiseases').is(":checked");
    },
    emailAgain: function() {
      return $("#email").val() !== $('#emailAgain').val() && $("#email").val() !== '';
    },
    phoneAgain: function() {
      return $("#phone").val() !== $('#phoneAgain').val() && $('#phone').val() !== '';
    }
  };

  $("#registration-form").submit(function(e) {
    e.preventDefault();
    return $.ajax({
      type: 'POST',
      url: '',
      data: $('#registration-form').serialize(),
      success: function(data) {
        if (data === null) {
          alert('there was an error on server try again later');
          return;
        }
        if (data.error === false) {
          window.location.hash = 'final';
        } else if (data.error.questionary !== null) {
          window.location.hash = 'errors';
        } else if (data === true) {
          window.location.hash = '1';
        } else {
          $('.btn-submit i').remove();
          $('.btn-submit').removeAttr('disabled');
        }
        console.log(data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert("aaaa");
        $('.btn-submit i').remove();
        $('.btn-submit').removeAttr('disabled');
      }
    });
  });

  $(".btn-submit").click(function(e) {
    var arr, new_window, questcheck;
    e.preventDefault();
    arr = validate.emptyfields();
    if (!validate.email($('#email').val()) && $('#email').val() !== "") {
      arr.push($('#email').attr('data-error'));
    }
    if (validate.emailAgain()) {
      arr.push($('#emailAgain').attr('data-error'));
    }
    if (validate.phoneAgain()) {
      arr.push($('#phoneAgain').attr('data-error'));
    }
    if (validate.checkDisease()) {
      arr.push('please select diseases');
    }
    questcheck = true;
    $('div[data-step=\'questionnaire\'] input[type=\'radio\']:checked').each(function() {
      if ($(this).attr('data-correct') !== '1') {
        questcheck = false;
        $(this).next().css('color', 'red');
      }
    });
    if (!questcheck) {
      arr.push($('div[data-step="questionnaire"]').attr('data-error'));
    }
    if ($('div[data-step=\'questionnaire\'] input[type=\'radio\']:checked').length === 0) {
      arr.push("Please fill querstionarie");
    }
    if (arr.length > 0) {
      validate.generateError(arr).insertBefore(this);
      return;
    }
    if ($('#nemid').val() === '') {
        new_window = window.open('/en-US/nemid/verification', '', 'width=500,height=300');
      return new_window.onbeforeunload = function() {
        var pid;
        pid = $(new_window.document.getElementById('pid')).val();
        if (pid !== '') {
          $('#nemid').val(pid);
        }
        if ($('#nemid').val() !== '') {
          $('.registration-step').not('div[data-step=disease]').hide();
          $('div[data-step=disease]').children().not($('div[data-step=disease]').children().eq(0)).hide();
          $('div[data-step=disease]').children().children().hide();
          return $('.last.row').show();
        } else {
          return validate.generateError(['Validate nemid']).insertBefore(".btn-submit");
        }
      };
    } else {
      $('.registration-step').not('div[data-step=disease]').hide();
      $('div[data-step=disease]').children().not($('div[data-step=disease]').children().eq(0)).hide();
      $('div[data-step=disease]').children().children().hide();
      return $('.last.row').show();
    }
  });

  $('#HavePrioritzedDiseases').change(function() {
    return $("#diseasesListContainer").slideToggle();
  });

  $('#addDiseaseButton').click(function(evt) {
    var selectedDiseaseId, selectedOption;
    evt.preventDefault();
    selectedDiseaseId = parseInt($('#diseasesListContainer .combobox-selected > input').attr('value'));
    selectedOption = $('#diseasesListContainer select option[value="' + selectedDiseaseId + '"]');
    addDisease(selectedDiseaseId, selectedOption.text());
    selectedOption.prop('disabled', true);
    $('#diseasesSelect').data('combobox').clearElement();
    $('#diseasesSelect').data('combobox').clearTarget();
    $('#diseasesSelect').data('combobox').refresh();
  });

  addDisease = function(id, name) {
    var selectedDisease;
    if (!isNaN(id) && $('#selected' + id).length === 0) {
      selectedDisease = $('<div class="selected-disease" id="selected' + id + '"><div class="form-group"><div class="input-group">' + '<input class="form-control" type="text" value="' + name + '" readonly/>' + '<input type="hidden" name="selectedDiseases[]" value="' + id + '" readonly/>' + '</div><button class="btn btn-link">Delete</button>' + '</div></div>');
      selectedDisease.find('button').click(function(e) {
        var container;
        e.preventDefault();
        container = $(this).closest('.selected-disease');
        id = container.find('input:hidden').attr('value');
        container.remove();
        $('#diseasesListContainer select option[value="' + id + '"]').prop('disabled', false);
        $('#diseasesSelect').data('combobox').refresh();
      });
      $('#selectedDiseasesContainer').append(selectedDisease);
    }
  };

  $('#addNotListedDiseaseButton').click(function(e) {
    var parentElelent;
    e.preventDefault();
    if ($(this).prev().find('input').val() !== '') {
      parentElelent = $('<div class=\'selected-disease\'></div>');
      $(this).parent().clone().appendTo(parentElelent);
      parentElelent.find('input[type="text"]').attr('readonly', '').attr('name', 'selectedDiseases[]');
      parentElelent.find('button').remove();
      parentElelent.append('<button class="btn btn-link">');
      parentElelent.find('button').text('Delete');
      parentElelent.find('button').click(function() {
        $(this).parent().remove();
      });
      $('#selectedDiseasesContainer').append(parentElelent);
      $(this).prev().find('input').val('');
    }
  });

  $('#isSome').change(function() {
    if (this.checked) {
      $('#storeboxAccountInfo').slideUp('fast', function() {
        $('#storeboxEmail').val($('#email').val());
        $('#storeboxEmailAgain').val($('#emailAgain').val());
        $('#storeboxPhone').val($('#phone').val());
        $('#storeboxPhoneAgain').val($('#phoneAgain').val());
      });
    } else {
      $('#storeboxEmail').val('');
      $('#storeboxEmailAgain').val('');
      $('#storeboxPhone').val('');
      $('#storeboxPhoneAgain').val('');
      $('#storeboxAccountInfo').slideDown('fast');
    }
  });

  $('#email').on('keyup paste', function() {
    if ($('#isSome')[0].checked) {
      $('#storeboxEmail').val($(this).val());
    }
  });

  $('#emailAgain').on('keyup paste', function() {
    if ($('#isSome')[0].checked) {
      $('#storeboxEmailAgain').val($(this).val());
    }
  });

  $('#phone').on('keyup paste', function() {
    if ($('#isSome')[0].checked) {
      $('#storeboxPhone').val($(this).val());
    }
  });

  $('#phoneAgain').on('keyup paste', function() {
    if ($('#isSome')[0].checked) {
      $('#storeboxPhoneAgain').val($(this).val());
    }
  });

  $(window).ready(function() {
    return $('.combobox').combobox();
  });

  window.onhashchange = function() {
    var currentStep, returnTo1;
    currentStep = window.location.href.split('#')[1];
    returnTo1 = false;
    if (currentStep === 'final') {
      $('.registration-step').hide();
      $('div[data-step="final"]').show();
    }
  };

  $('#phone, #phoneAgain, #storeboxPhone, #storeboxPhoneAgain').mask($('#mobile-format').val() + '99999999', {
    placeholder: $('#mobile-format').val() + 'XXXXXXXX'
  }).focus(function() {
    setCaretToPos(this, 5);
  });

}).call(this);
