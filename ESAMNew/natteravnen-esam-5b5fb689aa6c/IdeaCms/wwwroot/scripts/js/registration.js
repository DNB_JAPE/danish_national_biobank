
//$('#phone, #phoneAgain, #storeboxPhone, #storeboxPhoneAgain').mask($("#mobile-format").val() + "99999999").focus(function () {
//    setCaretToPos(this, 5);
//});
$(".registration-step .comment").hide();
var currentStep = 'undefined';

var validationSteps = {
    "questionnaire": function () {

        var Status = true;

        $.each($('.type-21.required'), function (index, value) {

            if (!radioButtonsValidation($("input:radio[name=" + $(value).data('name') + "]"))) {

                Status = false;
            }
        });
        $('.type-21.required input[type="radio"]').each(function () {
            if (this.checked == true && $(this).attr('data-correct') == '') {
                Status = false;
            }
        });

        if (Status) {
            $(".registration-step .comment").hide();
            return true;
        }

        if ($('.registration-step[data-step="questionnaire"] .alert').length == 0)
            $('.registration-step[data-step="questionnaire"] button.btn-next').before(
                validationMessage("To continue please check answers above!")
            );
        $(".registration-step .comment").show();
        return false;
    },

    "disease": function () {


        var result = true;
        if (!document.forms["registration-form"]["HavePrioritzedDiseases"].checked &&
            (document.forms["registration-form"]["selecteddisease"] == null || document.forms["registration-form"]["selecteddisease"].length == 0) &&
            (document.forms["registration-form"]["Unlisteddisease"] == null || document.forms["registration-form"]["Unlisteddisease"].length == 0)) {

            if ($('.registration-step[data-step="disease"] .alert').length == 0) {
                $('.registration-step[data-step="disease"] button.btn-next').before(
                    validationMessage("To continue please choes diseases!")
                );
            }
            
            result = false;
        }
        
        //if (!result)
        //    $('.registration-step[data-step="contact-information"] .alert').remove();
        //$('.registration-step[data-step="contact-information"] button.btn-submit').before(
        //    validationMessage(message)
        //);

        return result;



    },
    "contact_information": function () {
        var mailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        var phoneReg = /^([+]46)\s*(7\d{1})\s*(\d{4})\s*(\d{3})$/;

        var email = document.forms["registration-form"]["email"].value;
        var emailAgain = document.forms["registration-form"]["emailAgain"].value;
        var phone = document.forms["registration-form"]["phone"].value;
        var phoneAgain = document.forms["registration-form"]["phoneAgain"].value;

        var isStoreboxAccount = document.forms["registration-form"]["isStoreboxAccount"].value;
        var isSome = document.forms["registration-form"]["isSome"].checked;

        var storeboxEmail = document.forms["registration-form"]["storeboxEmail"].value;
        var storeboxEmailAgain = document.forms["registration-form"]["storeboxEmailAgain"].value;
        var storeboxPhone = document.forms["registration-form"]["storeboxPhone"].value;
        var storeboxPhoneAgain = document.forms["registration-form"]["storeboxPhoneAgain"].value;

        var message = "";

        var result = true;


        if (!validateEmail(email)) {
            $('#email').after(
                validationMessage("Email fromat isn't valid")
            );
            $('html, body').animate({
                scrollTop: $("#email").offset().top
            });
            result = false;
        }


        if ($('[name="isStoreboxAccount"]:checked').val() == "1" && $('[name="isSome"]:checked').val() != "1" ) {
            if (!validateEmail(storeboxEmail)) {
                $('#storeboxEmail').after(
                    validationMessage("Email fromat isn't valid")
                );
                $('html, body').animate({
                    scrollTop: $("#storeboxEmail").offset().top
                });
                result = false;
            }
        }


        //if (!result)
        //    $('.registration-step[data-step="contact-information"] .alert').remove();
        //$('.registration-step[data-step="contact-information"] button.btn-submit').before(
        //    validationMessage(message)
        //);
        //console.log(result);
        return result;

    },

    "terms": function () {
        var result = true;
        var Participate_text = document.forms["registration-form"]["Participate_text"];
        var acceptDedidentifedDataAccessed = document.forms["registration-form"]["acceptDedidentifedDataAccessed"];
        var contatMeIfProjectExpanded = document.forms["registration-form"]["contatMeIfProjectExpanded"];
        var sendNewsletters = document.forms["registration-form"]["sendNewsletters"];
        var terms_validation_text = document.forms["registration-form"]["terms_validation_text"];
        //if ((Participate_text.length > 0 && !$(Participate_text).is(":checked")) || (acceptDedidentifedDataAccessed.length > 0 && !$(acceptDedidentifedDataAccessed).is(":checked")) || (contatMeIfProjectExpanded.length > 0 && !$(contatMeIfProjectExpanded).is(":checked")) || (sendNewsletters.length > 0 && !$(sendNewsletters).is(":checked"))) {
        //    result = false
        //}
        if (!result) {
            $(terms_validation_text).after(
                validationMessage(terms_validation_text.value)
            );
        }
        return result;
    }
};

$(document).ready(function () {
    $("#additionalInfo").click(function (e) {
        e.preventDefault();
        var storbox_account_id = $(this).parent().find('input[name=storbox_account_id]').val();
        var validation_code = $(this).parent().find('input[name=validation_code]').val();
        //storbox_account_id
        
        //var Id = $("#Id").val();
        //var nemId = $(this).find('input[name=storbox_account_id]').val();
        var $this = $(this);
        var culture = $("#culture").val();
        $.ajax({
            type: "POST",
            url: "/" + culture + "/Ajax/AdditionalInfo",
            data: { validation_code: validation_code, storbox_account_id: storbox_account_id },
            success: function (data) {
                console.log(data);
                if (!data.error) {
                    $this.parent().slideUp();
                } else {
                    $this.parent().find('.col').prepend('<div class="alert alert-danger">' + $("#unknown_error").val() + '</div>');
                }
            }
        });
    });


    currentStep = parseInt($('.registration-step.active').attr('data-step'));

    $('.combobox').combobox();

    $('.btn-next').click(function (evt) {

        evt.preventDefault();
        if (!$(this).is('.blocked'))
            nextStep();
    });

    $('.btn-submit').click(function (evt) {
        evt.preventDefault();
        $(".alert-danger").remove();
        finish();
    });


    $('.button--submit').click(function (e) {
        e.preventDefault();
        if (checkStuff()) {
            e.stopPropagation();
        };
    });

    $('#HavePrioritzedDiseases').change(function () {
        if (this.checked) {
            $('#diseasesListContainer').hide('slow');
        } else {
            $('#diseasesListContainer').show('slow');
        }
    });

    $('#isStoreboxAccountY').change(function () {
        if (this.checked) {
            $('#storeboxAccount').show('slow');
        }
    });

    $('#isStoreboxAccountN').change(function () {
        if (this.checked) {
            $('#storeboxAccount').hide('slow');
        }
    });

    $('#isSome').change(function () {
        if (this.checked) {
            $('#storeboxAccountInfo').slideUp('fast', function () {

                $("#storeboxEmail").val($('#email').val());
                $("#storeboxEmailAgain").val($('#emailAgain').val());
                $("#storeboxPhone").val($("#phone").val());
                $("#storeboxPhoneAgain").val($("#phoneAgain").val());

            });
        } else {
            $("#storeboxEmail").val("");
            $("#storeboxEmailAgain").val("");
            $("#storeboxPhone").val("");
            $("#storeboxPhoneAgain").val("");

            $('#storeboxAccountInfo').slideDown('fast');
        }
    });

    $('#addDiseaseButton').click(function (evt) {
        evt.preventDefault();

        var selectedDiseaseId = parseInt($('#diseasesListContainer .combobox-selected > input').attr('value'));
        var selectedOption = $('#diseasesListContainer select option[value="' + selectedDiseaseId + '"]');

        addDisease(selectedDiseaseId, selectedOption.text());

        selectedOption.prop("disabled", true);

        $('#diseasesSelect').data('combobox').clearElement();
        $('#diseasesSelect').data('combobox').clearTarget();
        $('#diseasesSelect').data('combobox').refresh();
    });
});


$("#addNotListedDiseaseButton").click(function (e) {
    e.preventDefault();
    if ($(this).prev().find("input").val() != "") {
        var parentElelent = $("<div class='selected-disease'></div>");
        $(this).parent().clone().appendTo(parentElelent);
        parentElelent.find('input[type="text"]').attr('readonly', '').attr('name', 'Unlisteddisease');
        parentElelent.find('button').remove();
        parentElelent.append('<button class="btn btn-link remove-disease">');
        parentElelent.find('button').text('X');
        parentElelent.find('button').click(function () {
            $(this).parent().remove();
        });

        $("#selectedDiseasesContainer").append(parentElelent);
        $(this).prev().find('input').val("");
    }
});

function finish() {
    if (currentStep != null) {
        $(".alert-danger").remove();
        if (validationSteps[currentStep] != null && !validationSteps[currentStep]()) return;

        //submit form

        var questcheck = true;

        $("div[data-step='questionnaire'] input[type='radio']:checked").each(function () {
            if ($(this).attr('data-correct') != '1') {
                questcheck = false;
                $(this).next().css('color', 'red');
            }
        });

        if (!questcheck) {
            $('html, body').animate({
                scrollTop: $("div[data-step='questionnaire']").offset().top
            });
            $('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + $('div[data-step="questionnaire"]').attr('data-error') + '</div>').insertAfter($("div[data-step='questionnaire'] h1"));
            return false;
        }

        if (!validationSteps.questionnaire()) {
            $('html, body').animate({
                scrollTop: $("div[data-step='questionnaire']").offset().top
            });

            return false;
        }

        if (!checkStuff()) {
            return false;
        }

        $("div[data-step='disease'] .alert").remove();
        if (!validationSteps.disease()) {
            $('html, body').animate({
                scrollTop: $("div[data-step='disease']").offset().top
            });
            $(validationMessage($('#diseasesListContainer').attr('data-error'))).insertBefore($("#selectedDiseasesContainer")).eq(1);
            return false;
        }

        if (!validationSteps.contact_information()) {
            return false;
        }

        if (!validationSteps.terms()) {
            return false;
        }



        // $('.registration-step.active').removeClass('active');
        //if ($("#nemid").val() == '') {
        //    //var new_window = window.open('/en-US/nemid/verification', '', "width=500,height=300");
        //    //new_window.onbeforeunload = function () {
        //    //    var pid = null;
        //    //    pid = $(new_window.document.getElementById("pid")).val();
        //    //    if (pid != '') {
        //    //        $("#nemid").val(pid);
        //    //    }

        //    //    if ($('#nemid').val() != '') {
        //    //        $('#registration-form').submit();

        //    //    }
        //    //    else {
        //    //        $('form .alert-danger').remove();
        //    //        //$(validationMessage('Please verify nemid')).insertBefore('.btn-submit');
        //    //    }
        //    //}
        //    $(validationMessage($('#submit_nemid').attr('data-error'))).insertBefore($("#submit_nemid")).eq(1);
        //} else {
        //    $('#registration-form').submit();

        //}

        $('#registration-form').submit();

        // if(pid !== false) {
        //   $('#registration-form').submit();
        // }
        // else {
        //   validationMessage('Login to nemid');
        // }
    }
}

function nextStep() {
    if (currentStep != null) {
        if (validationSteps[currentStep] != null && !validationSteps[currentStep]()) return;

        if (currentStep != 6)
            currentStep = $(".registration-step.active").next().attr('data-step');
        window.location.hash = currentStep;
    }
}

function prevStep() {
    if ($(".registration-step.active").prev().length != 0) {
        currentStep = $(".registration-step.active").prev().attr('data-step');
        window.location.hash = currentStep;
        return false;
    }
}


$('#contact_info').change(function () {
    $('#contact-info-optional').toggle('slow');
});

function addDisease(id, name) {
    if (!isNaN(id) && $('#selected' + id).length == 0) {

        var selectedDisease = $('<div class="selected-disease" id="selected' + id + '"><div class="form-group"><div class="input-group">' +
            '<input class="form-control" type="text" value="' + name + '" readonly/>' +
            '<input type="hidden" name="selecteddisease" value="' + id + '" readonly/>' +
            '</div><button onclick="removeSelectedDisease(this, event);" class="btn btn-link remove-disease">X</button>' +
            '</div></div>');
        $('#selectedDiseasesContainer').append(selectedDisease);
    }
}

function removeSelectedDisease(element, evt) {
    evt.preventDefault();
    var container = $(element).closest(".selected-disease");
    var id = container.find('input:hidden').attr("value");

    container.remove();

    $('#diseasesListContainer select option[value="' + id + '"]').prop("disabled", false);
    $('#diseasesSelect').data('combobox').refresh();
}

function validationMessage(message) {
    if (message == '' || message == null) {
        return '';
    }

    return '<div class="alert alert-danger">' +
        '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
        message +
        '</div>';
}

function radioButtonsValidation(radios) {
    for (i = 0; i < radios.length; ++i) {
        if (radios[i].checked) return true;
    }
    return false;
}

function setCaretToPos(input, pos) {
    setSelectionRange(input, pos, pos);
}

function setSelectionRange(input, selectionStart, selectionEnd) {
    if (input.setSelectionRange) {
        input.focus();
        input.setSelectionRange(selectionStart, selectionEnd);
    } else if (input.createTextRange) {
        var range = input.createTextRange();
        range.collapse(true);
        range.moveEnd('character', selectionEnd);
        range.moveStart('character', selectionStart);
        range.select();
    }
}

$('#registration-form').submit(function (e) {
    e.preventDefault();
    $(".btn-submit").prepend("<i class='fa fa-spinner fa-spin '></i>");
    $(".btn-submit").attr("disabled", '');
    $(this).find('.alert-danger').remove();
    var culture = $("#culture").val();
    console.log("kk");
    $.ajax({
        type: "POST",
        url: "/" + culture + "/Ajax/Registration",
        data: $('#registration-form').serialize(),
        success: function (data) {
            if (data == null) {
                alert("there was an error on server try again later");
                return;
            }
            if (data.error == false) {
                if (data.nemid == false)
                {
                    $("#registration-form input").attr("readonly", true);
                    //$(':radio,:checkbox').click(function () {
                    //    return false;
                    //});

                    $('.remove-disease').css('display', 'none');
                    $('#addDiseaseButton').css('display', 'none');
                    $('#addNotListedDiseaseButton').css('display', 'none');

                    $(':radio,:checkbox').attr('onclick', "return false");

                    $("input[name=validation_code]").attr("readonly", false);
                    $("#submit_nemid").hide();
                    $("#nemid-frame").slideDown(100);
                }
                else
                {
                    var id = data.Id;
                    $("#Id").val(id);
                    window.location.hash = "final";
                }
                
            }
            else if (data.error.questionary != null) {
                $("#submit_nemid").show();
                $("#nemid-frame").slideUp(100);
                window.location.hash = "errors";
            } else if (data == true) {
                $("#submit_nemid").show();
                $("#nemid-frame").slideUp(100);
                window.location.hash = "1";
            } else {
                $("#submit_nemid").show();
                $("#nemid-frame").slideUp(100);
                for (var prop in data.error) {
                    if (prop == "nemid")
                    {
                        var iframe = document.getElementById('FrameId');
                        $("#nemid").val(null);
                        $("#AuthenticationInfo").val(null);
                        iframe.src = iframe.src;
                        $(validationMessage(data.error[prop])).insertBefore('.btn-submit');
                    }
                    else
                    {
                        if (!data.error.hasOwnProperty(prop)) continue;
                        $(validationMessage(data.error[prop])).insertBefore('.btn-submit');
                    }
                   
                }

                $(".btn-submit i").remove();
                $(".btn-submit").removeAttr("disabled");
            }
            if ($('input[name=storeboxEmail]').val() == "") {
                $('input[name=validation_code]').attr('disabled', 'disabled');
                $('.account-doesnt-exists').css('display', 'block');
            } else {
                $('input[name=storbox_account_id]').val($('input[name=storeboxEmail]').val());
                $('.account-exists').css('display', 'block');
            }
           
            
        },
        // vvv---- This is the new bit
        error: function (jqXHR, textStatus, errorThrown) {
            $(validationMessage("There was an error try again later")).insertBefore('.btn-submit');
            $(".btn-submit i").remove();
            $(".btn-submit").removeAttr("disabled");
        }
    });
    // $.post('', $('#registration-form').serialize() ,function(data){
    //   console.log("kk");
    //   if(data == null) {
    //     alert("there was an error try again later");
    //     return;
    //   }
    //   if(data.error == false) {
    //     window.location.hash="finish";
    //   }
    //   else if(data.error.questionary != null){
    //     window.location.hash="errors";
    //   } else if(data == true){
    //     window.location.hash="1";
    //   } else {
    //     for (var prop in data.error) {
    //       // skip loop if the property is from prototype
    //       if(!data.error.hasOwnProperty(prop)) continue;
    //       $(validationMessage(data.error[prop])).insertBefore('.btn-submit');
    //     }
    //   }
    //   console.log(typeof data);
    // });
});

$("#email").on('keyup paste', function () {
    if ($('#isSome')[0].checked)
        $("#storeboxEmail").val($(this).val());
});

$("#emailAgain").on('keyup paste', function () {
    if ($('#isSome')[0].checked)
        $("#storeboxEmailAgain").val($(this).val());
});

$("#phone").on('keyup paste', function () {
    if ($('#isSome')[0].checked)
        $("#storeboxPhone").val($(this).val());
});

$("#phoneAgain").on('keyup paste', function () {
    if ($('#isSome')[0].checked)
        $("#storeboxPhoneAgain").val($(this).val());
});

$(".btn-restart").click(function (e) {
    e.preventDefault();
    $("#registration-form").find("input[type=text], textarea").val("");
    $("#registration-form").find("input[type=checkbox]").each(function () {
        this.checked = false;
    });
    $("#registration-form").find("input[type=radio]").each(function () {
        this.checked = false;
    });
    $('#diseasesListContainer').show();
    $('#storeboxAccountInfo').show();
    currentStep = 1;
    window.location.hash = currentStep;
});

window.location.hash = 1;

window.onhashchange = function () {
    currentStep = window.location.href.split('#')[1];
    var returnTo1 = false;
    // var currentEl = $('.registration-step[data-step="'+currentStep+'"]');
    // if(currentEl.prev().length != 0) {
    //   var allPrevEl=currentEl.prevAll();
    //   allPrevEl.each(function(){
    //     if(typeof validationSteps[$(this).attr('data-step')] != 'undefined') {
    //         if(!validationSteps[$(this).attr('data-step')]()) {
    //           returnTo1=true;
    //         }
    //     }
    //
    //     console.log(typeof validationSteps[$(this).attr('data-step')]);
    //   });
    // }
    // if(returnTo1) {
    //   window.location.hash=1;
    // } else {
    //   $('.registration-step.active').removeClass('active');
    //   $('.registration-step[data-step="' + currentStep + '"]').addClass('active');
    // }

    if (currentStep == 'final') {
        $('.registration-step').hide();
        $('div[data-step="final"]').show();
    }
}

var checkStuff = function () {
    $(".alert-danger").remove();
    var error = [];
    if ($("input[name=contactinfo_optional]").val() == 0) {
        return true;
    }

    if ($("input[name=contactinfo_optional]").val() == 1 && $("#contact_info").is(":checked")) {
        return true;
    }
    var result = true;

    if ($("#email").val() == "") {
        //error.push($("#email").attr('data-error'));
        $el = $("<div class='alert alert-danger'>").append('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
        $el.append($("#email").attr('data-error') + "</br>");
        $el.insertAfter($("#email"));
        result = false;

        $('html, body').animate({
            scrollTop: $("#email").offset().top
        });
    }
    else if ($("#email").val() != $("#emailAgain").val()) {
        //error.push($("#emailAgain").attr('data-error'));
        $el = $("<div class='alert alert-danger'>").append('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
        $el.append($("#emailAgain").attr('data-error') + "</br>");
        $el.insertAfter($("#emailAgain"));
        result = false;
        $('html, body').animate({
            scrollTop: $("#email").offset().top
        });
    }

    if ($("#phone").val() == "") {
        //error.push($("#phone").attr('data-error'));
        $el = $("<div class='alert alert-danger'>").append('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
        $el.append($("#phone").attr('data-error') + "</br>");
        $el.insertAfter($("#email"));
        $('html, body').animate({
            scrollTop: $("#email").offset().top
        });

        result = false;
    }
    else if ($("#phone").val() != $("#phoneAgain").val()) {
        //error.push($("#phoneAgain").attr('data-error'));
        $el = $("<div class='alert alert-danger'>").append('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
        $el.append($("#phoneAgain").attr('data-error') + "</br>");
        $el.insertAfter($("#phoneAgain"));
        $('html, body').animate({
            scrollTop: $("#email").offset().top
        });
        result = false;
    }
    if ($('[name="isStoreboxAccount"]:checked').val() == "1" && $('[name="isSome"]:checked').val() != "1" ) {

        if ($("#storeboxEmail").val() == "") {
            //error.push($("#storeboxEmail").attr('data-error'));
            $el = $("<div class='alert alert-danger'>").append('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
            $el.append($("#storeboxEmail").attr('data-error') + "</br>");
            $el.insertAfter($("#storeboxEmail"));
            $('html, body').animate({
                scrollTop: $("#storeboxEmail").offset().top
            });
            result = false;
        }
        else if ($("#storeboxEmail").val() != $("#storeboxEmailAgain").val()) {
            // error.push($("#storeboxEmailAgain").attr('data-error'));
            $el = $("<div class='alert alert-danger'>").append('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
            $el.append($("#storeboxEmailAgain").attr('data-error') + "</br>");
            $el.insertAfter($("#storeboxEmailAgain"));
            $('html, body').animate({
                scrollTop: $("#storeboxEmail").offset().top
            });
            result = false;
        }

        if ($("#storeboxPhone").val() == "") {
            //error.push($("#storeboxPhone").attr('data-error'));
            $el = $("<div class='alert alert-danger'>").append('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
            $el.append($("#storeboxPhone").attr('data-error') + "</br>");
            $el.insertAfter($("#storeboxPhone"));
            $('html, body').animate({
                scrollTop: $("#storeboxEmail").offset().top
            });
            result = false;
        }
        else if ($("#storeboxPhone").val() != $("#storeboxPhoneAgain").val()) {
            // error.push($("#storeboxPhoneAgain").attr('data-error'));
            $el = $("<div class='alert alert-danger'>").append('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
            $el.append($("#storeboxPhoneAgain").attr('data-error') + "</br>");
            $el.insertAfter($("#storeboxPhoneAgain"));
            $('html, body').animate({
                scrollTop: $("#storeboxEmail").offset().top
            });
            result = false;
        }
    }
    

    //$el = $("<div class='alert alert-danger'>").append('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
    //for (var i = 0; i < error.length; i++) {
    //    $el.append(error[i] + "</br>");
    //}
   

    return result;
}

function eneblaForm(){
    if ($("#registration-form").length > 0) {

        $("#registration-form input").attr("readonly", false);
        $(':radio,:checkbox').removeAttr('onclick');

        $('.remove-disease').css('display', 'block');
        $('#addDiseaseButton').css('display', 'contents');
        $('#addNotListedDiseaseButton').css('display', 'contents');

        $("input[name=validation_code]").attr("readonly", true);
        $("#submit_nemid").removeAttr('disabled');
        $("#submit_nemid").show();
        $("#nemid-frame").slideUp(100);
    }
}

$("#checkReg").click(function () {
    var questcheck = true;
    $('.alert-danger').remove();
    $('.alert-warning').remove();
    $('.alert-success').remove();
    $("div[data-step='questionnaire'] input[type='radio']").each(function () {
        $(this).next().css('color', '#231F20');
    });

    $("div[data-step='questionnaire'] input[type='radio']:checked").each(function () {
        if ($(this).attr('data-correct') != '1') {
            questcheck = false;
            $(this).next().css('color', 'red');
        }
    });

    if (!questcheck) {
        $('html, body').animate({
            scrollTop: $("div[data-step='questionnaire']").offset().top
        });
        $('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + $('div[data-step="questionnaire"]').attr('data-error') + '</div>').insertAfter($("div[data-step='questionnaire'] h1"));
        return false;
    }
    else {
        if (!validationSteps.questionnaire()) {
            $('html, body').animate({
                scrollTop: $("div[data-step='questionnaire']").offset().top
            });

            $('<div class="alert alert-warning"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + $('div[data-step="questionnaire"]').attr('data-warning') + '</div>').insertAfter($("div[data-step='questionnaire'] h1"));
            return false;
        }
        else {
            $('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + $('div[data-step="questionnaire"]').attr('data-success') + '</div>').insertAfter($("#success-message"));
        }
    }
});



function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
