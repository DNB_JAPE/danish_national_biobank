﻿//-----------------------------------------------------------------------
// <copyright file="AuthorizationHeaderHandler.cs" company="None">
//     Copyright (c) Allow to distribute this code.
// </copyright>
// <author>Asma Khalid</author>
//-----------------------------------------------------------------------

namespace IdeaCms.Helpers.Common
{
    using Core.Operations;
    using DAL.Context;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Security.Claims;
    using System.Security.Principal;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web;

    /// <summary>
    /// Authorization for web API class.
    /// </summary>
    public class AuthorizationHeaderHandler : DelegatingHandler
    {
        #region Send method.

        SettingsOperations settingsOperation = new SettingsOperations();
        

        /// <summary>
        /// Send method.
        /// </summary>
        /// <param name="request">Request parameter</param>
        /// <param name="cancellationToken">Cancellation token parameter</param>
        /// <returns>Return HTTP response.</returns>
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            // Initialization.
            AuthenticationHeaderValue authorization = request.Headers.Authorization;
            string userName = null;
            string password = null;
            string _userName = settingsOperation.GetSettingByDescription("REST_USERNAME_VALUE");
            string _password = settingsOperation.GetSettingByDescription("REST_PASSWORD_VALUE");


            // Verification.
            //if (request.Headers.TryGetValues(setting.Where(x => x.Description == "REST_API_KEY_HEADER").FirstOrDefault().Value, out apiKeyHeaderValues) &&
            //    !string.IsNullOrEmpty(authorization.Parameter))
            if (!string.IsNullOrEmpty(authorization.Parameter))

            {
                //var apiKeyHeaderValue = apiKeyHeaderValues.First();

                // Get the auth token
                string authToken = authorization.Parameter;

                // Decode the token from BASE64
                string decodedToken = Encoding.UTF8.GetString(Convert.FromBase64String(authToken));

                // Extract username and password from decoded token
                userName = decodedToken.Substring(0, decodedToken.IndexOf(":"));
                password = decodedToken.Substring(decodedToken.IndexOf(":") + 1);

                // Verification.
                //apiKeyHeaderValue.Equals(setting.Where(x => x.Description == "REST_API_KEY_VALUE").FirstOrDefault().Value) &&
                if (userName.Equals(_userName) &&
                    password.Equals(_password))
                {
                    // Setting
                    var identity = new GenericIdentity(userName);
                    SetPrincipal(new GenericPrincipal(identity, null));
                }
            }

            // Info.
            return base.SendAsync(request, cancellationToken);
        }

        #endregion

        #region Set principal method.

        /// <summary>
        /// Set principal method.
        /// </summary>
        /// <param name="principal">Principal parameter</param>
        private static void SetPrincipal(IPrincipal principal)
        {
            // setting.
            Thread.CurrentPrincipal = principal;

            // Verification.
            if (HttpContext.Current != null)
            {
                // Setting.
                HttpContext.Current.User = principal;
            }
        }

        #endregion
    }
}