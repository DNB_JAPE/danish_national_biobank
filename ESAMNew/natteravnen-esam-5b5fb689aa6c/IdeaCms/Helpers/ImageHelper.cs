﻿using ImageMagick;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace IdeaCms.Helpers
{
    public class ImageHelper
    {
        public static void ResizeImage(string filepath,string standartuploads,string fileName, int width,int height,int quality = 100)
        {
            
            using (var image = new MagickImage(@filepath))
            {
            
                double wth = image.Width;
                double hth = image.Height;
                if (width==0 && height==0)
                {
                    image.Write(Path.Combine(standartuploads, fileName));
                    return;
                }
                double with = width;
                double heigh = height;
                if (hth >= wth)
                {
                    double index = width / wth;
                    heigh = index * hth;
                    if (height < heigh)
                    {
                        image.Resize(width, (int)heigh);
                        image.Crop(width, height, Gravity.Center);
                    }
                    else if (height > heigh)
                    {
                        with = wth * index;
                        index = height / heigh;
                        with = with * index;
                        image.Resize((int)with, height);
                        image.Crop(width, height, Gravity.Center);
                    }
                    else
                    {
                        image.Resize(width, height);
                    }
                }
                else if (hth < wth)
                {
                    double index = heigh / hth;
                    with = wth * index;
                    if(width<with)
                    {
                        image.Resize((int)with, height);
                        image.Crop(width, height, Gravity.Center);
                    }
                    else if(width > with)
                    {
                        heigh = hth * index;
                        index = width / with;
                        heigh = heigh * index;
                        image.Resize(width, (int)heigh);
                        image.Crop(width,height,Gravity.Center);
                    }
                    else
                    {
                        image.Resize(width, height);
                    }

                }
                image.Strip();
                image.Quality = quality;
                image.Write(Path.Combine(standartuploads, fileName));
            }
        }
    }
}
