﻿using MailKit.Net.Smtp;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IdeaCms.Helpers
{
    public class MailHelper
    {
        public static bool SendMailToUser(int port,string SmtpServer,string Mail,string password,string body, string userMail, string username = "example", string subject = "ContactMail")
        {
            try
            {
                var message = new MimeMessage();
                message.From.Add(new MailboxAddress("Research", Mail));
                message.To.Add(new MailboxAddress(username, userMail));
                message.Subject = subject;
                message.Body = new TextPart("plain")
                {
                    Text = body
                };
                using (var client = new SmtpClient())
                {   //587
                    //smtp.gmail.com
                    client.Connect(SmtpServer, port, false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    // Note: since we don't have an OAuth2 token, disable 	// the XOAUTH2 authentication mechanism.     
                    client.Authenticate(Mail, password);
                    client.Send(message);
                    client.Disconnect(true);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool SendMailToServer(int port, string SmtpServer, string Mail, string password, string body, string userMail, string username = "example", string subject = "ContactMail")
        {
            try
            {
                var message = new MimeMessage();
                message.From.Add(new MailboxAddress(username, userMail));
                message.To.Add(new MailboxAddress("Research", Mail));
                message.Subject = subject;
                message.Body = new TextPart("plain")
                {
                    Text = "From: " + username + " \n" + userMail + " \n" + body
                };
                using (var client = new SmtpClient())
                {
                    client.Connect(SmtpServer, port, false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    // Note: since we don't have an OAuth2 token, disable 	// the XOAUTH2 authentication mechanism.     
                    client.Authenticate(Mail, password);
                    client.Send(message);
                    client.Disconnect(true);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        //HTML Body Example

        //var bodyBuilder = new BodyBuilder();
        //bodyBuilder.HtmlBody = @"<HTML><head><title>usage marquee</title></headbody><div align='center'><marqueedirection = left'loop='7scrollamount='1'scrolldelay='2behavior='alternate'width='60%'bgcolor='#ff3424'>Latest news !. Latest news !.Latest news !.Latest news !.</marquee></div></body></html>";
        //message.Body = bodyBuilder.ToMessageBody();


        static bool invalid = false;
        public static bool IsValidEmail(string strIn)
        {
            invalid = false;
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalid)
                return false;

            // Return true if strIn is in valid e-mail format.
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
            
        }
        static string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalid = true;
            }
            return match.Groups[1].Value + domainName;
        }
    }
}