﻿using System.Collections.Generic;
using Core.Operations;
using System.Web;

namespace IdeaCms.Helpers
{
    public class SessionHelper 
    {
        private readonly HttpContext _httpContextAccessor;
        private HttpContext _session => _httpContextAccessor;

        AdministratorsOperations Operation = new AdministratorsOperations();
        AdministratorGroupOperations groupOperation = new AdministratorGroupOperations();
        
        

        public SessionHelper(HttpContext httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public bool TestSession()
        {
            if (_session == null)
                return false;
            else
                return true;
        }
        public string GetSessionUserName()
        {
            int SessionId = (int)_session.Session["user"];

            return Operation.GetSessionUserName(SessionId);
        }
        public int GetSessionId()
        {
            return (int)_session.Session["User"];
        }
        public List<int> GetSessionPermisions()
        {
            int SessionId = (int)_session.Session["User"];
            return groupOperation.GetSessionPermisions(SessionId);
        }
        public bool CheckPermision(string Controller, List<int> permission)
        {
            return groupOperation.CheckPermision(Controller, permission);
        }

    }
}
