﻿using DAL.Context;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Uow : IDisposable
    {
        public void SaveChanges()
        {
            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public Repository<Administrators> AdministratorsRepository
        {
            get
            {
                if (_AdministratorsRepository == null)
                {
                    _AdministratorsRepository = new Repository<Administrators>(context);
                }
                return _AdministratorsRepository;
            }
        }

        public Repository<AdministratorGroups> AdministratorGroupsRepository
        {
            get
            {
                if (_AdministratorGroupsRepository == null)
                {
                    _AdministratorGroupsRepository = new Repository<AdministratorGroups>(context);
                }
                return _AdministratorGroupsRepository;
            }
        }
        public Repository<Pages> PagesRepository
        {
            get
            {
                if (_PagesRepository == null)
                {
                    _PagesRepository = new Repository<Pages>(context);
                }
                return _PagesRepository;
            }
        }
        public Repository<Sections> SectionsRepository
        {
            get
            {
                if (_SectionsRepository == null)
                {
                    _SectionsRepository = new Repository<Sections>(context);
                }
                return _SectionsRepository;
            }
        }
        public Repository<SectionTranslation> SectionTranslationsRepository
        {
            get
            {
                if (_SectionTranslationsRepository == null)
                {
                    _SectionTranslationsRepository = new Repository<SectionTranslation>(context);
                }
                return _SectionTranslationsRepository;
            }
        }
        public Repository<MenuTypes> MenuTypesRepository
        {
            get
            {
                if (_MenuTypesRepository == null)
                {
                    _MenuTypesRepository = new Repository<MenuTypes>(context);
                }
                return _MenuTypesRepository;
            }
        }
        public Repository<SectionTypes> SectionTypesRepository
        {
            get
            {
                if (_SectionTypesRepository == null)
                {
                    _SectionTypesRepository = new Repository<SectionTypes>(context);
                }
                return _SectionTypesRepository;
            }
        }
        public Repository<SectionToMenu> SectionToMenuRepository
        {
            get
            {
                if (_SectionToMenuRepository == null)
                {
                    _SectionToMenuRepository = new Repository<SectionToMenu>(context);
                }
                return _SectionToMenuRepository;
            }
        }
        public Repository<Language> LanguageRepository
        {
            get
            {
                if (_LanguageRepository == null)
                {
                    _LanguageRepository = new Repository<Language>(context);
                }
                return _LanguageRepository;
            }
        }
        public Repository<InfoTranslation> InfoTranslationRepository
        {
            get
            {
                if (_InfoTranslationRepository == null)
                {
                    _InfoTranslationRepository = new Repository<InfoTranslation>(context);
                }
                return _InfoTranslationRepository;
            }
        }
        public Repository<File> FileRepository
        {
            get
            {
                if (_FileRepository == null)
                {
                    _FileRepository = new Repository<File>(context);
                }
                return _FileRepository;
            }
        }
        public Repository<Info> InfoRepository
        {
            get
            {
                if (_InfoRepository == null)
                {
                    _InfoRepository = new Repository<Info>(context);
                }
                return _InfoRepository;
            }
        }
        public Repository<InfoFiles> InfoFileRepository
        {
            get
            {
                if (_InfoFileRepository == null)
                {
                    _InfoFileRepository = new Repository<InfoFiles>(context);
                }
                return _InfoFileRepository;
            }
        }
        public Repository<InfoToInfoFiles> InfoToInfoFilesRepository
        {
            get
            {
                if (_InfoToInfoFilesRepository == null)
                {
                    _InfoToInfoFilesRepository = new Repository<InfoToInfoFiles>(context);
                }
                return _InfoToInfoFilesRepository;
            }
        }
        public Repository<Gallery> GalleryRepository
        {
            get
            {
                if (_GalleryRepository == null)
                {
                    _GalleryRepository = new Repository<Gallery>(context);
                }
                return _GalleryRepository;
            }
        }
        public Repository<GalleryGroupTranslate> GalleryGroupTranslateRepository
        {
            get
            {
                if (_GalleryGroupTranslationRepository == null)
                {
                    _GalleryGroupTranslationRepository = new Repository<GalleryGroupTranslate>(context);
                }
                return _GalleryGroupTranslationRepository;
            }
        }
        public Repository<GalleryGroup> GalleryGroupRepository
        {
            get
            {
                if (_GalleryGroupRepository == null)
                {
                    _GalleryGroupRepository = new Repository<GalleryGroup>(context);
                }
                return _GalleryGroupRepository;
            }
        }
        public Repository<GalleryTranslation> GalleryTranslationRepository
        {
            get
            {
                if (_GalleryTranslationRepository == null)
                {
                    _GalleryTranslationRepository = new Repository<GalleryTranslation>(context);
                }
                return _GalleryTranslationRepository;
            }
        }
        public Repository<Settings> SettingsRepository
        {
            get
            {
                if (_SettingsRepository == null)
                {
                    _SettingsRepository = new Repository<Settings>(context);
                }
                return _SettingsRepository;
            }
        }
        public Repository<Banner> BannerRepository
        {
            get
            {
                if (_BannerRepository == null)
                {
                    _BannerRepository = new Repository<Banner>(context);
                }
                return _BannerRepository;
            }
        }
        public Repository<BannerTranslation> BannerTranslationRepository
        {
            get
            {
                if (_BannerTranslationRepository == null)
                {
                    _BannerTranslationRepository = new Repository<BannerTranslation>(context);
                }
                return _BannerTranslationRepository;
            }
        }
        public Repository<BannerToSection> BannerToSectionRepository
        {
            get
            {
                if (_BannerToSectionRepository == null)
                {
                    _BannerToSectionRepository = new Repository<BannerToSection>(context);
                }
                return _BannerToSectionRepository;
            }
        }
        public Repository<InfoAdditional> InfoAdditionalRepository
        {
            get
            {
                if (_InfoAdditionalRepository == null)
                {
                    _InfoAdditionalRepository = new Repository<InfoAdditional>(context);
                }
                return _InfoAdditionalRepository;
            }
        }
        public Repository<InfoAdditionalTranslate> InfoAdditionalTranslateRepository
        {
            get
            {
                if (_InfoAdditionalTranslateRepository == null)
                {
                    _InfoAdditionalTranslateRepository = new Repository<InfoAdditionalTranslate>(context);
                }
                return _InfoAdditionalTranslateRepository;
            }
        }
        public Repository<DiseaseTranslation> DiseaseTranslationRepository
        {
            get
            {
                if (_DiseaseTranslationRepository == null)
                {
                    _DiseaseTranslationRepository = new Repository<DiseaseTranslation>(context);
                }
                return _DiseaseTranslationRepository;
            }
        }
        public Repository<Disease> DiseaseRepository
        {
            get
            {
                if (_DiseaseRepository == null)
                {
                    _DiseaseRepository = new Repository<Disease>(context);
                }
                return _DiseaseRepository;
            }
        }
        public Repository<Translation> TranslationRepository
        {
            get
            {
                if (_TranslationRepository == null)
                {
                    _TranslationRepository = new Repository<Translation>(context);
                }
                return _TranslationRepository;
            }
        }
        public Repository<Subscribes> SubscribesRepository
        {
            get
            {
                if (_SubscribesRepository == null)
                {
                    _SubscribesRepository = new Repository<Subscribes>(context);
                }
                return _SubscribesRepository;
            }
        }
        public Repository<AdministratorGroupsPermissions> AdministratorGroupsPermissionsRepository
        {
            get
            {
                if (_AdministratorGroupsPermissionsRepository == null)
                {
                    _AdministratorGroupsPermissionsRepository = new Repository<AdministratorGroupsPermissions>(context);
                }
                return _AdministratorGroupsPermissionsRepository;
            }
        }
        public Repository<Users> UsersRepository
        {
            get
            {
                if (_UsersRepository == null)
                {
                    _UsersRepository = new Repository<Users>(context);
                }
                return _UsersRepository;
            }
        }
        public Repository<UserDisease> UserDiseaseRepository
        {
            get
            {
                if (_UserDiseaseRepository == null)
                {
                    _UserDiseaseRepository = new Repository<UserDisease>(context);
                }
                return _UserDiseaseRepository;
            }
        }

        public Repository<PagesTranslation> PagesTranslationRepository
        {
            get
            {
                if (_PagesTranslationRepository == null)
                {
                    _PagesTranslationRepository = new Repository<PagesTranslation>(context);
                }
                return _PagesTranslationRepository;
            }
        }
        private Repository<Administrators> _AdministratorsRepository;
        private Repository<AdministratorGroups> _AdministratorGroupsRepository;
        private Repository<Pages> _PagesRepository;
        private Repository<Sections> _SectionsRepository;
        private Repository<SectionTranslation> _SectionTranslationsRepository;
        private Repository<MenuTypes> _MenuTypesRepository;
        private Repository<SectionTypes> _SectionTypesRepository;
        private Repository<SectionToMenu> _SectionToMenuRepository;
        private Repository<Language> _LanguageRepository;
        private Repository<InfoTranslation> _InfoTranslationRepository;
        private Repository<File> _FileRepository;
        private Repository<Info> _InfoRepository;
        private Repository<InfoFiles> _InfoFileRepository;
        private Repository<InfoToInfoFiles> _InfoToInfoFilesRepository;
        private Repository<Gallery> _GalleryRepository;
        private Repository<GalleryGroupTranslate> _GalleryGroupTranslationRepository;
        private Repository<GalleryGroup> _GalleryGroupRepository;
        private Repository<GalleryTranslation> _GalleryTranslationRepository;
        private Repository<Settings> _SettingsRepository;
        private Repository<Banner> _BannerRepository;
        private Repository<BannerTranslation> _BannerTranslationRepository;
        private Repository<BannerToSection> _BannerToSectionRepository;
        private Repository<InfoAdditional> _InfoAdditionalRepository;
        private Repository<InfoAdditionalTranslate> _InfoAdditionalTranslateRepository;
        private Repository<DiseaseTranslation> _DiseaseTranslationRepository;
        private Repository<Disease> _DiseaseRepository;
        private Repository<Translation> _TranslationRepository;
        private Repository<Subscribes> _SubscribesRepository;
        private Repository<AdministratorGroupsPermissions> _AdministratorGroupsPermissionsRepository;
        private Repository<Users> _UsersRepository;
        private Repository<UserDisease> _UserDiseaseRepository;
        private Repository<PagesTranslation> _PagesTranslationRepository;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool disposed = false;

        static public string connectionstring;
        public ResearchEntities context = new ResearchEntities();
        //public Uow()
        //{
        //    ((IObjectContextAdapter)this).ObjectContext.ObjectMaterialized += new ObjectMaterializedEventHandler(ObjectMaterialized);
        //}

        //void ObjectMaterialized(object sender, ObjectMaterializedEventArgs e)
        //{
        //    DecryptEntity(e.Entity);
        //}

        //private void DecryptEntity(object entity)
        //{
        //    //Get all the properties that are encryptable and decyrpt them
        //    var encryptedProperties = entity.GetType().GetProperties().Where(p => p.GetCustomAttributes(typeof(Encrypted), true).Any(a => p.PropertyType == typeof(String)));

        //    foreach (var property in encryptedProperties)
        //    {
        //        string encryptedValue = property.GetValue(entity) as string;
        //        if (!String.IsNullOrEmpty(encryptedValue))
        //        {
        //            string value = EncryptionService.Decrypt(encryptedValue);
        //            this.context.Entry(entity).Property(property.Name).OriginalValue = value;
        //            this.context.Entry(entity).Property(property.Name).IsModified = false;
        //        }
        //    }
        //}

       
    }
}
