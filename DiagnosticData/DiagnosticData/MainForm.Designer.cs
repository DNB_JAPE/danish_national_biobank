﻿namespace DiagnosticData
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._updateDailyButton = new System.Windows.Forms.Button();
            this._updateHistoricButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this._dailyTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this._dailyArchiveTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._historicTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this._historicArchiveTextBox = new System.Windows.Forms.TextBox();
            this._stateLabel = new System.Windows.Forms.Label();
            this._stopButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this._dailyUnmatchedTextBox = new System.Windows.Forms.TextBox();
            this._cidCheckBox = new System.Windows.Forms.CheckBox();
            this._accCheckBox = new System.Windows.Forms.CheckBox();
            this._fcidCheckBox = new System.Windows.Forms.CheckBox();
            this._accNUD = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this._accNUD)).BeginInit();
            this.SuspendLayout();
            // 
            // _updateDailyButton
            // 
            this._updateDailyButton.Location = new System.Drawing.Point(90, 131);
            this._updateDailyButton.Name = "_updateDailyButton";
            this._updateDailyButton.Size = new System.Drawing.Size(106, 23);
            this._updateDailyButton.TabIndex = 0;
            this._updateDailyButton.Text = "Opdater Daglige";
            this._updateDailyButton.UseVisualStyleBackColor = true;
            this._updateDailyButton.Click += new System.EventHandler(this._updateDailyButton_Click);
            // 
            // _updateHistoricButton
            // 
            this._updateHistoricButton.Location = new System.Drawing.Point(365, 131);
            this._updateHistoricButton.Name = "_updateHistoricButton";
            this._updateHistoricButton.Size = new System.Drawing.Size(106, 23);
            this._updateHistoricButton.TabIndex = 1;
            this._updateHistoricButton.Text = "Opdater Historiske";
            this._updateHistoricButton.UseVisualStyleBackColor = true;
            this._updateHistoricButton.Click += new System.EventHandler(this._updateHistoricButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nye daglige:";
            // 
            // _dailyTextBox
            // 
            this._dailyTextBox.Location = new System.Drawing.Point(153, 6);
            this._dailyTextBox.Name = "_dailyTextBox";
            this._dailyTextBox.ReadOnly = true;
            this._dailyTextBox.Size = new System.Drawing.Size(100, 20);
            this._dailyTextBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Matchede daglige ialt:";
            // 
            // _dailyArchiveTextBox
            // 
            this._dailyArchiveTextBox.Location = new System.Drawing.Point(153, 32);
            this._dailyArchiveTextBox.Name = "_dailyArchiveTextBox";
            this._dailyArchiveTextBox.ReadOnly = true;
            this._dailyArchiveTextBox.Size = new System.Drawing.Size(100, 20);
            this._dailyArchiveTextBox.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(294, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Nye historiske:";
            // 
            // _historicTextBox
            // 
            this._historicTextBox.Location = new System.Drawing.Point(428, 6);
            this._historicTextBox.Name = "_historicTextBox";
            this._historicTextBox.ReadOnly = true;
            this._historicTextBox.Size = new System.Drawing.Size(100, 20);
            this._historicTextBox.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(294, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Matchede historiske ialt:";
            // 
            // _historicArchiveTextBox
            // 
            this._historicArchiveTextBox.Location = new System.Drawing.Point(428, 32);
            this._historicArchiveTextBox.Name = "_historicArchiveTextBox";
            this._historicArchiveTextBox.ReadOnly = true;
            this._historicArchiveTextBox.Size = new System.Drawing.Size(100, 20);
            this._historicArchiveTextBox.TabIndex = 9;
            // 
            // _stateLabel
            // 
            this._stateLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._stateLabel.Location = new System.Drawing.Point(12, 175);
            this._stateLabel.Name = "_stateLabel";
            this._stateLabel.Size = new System.Drawing.Size(412, 13);
            this._stateLabel.TabIndex = 10;
            this._stateLabel.Text = "Status:";
            // 
            // _stopButton
            // 
            this._stopButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._stopButton.Location = new System.Drawing.Point(430, 170);
            this._stopButton.Name = "_stopButton";
            this._stopButton.Size = new System.Drawing.Size(100, 23);
            this._stopButton.TabIndex = 11;
            this._stopButton.Text = "Stop";
            this._stopButton.UseVisualStyleBackColor = true;
            this._stopButton.Click += new System.EventHandler(this._stopButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Ikke-Matchede daglige ialt:";
            // 
            // _dailyUnmatchedTextBox
            // 
            this._dailyUnmatchedTextBox.Location = new System.Drawing.Point(153, 58);
            this._dailyUnmatchedTextBox.Name = "_dailyUnmatchedTextBox";
            this._dailyUnmatchedTextBox.ReadOnly = true;
            this._dailyUnmatchedTextBox.Size = new System.Drawing.Size(100, 20);
            this._dailyUnmatchedTextBox.TabIndex = 13;
            // 
            // _cidCheckBox
            // 
            this._cidCheckBox.AutoSize = true;
            this._cidCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._cidCheckBox.Checked = true;
            this._cidCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._cidCheckBox.Location = new System.Drawing.Point(303, 60);
            this._cidCheckBox.Name = "_cidCheckBox";
            this._cidCheckBox.Size = new System.Drawing.Size(44, 17);
            this._cidCheckBox.TabIndex = 14;
            this._cidCheckBox.Text = "CID";
            this._cidCheckBox.UseVisualStyleBackColor = true;
            // 
            // _accCheckBox
            // 
            this._accCheckBox.AutoSize = true;
            this._accCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._accCheckBox.Location = new System.Drawing.Point(300, 106);
            this._accCheckBox.Name = "_accCheckBox";
            this._accCheckBox.Size = new System.Drawing.Size(47, 17);
            this._accCheckBox.TabIndex = 15;
            this._accCheckBox.Text = "ACC";
            this._accCheckBox.UseVisualStyleBackColor = true;
            // 
            // _fcidCheckBox
            // 
            this._fcidCheckBox.AutoSize = true;
            this._fcidCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._fcidCheckBox.Checked = true;
            this._fcidCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._fcidCheckBox.Location = new System.Drawing.Point(297, 83);
            this._fcidCheckBox.Name = "_fcidCheckBox";
            this._fcidCheckBox.Size = new System.Drawing.Size(50, 17);
            this._fcidCheckBox.TabIndex = 16;
            this._fcidCheckBox.Text = "FCID";
            this._fcidCheckBox.UseVisualStyleBackColor = true;
            // 
            // _accNUD
            // 
            this._accNUD.Location = new System.Drawing.Point(457, 105);
            this._accNUD.Name = "_accNUD";
            this._accNUD.Size = new System.Drawing.Size(71, 20);
            this._accNUD.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(353, 107);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Tidsinterval i Dage:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 197);
            this.Controls.Add(this.label6);
            this.Controls.Add(this._accNUD);
            this.Controls.Add(this._fcidCheckBox);
            this.Controls.Add(this._accCheckBox);
            this.Controls.Add(this._cidCheckBox);
            this.Controls.Add(this._dailyUnmatchedTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this._stopButton);
            this.Controls.Add(this._stateLabel);
            this.Controls.Add(this._historicArchiveTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._historicTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._dailyArchiveTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._dailyTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._updateHistoricButton);
            this.Controls.Add(this._updateDailyButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "Daglige Prøver";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._accNUD)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _updateDailyButton;
        private System.Windows.Forms.Button _updateHistoricButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _dailyTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _dailyArchiveTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox _historicTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox _historicArchiveTextBox;
        private System.Windows.Forms.Label _stateLabel;
        private System.Windows.Forms.Button _stopButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox _dailyUnmatchedTextBox;
        private System.Windows.Forms.CheckBox _cidCheckBox;
        private System.Windows.Forms.CheckBox _accCheckBox;
        private System.Windows.Forms.CheckBox _fcidCheckBox;
        private System.Windows.Forms.NumericUpDown _accNUD;
        private System.Windows.Forms.Label label6;
    }
}