﻿using DNBTools.DataClasses;
using DNBTools.Nautilus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiagnosticData
{
    public partial class MainForm : Form
    {
        private LimsCommunication _limsCommunication;
        private Operator _operator;
        private string _masterWorkflow;
        private string _dpMatchInterval;
        private volatile bool _stop;

        public MainForm(LimsCommunication limsCommunication, Operator operatorObj, string masterWorkflow, string dpMatchInterval)
        {
            InitializeComponent();
            _limsCommunication = limsCommunication;
            _operator = operatorObj;
            _masterWorkflow = masterWorkflow;
            _dpMatchInterval = dpMatchInterval;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            UpdateUI(false);
        }

        private void UpdateUI(bool active)
        {
            if(active)
            {
                _stopButton.Enabled = true;
                _updateDailyButton.Enabled = false;
                _updateHistoricButton.Enabled = false;
            }
            else 
            {
                _dailyTextBox.Text = _limsCommunication.CountLinesInTable("intfc.flexilab").ToString();
                _dailyArchiveTextBox.Text = _limsCommunication.CountLinesInTable("intfc.flexilab_dp_archive").ToString();
                _dailyUnmatchedTextBox.Text = _limsCommunication.CountLinesInTable("intfc.flexilab_dp_unmatched").ToString();
                _historicTextBox.Text = _limsCommunication.CountLinesInTable("intfc.flexilab_historic").ToString();
                _historicArchiveTextBox.Text = _limsCommunication.CountLinesInTable("intfc.flexilab_historic_archive").ToString();
                _stateLabel.Text = "Status: Klar";
                _stopButton.Enabled = false;
                _updateDailyButton.Enabled = true;
                _updateHistoricButton.Enabled = true;
            }
            Refresh();
        }

        private void _updateHistoricButton_Click(object sender, EventArgs e)
        {
            UpdateUI(true);
            _stop = false;
            List<MatchedSample> samples = new List<MatchedSample>();
            int sampleCountNautilus = 0;
            if (_cidCheckBox.Checked)
            {
                _stateLabel.Text = "Status: Finder prøver til opdatering ud fra CID";
                _stateLabel.Refresh();
                List<MatchedSample> matchedSamplesCID = GetMatchedSamplesCID();
                if (matchedSamplesCID == null)
                {
                    UpdateUI(false);
                    return;
                }
                int sampleCountCID = matchedSamplesCID.GroupBy(o => new { o.Aliquotes }).Select(o => o.FirstOrDefault()).Count();
                DiagnosticDataExtension.Log(sampleCountCID.ToString() + " historiske prøver udvalgt til opdatering fra CID");
                samples.AddRange(matchedSamplesCID);
                sampleCountNautilus += sampleCountCID;
            }
            if (_fcidCheckBox.Checked)
            {
                _stateLabel.Text = "Status: Finder prøver til opdatering ud fra FCID";
                _stateLabel.Refresh();
                List<MatchedSample> matchedSamplesFCID = GetMatchedSamplesFCID();
                if (matchedSamplesFCID == null)
                {
                    UpdateUI(false);
                    return;
                }
                int sampleCountFCID = matchedSamplesFCID.GroupBy(o => new { o.FCID }).Select(o => o.FirstOrDefault()).Count();
                DiagnosticDataExtension.Log(sampleCountFCID.ToString() + " historiske prøver udvalgt til opdatering fra FCID");
                samples.AddRange(matchedSamplesFCID);
                sampleCountNautilus += sampleCountFCID;
            }
            if (_accCheckBox.Checked)
            {
                _stateLabel.Text = "Status: Finder prøver til opdatering ud fra ACC";
                _stateLabel.Refresh();
                List<MatchedSample> matchedSamplesACC = GetMatchedSamplesACC();
                if (matchedSamplesACC == null)
                {
                    UpdateUI(false);
                    return;
                }
                int sampleCountACC = matchedSamplesACC.Count;
                DiagnosticDataExtension.Log(sampleCountACC + " historiske prøver udvalgt til opdatering fra ACC");
                samples.AddRange(matchedSamplesACC);
                sampleCountNautilus += sampleCountACC;
            }
            UpdateSamples(samples, sampleCountNautilus, MoveToArchiveHistoric);
        }

        private void _updateDailyButton_Click(object sender, EventArgs e)
        {
            UpdateUI(true);
            _stop = false;
            _stateLabel.Text = "Status: Finder prøver til opdatering";
            _stateLabel.Refresh();
            List<MatchedSample> samples = GetMatchedSamplesDP();
            if(samples == null)
            {
                UpdateUI(false);
                return;
            }
            DiagnosticDataExtension.Log(samples.Count + " prøver udvalgt til opdatering");
            UpdateSamples(samples, samples.Count, MoveToArchiveDP);
        }

        private void _stopButton_Click(object sender, EventArgs e)
        {
            _stop = true;
        }

        private List<MatchedSample> GetMatchedSamplesDP()
        {
            try
            {
                List<MatchedSample> matchedSamples = new List<MatchedSample>();
                foreach (List<string> data in _limsCommunication.GetFlexilabDPMatchedLines(_masterWorkflow, _dpMatchInterval))
                {
                    matchedSamples.Add(new MatchedSample(data));
                }
                return matchedSamples;
            }
            catch (Exception ex)
            {
                DiagnosticDataExtension.Log("Error Daily: " + ex.Message);
                MessageBox.Show("Noget gik galt under opslaget af daglige prøver. Se logfilen for detaljer.");
                return null;
            }
        }

        private List<MatchedSample> GetMatchedSamplesCID()
        {
            try
            {
                List<MatchedSample> matchedSamples = new List<MatchedSample>();
                foreach (List<string> sampleData in _limsCommunication.GetFlexilabHistoricMatchedLinesCID())
                {
                    matchedSamples.Add(new MatchedSample(sampleData));
                }
                return matchedSamples;
            }
            catch (Exception ex)
            {
                DiagnosticDataExtension.Log("Error Historic (CID): " + ex.Message);
                MessageBox.Show("Noget gik galt under opslaget med CID. Se logfilen for detaljer.");
                return null;
            }
        }

        private List<MatchedSample> GetMatchedSamplesFCID()
        {
            try
            {
                List<MatchedSample> matchedSamples = new List<MatchedSample>();
                foreach (List<string> sampleData in _limsCommunication.GetFlexilabHistoricMatchedLinesFCID())
                {
                    matchedSamples.Add(new MatchedSample(sampleData));
                }
                return matchedSamples;
            }
            catch (Exception ex)
            {
                DiagnosticDataExtension.Log("Error Historic (FCID): " + ex.Message);
                MessageBox.Show("Noget gik galt under opslaget med FCID. Se logfilen for detaljer.");
                return null;
            }
        }

        private List<MatchedSample> GetMatchedSamplesACC()
        {
            try
            {
                List<List<string>> sampleDataList = _limsCommunication.GetFlexilabHistoricMatchedLinesACC(_accNUD.Value); // Retrieve samples
                List<string> sampleBarcodes = sampleDataList.Select(b => b[5]).GroupBy(x=>x).Where(c=>c.Count()>1).Select(y=>y.Key).ToList(); // Find duplicates
                List<List<string>> duplicates = sampleDataList.Where(s => sampleBarcodes.Contains(s[5])).ToList(); // Select Duplicates
                sampleDataList = sampleDataList.Where(s => !sampleBarcodes.Contains(s[5])).ToList(); // Select nonDuplicates
                foreach (List<string> sampleData in duplicates)
                {
                    DiagnosticDataExtension.Log("Historic (ACC) ignored samples because of multiple hits: " + sampleData[5]);
                }
                List<MatchedSample> matchedSamples = new List<MatchedSample>();
                foreach (List<string> sampleData in sampleDataList)
                {
                    matchedSamples.Add(new MatchedSample(sampleData));
                }
                return matchedSamples;
            }
            catch (Exception ex)
            {
                DiagnosticDataExtension.Log("Error Historic (ACC): " + ex.Message);
                MessageBox.Show("Noget gik galt under opslaget med ACC. Se logfilen for detaljer.");
                return null;
            }
        }

        private void MoveToArchiveHistoric(MatchedSample sample)
        {
            _limsCommunication.MoveLineFromFlexilabHistoricToArchive(sample.FCID, sample.Collected, sample.Received, sample.Analysis, sample.PID, sample.ACCNumber, sample.Specimen_Code);
        }

        private void MoveToArchiveDP(MatchedSample sample)
        {
            _limsCommunication.MoveLineFromFlexilabDPToArchive(sample.RecordId);
        }

        private async void UpdateSamples(List<MatchedSample> matchedSamples, int nautilusSamplesCount, Action<MatchedSample> moveToArchiveFunc)
        {
            await Task.Run(() =>
            {
                try
                {
                    int skippedSamples = 0;
                    for (int currentIndex = 0; currentIndex < matchedSamples.Count; currentIndex++)
                    {
                        if (_stop)
                        {
                            return;
                        }
                        _stateLabel.Text = "Status: Opdaterer Flexilabprøve " + (currentIndex + 1) + " af " + matchedSamples.Count + " (Nautilus: " + nautilusSamplesCount + ")";
                        _stateLabel.Refresh();
                        MatchedSample sample = matchedSamples[currentIndex];
                        if(sample.PID.Length != 10)
                        {
                            DiagnosticDataExtension.Log("Ugyldigt CPR (" + sample.PID + ") - skippes");
                            skippedSamples++;
                            continue;
                        }
                        string patientId = _limsCommunication.PatientGetNautilusId(sample.PID);
                        if (string.IsNullOrEmpty(patientId))
                        {
                            DiagnosticDataExtension.Log("Patient (" + sample.PID + ") oprettes");
                            _limsCommunication.PatientCreate(sample.PID, null, null, null, _operator, true);
                            patientId = _limsCommunication.PatientGetNautilusId(sample.PID);
                            if (string.IsNullOrEmpty(patientId))
                            {
                                DiagnosticDataExtension.Log("Patienten (" + sample.PID + ") kunne ikke oprettes");
                                skippedSamples++;
                                continue;
                            }
                        }
                        DateTime visitTimestamp = DateTime.Parse(sample.Collected);
                        string visitId = _limsCommunication.VisitGet(sample.PID, visitTimestamp, "SSI Diagnostica");
                        if (string.IsNullOrEmpty(visitId))
                        {
                            DiagnosticDataExtension.Log("Visit (" + sample.PID + "/" + sample.Received + ") oprettes");
                            _limsCommunication.VisitCreate(sample.PID, visitTimestamp, "SSI Diagnostica");
                            visitId = _limsCommunication.VisitGet(sample.PID, visitTimestamp, "SSI Diagnostica");
                            if (string.IsNullOrEmpty(visitId))
                            {
                                DiagnosticDataExtension.Log("Visit (" + sample.PID + "/" + sample.Received + ") kunne ikke oprettes");
                                skippedSamples++;
                                continue;
                            }
                        }
                        DiagnosticDataExtension.Log("Forbindelsen mellem visitten (" + sample.PID + "/" + sample.Received + ") og masteren (" + sample.AliquotId + ") oprettes");
                        _limsCommunication.ConnectVisitAndMaster(sample.AliquotId, visitId, sample.Specimen);
                        DiagnosticDataExtension.Log("Prøven flyttes til arkivet");
                        moveToArchiveFunc(sample);
                    }
                    if (skippedSamples > 0)
                    {
                        DiagnosticDataExtension.Log(skippedSamples + "prøver blev sprunget over");
                        MessageBox.Show("Der var problemer med at opdatere " + skippedSamples + "prøver. Se loggen for detaljer");
                    }
                }
                catch (Exception ex)
                {
                    DiagnosticDataExtension.Log("Error Historic: " + ex.Message);
                    MessageBox.Show("Noget gik galt under opdateringen. Se logfilen for detaljer.");
                    return;
                }
                finally
                {
                    UpdateUI(false);
                }
            });
        }
    }
}
