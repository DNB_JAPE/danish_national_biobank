﻿using DNBTools;
using DNBTools.Nautilus;
using LSEXT;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Schema;
using Thermo.Nautilus.Extensions.Database;
using Thermo.Nautilus.Extensions.Model;
using Thermo.Nautilus.Extensions.UserInterface;

namespace DiagnosticData
{
    /// <summary>
    /// Extension for updating Nautilus with BC800 data.
    /// </summary>
    public class DiagnosticDataExtension : EntityExtension//, IVersion
    {
        private string _logFile;

        //public int GetVersion()
        //{
        //    return 3;
        //}

        #region Extension
        /// <summary>
        /// Called by Lims to decide if the extension should be listed in the right click menu.
        /// </summary>
        /// <param name="Parameters">Lims parameters (not used).</param>
        /// <returns>ExecuteExtension.exEnabled - Always allow this extension to start.</returns>
        public override EntityExtension.enumExtensionVisibility CanExecuteExtension()
        {
            return EntityExtension.enumExtensionVisibility.Enabled;
        }

        /// <summary>
        /// Called by Lims to activate the extension.
        /// </summary>
        public override void ExecuteExtension()
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            string configurationPath = Path.GetDirectoryName(path);
            _logFile = Path.Combine(configurationPath, (DateTime.Today.ToShortDateString() + ".log"));
            try
            {
                XmlDocument configurationDoc = new XmlDocument();
                configurationDoc.Load(Path.Combine(configurationPath, "Configuration.xml"));
                string logDir = configurationDoc.SelectSingleNode("/Configuration/LogDir").InnerXml;
                _logFile = Path.Combine(logDir, (DateTime.Today.ToShortDateString() + ".log"));
                Log("Starter extension");
                string server = Parameters["SERVER"].ToString();
                string connectionString;
                if (server.ToUpper() == "NAUTILUSP")
                {
                    Log("Server er " + server + " - forbinder til produktion");
                    connectionString = configurationDoc.SelectSingleNode("/Configuration/NautConStrProd").InnerXml;
                }
                else
                {
                    Log("Server er " + server + " - forbinder til test");
                    connectionString = configurationDoc.SelectSingleNode("/Configuration/NautConStrTest").InnerXml;
                }
                LimsCommunication _limsCommunication = new LimsCommunication(connectionString);
                string dpMasterAliquotWF = configurationDoc.SelectSingleNode("/Configuration/DPMasterAliquotWF").InnerXml;
                string dpMatchInterval = configurationDoc.SelectSingleNode("/Configuration/DPMatchInterval").InnerXml;
                string operatorName = "";
                string operatorId = "";
                try
                {
                    operatorId = Parameters["OPERATOR_ID"].ToString();
                    operatorName = Parameters["OPERATOR_NAME"].ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Kunne ikke læse data fra Nautilus: " + ex.Message);
                    return;
                }
                MainForm form = new MainForm(_limsCommunication, new DNBTools.DataClasses.Operator(operatorId, operatorName), dpMasterAliquotWF, dpMatchInterval);
                form.ShowDialog();
            }
            catch(Exception ex)
            {
                Log(ex.Message);
            }
        }
        #endregion Extension

        /// <summary>
        /// Log message
        /// </summary>
        /// <param name="message"></param>
        internal static void Log(string message)
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            string configurationPath = Path.GetDirectoryName(path);
            string logFile = Path.Combine(configurationPath, (DateTime.Today.ToShortDateString() + ".log"));
            File.AppendAllText(logFile, DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + " " + message + Environment.NewLine);
        }
    }
}
