﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiagnosticData
{
    /// <summary>
    /// Helper data class.
    /// </summary>
    internal class MatchedSample
    {
        internal List<string> _data;

        internal MatchedSample(List<string> data)
        {
            _data = data;
        }

        internal string FCID { get { return _data[0]; } }
        internal string Collected { get { return _data[1]; } }
        internal string Received { get { return _data[2]; } }
        internal string Analysis { get { return _data[3]; } }
        internal string Specimen { get { return _data[4]; } }
        internal string Aliquotes { get { return _data[5]; } }
        internal string PID { get { return _data[6]; } }
        internal string ACCNumber { get { return _data[7]; } }
        internal string Specimen_Code { get { return _data[8]; } }
        internal string RecordId { get { return _data[9]; } }   // Not available in historic data
        internal string AliquotId { get { return _data[10]; } } // From Nautilus
    }
}
