﻿namespace BrooksExtension
{
    partial class BrooksExtensionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._tabControl = new System.Windows.Forms.TabControl();
            this._biobankPage = new System.Windows.Forms.TabPage();
            this._biobankUserControl = new BrooksExtension.BiobankUserControl();
            this._toolsPage = new System.Windows.Forms.TabPage();
            this._toolsUserControl = new BrooksExtension._toolsUserControl();
            this._orderPage = new System.Windows.Forms.TabPage();
            this._orderUserControl = new BrooksExtension.OrderUserControl();
            this._tabControl.SuspendLayout();
            this._biobankPage.SuspendLayout();
            this._toolsPage.SuspendLayout();
            this._orderPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tabControl
            // 
            this._tabControl.Controls.Add(this._biobankPage);
            this._tabControl.Controls.Add(this._toolsPage);
            this._tabControl.Controls.Add(this._orderPage);
            this._tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tabControl.Location = new System.Drawing.Point(0, 0);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SelectedIndex = 0;
            this._tabControl.Size = new System.Drawing.Size(559, 387);
            this._tabControl.TabIndex = 0;
            // 
            // _biobankPage
            // 
            this._biobankPage.Controls.Add(this._biobankUserControl);
            this._biobankPage.Location = new System.Drawing.Point(4, 22);
            this._biobankPage.Name = "_biobankPage";
            this._biobankPage.Padding = new System.Windows.Forms.Padding(3);
            this._biobankPage.Size = new System.Drawing.Size(551, 361);
            this._biobankPage.TabIndex = 0;
            this._biobankPage.Text = "Biobank";
            this._biobankPage.UseVisualStyleBackColor = true;
            // 
            // _biobankUserControl
            // 
            this._biobankUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._biobankUserControl.Location = new System.Drawing.Point(3, 3);
            this._biobankUserControl.Name = "_biobankUserControl";
            this._biobankUserControl.Size = new System.Drawing.Size(545, 355);
            this._biobankUserControl.TabIndex = 0;
            // 
            // _toolsPage
            // 
            this._toolsPage.AutoScroll = true;
            this._toolsPage.Controls.Add(this._toolsUserControl);
            this._toolsPage.Location = new System.Drawing.Point(4, 22);
            this._toolsPage.Name = "_toolsPage";
            this._toolsPage.Padding = new System.Windows.Forms.Padding(3);
            this._toolsPage.Size = new System.Drawing.Size(551, 361);
            this._toolsPage.TabIndex = 2;
            this._toolsPage.Text = "Værktøj";
            this._toolsPage.UseVisualStyleBackColor = true;
            // 
            // _toolsUserControl
            // 
            this._toolsUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._toolsUserControl.Location = new System.Drawing.Point(3, 3);
            this._toolsUserControl.Name = "_toolsUserControl";
            this._toolsUserControl.Size = new System.Drawing.Size(545, 355);
            this._toolsUserControl.TabIndex = 0;
            // 
            // _orderPage
            // 
            this._orderPage.Controls.Add(this._orderUserControl);
            this._orderPage.Location = new System.Drawing.Point(4, 22);
            this._orderPage.Name = "_orderPage";
            this._orderPage.Padding = new System.Windows.Forms.Padding(3);
            this._orderPage.Size = new System.Drawing.Size(551, 361);
            this._orderPage.TabIndex = 3;
            this._orderPage.Text = "Ordrer";
            this._orderPage.UseVisualStyleBackColor = true;
            // 
            // _orderUserControl
            // 
            this._orderUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._orderUserControl.Location = new System.Drawing.Point(3, 3);
            this._orderUserControl.Name = "_orderUserControl";
            this._orderUserControl.Size = new System.Drawing.Size(545, 355);
            this._orderUserControl.TabIndex = 0;
            // 
            // DesignForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(559, 387);
            this.Controls.Add(this._tabControl);
            this.MinimumSize = new System.Drawing.Size(575, 426);
            this.Name = "DesignForm";
            this.Text = "Brooks Extension";
            this._tabControl.ResumeLayout(false);
            this._biobankPage.ResumeLayout(false);
            this._toolsPage.ResumeLayout(false);
            this._orderPage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl _tabControl;
        private System.Windows.Forms.TabPage _biobankPage;
        private BiobankUserControl _biobankUserControl;
        private System.Windows.Forms.TabPage _toolsPage;
        private _toolsUserControl _toolsUserControl;
        private System.Windows.Forms.TabPage _orderPage;
        private OrderUserControl _orderUserControl;


    }
}