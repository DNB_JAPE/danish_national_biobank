﻿using DNBTools.Brooks;
using DNBTools.DataClasses;
using DNBTools.Nautilus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrooksExtension
{
    public partial class BrooksExtensionForm : Form
    {
        public BrooksExtensionForm(List<BiobankEntity> entities, string operatorId, BrooksCommunication brooksCommunication, LimsCommunication limsCommunication, bool productionMode)
        {
            InitializeComponent();
            _biobankUserControl.Initialize(entities, operatorId, brooksCommunication, limsCommunication, productionMode);
            _toolsUserControl.Initialize(entities, operatorId, brooksCommunication, limsCommunication, productionMode);
            _orderUserControl.Initialize(operatorId, brooksCommunication, limsCommunication, productionMode);
        }
    }
}
