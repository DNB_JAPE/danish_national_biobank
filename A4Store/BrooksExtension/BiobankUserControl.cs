﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DNBTools.Brooks;
using DNBTools.DataClasses;
using DNBTools;
using DNBTools.Nautilus;

namespace BrooksExtension
{
    public partial class BiobankUserControl : BrooksUserControl
    {
        private List<BiobankEntity> _entities;

        public BiobankUserControl()
        {
            InitializeComponent();
        }

        public void Initialize(List<BiobankEntity> entities, string operatorName, BrooksCommunication brooksCommunication, LimsCommunication limsCommunication, bool productionMode)
        {
            base.Initialize(operatorName, limsCommunication, brooksCommunication, productionMode);
            _entities = entities;
        }

        private void BiobankUserControl_VisibleChanged(object sender, EventArgs e)
        {
            if (!DesignMode && _entities != null)
            {
                if (_entities[0] is Plate)
                {
                    _trayDataGridView.Rows.Clear();
                    int fullTrayCount = Convert.ToInt32(Math.Floor(_entities.Count() / 6.0));
                    for (int i = 0; i < fullTrayCount; i++)
                    {
                        _trayDataGridView.Rows.Add(string.Empty, 6);
                    }
                    if (fullTrayCount * 6 < _entities.Count())
                    {
                        _trayDataGridView.Rows.Add(string.Empty, _entities.Count() - fullTrayCount * 6);
                    }
                }
                EnableUI(true);
            }
        }

        private void EnableUI(bool enable)
        {
            if(!enable)
            {
                _priorityNUD.Enabled = false;
                _descriptionTextBox.Enabled = false;
                _trayDataGridView.Enabled = false;
                _vialScanCheckBox.Enabled = false;
                _insertButton.Enabled = false;
                _removeButton.Enabled = false;
            }
            else
            {
                _priorityNUD.Enabled = true;
                _descriptionTextBox.Enabled = true;
                _trayDataGridView.Enabled = _entities[0] is Plate;
                _vialScanCheckBox.Enabled = _entities[0] is Plate && _entities[0].LogicalId.StartsWith("CR");
                bool isTrayDataFilled = true;
                for (int i = 0; i < _trayDataGridView.RowCount; i++)
                {
                    if (_trayDataGridView[0, i].Value == null || _trayDataGridView[0, i].Value.Equals(string.Empty))
                    {
                        isTrayDataFilled = false;
                        break;
                    }
                }
                _insertButton.Enabled = _entities[0] is Plate && _descriptionTextBox.Text != "" && isTrayDataFilled;
                //if(_entities[0] is Plate && (_entities[0].LogicalId.StartsWith("MR0") || _entities[0].LogicalId.StartsWith("MR1")))
                // Changed by ESAT 20180221
                // to enable new plates to enter Brooks.
                // ESAT 20180413
                // Added support for 500 and 400 barcodes.
                // ESAT 20181023
                // Added support for BS barcodes.
                if (_entities[0] is Plate && (_entities[0].LogicalId.StartsWith("MR0") ||
                    _entities[0].LogicalId.StartsWith("MR1") || 
                    _entities[0].LogicalId.StartsWith("600") || 
                    _entities[0].LogicalId.StartsWith("500") || 
                    _entities[0].LogicalId.StartsWith("400") ||
                    _entities[0].LogicalId.StartsWith("BS")
                    ))
                {
                    _removeButton.Enabled = false;
                }
                else
                {
                    _removeButton.Enabled = _descriptionTextBox.Text != "";
                }
            }
        }

        private void _trayDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                int trayId = -1;
                if (_trayDataGridView[0, e.RowIndex].Value != null && Int32.TryParse(_trayDataGridView[0, e.RowIndex].Value.ToString(), out trayId))
                {
                    _trayDataGridView[0, e.RowIndex].Value = trayId.ToString("D8");
                }
            }
            EnableUI(true);
        }

        private void _insertButton_Click(object sender, EventArgs e)
        {
            using (WaitCursor wait = new WaitCursor())
            {
                EnableUI(false);
                if (!CheckForOpenOrders())
                {
                    EnableUI(true);
                    return;
                }
                if (_entities[0] is Plate)
                {
                    BrooksOrderState state = null;
                    Dictionary<string, int> trays = new Dictionary<string, int>();
                    foreach (DataGridViewRow row in _trayDataGridView.Rows)
                    {
                        trays.Add(row.Cells[0].Value.ToString(), Convert.ToInt32(row.Cells[1].Value));
                    }
                    List<string> plates = new List<string>();
                    foreach (BiobankEntity entity in _entities)
                    {
                        plates.Add(entity.LogicalId);
                    }
                    BrooksPlateType plateType = _brooksCommunication.GetPlateType(_entities[0].LogicalId);
                    if (plateType == BrooksPlateType.RACK_CR || plateType == BrooksPlateType.RACK_075 || plateType == BrooksPlateType.RACK_05)
                    {
                        List<BrooksPlateWell> tubes = LookupTubes();
                        if (tubes == null) { EnableUI(true); return; }
                        state = _brooksCommunication.LoadTubes(tubes, plates, trays, _operatorName, Convert.ToInt32(_priorityNUD.Value), _descriptionTextBox.Text, _vialScanCheckBox.Checked);
                    }
                    else
                    {
                        state = _brooksCommunication.LoadPlates(plates, trays, _operatorName, Convert.ToInt32(_priorityNUD.Value), _descriptionTextBox.Text);
                    }
                    if (state == null || state.ErrorNumber != 0)
                    {
                        MessageBox.Show("Fejl under oprettelse af ordre: " + state.ErrorNumber + " - " + state.ErrorMessage);
                        EnableUI(true);
                        return;
                    }
                }
                MessageBox.Show("Ordren er nu oprettet");
                EnableUI(true);
            }
        }

        private List<BrooksPlateWell> LookupTubes()
        {
            List<string> plateIds = new List<string>();
            foreach (BiobankEntity entity in _entities)
            {
                plateIds.Add(entity.LimsId);
            }
            List<Tuple<int, string, string, string, string>> samples = null;
            try
            {
                samples = _limsCommunication.LookupSampleDataForCarriers(plateIds);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Kunne ikke slå prøvernes data op: " + ex.Message);
                return null;
            }
            if (samples == null)
            {
                MessageBox.Show("Kunne ikke slå prøvernes data op!");
                return null;
            }
            List<BrooksPlateWell> retval = new List<BrooksPlateWell>();
            foreach (Tuple<int, string, string, string, string> sample in samples)
            {
                retval.Add(new BrooksPlateWell(sample.Item2, sample.Item3, sample.Item4, sample.Item5));
            }
            return retval;
        }

        private void _removeButton_Click(object sender, EventArgs e)
        {
            EnableUI(false);
            if (!CheckForOpenOrders())
            {
                EnableUI(true);
                return;
            }
            BrooksOrderState state = null;
            if (_entities[0] is Plate)
            {
                using (WaitCursor wait = new WaitCursor())
                {
                    List<string> plateBarcodes = new List<string>();
                    foreach (BiobankEntity plate in _entities)
                    {
                        plateBarcodes.Add(plate.LogicalId);
                    }
                    state = _brooksCommunication.RemovePlates(plateBarcodes, _operatorName, Convert.ToInt32(_priorityNUD.Value), _descriptionTextBox.Text);
                }
            }
            else
            {
                ContainerType containerType = _limsCommunication.SampleGetContainerType(_entities[0].LimsId);
                using (WaitCursor wait = new WaitCursor())
                {
                    switch (containerType)
                    {
                        case ContainerType.CR:
                            {
                                List<string> tubes = GetTubes();
                                state = _brooksCommunication.RemoveTubes(tubes, BrooksPlateType.RACK_CR, _operatorName, Convert.ToInt32(_priorityNUD.Value), _descriptionTextBox.Text);
                                break;
                            }
                        case ContainerType.MX05:
                            {
                                List<string> tubes = GetTubes();
                                state = _brooksCommunication.RemoveTubes(tubes, BrooksPlateType.RACK_05, _operatorName, Convert.ToInt32(_priorityNUD.Value), _descriptionTextBox.Text);
                                break;
                            }
                        case ContainerType.MX075:
                            {
                                List<string> tubes = GetTubes();
                                state = _brooksCommunication.RemoveTubes(tubes, BrooksPlateType.RACK_075, _operatorName, Convert.ToInt32(_priorityNUD.Value), _descriptionTextBox.Text);
                                break;
                            }
                        default:
                            {
                                List<string> plates = GetPlates();
                                if (plates == null) { return; }
                                state = _brooksCommunication.RemovePlates(plates, _operatorName, Convert.ToInt32(_priorityNUD.Value), _descriptionTextBox.Text);
                                break;
                            }
                    }
                }
            }
            if (state == null || state.ErrorNumber != 0)
            {
                MessageBox.Show("Kunne ikke oprette ordre: " + state.ErrorNumber + " - " + state.ErrorMessage);
                EnableUI(true);
                return;
            }
            MessageBox.Show("Ordren er nu oprettet");
            EnableUI(true);
        }

        private List<string> GetPlates()
        {
            Dictionary<string, string> plateIdsAndBCs = null;
            List<string> sampleIds = new List<string>();
            foreach (BiobankEntity sample in _entities)
            {
                sampleIds.Add(sample.LimsId);
            }
            try
            {
                plateIdsAndBCs = _limsCommunication.LookupCarrierIdsAndRefsForSamples(sampleIds);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Kunne ikke slå pladernes stregkoder op: " + ex.Message);
                return null;
            }
            if (plateIdsAndBCs == null)
            {
                MessageBox.Show("Kunne ikke slå  pladernes stregkoder op!");
                return null;
            }
            return new List<string>(plateIdsAndBCs.Values);
        }

        private List<string> GetTubes()
        {
            List<string> sampleIds = new List<string>();
            foreach (BiobankEntity sample in _entities)
            {
                sampleIds.Add(sample.LogicalId);
            }
            return sampleIds;
        }

        private void _descriptionTextBox_TextChanged(object sender, EventArgs e)
        {
                EnableUI(true);
        }

        private bool CheckForOpenOrders() 
        {
            Tuple<List<BrooksOrder>, BrooksOrderState> openOrders = _brooksCommunication.GetOrderList("COMPLETE");
            if (openOrders.Item2 == null || openOrders.Item2.ErrorNumber != 0)
            {
                MessageBox.Show("Kunne ikke få liste af åbne ordrer: " + openOrders.Item2.ErrorNumber + " - " + openOrders.Item2.ErrorMessage);
                return false;
            }
            if (openOrders.Item1.Count > 0)
            {
                DialogResult uiresult = MessageBox.Show("Der er " + openOrders.Item1.Count + " ikke-opdaterede ordrer i systemet. Vil du fortsætte?", "Åbne ordrer i systemet", MessageBoxButtons.OKCancel);
                if (uiresult == DialogResult.Cancel)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
