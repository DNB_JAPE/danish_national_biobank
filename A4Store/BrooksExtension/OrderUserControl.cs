﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DNBTools.Brooks;
using DNBTools.Nautilus;
using DNBTools;

namespace BrooksExtension
{
    public partial class OrderUserControl : BrooksUserControl
    {
        public OrderUserControl()
        {
            InitializeComponent();
        }

        public void Initialize(string operatorName, BrooksCommunication brooksCommunication, LimsCommunication limsCommunication, bool productionMode)
        {
            base.Initialize(operatorName, limsCommunication, brooksCommunication, productionMode);
            _stateComboBox.SelectedIndex = 0;
        }

        private void UpdateUI() 
        {
            if (!DesignMode && _brooksCommunication != null)
            {                
                Tuple<List<BrooksOrder>, BrooksOrderState> state = _brooksCommunication.GetOrderList(_stateComboBox.Text == "Alle" ? null : _stateComboBox.Text);
                if (state == null || state.Item2 == null || state.Item2.ErrorNumber != 0)
                {
                    MessageBox.Show("Kunne ikke hente ordrer fra Brooks: " + state.Item2.ErrorNumber + " - " + state.Item2.ErrorMessage);
                    return;
                }
                if (state.Item1 != null)
                {
                    _dataGridView.Rows.Clear();
                    foreach (BrooksOrder order in state.Item1)
                    {
                        string start = order.StartDate == DateTime.MaxValue ? "" : order.StartDate.ToShortDateString();
                        string end = order.CompletedDate == DateTime.MaxValue ? "" : order.CompletedDate.ToShortDateString();
                        _dataGridView.Rows.Add(order.OrderId, order.OperatorName, order.Priority, order.OrderState, order.OrderType, order.OrderItemType, start, end, order.Description);
                        _dataGridView.Rows[_dataGridView.Rows.Count - 1].Tag = order;
                    }
                }
            }
        }

        private void OrderUserControl_VisibleChanged(object sender, EventArgs e)
        {
            UpdateUI();
        }

        private void _dataGridView_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                DataGridViewRow row;
                try
                {
                    row = _dataGridView.Rows[_dataGridView.HitTest(e.X, e.Y).RowIndex];
                }
                catch
                {
                    return;
                }
                _dataGridView.ClearSelection();
                row.Selected = true;
                ContextMenuStrip menu = new ContextMenuStrip();
                menu.Tag = row;
                menu.Items.Add("Luk (Opdaterer LIMS)").Name = "Luk (Opdaterer LIMS)";
                menu.Items.Add("Afvis").Name = "Afvis";
                menu.Items.Add("Detaljer").Name = "Detaljer";
                menu.Items.Add("Luk uden at opdatere").Name = "Luk uden at opdatere";
                menu.Show(_dataGridView, new Point(e.X, e.Y));
                menu.ItemClicked += menu_ItemClicked;
                if (_productionMode)
                {
                    string orderState = row.Cells[3].Value.ToString();
                    switch (orderState)
                    {
                        case "NEW":
                            {
                                menu.Items["Luk (Opdaterer LIMS)"].Enabled = false;
                                menu.Items["Luk uden at opdatere"].Enabled = false;
                                break;
                            }
                        case "SENT":
                        case "ISSUED":
                        case "ACCEPTED":
                        case "PENDING":
                        case "PROCESSING":
                        case "RECEIVED":
                            {
                                menu.Items["Luk (Opdaterer LIMS)"].Enabled = false;
                                menu.Items["Afvis"].Enabled = false;
                                menu.Items["Luk uden at opdatere"].Enabled = false;
                                break;
                            }
                        case "COMPLETE":
                            {
                                menu.Items["Afvis"].Enabled = false;
                                break;
                            }
                        case "REJECTED":
                            {
                                menu.Items["Afvis"].Enabled = false;
                                break;
                            }
                    }
                }
            }
        }

        void menu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            using (new WaitCursor())
            {
                BrooksOrderState state;
                BrooksOrder order = (e.ClickedItem.Owner.Tag as DataGridViewRow).Tag as BrooksOrder;
                int orderId = Convert.ToInt32(order.OrderId);
                switch (e.ClickedItem.Name.ToString())
                {
                    case "Luk (Opdaterer LIMS)":
                        state = UpdateLocation(order);
                        if (state == null || state.ErrorNumber != 0)
                        {
                            MessageBox.Show("Kunne ikke opdatere location: " + state.ErrorNumber + " - " + state.ErrorMessage);
                            UpdateUI();
                            return;
                        }
                        state = _brooksCommunication.CloseOrder(orderId);
                        if (state == null || state.ErrorNumber != 0)
                        {
                            MessageBox.Show("Kunne ikke lukke ordre: " + state.ErrorNumber + " - " + state.ErrorMessage);
                        }
                        UpdateUI();
                        return;
                    case "Afvis":
                        state = _brooksCommunication.FailOrder(orderId);
                        if (state == null || state.ErrorNumber != 0)
                        {
                            MessageBox.Show("Kunne ikke afvise ordre: " + state.ErrorNumber + " - " + state.ErrorMessage);
                        }
                        UpdateUI();
                        return;
                    case "Detaljer":
                        Tuple<List<BrooksLabwareEvent>, BrooksOrderState> eventState = _brooksCommunication.GetOrderEvents(orderId);
                        if (eventState == null || eventState.Item2 == null || eventState.Item2.ErrorNumber != 0)
                        {
                            MessageBox.Show("Kunne ikke hente ordre detaljer: " + eventState.Item2.ErrorNumber + " - " + eventState.Item2.ErrorMessage);
                            return;
                        }
                        OrderDetailsForm form = new OrderDetailsForm(eventState.Item1);
                        form.ShowDialog();
                        return;
                    case "Luk uden at opdatere":
                        state = _brooksCommunication.CloseOrder(orderId);
                        if (state == null || state.ErrorNumber != 0)
                        {
                            MessageBox.Show("Kunne ikke lukke ordre: " + state.ErrorNumber + " - " + state.ErrorMessage);
                        }
                        UpdateUI();
                        return;
                }
            }
        }

        private BrooksOrderState UpdateLocation(BrooksOrder order)
        {
            try
            {
                if (order.OrderType == "LOAD")
                {
                    Tuple<List<string>, BrooksOrderState> state = _brooksCommunication.GetOrderPlates(Convert.ToInt32(order.OrderId));
                    if (state == null || state.Item2 == null || state.Item2.ErrorNumber != 0)
                    {
                        return state.Item2;
                    }
                    foreach (string plate in state.Item1)
                    {
                        _limsCommunication.UpdateLocationForCarrier(plate, _limsCommunication.LocationGetId(LimsCommunication.BrooksLocationName));
                        if (order.OrderItemType == "TUBE")
                        {
                            _limsCommunication.LocationPlateRemoveSamples(plate);
                        }
                    }
                }
                else if (order.OrderType == "REMOVE")
                {
                    if (order.OrderItemType == "TUBE")
                    {
                        Tuple<List<BrooksPlateWell>, BrooksOrderState> tubes = _brooksCommunication.GetOrderTubes(Convert.ToInt32(order.OrderId));
                        if (tubes == null || tubes.Item2 == null || tubes.Item2.ErrorNumber != 0)
                        {
                            return tubes.Item2;
                        }
                        foreach (BrooksPlateWell tube in tubes.Item1)
                        {
                            _limsCommunication.LocationPlateAddSample(tube.PlateId, tube.TubeId, tube.Row, tube.Column);
                        }
                    }
                    Tuple<List<string>, BrooksOrderState> state = _brooksCommunication.GetOrderPlates(Convert.ToInt32(order.OrderId));
                    if (state == null || state.Item2 == null || state.Item2.ErrorNumber != 0)
                    {
                        return state.Item2;
                    }
                    foreach (string plate in state.Item1)
                    {
                        _limsCommunication.UpdateLocationForCarrier(plate, _limsCommunication.LocationGetId(LimsCommunication.PickedLocationName));
                    }
                }
                return new BrooksOrderState();
            }
            catch(Exception ex)
            {
                return new BrooksOrderState(-1, ex.Message);
            }
        }

        private void _stateComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateUI();
        }

        private void _updateButton_Click(object sender, EventArgs e)
        {
            UpdateUI();
        }
    }
}
