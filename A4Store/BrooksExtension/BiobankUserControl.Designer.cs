﻿namespace BrooksExtension
{
    partial class BiobankUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._priorityNUD = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._descriptionTextBox = new System.Windows.Forms.TextBox();
            this._insertButton = new System.Windows.Forms.Button();
            this._removeButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this._trayDataGridView = new System.Windows.Forms.DataGridView();
            this._trayColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._platesColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._vialScanCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this._priorityNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._trayDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // _priorityNUD
            // 
            this._priorityNUD.Location = new System.Drawing.Point(94, 10);
            this._priorityNUD.Name = "_priorityNUD";
            this._priorityNUD.Size = new System.Drawing.Size(52, 20);
            this._priorityNUD.TabIndex = 0;
            this._priorityNUD.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Prioritet:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(14, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 46);
            this.label2.TabIndex = 2;
            this.label2.Text = "Kommentar (skal udfyldes):";
            // 
            // _descriptionTextBox
            // 
            this._descriptionTextBox.Location = new System.Drawing.Point(94, 36);
            this._descriptionTextBox.Multiline = true;
            this._descriptionTextBox.Name = "_descriptionTextBox";
            this._descriptionTextBox.Size = new System.Drawing.Size(160, 89);
            this._descriptionTextBox.TabIndex = 3;
            this._descriptionTextBox.TextChanged += new System.EventHandler(this._descriptionTextBox_TextChanged);
            // 
            // _insertButton
            // 
            this._insertButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._insertButton.BackgroundImage = global::BrooksExtension.Properties.Resources.In;
            this._insertButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._insertButton.Location = new System.Drawing.Point(205, 201);
            this._insertButton.Name = "_insertButton";
            this._insertButton.Size = new System.Drawing.Size(150, 48);
            this._insertButton.TabIndex = 4;
            this._insertButton.Text = "Indsæt";
            this._insertButton.UseVisualStyleBackColor = true;
            this._insertButton.Click += new System.EventHandler(this._insertButton_Click);
            // 
            // _removeButton
            // 
            this._removeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._removeButton.BackgroundImage = global::BrooksExtension.Properties.Resources.In;
            this._removeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._removeButton.Location = new System.Drawing.Point(361, 201);
            this._removeButton.Name = "_removeButton";
            this._removeButton.Size = new System.Drawing.Size(150, 48);
            this._removeButton.TabIndex = 5;
            this._removeButton.Text = "Udtag";
            this._removeButton.UseVisualStyleBackColor = true;
            this._removeButton.Click += new System.EventHandler(this._removeButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(266, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(184, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Trays (skal udfyldes ved indsættelse):";
            // 
            // _trayDataGridView
            // 
            this._trayDataGridView.AllowUserToAddRows = false;
            this._trayDataGridView.AllowUserToDeleteRows = false;
            this._trayDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._trayDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._trayDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._trayColumn,
            this._platesColumn});
            this._trayDataGridView.Location = new System.Drawing.Point(269, 36);
            this._trayDataGridView.Name = "_trayDataGridView";
            this._trayDataGridView.RowHeadersVisible = false;
            this._trayDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._trayDataGridView.Size = new System.Drawing.Size(242, 89);
            this._trayDataGridView.TabIndex = 23;
            this._trayDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._trayDataGridView_CellEndEdit);
            // 
            // _trayColumn
            // 
            this._trayColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._trayColumn.HeaderText = "Tray Id";
            this._trayColumn.Name = "_trayColumn";
            // 
            // _platesColumn
            // 
            this._platesColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._platesColumn.HeaderText = "Antal Plader";
            this._platesColumn.Name = "_platesColumn";
            this._platesColumn.ReadOnly = true;
            // 
            // _vialScanCheckBox
            // 
            this._vialScanCheckBox.AutoSize = true;
            this._vialScanCheckBox.Location = new System.Drawing.Point(269, 131);
            this._vialScanCheckBox.Name = "_vialScanCheckBox";
            this._vialScanCheckBox.Size = new System.Drawing.Size(119, 17);
            this._vialScanCheckBox.TabIndex = 24;
            this._vialScanCheckBox.Text = "Unlad at skanne rør";
            this._vialScanCheckBox.UseVisualStyleBackColor = true;
            // 
            // BiobankUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._vialScanCheckBox);
            this.Controls.Add(this._trayDataGridView);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._removeButton);
            this.Controls.Add(this._insertButton);
            this.Controls.Add(this._descriptionTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._priorityNUD);
            this.Name = "BiobankUserControl";
            this.Size = new System.Drawing.Size(524, 264);
            this.VisibleChanged += new System.EventHandler(this.BiobankUserControl_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this._priorityNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._trayDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown _priorityNUD;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _descriptionTextBox;
        private System.Windows.Forms.Button _insertButton;
        private System.Windows.Forms.Button _removeButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView _trayDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn _trayColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _platesColumn;
        private System.Windows.Forms.CheckBox _vialScanCheckBox;
    }
}
