﻿namespace BrooksExtension
{
    partial class OrderDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._dataGridView = new System.Windows.Forms.DataGridView();
            this._eventIdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._creationDateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._eventTypeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._orderTypeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._itemTypeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._itemColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // _dataGridView
            // 
            this._dataGridView.AllowUserToAddRows = false;
            this._dataGridView.AllowUserToDeleteRows = false;
            this._dataGridView.AllowUserToResizeRows = false;
            this._dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._eventIdColumn,
            this._creationDateColumn,
            this._eventTypeColumn,
            this._orderTypeColumn,
            this._itemTypeColumn,
            this._itemColumn});
            this._dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dataGridView.Location = new System.Drawing.Point(0, 0);
            this._dataGridView.MultiSelect = false;
            this._dataGridView.Name = "_dataGridView";
            this._dataGridView.ReadOnly = true;
            this._dataGridView.RowHeadersVisible = false;
            this._dataGridView.Size = new System.Drawing.Size(765, 477);
            this._dataGridView.TabIndex = 0;
            // 
            // _eventIdColumn
            // 
            this._eventIdColumn.HeaderText = "Log Id";
            this._eventIdColumn.Name = "_eventIdColumn";
            this._eventIdColumn.ReadOnly = true;
            // 
            // _creationDateColumn
            // 
            this._creationDateColumn.HeaderText = "Oprettet";
            this._creationDateColumn.Name = "_creationDateColumn";
            this._creationDateColumn.ReadOnly = true;
            // 
            // _eventTypeColumn
            // 
            this._eventTypeColumn.HeaderText = "Log Type";
            this._eventTypeColumn.Name = "_eventTypeColumn";
            this._eventTypeColumn.ReadOnly = true;
            // 
            // _orderTypeColumn
            // 
            this._orderTypeColumn.HeaderText = "Labware";
            this._orderTypeColumn.Name = "_orderTypeColumn";
            this._orderTypeColumn.ReadOnly = true;
            // 
            // _itemTypeColumn
            // 
            this._itemTypeColumn.HeaderText = "Labware Type";
            this._itemTypeColumn.Name = "_itemTypeColumn";
            this._itemTypeColumn.ReadOnly = true;
            // 
            // _itemColumn
            // 
            this._itemColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._itemColumn.HeaderText = "Labware";
            this._itemColumn.Name = "_itemColumn";
            this._itemColumn.ReadOnly = true;
            // 
            // OrderDetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 477);
            this.Controls.Add(this._dataGridView);
            this.Name = "OrderDetailsForm";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Ordredetaljer";
            ((System.ComponentModel.ISupportInitialize)(this._dataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView _dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn _eventIdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _creationDateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _eventTypeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _orderTypeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _itemTypeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _itemColumn;
    }
}