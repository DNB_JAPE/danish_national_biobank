﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DNBTools;
using DNBTools.Nautilus;
using DNBTools.Brooks;

namespace BrooksExtension
{
    public partial class BrooksUserControl : UserControl
    {
        protected LimsCommunication _limsCommunication;
        protected BrooksCommunication _brooksCommunication;
        protected string _operatorName;
        protected bool _productionMode;

        public BrooksUserControl()
        {
            InitializeComponent();
        }

        public void Initialize(string operatorName, LimsCommunication limsCommunication, BrooksCommunication brooksCommunication, bool productionMode)
        {
            _limsCommunication = limsCommunication;
            _brooksCommunication = brooksCommunication;
            _operatorName = operatorName;
            _productionMode = productionMode;
        }
    }
}
