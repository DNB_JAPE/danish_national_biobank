﻿using DNBTools.Brooks;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrooksExtension
{
    public partial class OrderDetailsForm : Form
    {
        public OrderDetailsForm(List<BrooksLabwareEvent> events)
        {
            InitializeComponent();
            _dataGridView.Rows.Clear();
            foreach (BrooksLabwareEvent orderEvent in events)
            {
                string start = orderEvent.CreationDate == DateTime.MaxValue ? "" : orderEvent.CreationDate.ToShortDateString();
                _dataGridView.Rows.Add(orderEvent.LabwareEventId, start, orderEvent.EventType, orderEvent.ItemType, orderEvent.LabwareType, orderEvent.ItemId);

            }
            
        }
    }
}
