﻿using DNBTools;
using DNBTools.Brooks;
using DNBTools.DataClasses;
using DNBTools.Nautilus;
using LSEXT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Thermo.Nautilus.Extensions.UserInterface;

namespace BrooksExtension
{
    /// <summary>
    /// Extension for sending orders to Brooks robot freezer.
    /// </summary>
    public class BrooksEntityExtension : EntityExtension, IVersion
    {
        public int GetVersion()
        {
            return 5;
        }

        /// <summary>
        /// Called by Lims to decide if the extension should be listed in the right click menu.
        /// </summary>
        /// <param name="Parameters">Lims parameters (not used).</param>
        /// <returns>ExecuteExtension.exEnabled - Always allow this extension to start.</returns>
        public override EntityExtension.enumExtensionVisibility CanExecuteExtension()
        {
            return EntityExtension.enumExtensionVisibility.Enabled;
        }

        /// <summary>
        /// Called by Lims to activate the extension.
        /// </summary>
        public override void ExecuteExtension()
        {
            // Read configuration
            Configuration configuration;
            try
            {
                configuration = new Configuration();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Kunne ikke indlæse konfigurationen: " + ex.Message);
                return;
            }
            // Create LimsCommunication
            LimsCommunication limsCommunication = null;
            try
            {
                limsCommunication = new LimsCommunication(configuration.NautConStr);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Kunne ikke Oprette forbindelse til Nautilus DB: " + ex.Message);
                return;
            }
            // Read entities from Nautilus
            List<BiobankEntity> entities = new List<BiobankEntity>();
            string operatorId = string.Empty;
            string entityTypeCheck = string.Empty;
            try
            {
                ADODB.Recordset records = (ADODB.Recordset)Parameters["RECORDS"];
                while (!records.EOF)
                {
                    foreach (ADODB.Field field in records.Fields)
                    {
                        string id = field.Value.ToString();
                        if (field.Name.Equals("ALIQUOT_ID"))
                        {
                            Thermo.Nautilus.Extensions.Model.Aliquot nautilusAliquot = Nautilus.Aliquots[id, "ALIQUOT_ID"];
                            string barcode = nautilusAliquot["EXTERNAL_REFERENCE"].ToString();
                            entities.Add(new Sample(id, barcode));
                            string containerTypeId = nautilusAliquot["CONTAINER_TYPE_ID"].ToString();
                            if (entityTypeCheck != string.Empty)
                            {
                                if (containerTypeId != entityTypeCheck)
                                {
                                    MessageBox.Show("Ikke alle prøver er af samme type");
                                    return;
                                }
                            }
                            entityTypeCheck = containerTypeId;
                        }
                        else if (field.Name.Equals("PLATE_ID"))
                        {
                            Thermo.Nautilus.Extensions.Model.Plate nautilusPlate = Nautilus.Plates[id, "PLATE_ID"];
                            string barcode = nautilusPlate["EXTERNAL_REFERENCE"].ToString();
                            if (entityTypeCheck != string.Empty)
                            {
                                if (!barcode.StartsWith(entityTypeCheck))
                                {
                                    MessageBox.Show("Ikke alle prøver er af samme type");
                                    return;
                                }
                            }
                            else if (barcode.StartsWith("CR"))
                            {
                                entityTypeCheck = "CR";
                            }
                            else if (barcode.StartsWith("MR0"))
                            {
                                entityTypeCheck = "MR0";
                            }
                            else if (barcode.StartsWith("MR1"))
                            {
                                entityTypeCheck = "MR1";
                            }
                            else if (barcode.StartsWith("OPKU"))
                            {
                                entityTypeCheck = "OPKU";
                            }
                            else if (barcode.StartsWith("NPKU"))
                            {
                                entityTypeCheck = "NPKU";
                            }
                            else if (barcode.StartsWith("600"))
                            {
                                // Changed by ESAT 20180221
                                // to enable new plates to enter Brooks.
                                entityTypeCheck = "600";
                            }
                            else if (barcode.StartsWith("500"))
                            {
                                // Changed by ESAT 20180413
                                // to enable new plates to enter Brooks.
                                entityTypeCheck = "500";
                            }
                            else if (barcode.StartsWith("400"))
                            {
                                // Changed by ESAT 20180413
                                // to enable new plates to enter Brooks.
                                entityTypeCheck = "400";
                            }
                            else if (barcode.StartsWith("BS"))
                            {
                                // Changed by ESAT 20181023
                                // to enable new plates to enter Brooks.
                                entityTypeCheck = "BS";
                            }
                            else
                            {
                                MessageBox.Show("Ugyldig pladetype: " + barcode);
                                return;
                            }
                            entities.Add(new Plate(id, barcode));
                        }
                        else
                        {
                            MessageBox.Show("Nautilus objekt er hverken prøve eller plade");
                            return;
                        }
                    }
                    records.MoveNext();
                }
                if (entities.Count == 0)
                {
                    MessageBox.Show("Vælg venligst prøver eller plader inden extensionen åbnes");
                    return;
                }
                operatorId = limsCommunication.OperatorGetInitials(Parameters["OPERATOR_ID"]);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Kunne ikke læse data fra Nautilus: " + ex.Message);
                return;
            }
            // Create BrooksCommunication
            BrooksCommunication brooksCommunication = null;
            try
            {
                brooksCommunication = new BrooksCommunication(configuration.BrooksConStr);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Kunne ikke Oprette forbindelse til Brooks: " + ex.Message);
                return;
            }
            //// Open extension form
            BrooksExtensionForm form = new BrooksExtensionForm(entities, operatorId, brooksCommunication, limsCommunication, configuration.Mode == "Production");
            form.ShowDialog();
            brooksCommunication.Dispose();
            limsCommunication.Dispose();
        }
    }
}
