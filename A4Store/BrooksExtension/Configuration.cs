﻿using DNBTools.Nautilus;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace BrooksExtension
{
    public class Configuration
    {
        internal string Mode { get; set; }
        internal string NautConStr { get; set; }
        internal string BrooksConStr { get; set; }

        public Configuration()
        {
            string location = Path.Combine(Application.StartupPath, "DNB_Extensions", "Brooks");
            UriBuilder uri = new UriBuilder(location);
            string path = Uri.UnescapeDataString(uri.Path);
            XmlDocument configurationDoc = new XmlDocument();
            configurationDoc.Load(Path.Combine(path, "Configuration.xml"));
            Mode = configurationDoc.SelectSingleNode("/Configuration/Mode").InnerXml;
            if (NautilusTools.IsProduction())
            {
                NautConStr = configurationDoc.SelectSingleNode("/Configuration/NautConStrProd").InnerXml;
            }
            else
            {
                NautConStr = configurationDoc.SelectSingleNode("/Configuration/NautConStrTest").InnerXml;
            }
            BrooksConStr = configurationDoc.SelectSingleNode("/Configuration/BrooksConStr").InnerXml;
        }
    }
}
