﻿namespace BrooksExtension
{
    partial class _toolsUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._sampleLocationComboBox = new System.Windows.Forms.ComboBox();
            this._plateLocationComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this._sampleLocationInButton = new System.Windows.Forms.Button();
            this._sampleLocationOutButton = new System.Windows.Forms.Button();
            this._plateLocationOutButton = new System.Windows.Forms.Button();
            this._plateLocationInButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._pkuRemoveButton = new System.Windows.Forms.Button();
            this._pkuSampleComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._bufferDataGridView = new System.Windows.Forms.DataGridView();
            this.TrayNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlateTypeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ErrorColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlatesColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._bufferDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this._sampleLocationComboBox);
            this.groupBox1.Controls.Add(this._plateLocationComboBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._sampleLocationInButton);
            this.groupBox1.Controls.Add(this._sampleLocationOutButton);
            this.groupBox1.Controls.Add(this._plateLocationOutButton);
            this.groupBox1.Controls.Add(this._plateLocationInButton);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(544, 142);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Opdater Placering i Lims";
            // 
            // _sampleLocationComboBox
            // 
            this._sampleLocationComboBox.FormattingEnabled = true;
            this._sampleLocationComboBox.Location = new System.Drawing.Point(6, 94);
            this._sampleLocationComboBox.Name = "_sampleLocationComboBox";
            this._sampleLocationComboBox.Size = new System.Drawing.Size(124, 21);
            this._sampleLocationComboBox.TabIndex = 6;
            // 
            // _plateLocationComboBox
            // 
            this._plateLocationComboBox.FormattingEnabled = true;
            this._plateLocationComboBox.Location = new System.Drawing.Point(6, 41);
            this._plateLocationComboBox.Name = "_plateLocationComboBox";
            this._plateLocationComboBox.Size = new System.Drawing.Size(124, 21);
            this._plateLocationComboBox.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Prøve Stregkode:";
            // 
            // _sampleLocationInButton
            // 
            this._sampleLocationInButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._sampleLocationInButton.BackgroundImage = global::BrooksExtension.Properties.Resources.In;
            this._sampleLocationInButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._sampleLocationInButton.Location = new System.Drawing.Point(290, 78);
            this._sampleLocationInButton.Name = "_sampleLocationInButton";
            this._sampleLocationInButton.Size = new System.Drawing.Size(121, 53);
            this._sampleLocationInButton.TabIndex = 0;
            this._sampleLocationInButton.Text = "          Indsæt";
            this._sampleLocationInButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._sampleLocationInButton.UseVisualStyleBackColor = true;
            this._sampleLocationInButton.Click += new System.EventHandler(this._sampleLocationInButton_Click);
            // 
            // _sampleLocationOutButton
            // 
            this._sampleLocationOutButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._sampleLocationOutButton.BackgroundImage = global::BrooksExtension.Properties.Resources.Out;
            this._sampleLocationOutButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._sampleLocationOutButton.Location = new System.Drawing.Point(417, 78);
            this._sampleLocationOutButton.Name = "_sampleLocationOutButton";
            this._sampleLocationOutButton.Size = new System.Drawing.Size(121, 53);
            this._sampleLocationOutButton.TabIndex = 2;
            this._sampleLocationOutButton.Text = "          Udtag";
            this._sampleLocationOutButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._sampleLocationOutButton.UseVisualStyleBackColor = true;
            this._sampleLocationOutButton.Click += new System.EventHandler(this._sampleLocationOutButton_Click);
            // 
            // _plateLocationOutButton
            // 
            this._plateLocationOutButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._plateLocationOutButton.BackgroundImage = global::BrooksExtension.Properties.Resources.Out;
            this._plateLocationOutButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._plateLocationOutButton.Location = new System.Drawing.Point(417, 19);
            this._plateLocationOutButton.Name = "_plateLocationOutButton";
            this._plateLocationOutButton.Size = new System.Drawing.Size(121, 53);
            this._plateLocationOutButton.TabIndex = 2;
            this._plateLocationOutButton.Text = "          Udtag";
            this._plateLocationOutButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._plateLocationOutButton.UseVisualStyleBackColor = true;
            this._plateLocationOutButton.Click += new System.EventHandler(this._plateLocationOutButton_Click);
            // 
            // _plateLocationInButton
            // 
            this._plateLocationInButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._plateLocationInButton.BackgroundImage = global::BrooksExtension.Properties.Resources.In;
            this._plateLocationInButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._plateLocationInButton.Location = new System.Drawing.Point(290, 19);
            this._plateLocationInButton.Name = "_plateLocationInButton";
            this._plateLocationInButton.Size = new System.Drawing.Size(121, 53);
            this._plateLocationInButton.TabIndex = 0;
            this._plateLocationInButton.Text = "          Indsæt";
            this._plateLocationInButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._plateLocationInButton.UseVisualStyleBackColor = true;
            this._plateLocationInButton.Click += new System.EventHandler(this._plateLocationInButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Plade Stregkode:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Prøve Stregkode:";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this._pkuRemoveButton);
            this.groupBox2.Controls.Add(this._pkuSampleComboBox);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(3, 151);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(544, 83);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "PKU Kort - Fjern fra boks";
            // 
            // _pkuRemoveButton
            // 
            this._pkuRemoveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._pkuRemoveButton.BackgroundImage = global::BrooksExtension.Properties.Resources.Out;
            this._pkuRemoveButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._pkuRemoveButton.Location = new System.Drawing.Point(417, 19);
            this._pkuRemoveButton.Name = "_pkuRemoveButton";
            this._pkuRemoveButton.Size = new System.Drawing.Size(121, 53);
            this._pkuRemoveButton.TabIndex = 7;
            this._pkuRemoveButton.Text = "          Udtag";
            this._pkuRemoveButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._pkuRemoveButton.UseVisualStyleBackColor = true;
            this._pkuRemoveButton.Click += new System.EventHandler(this._pkuRemoveButton_Click);
            // 
            // _pkuSampleComboBox
            // 
            this._pkuSampleComboBox.FormattingEnabled = true;
            this._pkuSampleComboBox.Location = new System.Drawing.Point(6, 35);
            this._pkuSampleComboBox.Name = "_pkuSampleComboBox";
            this._pkuSampleComboBox.Size = new System.Drawing.Size(124, 21);
            this._pkuSampleComboBox.TabIndex = 10;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this._bufferDataGridView);
            this.groupBox3.Location = new System.Drawing.Point(3, 240);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(541, 162);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Trays og plader i Buffer";
            // 
            // _bufferDataGridView
            // 
            this._bufferDataGridView.AllowUserToAddRows = false;
            this._bufferDataGridView.AllowUserToDeleteRows = false;
            this._bufferDataGridView.AllowUserToOrderColumns = true;
            this._bufferDataGridView.AllowUserToResizeRows = false;
            this._bufferDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._bufferDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._bufferDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._bufferDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TrayNameColumn,
            this.PlateTypeColumn,
            this.ErrorColumn,
            this.PlatesColumn});
            this._bufferDataGridView.Location = new System.Drawing.Point(9, 19);
            this._bufferDataGridView.Name = "_bufferDataGridView";
            this._bufferDataGridView.RowHeadersVisible = false;
            this._bufferDataGridView.Size = new System.Drawing.Size(526, 137);
            this._bufferDataGridView.TabIndex = 2;
            // 
            // TrayNameColumn
            // 
            this.TrayNameColumn.HeaderText = "Tray";
            this.TrayNameColumn.MinimumWidth = 100;
            this.TrayNameColumn.Name = "TrayNameColumn";
            this.TrayNameColumn.ReadOnly = true;
            // 
            // PlateTypeColumn
            // 
            this.PlateTypeColumn.HeaderText = "Type";
            this.PlateTypeColumn.MinimumWidth = 100;
            this.PlateTypeColumn.Name = "PlateTypeColumn";
            this.PlateTypeColumn.ReadOnly = true;
            // 
            // ErrorColumn
            // 
            this.ErrorColumn.HeaderText = "Error";
            this.ErrorColumn.MinimumWidth = 50;
            this.ErrorColumn.Name = "ErrorColumn";
            this.ErrorColumn.ReadOnly = true;
            this.ErrorColumn.Width = 54;
            // 
            // PlatesColumn
            // 
            this.PlatesColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PlatesColumn.HeaderText = "Plader";
            this.PlatesColumn.MinimumWidth = 266;
            this.PlatesColumn.Name = "PlatesColumn";
            this.PlatesColumn.ReadOnly = true;
            // 
            // _toolsUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "_toolsUserControl";
            this.Size = new System.Drawing.Size(547, 418);
            this.VisibleChanged += new System.EventHandler(this._toolsUserControl_VisibleChanged);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._bufferDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox _plateLocationComboBox;
        private System.Windows.Forms.Button _plateLocationOutButton;
        private System.Windows.Forms.Button _plateLocationInButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox _sampleLocationComboBox;
        private System.Windows.Forms.Button _sampleLocationOutButton;
        private System.Windows.Forms.Button _sampleLocationInButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button _pkuRemoveButton;
        private System.Windows.Forms.ComboBox _pkuSampleComboBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView _bufferDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrayNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlateTypeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ErrorColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlatesColumn;

    }
}
