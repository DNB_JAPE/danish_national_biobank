﻿using DNBTools;
using DNBTools.Brooks;
using DNBTools.DataClasses;
using DNBTools.Nautilus;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace BrooksExtension
{
    public partial class _toolsUserControl : BrooksUserControl
    {
        private List<BiobankEntity> _entities;

        public _toolsUserControl()
        {
            InitializeComponent();
        }

        public void Initialize(List<BiobankEntity> entities, string operatorName, BrooksCommunication brooksCommunication, LimsCommunication limsCommunication, bool productionMode)
        {
            base.Initialize(operatorName, limsCommunication, brooksCommunication, productionMode);
            _entities = entities;
        }

        internal void EnableUI(bool enable)
        {
            _plateLocationComboBox.Enabled = enable;
            _sampleLocationComboBox.Enabled = enable;
            _plateLocationInButton.Enabled = enable;
            _plateLocationOutButton.Enabled = enable;
            _pkuSampleComboBox.Enabled = enable;
            _pkuRemoveButton.Enabled = enable;
            Refresh();
        }

        private void _toolsUserControl_VisibleChanged(object sender, EventArgs e)
        {
            if (!DesignMode && _entities != null && _entities.Count > 0)
            {
                _plateLocationComboBox.Items.Clear();
                if (_entities[0] is Plate)
                {
                    foreach (BiobankEntity entity in _entities)
                    {
                        EntityListBoxItem item = new EntityListBoxItem(entity);
                        _plateLocationComboBox.Items.Add(item);
                    }
                }
                _sampleLocationComboBox.Items.Clear();
                _pkuSampleComboBox.Items.Clear();
                if (_entities[0] is Sample)
                {
                    foreach (BiobankEntity entity in _entities)
                    {
                        EntityListBoxItem item = new EntityListBoxItem(entity);
                        _sampleLocationComboBox.Items.Add(item);
                    }
                    _pkuSampleComboBox.Items.Add("<Alle>");
                    foreach (BiobankEntity entity in _entities)
                    {
                        EntityListBoxItem item = new EntityListBoxItem(entity);
                        _pkuSampleComboBox.Items.Add(item);
                    }
                }
                Tuple<List<BrooksTray>, BrooksOrderState> state = _brooksCommunication.GetBufferTrays();
                if (state.Item2.ErrorNumber != 0) 
                {
                    MessageBox.Show("Fejl under oprettelse af ordre: " + state.Item2.ErrorNumber + " - " + state.Item2.ErrorMessage);
                    return;
                }
                if (state.Item1 != null)
                {
                    foreach (BrooksTray tray in state.Item1)
                    {
                        string plates = string.Empty;
                        foreach (string plate in tray.Plates)
                        {
                            plates = plates + plate + ", ";
                        }
                        plates = plates.TrimEnd(new char[] { ',', ' ' });
                        _bufferDataGridView.Rows.Add(tray.Name, tray.ContentType, tray.IsErrored, plates);
                    }
                }
            }
        }

        private void _plateLocationInButton_Click(object sender, EventArgs e)
        {
            EnableUI(false);
            if (!string.IsNullOrWhiteSpace(_plateLocationComboBox.Text))
            {
                try
                {
                    EntityListBoxItem item = _plateLocationComboBox.SelectedItem as EntityListBoxItem;
                    _limsCommunication.UpdateLocationForCarrier(item.Entity.LimsId, _limsCommunication.LocationGetId(LimsCommunication.BrooksLocationName));
                    _plateLocationComboBox.SelectedIndex = -1;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Fejl under opdatering af Lims: " + ex.Message + Environment.NewLine + ex.StackTrace);
                }
            }
            EnableUI(true);
        }

        private void _plateLocationOutButton_Click(object sender, EventArgs e)
        {
            EnableUI(false);
            if (!string.IsNullOrWhiteSpace(_plateLocationComboBox.Text))
            {
                try
                {
                    EntityListBoxItem item = _plateLocationComboBox.SelectedItem as EntityListBoxItem;
                    _limsCommunication.UpdateLocationForCarrier(item.Entity.LimsId, _limsCommunication.LocationGetId(LimsCommunication.PickedLocationName));
                    _plateLocationComboBox.SelectedIndex = -1;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Fejl under opdatering af Lims: " + ex.Message + Environment.NewLine + ex.StackTrace);
                }
            }
            EnableUI(true);
        }

        private void _sampleLocationInButton_Click(object sender, EventArgs e)
        {
            EnableUI(false);
            if (!string.IsNullOrWhiteSpace(_sampleLocationComboBox.Text))
            {
                try
                {
                    EntityListBoxItem item = _sampleLocationComboBox.SelectedItem as EntityListBoxItem;
                    _limsCommunication.UpdateLocationForSample(item.Entity.LimsId, _limsCommunication.LocationGetId(LimsCommunication.BrooksLocationName));
                    _plateLocationComboBox.SelectedIndex = -1;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Fejl under opdatering af Lims: " + ex.Message + Environment.NewLine + ex.StackTrace);
                }
            }
            EnableUI(true);
        }

        private void _sampleLocationOutButton_Click(object sender, EventArgs e)
        {
            EnableUI(false);
            if (!string.IsNullOrWhiteSpace(_sampleLocationComboBox.Text))
            {
                try
                {
                    EntityListBoxItem item = _sampleLocationComboBox.SelectedItem as EntityListBoxItem;
                    _limsCommunication.UpdateLocationForSample(item.Entity.LimsId, _limsCommunication.LocationGetId(LimsCommunication.PickedLocationName));
                    _sampleLocationComboBox.SelectedIndex = -1;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Fejl under opdatering af Lims: " + ex.Message + Environment.NewLine + ex.StackTrace);
                }
            }
            EnableUI(true);
        }

        private void _pkuRemoveButton_Click(object sender, EventArgs e)
        {
            EnableUI(false);
            if (!string.IsNullOrWhiteSpace(_pkuSampleComboBox.Text))
            {
                try
                {
                    if (_pkuSampleComboBox.SelectedItem is EntityListBoxItem)
                    {
                        EntityListBoxItem item = _pkuSampleComboBox.SelectedItem as EntityListBoxItem;
                        _limsCommunication.SamplePKURemove(item.Entity.LimsId);
                    }
                    else
                    {
                        foreach(EntityListBoxItem item in _pkuSampleComboBox.Items.OfType<EntityListBoxItem>())
                        {
                            _limsCommunication.SamplePKURemove(item.Entity.LimsId);
                        }
                    }
                    _pkuSampleComboBox.SelectedIndex = -1;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Fejl under opdatering af Lims: " + ex.Message + Environment.NewLine + ex.StackTrace);
                }
            }
            EnableUI(true);
        }
    }
}
