﻿namespace BrooksExtension
{
    partial class OrderUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this._stateComboBox = new System.Windows.Forms.ComboBox();
            this._dataGridView = new System.Windows.Forms.DataGridView();
            this._orderColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._operatorColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._priorityColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._stateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._abwareColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._startColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._completeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._descriptionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._updateButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Status:";
            // 
            // _stateComboBox
            // 
            this._stateComboBox.FormattingEnabled = true;
            this._stateComboBox.Items.AddRange(new object[] {
            "Alle",
            "NEW",
            "SENT",
            "ISSUED",
            "ACCEPTED",
            "PENDING",
            "PROCESSING",
            "COMPLETE",
            "RECEIVED",
            "REJECTED"});
            this._stateComboBox.Location = new System.Drawing.Point(58, 7);
            this._stateComboBox.Name = "_stateComboBox";
            this._stateComboBox.Size = new System.Drawing.Size(121, 21);
            this._stateComboBox.TabIndex = 1;
            this._stateComboBox.SelectedIndexChanged += new System.EventHandler(this._stateComboBox_SelectedIndexChanged);
            // 
            // _dataGridView
            // 
            this._dataGridView.AllowUserToAddRows = false;
            this._dataGridView.AllowUserToDeleteRows = false;
            this._dataGridView.AllowUserToResizeRows = false;
            this._dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._orderColumn,
            this._operatorColumn,
            this._priorityColumn,
            this._stateColumn,
            this._typeColumn,
            this._abwareColumn,
            this._startColumn,
            this._completeColumn,
            this._descriptionColumn});
            this._dataGridView.Location = new System.Drawing.Point(3, 34);
            this._dataGridView.MultiSelect = false;
            this._dataGridView.Name = "_dataGridView";
            this._dataGridView.RowHeadersVisible = false;
            this._dataGridView.Size = new System.Drawing.Size(837, 332);
            this._dataGridView.TabIndex = 2;
            this._dataGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this._dataGridView_MouseUp);
            // 
            // _orderColumn
            // 
            this._orderColumn.HeaderText = "Ordre";
            this._orderColumn.Name = "_orderColumn";
            this._orderColumn.ReadOnly = true;
            // 
            // _operatorColumn
            // 
            this._operatorColumn.HeaderText = "Operatør";
            this._operatorColumn.Name = "_operatorColumn";
            this._operatorColumn.ReadOnly = true;
            // 
            // _priorityColumn
            // 
            this._priorityColumn.HeaderText = "Prioritet";
            this._priorityColumn.Name = "_priorityColumn";
            this._priorityColumn.ReadOnly = true;
            // 
            // _stateColumn
            // 
            this._stateColumn.HeaderText = "Status";
            this._stateColumn.Name = "_stateColumn";
            this._stateColumn.ReadOnly = true;
            // 
            // _typeColumn
            // 
            this._typeColumn.HeaderText = "Type";
            this._typeColumn.Name = "_typeColumn";
            this._typeColumn.ReadOnly = true;
            // 
            // _abwareColumn
            // 
            this._abwareColumn.HeaderText = "Labware";
            this._abwareColumn.Name = "_abwareColumn";
            this._abwareColumn.ReadOnly = true;
            // 
            // _startColumn
            // 
            this._startColumn.HeaderText = "Oprettet";
            this._startColumn.Name = "_startColumn";
            this._startColumn.ReadOnly = true;
            // 
            // _completeColumn
            // 
            this._completeColumn.HeaderText = "Færdig";
            this._completeColumn.Name = "_completeColumn";
            this._completeColumn.ReadOnly = true;
            // 
            // _descriptionColumn
            // 
            this._descriptionColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._descriptionColumn.HeaderText = "Kommentar";
            this._descriptionColumn.Name = "_descriptionColumn";
            this._descriptionColumn.ReadOnly = true;
            // 
            // _updateButton
            // 
            this._updateButton.Location = new System.Drawing.Point(185, 5);
            this._updateButton.Name = "_updateButton";
            this._updateButton.Size = new System.Drawing.Size(75, 23);
            this._updateButton.TabIndex = 3;
            this._updateButton.Text = "Opdater";
            this._updateButton.UseVisualStyleBackColor = true;
            this._updateButton.Click += new System.EventHandler(this._updateButton_Click);
            // 
            // OrderUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._updateButton);
            this.Controls.Add(this._dataGridView);
            this.Controls.Add(this._stateComboBox);
            this.Controls.Add(this.label1);
            this.Name = "OrderUserControl";
            this.Size = new System.Drawing.Size(843, 369);
            this.VisibleChanged += new System.EventHandler(this.OrderUserControl_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this._dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox _stateComboBox;
        private System.Windows.Forms.DataGridView _dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn _orderColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _operatorColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _priorityColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _stateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _typeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _abwareColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _startColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _completeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _descriptionColumn;
        private System.Windows.Forms.Button _updateButton;
    }
}
