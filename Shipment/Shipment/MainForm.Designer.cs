﻿namespace Shipment
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._plateDataGridView = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this._sampleCountTextBox = new System.Windows.Forms.TextBox();
            this._updateButton = new System.Windows.Forms.Button();
            this._abortButton = new System.Windows.Forms.Button();
            this._reportButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this._dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this._descriptionTextBox = new System.Windows.Forms.TextBox();
            this._sampleDataGridView = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this._stateLabel = new System.Windows.Forms.Label();
            this._sampleColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._sampleProjectColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._plateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._aliquotColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._projectColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this._plateDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sampleDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // _plateDataGridView
            // 
            this._plateDataGridView.AllowUserToAddRows = false;
            this._plateDataGridView.AllowUserToDeleteRows = false;
            this._plateDataGridView.AllowUserToResizeRows = false;
            this._plateDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._plateDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._plateDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._plateColumn,
            this._aliquotColumn,
            this._projectColumn});
            this._plateDataGridView.Location = new System.Drawing.Point(12, 12);
            this._plateDataGridView.Name = "_plateDataGridView";
            this._plateDataGridView.RowHeadersVisible = false;
            this._plateDataGridView.Size = new System.Drawing.Size(329, 431);
            this._plateDataGridView.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 458);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Antal prøver ialt:";
            // 
            // _sampleCountTextBox
            // 
            this._sampleCountTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._sampleCountTextBox.Enabled = false;
            this._sampleCountTextBox.Location = new System.Drawing.Point(103, 455);
            this._sampleCountTextBox.Name = "_sampleCountTextBox";
            this._sampleCountTextBox.ReadOnly = true;
            this._sampleCountTextBox.Size = new System.Drawing.Size(100, 20);
            this._sampleCountTextBox.TabIndex = 2;
            // 
            // _updateButton
            // 
            this._updateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._updateButton.Location = new System.Drawing.Point(275, 576);
            this._updateButton.Name = "_updateButton";
            this._updateButton.Size = new System.Drawing.Size(101, 23);
            this._updateButton.TabIndex = 3;
            this._updateButton.Text = "Marker som sendt";
            this._updateButton.UseVisualStyleBackColor = true;
            this._updateButton.Click += new System.EventHandler(this._updateButton_Click);
            // 
            // _abortButton
            // 
            this._abortButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._abortButton.Location = new System.Drawing.Point(382, 576);
            this._abortButton.Name = "_abortButton";
            this._abortButton.Size = new System.Drawing.Size(101, 23);
            this._abortButton.TabIndex = 4;
            this._abortButton.Text = "Abryd";
            this._abortButton.UseVisualStyleBackColor = true;
            // 
            // _reportButton
            // 
            this._reportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._reportButton.Location = new System.Drawing.Point(489, 576);
            this._reportButton.Name = "_reportButton";
            this._reportButton.Size = new System.Drawing.Size(101, 23);
            this._reportButton.TabIndex = 5;
            this._reportButton.Text = "Udskriv...";
            this._reportButton.UseVisualStyleBackColor = true;
            this._reportButton.Click += new System.EventHandler(this._reportButton_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 485);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Afsendelsesdato:";
            // 
            // _dateTimePicker
            // 
            this._dateTimePicker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this._dateTimePicker.Location = new System.Drawing.Point(103, 482);
            this._dateTimePicker.Name = "_dateTimePicker";
            this._dateTimePicker.Size = new System.Drawing.Size(238, 20);
            this._dateTimePicker.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 511);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Beskrivelse:";
            // 
            // _descriptionTextBox
            // 
            this._descriptionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._descriptionTextBox.Location = new System.Drawing.Point(103, 508);
            this._descriptionTextBox.Name = "_descriptionTextBox";
            this._descriptionTextBox.Size = new System.Drawing.Size(238, 20);
            this._descriptionTextBox.TabIndex = 9;
            this._descriptionTextBox.TextChanged += new System.EventHandler(this._descriptionTextBox_TextChanged);
            // 
            // _sampleDataGridView
            // 
            this._sampleDataGridView.AllowUserToAddRows = false;
            this._sampleDataGridView.AllowUserToDeleteRows = false;
            this._sampleDataGridView.AllowUserToResizeRows = false;
            this._sampleDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._sampleDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._sampleDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._sampleColumn,
            this._sampleProjectColumn});
            this._sampleDataGridView.Location = new System.Drawing.Point(342, 12);
            this._sampleDataGridView.Name = "_sampleDataGridView";
            this._sampleDataGridView.RowHeadersVisible = false;
            this._sampleDataGridView.Size = new System.Drawing.Size(248, 431);
            this._sampleDataGridView.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 581);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Status:";
            // 
            // _stateLabel
            // 
            this._stateLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._stateLabel.AutoSize = true;
            this._stateLabel.Location = new System.Drawing.Point(46, 581);
            this._stateLabel.Name = "_stateLabel";
            this._stateLabel.Size = new System.Drawing.Size(35, 13);
            this._stateLabel.TabIndex = 12;
            this._stateLabel.Text = "label5";
            // 
            // _sampleColumn
            // 
            this._sampleColumn.HeaderText = "Prøve";
            this._sampleColumn.Name = "_sampleColumn";
            this._sampleColumn.ReadOnly = true;
            // 
            // _sampleProjectColumn
            // 
            this._sampleProjectColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._sampleProjectColumn.HeaderText = "Projekt";
            this._sampleProjectColumn.Name = "_sampleProjectColumn";
            this._sampleProjectColumn.ReadOnly = true;
            // 
            // _plateColumn
            // 
            this._plateColumn.HeaderText = "Plade";
            this._plateColumn.Name = "_plateColumn";
            this._plateColumn.ReadOnly = true;
            // 
            // _aliquotColumn
            // 
            this._aliquotColumn.HeaderText = "Antal Prøver";
            this._aliquotColumn.Name = "_aliquotColumn";
            this._aliquotColumn.ReadOnly = true;
            // 
            // _projectColumn
            // 
            this._projectColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._projectColumn.HeaderText = "Projekt";
            this._projectColumn.Name = "_projectColumn";
            this._projectColumn.ReadOnly = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(602, 611);
            this.Controls.Add(this._stateLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._sampleDataGridView);
            this.Controls.Add(this._descriptionTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._dateTimePicker);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._reportButton);
            this.Controls.Add(this._abortButton);
            this.Controls.Add(this._updateButton);
            this.Controls.Add(this._sampleCountTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._plateDataGridView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.Text = "Shipment";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._plateDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sampleDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView _plateDataGridView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _sampleCountTextBox;
        private System.Windows.Forms.Button _updateButton;
        private System.Windows.Forms.Button _abortButton;
        private System.Windows.Forms.Button _reportButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker _dateTimePicker;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox _descriptionTextBox;
        private System.Windows.Forms.DataGridView _sampleDataGridView;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label _stateLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn _plateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _aliquotColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _projectColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _sampleColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _sampleProjectColumn;
    }
}