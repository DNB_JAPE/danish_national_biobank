﻿using DNBTools.Nautilus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shipment
{
    /// <summary>
    /// Report UI.
    /// </summary>
    public partial class ReportForm : Form
    {
        private LimsCommunication _limsCommunication;
        private List<Thermo.Nautilus.Extensions.Model.Aliquot> _data;

        public ReportForm(LimsCommunication limsCommunication, List<Thermo.Nautilus.Extensions.Model.Aliquot> data)
        {
            InitializeComponent();
            _data = data;
            _limsCommunication = limsCommunication;
        }

        /// <summary>
        /// Initialize the UI
        /// </summary>
        private void ReportForm_Load(object sender, EventArgs e)
        {
            _dataGridView.Rows.Add(new object[] { "Plade", true });
            _dataGridView.Rows.Add(new object[] { "Position", true });
            _dataGridView.Rows.Add(new object[] { "Stregkode", true });
            _dataGridView.Rows.Add(new object[] { "PersonID", false });
            _dataGridView.Rows.Add(new object[] { "Materiale", false });
            _dataGridView.Rows.Add(new object[] { "Volumen", false });
            _dataGridView.Rows.Add(new object[] { "Enhed", false });
            _dataGridView.Rows.Add(new object[] { "Pippeteringskommentar", false });
            _dataGridView.Rows.Add(new object[] { "Index på plade", false });
            _dataGridView.Rows.Add(new object[] { "Raekke", false });
            _dataGridView.Rows.Add(new object[] { "Kolonne", false });
            _dataGridView.Rows.Add(new object[] { "Provetagnings_dato", false });
            _dataGridView.Rows.Add(new object[] { "Fryse_dato", false });
            _dataGridView.Rows.Add(new object[] { "Maalt_Koncentration", false });
            _dataGridView.Rows.Add(new object[] { "Maalt_A260/A230", false });
            _dataGridView.Rows.Add(new object[] { "Maalt_A260/A280", false });
            _dataGridView.Rows.Add(new object[] { "Normaliseret_Koncentration", false });
            _dataGridView.Rows.Add(new object[] { "Maalt_Koncentration_Foraelder", false });
            _dataGridView.Rows.Add(new object[] { "Maalt_A260/A230_Foraelder", false });
            _dataGridView.Rows.Add(new object[] { "Maalt_A260/A280_Foraelder", false });
            _dataGridView.Rows.Add(new object[] { "Foraelder_Stregkode", false });
            _dataGridView.Rows.Add(new object[] { "Master_Stregkode", false });
            _dataGridView.Rows.Add(new object[] { "Aliquot_Location", false });
            _dataGridView.Rows.Add(new object[] { "Plate_Location", false });

        }

        /// <summary>
        /// Create the report with the selected columns.
        /// </summary>
        private void _saveButton_Click(object sender, EventArgs e)
        {
            try
            {

                _saveFileDialog.AddExtension = true;
                _saveFileDialog.DefaultExt = "csv";
                _saveFileDialog.OverwritePrompt = true;
                _saveFileDialog.Filter = "CVS files (*.csv)|*.csv|All files (*.*)|*.*";
                _saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                if (_saveFileDialog.ShowDialog(this) == DialogResult.OK)
                {
                    this.Cursor = Cursors.WaitCursor;

                    StreamWriter saveFile = new StreamWriter(_saveFileDialog.OpenFile());
                    // Write Header
                    string header = "";

                    progressBar.Maximum = _data.Count();

                    foreach (DataGridViewRow row in _dataGridView.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[1].Value))
                        {
                            header = header + row.Cells[0].Value.ToString() + ";";
                        }
                    }
                    header = header.TrimEnd(';');
                    saveFile.WriteLine(header);
                    int x = 1;
                    foreach (Thermo.Nautilus.Extensions.Model.Aliquot aliquot in _data)
                    {
                        progressBar.Value = x;

                        x++;
                        string line = "";
                        if (Convert.ToBoolean(_dataGridView.Rows[0].Cells[1].Value))
                        {
                            try
                            {
                                line = line + "\"" + aliquot.Plate.Name + "\";";
                            }
                            catch
                            {
                                line = line + "\"\";";
                            }
                        }
                        string position = _limsCommunication.SampleGetPosition(aliquot.Id.ToString());
                        if (Convert.ToBoolean(_dataGridView.Rows[1].Cells[1].Value)) { line = line + "\"" + (position == null ? "" : position) + "\";"; }
                        if (Convert.ToBoolean(_dataGridView.Rows[2].Cells[1].Value)) { line = line + "\"" + (aliquot["EXTERNAL_REFERENCE"] != null ? aliquot["EXTERNAL_REFERENCE"].ToString() : "") + "\";"; }
                        if (Convert.ToBoolean(_dataGridView.Rows[3].Cells[1].Value))
                        {
                            try
                            {
                                line = line + "\"" + _limsCommunication.BiobankClientGetClientProjectPid(aliquot.Sample.Sdg["EXTERNAL_REFERENCE"].ToString(), aliquot.Sample["STUDY_ID"].ToString()) + "\";";
                            }
                            catch
                            {
                                line = line + "\"\";";
                            }
                        }
                        if (Convert.ToBoolean(_dataGridView.Rows[4].Cells[1].Value)) { line = line + "\"" + (aliquot["MATRIX_TYPE"] != null ? aliquot["MATRIX_TYPE"].ToString() : "") + "\";"; }
                        if (Convert.ToBoolean(_dataGridView.Rows[5].Cells[1].Value)) { line = line + "\"" + (aliquot["AMOUNT"] != null ? aliquot["AMOUNT"].ToString() : "") + "\";"; }
                        if (Convert.ToBoolean(_dataGridView.Rows[6].Cells[1].Value)) { line = line + "\"" + _limsCommunication.UnitGetName(aliquot["UNIT_ID"].ToString()) + "\";"; }
                        if (Convert.ToBoolean(_dataGridView.Rows[7].Cells[1].Value)) { line = line + "\"" + (aliquot["U_PIPETTING_COMMENT"] != null ? aliquot["U_PIPETTING_COMMENT"].ToString() : "") + "\";"; }
                        if (Convert.ToBoolean(_dataGridView.Rows[8].Cells[1].Value)) { line = line + "\"" + (aliquot["PLATE_ORDER"] != null ? aliquot["PLATE_ORDER"] : "") + "\";"; }
                        if (Convert.ToBoolean(_dataGridView.Rows[9].Cells[1].Value)) { line = line + "\"" + (aliquot["PLATE_ROW"] != null ? aliquot["PLATE_ROW"] : "") + "\";"; }
                        if (Convert.ToBoolean(_dataGridView.Rows[10].Cells[1].Value)) { line = line + "\"" + (aliquot["PLATE_COLUMN"] != null ? aliquot["PLATE_COLUMN"] : "") + "\";"; }
                        if (Convert.ToBoolean(_dataGridView.Rows[11].Cells[1].Value)) { line = line + "\"" + (aliquot.Sample["SAMPLED_ON"] != null ? aliquot.Sample["SAMPLED_ON"].ToString() : "") + "\";"; }
                        if (Convert.ToBoolean(_dataGridView.Rows[12].Cells[1].Value)) { line = line + "\"" + (aliquot["RECEIVED_ON"] != null ? aliquot["RECEIVED_ON"].ToString() : "") + "\";"; }
                        Tuple<string, string, string> testResults = _limsCommunication.TestGetConcentrationResults(aliquot.Id.ToString());
                        if (Convert.ToBoolean(_dataGridView.Rows[13].Cells[1].Value)) { line = line + "\"" + (testResults != null ? testResults.Item1 : "") + "\";"; }
                        if (Convert.ToBoolean(_dataGridView.Rows[14].Cells[1].Value)) { line = line + "\"" + (testResults != null ? testResults.Item2 : "") + "\";"; }
                        if (Convert.ToBoolean(_dataGridView.Rows[15].Cells[1].Value)) { line = line + "\"" + (testResults != null ? testResults.Item3 : "") + "\";"; }
                        if (Convert.ToBoolean(_dataGridView.Rows[16].Cells[1].Value)) { line = line + "\"" + (aliquot["U_KONC"] != null ? aliquot["U_KONC"].ToString() : "") + "\";"; }
                        Tuple<string, string, string> ParentTestResults = _limsCommunication.TestGetParentConcentrationResults(aliquot.Id.ToString());
                        if (Convert.ToBoolean(_dataGridView.Rows[17].Cells[1].Value)) { line = line + "\"" + (ParentTestResults != null ? ParentTestResults.Item1 : "") + "\";"; }
                        if (Convert.ToBoolean(_dataGridView.Rows[18].Cells[1].Value)) { line = line + "\"" + (ParentTestResults != null ? ParentTestResults.Item2 : "") + "\";"; }
                        if (Convert.ToBoolean(_dataGridView.Rows[19].Cells[1].Value)) { line = line + "\"" + (ParentTestResults != null ? ParentTestResults.Item3 : "") + "\";"; }
                        // Get parent barcode
                        if (Convert.ToBoolean(_dataGridView.Rows[20].Cells[1].Value))
                        {
                            try
                            {
                                line = line + "\"" + _limsCommunication.SampleGetParentBarcode(aliquot["ALIQUOT_ID"].ToString()) + "\";";
                            }
                            catch
                            {
                                line = line + "\"\";";
                            }
                        }
                        // Get Master barcode
                        if (Convert.ToBoolean(_dataGridView.Rows[21].Cells[1].Value))
                        {
                            try
                            {
                                line = line + "\"" + _limsCommunication.SampleGetMasterBarcode(aliquot["ALIQUOT_ID"].ToString()) + "\";";
                            }
                            catch
                            {
                                line = line + "\"\";";
                            }
                        }
                        // Get Aliquot location name
                        if (Convert.ToBoolean(_dataGridView.Rows[22].Cells[1].Value)) { line = line + "\"" + _limsCommunication.GetTubeLocationName(aliquot.Id.ToString()) + "\";"; }
                        // Get Plate location name
                        if (Convert.ToBoolean(_dataGridView.Rows[23].Cells[1].Value)) { line = line + "\"" + _limsCommunication.GetPlateLocationName(aliquot.Plate_Id.ToString()) + "\";"; }


                        line = line.TrimEnd(';');
                        saveFile.WriteLine(line);
                    }
                    saveFile.Close();
                    this.Cursor = Cursors.Default;
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Der skete en fejl ved databehandling. Kontakt IT afdelingen: " + ex.Message);
            }
        }
    }
}
