﻿using DNBTools;
using DNBTools.Nautilus;
using LSEXT;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Schema;
using Thermo.Nautilus.Extensions.Database;
using Thermo.Nautilus.Extensions.Model;
using Thermo.Nautilus.Extensions.UserInterface;

namespace Shipment
{
    /// <summary>
    /// Extension for setting shipment data.
    /// </summary>
    public class ShipmentExtension : EntityExtension, IVersion
    {
        private string _logFile;

        public int GetVersion()
        {
            return 3;
        }

        #region Extension
        /// <summary>
        /// Called by Lims to decide if the extension should be listed in the right click menu.
        /// </summary>
        /// <param name="Parameters">Lims parameters (not used).</param>
        /// <returns>ExecuteExtension.exEnabled - Always allow this extension to start.</returns>
        public override EntityExtension.enumExtensionVisibility CanExecuteExtension()
        {
            return EntityExtension.enumExtensionVisibility.Enabled;
        }

        /// <summary>
        /// Called by Lims to activate the extension.
        /// </summary>
        public override void ExecuteExtension()
        {
            try
            {
                LoadingForm loading = new LoadingForm();
                loading.Show();

                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                string configurationPath = Path.GetDirectoryName(path);
                _logFile = Path.Combine(configurationPath, (DateTime.Today.ToShortDateString() + ".log"));
                try
                {
                    XmlDocument configurationDoc = new XmlDocument();
                    configurationDoc.Load(Path.Combine(configurationPath, "Configuration.xml"));
                    string logDir = configurationDoc.SelectSingleNode("/Configuration/LogDir").InnerXml;
                    _logFile = Path.Combine(logDir, (DateTime.Today.ToShortDateString() + ".log"));
                    Log("Starter extension");
                    string connectionString;
                    if (NautilusTools.IsProduction())
                    {
                        connectionString = configurationDoc.SelectSingleNode("/Configuration/NautConStrProd").InnerXml;
                    }
                    else
                    {
                        connectionString = configurationDoc.SelectSingleNode("/Configuration/NautConStrTest").InnerXml;
                    }
                    LimsCommunication limsCommunication = new LimsCommunication(connectionString);
                    // Read entities from Nautilus
                    List<Thermo.Nautilus.Extensions.Model.Aliquot> entities = new List<Thermo.Nautilus.Extensions.Model.Aliquot>();
                    string operatorId = string.Empty;
                    string emptyPlates = "";
                    bool plateMode = true;
                    try
                    {
                        ADODB.Recordset records = (ADODB.Recordset)Parameters["RECORDS"];
                        while (!records.EOF)
                        {
                            string barcodePrefix = string.Empty;
                            foreach (ADODB.Field field in records.Fields)
                            {
                                string id = field.Value.ToString();
                                if (field.Name.Equals("ALIQUOT_ID"))
                                {
                                    plateMode = false;
                                    entities.Add(Nautilus.Aliquots[id, "ALIQUOT_ID"]);
                                }
                                else if (field.Name.Equals("PLATE_ID"))
                                {
                                    Thermo.Nautilus.Extensions.Model.Plate nautilusPlate = Nautilus.Plates[id, "PLATE_ID"];
                                    if (nautilusPlate.Aliquots.Count() == 0)
                                    {
                                        emptyPlates = emptyPlates + ", " + nautilusPlate["EXTERNAL_REFERENCE"].ToString();
                                    }
                                    entities.AddRange(nautilusPlate.Aliquots);
                                }
                                else
                                {
                                    Log("Nautilus objekt er hverken prøve eller plade - afbryder");
                                    MessageBox.Show("Nautilus objekt er hverken prøve eller plade");
                                    return;
                                }
                            }
                            records.MoveNext();
                        }
                        operatorId = limsCommunication.OperatorGetInitials(Parameters["OPERATOR_ID"]);
                    }
                    catch (Exception ex)
                    {
                        Log("Kunne ikke læse data fra Nautilus: " + ex.Message);
                        MessageBox.Show("Kunne ikke læse data fra Nautilus: " + ex.Message);
                        return;
                    }
                    if (emptyPlates != "")
                    {
                        MessageBox.Show("Følgende plader er tomme og ignoreres: " + emptyPlates.TrimEnd(','));
                    }
                    MainForm form = new MainForm(limsCommunication, entities, operatorId, plateMode);
                    loading.Close();
                    form.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Øv, kunne starte Extension'en: " + ex.Message);
                    Log(ex.Message);
                }
            }
            catch (Exception x)
            {
                MessageBox.Show("Øv, fejlede da jeg ville starte Extension'en: " + x.Message);
                Log(x.Message);
            }

        }
        #endregion Extension

        /// <summary>
        /// Log message
        /// </summary>
        /// <param name="message"></param>
        internal static void Log(string message)
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            string configurationPath = Path.GetDirectoryName(path);
            string logFile = Path.Combine(configurationPath, (DateTime.Today.ToShortDateString() + ".log"));
            File.AppendAllText(logFile, DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + " " + message + Environment.NewLine);
        }
    }
}
