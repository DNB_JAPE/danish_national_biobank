﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shipment
{
    /// <summary>
    /// Helper dialog. If the selected labware is from more than one project, this dialog is shown.
    /// The user must select which project the shipment is for.
    /// </summary>
    public partial class SelectProjectForm : Form
    {
        public string SelectedProject { get; private set; }
        private List<string> _projects;

        public SelectProjectForm(List<string> projects)
        {
            InitializeComponent();
            _projects = projects;
        }

        /// <summary>
        /// Initialize UI
        /// </summary>
        private void SelectProjectForm_Load(object sender, EventArgs e)
        {
            _dataGridView.Rows.Add("<Ingen>");
            foreach (string project in _projects)
            {
                _dataGridView.Rows.Add(project);
            }
            _dataGridView.Rows[0].Selected = true;
            _okButton.Enabled = true;
        }

        /// <summary>
        /// Update state when the user changes selection
        /// </summary>
        private void _dataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (_dataGridView.SelectedRows.Count == 1 && _dataGridView.SelectedRows[0].Cells[0].Value != null)
            {
                SelectedProject = _dataGridView.SelectedRows[0].Cells[0].Value.ToString();
                _okButton.Enabled = true;
            }
            else 
            {
                _okButton.Enabled = false;
                SelectedProject = "";
            }
        }
    }
}
