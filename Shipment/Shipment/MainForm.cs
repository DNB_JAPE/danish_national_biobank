﻿using DNBTools.Nautilus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shipment
{
    /// <summary>
    /// This is the central UI in the project.
    /// </summary>
    public partial class MainForm : Form
    {
        private LimsCommunication _limsCommunication;
        private string _operatorId;
        private bool _plateMode;
        private List<Thermo.Nautilus.Extensions.Model.Aliquot> _data;
        
        public MainForm(LimsCommunication limsCommunication, List<Thermo.Nautilus.Extensions.Model.Aliquot> data, string operatorId, bool plateMode)
        {
            InitializeComponent();
            _limsCommunication = limsCommunication;
            _data = data;
            _operatorId = operatorId;
            _plateMode = plateMode;
        }

        /// <summary>
        /// Initialize UI
        /// </summary>
        private void MainForm_Load(object sender, EventArgs e)
        {
            _plateDataGridView.Rows.Clear();
            Dictionary<string, Triple> plates = new Dictionary<string, Triple>();
            List<string> projects = new List<string>();
            foreach (Thermo.Nautilus.Extensions.Model.Aliquot aliquot in _data)
            {
                string project = aliquot["U_PROJECTNR"] == null ? "" : aliquot["U_PROJECTNR"].ToString();
                if (!projects.Contains(project)) { projects.Add(project); }
                _sampleDataGridView.Rows.Add(new object[] { aliquot["EXTERNAL_REFERENCE"], project });
                if(_plateMode)
                {
                    string plateBarcode = aliquot.Plate["EXTERNAL_REFERENCE"].ToString();
                    if (!plates.ContainsKey(plateBarcode))
                    {
                        plates.Add(plateBarcode, new Triple() { Plate = aliquot.Plate, AliquotCount = 1, Project = project });
                    }
                    else
                    {
                        plates[plateBarcode].AliquotCount++;
                    }
                }
            }
            foreach (string plateBarcode in plates.Keys)
            {
                _plateDataGridView.Rows.Add(new object[] { plateBarcode, plates[plateBarcode].AliquotCount, plates[plateBarcode].Project });
            }
            if(projects.Count() > 1)
            {
                SelectProjectForm form = new SelectProjectForm(projects);
                if(form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    foreach (DataGridViewRow row in _plateDataGridView.Rows)
                    {
                        row.Cells[2].Value = form.SelectedProject;
                    }
                    foreach (DataGridViewRow row in _sampleDataGridView.Rows)
                    {
                        row.Cells[1].Value = form.SelectedProject;
                    }
                }
            }
            _sampleCountTextBox.Text = _data.Count().ToString();
            UpdateUI(false);
        }

        /// <summary>
        /// Show report dialog
        /// </summary>
        private void _reportButton_Click(object sender, EventArgs e)
        {
            ShipmentExtension.Log("Report clicked");
            ReportForm reportForm = new ReportForm(_limsCommunication, _data);
            reportForm.ShowDialog();
        }

        /// <summary>
        /// Update UI when the user changes the comment.
        /// </summary>
        private void _descriptionTextBox_TextChanged(object sender, EventArgs e)
        {
            UpdateUI(false);
        }

        /// <summary>
        /// Updates state of the different UI elements
        /// </summary>
        private void UpdateUI(bool active)
        {
            if (active)
            {
                _updateButton.Enabled = false;
                _reportButton.Enabled = false;
                _abortButton.Enabled = false;
            }
            else
            {
                _stateLabel.Text = "Klar";
                _updateButton.Enabled = _sampleDataGridView.RowCount > 0 ? true : false;
                if (_descriptionTextBox.Text == "")
                {
                    _updateButton.Enabled = false;
                    _stateLabel.Text = "Beskrivelse er ikke udfyldt";
                }
                string project = null;
                foreach (DataGridViewRow row in _sampleDataGridView.Rows)
                {
                    if (project == null)
                    {
                        project = row.Cells[1].Value.ToString();
                    }
                    if (project != row.Cells[1].Value.ToString())
                    {
                        _updateButton.Enabled = false;
                        _stateLabel.Text = "Prøverne tilhører forskellige projekter";
                    }
                }
                _reportButton.Enabled = true;
                _abortButton.Enabled = true;
            }
        }

        /// <summary>
        /// Do the update in Nautilus
        /// </summary>
        private void _updateButton_Click(object sender, EventArgs e)
        {
            UpdateUI(true);
            try
            {
                ShipmentExtension.Log("Update clicked");
                int index = 0;
                List<string> disposedPlates = new List<string>();
                foreach (Thermo.Nautilus.Extensions.Model.Aliquot aliquot in _data)
                {
                    index++;
                    _stateLabel.Text = "Behandler prøve " + index + " af " + _data.Count();
                    ShipmentExtension.Log("Aliquot id = " + aliquot.Id.ToString());
                    if(_plateMode)
                    {
                        string plateBarcode = "";
                        try
                        {
                            plateBarcode = aliquot.Plate["EXTERNAL_REFERENCE"].ToString();
                        }
                        catch {/*Ignore*/}
                        if (plateBarcode != "" && !disposedPlates.Contains(plateBarcode))
                        {
                            ShipmentExtension.Log("Dispose plade: " + plateBarcode);
                            _limsCommunication.PlateDispose(plateBarcode, _operatorId, DateTime.Now.ToString("yyyy/MM/dd hh/mm/ss"), "Disposed by Shipment extension", false);
                            disposedPlates.Add(plateBarcode);
                        }
                    }
                    else
                    {
                        ShipmentExtension.Log("Dispose prøve");
                        _limsCommunication.SampleDispose(aliquot.Id.ToString(), _operatorId, DateTime.Now.ToString("yyyy/MM/dd hh/mm/ss"), "Disposed by Shipment extension", true);
                    }
                    ShipmentExtension.Log("Set Condition og U_Shipment");
                    _limsCommunication.SampleSetField(aliquot.Id.ToString(), "Condition", "STC");
                    _limsCommunication.SampleUserSetField(aliquot.Id.ToString(), "U_SHIPMENT", _dateTimePicker.Value.ToString("yyyy/MM/dd") + "/" + _descriptionTextBox.Text);
                    if (aliquot["U_PROJECTNR"].ToString() != _sampleDataGridView.Rows[0].Cells[1].Value.ToString())
                    {
                        ShipmentExtension.Log("Opdater U_ProjectNr");
                        _limsCommunication.SampleUserSetField(aliquot.Id.ToString(), "U_PROJECTNR", _sampleDataGridView.Rows[0].Cells[1].Value.ToString());
                    }
                }
                _stateLabel.Text = "Opdaterer ordre: " + _sampleDataGridView.Rows[0].Cells[1].Value.ToString();
                ShipmentExtension.Log("Afslut ordre: " + _sampleDataGridView.Rows[0].Cells[1].Value.ToString());
                _limsCommunication.DnbOrderSetState(_sampleDataGridView.Rows[0].Cells[1].Value.ToString(), "6", _dateTimePicker.Value);
            }
            catch(Exception ex)
            {
                ShipmentExtension.Log("Kunne ikke sætte ordrens status: " + ex.Message);
                MessageBox.Show("Kunne ikke sætte ordrens status: " + ex.Message);
            }
            UpdateUI(false);
        }
    }
}
