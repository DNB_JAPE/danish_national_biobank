﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shipment
{
    /// <summary>
    /// Helper class for keeping data.
    /// </summary>
    internal class Triple
    {
        internal Thermo.Nautilus.Extensions.Model.Plate Plate { get; set; }
        internal int AliquotCount { get; set; }
        internal string Project { get; set; }
    }
}
