﻿using DNBTools;
using DNBTools.DataClasses;
using DNBTools.Nautilus;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace BSGExtension
{
    public partial class BSGExtensionForm : Form
    {
        private Sample _parent;
        private Operator _operator;
        private LimsCommunication _limsCommunication;
        private Configuration _configuration;

        public BSGExtensionForm(string sampleId, string operatorId, LimsCommunication limsCommunication, Configuration configuration)
        {
            InitializeComponent();
            _limsCommunication = limsCommunication;
            _configuration = configuration;
            _parent = _limsCommunication.SampleGet(sampleId);
            _operator = limsCommunication.OperatorGet(operatorId);
        }

        private void BSGExtensionForm_Load(object sender, EventArgs e)
        {
            _sampleNUD.Enabled = false;
            _sampleTextBox.Text = _parent.Name;
            _operatorTextBox.Text = _operator.LogicalId;
            _plateTextBox.Select();
            // Default values
            cmbType.Text = "Plasma";
            cmbVolumen.Text = "1000";
        }

        private void UpdateUI()
        {
            using (new WaitCursor())
            {
                _dataGridView.Rows.Clear();
                if (_limsCommunication.PlateExist(_plateTextBox.Text))
                {
                    _sampleNUD.Enabled = true;
                    List<string> aliquotIds = _limsCommunication.SampleGetIdsFromPlate(_plateTextBox.Text);
                    List<Sample> samples = _limsCommunication.SamplesGet(aliquotIds, "Plate_Order");
                    foreach (Sample sample in samples)
                    {
                        _dataGridView.Rows.Add(new object[] { false, sample.PositionAsString(), sample.Amount, sample.LogicalId });
                        _dataGridView.Rows[_dataGridView.Rows.Count - 1].Tag = sample;
                    }
                }
                else
                {
                    _sampleNUD.Enabled = false;
                }
            }
        }

        private void _plateTextBox_TextChanged(object sender, EventArgs e)
        {
            _sampleNUD.Value = 0;
            UpdateUI();
        }

        private void _sampleNUD_ValueChanged(object sender, EventArgs e)
        {
            UpdateUI();
            int count = 0;
            foreach (DataGridViewRow row in _dataGridView.Rows)
            {
                Sample sample = row.Tag as Sample;
                if (sample.Amount == 0 && count < _sampleNUD.Value)
                {
                    row.Cells[0].Value = true;
                    count++;
                }
            }
            if(count < _sampleNUD.Value)
            {
                MessageBox.Show("Der er kun " + count + " tomme rør på pladen!");
            }
        }

        private void _createButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (new WaitCursor())
                {
                    _createButton.Enabled = false;
                    _deleteButton.Enabled = false;
                    _closeButton.Enabled = false;
                    if (!_limsCommunication.PlateExist(_plateTextBox.Text))
                    {
                        MessageBox.Show("Kunne ikke finde pladen: " + _plateTextBox.Text);
                        _createButton.Enabled = true;
                        _deleteButton.Enabled = true;
                        _closeButton.Enabled = true;
                        return;
                    }
                    List<Sample> samplesToCreate = new List<Sample>();
                    foreach (DataGridViewRow row in _dataGridView.Rows)
                    {
                        bool selected = Convert.ToBoolean(row.Cells[0].Value);
                        Sample sample = row.Tag as Sample;
                        if (selected && sample.Amount > 0)
                        {
                            MessageBox.Show("Prøven på position " + row.Cells[1].Value + " er ikke tom!");
                            _createButton.Enabled = true;
                            _deleteButton.Enabled = true;
                            _closeButton.Enabled = true;
                            return;
                        }
                        else if (selected)
                        {
                            samplesToCreate.Add(sample);
                        }
                    }
                    // ESAT 2019-03-12
                    if (cmbVolumen.Text == string.Empty)
                        cmbVolumen.Text = "1000";   // Default value
                    if (cmbType.Text.Trim() == string.Empty)
                        cmbType.Text = "Plasma";    // Default value

                    long newVolumen = Convert.ToInt64(cmbVolumen.Text);
                    string newType = cmbType.Text.Trim();
                        //
                    foreach (Sample sample in samplesToCreate)
                    {
                        //// Her skal volumen sættes efter størrelse på glasset
                        //_limsCommunication.SampleSetVolumeToMax(sample.LimsId);
                        // ESAT 2019-03-12
                        _limsCommunication.SampleSetVolume(sample.LimsId, newVolumen);
                        _limsCommunication.SampleSetField(sample.LimsId, "MATRIX_TYPE", newType);
                        // 
                        _limsCommunication.SampleSetBsgId(sample.LimsId, _parent.BsgId);
                        _limsCommunication.SampleSetReceivedOn(sample.LimsId, DateTime.Now);
                        _limsCommunication.SampleSetOperator(sample.LimsId, _operator.LimsId);
                        _limsCommunication.SampleSetSampleId(sample.LimsId, _parent.SampleId);
                        _limsCommunication.SampleCreateFormulation(_parent.LimsId, sample.LimsId);
                        _changeListBox.Items.Add("Oprettet: " + sample.Name + " i " + sample.PositionAsString() + " på " + _plateTextBox.Text);
                    }
                    _createButton.Enabled = true;
                    _deleteButton.Enabled = true;
                    _closeButton.Enabled = true;
                    MessageBox.Show(samplesToCreate.Count + " prøver blev oprettet");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Fejl: " + ex.Message);
            }
            UpdateUI();
        }

        private void _deleteButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (new WaitCursor())
                {
                    _createButton.Enabled = false;
                    _deleteButton.Enabled = false;
                    _closeButton.Enabled = false;
                    if (!_limsCommunication.PlateExist(_plateTextBox.Text))
                    {
                        MessageBox.Show("Kunne ikke finde pladen: " + _plateTextBox.Text);
                        _createButton.Enabled = true;
                        _deleteButton.Enabled = true;
                        _closeButton.Enabled = true;
                        return;
                    }
                    List<Sample> samplesToDelete = new List<Sample>();
                    foreach (DataGridViewRow row in _dataGridView.Rows)
                    {
                        bool selected = Convert.ToBoolean(row.Cells[0].Value);
                        Sample sample = row.Tag as Sample;
                        if (selected && sample.Amount == 0)
                        {
                            MessageBox.Show("Prøven på position " + row.Cells[1].Value + " er tom!");
                            _createButton.Enabled = true;
                            _deleteButton.Enabled = true;
                            _closeButton.Enabled = true;
                            return;
                        }
                        else if (selected)
                        {
                            samplesToDelete.Add(sample);
                        }
                    }
                    foreach (Sample sample in samplesToDelete)
                    {
                        _limsCommunication.SampleSetVolume(sample.LimsId, 0);
                        _limsCommunication.SampleSetBsgId(sample.LimsId, "");
                        _limsCommunication.SampleSetSampleId(sample.LimsId, "");
                        _limsCommunication.SampleSetReceivedOn(sample.LimsId, DateTime.MinValue);
                        _limsCommunication.SampleSetOperator(sample.LimsId, "");
                        _limsCommunication.SampleDeleteFormulation(_parent.LimsId, sample.LimsId);
                        _changeListBox.Items.Add("Slettet: " + sample.Name + " i " + sample.PositionAsString() + " på " + _plateTextBox.Text);
                    }
                    _createButton.Enabled = true;
                    _deleteButton.Enabled = true;
                    _closeButton.Enabled = true;
                    MessageBox.Show(samplesToDelete.Count + " prøver blev slettet");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Fejl: " + ex.Message);
            }
            UpdateUI();
        }

        private void _closeButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
