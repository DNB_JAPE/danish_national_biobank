@ECHO OFF
REM =============================================================================
REM Registration file for .NET assembly (Nautilus extension)
REM =============================================================================
REM
ECHO ****************************************************************************
ECHO ****************************************************************************
ECHO ******* This batch file will register your Nautilus Extension. ********
ECHO ******* You may see a RegAsm warning -- this message can be ignored ********
ECHO ****************************************************************************
ECHO ****************************************************************************
ECHO.
ECHO.
ECHO.
C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319\regasm.exe "C:\Program Files (x86)\Thermo\Nautilus\Extensions\BSG\BSGExtension.dll" /codebase
ECHO.
ECHO.
ECHO ****************************************************************************
ECHO ****************************************************************************
ECHO ******* Make sure that your assembly was REGISTERED SUCCESSFULLY ***********
ECHO ********************** Before closing this window! *************************
ECHO ****************************************************************************
ECHO ****************************************************************************
PAUSE