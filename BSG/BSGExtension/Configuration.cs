﻿using DNBTools.Nautilus;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace BSGExtension
{
    public class Configuration
    {
        public string NautConStr { get; set; }

        public Configuration()
        {
            string location = Path.Combine(Application.StartupPath, "DNB_Extensions", "BSG");
            UriBuilder uri = new UriBuilder(location);
            string path = Uri.UnescapeDataString(uri.Path);
            XmlDocument configurationDoc = new XmlDocument();
            configurationDoc.Load(Path.Combine(path, "Configuration.xml"));
            if (NautilusTools.IsProduction())
            {
                NautConStr = configurationDoc.SelectSingleNode("/Configuration/NautConStrProd").InnerXml;
            }
            else 
            {
                NautConStr = configurationDoc.SelectSingleNode("/Configuration/NautConStrTest").InnerXml;
            }
        }
    }
}
