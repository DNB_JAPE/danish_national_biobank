﻿using DNBTools;
using DNBTools.Nautilus;
using LSEXT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Thermo.Nautilus.Extensions.UserInterface;

namespace BSGExtension
{
    public class BSGExtension : EntityExtension, IVersion
    {
        public int GetVersion()
        {
            return 3;
        }

        /// <summary>
        /// Called by Lims to decide if the extension should be listed in the right click menu.
        /// </summary>
        /// <param name="Parameters">Lims parameters (not used).</param>
        /// <returns>ExecuteExtension.exEnabled - Always allow this extension to start.</returns>
        public override EntityExtension.enumExtensionVisibility CanExecuteExtension()
        {
            return EntityExtension.enumExtensionVisibility.Enabled;
        }

        /// <summary>
        /// Called by Lims to activate the extension.
        /// </summary>
        public override void ExecuteExtension()
        {
            // Read configuration
            Configuration configuration;
            try
            {
                configuration = new Configuration();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Kunne ikke indlæse konfigurationen: " + ex.Message);
                return;
            }
            // Get Aliquot information
            string aliquotId;
            string operatorId;
            try
            {
                ADODB.Recordset records = (ADODB.Recordset)Parameters["RECORDS"];
                if(records.Fields.Count > 1)
                {
                    MessageBox.Show("Mere end en prøve er valgt");
                    return;
                }
                aliquotId = records.Fields[0].Value.ToString();
                operatorId = Parameters["OPERATOR_ID"].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Kunne ikke læse data fra Nautilus: " + ex.Message);
                return;
            }
            // Create LimsCommunication
            LimsCommunication limsCommunication = null;
            try
            {
                limsCommunication = new LimsCommunication(configuration.NautConStr);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Kunne ikke Oprette forbindelse til Nautilus DB: " + ex.Message);
                return;
            }
            // Open extension form
            BSGExtensionForm extensionForm = new BSGExtensionForm(aliquotId, operatorId, limsCommunication, configuration);
            extensionForm.ShowDialog();
            limsCommunication.Dispose();
        }
    }
}
