﻿namespace BSGExtension
{
    partial class BSGExtensionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this._sampleTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this._operatorTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._plateTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this._sampleNUD = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this._createButton = new System.Windows.Forms.Button();
            this._changeListBox = new System.Windows.Forms.ListBox();
            this._closeButton = new System.Windows.Forms.Button();
            this._dataGridView = new System.Windows.Forms.DataGridView();
            this._selectColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._positionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._volumeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._nameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._deleteButton = new System.Windows.Forms.Button();
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.cmbVolumen = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this._sampleNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Prøve:";
            // 
            // _sampleTextBox
            // 
            this._sampleTextBox.Location = new System.Drawing.Point(15, 25);
            this._sampleTextBox.Name = "_sampleTextBox";
            this._sampleTextBox.ReadOnly = true;
            this._sampleTextBox.Size = new System.Drawing.Size(154, 20);
            this._sampleTextBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(172, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Operatør:";
            // 
            // _operatorTextBox
            // 
            this._operatorTextBox.Location = new System.Drawing.Point(175, 25);
            this._operatorTextBox.Name = "_operatorTextBox";
            this._operatorTextBox.ReadOnly = true;
            this._operatorTextBox.Size = new System.Drawing.Size(100, 20);
            this._operatorTextBox.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(278, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Plade:";
            // 
            // _plateTextBox
            // 
            this._plateTextBox.Location = new System.Drawing.Point(281, 25);
            this._plateTextBox.Name = "_plateTextBox";
            this._plateTextBox.Size = new System.Drawing.Size(100, 20);
            this._plateTextBox.TabIndex = 5;
            this._plateTextBox.TextChanged += new System.EventHandler(this._plateTextBox_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Foreslå nye prøver:";
            // 
            // _sampleNUD
            // 
            this._sampleNUD.Location = new System.Drawing.Point(121, 52);
            this._sampleNUD.Maximum = new decimal(new int[] {
            96,
            0,
            0,
            0});
            this._sampleNUD.Name = "_sampleNUD";
            this._sampleNUD.Size = new System.Drawing.Size(54, 20);
            this._sampleNUD.TabIndex = 7;
            this._sampleNUD.ValueChanged += new System.EventHandler(this._sampleNUD_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 450);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Ændringer foretaget:";
            // 
            // _createButton
            // 
            this._createButton.Location = new System.Drawing.Point(15, 424);
            this._createButton.Name = "_createButton";
            this._createButton.Size = new System.Drawing.Size(77, 23);
            this._createButton.TabIndex = 9;
            this._createButton.Text = "Opret";
            this._createButton.UseVisualStyleBackColor = true;
            this._createButton.Click += new System.EventHandler(this._createButton_Click);
            // 
            // _changeListBox
            // 
            this._changeListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._changeListBox.FormattingEnabled = true;
            this._changeListBox.Location = new System.Drawing.Point(15, 466);
            this._changeListBox.Name = "_changeListBox";
            this._changeListBox.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this._changeListBox.Size = new System.Drawing.Size(359, 173);
            this._changeListBox.TabIndex = 10;
            // 
            // _closeButton
            // 
            this._closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._closeButton.Location = new System.Drawing.Point(306, 650);
            this._closeButton.Name = "_closeButton";
            this._closeButton.Size = new System.Drawing.Size(75, 23);
            this._closeButton.TabIndex = 11;
            this._closeButton.Text = "Luk";
            this._closeButton.UseVisualStyleBackColor = true;
            this._closeButton.Click += new System.EventHandler(this._closeButton_Click);
            // 
            // _dataGridView
            // 
            this._dataGridView.AllowUserToAddRows = false;
            this._dataGridView.AllowUserToDeleteRows = false;
            this._dataGridView.AllowUserToResizeColumns = false;
            this._dataGridView.AllowUserToResizeRows = false;
            this._dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this._dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._selectColumn,
            this._positionColumn,
            this._volumeColumn,
            this._nameColumn});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._dataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this._dataGridView.Location = new System.Drawing.Point(15, 78);
            this._dataGridView.Name = "_dataGridView";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this._dataGridView.RowHeadersVisible = false;
            this._dataGridView.Size = new System.Drawing.Size(366, 312);
            this._dataGridView.TabIndex = 12;
            // 
            // _selectColumn
            // 
            this._selectColumn.HeaderText = "Valg";
            this._selectColumn.Name = "_selectColumn";
            this._selectColumn.Width = 30;
            // 
            // _positionColumn
            // 
            this._positionColumn.HeaderText = "Position";
            this._positionColumn.Name = "_positionColumn";
            this._positionColumn.ReadOnly = true;
            this._positionColumn.Width = 50;
            // 
            // _volumeColumn
            // 
            this._volumeColumn.HeaderText = "Volumen";
            this._volumeColumn.Name = "_volumeColumn";
            this._volumeColumn.ReadOnly = true;
            this._volumeColumn.Width = 50;
            // 
            // _nameColumn
            // 
            this._nameColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._nameColumn.HeaderText = "Stregkode";
            this._nameColumn.Name = "_nameColumn";
            this._nameColumn.ReadOnly = true;
            // 
            // _deleteButton
            // 
            this._deleteButton.Location = new System.Drawing.Point(98, 424);
            this._deleteButton.Name = "_deleteButton";
            this._deleteButton.Size = new System.Drawing.Size(75, 23);
            this._deleteButton.TabIndex = 13;
            this._deleteButton.Text = "Slet";
            this._deleteButton.UseVisualStyleBackColor = true;
            this._deleteButton.Click += new System.EventHandler(this._deleteButton_Click);
            // 
            // cmbType
            // 
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Items.AddRange(new object[] {
            "Plasma",
            "PBMC",
            "Buffy"});
            this.cmbType.Location = new System.Drawing.Point(15, 397);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(191, 21);
            this.cmbType.TabIndex = 14;
            // 
            // cmbVolumen
            // 
            this.cmbVolumen.FormattingEnabled = true;
            this.cmbVolumen.Items.AddRange(new object[] {
            "1000",
            "700",
            "500"});
            this.cmbVolumen.Location = new System.Drawing.Point(253, 397);
            this.cmbVolumen.Name = "cmbVolumen";
            this.cmbVolumen.Size = new System.Drawing.Size(121, 21);
            this.cmbVolumen.TabIndex = 15;
            // 
            // BSGExtensionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(393, 684);
            this.Controls.Add(this.cmbVolumen);
            this.Controls.Add(this.cmbType);
            this.Controls.Add(this._deleteButton);
            this.Controls.Add(this._dataGridView);
            this.Controls.Add(this._closeButton);
            this.Controls.Add(this._changeListBox);
            this.Controls.Add(this._createButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this._sampleNUD);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._plateTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._operatorTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._sampleTextBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BSGExtensionForm";
            this.Text = "BSG";
            this.Load += new System.EventHandler(this.BSGExtensionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._sampleNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _sampleTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _operatorTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox _plateTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown _sampleNUD;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button _createButton;
        private System.Windows.Forms.ListBox _changeListBox;
        private System.Windows.Forms.Button _closeButton;
        private System.Windows.Forms.DataGridView _dataGridView;
        private System.Windows.Forms.Button _deleteButton;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _selectColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _positionColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _volumeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _nameColumn;
        private System.Windows.Forms.ComboBox cmbType;
        private System.Windows.Forms.ComboBox cmbVolumen;
    }
}