﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using System.IO;

namespace ExcelTools
{
    public static class ExcelTools
    {
        public static bool isExcelInstalled = Type.GetTypeFromProgID("Excel.Application") != null ? true : false;

        public static ExcelPackage CreateWorkBook(string WorkSheet1, string WorkSheet2,string WorkSheet3,string Path)
        {
            ExcelPackage l_Reply = new ExcelPackage();
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Worksheets.Add(WorkSheet1);
                excel.Workbook.Worksheets.Add(WorkSheet2);
                excel.Workbook.Worksheets.Add(WorkSheet3);

                FileInfo excelFile = new FileInfo(Path);
                excel.SaveAs(excelFile);
                l_Reply = excel;
            }
            return l_Reply;
        }

        public static ExcelPackage CreateWorkBook(List<string> WorkSheetNames, string Path)
        {
            ExcelPackage l_Reply = new ExcelPackage();
            foreach (var wsn in WorkSheetNames)
            {
                l_Reply.Workbook.Worksheets.Add(wsn);
            }

            FileInfo excelFile = new FileInfo(Path);
            l_Reply.SaveAs(excelFile);

            //using (ExcelPackage excel = new ExcelPackage())
            //{
            //    foreach (var wsn in WorkSheetNames)
            //    {
            //        excel.Workbook.Worksheets.Add(wsn);
            //    }

            //    FileInfo excelFile = new FileInfo(Path);
            //    excel.SaveAs(excelFile);
            //    l_Reply = excel;
            //}
            return l_Reply;
        }
        public static bool AddRow(List<string[]> headerRow,ExcelWorksheet excelWorksheet  )
        {
            bool l_Reply = false;
            try
            {
                string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                excelWorksheet.Cells[headerRange].LoadFromArrays(headerRow);
                l_Reply = true;
            }
            catch (Exception ex)
            {

                throw;
            }
            return l_Reply;
        }

        public static bool AddDataToCell(ExcelWorksheet worksheet, string cell, string value)
        {
            bool l_Reply = false;
            try
            {
                worksheet.Cells[cell].Value = value;
            }
            catch (Exception ex)
            {

                throw;
            }
            return l_Reply;
        }

        public static bool AddDataToMultipleRows(ExcelWorksheet worksheet, int x, int y, List<object[]> cellData)
        {
            bool l_Reply = false;
            try
            {
                worksheet.Cells[x, y].LoadFromArrays(cellData);
                l_Reply = true;
            }
            catch (Exception ex)
            {

                throw;
            }
            return l_Reply;

        }

        public static void OpenExcel(string path)
        {
            //FileInfo excelFile = new FileInfo(@"C:\Users\amir\Desktop\test.xlsx");
            FileInfo excelFile = new FileInfo(path);

            if (isExcelInstalled)
            {
                System.Diagnostics.Process.Start(excelFile.ToString());
            }

        }
    }
}
