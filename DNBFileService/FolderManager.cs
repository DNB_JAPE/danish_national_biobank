﻿using DNBTools;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace ExcelTools
{
    public class FolderManager
    {
        public List<FolderControl> WatchList;
        private FolderConfigSettings folderConfigSettings;
        private string _ConfigFileName =  "config.xml";
        private int _DebugLevel = 3;

        public event WatchHandler WatchEvent;
        public delegate void WatchHandler(FolderManager fm, FileSystemEventArgs e);

        public FolderManager()
        {
            //EventLog.WriteEntry("FolerManager is starting", EventLogEntryType.Information, 1000);
            WatchList = new List<FolderControl>();
            ReadSetup();
            //SysLog.EnableLog(folderConfigSettings.SyslogIP, folderConfigSettings.SyslogName, true, 100);
        }

        #region Properties

        public int DebugLevel
        {
            get { return _DebugLevel; }
            set { _DebugLevel = value; }
        }

        #endregion Properties

        #region Public Functions

        public void StartSync()
        {
            try
            {
                if (_DebugLevel > 2)
                    SysLog.Log("Manuel start of syncronizing", LogLevel.Debug);
                foreach (var i in WatchList)
                {
                    Sync(i);
                }
            }
            catch (Exception ex)
            {
                SysLog.Log("Error when starting sync manually: " + ex.Message, LogLevel.Error);
            }
        }

        private void DeleteFilesIfTooOld()
        {

        }


        //public List<FileInfo> AddFolderToWatchList(string SourcePath, string DestinationPath, string Filter, bool SyncEnabled, DateTime LastSync)
        //{
        //    List<FileInfo> l_Reply = new List<FileInfo>();
        //    try
        //    {
        //        SysLog.Log("Adding folder to WatchList", LogLevel.Info);
        //        if (WatchList == null) WatchList = new List<FolderControl>();

        //        FolderControl fc = new FolderControl(SourcePath, DestinationPath, Filter, SyncEnabled, LastSync);
        //        if (!IsFolderInWatchListThenUpdateIt(fc))
        //        {
        //            fc.InitManager();
        //            WatchList.Add(fc);
        //            folderConfigSettings.Entries.Add(new FolderConfig(SourcePath, DestinationPath, SyncEnabled, LastSync, Filter));
        //            FolderConfigSettings.Serialize(_ConfigFileName, folderConfigSettings);
        //        }
        //        l_Reply = fc.SourceFileList;
        //        fc.SyncFolders();
        //    }
        //    catch (Exception ex)
        //    {
        //        SysLog.Log("Error when adding folder to WatchList: " + ex.Message, LogLevel.Error);
        //    }
        //    return l_Reply;
        //}

        #endregion Public Functions

        #region Internal Functions
        private void ReadSetup()
        {
            try
            {
                string localPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                var cfg = File.Open(string.Format("{0}\\{1}", localPath, _ConfigFileName), FileMode.Open);
                if (cfg != null)
                {
                    cfg.Close();
                    folderConfigSettings = FolderConfigSettings.Deserialize(string.Format("{0}\\{1}", localPath, _ConfigFileName));// new FolderConfigSettings();
                                                                                                                                   //folderConfigSettings.Reload();
                    foreach (FolderConfig i in folderConfigSettings.Entries)
                    {
                        //FolderControl fldC = new FolderControl(i.SourcePath, i.DestinationPath, i.Filter, i.SyncEnabled, i.LastSyncTime);
                        FolderControl fldC = new FolderControl(i);
                        fldC.WatchEvent += FldC_WatchEvent;
                        fldC.MaxFileAgeInDays = folderConfigSettings.MaxFileAgeInDays;
                        fldC.InitManager();
                        WatchList.Add(fldC);
                    }
                }
                else
                {
                    // Make default settings file.
                    folderConfigSettings = new FolderConfigSettings();
                    folderConfigSettings.SyslogIP = "127.0.0.1";
                    folderConfigSettings.SyslogName = "JohnDoe";    // So we can see that it is not initialized...
                    folderConfigSettings.Entries = new List<FolderConfig>();
                    folderConfigSettings.MaxFileAgeInDays = 365;
                    FolderConfigSettings.Serialize(string.Format("{0}\\{1}", localPath, _ConfigFileName), folderConfigSettings);
                }
            }
            catch (Exception e)
            {
                SysLog.Log("Read Setup error: " + e.Message, LogLevel.Error);
            }
        }

        private void Sync(FolderControl folderConfig)
        {
            try
            {
                FolderControl t = WatchList.Find(f => f.FolderConfiguration.SourcePath == folderConfig.FolderConfiguration.SourcePath);

                if (t != null)
                {
                    t.SyncFolders();

                    SysLog.Log(string.Format("Updating folder watch [{0}] ", folderConfig.FolderConfiguration.ToString()), LogLevel.Info);
                    foreach (var i in folderConfigSettings.Entries.Where(x => x.SourcePath == folderConfig.FolderConfiguration.SourcePath))
                    {
                        i.DestinationPath = folderConfig.FolderConfiguration.DestinationPath;
                        i.Filter = folderConfig.FolderConfiguration.Filter;
                        i.LastSyncTime = folderConfig.FolderConfiguration.LastSyncTime;
                        i.SourcePath = folderConfig.FolderConfiguration.SourcePath;
                        i.SyncEnabled = folderConfig.FolderConfiguration.SyncEnabled;
                    }
                    FolderConfigSettings.Serialize(_ConfigFileName, folderConfigSettings);
                }
                else
                {
                    SysLog.Log(string.Format("Couldn't find Folder watch [{0}] ", folderConfig.FolderConfiguration.ToString()), LogLevel.Warning);
                }
            }
            catch (Exception ex)
            {
                SysLog.Log("Sync Error: " + ex.Message, LogLevel.Error);
            }
        }

        private void FldC_WatchEvent(FolderControl fc, FileSystemEventArgs e)
        {
            try
            {
                if (_DebugLevel > 2)
                    SysLog.Log("Foldermanager got event", LogLevel.Debug);

                if (WatchEvent != null)
                    WatchEvent.Invoke(this, e);
                if (fc != null)
                {
                    if (fc.FolderConfiguration != null)
                    {
                        Sync(fc);
                        fc.FolderConfiguration.LastSyncTime = DateTime.Now;
                    }
                    else
                        SysLog.Log("Missing foldercontrol.config??", LogLevel.Error);
                }
                else
                    SysLog.Log("Missing foldercontrol??", LogLevel.Error);
            }
            catch (Exception ex)
            {
                SysLog.Log("FM Event Error: " + ex.Message, LogLevel.Error);
            }
        }

        #endregion Internal Functions
    }
}
