﻿using System;
using System.Windows.Forms;

namespace DNBFileService
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void testsToolStripMenuItem_Click(object sender, EventArgs e)
		{

		}

		private void helpToolStripMenuItem_Click(object sender, EventArgs e)
		{

		}

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            // For later...
            ContextMenu cm = new ContextMenu();
            cm.MenuItems.Add("Change path");
            cm.MenuItems.Add("Enable/Disable");

            lstConfig.ContextMenu = cm;
        }
    }
}
