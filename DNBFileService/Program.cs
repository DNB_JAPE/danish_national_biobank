﻿using System;
using System.IO;
using System.ServiceProcess;
using System.Threading;
using System.Windows.Forms;
using System.ServiceModel;
using ExcelTools;
using System.Diagnostics;
using DNBTools;
using System.Xml;

namespace DNBFileService
{
    static public class Program
    {
        static bool serviceWasRunning = false;
        static MainForm mainForm;
        static Thread fileThread;
        static FolderManager _folderManager;

        static string SysLogIP = "127.0.0.1";
        static bool SysLogEnabled = true;
        static string SysLogName = "JohnDoe";
        static int DebugLevel = 3;
        static bool _runWithGUI = false;

        /// (summary)
        /// The main entry point for the application.
        /// STAThread for the benefit of the GUI; service will ignore it.
        /// (/summary)
        [STAThread]
        static void Main(string[] args)
        {
            ReadSetup();
            SysLog.EnableLog(SysLogIP, SysLogName, true, 100);
            // Attach lstLog view to log event
            SysLog.LogEvent += SysLog_LogEvent;
            if (args.Length > 0)
            {
                if (args[0].ToUpper() == "/GUI")
                {
                    SysLog.Log("Started FileSync as an app", LogLevel.Info);
                    DisableServiceIfRunning();
                    _runWithGUI = true;
                    OnStart(args);
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    mainForm = new MainForm();
                    Application.Run(mainForm);
                    OnStop();
                    EnableServiceIfWasRunning();
                }
            }
            else
            {
                SysLog.Log("Started FileSync as a service", LogLevel.Info);
                _runWithGUI = false;
                ServiceBase.Run(new DNBFileService());
            }
        }

        private static void SysLog_LogEvent(LogEvents e)
        {
            AddToLstLog(e.LogMessage);
        }

        /// (summary)
        /// This method receives control when either the service or the GUI starts.
        /// (/summary)
        /// (param name="args")(/param)
        static internal void OnStart(string[] args)
        {
            fileThread = new Thread(() => fileWatcherThread());
            fileThread.IsBackground = true;
            fileThread.Start();
        }

        /// (summary)
        /// Shut down the filemanager thread.
        /// (/summary)
        static internal void OnStop()
        {
            SysLog.Log("Service stopped", LogLevel.Info);
            fileThread.Abort();
        }

        static private void fileWatcherThread()
        {
            try
            {
                _folderManager = new FolderManager();
                _folderManager.DebugLevel = DebugLevel;
                UpdateLstConfig();
                _folderManager.WatchEvent += _folderManager_WatchEvent;

                // Since the service is started, the monitored 
                // folders should be syncronized right away
                _folderManager.StartSync();
                while (true)
                {
                    if (DebugLevel > 2)
                        Debug.Print("Still alive :-)");
                    Thread.Sleep(500);
                }
            }
            catch (ThreadAbortException)
            {
                if (DebugLevel > 2)
                    SysLog.Log("FilewatcherThread aborted", LogLevel.Debug);
            }
            catch (Exception ex)
            {
                SysLog.Log("FilewatcherThread error: " + ex.Message, LogLevel.Error);
            }
        }
        private static void UpdateLstConfig()
        {
            try
            {
                if (_runWithGUI)
                {
                    MethodInvoker action = () => { mainForm.lstConfig.Items.Clear(); };
                    mainForm.BeginInvoke(action);

                    foreach (var x in _folderManager.WatchList)
                    {
                        AddToLstConfig(x.FolderConfiguration.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                SysLog.Log("UpdateLstConfig error: " + ex.Message, LogLevel.Error);
            }
        }

        private static void AddToLstConfig(string addMe)
        {
            if (_runWithGUI)
            {
                MethodInvoker action = () =>
              {
                  if (mainForm.lstConfig.Items.Count > 1000)
                      mainForm.lstConfig.Items.RemoveAt(0);

                  mainForm.lstConfig.Items.Add(addMe);
                  mainForm.lstConfig.TopIndex = mainForm.lstConfig.Items.Count - 1;

              };
                mainForm.BeginInvoke(action);
            }
        }

        /// <summary>
        /// Actually update log listbox
        /// </summary>
        /// <param name="addMe"></param>
        private static void AddToLstLog(string addMe)
        {
            if (_runWithGUI)
            {
                MethodInvoker action = () =>
                {
                    if (mainForm.lstLog.Items.Count > 1000)
                        mainForm.lstLog.Items.RemoveAt(0);
                    mainForm.lstLog.Items.Add(addMe);
                    mainForm.lstLog.TopIndex = mainForm.lstLog.Items.Count - 1;
                };
                mainForm.BeginInvoke(action);
            }
        }

        /// <summary>
        /// Update log listbox
        /// </summary>
        /// <param name="fm"></param>
        /// <param name="e"></param>
        private static void _folderManager_WatchEvent(FolderManager fm, FileSystemEventArgs e)
        {
            try
            {
                AddToLstLog(string.Format("File [{0}] in [{1}] has been {2}", e.Name, e.FullPath, e.ChangeType));
                if (_runWithGUI)
                    UpdateLstConfig();
            }
            catch (Exception ex)
            {
                SysLog.Log("Error in program event: " + ex.Message, LogLevel.Error);
            }
        }

        /// (summary)
        /// If the service is running, stop it.
        /// (/summary)
        private static void DisableServiceIfRunning()
        {
            try
            {
                //
                // ServiceController will throw System.InvalidOperationException if service not found.
                //
                ServiceController sc = new ServiceController(name: "DNBFileService");
                sc.Stop();
                serviceWasRunning = true;
                Thread.Sleep(1000);  // wait for service to stop
            }
            catch (Exception)
            {
            }
        }

        /// (summary)
        /// If the service was running, start it up again.
        /// (/summary)
        private static void EnableServiceIfWasRunning()
        {
            if (serviceWasRunning)
            {
                try
                {
                    //
                    // ServiceController will throw System.InvalidOperationException if service not found.
                    //
                    ServiceController sc = new ServiceController("DNBFileService");
                    sc.Start();
                }
                catch (Exception)
                {
                }
            }
        }

        private static void ReadSetup()
        {
            string location = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location); // Path.Combine(Application.StartupPath, "DNB_Extensions", "Kiwi");
            UriBuilder uri = new UriBuilder(location);
            string path = Uri.UnescapeDataString(uri.Path);
            XmlDocument configurationDoc = new XmlDocument();
            configurationDoc.Load(Path.Combine(path, "Configuration.xml"));
            SysLogIP = configurationDoc.SelectSingleNode("/Configuration/SysLog/IP").InnerXml;
            SysLogEnabled = Convert.ToBoolean(configurationDoc.SelectSingleNode("/Configuration/SysLog/Enabled").InnerXml);
            SysLogName = configurationDoc.SelectSingleNode("/Configuration/SysLog/Name").InnerXml;
            DebugLevel = Convert.ToInt16(configurationDoc.SelectSingleNode("/Configuration/SysLog/DebugLevel").InnerXml);
            configurationDoc = null;
        }
    }
}
