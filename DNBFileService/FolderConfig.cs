﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace ExcelTools
{
    [Serializable]
    public class FolderConfig
    {
        string _sourcePath;
        string _destinationPath;
        string _filter;
        bool _syncEnabled;
        DateTime _lastSync = Convert.ToDateTime("2010-01-01 00:00:00");

        public FolderConfig()
        {
            _sourcePath = "";
            _destinationPath = "";
            _syncEnabled = true;
            _filter = "*.*";
        }

        public FolderConfig(string SourcePath, string DestinationPath, bool SyncEnabled, DateTime LastSync, string Filter = "*.*")
        {
            _sourcePath = SourcePath;
            _destinationPath = DestinationPath;
            _syncEnabled = SyncEnabled;
            _filter = Filter;
            _lastSync = LastSync;
        }

        #region Properties

        public string SourcePath {
            get { return _sourcePath; }
            set { _sourcePath = value; }
        }

        public string DestinationPath {
            get { return _destinationPath; }
            set { _destinationPath = value; }
        }

        public string Filter {
            get { return _filter; }
            set { _filter = value; }
        }

        public bool SyncEnabled {
            get { return _syncEnabled; }
            set { _syncEnabled = value; }
        }

        public DateTime LastSyncTime {
            get { return _lastSync; }
            set { _lastSync = value; }
        }

        #endregion Properties

        public override string ToString()
        {
            return string.Format("[{0}] to [{1}] Filter [{2}] Enabled[{3}] Last sync [{4}]", _sourcePath, _destinationPath, _filter, _syncEnabled, _lastSync);
        }
    }

    [Serializable]
    public class FolderConfigSettings 
    {
        List<FolderConfig> _Entries;
        string _syslogIP;
        string _syslogName;
        int _MaxFileAgeInDays = 365;
        int _DeleteOlderThan = 0;
        public FolderConfigSettings()
        {
            _Entries = new List<FolderConfig>();
            _syslogIP = "127.0.0.1";
            _syslogName = "JohnDoe";
        }

        public string SyslogIP
        {
            get { return _syslogIP; }
            set { _syslogIP = value; }
        }

        public string SyslogName
        {
            get { return _syslogName; }
            set { _syslogName = value; }
        }

        public int DeleteOlderThan
        {
            get { return _DeleteOlderThan; }
            set { _DeleteOlderThan = value; }
        }

        public int MaxFileAgeInDays
        {
            get { return _MaxFileAgeInDays; }
            set { _MaxFileAgeInDays = value; }
        }

        public List<FolderConfig> Entries
        {
            get { return _Entries; }
            set { _Entries = value; }
        }

        public static void Serialize(string file, FolderConfigSettings c)
        {
            System.Xml.Serialization.XmlSerializer xs
               = new System.Xml.Serialization.XmlSerializer(c.GetType());
            StreamWriter writer = File.CreateText(file);
            xs.Serialize(writer, c);
            writer.Flush();
            writer.Close();
        }
        public static FolderConfigSettings Deserialize(string file)
        {
            System.Xml.Serialization.XmlSerializer xs
               = new System.Xml.Serialization.XmlSerializer(
                  typeof(FolderConfigSettings));
            StreamReader reader = File.OpenText(file);
            FolderConfigSettings c = (FolderConfigSettings)xs.Deserialize(reader);
            reader.Close();
            return c;
        }
    }
}
