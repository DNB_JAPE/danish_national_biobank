﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.IO;
using System.Security.Permissions;
using System.Diagnostics;
using System.Timers;

namespace DNBTools
{
    public class Watcher:IDisposable    
    {
        public event WatchHandler WatchEvent;
        public delegate void WatchHandler(Watcher w, FileSystemEventArgs e);

        private Timer _eventTimer = new Timer();
        private int _debugLevel = 3;

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        public List<FileSystemWatcher> CreateFolderWatcher(string path, string filter = "*.*")
        {
            List<FileSystemWatcher> watchList = new List<FileSystemWatcher>();
            
            try
            {
                if (_debugLevel > 2)
                    SysLog.Log(string.Format("Started FileWatcher with path [{0}] and filter [{1}]", path, filter), LogLevel.Debug);
                string[] myFilter = filter.Split(';');
                foreach (string  f in myFilter)
                {
                    FileSystemWatcher watcher = new FileSystemWatcher();

                    watcher.Path = path;
                    /* Watch for changes in LastAccess and LastWrite times, and the renaming of files or directories. */
                    watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
                    watcher.Filter = f;

                    // Add event handlers.
                    watcher.Changed += new FileSystemEventHandler(OnChanged);
                    watcher.Created += new FileSystemEventHandler(OnChanged);
                    //  watcher.Deleted += new FileSystemEventHandler(OnChanged);
                    watcher.Renamed += new RenamedEventHandler(OnRenamed);

                    // Begin watching.
                    watcher.EnableRaisingEvents = true;
                    watchList.Add(watcher);
                }
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Error when setting up Watcher: {0} ", ex.Message),LogLevel.Error);
                throw;
            }
            return watchList;
        }

        #region Properties

        public int DebugLevel
        {
            get { return _debugLevel; }
            set { _debugLevel = value; }
        }

        #endregion Properties

        #region EventHandling

        string _previousPath = string.Empty;
        // Define the event handlers.
        private void OnChanged(object source, FileSystemEventArgs e)
        {
            if (e.FullPath != _previousPath)
            {
                _previousPath = e.FullPath;
                WatchEvent?.Invoke(this, e);
                StartTimerForReset();
            }
            if (_debugLevel > 3)
                SysLog.Log(string.Format("File: {0} has been {1} ", e.FullPath, e.ChangeType), LogLevel.Debug);
        }

        private void OnRenamed(object source, RenamedEventArgs e)
        {
            if (e.FullPath != _previousPath)
            {
                _previousPath = e.FullPath;
                WatchEvent?.Invoke(this, e);
                StartTimerForReset();
            }
            WatchEvent?.Invoke(this, e);
            if (_debugLevel > 3)
                SysLog.Log(string.Format("File: {0} has been renamed to {1} ", e.OldFullPath, e.FullPath), LogLevel.Debug);
        }

        #endregion EventHandling

        #region ResetTimer
        /// <summary>
        /// Timer used for managing multiple hits on the same file.
        /// When event is triggered, the timer is started and
        /// will reset the previous path to the changed file so
        /// when a file is changed, only 1 event is raised.
        /// </summary>
        private void StartTimerForReset()
        {
            if (!_eventTimer.Enabled)
            {
                _eventTimer = new Timer();
                _eventTimer.Interval = 500;
                _eventTimer.Elapsed += _eventTimer_Elapsed;
                _eventTimer.Enabled = true;
                _eventTimer.Start();
            }
        }

        private void _eventTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _previousPath = string.Empty;
            _eventTimer.Enabled = false;
        }

        #endregion ResetTimer

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Watcher() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
       
    }
}
