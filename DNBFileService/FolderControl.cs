﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using DNBTools;

namespace ExcelTools
{
    public class FolderControl: IDisposable
    {
        private List<FileSystemWatcher> _sourceFolderWatch;
        private Watcher myWatch;
        private string _destinationFolder;
        private string _sourceFolder;
        private string _filter;
        private List<FileInfo> _destinationFileList;
        private List<FileInfo> _sourceFileList;
        private List<FileInfo> _deleteFileList;
        private FolderConfig _folderConfig;
        private int _debugLevel = 3;
        private int _MaxFileAgeInDays = 365;
        private int _DeleteOlderThan = 0;

        public event WatchHandler WatchEvent;
        public delegate void WatchHandler(FolderControl fc, FileSystemEventArgs e);

        public FolderControl(FolderConfig myConfiguration)
        {   _folderConfig = myConfiguration;

            _sourceFileList = GetFileList(_folderConfig.SourcePath, _folderConfig.Filter);
            _destinationFileList = GetFileList(_folderConfig.DestinationPath, _folderConfig.Filter);
            
            if (_debugLevel > 2)
                SysLog.Log("FolderControl started", LogLevel.Info);
        }

        public FolderControl(string SourcePath, string DestinationPath, string Filter, bool SyncEnabled, DateTime LastSync)
        {
            _destinationFolder = DestinationPath;
            _sourceFolder = SourcePath;
            _filter = Filter;
            _sourceFileList = GetFileList(_sourceFolder, _filter);
            _destinationFileList = GetFileList(_destinationFolder, _filter);

            _folderConfig = new FolderConfig(SourcePath, DestinationPath, SyncEnabled, LastSync, Filter);
            if (_debugLevel > 2)
                SysLog.Log("FolderControl started", LogLevel.Info);
        }

        public bool InitManager()
        {
            bool l_Reply = false;
            try
            {
                myWatch = new Watcher();
                myWatch.DebugLevel = _debugLevel;
                myWatch.WatchEvent += MyWatch_WatchEvent;
                _sourceFolderWatch = myWatch.CreateFolderWatcher(_folderConfig.SourcePath, _folderConfig.Filter);
                if (_debugLevel > 2)
                    SysLog.Log("FolderControl initialized", LogLevel.Info);
                l_Reply = true;
            }
            catch (Exception ex)
            {
                SysLog.Log("Error when FolderControl initialized: " + ex.Message, LogLevel.Error);
            }
            return l_Reply;
        }

        #region Properties
        public int DeleteOlderThan { get { return _DeleteOlderThan; } set { _DeleteOlderThan = value; } }

        public int MaxFileAgeInDays { get { return _MaxFileAgeInDays; } set { _MaxFileAgeInDays = value; } }

        public List<FileInfo> DeleteFileList { get { return _deleteFileList; } set { _deleteFileList = value; } }
        public List<FileInfo> DestinationFileList { get { return _destinationFileList; } set { _destinationFileList = value; } }

        public List<FileInfo> SourceFileList { get { return _sourceFileList; } set { _sourceFileList = value; } }

        public FolderConfig FolderConfiguration { get { return _folderConfig; } set { _folderConfig = value; } }

        public int DebugLevel { get { return _debugLevel; } set { _debugLevel = value; } }

        #endregion Properties

        #region Functions

        public List<FileInfo> GetFileList(string path="", string filter="")
        {
            List<FileInfo> fileList = new List<FileInfo>();

            try
            {
                DateTime syncStartTime = DateTime.Now - new TimeSpan(_MaxFileAgeInDays, 0, 0, 0);
                string[] files;

                files = filter.Split(';').SelectMany(f => System.IO.Directory.GetFiles(path, f).Where(p => new FileInfo(p).LastWriteTime > syncStartTime)).ToArray();

                //if (filter == "*.*")
                //    files = Directory.GetFiles(path, "*.*").Where(p => new FileInfo(p).LastWriteTime > syncStartTime).ToArray();
                //else // Only for multiple filter patterns
                //    files = Directory.GetFiles(path, filter).Where(p => filter.Contains(Path.GetExtension(p).ToLower())).Where(p => new FileInfo(p).LastWriteTime > syncStartTime).ToArray();

                foreach (var f in files)
                {
                    FileInfo fi = new FileInfo(f);
                    if (fi != null)
                        fileList.Add(fi);
                }
                SysLog.Log(string.Format("GetFileList found {0} files", fileList.Count), LogLevel.Info);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Error: Path: [{0}], Filter: [{1}] in GetFileList: {2}", path, filter, ex.Message), LogLevel.Error);
            }
            return fileList;
        }

        public bool SyncFolders()
        {
            bool l_Reply = false;
            try
            {
                // Check if sync is enabled
                if (!_folderConfig.SyncEnabled)
                {
                    SysLog.Log("Sync of folders is disabled for " + _folderConfig.SourcePath, LogLevel.Info);
                    return true;
                }

                SysLog.Log("Start Sync of folders", LogLevel.Info);
                CopyFiles();

                DeleteFiles();
            }
            catch (Exception ex)
            {
                SysLog.Log("Error in Sync files: " + ex.Message, LogLevel.Error);
            }
            SysLog.Log("Sync done", LogLevel.Info);
            _folderConfig.LastSyncTime = DateTime.Now;
            return l_Reply;
        }

        /// <summary>
        /// First the destination folder will be checked
        /// for space. If not enough space awailable, the sync will
        /// not continue.
        /// If file is new or modified, 
        /// it will be added to NewFiles list
        /// </summary>
        /// <param name="newF"></param>
        private void CheckFolderForSync(ref List<FileInfo> newF)
        {
            try
            {
                newF.Clear();
                
                // First Update current folder list
                _sourceFileList = GetFileList(_folderConfig.SourcePath, _folderConfig.Filter);
                _destinationFileList = GetFileList(_folderConfig.DestinationPath, _folderConfig.Filter);
                ulong totalBytesNeeded = 0;
                // Get missing files list
                foreach (var sf in _sourceFileList)
                {
                    FileInfo df = _destinationFileList.FirstOrDefault(f => f.Name == sf.Name);
                    if (df != null)
                    {
                        if ((sf.LastWriteTime - df.LastWriteTime) > new TimeSpan(0, 0, 0, 4))
                        {
                            newF.Add(sf);
                            totalBytesNeeded = totalBytesNeeded + (ulong)sf.Length;
                        }
                    }
                    else
                    {
                        newF.Add(sf);
                        totalBytesNeeded = totalBytesNeeded + (ulong)sf.Length;
                    }
                }
                // Check for space on destination
                // and clear New Files list if no space available.
                if (IsThereNotFreeSpaceAvailable(_folderConfig.DestinationPath, totalBytesNeeded)) newF.Clear();
            }
            catch (Exception ex)
            {
                SysLog.Log("Error in Check for Sync: " + ex.Message, LogLevel.Error);
            }
        }

        private void CheckFoldersForDeletion(ref List<FileInfo> delF)
        {
            try
            {
                delF.Clear();
                if (_DeleteOlderThan == 0) return;

                DateTime syncStartTime = DateTime.Now - new TimeSpan(_DeleteOlderThan, 0, 0, 0);
                string[] files;
                if (_folderConfig.Filter == "*.*")
                    files = Directory.GetFiles(_folderConfig.SourcePath, _folderConfig.Filter).Where(p => new FileInfo(p).LastWriteTime < syncStartTime).ToArray();
                else // Only for multiple filter patterns
                    files = Directory.GetFiles(_folderConfig.SourcePath, "*.*").Where(p => _folderConfig.Filter.Contains(Path.GetExtension(p).ToLower())).Where(p => new FileInfo(p).LastWriteTime < syncStartTime).ToArray();

                foreach (var f in files)
                {
                    FileInfo fi = new FileInfo(f);
                    if (fi != null)
                        delF.Add(fi);
                }
            }
            catch (Exception ex)
            {
                SysLog.Log("Error in Check for deletion: " + ex.Message, LogLevel.Error);
            }
        }

        private bool IsThereNotFreeSpaceAvailable(string path, ulong spaceRequired)
        {
            ulong spaceAvailable = DNBFileService.UNCDriveInfo.GetFreeSpaceAvailable(path);
            bool l_Reply = spaceRequired > spaceAvailable;

            if (l_Reply) SysLog.Log(string.Format("Not enough space available on destination folder. Required {0} Mb Availabel {1} Mb", spaceRequired / 1024 / 1024, spaceAvailable / 1024 / 1024), LogLevel.Warning);
            return l_Reply;
        }

        private void CopyFiles()
        {
            List<FileInfo> failedFiles = new List<FileInfo>();
            List<FileInfo> newFiles = new List<FileInfo>();

            CheckFolderForSync(ref newFiles);

            if (newFiles.Count > 0)
            {
                SysLog.Log(string.Format("Found {0} new or altered files", newFiles.Count), LogLevel.Info);
                // Found some and copy them.
                foreach (var nf in newFiles)
                {
                    try
                    {
                        nf.CopyTo(_folderConfig.DestinationPath + "\\" + nf.Name, true);
                        SysLog.Log(string.Format("Copying {0} to {1}", nf.Name, _folderConfig.DestinationPath), LogLevel.Info);
                    }
                    catch (Exception x)
                    {
                        SysLog.Log(string.Format("Couldn't copy {0} Reason: {1}", nf.Name, x.Message), LogLevel.Warning);
                        failedFiles.Add(nf);
                    }
                }
                // Check if files failed
                if (failedFiles.Count > 0)
                {
                    // WHAT TO DO?
                }
            }
            else
                SysLog.Log("No new or altered files found", LogLevel.Info);
        }

        private void DeleteFiles()
        {
            List<FileInfo> failedFiles = new List<FileInfo>();
            failedFiles.Clear();
            List<FileInfo> deleteFiles = new List<FileInfo>();
            CheckFoldersForDeletion(ref deleteFiles);

            if (deleteFiles.Count > 0)
            {
                SysLog.Log(string.Format("Found {0} files to delete", deleteFiles.Count), LogLevel.Info);
                // Found some and copy them.
                foreach (var nf in deleteFiles)
                {
                    try
                    {
                        nf.Delete();
                        SysLog.Log(string.Format("Deleted {0}", nf.Name), LogLevel.Info);
                    }
                    catch (Exception x)
                    {
                        SysLog.Log(string.Format("Couldn't delete {0} Reason: {1}", nf.Name, x.Message), LogLevel.Warning);
                        failedFiles.Add(nf);
                    }
                }

                // Check if files failed
                if (failedFiles.Count > 0)
                {
                    // WHAT TO DO?
                }
            }
            else
                SysLog.Log("No old files found", LogLevel.Info);

        }

        #endregion Functions

        #region Events

        private void MyWatch_WatchEvent(Watcher w, FileSystemEventArgs e)
        {
            try
            {
                //SysLog.Log(string.Format("File [{0}] in [{1}] has been {2}", e.Name, e.FullPath, e.ChangeType), LogLevel.Info);
                if (WatchEvent != null)
                    WatchEvent.Invoke(this, e);
                SyncFolders();
            }
            catch (Exception ex)
            {
                SysLog.Log("Error in WatchEvent for Foldercontrol: " + ex.Message, LogLevel.Error);
            }
        }
        #endregion Events

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    foreach (var i in _sourceFolderWatch)
                    {
                        i.EnableRaisingEvents = false;
                        i.Dispose();
                    }
                }
                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~FolderControl() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
