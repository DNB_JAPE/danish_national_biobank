﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.IO;
using System.Xml;
using System.Threading.Tasks;

namespace ITState
{
    internal class DiskSpaceWatcher : Watcher
    {
        protected string _path = string.Empty;
        protected int _limit;

        internal DiskSpaceWatcher(XmlNode node)
            : base(node)
        {
            if (State == WatcherState.Initialiserer)
            {
                try
                {
                    XmlNode targetNode = _configurationNode.SelectSingleNode("Target");
                    _path = targetNode.InnerText;
                    XmlNode limitNode = _configurationNode.SelectSingleNode("Limit");
                    _limit = Convert.ToInt32(limitNode.InnerText);
                }
                catch
                {
                    State = WatcherState.Stoppet;
                    StateDescription = "Fejlede under initialisering";
                }
            }
        }

        internal override async void DoWork()
        {
            await Task.Run(() =>
            {
                try
                {
                    DriveInfo driveInfo = new DriveInfo(_path);
                    long freeSpace = driveInfo.AvailableFreeSpace;
                    freeSpace = ((freeSpace / 1024) / 1024) / 1024;
                    if (freeSpace > _limit)
                    {
                        State = WatcherState.Ok;
                        StateDescription = freeSpace.ToString() + " GB free on " + _path;
                    }
                    else
                    {
                        State = WatcherState.Error;
                        StateDescription = freeSpace.ToString() + " GB free on " + _path;
                    }
                }
                catch (Exception ex)
                {
                    State = WatcherState.Error;
                    StateDescription = ex.Message;
                }
            });
        }

        internal override string Stop()
        {
            base.Stop();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("    <DiskSpaceWatcher Name=\"" + Name + "\">");
            sb.AppendLine("      <Target>" + _path + "</Target>");
            sb.AppendLine("      <Limit>" + _limit + "</Limit>");
            sb.AppendLine("    </DiskSpaceWatcher>");
            return sb.ToString();
        }
    }
}
