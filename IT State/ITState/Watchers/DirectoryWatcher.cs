﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ITState
{
    internal class DirectoryWatcher : Watcher
    {
        protected string _path = string.Empty;

        internal DirectoryWatcher(XmlNode node)
            : base(node)
        {
            if (State == WatcherState.Initialiserer)
            {
                try
                {
                    XmlNode targetNode = _configurationNode.SelectSingleNode("Target");
                    _path = targetNode.InnerText;
                }
                catch
                {
                    State = WatcherState.Stoppet;
                    StateDescription = "Fejlede under initialisering";
                }
            }
        }

        internal override async void DoWork()
        {
            await Task.Run(() =>
            {
                try
                {
                    DirectoryInfo directory = new DirectoryInfo(_path);
                    FileInfo[] files = directory.GetFiles();
                    if (files.Length == 0)
                    {
                        State = WatcherState.Ok;
                        StateDescription = "'" + _path + "' er tom";
                    }
                    else
                    {
                        State = WatcherState.Error;
                        StateDescription = "Der er " + files.Length + " filer i folderen";
                    }
                }
                catch (Exception ex)
                {
                    State = WatcherState.Error;
                    StateDescription = ex.Message;
                }
            });
        }

        internal override string Stop()
        {
            base.Stop();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("    <DirectoryWatcher Name=\"" + Name + "\">");
            sb.AppendLine("      <Target>" + _path + "</Target>");
            sb.AppendLine("    </DirectoryWatcher>");
            return sb.ToString();
        }
    }
}
