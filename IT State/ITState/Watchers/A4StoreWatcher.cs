﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data.Odbc;
using System.Data;
using System.Xml;
using System.Threading;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;

namespace ITState
{
    internal class A4StoreWatcher: Watcher
    {
        protected string _connectionString;

        internal A4StoreWatcher(XmlNode node)
            : base(node)
        {
            if (State == WatcherState.Initialiserer)
            {
                try
                {
                    XmlNode targetNode = _configurationNode.SelectSingleNode("Target");
                    _connectionString = Encryption.Decrypt(targetNode.InnerText);
                }
                catch
                {
                    State = WatcherState.Stoppet;
                    StateDescription = "Fejlede under initialisering";
                }
            }
        }

        internal override async void DoWork()
        {
            await Task.Run(() =>
            {
                try
                {
                    using (OracleConnection dbConnection = new OracleConnection(_connectionString))
                    {
                        dbConnection.Open();
                        using (OracleCommand cmd = dbConnection.CreateCommand())
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandText = "SELECT VALUE FROM SIS_OWNER.APP_VARIABLE WHERE VARIABLE_NAME='STATION_STATE'";
                            string stateString = cmd.ExecuteScalar().ToString();
                            if (stateString.Equals("AUTOMATIC"))
                            {
                                State = WatcherState.Ok;
                                StateDescription = "A4Store kører";
                            }
                            else
                            {
                                State = WatcherState.Error;
                                StateDescription = "A4Store kører ikke: " + stateString;
                            }
                        }
                    }
                }
                catch
                {
                    State = WatcherState.Error;
                    StateDescription = "Kunne ikke finde status på A4Store";
                }
            });
        }

        internal override string Stop()
        {
            base.Stop();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("    <A4StoreWatcher Name=\"" + Name + "\">");
            sb.AppendLine("      <Target>" + Encryption.Encrypt(_connectionString) + "</Target>");
            sb.AppendLine("    </A4StoreWatcher>");
            return sb.ToString();
        }
    }
}
