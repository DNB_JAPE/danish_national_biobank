﻿using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ITState
{
    internal class WindowsServiceWatcher : Watcher
    {
        protected string _path = string.Empty;
        protected string _serviceName;

        internal WindowsServiceWatcher(XmlNode node)
            : base(node)
        {
            if (State == WatcherState.Initialiserer)
            {
                try
                {
                    XmlNode targetNode = _configurationNode.SelectSingleNode("Target");
                    _path = targetNode.InnerText;
                    _serviceName = _configurationNode.Attributes["serviceName"].Value;
                    State = WatcherState.Initialiseret;
                    StateDescription = string.Empty;
                }
                catch
                {
                    State = WatcherState.Stoppet;
                    StateDescription = "Fejlede under initialisering";
                }
            }
        }

        internal override async void DoWork()
        {
            try
            {
                using (ServiceController serviceController = new ServiceController(_serviceName, _path))
                {
                    await Task.Run(() =>
                    {
                        serviceController.Refresh();
                    });
                    if (serviceController.Status == ServiceControllerStatus.Running)
                    {
                        State = WatcherState.Ok;
                        StateDescription = "'" + _serviceName + "' kører på " + _path;
                    }
                    else
                    {
                        State = WatcherState.Error;
                        StateDescription = "'" + _serviceName + "' kører ikke på " + _path;
                    }
                }
            }
            catch
            {
                State = WatcherState.Error;
                StateDescription = "Kunne ikke finde: '" + _serviceName + "' på " + _path;
            }
        }

        internal override string Stop()
        {
            base.Stop();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("    <WindowsServiceWatcher Name=\"" + Name + "\" serviceName=\"" + _serviceName + "\">");
            sb.AppendLine("      <Target>" + _path + "</Target>");
            sb.AppendLine("    </WindowsServiceWatcher>");
            return sb.ToString();
        }
    }
}
