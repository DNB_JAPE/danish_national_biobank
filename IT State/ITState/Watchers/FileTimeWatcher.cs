﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ITState
{
    internal class FileTimeWatcher : Watcher
    {
        protected string _path = string.Empty;
        protected TimeSpan _span = TimeSpan.MinValue;

        internal FileTimeWatcher(XmlNode node)
            : base(node)
        {
            if (State == WatcherState.Initialiserer)
            {
                try
                {
                    XmlNode targetNode = _configurationNode.SelectSingleNode("Target");
                    _path = targetNode.InnerText;
                    XmlNode dayNode = _configurationNode.SelectSingleNode("Days");
                    int days = Convert.ToInt32(dayNode.InnerText);
                    XmlNode hourNode = _configurationNode.SelectSingleNode("Hours");
                    int hours = Convert.ToInt32(hourNode.InnerText);
                    XmlNode minuteNode = _configurationNode.SelectSingleNode("Minutes");
                    int minutes = Convert.ToInt32(minuteNode.InnerText);
                    _span = new TimeSpan(days, hours, minutes, 0);
                }
                catch
                {
                    State = WatcherState.Stoppet;
                    StateDescription = "Fejlede under initialisering";
                }
            }
        }

        internal override async void DoWork()
        {
            await Task.Run(() =>
            {
                try
                {
                    FileInfo file = new FileInfo(_path);
                    if(!file.Exists)
                    {
                        State = WatcherState.Error;
                        StateDescription = "Der er ingen backup fil i folderen";
                    }
                    else if (file.LastWriteTime < DateTime.Today - _span)
                    {
                        State = WatcherState.Error;
                        StateDescription = "Filen er for gammel";
                    }
                    else
                    {
                        State = WatcherState.Ok;
                        StateDescription = "Ok: " + file.LastWriteTime.ToShortDateString();
                    }
                }
                catch (Exception ex)
                {
                    State = WatcherState.Error;
                    StateDescription = ex.Message;
                }
            });
        }

        internal override string Stop()
        {
            base.Stop();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("    <FileTimeWatcher Name=\"" + Name + "\">");
            sb.AppendLine("      <Target>" + _path + "</Target>");
            sb.AppendLine("      <Days>" + _span.Days + "</Days>");
            sb.AppendLine("      <Hours>" + _span.Hours + "</Hours>");
            sb.AppendLine("      <Minutes>" + _span.Minutes + "</Minutes>");
            sb.AppendLine("    </FileTimeWatcher>");
            return sb.ToString();
        }
    }
}
