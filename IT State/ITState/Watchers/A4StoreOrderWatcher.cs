﻿using Oracle.DataAccess.Client;
using System;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ITState
{
    internal class A4StoreOrderWatcher : Watcher
    {
        protected string _connectionString;

        internal A4StoreOrderWatcher(XmlNode node)
            : base(node)
        {
            if (State == WatcherState.Initialiserer)
            {
                try
                {
                    XmlNode targetNode = _configurationNode.SelectSingleNode("Target");
                    _connectionString = Encryption.Decrypt(targetNode.InnerText);
                }
                catch
                {
                    State = WatcherState.Stoppet;
                    StateDescription = "Fejlede under initialisering";
                }
            }
        }

        internal override async void DoWork()
        {
            await Task.Run(() =>
            {
                try
                {
                    using (OracleConnection dbConnection = new OracleConnection(_connectionString))
                    {
                        dbConnection.Open();
                        using (OracleCommand cmd = dbConnection.CreateCommand())
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandText = "SELECT COUNT(*) FROM INTFC.BROOKS_ORDERS WHERE LOCATION_UPDATED!=1";
                            int count = Convert.ToInt32(cmd.ExecuteScalar());
                            if (count == 0)
                            {
                                State = WatcherState.Ok;
                                StateDescription = "Alle ordrer er opdaterede";
                            }
                            else
                            {
                                State = WatcherState.Error;
                                StateDescription = "Der er " + count + " ordrer der ikke opdaterede";
                            }
                        }
                    }
                }
                catch
                {
                    State = WatcherState.Error;
                    StateDescription = "Kunne ikke finde status på A4Store ordrer";
                }
            });
        }

        internal override string Stop()
        {
            base.Stop();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("    <A4StoreOrderWatcher Name=\"" + Name + "\">");
            sb.AppendLine("      <Target>" + Encryption.Encrypt(_connectionString) + "</Target>");
            sb.AppendLine("    </A4StoreOrderWatcher>");
            return sb.ToString();
        }
    }
}
