﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ITState
{
    internal class FileParserWatcher : Watcher
    {
        protected string _path = string.Empty;
        private string _errorToken;
        private string _beginToken;
        private string _beginTokenType;
        private DateTime _lastClear;

        internal FileParserWatcher(XmlNode node)
            : base(node)
        {
            if (State == WatcherState.Initialiserer)
            {
                try
                {
                    XmlNode targetNode = _configurationNode.SelectSingleNode("Target");
                    _path = targetNode.InnerText;
                    XmlNode errorNode = node.SelectSingleNode("ErrorToken");
                    _errorToken = errorNode.InnerText;
                    XmlNode beginTokenNode = node.SelectSingleNode("BeginToken");
                    if (beginTokenNode != null)
                    {
                        _beginToken = beginTokenNode.InnerText;
                        XmlAttribute attribute = (XmlAttribute)beginTokenNode.Attributes.GetNamedItem("type");
                        _beginTokenType = attribute.Value;
                    }
                    else
                    {
                        _beginTokenType = "Text";
                    }
                    XmlNode lastClearNode = _configurationNode.SelectSingleNode("LastClear");
                    if (lastClearNode == null)
                    {
                        _lastClear = DateTime.MinValue;
                    }
                    else
                    {
                        _lastClear = DateTime.Parse(lastClearNode.InnerText);
                    }
                }
                catch
                {
                    State = WatcherState.Stoppet;
                    StateDescription = "Fejlede under initialisering";
                }
            }
        }

        internal override async void DoWork()
        {
            await Task.Run(() =>
            {
                try
                {
                    FileInfo file = new FileInfo(_path);
                    if (!file.Exists)
                    {
                        State = WatcherState.Error;
                        StateDescription = "'" + _path + "' eksisterer ikke";
                    }
                    else
                    {
                        bool someDateFound = false;
                        using (StreamReader reader = new StreamReader(file.FullName))
                        {
                            int index = 0;
                            string line = string.Empty;
                            bool begun = string.IsNullOrEmpty(_beginToken) ? true : false;
                            while ((line = reader.ReadLine()) != null)
                            {
                                index++;
                                if (!begun)
                                {
                                    if (_beginTokenType.Equals("Text") && line.Contains(_beginToken))
                                    {
                                        begun = true;
                                    }
                                    else if (_beginTokenType.Equals("Date") && line.Length >= _beginToken.Length)
                                    {
                                        string linePart = line.Substring(0, _beginToken.Length);
                                        DateTime lineValue;
                                        bool parsed = DateTime.TryParseExact(linePart, _beginToken, CultureInfo.InvariantCulture, DateTimeStyles.None, out lineValue);
                                        if (parsed)
                                        {
                                            someDateFound = true;
                                            if (lineValue > _lastClear)
                                            {
                                                begun = true;
                                            }
                                        }
                                    }
                                }
                                else if (begun && line.Contains(_errorToken))
                                {
                                    State = WatcherState.Error;
                                    StateDescription = "Linie " + index + ": '" + line + "'";
                                    return;
                                }
                            }
                        }
                        if (_beginTokenType.Equals("Date") && !someDateFound)
                        {
                            State = WatcherState.Error;
                            StateDescription = "Fandt ingen datoer, der passer med formatet";
                        }
                        else
                        {
                            State = WatcherState.Ok;
                            StateDescription = _path + " ";
                        }
                    }
                }
                catch(Exception ex)
                {
                    State = WatcherState.Error;
                    StateDescription = "Fejl: '" + ex.Message;
                }
            });
        }

        internal override void Clear()
        {
            _lastClear = DateTime.Now;
        }

        internal override string Stop()
        {
            base.Stop();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("    <FileParserWatcher Name=\"" + Name + "\">");
            sb.AppendLine("      <Target>" + _path + "</Target>");
            sb.AppendLine("      <ErrorToken>" + _errorToken + "</ErrorToken>");
            if(_beginTokenType == "Date")
            {
                sb.AppendLine("      <BeginToken type=\"Date\">" + _beginToken + "</BeginToken>");
                sb.AppendLine("      <LastClear>" + _lastClear.ToString("G") + "</LastClear>");
            }
            sb.AppendLine("    </FileParserWatcher>");
            return sb.ToString();
        }
    }
}
