﻿using System;
using System.Data;
using Microsoft.Office.Interop.Excel;
using System.IO;
using System.Windows.Forms;

namespace ITState
{
    public class ExcellConverter
    {
        public void ConvertExcel()
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                DirectoryInfo dir = new DirectoryInfo(dialog.SelectedPath);
                FileInfo[] inputFiles = dir.GetFiles();
                foreach (FileInfo inputFile in inputFiles)
                {
                    string csvFileName = string.Empty;
                    if (inputFile.Extension.Equals(".xls"))
                    {
                        csvFileName = inputFile.FullName.Substring(0, inputFile.FullName.Length - 3) + "csv";
                    }
                    else if (inputFile.Extension.Equals(".xlsx"))
                    {
                        csvFileName = inputFile.FullName.Substring(0, inputFile.FullName.Length - 4) + "csv";
                    }
                    if (csvFileName != string.Empty)
                    {
                        ExcellConverter excell = new ExcellConverter();
                        excell.ConvertExcelToCSV(inputFile.FullName, csvFileName);
                    }
                }
            }
        }

        public bool ConvertExcelToCSV(string sourceExcelPathAndName, string targetCSVPathAndName)
        {
            Microsoft.Office.Interop.Excel.Application oXL = null;
            Workbook mWorkBook = null;
            Sheets mWorkSheets = null;
            Worksheet mWSheet1 = null;
            try
            {
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = false;
                oXL.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Workbooks workbooks = oXL.Workbooks;
                mWorkBook = workbooks.Open(sourceExcelPathAndName, 0, false, 5, "", "", false, XlPlatform.xlWindows, "", true, false, 0, true, false, false);
                //Get all the sheets in the workbook
                mWorkSheets = mWorkBook.Worksheets;
                //Get the specified sheet
                mWSheet1 = (Worksheet)mWorkSheets[1];
                Microsoft.Office.Interop.Excel.Range range = mWSheet1.UsedRange;
                //replacing ENTER with a space
                range.Replace("\n", " ", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                //replacing COMMA with the column delimeter
                range.Replace(",", @"|#|", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                mWorkBook.SaveAs(targetCSVPathAndName + ".tmp", XlFileFormat.xlTextMSDOS, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, false);
                mWSheet1 = null; 
                mWorkBook.Close(Type.Missing, Type.Missing, Type.Missing);
                mWorkBook = null;
                oXL.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oXL);
                oXL = null;
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                using (StreamReader reader = new StreamReader(targetCSVPathAndName + ".tmp"))
                {
                    using (StreamWriter writer = new StreamWriter(targetCSVPathAndName + ".tmp1"))
                    {
                        while (true)
                        {
                            string line = reader.ReadLine();
                            if (line == null)
                            {
                                break;
                            }
                            line = line.Replace('\t', ';');
                            writer.WriteLine(line);
                        }
                    }
                }
                File.Delete(targetCSVPathAndName + ".tmp");
                File.Move(targetCSVPathAndName + ".tmp1", targetCSVPathAndName);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Failed to convert " + sourceExcelPathAndName + ": " + ex.Message);
                if (mWSheet1 != null) { mWSheet1 = null; }
                if (mWorkBook != null) { mWorkBook.Close(Type.Missing, Type.Missing, Type.Missing); }
                if (mWorkBook != null) { mWorkBook = null; }
                if (oXL != null)
                {
                    oXL.Quit();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oXL);
                }
                if (oXL != null) { oXL = null; }
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                return false;
            }
        }
    }
}
