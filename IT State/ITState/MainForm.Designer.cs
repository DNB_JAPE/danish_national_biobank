﻿namespace ITState
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._stateListView = new System.Windows.Forms.ListView();
            this.nameColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.stateColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.descriptionColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // _stateListView
            // 
            this._stateListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nameColumnHeader,
            this.stateColumnHeader,
            this.descriptionColumnHeader});
            this._stateListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._stateListView.FullRowSelect = true;
            this._stateListView.GridLines = true;
            this._stateListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this._stateListView.Location = new System.Drawing.Point(0, 0);
            this._stateListView.MultiSelect = false;
            this._stateListView.Name = "_stateListView";
            this._stateListView.Size = new System.Drawing.Size(666, 273);
            this._stateListView.TabIndex = 1;
            this._stateListView.UseCompatibleStateImageBehavior = false;
            this._stateListView.View = System.Windows.Forms.View.Details;
            this._stateListView.MouseDown += new System.Windows.Forms.MouseEventHandler(this._stateListView_MouseDown);
            // 
            // nameColumnHeader
            // 
            this.nameColumnHeader.Text = "Name";
            this.nameColumnHeader.Width = 200;
            // 
            // stateColumnHeader
            // 
            this.stateColumnHeader.Text = "State";
            // 
            // descriptionColumnHeader
            // 
            this.descriptionColumnHeader.Text = "Description";
            this.descriptionColumnHeader.Width = 865;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(666, 273);
            this.Controls.Add(this._stateListView);
            this.Name = "MainForm";
            this.Text = "ITState";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView _stateListView;
        private System.Windows.Forms.ColumnHeader nameColumnHeader;
        private System.Windows.Forms.ColumnHeader stateColumnHeader;
        private System.Windows.Forms.ColumnHeader descriptionColumnHeader;
    }
}

