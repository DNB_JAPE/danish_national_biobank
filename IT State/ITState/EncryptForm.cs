﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ITState
{
    public partial class EncryptForm : Form
    {
        public EncryptForm()
        {
            InitializeComponent();
        }

        private void _encryptButton_Click(object sender, EventArgs e)
        {
            _encryptedTextBox.Text = Encryption.Encrypt(_plainTextBox.Text);
        }

        private void _decryptButton_Click(object sender, EventArgs e)
        {
            _plainTextBox.Text = Encryption.Decrypt(_encryptedTextBox.Text);
        }

        private void _base64EncodeButton_Click(object sender, EventArgs e)
        {
            _base64EncodedTextBox.Text = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(_base64PlainTextBox.Text));
        }

        private void _base64DecodeButton_Click(object sender, EventArgs e)
        {
            _base64PlainTextBox.Text = ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(_base64EncodedTextBox.Text));
        }
    }
}
