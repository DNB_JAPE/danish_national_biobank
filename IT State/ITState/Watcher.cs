﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Xml;
using System.Threading;
using System.Threading.Tasks;

namespace ITState
{
    internal class Watcher : IDisposable
    {
        protected XmlNode _configurationNode;
        private System.Windows.Forms.Timer _timer;
        private readonly SynchronizationContext synchronizationContext;
        protected object _lock = new object();

        internal WatcherState State { get; set; }
        internal string StateDescription { get; set; }
        internal virtual string Name { get; set; }
        internal virtual bool Running { get; set; }

        internal Watcher(XmlNode node)
        {
            try
            {
                _configurationNode = node;
                Name = _configurationNode.Attributes["Name"].Value;
                Running = false;
                State = WatcherState.Initialiserer;
                StateDescription = string.Empty;
            }
            catch
            {
                Running = false;
                State = WatcherState.Stoppet;
                StateDescription = "Fejlede under initialisering";
            }
            synchronizationContext = SynchronizationContext.Current;
            _timer = new System.Windows.Forms.Timer();
            _timer.Interval = 10000;
            _timer.Tick += _timer_Tick;
        }

        public void Dispose()
        {
            _timer.Dispose();
        }

        internal virtual void Start()
        {
            lock (_lock)
            {
                Running = true;
                State = WatcherState.Initialiserer;
                StateDescription = string.Empty;
                _timer.Enabled = true;
            }
        }

        internal virtual string Stop()
        {
            lock (_lock)
            {
                Running = false;
                State = WatcherState.Stoppet;
                StateDescription = "Stoppet";
                _timer.Enabled = false;
                return "    <Watcher Name=\"" + Name + "\"></Watcher>" + Environment.NewLine;
            }
        }      

        void _timer_Tick(object sender, EventArgs e)
        {
            lock (_lock)
            {
                _timer.Enabled = false;
                DoWork();
                _timer.Enabled = true;
            }
        }

        internal virtual void Clear()
        {
            // Used to exclude errors older than now
        }

        internal virtual async void DoWork()
        {
            await Task.Run(() =>
            {
            });
        }
    }
}
