﻿namespace ITState
{
    partial class EncryptForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._decryptButton = new System.Windows.Forms.Button();
            this._encryptButton = new System.Windows.Forms.Button();
            this._encryptedTextBox = new System.Windows.Forms.TextBox();
            this._plainTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._base64DecodeButton = new System.Windows.Forms.Button();
            this._base64EncodeButton = new System.Windows.Forms.Button();
            this._base64EncodedTextBox = new System.Windows.Forms.TextBox();
            this._base64PlainTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.EncryptionGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.EncryptionGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _decryptButton
            // 
            this._decryptButton.Location = new System.Drawing.Point(86, 102);
            this._decryptButton.Name = "_decryptButton";
            this._decryptButton.Size = new System.Drawing.Size(75, 23);
            this._decryptButton.TabIndex = 11;
            this._decryptButton.Text = "Decrypt";
            this._decryptButton.UseVisualStyleBackColor = true;
            this._decryptButton.Click += new System.EventHandler(this._decryptButton_Click);
            // 
            // _encryptButton
            // 
            this._encryptButton.Location = new System.Drawing.Point(5, 102);
            this._encryptButton.Name = "_encryptButton";
            this._encryptButton.Size = new System.Drawing.Size(75, 23);
            this._encryptButton.TabIndex = 10;
            this._encryptButton.Text = "Encrypt";
            this._encryptButton.UseVisualStyleBackColor = true;
            this._encryptButton.Click += new System.EventHandler(this._encryptButton_Click);
            // 
            // _encryptedTextBox
            // 
            this._encryptedTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._encryptedTextBox.Location = new System.Drawing.Point(5, 75);
            this._encryptedTextBox.Name = "_encryptedTextBox";
            this._encryptedTextBox.Size = new System.Drawing.Size(249, 20);
            this._encryptedTextBox.TabIndex = 9;
            // 
            // _plainTextBox
            // 
            this._plainTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._plainTextBox.Location = new System.Drawing.Point(6, 33);
            this._plainTextBox.Name = "_plainTextBox";
            this._plainTextBox.Size = new System.Drawing.Size(248, 20);
            this._plainTextBox.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Encrypted Value:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Plain Value:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._base64DecodeButton);
            this.groupBox1.Controls.Add(this._base64EncodeButton);
            this.groupBox1.Controls.Add(this._base64EncodedTextBox);
            this.groupBox1.Controls.Add(this._base64PlainTextBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(12, 155);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(260, 139);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Base 64";
            // 
            // _base64DecodeButton
            // 
            this._base64DecodeButton.Location = new System.Drawing.Point(86, 102);
            this._base64DecodeButton.Name = "_base64DecodeButton";
            this._base64DecodeButton.Size = new System.Drawing.Size(75, 23);
            this._base64DecodeButton.TabIndex = 17;
            this._base64DecodeButton.Text = "Decode";
            this._base64DecodeButton.UseVisualStyleBackColor = true;
            this._base64DecodeButton.Click += new System.EventHandler(this._base64DecodeButton_Click);
            // 
            // _base64EncodeButton
            // 
            this._base64EncodeButton.Location = new System.Drawing.Point(5, 102);
            this._base64EncodeButton.Name = "_base64EncodeButton";
            this._base64EncodeButton.Size = new System.Drawing.Size(75, 23);
            this._base64EncodeButton.TabIndex = 16;
            this._base64EncodeButton.Text = "Encode";
            this._base64EncodeButton.UseVisualStyleBackColor = true;
            this._base64EncodeButton.Click += new System.EventHandler(this._base64EncodeButton_Click);
            // 
            // _base64EncodedTextBox
            // 
            this._base64EncodedTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._base64EncodedTextBox.Location = new System.Drawing.Point(5, 75);
            this._base64EncodedTextBox.Name = "_base64EncodedTextBox";
            this._base64EncodedTextBox.Size = new System.Drawing.Size(249, 20);
            this._base64EncodedTextBox.TabIndex = 15;
            // 
            // _base64PlainTextBox
            // 
            this._base64PlainTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._base64PlainTextBox.Location = new System.Drawing.Point(6, 33);
            this._base64PlainTextBox.Name = "_base64PlainTextBox";
            this._base64PlainTextBox.Size = new System.Drawing.Size(248, 20);
            this._base64PlainTextBox.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Encoded Value:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Plain Value:";
            // 
            // EncryptionGroupBox
            // 
            this.EncryptionGroupBox.Controls.Add(this.label1);
            this.EncryptionGroupBox.Controls.Add(this.label2);
            this.EncryptionGroupBox.Controls.Add(this._decryptButton);
            this.EncryptionGroupBox.Controls.Add(this._plainTextBox);
            this.EncryptionGroupBox.Controls.Add(this._encryptButton);
            this.EncryptionGroupBox.Controls.Add(this._encryptedTextBox);
            this.EncryptionGroupBox.Location = new System.Drawing.Point(12, 12);
            this.EncryptionGroupBox.Name = "EncryptionGroupBox";
            this.EncryptionGroupBox.Size = new System.Drawing.Size(260, 137);
            this.EncryptionGroupBox.TabIndex = 13;
            this.EncryptionGroupBox.TabStop = false;
            this.EncryptionGroupBox.Text = "Encryption";
            // 
            // EncryptForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 311);
            this.Controls.Add(this.EncryptionGroupBox);
            this.Controls.Add(this.groupBox1);
            this.Name = "EncryptForm";
            this.Text = "EncryptForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.EncryptionGroupBox.ResumeLayout(false);
            this.EncryptionGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _decryptButton;
        private System.Windows.Forms.Button _encryptButton;
        private System.Windows.Forms.TextBox _encryptedTextBox;
        private System.Windows.Forms.TextBox _plainTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button _base64DecodeButton;
        private System.Windows.Forms.Button _base64EncodeButton;
        private System.Windows.Forms.TextBox _base64EncodedTextBox;
        private System.Windows.Forms.TextBox _base64PlainTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox EncryptionGroupBox;
    }
}