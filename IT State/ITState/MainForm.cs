﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace ITState
{
    public partial class MainForm : Form
    {
        private List<Watcher> _watchers = new List<Watcher>();
        private System.Windows.Forms.Timer _timer;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            XmlDocument configuration = new XmlDocument();
            configuration.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Configuration.xml"));
            XmlNodeList directoryWatcherNodes = configuration.SelectNodes("//Configuration/Watchers/DirectoryWatcher");
            foreach (XmlNode directoryWatcherNode in directoryWatcherNodes)
            {
                _watchers.Add(new DirectoryWatcher(directoryWatcherNode));
            }
            XmlNodeList WindowsServiceWatcherNodes = configuration.SelectNodes("//Configuration/Watchers/WindowsServiceWatcher");
            foreach (XmlNode WindowsServiceWatcherNode in WindowsServiceWatcherNodes)
            {
                _watchers.Add(new WindowsServiceWatcher(WindowsServiceWatcherNode));
            }
            XmlNodeList FileParserWatcherNodes = configuration.SelectNodes("//Configuration/Watchers/FileParserWatcher");
            foreach (XmlNode FileParserWatcherNode in FileParserWatcherNodes)
            {
                _watchers.Add(new FileParserWatcher(FileParserWatcherNode));
            }
            XmlNodeList A4StoreWatcherNodes = configuration.SelectNodes("//Configuration/Watchers/A4StoreWatcher");
            foreach (XmlNode A4StoreWatcherNode in A4StoreWatcherNodes)
            {
                _watchers.Add(new A4StoreWatcher(A4StoreWatcherNode));
            }
            XmlNodeList A4StoreOrderWatcherNodes = configuration.SelectNodes("//Configuration/Watchers/A4StoreOrderWatcher");
            foreach (XmlNode A4StoreOrderWatcherNode in A4StoreOrderWatcherNodes)
            {
                _watchers.Add(new A4StoreOrderWatcher(A4StoreOrderWatcherNode));
            }
            XmlNodeList DiskSpaceWatcherNodes = configuration.SelectNodes("//Configuration/Watchers/DiskSpaceWatcher");
            foreach (XmlNode DiskSpaceWatcherNode in DiskSpaceWatcherNodes)
            {
                _watchers.Add(new DiskSpaceWatcher(DiskSpaceWatcherNode));
            }
            XmlNodeList FileTimeWatcherNodes = configuration.SelectNodes("//Configuration/Watchers/FileTimeWatcher");
            foreach (XmlNode FileTimeWatcherNode in FileTimeWatcherNodes)
            {
                _watchers.Add(new FileTimeWatcher(FileTimeWatcherNode));
            }
            UpdateUI();
            _stateListView.Columns[_stateListView.Columns.Count - 1].Width = -2; // Magic that makes the last column expand to fill the remaining width.
            _timer = new System.Windows.Forms.Timer();
            _timer.Interval = 2000;
            _timer.Tick += _timer_Tick;
            _timer.Enabled = true;
            foreach(Watcher watcher in _watchers)
            {
                watcher.Start();
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _timer.Enabled = false;
            _timer.Dispose();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            sb.AppendLine("<Configuration>");
            sb.AppendLine("  <Watchers>");
            foreach (Watcher watcher in _watchers)
            {
                sb.Append(watcher.Stop());
            }
            sb.AppendLine("  </Watchers>");
            sb.AppendLine("</Configuration>");
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Configuration.xml");
            StreamWriter writer = new StreamWriter(path,false);
            writer.Write(sb);
            writer.Close();
        }

        void _timer_Tick(object sender, EventArgs e)
        {
            _timer.Enabled = false;
            UpdateUI();
            _timer.Enabled = true;
        }

        private void UpdateUI()
        {
            _stateListView.Items.Clear();
            foreach (Watcher watcher in _watchers)
            {
                ListViewItem item = new ListViewItem(new string[] { watcher.Name, watcher.State.ToString(), watcher.StateDescription });
                switch (watcher.State)
                {
                    case WatcherState.Error:
                        {
                            item.ForeColor = Color.Red;
                            break;
                        }
                    case WatcherState.Stoppet:
                    case WatcherState.Initialiserer:
                        {
                            item.ForeColor = Color.Gray;
                            break;
                        }
                    default:
                        {
                            item.ForeColor = Color.Black;
                            break;
                        }
                }
                item.Tag = watcher;
                _stateListView.Items.Add(item);
            }
        }

        private void _stateListView_MouseDown(object sender, MouseEventArgs e)
        {
            bool match = false;
            if (e.Button == MouseButtons.Right)
            {
                foreach (ListViewItem item in _stateListView.Items)
                {
                    if (item.Bounds.Contains(new Point(e.X, e.Y)))
                    {
                        Watcher watcher = item.Tag as Watcher;
                        MenuItem muteMenuItem = new MenuItem(!watcher.Running ? "Genoptag" : "Mute");
                        muteMenuItem.Tag = watcher;
                        muteMenuItem.Click += new EventHandler(muteMenuItem_Click);
                        MenuItem clearMenuItem = new MenuItem("Clear");
                        clearMenuItem.Tag = watcher;
                        clearMenuItem.Click += new EventHandler(clearMenuItem_Click);
                        MenuItem[] menuItems = new MenuItem[] { muteMenuItem, clearMenuItem };
                        _stateListView.ContextMenu = new ContextMenu(menuItems);
                        match = true;
                        break;
                    }
                }
                if (match)
                {
                    _stateListView.ContextMenu.Show(_stateListView, new Point(e.X, e.Y));
                }
                else
                {
                    _stateListView.ContextMenu = null;
                }
            }
        }

        private void muteMenuItem_Click(object sender, EventArgs e)
        {
            Watcher watcher = (sender as MenuItem).Tag as Watcher;
            if(watcher.Running)
            {
                watcher.Stop();
            }
            else 
            {
                watcher.Start();
            }
            UpdateUI();
        }

        private void clearMenuItem_Click(object sender, EventArgs e)
        {
            Watcher watcher = (sender as MenuItem).Tag as Watcher;
            watcher.Clear();
            watcher.Start();
        }
    }
}
