﻿using DNBTools.DataClasses;
using DNBTools.Nautilus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AddPatientExtension
{
    public partial class AddPatientExtensionForm : Form
    {
        private Operator _operator;
        private LimsCommunication _limsCommunication;

        public AddPatientExtensionForm(string operatorId, LimsCommunication limsCommunication)
        {
            InitializeComponent();
            _limsCommunication = limsCommunication;
            _operator = _limsCommunication.OperatorGet(operatorId);
        }

        private void ResetForm()
        {
            _createButton.Enabled = false;
            _cprTextBox.Text = string.Empty;
            _projectComboBox.SelectedIndex = -1;
            _projectPersonIdTextBox.Text = string.Empty;
            _customerComboBox.SelectedIndex = -1;
        }

        private void SetButtonState()
        {
            Regex cprRegex = new Regex(@"^\d\d\d\d\d\d\d\d\d\d$");
            if(!cprRegex.IsMatch(_cprTextBox.Text) || _projectComboBox.Text == string.Empty || _projectPersonIdTextBox.Text == string.Empty || _customerComboBox.Text == string.Empty)
            {
                _createButton.Enabled = false;
            }
            else
            {
                _createButton.Enabled = true;
            }
        }

        private void AddPersonExtensionForm_Load(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void _cprTextBox_TextChanged(object sender, EventArgs e)
        {
            SetButtonState();
        }

        private void _projectComboBox_TextChanged(object sender, EventArgs e)
        {
            SetButtonState();
        }

        private void _projectPersonIdTextBox_TextChanged(object sender, EventArgs e)
        {
            SetButtonState();
        }

        private void _customerComboBox_TextChanged(object sender, EventArgs e)
        {
            SetButtonState();
        }

        private void _createButton_Click(object sender, EventArgs e)
        {
            _createButton.Enabled = false;
            try
            {
                _limsCommunication.PatientCreate(_cprTextBox.Text, _projectPersonIdTextBox.Text, _customerComboBox.Text, _projectComboBox.Text, _operator, false);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Kunne ikke oprette patienten: " + ex.Message);
            }
            if (_projectComboBox.Text == "BSG")
            {
                try
                {
                    string patientId = _limsCommunication.PatientGetNautilusId(_cprTextBox.Text);
                    _limsCommunication.PatientSetBsgId(patientId, _projectPersonIdTextBox.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Kunne ikke sætte BSG id på patienten: " + ex.Message);
                }
            }
            ResetForm();
        }
    }
}
