﻿using DNBTools.Nautilus;
using LSEXT;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Thermo.Nautilus.Extensions.UserInterface;

/// <summary>
/// Extension allowing users to easily create new persons in Nautilus.
/// </summary>
namespace AddPatientExtension
{
    public class AddPatientExtension : EntityExtension, IVersion
    {
        public int GetVersion()
        {
            return 3;
        }

        /// <summary>
        /// Called by Lims to decide if the extension should be listed in the right click menu.
        /// </summary>
        /// <param name="Parameters">Lims parameters (not used).</param>
        /// <returns>ExecuteExtension.exEnabled - Always allow this extension to start.</returns>
        public override EntityExtension.enumExtensionVisibility CanExecuteExtension()
        {
            return EntityExtension.enumExtensionVisibility.Enabled;
        }

        /// <summary>
        /// Called by Lims to activate the extension.
        /// </summary>
        public override void ExecuteExtension()
        {
            string operatorId;
            try
            {
                operatorId = Parameters["OPERATOR_ID"].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Kunne ikke læse data fra Nautilus: " + ex.Message);
                return;
            }
            string connectionString = string.Empty;
            try
            {
                string location = Path.Combine(Application.StartupPath, "DNB_Extensions", "AddPatient");
                UriBuilder uri = new UriBuilder(location);
                string path = Uri.UnescapeDataString(uri.Path);
                XmlDocument configurationDoc = new XmlDocument();
                configurationDoc.Load(Path.Combine(path, "Configuration.xml"));
                if (NautilusTools.IsProduction())
                {
                    connectionString = configurationDoc.SelectSingleNode("/Configuration/NautConStrProd").InnerXml;
                }
                else
                {
                    connectionString = configurationDoc.SelectSingleNode("/Configuration/NautConStrTest").InnerXml;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Kunne ikke læse data fra konfigurationen: " + ex.Message);
                return;
            }
            LimsCommunication limsCommunication = null;
            try
            {
                limsCommunication = new LimsCommunication(connectionString);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Kunne ikke Oprette forbindelse til Nautilus DB: " + ex.Message);
                return;
            }
            AddPatientExtensionForm extensionForm = new AddPatientExtensionForm(operatorId, limsCommunication);
            extensionForm.ShowDialog();
            limsCommunication.Dispose();
        }
    }
}
