﻿namespace AddPatientExtension
{
    partial class AddPatientExtensionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._cprLabel = new System.Windows.Forms.Label();
            this._customerLabel = new System.Windows.Forms.Label();
            this._projectPersonIdLabel = new System.Windows.Forms.Label();
            this._projectLabel = new System.Windows.Forms.Label();
            this._cprTextBox = new System.Windows.Forms.TextBox();
            this._projectComboBox = new System.Windows.Forms.ComboBox();
            this._projectPersonIdTextBox = new System.Windows.Forms.TextBox();
            this._customerComboBox = new System.Windows.Forms.ComboBox();
            this._createButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _cprLabel
            // 
            this._cprLabel.AutoSize = true;
            this._cprLabel.Location = new System.Drawing.Point(12, 9);
            this._cprLabel.Name = "_cprLabel";
            this._cprLabel.Size = new System.Drawing.Size(32, 13);
            this._cprLabel.TabIndex = 0;
            this._cprLabel.Text = "CPR:";
            // 
            // _customerLabel
            // 
            this._customerLabel.AutoSize = true;
            this._customerLabel.Location = new System.Drawing.Point(12, 127);
            this._customerLabel.Name = "_customerLabel";
            this._customerLabel.Size = new System.Drawing.Size(70, 13);
            this._customerLabel.TabIndex = 2;
            this._customerLabel.Text = "Kunde Navn:";
            // 
            // _projectPersonIdLabel
            // 
            this._projectPersonIdLabel.AutoSize = true;
            this._projectPersonIdLabel.Location = new System.Drawing.Point(12, 88);
            this._projectPersonIdLabel.Name = "_projectPersonIdLabel";
            this._projectPersonIdLabel.Size = new System.Drawing.Size(91, 13);
            this._projectPersonIdLabel.TabIndex = 3;
            this._projectPersonIdLabel.Text = "Projekt Person Id:";
            // 
            // _projectLabel
            // 
            this._projectLabel.AutoSize = true;
            this._projectLabel.Location = new System.Drawing.Point(12, 48);
            this._projectLabel.Name = "_projectLabel";
            this._projectLabel.Size = new System.Drawing.Size(43, 13);
            this._projectLabel.TabIndex = 4;
            this._projectLabel.Text = "Projekt:";
            // 
            // _cprTextBox
            // 
            this._cprTextBox.Location = new System.Drawing.Point(12, 25);
            this._cprTextBox.MaxLength = 10;
            this._cprTextBox.Name = "_cprTextBox";
            this._cprTextBox.Size = new System.Drawing.Size(68, 20);
            this._cprTextBox.TabIndex = 5;
            this._cprTextBox.TextChanged += new System.EventHandler(this._cprTextBox_TextChanged);
            // 
            // _projectComboBox
            // 
            this._projectComboBox.FormattingEnabled = true;
            this._projectComboBox.Items.AddRange(new object[] {
            "BSG"});
            this._projectComboBox.Location = new System.Drawing.Point(12, 64);
            this._projectComboBox.Name = "_projectComboBox";
            this._projectComboBox.Size = new System.Drawing.Size(121, 21);
            this._projectComboBox.TabIndex = 6;
            // 
            // _projectPersonIdTextBox
            // 
            this._projectPersonIdTextBox.Location = new System.Drawing.Point(12, 104);
            this._projectPersonIdTextBox.Name = "_projectPersonIdTextBox";
            this._projectPersonIdTextBox.Size = new System.Drawing.Size(121, 20);
            this._projectPersonIdTextBox.TabIndex = 7;
            this._projectPersonIdTextBox.TextChanged += new System.EventHandler(this._projectPersonIdTextBox_TextChanged);
            // 
            // _customerComboBox
            // 
            this._customerComboBox.FormattingEnabled = true;
            this._customerComboBox.Items.AddRange(new object[] {
            "SSI",
            "KB",
            "COPSAC"});
            this._customerComboBox.Location = new System.Drawing.Point(12, 143);
            this._customerComboBox.Name = "_customerComboBox";
            this._customerComboBox.Size = new System.Drawing.Size(121, 21);
            this._customerComboBox.TabIndex = 8;
            this._customerComboBox.TextChanged += new System.EventHandler(this._customerComboBox_TextChanged);
            // 
            // _createButton
            // 
            this._createButton.Location = new System.Drawing.Point(58, 170);
            this._createButton.Name = "_createButton";
            this._createButton.Size = new System.Drawing.Size(75, 23);
            this._createButton.TabIndex = 9;
            this._createButton.Text = "Opret";
            this._createButton.UseVisualStyleBackColor = true;
            this._createButton.Click += new System.EventHandler(this._createButton_Click);
            // 
            // AddPatientExtensionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(148, 202);
            this.Controls.Add(this._createButton);
            this.Controls.Add(this._customerComboBox);
            this.Controls.Add(this._projectPersonIdTextBox);
            this.Controls.Add(this._projectComboBox);
            this.Controls.Add(this._cprTextBox);
            this.Controls.Add(this._projectLabel);
            this.Controls.Add(this._projectPersonIdLabel);
            this.Controls.Add(this._customerLabel);
            this.Controls.Add(this._cprLabel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddPatientExtensionForm";
            this.ShowIcon = false;
            this.Text = "Tilføj Person";
            this.Load += new System.EventHandler(this.AddPersonExtensionForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _cprLabel;
        private System.Windows.Forms.Label _customerLabel;
        private System.Windows.Forms.Label _projectPersonIdLabel;
        private System.Windows.Forms.Label _projectLabel;
        private System.Windows.Forms.TextBox _cprTextBox;
        private System.Windows.Forms.ComboBox _projectComboBox;
        private System.Windows.Forms.TextBox _projectPersonIdTextBox;
        private System.Windows.Forms.ComboBox _customerComboBox;
        private System.Windows.Forms.Button _createButton;
    }
}