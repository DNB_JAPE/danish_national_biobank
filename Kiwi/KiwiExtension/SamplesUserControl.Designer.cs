﻿namespace KiwiExtension
{
    partial class SamplesUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._pickSamplesButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this._samplesListBox = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this._selectLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._pickjobIdTextbox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this._continueButton = new System.Windows.Forms.Button();
            this._plateTextBox = new System.Windows.Forms.TextBox();
            this._partitionComboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this._statusLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _pickSamplesButton
            // 
            this._pickSamplesButton.BackgroundImage = global::KiwiExtension.Properties.Resources.Out;
            this._pickSamplesButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._pickSamplesButton.Location = new System.Drawing.Point(241, 113);
            this._pickSamplesButton.Name = "_pickSamplesButton";
            this._pickSamplesButton.Size = new System.Drawing.Size(121, 53);
            this._pickSamplesButton.TabIndex = 5;
            this._pickSamplesButton.Text = "               Pluk";
            this._pickSamplesButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._pickSamplesButton.UseVisualStyleBackColor = true;
            this._pickSamplesButton.Click += new System.EventHandler(this._pickSamplesButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(233, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Plade:";
            // 
            // _samplesListBox
            // 
            this._samplesListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._samplesListBox.FormattingEnabled = true;
            this._samplesListBox.Location = new System.Drawing.Point(6, 29);
            this._samplesListBox.Name = "_samplesListBox";
            this._samplesListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this._samplesListBox.Size = new System.Drawing.Size(221, 355);
            this._samplesListBox.TabIndex = 8;
            this._samplesListBox.SelectedIndexChanged += new System.EventHandler(this._samplesListBox_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Prøver:";
            // 
            // _selectLabel
            // 
            this._selectLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._selectLabel.AutoSize = true;
            this._selectLabel.Location = new System.Drawing.Point(3, 387);
            this._selectLabel.Name = "_selectLabel";
            this._selectLabel.Size = new System.Drawing.Size(60, 13);
            this._selectLabel.TabIndex = 10;
            this._selectLabel.Text = "0 af 0 valgt";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(238, 183);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 90);
            this.label4.TabIndex = 23;
            this.label4.Text = "\'Plade\' skal være udfyldt med stregkoden på en tom plade. Husk at pladen skal hav" +
    "e den rette type i Kiwi.";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(238, 311);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 64);
            this.label3.TabIndex = 24;
            this.label3.Text = "Derefter skal man lave en udtagningsordre på pladen, når man skal bruge den.";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(238, 273);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 38);
            this.label5.TabIndex = 25;
            this.label5.Text = "Efter plukning bliver pladen flyttet til lageret.";
            // 
            // _pickjobIdTextbox
            // 
            this._pickjobIdTextbox.Location = new System.Drawing.Point(395, 87);
            this._pickjobIdTextbox.Name = "_pickjobIdTextbox";
            this._pickjobIdTextbox.Size = new System.Drawing.Size(121, 20);
            this._pickjobIdTextbox.TabIndex = 27;
            this._pickjobIdTextbox.TextChanged += new System.EventHandler(this._pickjobIdTextbox_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(392, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "Pickjob Id:";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // _continueButton
            // 
            this._continueButton.BackgroundImage = global::KiwiExtension.Properties.Resources.Move;
            this._continueButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._continueButton.Location = new System.Drawing.Point(395, 114);
            this._continueButton.Name = "_continueButton";
            this._continueButton.Size = new System.Drawing.Size(121, 53);
            this._continueButton.TabIndex = 29;
            this._continueButton.Text = "              Fortsæt";
            this._continueButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._continueButton.UseVisualStyleBackColor = true;
            this._continueButton.Click += new System.EventHandler(this._continueButton_Click);
            // 
            // _plateTextBox
            // 
            this._plateTextBox.Location = new System.Drawing.Point(241, 87);
            this._plateTextBox.Name = "_plateTextBox";
            this._plateTextBox.Size = new System.Drawing.Size(121, 20);
            this._plateTextBox.TabIndex = 30;
            this._plateTextBox.TextChanged += new System.EventHandler(this._plateTextBox_TextChanged);
            // 
            // _partitionComboBox
            // 
            this._partitionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._partitionComboBox.Enabled = false;
            this._partitionComboBox.FormattingEnabled = true;
            this._partitionComboBox.Location = new System.Drawing.Point(241, 28);
            this._partitionComboBox.Name = "_partitionComboBox";
            this._partitionComboBox.Size = new System.Drawing.Size(158, 21);
            this._partitionComboBox.TabIndex = 32;
            this._partitionComboBox.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Enabled = false;
            this.label7.Location = new System.Drawing.Point(241, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(158, 13);
            this.label7.TabIndex = 31;
            this.label7.Text = "Placering af prøverne (Partition):";
            this.label7.Visible = false;
            // 
            // _statusLabel
            // 
            this._statusLabel.AutoSize = true;
            this._statusLabel.Location = new System.Drawing.Point(326, 387);
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(0, 13);
            this._statusLabel.TabIndex = 33;
            // 
            // SamplesUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._statusLabel);
            this.Controls.Add(this._partitionComboBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this._plateTextBox);
            this.Controls.Add(this._continueButton);
            this.Controls.Add(this.label6);
            this.Controls.Add(this._pickjobIdTextbox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._selectLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._samplesListBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._pickSamplesButton);
            this.Name = "SamplesUserControl";
            this.Size = new System.Drawing.Size(547, 418);
            this.VisibleChanged += new System.EventHandler(this.PickSamplesUserControl_VisibleChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _pickSamplesButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox _samplesListBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label _selectLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox _pickjobIdTextbox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button _continueButton;
        private System.Windows.Forms.TextBox _plateTextBox;
        private System.Windows.Forms.ComboBox _partitionComboBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label _statusLabel;
    }
}
