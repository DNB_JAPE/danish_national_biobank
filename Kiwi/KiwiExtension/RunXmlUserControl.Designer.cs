﻿namespace KiwiExtension
{
    partial class RunXmlUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._responseTextBox = new System.Windows.Forms.TextBox();
            this._xmlTextBox = new System.Windows.Forms.TextBox();
            this._runXmlButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _responseTextBox
            // 
            this._responseTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._responseTextBox.Location = new System.Drawing.Point(237, 13);
            this._responseTextBox.Multiline = true;
            this._responseTextBox.Name = "_responseTextBox";
            this._responseTextBox.ReadOnly = true;
            this._responseTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._responseTextBox.Size = new System.Drawing.Size(298, 389);
            this._responseTextBox.TabIndex = 3;
            this._responseTextBox.WordWrap = false;
            // 
            // _xmlTextBox
            // 
            this._xmlTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._xmlTextBox.Location = new System.Drawing.Point(12, 13);
            this._xmlTextBox.Multiline = true;
            this._xmlTextBox.Name = "_xmlTextBox";
            this._xmlTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._xmlTextBox.Size = new System.Drawing.Size(219, 360);
            this._xmlTextBox.TabIndex = 4;
            // 
            // _runXmlButton
            // 
            this._runXmlButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._runXmlButton.Location = new System.Drawing.Point(145, 379);
            this._runXmlButton.Name = "_runXmlButton";
            this._runXmlButton.Size = new System.Drawing.Size(86, 23);
            this._runXmlButton.TabIndex = 6;
            this._runXmlButton.Text = "Kør Xml";
            this._runXmlButton.UseVisualStyleBackColor = true;
            this._runXmlButton.Click += new System.EventHandler(this._runXmlButton_Click);
            // 
            // RunXmlUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._runXmlButton);
            this.Controls.Add(this._xmlTextBox);
            this.Controls.Add(this._responseTextBox);
            this.Name = "RunXmlUserControl";
            this.Size = new System.Drawing.Size(547, 418);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _responseTextBox;
        private System.Windows.Forms.TextBox _xmlTextBox;
        private System.Windows.Forms.Button _runXmlButton;
    }
}
