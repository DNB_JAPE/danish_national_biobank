﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using DNBTools;
using DNBTools.Kiwi;
using DNBTools.Nautilus;
using DNBTools.DataClasses;

namespace KiwiExtension
{
    public partial class ToolsUserControl : KiwiUserControl
    {
        private List<BiobankEntity> _entities;

        public ToolsUserControl()
        {
            InitializeComponent();
        }

        public void Initialize(List<BiobankEntity> entities, Configuration configuration, LimsCommunication limsCommunication, KiwiCommunication kiwiCommunication)
        {
            base.Initialize(configuration, limsCommunication, kiwiCommunication);
            _entities = entities;
        }

        internal void EnableUI(bool enable)
        {
            _plateLocationComboBox.Enabled = enable;
            _sampleLocationComboBox.Enabled = enable;
            _plateLocationInButton.Enabled = enable;
            _plateLocationOutButton.Enabled = enable;
            _emptyPlatesComboBox.Enabled = enable;
            _emptyPlatesListBox.Enabled = enable;
            Refresh();
        }

        private void ToolsUserControl_VisibleChanged(object sender, EventArgs e)
        {
            if (!DesignMode && _entities != null && _entities.Count > 0)
            {
                _plateLocationComboBox.Items.Clear();
                if (_entities[0] is Plate)
                {
                    foreach (BiobankEntity entity in _entities)
                    {
                        EntityListBoxItem item = new EntityListBoxItem(entity);
                        _plateLocationComboBox.Items.Add(item);
                    }
                }
                _sampleLocationComboBox.Items.Clear();
                if (_entities[0] is Sample)
                {
                    foreach (BiobankEntity entity in _entities)
                    {
                        EntityListBoxItem item = new EntityListBoxItem(entity);
                        _sampleLocationComboBox.Items.Add(item);
                    }
                }
                List<string> partitions = new List<string>();
                partitions.Add("<Ingen Valgt>");
                partitions.AddRange(_configuration.Config05.Partitions);
                partitions.AddRange(_configuration.Config075.Partitions);
                partitions.AddRange(_configuration.Config10.Partitions);
                partitions.AddRange(_configuration.Config14.Partitions);
                partitions.Sort();
                _emptyPlatesComboBox.Items.Clear();
                _emptyPlatesComboBox.Items.AddRange(partitions.Distinct().ToArray());
                _emptyPlatesComboBox.SelectedIndex = 0;
            }
        }

        private void _plateLocationOutButton_Click(object sender, EventArgs e)
        {
            EnableUI(false);
            if (!string.IsNullOrWhiteSpace(_plateLocationComboBox.Text))
            {
                try
                {
                    EntityListBoxItem item = _plateLocationComboBox.SelectedItem as EntityListBoxItem;
                    _limsCommunication.UpdateLocationForCarrier(item.Entity.LogicalId, _limsCommunication.LocationGetId(LimsCommunication.PickedLocationName));
                    _plateLocationComboBox.SelectedIndex = -1;
                }
                catch (Exception ex)
                {
                    SysLog.Log("Fejl under opdatering af Lims: " + ex.Message + Environment.NewLine + ex.StackTrace, LogLevel.Error);
                    MessageBox.Show("Fejl under opdatering af Lims: " + ex.Message + Environment.NewLine + ex.StackTrace);
                }
            }
            EnableUI(true);
        }

        private void _plateLocationInButton_Click(object sender, EventArgs e)
        {
            EnableUI(false);
            if (!string.IsNullOrWhiteSpace(_plateLocationComboBox.Text))
            {
                try
                {
                    EntityListBoxItem item = _plateLocationComboBox.SelectedItem as EntityListBoxItem;
                    _limsCommunication.UpdateLocationForCarrier(item.Entity.LogicalId, _limsCommunication.LocationGetId(LimsCommunication.KiwiLocationName));
                    _plateLocationComboBox.SelectedIndex = -1;
                }
                catch (Exception ex)
                {
                    SysLog.Log("Fejl under opdatering af Lims: " + ex.Message + Environment.NewLine + ex.StackTrace, LogLevel.Error);
                    MessageBox.Show("Fejl under opdatering af Lims: " + ex.Message + Environment.NewLine + ex.StackTrace);
                }
            }
            EnableUI(true);
        }

        private void _sampleLocationInButton_Click(object sender, EventArgs e)
        {
            EnableUI(false);
            if (!string.IsNullOrWhiteSpace(_sampleLocationComboBox.Text))
            {
                try
                {
                    EntityListBoxItem item = _sampleLocationComboBox.SelectedItem as EntityListBoxItem;
                    _limsCommunication.UpdateLocationForSample(item.Entity.LimsId, _limsCommunication.LocationGetId(LimsCommunication.KiwiLocationName));
                    _plateLocationComboBox.SelectedIndex = -1;
                }
                catch (Exception ex)
                {
                    SysLog.Log("Fejl under opdatering af Lims: " + ex.Message + Environment.NewLine + ex.StackTrace, LogLevel.Error);
                    MessageBox.Show("Fejl under opdatering af Lims: " + ex.Message + Environment.NewLine + ex.StackTrace);
                }
            }
            EnableUI(true);
        }

        private void _sampleLocationOutButton_Click(object sender, EventArgs e)
        {
            EnableUI(false);
            if (!string.IsNullOrWhiteSpace(_sampleLocationComboBox.Text))
            {
                try
                {
                    EntityListBoxItem item = _sampleLocationComboBox.SelectedItem as EntityListBoxItem;
                    _limsCommunication.UpdateLocationForSample(item.Entity.LimsId, _limsCommunication.LocationGetId(LimsCommunication.PickedLocationName));
                    _sampleLocationComboBox.SelectedIndex = -1;
                }
                catch (Exception ex)
                {
                    SysLog.Log("Fejl under opdatering af Lims: " + ex.Message + Environment.NewLine + ex.StackTrace, LogLevel.Error);
                    MessageBox.Show("Fejl under opdatering af Lims: " + ex.Message + Environment.NewLine + ex.StackTrace);
                }
            }
            EnableUI(true);
        }

        private void _emptyPlatesComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_emptyPlatesComboBox.Text == "<Ingen Valgt>")
            {
                return;
            }
            else
            {
                EnableUI(false);
                _emptyPlatesListBox.Items.Clear();
                KiwiCommunication kiwiCommunication;
                if (_configuration.Config05.Partitions.Contains(_emptyPlatesComboBox.Text))
                {
                    kiwiCommunication = new KiwiCommunication(_configuration.Config05.StoreName, _configuration.Config05.StoreAddress, _configuration.Config05.StorePort);
                }
                else if (_configuration.Config075.Partitions.Contains(_emptyPlatesComboBox.Text))
                {
                    kiwiCommunication = new KiwiCommunication(_configuration.Config075.StoreName, _configuration.Config075.StoreAddress, _configuration.Config075.StorePort);
                }
                else if (_configuration.Config10.Partitions.Contains(_emptyPlatesComboBox.Text))
                {
                    kiwiCommunication = new KiwiCommunication(_configuration.Config10.StoreName, _configuration.Config10.StoreAddress, _configuration.Config10.StorePort);
                }
                else
                {
                    kiwiCommunication = new KiwiCommunication(_configuration.Config14.StoreName, _configuration.Config14.StoreAddress, _configuration.Config14.StorePort);
                }
                Tuple<string, string> response = kiwiCommunication.GetPartitionPlatesList(KiwiEntityExtension.OrderId++, _configuration.OperatorKiwiName, _emptyPlatesComboBox.Text);
                Tuple<string, string> status = kiwiCommunication.StatusFromResponse(response.Item2);
                if (status.Item1 == "ERR")
                {
                    MessageBox.Show("Fejl under udstedelse af ordre (GetPartitionPlatesList): " + status.Item2);
                    EnableUI(true);
                    return;
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(response.Item2);
                    XmlNodeList plateNodes = doc.SelectNodes("/STXRequest/Plates");
                    List<string> barcodes = new List<string>();
                    foreach (XmlNode plateNode in plateNodes)
                    {
                        string barcode = plateNode.SelectSingleNode("PltBCR").InnerXml;
                        if (!String.IsNullOrWhiteSpace(barcode))
                        {
                            barcodes.Add(barcode);
                        }
                    }
                    if (barcodes.Count > 0)
                    {
                        foreach (string barcode in barcodes)
                        {
                            Tuple<string, string> plateResponse = kiwiCommunication.GetPlateInfo(KiwiEntityExtension.OrderId++, _configuration.OperatorKiwiName, barcode);
                            Tuple<string, string> plateStatus = kiwiCommunication.StatusFromResponse(plateResponse.Item2);
                            if (status.Item1 == "ERR")
                            {
                                MessageBox.Show("Fejl under udstedelse af ordre (GetPlateInfo): " + status.Item2);
                                EnableUI(true);
                                return;
                            }
                            else
                            {
                                if (!plateResponse.Item2.Contains("TbBCR"))
                                {
                                    _emptyPlatesListBox.Items.Add(barcode);
                                }
                            }
                        }
                    }
                    if (_emptyPlatesListBox.Items.Count==0)
                    {
                        _emptyPlatesListBox.Items.Add("<Ingen>");
                    }
                }
                EnableUI(true);
            }
        }
    }
}
