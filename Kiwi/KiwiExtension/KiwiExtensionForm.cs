﻿using DNBTools;
using DNBTools.DataClasses;
using DNBTools.Kiwi;
using DNBTools.Nautilus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KiwiExtension
{
    public partial class KiwiExtensionForm : Form
    {
        private List<BiobankEntity> _entities;
        private KiwiCommunication _kiwiCommunication;
        private LimsCommunication _limsCommunication;
        private Configuration _configuration;

        public KiwiExtensionForm(List<BiobankEntity> entities, KiwiCommunication kiwiCommunication, LimsCommunication limsCommunication, Configuration configuration)
        {
            InitializeComponent();
            _entities = entities;
            _kiwiCommunication = kiwiCommunication;
            _limsCommunication = limsCommunication;
            _configuration = configuration;
        }

        private void KiwiExtensionForm_Load(object sender, EventArgs e)
        {
            _toolsUserControl.Initialize(_entities, _configuration, _limsCommunication, _kiwiCommunication);
            //_infoUserControl.Initialize(_configuration, _limsCommunication, _kiwiCommunication);
            //_consolidateUserControl.Initialize(_configuration, _limsCommunication, _kiwiCommunication);
            _inventoryUserControl.Initialize(_configuration, _limsCommunication, _kiwiCommunication);
            _runXmlUserControl.Initialize(_configuration, _limsCommunication, _kiwiCommunication);
            _nautilusUserControl.Initialize(_configuration, _limsCommunication, _kiwiCommunication);
            if (_entities[0] is Plate)
            {
                _platesUserControl.Initialize(_entities, _configuration, _limsCommunication, _kiwiCommunication);
                _tabControl.SelectedTab = _platesTabPage;
                _samplesUserControl.EnableUI(false);
                SysLog.Log("Plate Usercontrol started", LogLevel.Info);
            }
            else
            {
                _samplesUserControl.Initialize(_entities, _configuration, _limsCommunication, _kiwiCommunication);
                _tabControl.SelectedTab = _samplesTabPage;
                _platesUserControl.EnableUI(false);
                SysLog.Log("Sample Usercontrol started", LogLevel.Info);
            }
            if(!_configuration.Mode.Equals("Development"))
            {
            //    _infoUserControl.EnableUI(false);
            //    _consolidateUserControl.EnableUI(false);
                _tabControl.TabPages.Remove(_runXmlUserTabPage);
            }
            
        }
    }
}
