﻿namespace KiwiExtension
{
    partial class NautilusUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._updateButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this._bothListBox = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this._kiwiListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this._nautilusListBox = new System.Windows.Forms.ListBox();
            this._stateLabel = new System.Windows.Forms.Label();
            this._nautilusCountLabel = new System.Windows.Forms.Label();
            this._kiwiCountLabel = new System.Windows.Forms.Label();
            this._bothCountLabel = new System.Windows.Forms.Label();
            this._allRadioButton = new System.Windows.Forms.RadioButton();
            this._differentRadioButton = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // _updateButton
            // 
            this._updateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._updateButton.Location = new System.Drawing.Point(460, 383);
            this._updateButton.Name = "_updateButton";
            this._updateButton.Size = new System.Drawing.Size(75, 23);
            this._updateButton.TabIndex = 6;
            this._updateButton.Text = "Opdater";
            this._updateButton.UseVisualStyleBackColor = true;
            this._updateButton.Click += new System.EventHandler(this._updateButton_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(260, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 31);
            this.label3.TabIndex = 5;
            this.label3.Text = "Plader som begge systemer tror er i Kiwi:";
            // 
            // _bothListBox
            // 
            this._bothListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._bothListBox.FormattingEnabled = true;
            this._bothListBox.Location = new System.Drawing.Point(260, 54);
            this._bothListBox.Name = "_bothListBox";
            this._bothListBox.Size = new System.Drawing.Size(120, 303);
            this._bothListBox.TabIndex = 4;
            this._bothListBox.SelectedIndexChanged += new System.EventHandler(this._bothListBox_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(133, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 39);
            this.label2.TabIndex = 3;
            this.label2.Text = "Plader som kun Kiwi tror er i Kiwi:";
            // 
            // _kiwiListBox
            // 
            this._kiwiListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._kiwiListBox.FormattingEnabled = true;
            this._kiwiListBox.Location = new System.Drawing.Point(133, 54);
            this._kiwiListBox.Name = "_kiwiListBox";
            this._kiwiListBox.Size = new System.Drawing.Size(120, 303);
            this._kiwiListBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 31);
            this.label1.TabIndex = 1;
            this.label1.Text = "Plader som kun Nautilus tror er i Kiwi:";
            // 
            // _nautilusListBox
            // 
            this._nautilusListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._nautilusListBox.FormattingEnabled = true;
            this._nautilusListBox.Location = new System.Drawing.Point(3, 54);
            this._nautilusListBox.Name = "_nautilusListBox";
            this._nautilusListBox.Size = new System.Drawing.Size(120, 303);
            this._nautilusListBox.TabIndex = 0;
            // 
            // _stateLabel
            // 
            this._stateLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._stateLabel.AutoSize = true;
            this._stateLabel.Location = new System.Drawing.Point(3, 388);
            this._stateLabel.Name = "_stateLabel";
            this._stateLabel.Size = new System.Drawing.Size(28, 13);
            this._stateLabel.TabIndex = 7;
            this._stateLabel.Text = "Klar:";
            // 
            // _nautilusCountLabel
            // 
            this._nautilusCountLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._nautilusCountLabel.AutoSize = true;
            this._nautilusCountLabel.Location = new System.Drawing.Point(3, 360);
            this._nautilusCountLabel.Name = "_nautilusCountLabel";
            this._nautilusCountLabel.Size = new System.Drawing.Size(34, 13);
            this._nautilusCountLabel.TabIndex = 8;
            this._nautilusCountLabel.Text = "Antal:";
            // 
            // _kiwiCountLabel
            // 
            this._kiwiCountLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._kiwiCountLabel.AutoSize = true;
            this._kiwiCountLabel.Location = new System.Drawing.Point(133, 360);
            this._kiwiCountLabel.Name = "_kiwiCountLabel";
            this._kiwiCountLabel.Size = new System.Drawing.Size(34, 13);
            this._kiwiCountLabel.TabIndex = 9;
            this._kiwiCountLabel.Text = "Antal:";
            // 
            // _bothCountLabel
            // 
            this._bothCountLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._bothCountLabel.AutoSize = true;
            this._bothCountLabel.Location = new System.Drawing.Point(260, 360);
            this._bothCountLabel.Name = "_bothCountLabel";
            this._bothCountLabel.Size = new System.Drawing.Size(34, 13);
            this._bothCountLabel.TabIndex = 10;
            this._bothCountLabel.Text = "Antal:";
            // 
            // _allRadioButton
            // 
            this._allRadioButton.AutoSize = true;
            this._allRadioButton.Checked = true;
            this._allRadioButton.Location = new System.Drawing.Point(386, 54);
            this._allRadioButton.Name = "_allRadioButton";
            this._allRadioButton.Size = new System.Drawing.Size(74, 17);
            this._allRadioButton.TabIndex = 11;
            this._allRadioButton.TabStop = true;
            this._allRadioButton.Text = "Alle plader";
            this._allRadioButton.UseVisualStyleBackColor = true;
            // 
            // _differentRadioButton
            // 
            this._differentRadioButton.AutoSize = true;
            this._differentRadioButton.Location = new System.Drawing.Point(386, 77);
            this._differentRadioButton.Name = "_differentRadioButton";
            this._differentRadioButton.Size = new System.Drawing.Size(158, 17);
            this._differentRadioButton.TabIndex = 12;
            this._differentRadioButton.Text = "Forskellige i Kiwi og Nautilus";
            this._differentRadioButton.UseVisualStyleBackColor = true;
            this._differentRadioButton.CheckedChanged += new System.EventHandler(this._differentRadioButton_CheckedChanged);
            // 
            // NautilusUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._differentRadioButton);
            this.Controls.Add(this._allRadioButton);
            this.Controls.Add(this._bothCountLabel);
            this.Controls.Add(this._kiwiCountLabel);
            this.Controls.Add(this._nautilusCountLabel);
            this.Controls.Add(this._stateLabel);
            this.Controls.Add(this._updateButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._bothListBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._kiwiListBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._nautilusListBox);
            this.Name = "NautilusUserControl";
            this.Size = new System.Drawing.Size(547, 418);
            this.VisibleChanged += new System.EventHandler(this.NautilusUserControl_VisibleChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox _nautilusListBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox _kiwiListBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox _bothListBox;
        private System.Windows.Forms.Button _updateButton;
        private System.Windows.Forms.Label _stateLabel;
        private System.Windows.Forms.Label _nautilusCountLabel;
        private System.Windows.Forms.Label _kiwiCountLabel;
        private System.Windows.Forms.Label _bothCountLabel;
        private System.Windows.Forms.RadioButton _allRadioButton;
        private System.Windows.Forms.RadioButton _differentRadioButton;

    }
}
