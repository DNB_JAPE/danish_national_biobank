﻿namespace KiwiExtension
{
    partial class ToolsUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._plateLocationInButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this._plateLocationOutButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._plateLocationComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._emptyPlatesComboBox = new System.Windows.Forms.ComboBox();
            this._emptyPlatesListBox = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._sampleLocationComboBox = new System.Windows.Forms.ComboBox();
            this._sampleLocationOutButton = new System.Windows.Forms.Button();
            this._sampleLocationInButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // _plateLocationInButton
            // 
            this._plateLocationInButton.BackgroundImage = global::KiwiExtension.Properties.Resources.In;
            this._plateLocationInButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._plateLocationInButton.Location = new System.Drawing.Point(265, 19);
            this._plateLocationInButton.Name = "_plateLocationInButton";
            this._plateLocationInButton.Size = new System.Drawing.Size(121, 53);
            this._plateLocationInButton.TabIndex = 0;
            this._plateLocationInButton.Text = "          Indsæt";
            this._plateLocationInButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._plateLocationInButton.UseVisualStyleBackColor = true;
            this._plateLocationInButton.Click += new System.EventHandler(this._plateLocationInButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Plade stregkode";
            // 
            // _plateLocationOutButton
            // 
            this._plateLocationOutButton.BackgroundImage = global::KiwiExtension.Properties.Resources.Out;
            this._plateLocationOutButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._plateLocationOutButton.Location = new System.Drawing.Point(392, 19);
            this._plateLocationOutButton.Name = "_plateLocationOutButton";
            this._plateLocationOutButton.Size = new System.Drawing.Size(121, 53);
            this._plateLocationOutButton.TabIndex = 2;
            this._plateLocationOutButton.Text = "          Udtag";
            this._plateLocationOutButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._plateLocationOutButton.UseVisualStyleBackColor = true;
            this._plateLocationOutButton.Click += new System.EventHandler(this._plateLocationOutButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this._plateLocationComboBox);
            this.groupBox1.Controls.Add(this._plateLocationOutButton);
            this.groupBox1.Controls.Add(this._plateLocationInButton);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(544, 80);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Opdater Placering i Lims - Plader";
            // 
            // _plateLocationComboBox
            // 
            this._plateLocationComboBox.FormattingEnabled = true;
            this._plateLocationComboBox.Location = new System.Drawing.Point(6, 41);
            this._plateLocationComboBox.Name = "_plateLocationComboBox";
            this._plateLocationComboBox.Size = new System.Drawing.Size(124, 21);
            this._plateLocationComboBox.TabIndex = 6;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this._emptyPlatesComboBox);
            this.groupBox2.Controls.Add(this._emptyPlatesListBox);
            this.groupBox2.Location = new System.Drawing.Point(1, 172);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(544, 179);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tomme Plader";
            // 
            // _emptyPlatesComboBox
            // 
            this._emptyPlatesComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._emptyPlatesComboBox.FormattingEnabled = true;
            this._emptyPlatesComboBox.Location = new System.Drawing.Point(6, 18);
            this._emptyPlatesComboBox.Name = "_emptyPlatesComboBox";
            this._emptyPlatesComboBox.Size = new System.Drawing.Size(124, 21);
            this._emptyPlatesComboBox.TabIndex = 5;
            this._emptyPlatesComboBox.SelectedIndexChanged += new System.EventHandler(this._emptyPlatesComboBox_SelectedIndexChanged);
            // 
            // _emptyPlatesListBox
            // 
            this._emptyPlatesListBox.FormattingEnabled = true;
            this._emptyPlatesListBox.Location = new System.Drawing.Point(6, 45);
            this._emptyPlatesListBox.Name = "_emptyPlatesListBox";
            this._emptyPlatesListBox.Size = new System.Drawing.Size(124, 121);
            this._emptyPlatesListBox.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this._sampleLocationComboBox);
            this.groupBox3.Controls.Add(this._sampleLocationOutButton);
            this.groupBox3.Controls.Add(this._sampleLocationInButton);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Location = new System.Drawing.Point(0, 86);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(544, 80);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Opdater Placering i Lims - Prøver";
            // 
            // _sampleLocationComboBox
            // 
            this._sampleLocationComboBox.FormattingEnabled = true;
            this._sampleLocationComboBox.Location = new System.Drawing.Point(6, 41);
            this._sampleLocationComboBox.Name = "_sampleLocationComboBox";
            this._sampleLocationComboBox.Size = new System.Drawing.Size(124, 21);
            this._sampleLocationComboBox.TabIndex = 6;
            // 
            // _sampleLocationOutButton
            // 
            this._sampleLocationOutButton.BackgroundImage = global::KiwiExtension.Properties.Resources.Out;
            this._sampleLocationOutButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._sampleLocationOutButton.Location = new System.Drawing.Point(392, 19);
            this._sampleLocationOutButton.Name = "_sampleLocationOutButton";
            this._sampleLocationOutButton.Size = new System.Drawing.Size(121, 53);
            this._sampleLocationOutButton.TabIndex = 2;
            this._sampleLocationOutButton.Text = "          Udtag";
            this._sampleLocationOutButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._sampleLocationOutButton.UseVisualStyleBackColor = true;
            this._sampleLocationOutButton.Click += new System.EventHandler(this._sampleLocationOutButton_Click);
            // 
            // _sampleLocationInButton
            // 
            this._sampleLocationInButton.BackgroundImage = global::KiwiExtension.Properties.Resources.In;
            this._sampleLocationInButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._sampleLocationInButton.Location = new System.Drawing.Point(265, 19);
            this._sampleLocationInButton.Name = "_sampleLocationInButton";
            this._sampleLocationInButton.Size = new System.Drawing.Size(121, 53);
            this._sampleLocationInButton.TabIndex = 0;
            this._sampleLocationInButton.Text = "          Indsæt";
            this._sampleLocationInButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._sampleLocationInButton.UseVisualStyleBackColor = true;
            this._sampleLocationInButton.Click += new System.EventHandler(this._sampleLocationInButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Prøve stregkode";
            // 
            // ToolsUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "ToolsUserControl";
            this.Size = new System.Drawing.Size(547, 418);
            this.VisibleChanged += new System.EventHandler(this.ToolsUserControl_VisibleChanged);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _plateLocationInButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _plateLocationOutButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox _emptyPlatesListBox;
        private System.Windows.Forms.ComboBox _emptyPlatesComboBox;
        private System.Windows.Forms.ComboBox _plateLocationComboBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox _sampleLocationComboBox;
        private System.Windows.Forms.Button _sampleLocationOutButton;
        private System.Windows.Forms.Button _sampleLocationInButton;
        private System.Windows.Forms.Label label2;
    }
}
