﻿using DNBTools;
using DNBTools.DataClasses;
using DNBTools.Kiwi;
using DNBTools.Nautilus;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Security.Principal;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace KiwiExtension
{
    public partial class SamplesUserControl : KiwiUserControl
    {
        private List<BiobankEntity> _samples;

        public SamplesUserControl()
        {
            InitializeComponent();
            SysLog.EnableLog();
        }

        public void Initialize(List<BiobankEntity> samples, Configuration configuration, LimsCommunication limsCommunication, KiwiCommunication kiwiCommunication)
        {
            base.Initialize(configuration, limsCommunication, kiwiCommunication);
            _samples = samples;
            EnableUI(_samples.Count > 0);
        }

        /// <summary>
        /// Enables/disables the entire UI.
        /// </summary>
        /// <param name="enable"></param>
        internal void EnableUI(bool enable)
        {
            _samplesListBox.Enabled = enable;
            _partitionComboBox.Enabled = enable;
            _plateTextBox.Enabled = enable;
            _pickSamplesButton.Enabled = enable;
            _selectLabel.Enabled = enable;
            label1.Enabled = enable;
            label2.Enabled = enable;
            label3.Enabled = enable;
            label4.Enabled = enable;
            label5.Enabled = enable;
            Refresh();
        }

        /// <summary>
        /// Is called whenever the tab is visible.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PickSamplesUserControl_VisibleChanged(object sender, EventArgs e)
        {
            if (!this.DesignMode && _samples != null)
            {
                _samplesListBox.Items.Clear();
                if (_samples.Count > 0)
                {
                    foreach (BiobankEntity sample in _samples)
                    {
                        _samplesListBox.Items.Add(sample.LogicalId);
                    }
                }
                _selectLabel.Text = _samplesListBox.SelectedItems.Count + " af " + _samplesListBox.Items.Count + " valgt";
                _plateTextBox.Text = "";
                _pickSamplesButton.Enabled = false;
                for (int i = 0; i < _samplesListBox.Items.Count; i++)
                {
                    _samplesListBox.SetSelected(i, true);
                }
                //_partitionComboBox.Items.Clear();
                //_partitionComboBox.Items.AddRange(_configuration.DefaultConfig.Partitions);
                //_partitionComboBox.SelectedIndex = 0;
            }
        }

        private void _samplesListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateState();
        }
 
        private void UpdateState()
        {
            //if (_samplesListBox.SelectedItems.Count == 0 || _samplesListBox.SelectedItems.Count > 96 || _plateTextBox.Text == string.Empty)
            //if (_samplesListBox.SelectedItems.Count == 0 || _samplesListBox.SelectedItems.Count > 96 )  // ESAT 20181119
            if (_samplesListBox.SelectedItems.Count == 0 )  // ESAT 20190429
            {
                _pickSamplesButton.Enabled = false;
            }
            else
            {
                _pickSamplesButton.Enabled = true;
            }
            _selectLabel.Text = _samplesListBox.SelectedItems.Count + " af " + _samplesListBox.Items.Count + " valgt";
            Refresh();
        }

        private void _pickSamplesButton_Click(object sender, EventArgs e)
        {
            SysLog.Log("Picking samples from Kiwi", LogLevel.Info);
            if (_plateTextBox.Text.Trim() != string.Empty)  // ESAT 20181119
            {
                if (!IsPlateEmpty(_plateTextBox.Text))
                {
                    SysLog.Log("Pladen er ikke tom eller ukendt af Kiwi!" + Environment.NewLine + "Det er pt.ikke muligt, at plukke til ikke - tomme plader." + Environment.NewLine + "Funktionaliteten er på vej: -)", LogLevel.Warning);
                    //MessageBox.Show("Pladen er ikke tom eller ukendt af Kiwi!" + Environment.NewLine + "Det er pt. ikke muligt, at plukke til ikke-tomme plader." + Environment.NewLine + "Funktionaliteten er på vej :-)");
                    DialogResult res = MessageBox.Show("Pladen er ikke tom eller ukendt af Kiwi!" + Environment.NewLine + "Det er pt. ikke muligt, at plukke til ikke-tomme plader." + Environment.NewLine + "Skal vi prøve alligevel?", "Kiwi", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    if (res == DialogResult.No)
                        return;
                }
            }

            using (new WaitCursor()) 
            {
                EnableUI(false);
                try
                {
                    string selectedPlate = _plateTextBox.Text.Trim();
                    List<string> barcodes = GetBarcodes();

                    int orderId = KiwiEntityExtension.OrderId++;
                    _pickjobIdTextbox.Text = orderId.ToString();

                    Tuple<string, string> response = PickTheTubes(orderId, barcodes, selectedPlate);

                    bool justContinue = true;
                    while (justContinue)
                    {
                        Tuple<string, string> status = _kiwiCommunication.StatusFromResponse(response.Item2);
                        while (status.Item1 == "RUN" || status.Item1 == "WAIT")
                        {
                            Thread.Sleep(2000);
                            response = _kiwiCommunication.GetCmdStatus(KiwiEntityExtension.OrderId++, _configuration.OperatorKiwiName, orderId);
                            status = _kiwiCommunication.StatusFromResponse(response.Item2);
                        }
                        if (status.Item1 == "ERR")
                        {
                            SysLog.Log("Fejl under udstedelse af ordre: " + status.Item2, LogLevel.Warning);
                            DialogResult res = MessageBox.Show(string.Format("Fejl under udstedelse af ordre: {0} --- Ret fejlen og tryk på Gentag eller Afbryd hvis det ikke kan lade sig gøre at rette fejlen.", status.Item2),
                                "Fejl hos Kiwi", MessageBoxButtons.RetryCancel);
                            if (res == DialogResult.Cancel)
                            {
                                EnableUI(true);
                                return;
                            }
                            else
                            {
                                response = _kiwiCommunication.Continue(orderId.ToString(), _configuration.OperatorKiwiName);
                            }
                        }
                        else
                        {
                            justContinue = false;
                        }
                    }
                    UpdateNautilus myNautilus = new UpdateNautilus(_limsCommunication, _kiwiCommunication);
                    myNautilus.FilePath = _configuration.NautilusFilePath;

                    string kiwiSelectedPlate = string.Empty;
                    //string tubeType = string.Empty;
                    Tuple<string, int> position = null;
                    foreach (string barcode in barcodes)
                    {
                        if (selectedPlate == string.Empty)
                        {
                            Tuple<string, string> responseTube = _kiwiCommunication.GetTubeInfo(KiwiEntityExtension.OrderId++, _configuration.OperatorKiwiName, barcode);
                            if (responseTube.Item1 == "ERR")
                            {
                                SysLog.Log("Fejl under tjek af plade info: " + responseTube.Item2, LogLevel.Warning);
                                MessageBox.Show("Fejl under tjek af plade info: " + responseTube.Item2);
                                EnableUI(true);
                                return;
                            }
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(responseTube.Item2);
                            XmlNode node = doc.SelectSingleNode("/STXRequest/Tubes/Plate/PltBCR");
                            kiwiSelectedPlate = node.InnerText.Trim();

                            SysLog.Log(string.Format("Kiwi found plate {0} for {1}: ", kiwiSelectedPlate, barcode), LogLevel.Info);
                        }
                        myNautilus.PlateBC = selectedPlate == string.Empty ? kiwiSelectedPlate : selectedPlate;
                        
                        position = DNBTools.DNBTools.NextPositionMatrix(position);
                        if (position == null)
                        {
                            SysLog.Log("Ugyldig position efter " + position.Item1 + "," + position.Item2, LogLevel.Warning);
                            MessageBox.Show("Ugyldig position efter " + position.Item1 + "," + position.Item2);
                            break; // To continue with the data that allready is collected.
                            //EnableUI(true);
                            //return;
                        }
                        try
                        {
                            foreach (BiobankEntity sample in _samples)
                            {
                                if(sample.LogicalId == barcode)
                                {
                                    SysLog.Log("Set Aliquot in position " + position.Item1 + ", " + position.Item2 + " on plate: " + myNautilus.PlateBC, LogLevel.Info);
                                    _statusLabel.Text = "Set Aliquot in position " + position.Item1 + ", " + position.Item2;
                                    // Need to do it by file because the SP doesn't add tracking to sample.
                                    //_limsCommunication.MoveSampleToCarrier(sample.LimsId, selectedPlate == string.Empty ? kiwiSelectedPlate : selectedPlate, position.Item1, position.Item2.ToString());
                                    myNautilus.Content.Add(sample);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            SysLog.Log("Fejl under opdatering af Lims: " + ex.Message + Environment.NewLine + ex.StackTrace, LogLevel.Error);
                            MessageBox.Show("Fejl under opdatering af Lims: " + ex.Message + Environment.NewLine + ex.StackTrace);
                            break; // To continue with the data that allready is collected.

                            //EnableUI(true);
                            //return;
                        }
                        
                    }
                    if (!myNautilus.BuildNautilusImportFile())
                    {
                        // TODO: What to do if fails?
                        MessageBox.Show("Kunne ikke oprette fil til Lims");
                    }

                    DialogResult resPlate = MessageBox.Show(string.Format("Prøverne er flyttet over på plade: {0} Skal jeg trække pladen ud nu?", myNautilus.PlateBC),
                                "Færdig med plukning", MessageBoxButtons.YesNo);
                    if (resPlate == DialogResult.Yes)
                    {
                        try
                        {
                            // Lookup samples in Kiwi
                            _statusLabel.Text = "Udtager '" + myNautilus.PlateBC + "'";
                            Refresh();
                            orderId = KiwiEntityExtension.OrderId++;
                            Tuple<string, string> response1 = _kiwiCommunication.RetrievePlate(orderId, _configuration.OperatorKiwiName, _configuration.DefaultConfig.TransferStation, _configuration.DefaultConfig.ContainerTypeId, myNautilus.PlateBC);
                            Tuple<string, string> status = _kiwiCommunication.StatusFromResponse(response1.Item2);
                            while (status.Item1 == "RUN" || status.Item1 == "WAIT")
                            {
                                Thread.Sleep(2000);
                                response = _kiwiCommunication.GetCmdStatus(KiwiEntityExtension.OrderId++, _configuration.OperatorKiwiName, orderId);
                                status = _kiwiCommunication.StatusFromResponse(response1.Item2);
                            }
                            if (status.Item1 == "ERR")
                            {
                                SysLog.Log(String.Format("Kiwi Error during retrieval: {0}", status.Item2), LogLevel.Warning);
                                MessageBox.Show("Fejl under udtagning: " + status.Item2);
                                EnableUI(true);
                                return;
                            }
                            else
                            {
                                try
                                {
                                    string plateID = _limsCommunication.PlateGetId(myNautilus.PlateBC);

                                    if (plateID != null && plateID != string.Empty)
                                        _limsCommunication.UpdateLocationForCarrier(plateID, _limsCommunication.LocationGetId(LimsCommunication.PickedLocationName));
                                    else
                                    {
                                        MessageBox.Show("Kunne ikke opdatere pladens lokation da jeg ikke kunne finde pladens ID.");
                                        SysLog.Log(String.Format("Kunne ikke opdatere pladens ({0}) lokation da jeg ikke kunne finde pladens ID.", myNautilus.PlateBC), LogLevel.Warning);
                                    }
                                    }
                                catch (Exception ex)
                                {
                                    SysLog.Log(String.Format("Kiwi Error during nautilus update: {0}", ex.Message + Environment.NewLine + ex.StackTrace), LogLevel.Warning);
                                    MessageBox.Show("Fejl under opdatering af Nautilus: " + ex.Message + Environment.NewLine + ex.StackTrace);
                                    EnableUI(true);
                                    return;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            SysLog.Log(String.Format("Error during nautilus update: {0}", ex.Message + Environment.NewLine + ex.StackTrace), LogLevel.Warning);
                            MessageBox.Show("Fejl i ordre: " + ex.Message + Environment.NewLine + ex.StackTrace);
                            EnableUI(true);
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    SysLog.Log("Fejl i ordre: " + ex.Message + Environment.NewLine + ex.StackTrace, LogLevel.Error);
                    MessageBox.Show("Fejl i ordre: " + ex.Message + Environment.NewLine + ex.StackTrace);
                }
                EnableUI(true);
                return;
            }
        }

        private void _plateTextBox_TextChanged(object sender, EventArgs e)
        {
            UpdateState();
        }

        private bool IsPlateEmpty(string barcode)
        {
            Tuple<string, string> response = _kiwiCommunication.GetPlateInfo(KiwiEntityExtension.OrderId++, _configuration.OperatorKiwiName, barcode);
            if (response.Item2.Contains("<Status>ERR</Status>") || response.Item2.Contains("TbBCR"))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void _continueButton_Click(object sender, EventArgs e)
        {
            _kiwiCommunication.Continue(_pickjobIdTextbox.Text, _configuration.OperatorKiwiName);
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void _pickjobIdTextbox_TextChanged(object sender, EventArgs e)
        {

        }

        public Tuple<string, string> PickTheTubes(int orderId, List<string> barcodes, string selectedPlate)
        {
            Tuple<string, string> response;
            // ESAT 20181119
            // TODO how to get info on plate that Kiwi has choosen??
            if (selectedPlate == string.Empty)
                response = _kiwiCommunication.PickTubes(orderId, _configuration.OperatorKiwiName, barcodes); // ESAT 20181119
            else
                response = _kiwiCommunication.PickTubesToPlate(orderId, _configuration.OperatorKiwiName, barcodes, selectedPlate);
            return response;
        }

        public List<string> GetBarcodes()
        {
            //List<string> barcodes = new List<string>();
            //foreach (string barcode in _samplesListBox.SelectedItems)
            //{
            //    SysLog.Log(string.Format("Sample: {0} shall be picked", barcode), LogLevel.Info);
            //    barcodes.Add(barcode);
            //}

            List<string> barcodes = new List<string>();
            foreach (string barcode in _samplesListBox.SelectedItems)
            {
                SysLog.Log(string.Format("Sample: {0} shall be picked", barcode), LogLevel.Info);
                barcodes.Add(barcode);
            }

            return barcodes;
        }
    }

    public class UpdateNautilus
    {
        LimsCommunication myLimsCom;
        KiwiCommunication myKiwiCom;

        public UpdateNautilus(LimsCommunication limsCommunication, KiwiCommunication kiwiCommunication)
        {
            myLimsCom = limsCommunication;
            myKiwiCom = kiwiCommunication;
            Content = new List<BiobankEntity>();
        }

        public UpdateNautilus(string PlateBarcode, LimsCommunication limsCommunication, KiwiCommunication kiwiCommunication)
        {
            PlateBC = PlateBarcode;
            myLimsCom = limsCommunication;
            myKiwiCom = kiwiCommunication;
            Content = new List<BiobankEntity>();
        }

        string fileHeader = "RecordId,TRackBC,Tube_Type,TPositionId,TPositionBC,TStatusSummary,TSumStateDescription,TVolume,SRackBC,Sample_Type,SPositionId,SPositionBC,ActionDateTime,UserName,Location_Id,Description";

        public string FileHeader { get { return fileHeader; } set { fileHeader = value; } }

        public string TubeType { get; set; }

        public List<BiobankEntity> Content { get; set; }

        public string PlateBC { get; set; }

        public string UserName { get; set; }

        // TODO FIlename and path
        // projectname + PmatrixT2019-02-06_15_00_00RLH5Cmove075_(MR10013908).csv
        public string FilePath { get; set; }

        public bool BuildNautilusImportFile()
        {
            bool l_Reply = false;
            try
            {
                // @"\\srv-dnb-app03\LhopFiles\PROJECTS\Nautilus\Edit_Plate_ExistingAliquots\Test\KiwiTransfer_" + PlateBC + ".csv";

                string FileName = FilePath + "KiwiTransferPmatrixT"+ DateTime.Now.ToString("yyyy-MM-dd_HH_mm_ss") +"RKiwiCpicking.csv";

//                string tubeType = myCom.SampleGetContainerType;//TODO
                string TStatusSummary = "0";
                string TSumStateDescription = "Correct pipetting";
                //string TVolume = string.Empty;
                string SRackBC = "----------";
                string Sample_Type = string.Empty; // OK
                string SPositionId = "----------";
                string SPositionBC = "----------";
                string ActionDateTime = DateTime.Now.ToString("dd-MM-yyyy") + " 00:00:00";
                char[] limit={'\\'};
                string[] _uName= (WindowsIdentity.GetCurrent().Name).Split(limit);

                string UserName = _uName[1];
                
                //string Location_Id = myCom.GetTubeLocationName(//     string.Empty;
                string Description = "Picked by Kiwi";

                using (StreamWriter saveFile = File.AppendText(FileName))
                {
                    saveFile.WriteLine(FileHeader);
                    int x = 1;
                    string line = "";

                    foreach (BiobankEntity biobankEntity in Content)
                    {
                        //Sample limsSample = new Sample(biobankEntity.LimsId, biobankEntity.LogicalId);
                        Sample limsSample = myLimsCom.SampleGet(biobankEntity.LimsId);

                        string TPositionId = GetPosition(limsSample.LogicalId);
                        string Location_Id = myLimsCom.LocationGetId(myLimsCom.GetTubeLocationName(biobankEntity.LimsId)); // TODO Kiwi
                        string tubeType = myLimsCom.SampleGetContainerTypeID(limsSample.LimsId);
                        line = "";
                        line = line + "\"" + x + "\",";
                        line = line + "\"" + PlateBC + "\",";
                        line = line + "\"" + tubeType + "\",";
                        line = line + "\"" + TPositionId + "\",";
                        line = line + "\"" + limsSample.LimsId + "\",";
                        line = line + "\"" + TStatusSummary + "\",";
                        line = line + "\"" + TSumStateDescription + "\",";
                        line = line + "\"" + limsSample.Amount + "\",";
                        line = line + "\"" + SRackBC + "\",";
                        line = line + "\"" + Sample_Type + "\",";
                        line = line + "\"" + SPositionId + "\",";
                        line = line + "\"" + SPositionBC + "\",";
                        line = line + "\"" + ActionDateTime + "\",";
                        line = line + "\"" + myLimsCom.OperatorGetId(UserName) + "\","; 
                        line = line + "\"" + Location_Id + "\","; // Hardcode Kiwi location
                        line = line + "\"" + Description + "\",";
                        x++;
                        line = line.TrimEnd(',');
                        saveFile.WriteLine(line);
                    }
                    saveFile.Close();
                }
                l_Reply = true;
            }
            catch (Exception ex)
            {
                SysLog.Log("Fejl under oprettelse af fil til import af data: " + ex.Message + Environment.NewLine + ex.StackTrace, LogLevel.Error);
                MessageBox.Show("Fejl under oprettelse af fil til import af data: " + ex.Message + Environment.NewLine + ex.StackTrace);
            }
            return l_Reply;
        }

        private string GetPosition(string Barcode)
        {
            Tuple<string, string> responseTube = myKiwiCom.GetTubeInfo(KiwiEntityExtension.OrderId++, "ADMIN", Barcode);
            if (responseTube.Item1 == "ERR")
            {
                SysLog.Log("Fejl under tjek af plade info: " + responseTube.Item2, LogLevel.Warning);
                MessageBox.Show("Fejl under tjek af plade info: " + responseTube.Item2);
            }
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(responseTube.Item2);
            XmlNode node = doc.SelectSingleNode("/STXRequest/Tubes/PltTPos/PX");
            string intPos = node.InnerText.Trim();
            node = doc.SelectSingleNode("/STXRequest/Tubes/PltTPos/PYA");
            string alfPos = node.InnerText.Trim();
            
            return alfPos + intPos; 
        }
    }
}
