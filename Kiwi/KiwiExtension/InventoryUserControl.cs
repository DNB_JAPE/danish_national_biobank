﻿using DNBTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace KiwiExtension
{
    public partial class InventoryUserControl : KiwiUserControl
    {
        public InventoryUserControl()
        {
            InitializeComponent();
        }

        private void InventoryUserControl_VisibleChanged(object sender, EventArgs e)
        {
            if (!this.DesignMode && _configuration != null)
            {
                List<string> partitions = new List<string>();
                partitions.AddRange(_configuration.Config05.Partitions);
                partitions.AddRange(_configuration.Config075.Partitions);
                partitions.AddRange(_configuration.Config10.Partitions);
                partitions.AddRange(_configuration.Config14.Partitions);
                _partitionComboBox.Items.Clear();
                partitions = partitions.Distinct().ToList();
                partitions.Sort();
                _partitionComboBox.Items.AddRange(partitions.ToArray());
                _plateTextBox.Text = "";
                _inventoryPartitionButton.Enabled = false;
                _inventoryPlateButton.Enabled = false;
            }
        }

        internal void EnableUI(bool enable)
        {
            _inventoryPartitionButton.Enabled = enable;
            _partitionComboBox.Enabled = enable;
            _inventoryPlateButton.Enabled = enable;
            _plateTextBox.Enabled = enable;
            label1.Enabled = enable;
            label2.Enabled = enable;
        }

        private void _partitionComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            _inventoryPartitionButton.Enabled = true;
        }

        private void _plateTextBox_TextChanged(object sender, EventArgs e)
        {
            // Fixed bug so button gets enabled when there is NO null or whitespace in the textbox
            _inventoryPlateButton.Enabled = !string.IsNullOrWhiteSpace(_plateTextBox.Text);
        }

        private void _inventoryPartitionButton_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                EnableUI(false);
                int orderId = KiwiEntityExtension.OrderId++;
                Tuple<string, string> response = _kiwiCommunication.DoInventoryOnPartition(orderId, _configuration.OperatorKiwiName, _partitionComboBox.Text);
                MessageBox.Show(_kiwiCommunication.PrettyPrintResponse(response.Item2));
                EnableUI(true);
            }
        }

        private void _inventoryPlateButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_plateTextBox.Text))
            {
                MessageBox.Show("Vælg venligst plade");
            }
            else
            {
                using (new WaitCursor())
                {
                    EnableUI(false);
                    int orderId = KiwiEntityExtension.OrderId++;
                    Tuple<string, string> response = _kiwiCommunication.DoInventoryOnPlate(orderId, _configuration.OperatorKiwiName, _plateTextBox.Text);
                    MessageBox.Show(_kiwiCommunication.PrettyPrintResponse(response.Item2));
                    EnableUI(true);
                }
            }
        }
    }
}
