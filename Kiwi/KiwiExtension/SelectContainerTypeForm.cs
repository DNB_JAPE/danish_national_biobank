﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KiwiExtension
{
    public partial class SelectContainerTypeForm : Form
    {
        public string ContainerTypeId { get; set; }

        public SelectContainerTypeForm()
        {
            InitializeComponent();
        }

        private void SelectContainerTypeForm_Load(object sender, EventArgs e)
        {
            comboBox.SelectedIndex = 0;
        }

        private void button_Click(object sender, EventArgs e)
        {
            switch (comboBox.SelectedIndex)
            {
                case 0:
                    ContainerTypeId = "41";
                    break;
                case 1:
                    ContainerTypeId = "21";
                    break;
                case 2:
                    ContainerTypeId = "43";
                    break;
                case 3:
                    ContainerTypeId = "42";
                    break;
            }
            Close();
        }

        private void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            button.Select();
        }
    }
}
