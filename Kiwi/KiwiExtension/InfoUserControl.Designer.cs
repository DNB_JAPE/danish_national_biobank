﻿namespace KiwiExtension
{
    partial class InfoUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._plateInfoButton = new System.Windows.Forms.Button();
            this._plateInfoTextBox = new System.Windows.Forms.TextBox();
            this._tubeInfoTextBox = new System.Windows.Forms.TextBox();
            this._tubeInfoButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _plateInfoButton
            // 
            this._plateInfoButton.Location = new System.Drawing.Point(12, 3);
            this._plateInfoButton.Name = "_plateInfoButton";
            this._plateInfoButton.Size = new System.Drawing.Size(86, 23);
            this._plateInfoButton.TabIndex = 3;
            this._plateInfoButton.Text = "Plade Info";
            this._plateInfoButton.UseVisualStyleBackColor = true;
            this._plateInfoButton.Click += new System.EventHandler(this._plateInfoButton_Click);
            // 
            // _plateInfoTextBox
            // 
            this._plateInfoTextBox.Location = new System.Drawing.Point(104, 5);
            this._plateInfoTextBox.Name = "_plateInfoTextBox";
            this._plateInfoTextBox.Size = new System.Drawing.Size(123, 20);
            this._plateInfoTextBox.TabIndex = 4;
            this._plateInfoTextBox.WordWrap = false;
            // 
            // _tubeInfoTextBox
            // 
            this._tubeInfoTextBox.Location = new System.Drawing.Point(104, 52);
            this._tubeInfoTextBox.Name = "_tubeInfoTextBox";
            this._tubeInfoTextBox.Size = new System.Drawing.Size(123, 20);
            this._tubeInfoTextBox.TabIndex = 6;
            this._tubeInfoTextBox.WordWrap = false;
            // 
            // _tubeInfoButton
            // 
            this._tubeInfoButton.Location = new System.Drawing.Point(12, 50);
            this._tubeInfoButton.Name = "_tubeInfoButton";
            this._tubeInfoButton.Size = new System.Drawing.Size(86, 23);
            this._tubeInfoButton.TabIndex = 5;
            this._tubeInfoButton.Text = "Prøve Info";
            this._tubeInfoButton.UseVisualStyleBackColor = true;
            this._tubeInfoButton.Click += new System.EventHandler(this._tubeInfoButton_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(233, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(311, 36);
            this.label1.TabIndex = 7;
            this.label1.Text = "Viser Kiwi\'ens information om den angive plade. Indeholder bla. a. partitionsnavn" +
    " og stregkoder på prøver.";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(233, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(300, 33);
            this.label2.TabIndex = 8;
            this.label2.Text = "Viser Kiwi\'ens information om den angive prøve. Indeholder bla. a. partitionsnavn" +
    ", pladenavn og position.";
            // 
            // InfoUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._tubeInfoTextBox);
            this.Controls.Add(this._tubeInfoButton);
            this.Controls.Add(this._plateInfoTextBox);
            this.Controls.Add(this._plateInfoButton);
            this.Name = "InfoUserControl";
            this.Size = new System.Drawing.Size(547, 418);
            this.VisibleChanged += new System.EventHandler(this.InfoUserControl_VisibleChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _plateInfoButton;
        private System.Windows.Forms.TextBox _plateInfoTextBox;
        private System.Windows.Forms.TextBox _tubeInfoTextBox;
        private System.Windows.Forms.Button _tubeInfoButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}
