﻿//************************************************
//
//  2017.05.31 - ESAT : Added UIDoRetrieveToBuffer
//      Routine is used to get a list of plates out of the store and place it into the buffer location
//      for later retrieval.
//************************************************												  
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DNBTools;
using System.Threading;
using DNBTools.Kiwi;
using DNBTools.Nautilus;
using DNBTools.DataClasses;

namespace KiwiExtension
{
    public partial class PlatesUserControl : KiwiUserControl
    {
        private List<BiobankEntity> _plates;

        public PlatesUserControl()
        {
            InitializeComponent();
        }

        public void Initialize(List<BiobankEntity> plates, Configuration configuration, LimsCommunication limsCommunication, KiwiCommunication kiwiCommunication)
        {
            base.Initialize(configuration, limsCommunication, kiwiCommunication);
            _plates = plates;
            EnableUI(_plates.Count > 0);
            SysLog.Log("Plate Usercontrol initialized", LogLevel.Info);
        }

        internal void EnableUI(bool enable)
        {
            _platesListBox.Enabled = enable;
            _partitionComboBox.Enabled = enable;
            _storePlatesButton.Enabled = enable;
            _storePlateButton.Enabled = enable;
            _movePlateButton.Enabled = enable;
            _retrievePlateButton.Enabled = enable;
            _registerButton.Enabled = enable;   // ESAT 2017-11-21
            _moveToBufferButton.Enabled = enable;   // ESAT 2017-11-21
            _selectLabel.Enabled = enable;
            label2.Enabled = enable;
            label3.Enabled = enable;
            _statusLabel.Text = enable ? "Klar" : "Låst";
            Refresh();
        }

        private void PlatesUserControl_VisibleChanged(object sender, EventArgs e)
        {
            if (!DesignMode && _plates != null)
            {
                _platesListBox.Items.Clear();
                if (_plates.Count > 0)
                {
                    foreach (BiobankEntity plate in _plates)
                    {
                        EntityListBoxItem item = new EntityListBoxItem(plate);
                        _platesListBox.Items.Add(item);
                    }
                }
                _partitionComboBox.Items.Clear();
                _partitionComboBox.Items.AddRange(_configuration.DefaultConfig.Partitions);
                _partitionComboBox.SelectedIndex = 0;
                _storePlatesButton.Enabled = false;
                _storePlateButton.Enabled = false;
                _movePlateButton.Enabled = false;
                _retrievePlateButton.Enabled = false;
                for (int i = 0; i < _platesListBox.Items.Count; i++)
                {
                    _platesListBox.SetSelected(i, true);
                }
                _selectLabel.Text = _platesListBox.SelectedItems.Count + " af " + _platesListBox.Items.Count + " valgt";
                _statusLabel.Text = "Klar";
                SysLog.Log(string.Format("Plate Usercontrol: {0} ", _selectLabel.Text), LogLevel.Info);
            }
        }

        private void _platesListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateState();
            _selectLabel.Text = _platesListBox.SelectedItems.Count + " af " + _platesListBox.Items.Count + " valgt";
            SysLog.Log(string.Format("Plate Usercontrol: {0} ", _selectLabel.Text), LogLevel.Info);
        }

        private void UpdateState()
        {
            switch (_platesListBox.SelectedItems.Count)
            {
                case 0:
                    {
                        _storePlateButton.Enabled = false;
                        _storePlatesButton.Enabled = false;
                        _movePlateButton.Enabled = false;
                        _retrievePlateButton.Enabled = false;
                        break;
                    }
                case 1:
                    {
                        _storePlatesButton.Enabled = true;
                        _storePlateButton.Enabled = true;
                        _retrievePlateButton.Enabled = true;
                        _movePlateButton.Enabled = true;
                        break;
                    }
                default:
                    {
                        _storePlatesButton.Enabled = true;
                        _storePlateButton.Enabled = false;
                        _retrievePlateButton.Enabled = false;
                        _movePlateButton.Enabled = true;
                        break;
                    }
            }
            Refresh();
        }

        private void _storePlatesButton_Click(object sender, EventArgs e)
        {
            SysLog.Log("Store Plates", LogLevel.Info);
            using (new WaitCursor())
            {
                EnableUI(false);
                if (UIAddPlatesToKiwi(string.Empty, "0"))
                {
                    if (UIDoInventory())
                    {
                        UIDoMove();
                    }
                }
                EnableUI(true);
            }
        }

        private void _storePlateButton_Click(object sender, EventArgs e)
        {
            SysLog.Log("Store Plate", LogLevel.Info);
            using (new WaitCursor())
            {
                EnableUI(false);
                UIAddPlatesToKiwi(_partitionComboBox.Text, _configuration.DefaultConfig.TransferStation);
                EnableUI(true);
            }
        }

        private void _movePlateButton_Click(object sender, EventArgs e)
        {
            SysLog.Log("Move Plate", LogLevel.Info);
            using (new WaitCursor())
            {
                EnableUI(false);
                UIDoMove();
                EnableUI(true);
            }
        }

        private void _retrievePlateButton_Click(object sender, EventArgs e)
        {
            SysLog.Log("Retrieve Plate", LogLevel.Info);
            using (new WaitCursor())
            {
                EnableUI(false);
                UIDoRetrieve();
                EnableUI(true);
            }
        }

        private void _registerButton_Click(object sender, EventArgs e)
        {
            SysLog.Log("Register Plates", LogLevel.Info);
            using (new WaitCursor())
            {
                EnableUI(false);
                UIAddPlatesToKiwi(string.Empty, "0");
                EnableUI(true);
            }
        }

        private bool UIAddPlatesToKiwi(string partition, string transferStation)
        {
            try
            {
                SysLog.Log(string.Format("Add {0} plates to Kiwi", _platesListBox.SelectedItems.Count), LogLevel.Info);
                foreach (EntityListBoxItem item in _platesListBox.SelectedItems)
                {
                    // Lookup samples in Nautilus
                    _statusLabel.Text = string.Format("Henter information fra Nautilus for '{0}'", item.Barcode);
                    SysLog.Log(_statusLabel.Text, LogLevel.Info);
                    Refresh();
                    List<Tuple<string, string, string>> samples = LookupSamplesInNautilus(item.Barcode);
                    if (samples == null)
                    {
                        return false;
                    }
                    // Store plates
                    _statusLabel.Text = "Føjer '" + item.Barcode + "' til Kiwi";
                    SysLog.Log(_statusLabel.Text, LogLevel.Info);
                    Refresh();
                    if (AddPlateToKiwi(item.Barcode, samples, partition, transferStation))
                    {
                        if (!UpdateNautilus(item.Barcode, item.Entity.LogicalId))
                            return false;
                        //try
                        //{
                        //    // Opdater Nautilus
                        //    _statusLabel.Text = "Opdaterer location for '" + item.Barcode + "' i Nautilus";
                        //    Refresh();
                        //    _limsCommunication.UpdateLocationForCarrier(item.Entity.LogicalId, _limsCommunication.LocationGetId(LimsCommunication.KiwiLocationName));
                        //}
                        //catch (Exception ex)
                        //{
                        //    SysLog.Log("Fejl under opdatering af Nautilus: " + ex.Message + Environment.NewLine + ex.StackTrace, LogLevel.Error);
                        //    MessageBox.Show("Fejl under opdatering af Nautilus: " + ex.Message + Environment.NewLine + ex.StackTrace);
                        //    return false;
                        //}
                    }
                    else
                    {
                        DialogResult res = MessageBox.Show("Skal vi prøve med den samme plade igen?", "Prøv igen?", MessageBoxButtons.YesNo);
                        if (res == DialogResult.Yes)
                        {
                            if (!AddPlateToKiwi(item.Barcode, samples, partition, transferStation))
                            {
                                DialogResult res1 = MessageBox.Show("Det ser ud til at der stadig er en fejl. Skal vi afbryde (Ja) eller prøve med den næste?(Nej)", "Prøv igen?", MessageBoxButtons.YesNo);
                                if (res1 == DialogResult.Yes)
                                    return false;
                            }
                            if (!UpdateNautilus(item.Barcode, item.Entity.LogicalId))
                                return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                SysLog.Log("Fejl under tilføjelse: " + ex.Message + Environment.NewLine + ex.StackTrace, LogLevel.Error);
                MessageBox.Show("Fejl under tilføjelse: " + ex.Message + Environment.NewLine + ex.StackTrace);
                return false;
            }
        }

        private bool UpdateNautilus(string BarCode, string LogicalId)
        {
            try
            {
                // Opdater Nautilus
                _statusLabel.Text = "Opdaterer location for '" + BarCode + "' i Nautilus";
                Refresh();
                _limsCommunication.UpdateLocationForCarrier(LogicalId, _limsCommunication.LocationGetId(LimsCommunication.KiwiLocationName));
                return true;
            }
            catch (Exception ex)
            {
                SysLog.Log("Fejl under opdatering af Nautilus: " + ex.Message + Environment.NewLine + ex.StackTrace, LogLevel.Error);
                MessageBox.Show("Fejl under opdatering af Nautilus: " + ex.Message + Environment.NewLine + ex.StackTrace);
                return false;
            }
        }

        private bool UIDoInventory()
        {
            SysLog.Log("Inventory", LogLevel.Info);
            try
            {
                _statusLabel.Text = "Kører Inventory";
                Refresh();
                int orderId = KiwiEntityExtension.OrderId++;
                Tuple<string, string> response = _kiwiCommunication.DoInventoryOnPartition(orderId, _configuration.OperatorKiwiName, _configuration.DefaultConfig.Buffer);
                Tuple<string, string> status = _kiwiCommunication.StatusFromResponse(response.Item2);
                while (status.Item1 == "RUN" || status.Item1 == "WAIT")
                {
                    Thread.Sleep(10000);
                    response = _kiwiCommunication.GetCmdStatus(KiwiEntityExtension.OrderId++, _configuration.OperatorKiwiName, orderId);
                    status = _kiwiCommunication.StatusFromResponse(response.Item2);
                }
                if (status.Item1 == "ERR")
                {
                    SysLog.Log(String.Format("Kiwi Error during Inventory: {0}", status.Item2), LogLevel.Warning);
                    MessageBox.Show("Fejl under inventory: " + status.Item2);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                SysLog.Log(String.Format("Error during Inventory: {0}", ex.Message + Environment.NewLine + ex.StackTrace), LogLevel.Error);
                MessageBox.Show("Fejl under inventory: " + ex.Message + Environment.NewLine + ex.StackTrace);
                return false;
            }
        }

        private bool UIDoMove()
        {
            try
            {
                foreach (EntityListBoxItem item in _platesListBox.SelectedItems)
                {
                    _statusLabel.Text = "Flytter plade '" + item.Barcode + "'";
                    Refresh();
                    int orderId = KiwiEntityExtension.OrderId++;
                    Tuple<string, string> response = _kiwiCommunication.MovePlate(orderId, _configuration.OperatorKiwiName, _partitionComboBox.Text, _configuration.DefaultConfig.TransferStation, _configuration.DefaultConfig.ContainerTypeId, item.Barcode);
                    Tuple<string, string> status = _kiwiCommunication.StatusFromResponse(response.Item2);
                    while (status.Item1 == "RUN" || status.Item1 == "WAIT")
                    {
                        Thread.Sleep(2000);
                        response = _kiwiCommunication.GetCmdStatus(KiwiEntityExtension.OrderId++, _configuration.OperatorKiwiName, orderId);
                        status = _kiwiCommunication.StatusFromResponse(response.Item2);
                    }
                    if (status.Item1 == "ERR")
                    {
                        SysLog.Log(String.Format("Kiwi Error during Move: {0}", status.Item2), LogLevel.Warning);
                        MessageBox.Show("Fejl under flytning: " + status.Item2);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                SysLog.Log(String.Format("Error during Move: {0}", ex.Message + Environment.NewLine + ex.StackTrace), LogLevel.Error);
                MessageBox.Show("Fejl under flytning: " + ex.Message + Environment.NewLine + ex.StackTrace);
                return false;
            }
        }

        private bool UIDoRetrieve()
        {
            try
            {
                // Lookup samples in Nautilus
                EntityListBoxItem item = _platesListBox.SelectedItem as EntityListBoxItem;
                _statusLabel.Text = "Udtager '" + item.Barcode + "'";
                Refresh();
                int orderId = KiwiEntityExtension.OrderId++;
                Tuple<string, string> response = _kiwiCommunication.RetrievePlate(orderId, _configuration.OperatorKiwiName, _configuration.DefaultConfig.TransferStation, _configuration.DefaultConfig.ContainerTypeId, item.Barcode);
                Tuple<string, string> status = _kiwiCommunication.StatusFromResponse(response.Item2);
                while (status.Item1 == "RUN" || status.Item1 == "WAIT")
                {
                    Thread.Sleep(2000);
                    response = _kiwiCommunication.GetCmdStatus(KiwiEntityExtension.OrderId++, _configuration.OperatorKiwiName, orderId);
                    status = _kiwiCommunication.StatusFromResponse(response.Item2);
                }
                if (status.Item1 == "ERR")
                {
                    SysLog.Log(String.Format("Kiwi Error during retrieval: {0}", status.Item2), LogLevel.Warning);
                    MessageBox.Show("Fejl under udtagning: " + status.Item2);
                    return false;
                }
                else
                {
                    try
                    {
                        _limsCommunication.UpdateLocationForCarrier(item.Entity.LogicalId, _limsCommunication.LocationGetId(LimsCommunication.PickedLocationName));
                    }
                    catch (Exception ex)
                    {
                        SysLog.Log(String.Format("Kiwi Error during nautilus update: {0}", ex.Message + Environment.NewLine + ex.StackTrace), LogLevel.Warning);
                        MessageBox.Show("Fejl under opdatering af Nautilus: " + ex.Message + Environment.NewLine + ex.StackTrace);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                SysLog.Log(String.Format("Error during nautilus update: {0}", ex.Message + Environment.NewLine + ex.StackTrace), LogLevel.Warning);
                MessageBox.Show("Fejl i ordre: " + ex.Message + Environment.NewLine + ex.StackTrace);
                return false;
            }
        }

        private List<Tuple<string, string, string>> LookupSamplesInNautilus(string plateBc)
        {
            try 
            {
                string plateId = _limsCommunication.PlateGetId(plateBc);
                if (plateId == null)
                {
                    SysLog.Log(String.Format("Plate is unknown in Nautilus: {0}", plateBc), LogLevel.Warning);
                    MessageBox.Show("Pladen er ukendt i Nautilus: " + plateBc);
                    return null;
                }
                List<Tuple<int, string, string, string, string>> data = _limsCommunication.LookupSampleDataForCarrier(plateId);
                List<Tuple<string, string, string>> retval = new List<Tuple<string, string, string>>();
                foreach (Tuple<int, string, string, string, string> sampleInfo in data)
                {
                    retval.Add(new Tuple<string, string, string>(sampleInfo.Item2, sampleInfo.Item4, sampleInfo.Item5)); // aliquotRef, row, column
                }
                return retval;
            }
            catch (Exception ex)
            {
                SysLog.Log(String.Format("Error during nautilus lookup: {0}", ex.Message + Environment.NewLine + ex.StackTrace), LogLevel.Warning);
                MessageBox.Show("Fejl under opslag i Nautilus: " + ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }

        private bool AddPlateToKiwi(string barcode, List<Tuple<string, string, string>> samples, string partition, string transferStation)
        {
            int orderId = KiwiEntityExtension.OrderId++;
            SysLog.Log(string.Format("Add plate {0} to Kiwi",barcode), LogLevel.Info);
            Tuple<string, string> response = _kiwiCommunication.StorePlate(orderId, _configuration.OperatorKiwiName,partition, transferStation, _configuration.DefaultConfig.ContainerTypeId, barcode, samples);
            Tuple<string, string> status = _kiwiCommunication.StatusFromResponse(response.Item2);
            // Wait for store plate to finish
            SysLog.Log(string.Format("Wait for store plate to finish"), LogLevel.Info);
            while (status.Item1 == "RUN" || status.Item1 == "WAIT")
            {
                Thread.Sleep(2000);
                response = _kiwiCommunication.GetCmdStatus(KiwiEntityExtension.OrderId++, _configuration.OperatorKiwiName, orderId);
                status = _kiwiCommunication.StatusFromResponse(response.Item2);
            }
            SysLog.Log(string.Format("Store plate finished with status {0}", status.Item1), LogLevel.Info);
            if (status.Item1 == "ERR")
            {
                SysLog.Log(string.Format("ERROR: Store plate failed: {0}", status.Item2), LogLevel.Error);
                MessageBox.Show("Fejl i tilføjelse af plade til Kiwi: " + status.Item2);
                return false;
            }
            return true;
        }
		private void _moveToBufferButton_Click(object sender, EventArgs e)
        {
            SysLog.Log("Move to buffer", LogLevel.Info);
            using (new WaitCursor())
            {
                EnableUI(false);
                UIDoRetrieveToBuffer();
                EnableUI(true);
            }
        }
		/// <summary>
        /// Retrieves the list of plates and puts 
        /// it into the buffer location instead of the transfer arm.
        /// </summary>
        /// <returns>True if all went OK, False if error has been detected</returns>
        private bool UIDoRetrieveToBuffer()
        {
            try
            {
                // Looping while there are selected items
                while(_platesListBox.SelectedItems.Count>0)
                {
                    try
                    {
                        // Lookup samples in Nautilus
                        EntityListBoxItem item = _platesListBox.SelectedItem as EntityListBoxItem;

                        _statusLabel.Text = "Udtager '" + item.Barcode + "'";
                        SysLog.Log(_statusLabel.Text, LogLevel.Info);
                        Refresh();
                        int orderId = KiwiEntityExtension.OrderId++;
                        // Transferstation should be empty according to Liconic.
                        Tuple<string, string> response = _kiwiCommunication.MovePlate(orderId, _configuration.OperatorKiwiName, _configuration.DefaultConfig.Buffer, "", _configuration.DefaultConfig.ContainerTypeId, item.Barcode);
                        Tuple<string, string> status = _kiwiCommunication.StatusFromResponse(response.Item2);
                        while (status.Item1 == "RUN" || status.Item1 == "WAIT")
                        {
                            Thread.Sleep(2000);
                            response = _kiwiCommunication.GetCmdStatus(KiwiEntityExtension.OrderId++, _configuration.OperatorKiwiName, orderId);
                            status = _kiwiCommunication.StatusFromResponse(response.Item2);
                        }
                        if (status.Item1 == "ERR")
                        {
                            SysLog.Log("Fejl under udtagning til buffer: " + status.Item2, LogLevel.Error);
                            MessageBox.Show("Fejl under udtagning til buffer: " + status.Item2);
                            return false;
                        }
                        else
                        {
                            try
                            {
                                _limsCommunication.UpdateLocationForCarrier(item.Entity.LogicalId, _limsCommunication.LocationGetId(LimsCommunication.PickedLocationName));
                            }
                            catch (Exception ex)
                            {
                                SysLog.Log("Fejl under opdatering af Nautilus: " + ex.Message + Environment.NewLine + ex.StackTrace, LogLevel.Error);
                                MessageBox.Show("Fejl under opdatering af Nautilus: " + ex.Message + Environment.NewLine + ex.StackTrace);
                                return false;
                            }
                        }
                        // Removing item if no error occurs
                        _platesListBox.Items.Remove(_platesListBox.SelectedItem);
                    }
                    catch (Exception xe)
                    {
                        SysLog.Log("Fejl i ordre: " + xe.Message + Environment.NewLine + xe.StackTrace, LogLevel.Error);
                        MessageBox.Show("Fejl i ordre: " + xe.Message + Environment.NewLine + xe.StackTrace);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                SysLog.Log("Fejl i ordre: " + ex.Message + Environment.NewLine + ex.StackTrace, LogLevel.Error);
                MessageBox.Show("Fejl i ordre: " + ex.Message + Environment.NewLine + ex.StackTrace);
                return false;
            }
        }
    }
}
