﻿using DNBTools;
using DNBTools.DataClasses;
using DNBTools.Kiwi;
using DNBTools.Nautilus;
using LSEXT;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using Thermo.Nautilus.Extensions.UserInterface;

namespace KiwiExtension
{
    /// <summary>
    /// Extension for sending orders to Kiwi robot freezer.
    /// </summary>
    public class KiwiEntityExtension : EntityExtension, IVersion
    {
        public static int OrderId;

        /// <summary>
        /// Version must match the one registered in Nautilus in order for extension to be loaded.
        /// </summary>
        /// <returns></returns>
        public int GetVersion()
        {
            return 2;
        }

        /// <summary>
        /// Called by Lims to decide if the extension should be listed in the right click menu.
        /// </summary>
        /// <param name="Parameters">Lims parameters (not used).</param>
        /// <returns>ExecuteExtension.exEnabled - Always allow this extension to start.</returns>
        public override EntityExtension.enumExtensionVisibility CanExecuteExtension()
        {
            return EntityExtension.enumExtensionVisibility.Enabled;
        }

        string appGuid = "1874052f-0255-49ac-8e85-3e9b8ec8aede";
        /// <summary>
        /// Called by Lims to activate the extension.
        /// </summary>
        public override void ExecuteExtension()
        {
            // Prevent second instance to run.
            using (Mutex mutex = new Mutex(false, appGuid))
            {
                if (!mutex.WaitOne(0, false))
                {
                    MessageBox.Show("Der er allerede nogen i gang med at bruge Kiwi. Vent venligst til senere.");
                    return;
                }
            }

            // Read SysLog configuration
            Configuration syslogConfig;
            try
            {
                syslogConfig = new Configuration();
            }
            catch (Exception ex)
            {
               // SysLog.Log(string.Format("Kunne ikke indlæse konfigurationen: {0} ", ex.Message), LogLevel.Error);
                MessageBox.Show("Kunne ikke indlæse konfigurationen til Syslog: " + ex.Message);
                return;
            }
            SysLog.EnableLog(syslogConfig.SysLogIP, syslogConfig.SysLogName, syslogConfig.SysLogEnabled, syslogConfig.DebugLevel);


            OrderId = 1;
            // Read entities from Nautilus
            List<BiobankEntity> entities = new List<BiobankEntity>();
            string containerTypeId = string.Empty;
            string operatorId;
            try
            {
                SysLog.Log(string.Format("Extension activated. {0} parameters in list.", Parameters.Count), LogLevel.Info);
                ADODB.Recordset records = (ADODB.Recordset)Parameters["RECORDS"];
                while (!records.EOF)
                {
                    foreach (ADODB.Field field in records.Fields)
                    {
                        BiobankEntity entity;
                        string id = field.Value.ToString();
                        if (field.Name.Equals("ALIQUOT_ID"))
                        {
                            Thermo.Nautilus.Extensions.Model.Aliquot nautilusAliquot = Nautilus.Aliquots[id, "ALIQUOT_ID"];
                            string barcode = nautilusAliquot["EXTERNAL_REFERENCE"].ToString();
                            List<string> containerTypes = new List<string> { "41", "21", "42", "43" };
                            if (nautilusAliquot["CONTAINER_TYPE_ID"] == null || !containerTypes.Contains(nautilusAliquot["CONTAINER_TYPE_ID"].ToString()))
                            {
                                SysLog.Log(string.Format("Ugyldig container type id for: {0}", barcode), LogLevel.Alert);
                                MessageBox.Show("Ugyldig container type id for: " + barcode);
                                return;
                            }
                            else if(containerTypeId != string.Empty && containerTypeId != nautilusAliquot["CONTAINER_TYPE_ID"].ToString())
                            {
                                SysLog.Log("Ikke alle prøver er af samme type", LogLevel.Alert);
                                MessageBox.Show("Ikke alle prøver er af samme type");
                                return;
                            }
                            else
                            {
                                containerTypeId = nautilusAliquot["CONTAINER_TYPE_ID"].ToString();
                            }
                            
                            entity = new Sample(id, barcode);
                        }
                        else if (field.Name.Equals("PLATE_ID"))
                        {
                            Thermo.Nautilus.Extensions.Model.Plate nautilusPlate = Nautilus.Plates[id, "PLATE_ID"];
                            string barcode = nautilusPlate["EXTERNAL_REFERENCE"].ToString();
                            entity = new Plate(id, barcode);
                        }
                        else
                        {
                            SysLog.Log("Nautilus objekt er hverken prøve eller plade", LogLevel.Alert);
                            MessageBox.Show("Nautilus objekt er hverken prøve eller plade");
                            return;
                        }
                        entities.Add(entity);
                    }
                    records.MoveNext();
                }
                if(entities.Count == 0)
                {
                    SysLog.Log("Vælg venligst prøver eller plader inden extensionen åbnes", LogLevel.Alert);
                    MessageBox.Show("Vælg venligst prøver eller plader inden extensionen åbnes");
                    return;
                }
                if(entities[0] is Plate)
                {
                    SelectContainerTypeForm selectContainerTypeForm = new SelectContainerTypeForm();
                    selectContainerTypeForm.ShowDialog();
                    containerTypeId = selectContainerTypeForm.ContainerTypeId;
                }
                operatorId = Parameters["OPERATOR_ID"].ToString();
                SysLog.Log("Containertype valgt: " + containerTypeId, LogLevel.Info);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Kunne ikke læse data fra Nautilus: {0} ", ex.Message), LogLevel.Error);
                MessageBox.Show("Kunne ikke læse data fra Nautilus: " + ex.Message);
                return;
            }
            // Read configuration
            SysLog.Log("Read configuration", LogLevel.Info);
            Configuration configuration;
            try
            {
                configuration = new Configuration(containerTypeId);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Kunne ikke indlæse konfigurationen: {0} ", ex.Message), LogLevel.Error);
                MessageBox.Show("Kunne ikke indlæse konfigurationen: " + ex.Message);
                return;
            }
            // Create KiwiCommunication
            SysLog.Log(string.Format("Create Kiwi communication. ContainerTypeID: {0}", containerTypeId), LogLevel.Info);
            KiwiCommunication kiwiCommunication = null;
            try
            {
                kiwiCommunication = new KiwiCommunication(configuration.DefaultConfig.StoreAddress, configuration.DefaultConfig.StoreAddress, configuration.DefaultConfig.StorePort);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Kunne ikke Oprette forbindelse til Kiwi: {0} ", ex.Message), LogLevel.Error);
                MessageBox.Show("Kunne ikke Oprette forbindelse til Kiwi: " + ex.Message);
                return;
            }
            // Create LimsCommunication
            SysLog.Log("Create Lims communication", LogLevel.Info);
            LimsCommunication limsCommunication = null;
            try
            {
                limsCommunication = new LimsCommunication(configuration.NautConStr);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Kunne ikke Oprette forbindelse til Nautilus DB: {0} ", ex.Message), LogLevel.Error);
                MessageBox.Show("Kunne ikke Oprette forbindelse til Nautilus DB: " + ex.Message);
                return;
            }
            // Set operator name in configuration
            Operator operatorObj = limsCommunication.OperatorGet(operatorId);
            SysLog.Log(string.Format("Operator ID: {0}", operatorObj.LogicalId), LogLevel.Info);
            configuration.InitializeOperator(operatorObj.LogicalId);
            // Open extension form
            SysLog.Log("Initializing extension", LogLevel.Info);
            KiwiExtensionForm extensionForm = new KiwiExtensionForm(entities, kiwiCommunication, limsCommunication, configuration);
            extensionForm.ShowDialog();
            kiwiCommunication.Dispose();
            limsCommunication.Dispose();
            SysLog.CloseSyslog();
        }
    }
}
