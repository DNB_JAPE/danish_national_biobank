﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DNBTools;

namespace KiwiExtension
{
    public partial class ConsolidateUserControl : KiwiUserControl
    {
        public ConsolidateUserControl()
        {
            InitializeComponent();
        }

        internal void EnableUI(bool enable)
        {
            _consolidateButton.Enabled = enable;
            _consolidateComboBox.Enabled = enable;
        }

        private void ConsolidateUserControl_VisibleChanged(object sender, EventArgs e)
        {
            _consolidateComboBox.Items.Clear();
            _consolidateComboBox.Items.Add("A");
            _consolidateComboBox.Items.Add("B");
            _consolidateComboBox.Items.Add("C");
            _consolidateComboBox.Items.Add("D");
            _consolidateComboBox.Items.Add("E");
            _consolidateComboBox.Items.Add("F");
            _consolidateButton.Enabled = false;
        }

        private void _consolidateComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            _consolidateButton.Enabled = _consolidateComboBox.SelectedItem == null ? false : true;
        }

        private void _consolidateButton_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                EnableUI(false);
                int orderId = KiwiEntityExtension.OrderId++;
                //Tuple<string, string> response = _kiwiCommunication.Consolidate(orderId, _operatorName, _consolidateComboBox.Text);
                //Tuple<string, string> status = _kiwiCommunication.StatusFromResponse(response.Item2);
                //MessageBox.Show(_kiwiCommunication.PrettyPrintResponse(response.Item2, false));
                EnableUI(true);
            }
        }
    }
}
