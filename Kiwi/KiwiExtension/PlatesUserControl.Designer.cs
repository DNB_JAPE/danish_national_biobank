﻿namespace KiwiExtension
{
    partial class PlatesUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._selectLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._platesListBox = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this._partitionComboBox = new System.Windows.Forms.ComboBox();
            this._storePlatesButton = new System.Windows.Forms.Button();
            this._movePlateButton = new System.Windows.Forms.Button();
            this._retrievePlateButton = new System.Windows.Forms.Button();
            this._storePlateButton = new System.Windows.Forms.Button();
            this._statusLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._registerButton = new System.Windows.Forms.Button();
			this._moveToBufferButton = new System.Windows.Forms.Button();															 
            this.SuspendLayout();
            // 
            // _selectLabel
            // 
            this._selectLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._selectLabel.AutoSize = true;
            this._selectLabel.Location = new System.Drawing.Point(3, 388);
            this._selectLabel.Name = "_selectLabel";
            this._selectLabel.Size = new System.Drawing.Size(60, 13);
            this._selectLabel.TabIndex = 13;
            this._selectLabel.Text = "0 af 0 valgt";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Plader:";
            // 
            // _platesListBox
            // 
            this._platesListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._platesListBox.FormattingEnabled = true;
            this._platesListBox.Location = new System.Drawing.Point(6, 30);
            this._platesListBox.Name = "_platesListBox";
            this._platesListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this._platesListBox.Size = new System.Drawing.Size(221, 355);
            this._platesListBox.TabIndex = 11;
            this._platesListBox.SelectedIndexChanged += new System.EventHandler(this._platesListBox_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(233, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Endelig Placering (Partition):";
            // 
            // _partitionComboBox
            // 
            this._partitionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._partitionComboBox.FormattingEnabled = true;
            this._partitionComboBox.Location = new System.Drawing.Point(233, 30);
            this._partitionComboBox.Name = "_partitionComboBox";
            this._partitionComboBox.Size = new System.Drawing.Size(121, 21);
            this._partitionComboBox.TabIndex = 21;
            // 
            // _storePlatesButton
            // 
            this._storePlatesButton.BackgroundImage = global::KiwiExtension.Properties.Resources.In;
            this._storePlatesButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._storePlatesButton.Location = new System.Drawing.Point(233, 57);
            this._storePlatesButton.Name = "_storePlatesButton";
            this._storePlatesButton.Size = new System.Drawing.Size(121, 53);
            this._storePlatesButton.TabIndex = 22;
            this._storePlatesButton.Text = "          Buffer";
            this._storePlatesButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._storePlatesButton.UseVisualStyleBackColor = true;
            this._storePlatesButton.Click += new System.EventHandler(this._storePlatesButton_Click);
            // 
            // _movePlateButton
            // 
            this._movePlateButton.BackgroundImage = global::KiwiExtension.Properties.Resources.Move;
            this._movePlateButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._movePlateButton.Location = new System.Drawing.Point(360, 116);
            this._movePlateButton.Name = "_movePlateButton";
            this._movePlateButton.Size = new System.Drawing.Size(121, 53);
            this._movePlateButton.TabIndex = 17;
            this._movePlateButton.Text = "          Flyt";
            this._movePlateButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._movePlateButton.UseVisualStyleBackColor = true;
            this._movePlateButton.Click += new System.EventHandler(this._movePlateButton_Click);
            // 
            // _retrievePlateButton
            // 
            this._retrievePlateButton.BackgroundImage = global::KiwiExtension.Properties.Resources.Out;
            this._retrievePlateButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._retrievePlateButton.Location = new System.Drawing.Point(360, 57);
            this._retrievePlateButton.Name = "_retrievePlateButton";
            this._retrievePlateButton.Size = new System.Drawing.Size(121, 53);
            this._retrievePlateButton.TabIndex = 16;
            this._retrievePlateButton.Text = "          Udtag";
            this._retrievePlateButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._retrievePlateButton.UseVisualStyleBackColor = true;
            this._retrievePlateButton.Click += new System.EventHandler(this._retrievePlateButton_Click);
            // 
            // _storePlateButton
            // 
            this._storePlateButton.BackgroundImage = global::KiwiExtension.Properties.Resources.In;
            this._storePlateButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._storePlateButton.Location = new System.Drawing.Point(233, 116);
            this._storePlateButton.Name = "_storePlateButton";
            this._storePlateButton.Size = new System.Drawing.Size(121, 53);
            this._storePlateButton.TabIndex = 15;
            this._storePlateButton.Text = "          Arm";
            this._storePlateButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._storePlateButton.UseVisualStyleBackColor = true;
            this._storePlateButton.Click += new System.EventHandler(this._storePlateButton_Click);
            // 
            // _statusLabel
            // 
            this._statusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._statusLabel.AutoSize = true;
            this._statusLabel.Location = new System.Drawing.Point(233, 366);
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(25, 13);
            this._statusLabel.TabIndex = 23;
            this._statusLabel.Text = "Klar";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(233, 344);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Status:";
            // 
            // _registerButton
            // 
            this._registerButton.BackgroundImage = global::KiwiExtension.Properties.Resources.In;
            this._registerButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._registerButton.Location = new System.Drawing.Point(233, 175);
            this._registerButton.Name = "_registerButton";
            this._registerButton.Size = new System.Drawing.Size(121, 53);
            this._registerButton.TabIndex = 25;
            this._registerButton.Text = "          Registrer i Kiwi";
            this._registerButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._registerButton.UseVisualStyleBackColor = true;
            this._registerButton.Click += new System.EventHandler(this._registerButton_Click);
            // 
            // _moveToBufferButton
            // 
            this._moveToBufferButton.BackgroundImage = global::KiwiExtension.Properties.Resources.Out;
            this._moveToBufferButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._moveToBufferButton.Location = new System.Drawing.Point(360, 175);
            this._moveToBufferButton.Name = "_moveToBufferButton";
            this._moveToBufferButton.Size = new System.Drawing.Size(121, 53);
            this._moveToBufferButton.TabIndex = 26;
            this._moveToBufferButton.Text = "          Udtag til buffer";
            this._moveToBufferButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._moveToBufferButton.UseVisualStyleBackColor = true;
            this._moveToBufferButton.Click += new System.EventHandler(this._moveToBufferButton_Click);
            // 								  
            // PlatesUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this._moveToBufferButton);											
            this.Controls.Add(this._registerButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._statusLabel);
            this.Controls.Add(this._storePlatesButton);
            this.Controls.Add(this._partitionComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._movePlateButton);
            this.Controls.Add(this._retrievePlateButton);
            this.Controls.Add(this._storePlateButton);
            this.Controls.Add(this._selectLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._platesListBox);
            this.Name = "PlatesUserControl";
            this.Size = new System.Drawing.Size(547, 418);
            this.VisibleChanged += new System.EventHandler(this.PlatesUserControl_VisibleChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _selectLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox _platesListBox;
        private System.Windows.Forms.Button _storePlateButton;
        private System.Windows.Forms.Button _retrievePlateButton;
        private System.Windows.Forms.Button _movePlateButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox _partitionComboBox;
        private System.Windows.Forms.Button _storePlatesButton;
        private System.Windows.Forms.Label _statusLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _registerButton;
		private System.Windows.Forms.Button _moveToBufferButton;														
    }
}
