﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DNBTools;

namespace KiwiExtension
{
    public partial class RunXmlUserControl : KiwiUserControl
    {
        public RunXmlUserControl()
        {
            InitializeComponent();
            SysLog.Log("XML Usercontrol started", LogLevel.Info);
        }

        internal void EnableUI(bool enable)
        {
            _runXmlButton.Enabled = enable;
            _xmlTextBox.Enabled = enable;
        }

        private void _runXmlButton_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                EnableUI(false);
                _responseTextBox.Text = string.Empty;
                Tuple<string, string> response = _kiwiCommunication.RunXml(_xmlTextBox.Text);
                _responseTextBox.Text = response.Item2;
                EnableUI(true);
                SysLog.Log("XML response: " + _responseTextBox.Text, LogLevel.Info);
            }
        }
    }
}
