﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DNBTools;
using DNBTools.Kiwi;
using DNBTools.Nautilus;

namespace KiwiExtension
{
    public partial class KiwiUserControl : UserControl
    {
        protected Configuration _configuration;
        protected LimsCommunication _limsCommunication;
        protected KiwiCommunication _kiwiCommunication;

        public KiwiUserControl()
        {
            InitializeComponent();
        }

        public void Initialize(Configuration configuration, LimsCommunication limsCommunication, KiwiCommunication kiwiCommunication)
        {
            _configuration = configuration;
            _limsCommunication = limsCommunication;
            _kiwiCommunication = kiwiCommunication;
        }
    }
}
