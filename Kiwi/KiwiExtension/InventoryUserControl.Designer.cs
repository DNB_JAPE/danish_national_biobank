﻿namespace KiwiExtension
{
    partial class InventoryUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._inventoryPartitionButton = new System.Windows.Forms.Button();
            this._plateTextBox = new System.Windows.Forms.TextBox();
            this._inventoryPlateButton = new System.Windows.Forms.Button();
            this._partitionComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _inventoryPartitionButton
            // 
            this._inventoryPartitionButton.Location = new System.Drawing.Point(3, 0);
            this._inventoryPartitionButton.Name = "_inventoryPartitionButton";
            this._inventoryPartitionButton.Size = new System.Drawing.Size(86, 23);
            this._inventoryPartitionButton.TabIndex = 5;
            this._inventoryPartitionButton.Text = "Partition";
            this._inventoryPartitionButton.UseVisualStyleBackColor = true;
            this._inventoryPartitionButton.Click += new System.EventHandler(this._inventoryPartitionButton_Click);
            // 
            // _plateTextBox
            // 
            this._plateTextBox.Location = new System.Drawing.Point(95, 67);
            this._plateTextBox.Name = "_plateTextBox";
            this._plateTextBox.Size = new System.Drawing.Size(123, 20);
            this._plateTextBox.TabIndex = 7;
            this._plateTextBox.WordWrap = false;
            this._plateTextBox.TextChanged += new System.EventHandler(this._plateTextBox_TextChanged);
            // 
            // _inventoryPlateButton
            // 
            this._inventoryPlateButton.Location = new System.Drawing.Point(3, 65);
            this._inventoryPlateButton.Name = "_inventoryPlateButton";
            this._inventoryPlateButton.Size = new System.Drawing.Size(86, 23);
            this._inventoryPlateButton.TabIndex = 8;
            this._inventoryPlateButton.Text = "Plade";
            this._inventoryPlateButton.UseVisualStyleBackColor = true;
            this._inventoryPlateButton.Click += new System.EventHandler(this._inventoryPlateButton_Click);
            // 
            // _partitionComboBox
            // 
            this._partitionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._partitionComboBox.FormattingEnabled = true;
            this._partitionComboBox.Location = new System.Drawing.Point(95, 2);
            this._partitionComboBox.Name = "_partitionComboBox";
            this._partitionComboBox.Size = new System.Drawing.Size(123, 21);
            this._partitionComboBox.TabIndex = 10;
            this._partitionComboBox.SelectedIndexChanged += new System.EventHandler(this._partitionComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(237, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(292, 72);
            this.label1.TabIndex = 11;
            this.label1.Text = "Inventory på en partition, får Kiwi\'en til at læse stregkoder på alle plader i pa" +
    "rtitionen. Nyfundne plader bliver oprettet og ikke eksisterende bliver slettet.";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(237, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(292, 45);
            this.label2.TabIndex = 12;
            this.label2.Text = "Inventory på en plade, får Kiwi\'en til at scanne alle rør på pladen. Nyfundne rør" +
    " bliver oprettet og manglende bliver slettet.";
            // 
            // InventoryUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._partitionComboBox);
            this.Controls.Add(this._inventoryPlateButton);
            this.Controls.Add(this._plateTextBox);
            this.Controls.Add(this._inventoryPartitionButton);
            this.Name = "InventoryUserControl";
            this.Size = new System.Drawing.Size(547, 418);
            this.VisibleChanged += new System.EventHandler(this.InventoryUserControl_VisibleChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _inventoryPartitionButton;
        private System.Windows.Forms.TextBox _plateTextBox;
        private System.Windows.Forms.Button _inventoryPlateButton;
        private System.Windows.Forms.ComboBox _partitionComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}
