﻿namespace KiwiExtension
{
    partial class ConsolidateUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConsolidateUserControl));
            this._consolidateButton = new System.Windows.Forms.Button();
            this._consolidateComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _consolidateButton
            // 
            this._consolidateButton.Enabled = false;
            this._consolidateButton.Location = new System.Drawing.Point(12, 40);
            this._consolidateButton.Name = "_consolidateButton";
            this._consolidateButton.Size = new System.Drawing.Size(86, 23);
            this._consolidateButton.TabIndex = 1;
            this._consolidateButton.Text = "Konsolider";
            this._consolidateButton.UseVisualStyleBackColor = true;
            this._consolidateButton.Click += new System.EventHandler(this._consolidateButton_Click);
            // 
            // _consolidateComboBox
            // 
            this._consolidateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._consolidateComboBox.FormattingEnabled = true;
            this._consolidateComboBox.Location = new System.Drawing.Point(12, 13);
            this._consolidateComboBox.Name = "_consolidateComboBox";
            this._consolidateComboBox.Size = new System.Drawing.Size(86, 21);
            this._consolidateComboBox.TabIndex = 2;
            this._consolidateComboBox.SelectedIndexChanged += new System.EventHandler(this._consolidateComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(119, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(413, 79);
            this.label1.TabIndex = 3;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // ConsolidateUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label1);
            this.Controls.Add(this._consolidateComboBox);
            this.Controls.Add(this._consolidateButton);
            this.Name = "ConsolidateUserControl";
            this.Size = new System.Drawing.Size(547, 418);
            this.VisibleChanged += new System.EventHandler(this.ConsolidateUserControl_VisibleChanged);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _consolidateButton;
        private System.Windows.Forms.ComboBox _consolidateComboBox;
        private System.Windows.Forms.Label label1;
    }
}
