﻿using DNBTools;
using System;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace KiwiExtension
{
    public partial class InfoUserControl : KiwiUserControl
    {
        public InfoUserControl()
        {
            InitializeComponent();
        }

        private void InfoUserControl_VisibleChanged(object sender, EventArgs e)
        {
            _plateInfoTextBox.Text = string.Empty;
            _tubeInfoTextBox.Text = string.Empty;
        }

        internal void EnableUI(bool enable)
        {
            _plateInfoButton.Enabled = enable;
            _plateInfoTextBox.Enabled = enable;
            _tubeInfoButton.Enabled = enable;
            _tubeInfoTextBox.Enabled = enable;
        }

        private void _plateInfoButton_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                EnableUI(false);
                int orderId = KiwiEntityExtension.OrderId++;
                //Tuple<string, string> response = _kiwiCommunication.GetPlateInfo(orderId, _operatorName, _plateInfoTextBox.Text);
                //XmlDocument doc = new XmlDocument();
                //doc.LoadXml(response.Item2);
                //StringBuilder sb = new StringBuilder();
                //sb.Append("Resultat: ");
                //sb.Append(doc.SelectSingleNode("STXRequest/Answer/Status").InnerText);
                //string errMsg = doc.SelectSingleNode("STXRequest/Answer/ErrMsg").InnerText;
                //if (!string.IsNullOrWhiteSpace(errMsg))
                //{
                //    sb.Append(Environment.NewLine);
                //    sb.Append(Environment.NewLine);
                //    sb.Append(errMsg);
                //    sb.Append(" (");
                //    sb.Append(doc.SelectSingleNode("STXRequest/Answer/ErrCode").InnerText);
                //    sb.Append(")");
                //}
                //else
                //{
                //    sb.Append(Environment.NewLine);
                //    sb.AppendLine("Plade: " + doc.SelectSingleNode("STXRequest/Plates/PltBCR").InnerText);
                //    foreach (XmlNode node in doc.SelectNodes("STXRequest/Plates/PltPos"))
                //    {
                //        sb.AppendLine("  " + node.SelectSingleNode("./PYA").InnerXml + node.SelectSingleNode("./PX").InnerXml + " " + node.SelectSingleNode("./PTb/TpBCR").InnerXml);
                //    }
                //}
                //MessageBox.Show(sb.ToString());
                EnableUI(true);
            }
        }

        private void _tubeInfoButton_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                EnableUI(false);
                int orderId = KiwiEntityExtension.OrderId++;
                //Tuple<string, string> response = _kiwiCommunication.GetTubeInfo(orderId, _operatorName, _tubeInfoTextBox.Text);
                //XmlDocument doc = new XmlDocument();
                //doc.LoadXml(response.Item2);
                //StringBuilder sb = new StringBuilder();
                //sb.Append("Resultat: ");
                //sb.Append(doc.SelectSingleNode("STXRequest/Answer/Status").InnerText);
                //string errMsg = doc.SelectSingleNode("STXRequest/Answer/ErrMsg").InnerText;
                //if (!string.IsNullOrWhiteSpace(errMsg))
                //{
                //    sb.Append(Environment.NewLine);
                //    sb.Append(Environment.NewLine);
                //    sb.Append(errMsg);
                //    sb.Append(" (");
                //    sb.Append(doc.SelectSingleNode("STXRequest/Answer/ErrCode").InnerText);
                //    sb.Append(")");
                //}
                //else
                //{
                //    sb.Append(Environment.NewLine);
                //    sb.AppendLine("Prøve: " + doc.SelectSingleNode("STXRequest/Tubes/Tube").InnerText);
                //    XmlNode plateNode = doc.SelectSingleNode("STXRequest/Tubes/Plate");
                //    if (plateNode != null)
                //    {
                //        sb.AppendLine("  Plade:    " + doc.SelectSingleNode("STXRequest/Tubes/Plate/PltBCR").InnerText);
                //        sb.AppendLine("  Pos:      " + doc.SelectSingleNode("STXRequest/Tubes/PltPos/PYA").InnerText + doc.SelectSingleNode("STXRequest/Tubes/PltPos/PX").InnerText);
                //        sb.AppendLine("  Location: " + doc.SelectSingleNode("STXRequest/Tubes/Plate/Location/PLName").InnerText);
                //    }
                //}
                //MessageBox.Show(sb.ToString());
                EnableUI(true);
            }
        }
    }
}
