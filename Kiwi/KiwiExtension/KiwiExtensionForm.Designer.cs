﻿namespace KiwiExtension
{
    partial class KiwiExtensionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._tabControl = new System.Windows.Forms.TabControl();
            this._samplesTabPage = new System.Windows.Forms.TabPage();
            this._platesTabPage = new System.Windows.Forms.TabPage();
            this._inventoryTabPage = new System.Windows.Forms.TabPage();
            this._toolsTabPage = new System.Windows.Forms.TabPage();
            this._runXmlUserTabPage = new System.Windows.Forms.TabPage();
            this._nautilusTabPage = new System.Windows.Forms.TabPage();
            this._samplesUserControl = new KiwiExtension.SamplesUserControl();
            this._platesUserControl = new KiwiExtension.PlatesUserControl();
            this._inventoryUserControl = new KiwiExtension.InventoryUserControl();
            this._toolsUserControl = new KiwiExtension.ToolsUserControl();
            this._runXmlUserControl = new KiwiExtension.RunXmlUserControl();
            this._nautilusUserControl = new KiwiExtension.NautilusUserControl();
            this._tabControl.SuspendLayout();
            this._samplesTabPage.SuspendLayout();
            this._platesTabPage.SuspendLayout();
            this._inventoryTabPage.SuspendLayout();
            this._toolsTabPage.SuspendLayout();
            this._runXmlUserTabPage.SuspendLayout();
            this._nautilusTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tabControl
            // 
            this._tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tabControl.Controls.Add(this._samplesTabPage);
            this._tabControl.Controls.Add(this._platesTabPage);
            this._tabControl.Controls.Add(this._inventoryTabPage);
            this._tabControl.Controls.Add(this._toolsTabPage);
            this._tabControl.Controls.Add(this._runXmlUserTabPage);
            this._tabControl.Controls.Add(this._nautilusTabPage);
            this._tabControl.Location = new System.Drawing.Point(12, 19);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SelectedIndex = 0;
            this._tabControl.Size = new System.Drawing.Size(577, 412);
            this._tabControl.TabIndex = 0;
            // 
            // _samplesTabPage
            // 
            this._samplesTabPage.Controls.Add(this._samplesUserControl);
            this._samplesTabPage.Location = new System.Drawing.Point(4, 22);
            this._samplesTabPage.Name = "_samplesTabPage";
            this._samplesTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._samplesTabPage.Size = new System.Drawing.Size(569, 386);
            this._samplesTabPage.TabIndex = 0;
            this._samplesTabPage.Text = "Prøver";
            this._samplesTabPage.UseVisualStyleBackColor = true;
            // 
            // _platesTabPage
            // 
            this._platesTabPage.Controls.Add(this._platesUserControl);
            this._platesTabPage.Location = new System.Drawing.Point(4, 22);
            this._platesTabPage.Name = "_platesTabPage";
            this._platesTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._platesTabPage.Size = new System.Drawing.Size(569, 386);
            this._platesTabPage.TabIndex = 1;
            this._platesTabPage.Text = "Plader";
            this._platesTabPage.UseVisualStyleBackColor = true;
            // 
            // _inventoryTabPage
            // 
            this._inventoryTabPage.Controls.Add(this._inventoryUserControl);
            this._inventoryTabPage.Location = new System.Drawing.Point(4, 22);
            this._inventoryTabPage.Name = "_inventoryTabPage";
            this._inventoryTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._inventoryTabPage.Size = new System.Drawing.Size(569, 386);
            this._inventoryTabPage.TabIndex = 2;
            this._inventoryTabPage.Text = "Inventory";
            this._inventoryTabPage.UseVisualStyleBackColor = true;
            // 
            // _toolsTabPage
            // 
            this._toolsTabPage.Controls.Add(this._toolsUserControl);
            this._toolsTabPage.Location = new System.Drawing.Point(4, 22);
            this._toolsTabPage.Name = "_toolsTabPage";
            this._toolsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._toolsTabPage.Size = new System.Drawing.Size(569, 386);
            this._toolsTabPage.TabIndex = 3;
            this._toolsTabPage.Text = "Værktøjer";
            this._toolsTabPage.UseVisualStyleBackColor = true;
            // 
            // _runXmlUserTabPage
            // 
            this._runXmlUserTabPage.Controls.Add(this._runXmlUserControl);
            this._runXmlUserTabPage.Location = new System.Drawing.Point(4, 22);
            this._runXmlUserTabPage.Name = "_runXmlUserTabPage";
            this._runXmlUserTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._runXmlUserTabPage.Size = new System.Drawing.Size(569, 386);
            this._runXmlUserTabPage.TabIndex = 4;
            this._runXmlUserTabPage.Text = "Run XML";
            this._runXmlUserTabPage.UseVisualStyleBackColor = true;
            // 
            // _nautilusTabPage
            // 
            this._nautilusTabPage.Controls.Add(this._nautilusUserControl);
            this._nautilusTabPage.Location = new System.Drawing.Point(4, 22);
            this._nautilusTabPage.Name = "_nautilusTabPage";
            this._nautilusTabPage.Size = new System.Drawing.Size(569, 386);
            this._nautilusTabPage.TabIndex = 5;
            this._nautilusTabPage.Text = "Nautilus";
            this._nautilusTabPage.UseVisualStyleBackColor = true;
            // 
            // _samplesUserControl
            // 
            this._samplesUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._samplesUserControl.Location = new System.Drawing.Point(3, 3);
            this._samplesUserControl.Name = "_samplesUserControl";
            this._samplesUserControl.Size = new System.Drawing.Size(563, 380);
            this._samplesUserControl.TabIndex = 0;
            // 
            // _platesUserControl
            // 
            this._platesUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._platesUserControl.Location = new System.Drawing.Point(3, 3);
            this._platesUserControl.Name = "_platesUserControl";
            this._platesUserControl.Size = new System.Drawing.Size(563, 380);
            this._platesUserControl.TabIndex = 0;
            // 
            // _inventoryUserControl
            // 
            this._inventoryUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._inventoryUserControl.Location = new System.Drawing.Point(3, 3);
            this._inventoryUserControl.Name = "_inventoryUserControl";
            this._inventoryUserControl.Size = new System.Drawing.Size(563, 380);
            this._inventoryUserControl.TabIndex = 0;
            // 
            // _toolsUserControl
            // 
            this._toolsUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._toolsUserControl.Location = new System.Drawing.Point(3, 3);
            this._toolsUserControl.Name = "_toolsUserControl";
            this._toolsUserControl.Size = new System.Drawing.Size(563, 380);
            this._toolsUserControl.TabIndex = 0;
            // 
            // _runXmlUserControl
            // 
            this._runXmlUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._runXmlUserControl.Location = new System.Drawing.Point(3, 3);
            this._runXmlUserControl.Name = "_runXmlUserControl";
            this._runXmlUserControl.Size = new System.Drawing.Size(563, 380);
            this._runXmlUserControl.TabIndex = 0;
            // 
            // _nautilusUserControl
            // 
            this._nautilusUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._nautilusUserControl.Location = new System.Drawing.Point(0, 0);
            this._nautilusUserControl.Name = "_nautilusUserControl";
            this._nautilusUserControl.Size = new System.Drawing.Size(569, 386);
            this._nautilusUserControl.TabIndex = 0;
            // 
            // KiwiExtensionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 443);
            this.Controls.Add(this._tabControl);
            this.MinimumSize = new System.Drawing.Size(583, 414);
            this.Name = "KiwiExtensionForm";
            this.Text = "Kiwi";
            this.Load += new System.EventHandler(this.KiwiExtensionForm_Load);
            this._tabControl.ResumeLayout(false);
            this._samplesTabPage.ResumeLayout(false);
            this._platesTabPage.ResumeLayout(false);
            this._inventoryTabPage.ResumeLayout(false);
            this._toolsTabPage.ResumeLayout(false);
            this._runXmlUserTabPage.ResumeLayout(false);
            this._nautilusTabPage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl _tabControl;
        private System.Windows.Forms.TabPage _samplesTabPage;
        private SamplesUserControl _samplesUserControl;
        private System.Windows.Forms.TabPage _platesTabPage;
        private PlatesUserControl _platesUserControl;
        private System.Windows.Forms.TabPage _inventoryTabPage;
        private InventoryUserControl _inventoryUserControl;
        private System.Windows.Forms.TabPage _toolsTabPage;
        private ToolsUserControl _toolsUserControl;
        private System.Windows.Forms.TabPage _runXmlUserTabPage;
        private RunXmlUserControl _runXmlUserControl;
        private System.Windows.Forms.TabPage _nautilusTabPage;
        private NautilusUserControl _nautilusUserControl;


    }
}