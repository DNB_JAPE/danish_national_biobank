﻿namespace TestKiwiDriver
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.kiwiUserControl1 = new KiwiExtension.KiwiUserControl();
            this.txtTube = new System.Windows.Forms.TextBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtTubeBC = new System.Windows.Forms.TextBox();
            this.txtPlateBC = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTubeID = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // kiwiUserControl1
            // 
            this.kiwiUserControl1.Location = new System.Drawing.Point(219, 131);
            this.kiwiUserControl1.Name = "kiwiUserControl1";
            this.kiwiUserControl1.Size = new System.Drawing.Size(150, 150);
            this.kiwiUserControl1.TabIndex = 0;
            // 
            // txtTube
            // 
            this.txtTube.Location = new System.Drawing.Point(639, 38);
            this.txtTube.Name = "txtTube";
            this.txtTube.Size = new System.Drawing.Size(100, 20);
            this.txtTube.TabIndex = 2;
            // 
            // btnStart
            // 
            this.btnStart.Enabled = false;
            this.btnStart.Location = new System.Drawing.Point(691, 162);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 3;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(158, 35);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(211, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Transfer Tube to plate";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtTubeBC
            // 
            this.txtTubeBC.Location = new System.Drawing.Point(269, 65);
            this.txtTubeBC.Name = "txtTubeBC";
            this.txtTubeBC.Size = new System.Drawing.Size(100, 20);
            this.txtTubeBC.TabIndex = 5;
            // 
            // txtPlateBC
            // 
            this.txtPlateBC.Location = new System.Drawing.Point(269, 117);
            this.txtPlateBC.Name = "txtPlateBC";
            this.txtPlateBC.Size = new System.Drawing.Size(100, 20);
            this.txtPlateBC.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(155, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Tube  Barcode";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(155, 124);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Plate";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(155, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Tube  ID";
            // 
            // txtTubeID
            // 
            this.txtTubeID.Location = new System.Drawing.Point(269, 91);
            this.txtTubeID.Name = "txtTubeID";
            this.txtTubeID.Size = new System.Drawing.Size(100, 20);
            this.txtTubeID.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtTubeID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPlateBC);
            this.Controls.Add(this.txtTubeBC);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.txtTube);
            this.Controls.Add(this.kiwiUserControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private KiwiExtension.KiwiUserControl kiwiUserControl1;
        private System.Windows.Forms.TextBox txtTube;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtTubeBC;
        private System.Windows.Forms.TextBox txtPlateBC;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTubeID;
    }
}

