﻿using DNBTools;
using DNBTools.DataClasses;
using DNBTools.Kiwi;
using DNBTools.Nautilus;
using KiwiExtension;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Configuration = System.Configuration.Configuration;

namespace TestKiwiDriver
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void samplesUserControl1_Load(object sender, EventArgs e)
        {

        }

        private void samplesUserControl1_Load_1(object sender, EventArgs e)
        {
            //samplesUserControl1.Initialize()
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            List<BiobankEntity> samples = new List<BiobankEntity>();

            Sample sample = new Sample("", txtTube.Text);
            samples.Add(sample);
            
            string containerTypeId = "42";
            KiwiExtension.Configuration config;
            config = new KiwiExtension.Configuration(containerTypeId);
            LimsCommunication limsCom = new LimsCommunication();
            
            // Create KiwiCommunication
            SysLog.Log(string.Format("Create Kiwi communication. ContainerTypeID: {0}", containerTypeId), LogLevel.Info);
            KiwiCommunication kiwiCom = null;
            try
            {
                //kiwiCom = new KiwiCommunication( config.DefaultConfig.StoreAddress, config.DefaultConfig.StoreAddress, config.DefaultConfig.StorePort);
                kiwiCom = new KiwiCommunication("KIWI2-PC", "KIWI2-PC", 3333);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Kunne ikke Oprette forbindelse til Kiwi: {0} ", ex.Message), LogLevel.Error);
                MessageBox.Show("Kunne ikke Oprette forbindelse til Kiwi: " + ex.Message);
                return;
            }
            // Create LimsCommunication
            SysLog.Log("Create Lims communication", LogLevel.Info);
            LimsCommunication limsCommunication = null;
            try
            {
                limsCommunication = new LimsCommunication("Data Source=(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 172.16.0.132)(PORT = 1521)))(CONNECT_DATA =(SERVICE_NAME = nautilusp)));User Id=lims_sys;Password=limssys_p;");
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Kunne ikke Oprette forbindelse til Nautilus DB: {0} ", ex.Message), LogLevel.Error);
                MessageBox.Show("Kunne ikke Oprette forbindelse til Nautilus DB: " + ex.Message);
                return;
            }

            Tuple<string, string> responseTube = kiwiCom.GetTubeInfo(KiwiEntityExtension.OrderId++, "ADMIN", txtTube.Text);
            if (responseTube.Item1 == "ERR")
            {
                SysLog.Log("Fejl under tjek af plade info: " + responseTube.Item2, LogLevel.Warning);
                MessageBox.Show("Fejl under tjek af plade info: " + responseTube.Item2);
                //EnableUI(true);
                return;
            }
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(responseTube.Item2);
            XmlNode node = doc.SelectSingleNode("/STXRequest/Tubes/Plate/PltBCR");
            string TubeBC = node.InnerText;





            //samplesUserControl1.Initialize(samples, config, limsCom, kiwiCom);

        }

        protected KiwiExtension.Configuration _config;
        private void button1_Click(object sender, EventArgs e)
        {
            // Transfer tube to plate...

            List<BiobankEntity> samples = new List<BiobankEntity>();

            Sample sample = new Sample("", txtTubeBC.Text);
            samples.Add(sample);

            string containerTypeId = "42";
            KiwiExtension.Configuration config;
            _config = new KiwiExtension.Configuration(containerTypeId);
            LimsCommunication limsCom = new LimsCommunication();

            // Create KiwiCommunication
            SysLog.Log(string.Format("Create Kiwi communication. ContainerTypeID: {0}", containerTypeId), LogLevel.Info);
            KiwiCommunication kiwiCom = null;
            try
            {
                //kiwiCom = new KiwiCommunication( config.DefaultConfig.StoreAddress, config.DefaultConfig.StoreAddress, config.DefaultConfig.StorePort);
                kiwiCom = new KiwiCommunication("KIWI2-PC", "KIWI2-PC", 3333);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Kunne ikke Oprette forbindelse til Kiwi: {0} ", ex.Message), LogLevel.Error);
                MessageBox.Show("Kunne ikke Oprette forbindelse til Kiwi: " + ex.Message);
                return;
            }
            // Create LimsCommunication
            SysLog.Log("Create Lims communication", LogLevel.Info);
            LimsCommunication limsCommunication = null;
            try
            {
                limsCommunication = new LimsCommunication("Data Source=(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 172.16.0.132)(PORT = 1521)))(CONNECT_DATA =(SERVICE_NAME = nautilusp)));User Id=lims_sys;Password=limssys_p;");
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Kunne ikke Oprette forbindelse til Nautilus DB: {0} ", ex.Message), LogLevel.Error);
                MessageBox.Show("Kunne ikke Oprette forbindelse til Nautilus DB: " + ex.Message);
                return;
            }

            //Tuple<string, string> responseTube = kiwiCom.GetTubeInfo(KiwiEntityExtension.OrderId++, "ADMIN", txtTube.Text);
            //if (responseTube.Item1 == "ERR")
            //{
            //    SysLog.Log("Fejl under tjek af plade info: " + responseTube.Item2, LogLevel.Warning);
            //    MessageBox.Show("Fejl under tjek af plade info: " + responseTube.Item2);
            //    //EnableUI(true);
            //    return;
            //}
            //XmlDocument doc = new XmlDocument();
            //doc.LoadXml(responseTube.Item2);
            //XmlNode node = doc.SelectSingleNode("/STXRequest/Tubes/Plate/PltBCR");
            //string TubeBC = node.InnerText;

           // string selectedPlate = txtPlateBC.Text;
            string tubeBC = txtTubeBC.Text;
            string tubeID = txtTubeID.Text;

            Tuple<string, string> responseTube = kiwiCom.GetTubeInfo(KiwiEntityExtension.OrderId++, "ADMIN", tubeBC);
            if (responseTube.Item1 == "ERR")
            {
                SysLog.Log("Fejl under tjek af plade info: " + responseTube.Item2, LogLevel.Warning);
                MessageBox.Show("Fejl under tjek af plade info: " + responseTube.Item2);
            }

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(responseTube.Item2);
            XmlNode node = doc.SelectSingleNode("/STXRequest/Tubes/Plate/PltBCR");
            string selectedPlate = node.InnerText.Trim();
            
            UpdateNautilus myNautilus = new UpdateNautilus(selectedPlate, limsCommunication, kiwiCom);
            myNautilus.FilePath = @"\\srv-dnb-app03\LhopFiles\PROJECTS\Nautilus\Edit_Plate_ExistingAliquots\";
            Sample mySample = new Sample(tubeID, tubeBC);
            myNautilus.Content.Add(mySample);
            myNautilus.BuildNautilusImportFile();
            //limsCommunication.MoveSampleToCarrier(tubeBC, selectedPlate, "A", "1");
        }
    }
}
