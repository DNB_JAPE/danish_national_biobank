﻿using System;

namespace DNBTools
{
    public class DNBTools
    {
        public const string MX05 = "0.50mL_Matrix";
        public const string MX075 = "0.75mL_Matrix";
        public const string MX10 = "1.00mL_Matrix";
        public const string MX14 = "1.40mL_Matrix";

        public static string ContainerTypeNameFromType(ContainerType containerType)
        {
            switch(containerType)
            {
                case ContainerType.MX05:
                    {
                        return MX05;
                    }
                case ContainerType.MX075:
                    {
                        return MX075;
                    }
                case ContainerType.MX10:
                    {
                        return MX10;
                    }
                case ContainerType.MX14:
                    {
                        return MX14;
                    }
                default:
                    {
                        return "UNKNOWN";
                    }
            }
        }

        /// <summary>
        /// Parse a string into a plate type.
        /// Both the enum literal and the Nautilus name are valid values.
        /// </summary>
        /// <param name="plateTypeName"></param>
        /// <returns></returns>
        public static PlateType ParsePlateType(string plateTypeName)
        {
            if(string.IsNullOrWhiteSpace(plateTypeName))
            {
                return PlateType.UNKNOWN;
            }
            PlateType parsed;
            if (Enum.TryParse(plateTypeName, out parsed))
            {
                return parsed;
            }
            else
            {
                switch (plateTypeName)
                {
                    case MX05:
                        return PlateType.MX05;
                    case MX075:
                        return PlateType.MX075;
                    case MX10:
                        return PlateType.MX10;
                    case MX14:
                        return PlateType.MX14;
                    default:
                        return PlateType.UNKNOWN;
                }
            }
        }

        public static ContainerType ParseSampleType(string containerTypeName)
        {
            if (string.IsNullOrWhiteSpace(containerTypeName))
            {
                return ContainerType.UNKNOWN;
            }
            return (ContainerType)Enum.Parse(typeof(ContainerType), containerTypeName);
        }

        public static ContainerType PlateTypeToSampleType(PlateType plateType)
        {
            try
            {
                return (ContainerType)Enum.Parse(typeof(ContainerType), plateType.ToString());
            }
            catch
            {
                return ContainerType.UNKNOWN;
            }
        }

        public static PlateType SampleTypeToPlateType(ContainerType containerType)
        {
            try
            {
                return (PlateType)Enum.Parse(typeof(PlateType), containerType.ToString());
            }
            catch
            {
                return PlateType.UNKNOWN;
            }
        }

        /// <summary>
        /// Get sample type from plate reference.
        /// </summary>
        /// <param name="plateRef"></param>
        /// <returns></returns>
        public static ContainerType DetermineSampleType(string plateRef)
        {
            ContainerType containerType = ContainerType.UNKNOWN;
            if (plateRef.StartsWith("NPKU", StringComparison.OrdinalIgnoreCase))
            {
                containerType = ContainerType.NPKU;
            }
            else if (plateRef.StartsWith("OPKU", StringComparison.OrdinalIgnoreCase))
            {
                containerType = ContainerType.OPKU;
            }
            else if (plateRef.StartsWith("CR", StringComparison.OrdinalIgnoreCase))
            {
                containerType = ContainerType.CR;
            }
            else if (plateRef.StartsWith("MR0", StringComparison.OrdinalIgnoreCase))
            {
                containerType = ContainerType.MX05;
            }
            else if (plateRef.StartsWith("MR1", StringComparison.OrdinalIgnoreCase))
            {
                containerType = ContainerType.MX075;
            }
            else if (plateRef.StartsWith("600", StringComparison.OrdinalIgnoreCase))
            {
                containerType = ContainerType.MX05; 
            }
            else if (plateRef.StartsWith("500", StringComparison.OrdinalIgnoreCase))
            {
                containerType = ContainerType.MX05;
            }
            else if (plateRef.StartsWith("400", StringComparison.OrdinalIgnoreCase))
            {
                containerType = ContainerType.MX05;
            }
            else if (plateRef.StartsWith("BS", StringComparison.OrdinalIgnoreCase))
            {
                containerType = ContainerType.MX05;
            }
            return containerType;
        }

        /// <summary>
        /// Get the next position on a Matrix plate.
        /// </summary>
        /// <param name="position">Row, Column</param>
        /// <returns>Row, Column</returns>
        public static Tuple<string, int> NextPositionMatrix(Tuple<string, int> position)
        {
            if (position == null)
            {
                return new Tuple<string, int>("A", 1);
            }
            else if (position.Item1 == "H" && position.Item2 == 12)
            {
                return null;
            }
            else
            {
                string row = position.Item1;
                int column = position.Item2;
                if (column == 12)
                {
                    column = 1;
                    switch (row)
                    {
                        case "A":
                            {
                                row = "B";
                                break;
                            }
                        case "B":
                            {
                                row = "C";
                                break;
                            }
                        case "C":
                            {
                                row = "D";
                                break;
                            }
                        case "D":
                            {
                                row = "E";
                                break;
                            }
                        case "E":
                            {
                                row = "F";
                                break;
                            }
                        case "F":
                            {
                                row = "G";
                                break;
                            }
                        case "G":
                            {
                                row = "H";
                                break;
                            }
                        default:
                            {
                                return null;
                            }
                    }
                }
                else
                {
                    column++;
                }
                return new Tuple<string, int>(row, column);
            }
        }
    }
}
