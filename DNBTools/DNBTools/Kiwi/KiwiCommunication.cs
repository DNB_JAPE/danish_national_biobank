﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace DNBTools.Kiwi
{
    /// <summary>
    /// Handles all communication with Kiwi.
    /// </summary>
    public class KiwiCommunication : IDisposable
    {
        private string _deviceName;
        private string _stxAddress;
        private int _stxPort;

        /// <summary>
        /// Constructor.
        /// </summary>
        public KiwiCommunication(string name, string address, int port)
        {
            _deviceName = name;
            _stxAddress = address;
            _stxPort = port;
        }

        /// <summary>
        /// Release all resources.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// Send a request to the Kiwi store.
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        private string SendOrder(string order)
        {
            // shortenLog is used to minimize the log.
            // CmdStatus is sent every 2-3 seconds and will clutter the log.
            // If there is an error in the result it will be logged.
            bool shortenLog = order.Contains("<Cmd>GetCmdStatus</Cmd>");

            SysLog.Log(string.Format("Send order to Kiwi: {0}", shortenLog ? "Check Command Status": order), LogLevel.Info);

            AsynchronousClient STXSCK = new AsynchronousClient();
            STXSCK.StartClient(_stxAddress, _stxPort);
            String result = STXSCK.Send(order + Environment.NewLine);
            STXSCK.StopClient();

            shortenLog = result.Contains("<Cmd>GetCmdStatus</Cmd>") && result.Contains("<Status>RUN</Status>") && result.Contains("<ErrCode>0</ErrCode>");

            SysLog.Log(string.Format("Done sending order to Kiwi. Result: {0}", shortenLog ? "Command Status OK": result), LogLevel.Info);
            return result;
        }

        /// <summary>
        /// Run xml directly.
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public Tuple<string, string> RunXml(string xml)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                string result = SendOrder(xml);
                return new Tuple<string, string>(sb.ToString(), result);
            }
            catch (Exception ex)
            {
                SysLog.Log("Failure: " + ex.Message, LogLevel.Error);
                return new Tuple<string, string>(sb.ToString(), "Failure: " + ex.Message);
            }
        }

        ///// <summary>
        ///// Get the system status
        ///// </summary>
        ///// <returns>Ok or error</returns>
        //public Tuple<string, string> GetSystemStatus(int commandId, string operatorName)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    try
        //    {
        //        sb.AppendLine("<STXRequest>");
        //        sb.AppendLine("  <Command>");
        //        sb.AppendLine("    <ID>" + commandId + "</ID>");
        //        sb.AppendLine("    <Cmd>GetSystemStatus</Cmd>");
        //        sb.AppendLine("    <User>" + operatorName + "</User>");
        //        sb.AppendLine("    <Partition></Partition>");
        //        sb.AppendLine("    <Transferstation></Transferstation>");
        //        sb.AppendLine("  </Command>");
        //        sb.AppendLine("  <Device>");
        //        sb.AppendLine("    <Name>" + _deviceName + "</Name>");
        //        sb.AppendLine("  </Device>");
        //        sb.AppendLine("</STXRequest>");
        //        string result = SendOrder(sb.ToString());
        //        return new Tuple<string, string>(sb.ToString(), result);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new Tuple<string, string>(sb.ToString(), "Failure: " + ex.Message);
        //    }
        //}

        /// <summary>
        /// Get status of command.
        /// This will be RUN while the command is running. 
        /// The first call after the command ends, will return OK or ERR.
        /// Subsequent calls will return ERR.
        /// </summary>
        /// <param name="commandId"></param>
        /// <returns></returns>
        public Tuple<string, string> GetCmdStatus(int commandId, string operatorName, int queryId)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.AppendLine("<STXRequest>");
                sb.AppendLine("  <Command>");
                sb.AppendLine("    <ID>" + queryId + "</ID>");
                sb.AppendLine("    <Cmd>GetCmdStatus</Cmd>");
                sb.AppendLine("    <User>" + operatorName.ToUpper() + "</User>");
                sb.AppendLine("    <Partition></Partition>");
                sb.AppendLine("    <Transferstation></Transferstation>");
                sb.AppendLine("  </Command>");
                sb.AppendLine("</STXRequest>");
                string result = SendOrder(sb.ToString());
                return new Tuple<string, string>(sb.ToString(), result);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("GetCMDStatus failure: {0}", ex.Message), LogLevel.Error);
                return new Tuple<string, string>(sb.ToString(), "Failure: " + ex.Message);
            }
        }

        //public Tuple<string, string> GetCmdsStatus(int commandId, string operatorName)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    try
        //    {
        //        sb.AppendLine("<STXRequest>");
        //        sb.AppendLine("  <Command>");
        //        sb.AppendLine("    <ID>" + commandId + "</ID>");
        //        sb.AppendLine("    <Cmd>GetCommandsInfo</Cmd>");
        //        sb.AppendLine("    <User>" + operatorName + "</User>");
        //        sb.AppendLine("    <Partition></Partition>");
        //        sb.AppendLine("    <Transferstation></Transferstation>");
        //        sb.AppendLine("  </Command>");
        //        sb.AppendLine("</STXRequest>");
        //        string result = SendOrder(sb.ToString());
        //        return new Tuple<string, string>(sb.ToString(), result);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new Tuple<string, string>(sb.ToString(), "Failure: " + ex.Message);
        //    }
        //}

        /// <summary>
        /// Get full info for a set of plates.
        /// </summary>
        /// <param name="commandId"></param>
        /// <param name="operatorName"></param>
        /// <param name="barcode"></param>
        /// <returns></returns>
        public Tuple<string, string> GetTubeInfo(int commandId, string operatorName, string tube)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.AppendLine("<STXRequest>");
                sb.AppendLine("  <Command>");
                sb.AppendLine("    <ID>" + commandId + "</ID>");
                sb.AppendLine("    <Cmd>GetTubeInfo</Cmd>");
                sb.AppendLine("    <User>" + operatorName.ToUpper() + "</User>");
                sb.AppendLine("    <Partition></Partition>");
                sb.AppendLine("    <Transferstation></Transferstation>");
                sb.AppendLine("  </Command>");
                sb.AppendLine("  <Tubes>");
                sb.AppendLine("    <Tube>" + tube + "</Tube>");
                sb.AppendLine("  </Tubes>");
                sb.AppendLine("</STXRequest>");
                string result = SendOrder(sb.ToString());
                return new Tuple<string, string>(sb.ToString(), result);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("GetTubeInfo failure: {0}", ex.Message), LogLevel.Error);
                return new Tuple<string, string>(sb.ToString(), "Failure: " + ex.Message);
            }
        }

        /// <summary>
        /// Get full info for a set of plates.
        /// </summary>
        /// <param name="commandId"></param>
        /// <param name="operatorName"></param>
        /// <param name="barcode"></param>
        /// <returns></returns>
        public Tuple<string, string> GetPlateInfo(int commandId, string operatorName, string plate)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.AppendLine("<STXRequest>");
                sb.AppendLine("  <Command>");
                sb.AppendLine("    <ID>" + commandId + "</ID>");
                sb.AppendLine("    <Cmd>GetPlateInfo</Cmd>");
                sb.AppendLine("    <User>" + operatorName.ToUpper() + "</User>");
                sb.AppendLine("    <Partition></Partition>");
                sb.AppendLine("    <Transferstation></Transferstation>");
                sb.AppendLine("  </Command>");
                sb.AppendLine("  <Plates>");
                sb.AppendLine("    <Id>0</Id>");        //ESAT 20181119
                sb.AppendLine("    <PltBCR>" + plate + "</PltBCR>");
                sb.AppendLine("    <PltUID></PltUID>"); //ESAT 20181119
                sb.AppendLine("    <PltType></PltType>");
                sb.AppendLine("  </Plates>");
                sb.AppendLine("</STXRequest>");
                string result = SendOrder(sb.ToString());
                return new Tuple<string, string>(sb.ToString(), result);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("GetPlateInfo failure: {0}", ex.Message), LogLevel.Error);
                return new Tuple<string, string>(sb.ToString(), "Failure: " + ex.Message);
            }
        }

        ///// <summary>
        ///// Get full info for a tube.
        ///// </summary>
        ///// <param name="commandId"></param>
        ///// <param name="operatorName"></param>
        ///// <param name="barcode"></param>
        ///// <returns></returns>
        //public Tuple<string, string> GetTubeInfo(int commandId, string operatorName, string tube)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    try
        //    {
        //        sb.AppendLine("<STXRequest>");
        //        sb.AppendLine("  <Command>");
        //        sb.AppendLine("    <ID>" + commandId + "</ID>");
        //        sb.AppendLine("    <Cmd>GetTubeInfo</Cmd>");
        //        sb.AppendLine("    <User>" + operatorName + "</User>");
        //        sb.AppendLine("    <Partition></Partition>");
        //        sb.AppendLine("    <Transferstation></Transferstation>");
        //        sb.AppendLine("  </Command>");
        //        sb.AppendLine("  <Tubes>");
        //        sb.AppendLine("    <Tube>" + tube + "</Tube>");
        //        sb.AppendLine("  </Tubes>");
        //        sb.AppendLine("</STXRequest>");
        //        string result = SendOrder(sb.ToString());
        //        return new Tuple<string, string>(sb.ToString(), result);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new Tuple<string, string>(sb.ToString(), "Failure: " + ex.Message);
        //    }
        //}

        /// <summary>
        /// Move plate to another partition.
        /// </summary>
        /// <param name="commandId"></param>
        /// <param name="operatorName"></param>
        /// <param name="partition"></param>
        /// <param name="plate"></param>
        /// <returns></returns>
        public Tuple<string, string> MovePlate(int commandId, string operatorName, string partition, string transferStation, string containerTypeId, string plateBC)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.AppendLine("<STXRequest>");
                sb.AppendLine("  <Command>");
                sb.AppendLine("    <ID>" + commandId + "</ID>");
                sb.AppendLine("    <Cmd>MovePlate</Cmd>");
                sb.AppendLine("    <User>" + operatorName.ToUpper() + "</User>");
                sb.AppendLine("    <Partition>" + partition + "</Partition>");
                sb.AppendLine("    <Transferstation>" + transferStation + "</Transferstation>");
                sb.AppendLine("  </Command>");
                sb.AppendLine("  <Plates>");
                sb.AppendLine("    <PltBCR>" + plateBC + "</PltBCR>");
                sb.AppendLine("    <PltType>" + ConvertContainerTypeIdPlate(containerTypeId) + "</PltType>");
                sb.AppendLine("  </Plates>");
                sb.AppendLine("</STXRequest>");
                string result = SendOrder(sb.ToString());
                return new Tuple<string, string>(sb.ToString(), result);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("MovePlate failure: {0}", ex.Message), LogLevel.Error);
                return new Tuple<string, string>(sb.ToString(), "Failure: " + ex.Message);
            }
        }
        
        /// <summary>
        /// Add a plate to the store and place it inside.
        /// </summary>
        /// <param name="commandId"></param>
        /// <param name="operatorName"></param>
        /// <param name="partition"></param>
        /// <param name="transferStation"></param>
        /// <param name="plate"></param>
        /// <returns></returns>
        public Tuple<string, string> StorePlate(int commandId, string operatorName, string partition, string transferStation, string containerTypeId, string plateBC, List<Tuple<string, string, string>> samples)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.AppendLine("<STXRequest>");
                sb.AppendLine("  <Command>");
                sb.AppendLine("    <ID>" + commandId + "</ID>");
                sb.AppendLine("    <Cmd>StorePlate</Cmd>");
                sb.AppendLine("    <User>" + operatorName.ToUpper() + "</User>");
                sb.AppendLine("    <Partition>" + partition + "</Partition>");
                sb.AppendLine("    <Transferstation>" + transferStation + "</Transferstation>");
                sb.AppendLine("  </Command>");
                sb.AppendLine("  <Plates>");
                sb.AppendLine("    <PltBCR>" + plateBC + "</PltBCR>");
                foreach (Tuple<string, string, string> sample in samples)
                {
                    sb.AppendLine("    <PltTPos>");
                    sb.AppendLine("      <Id></Id>");
                    sb.AppendLine("      <PX>" + sample.Item3 + "</PX>");
                    sb.AppendLine("      <PY>" + ConvertRow(sample.Item2) + "</PY>"); // A to H columns
                    sb.AppendLine("      <PYA>" + sample.Item2 + "</PYA>"); // A to H columns
                    sb.AppendLine("      <PTV>" + ConvertContainerTypeIdSample(containerTypeId) + "</PTV>");
                    sb.AppendLine("      <PTb>");
                    sb.AppendLine("        <TbBCR>" + sample.Item1 + "</TbBCR>");
                    sb.AppendLine("        <TbT>" + ConvertContainerTypeIdSample(containerTypeId) + "</TbT>");
                    sb.AppendLine("      </PTb>");
                    sb.AppendLine("    </PltTPos>");
                }
                sb.AppendLine("    <PltType>" + ConvertContainerTypeIdPlate(containerTypeId) + "</PltType>");
                sb.AppendLine("  </Plates>");
                sb.AppendLine("</STXRequest>");
                string result = SendOrder(sb.ToString());
                return new Tuple<string,string>(sb.ToString(), result);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("StorePlate failure: {0}", ex.Message), LogLevel.Error);
                return new Tuple<string,string>(sb.ToString(), "Failure: " + ex.Message);
            }
        }

        /// <summary>
        /// Retrieve a plate from the store.
        /// </summary>
        /// <param name="commandId"></param>
        /// <param name="operatorName"></param>
        /// <param name="transferStation"></param>
        /// <param name="containertypeid"></param>
        /// <param name="plates"></param>
        /// <returns></returns>
        public Tuple<string, string> RetrievePlate(int commandId, string operatorName, string transferStation, string containerTypeId, string plateBC)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.AppendLine("<STXRequest>");
                sb.AppendLine("  <Command>");
                sb.AppendLine("    <ID>" + commandId + "</ID>");
                sb.AppendLine("    <Cmd>RetrievePlate</Cmd>");
                sb.AppendLine("    <User>" + operatorName.ToUpper() + "</User>");
                sb.AppendLine("    <Partition></Partition>");
                sb.AppendLine("    <Transferstation>" + transferStation + "</Transferstation>");
                sb.AppendLine("  </Command>");
                sb.AppendLine("  <Plates>");
                sb.AppendLine("    <Id>0</Id>");
                sb.AppendLine("    <PltBCR>" + plateBC + "</PltBCR>");
                sb.AppendLine("    <PltUID></PltUID>");
                sb.AppendLine("    <PltType>" + ConvertContainerTypeIdPlate(containerTypeId) + "</PltType>");
                sb.AppendLine("  </Plates>");
                sb.AppendLine("</STXRequest>");
                string result = SendOrder(sb.ToString());
                return new Tuple<string, string>(sb.ToString(), result);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("RetrievePlate failure: {0}", ex.Message), LogLevel.Error);
                return new Tuple<string, string>(sb.ToString(), "Failure: " + ex.Message);
            }
        }

        /// <summary>
        /// Pick a set of tubes from the store.
        /// </summary>
        /// <param name="commandId"></param>
        /// <param name="operatorName"></param>
        /// <param name="barcodes"></param>
        /// <returns></returns>
        public Tuple<string, string> PickTubes(int commandId, string operatorName, List<string> barcodes)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.AppendLine("<STXRequest>");
                sb.AppendLine("  <Command>");
                sb.AppendLine("    <ID>" + commandId + "</ID>");
                sb.AppendLine("    <Cmd>PickTubes</Cmd>");
                sb.AppendLine("    <User>" + operatorName.ToUpper() + "</User>");
                sb.AppendLine("    <Partition></Partition>");
                sb.AppendLine("    <Transferstation></Transferstation>");
                sb.AppendLine("  </Command>");
                foreach (string barcode in barcodes)
                {
                    sb.AppendLine("  <Tubes>");
                    sb.AppendLine("    <Tube>" + barcode + "</Tube>");
                    sb.AppendLine("  </Tubes>");
                }
                sb.AppendLine("</STXRequest>");
                string result = SendOrder(sb.ToString());
                return new Tuple<string, string>(sb.ToString(), result);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("PickTubes failure: {0}", ex.Message), LogLevel.Error);
                return new Tuple<string, string>(sb.ToString(), "Failure: " + ex.Message);
            }
        }

        /// <summary>
        /// Pick a set of tubes from the store.
        /// </summary>
        /// <param name="commandId"></param>
        /// <param name="operatorName"></param>
        /// <param name="barcodes"></param>
        /// <returns></returns>
        public Tuple<string, string> PickTubesToPlate(int commandId, string operatorName, List<string> barcodes, string plate)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.AppendLine("<STXRequest>");
                sb.AppendLine("  <Command>");
                sb.AppendLine("    <ID>" + commandId + "</ID>");
                sb.AppendLine("    <Cmd>PickTubes</Cmd>");
                sb.AppendLine("    <User>" + operatorName.ToUpper() + "</User>");
                sb.AppendLine("    <Partition></Partition>");
                sb.AppendLine("    <Transferstation></Transferstation>");
                sb.AppendLine("  </Command>");
                foreach (string barcode in barcodes)
                {
                    sb.AppendLine("  <Tubes>");
                    sb.AppendLine("    <Tube>" + barcode + "</Tube>");
                    sb.AppendLine("    <Plate>");
                    sb.AppendLine("      <PltBCR>" + plate + "</PltBCR>");
                    sb.AppendLine("    </Plate>");
                    sb.AppendLine("  </Tubes>");
                }
                sb.AppendLine("</STXRequest>");
                string result = SendOrder(sb.ToString());
                return new Tuple<string, string>(sb.ToString(), result);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("PickTubesToPlate failure: {0}", ex.Message), LogLevel.Error);
                return new Tuple<string, string>(sb.ToString(), "Failure: " + ex.Message);
            }
        }

        ///// <summary>
        ///// Move tubes internally in the store to fill plates.
        ///// </summary>
        ///// <param name="commandId"></param>
        ///// <param name="operatorName"></param>
        ///// <param name="partition"></param>
        ///// <returns></returns>
        //public Tuple<string, string> Consolidate(int commandId, string operatorName, string partition)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    try
        //    {
        //        sb.AppendLine("<STXRequest>");
        //        sb.AppendLine("  <Command>");
        //        sb.AppendLine("    <ID>" + commandId + "</ID>");
        //        sb.AppendLine("    <Cmd>Consolidate</Cmd>");
        //        sb.AppendLine("    <User>" + operatorName + "</User>");
        //        sb.AppendLine("    <Partition>" + partition + "</Partition>");
        //        sb.AppendLine("    <Transferstation></Transferstation>");
        //        sb.AppendLine("  </Command>");
        //        sb.AppendLine("</STXRequest>");
        //        string result = SendOrder(sb.ToString());
        //        return new Tuple<string, string>(sb.ToString(), result);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new Tuple<string, string>(sb.ToString(), "Failure: " + ex.Message);
        //    }
        //}

        /// <summary>
        /// Do inventory check on a partition. This will make the KIWI check plate barcodes throughout the partition.
        /// Newly found plates will be created in the KIWI DB.
        /// </summary>
        /// <param name="commandId"></param>
        /// <param name="operatorName"></param>
        /// <param name="partition"></param>
        /// <returns></returns>
        public Tuple<string, string> DoInventoryOnPartition(int commandId, string operatorName, string partition)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.AppendLine("<STXRequest>");
                sb.AppendLine("  <Command>");
                sb.AppendLine("    <ID>" + commandId + "</ID>");
                sb.AppendLine("    <Cmd>Inventory</Cmd>");
                sb.AppendLine("    <User>" + operatorName.ToUpper() + "</User>");
                sb.AppendLine("    <Partition>" + partition + "</Partition>");
                sb.AppendLine("    <Transferstation></Transferstation>");
                sb.AppendLine("  </Command>");
                sb.AppendLine("</STXRequest>");
                string result = SendOrder(sb.ToString());
                return new Tuple<string, string>(sb.ToString(), result);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("DoInventoryOnPartition failure: {0}", ex.Message), LogLevel.Error);
                return new Tuple<string, string>(sb.ToString(), "Failure: " + ex.Message);
            }
        }

        /// <summary>
        /// Do inventory check on a plate. This will make the KIWI check barcodes throughout the plate.
        /// </summary>
        /// <param name="commandId"></param>
        /// <param name="operatorName"></param>
        /// <param name="plate"></param>
        /// <returns></returns>
        public Tuple<string, string> DoInventoryOnPlate(int commandId, string operatorName, string plate)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.AppendLine("<STXRequest>");
                sb.AppendLine("  <Command>");
                sb.AppendLine("    <ID>" + commandId + "</ID>");
                sb.AppendLine("    <Cmd>Inventory</Cmd>");
                sb.AppendLine("    <User>" + operatorName.ToUpper() + "</User>");
                sb.AppendLine("    <Partition></Partition>");
                sb.AppendLine("    <Transferstation></Transferstation>");
                sb.AppendLine("  </Command>");
                sb.AppendLine("  <Plates>");
                sb.AppendLine("    <Id>0</Id>");
                sb.AppendLine("    <PltBCR>" + plate + "</PltBCR>");
                sb.AppendLine("    <PltUID></PltUID>");
                sb.AppendLine("    <PltType></PltType>");
                sb.AppendLine("  </Plates>");
                sb.AppendLine("</STXRequest>");
                string result = SendOrder(sb.ToString());
                return new Tuple<string, string>(sb.ToString(), result);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("DoInventoryOnPlate failure: {0}", ex.Message), LogLevel.Error);
                return new Tuple<string, string>(sb.ToString(), "Failure: " + ex.Message);
            }
        }

        /// <summary>
        /// Get a list of plates in a given partition.
        /// </summary>
        /// <param name="commandId"></param>
        /// <param name="operatorName"></param>
        /// <param name="partition"></param>
        /// <returns></returns>
        public Tuple<string, string> GetPartitionPlatesList(int commandId, string operatorName, string partition)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.AppendLine("<STXRequest>");
                sb.AppendLine("  <Command>");
                sb.AppendLine("    <ID>" + commandId + "</ID>");
                sb.AppendLine("    <Cmd>GetPartitionPlatesList</Cmd>");
                sb.AppendLine("    <User>" + operatorName.ToUpper() + "</User>");
                sb.AppendLine("    <Partition>" + partition + "</Partition>");
                sb.AppendLine("    <Transferstation></Transferstation>");
                sb.AppendLine("  </Command>");
                sb.AppendLine("</STXRequest>");
                string result = SendOrder(sb.ToString());
                return new Tuple<string, string>(sb.ToString(), result);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("GetPartitionPlatesList failure: {0}", ex.Message), LogLevel.Error);
                return new Tuple<string, string>(sb.ToString(), "Failure: " + ex.Message);
            }
        }

        public Tuple<string, string> Continue(string pickjobId, string operatorName)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.AppendLine("<STXRequest>");
                sb.AppendLine("  <Command>");
                sb.AppendLine("    <ID>" + pickjobId + "</ID>");
                sb.AppendLine("    <Cmd>Continue</Cmd>");
                sb.AppendLine("    <User>" + operatorName.ToUpper() + "</User>");
                sb.AppendLine("    <Partition></Partition>");
                sb.AppendLine("    <Transferstation></Transferstation>");
                sb.AppendLine("  </Command>");
                sb.AppendLine("</STXRequest>");
                string result = SendOrder(sb.ToString());
                return new Tuple<string, string>(sb.ToString(), result);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Continue failure: {0}", ex.Message), LogLevel.Error);
                return new Tuple<string, string>(sb.ToString(), "Failure: " + ex.Message);
            }
        }

        #region Helper Functions
        /// <summary>
        /// Get the Kiwi sample type number from the container type id.
        /// </summary>
        /// <param name="plateTypeName"></param>
        /// <returns></returns>
        public int ConvertContainerTypeIdSample(string containerTypeId)
        {
            switch (containerTypeId)
            {
                case "41":
                    {
                        return 0;
                    }
                case "21":
                    {
                        return 1;
                    }
                case "43":
                    {
                        return 2;
                    }
                case "42":
                    {
                        return 3;
                    }
            }
            throw new ArgumentException("Kan ikke bestemme rørtype ud fra valgte rørtype!");
        }

        /// <summary>
        /// Get the Kiwi plate type number from the container type id.
        /// </summary>
        /// <param name="plateTypeName"></param>
        /// <returns></returns>
        public int ConvertContainerTypeIdPlate(string containerTypeId)
        {
            switch (containerTypeId)
            {
                case "41":
                    {
                        return 0;   // ESAT 2017-11-21 started with 1 before
                    }
                case "21":
                    {
                        return 1;   // ESAT 2017-11-21
                    }
                case "43":
                    {
                        return 3;   // ESAT 2017-11-27
                    }
                case "42":
                    {
                        return 4;   // ESAT 2017-11-27
                    }
            }
            throw new ArgumentException("Kan ikke bestemme pladetype ud fra valgte rørtype!");
        }

        /// <summary>
        /// Convert row as int into letterformat.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private string ConvertRow(string row)
        {
            switch (row)
            {
                case "A":
                    {
                        return "1";
                    }
                case "B":
                    {
                        return "2";
                    }
                case "C":
                    {
                        return "3";
                    }
                case "D":
                    {
                        return "4";
                    }
                case "E":
                    {
                        return "5";
                    }
                case "F":
                    {
                        return "6";
                    }
                case "G":
                    {
                        return "7";
                    }
                case "H":
                    {
                        return "8";
                    }
            }
            throw new ArgumentException("Kan ikke bestemme rørtype ud fra valgte rørtype!");
        }

        /// <summary>
        /// Get a list of partitions valid for the given plate type.
        /// </summary>
        /// <param name="plateType"></param>
        /// <returns></returns>
        public List<string> ValidPartitions(PlateType plateType)
        {
            List<String> retval = new List<string>();
            switch (plateType)
            {
                case PlateType.MX05:
                    {
                        retval.Add("D");
                        retval.Add("E");
                        return retval;
                    }
                case PlateType.MX075:
                    {
                        retval.Add("D");
                        retval.Add("F");
                        return retval;
                    }
                case PlateType.MX10:
                    {
                        retval.Add("A");
                        retval.Add("B");
                        return retval;
                    }
                case PlateType.MX14:
                    {
                        retval.Add("A");
                        retval.Add("C");
                        return retval;
                    }
                default:
                    return null;
            }
        }

        /// <summary>
        /// Get the appropriate partition from the plate type.
        /// </summary>
        /// <param name="plateTypeName"></param>
        /// <returns></returns>
        public string Partition(PlateType plateType)
        {
            switch (plateType)
            {
                case PlateType.MX05:
                    {
                        return "E";
                    }
                case PlateType.MX075:
                    {
                        return "F";
                    }
                case PlateType.MX10:
                    {
                        return "B";
                    }
                case PlateType.MX14:
                    {
                        return "C";
                    }
            }
            throw new ArgumentException("Kan ikke bestemme partition ud fra valgte pladetype!");
        }

        /// <summary>
        /// Get the appropriate transfer station from the plate type.
        /// </summary>
        /// <param name="plateTypeName"></param>
        /// <returns></returns>
        public string TransferStation(PlateType plateType)
        {
            switch (plateType)
            {
                case PlateType.MX05:
                    {
                        return "2";
                    }
                case PlateType.MX075:
                    {
                        return "2";
                    }
                case PlateType.MX10:
                    {
                        return "1";
                    }
                case PlateType.MX14:
                    {
                        return "1";
                    }
            }
            throw new ArgumentException("Kan ikke bestemme partition ud fra valgte pladetype!");
        }

        /// <summary>
        /// Get a user friendly version of the response.
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public string PrettyPrintResponse(string response)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(response);
            string errMsg = doc.SelectSingleNode("STXRequest/Answer/ErrMsg").InnerText;
            if (string.IsNullOrWhiteSpace(errMsg))
            {
                return "Ordren er startet";
            }
            else
            {
                return "Ordren kunne ikke startes: " + errMsg;

            }
        }

        /// <summary>
        /// Pick the status field and possibly error code from the response.
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public Tuple<string, string> StatusFromResponse(string response)
        {
            if(response.StartsWith("Failure:"))
            {
                return new Tuple<string, string>("ERR", response);
            }
            int statusStartindex = response.IndexOf("<Status>") + 8;
            int statusEndindex = response.IndexOf("</Status>");
            string status = response.Substring(statusStartindex, statusEndindex - statusStartindex);
            string error = string.Empty;
            if(status.Equals("ERR"))
            {
                int errorStartindex = response.IndexOf("<ErrMsg>") + 8;
                int errorEndindex = response.IndexOf("</ErrMsg>");
                error = response.Substring(errorStartindex, errorEndindex - errorStartindex);
            }
            return new Tuple<string,string>(status, error);
        }
        #endregion Helper Functions

        /// <summary>
        /// Retrieve a plate from the store.
        /// </summary>
        /// <param name="commandId"></param>
        /// <param name="operatorName"></param>
        /// <param name="bufferDestination"></param>
        /// <param name="containerTypeId"></param>
        /// <param name="plates"></param>
        /// <returns></returns>
        public Tuple<string, string> RetrievePlateToBuffer(int commandId, string operatorName, string bufferDestination, string containerTypeId, string plateBC)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.AppendLine("<STXRequest>");
                sb.AppendLine("  <Command>");
                sb.AppendLine("    <ID>" + commandId + "</ID>");
                sb.AppendLine("    <Cmd>RetrievePlate</Cmd>");
                sb.AppendLine("    <User>" + operatorName.ToUpper() + "</User>");
                sb.AppendLine("    <Partition>" + bufferDestination + "</Partition>");
                sb.AppendLine("    <Transferstation></Transferstation>");
                sb.AppendLine("  </Command>");
                sb.AppendLine("  <Plates>");
                sb.AppendLine("    <Id>0</Id>");
                sb.AppendLine("    <PltBCR>" + plateBC + "</PltBCR>");
                sb.AppendLine("    <PltUID></PltUID>");
                sb.AppendLine("    <PltType>" + ConvertContainerTypeIdPlate(containerTypeId) + "</PltType>");
                sb.AppendLine("  </Plates>");
                sb.AppendLine("</STXRequest>");
                string result = SendOrder(sb.ToString());
                return new Tuple<string, string>(sb.ToString(), result);
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("RetrievePlate failure: {0}", ex.Message), LogLevel.Error);
                return new Tuple<string, string>(sb.ToString(), "Failure: " + ex.Message);
            }
        }


    }
}
