﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNBTools.Brooks
{
    public class BrooksTray
    {
        public string Name { get; set; }
        public List<string> Plates { get; set; }
        public bool IsErrored { get; set; }
        public string ContentType { get; set; }

        public BrooksTray(string name, bool isErrored, string contentType)
        {
            Name = name;
            ContentType = contentType;
            IsErrored = isErrored;
            Plates = new List<string>();
        }
    }
}
