﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text;
using System.Windows.Forms;

namespace DNBTools.Brooks
{
    public class BrooksCommunication : IDisposable
    {
        private Dictionary<BrooksPlateType, string> PlateTubeDictionary = new Dictionary<BrooksPlateType, string>()
        {
            { BrooksPlateType.RACK_CR, "T_CR" },
            { BrooksPlateType.PKU_OLD, "" },
            { BrooksPlateType.PKU_NEW, "" },
            { BrooksPlateType.RACK_075, "T_075" },
            { BrooksPlateType.RACK_05, "T_05" }
        };

        private OracleConnection _cnnNaut;

        /// <summary>
        /// Constructor.
        /// </summary>
        public BrooksCommunication(string connectionString)
        {
            _cnnNaut = new OracleConnection(connectionString);
            _cnnNaut.Open();
        }

        public void Dispose()
        {
            if (_cnnNaut != null)
            {
                _cnnNaut.Close();
            }
            _cnnNaut = null;
        }

        #region Interface
        /// <summary>
        /// Load empty trays into the store.
        /// </summary>
        /// <returns>State object</returns>
        public BrooksOrderState LoadTrays(List<string> trays, string operatorName, int priority, string description)
        {
            Tuple<int, BrooksOrderState> orderIdState = GetNextOrderId();
            if (orderIdState.Item2.ErrorNumber != 0) { return orderIdState.Item2; }
            BrooksOrderState state = IssueOrder(orderIdState.Item1, "LOAD", "PLATE", operatorName, priority, description);
            if (state.ErrorNumber != 0) { return state; }
            foreach (string tray in trays)
            {
                state = AddOrderTray(orderIdState.Item1, tray, 0);
                if (state.ErrorNumber != 0) { return state; }
            }
            return SendOrder(orderIdState.Item1);
        }

        /// <summary>
        /// Load empty plates into the store.
        /// </summary>
        /// <returns>State object</returns>
        public BrooksOrderState LoadPlates(List<string> plates, Dictionary<string, int> trays, string operatorName, int priority, string description) 
        {
            if (plates.Count == 0) { return new BrooksOrderState(); }
            Tuple<int, BrooksOrderState> orderIdState = GetNextOrderId();
            if (orderIdState.Item2.ErrorNumber != 0) { return orderIdState.Item2; }
            BrooksOrderState state = IssueOrder(orderIdState.Item1, "LOAD", "PLATE", operatorName, priority, description);
            if (state.ErrorNumber != 0) { return state; }
            state = AddPlateMethod(orderIdState.Item1, GetPlateType(plates[0]).ToString(), "NO_LID", PlateTubeDictionary[GetPlateType(plates[0])]);
            if (state.ErrorNumber != 0) { return state; }
            foreach (string tray in trays.Keys)
            {
                state = AddOrderTray(orderIdState.Item1, tray, trays[tray]);
                if (state.ErrorNumber != 0) { return state; }
            }
            foreach(string plate in plates)
            {
                state = AddOrderPlate(orderIdState.Item1, plate);
                if (state.ErrorNumber != 0) { return state; }
            }
            return SendOrder(orderIdState.Item1);
        }

        /// <summary>
        /// Load nonempty plates into the store.
        /// </summary>
        /// <returns>State object</returns>
        public BrooksOrderState LoadTubes(List<BrooksPlateWell> plateWells, List<string> plates, Dictionary<string, int> trays, string operatorName, int priority, string description, bool vialScan)
        {
            if (plates.Count == 0) { return new BrooksOrderState(); }
            Tuple<int, BrooksOrderState> orderIdState = GetNextOrderId();
            if (orderIdState.Item2.ErrorNumber != 0) { return orderIdState.Item2; }
            BrooksPlateType plateType = GetPlateType(plates[0]);
            BrooksOrderState state = null;
            if (plateType == BrooksPlateType.RACK_CR)
            {
                state = IssueOrder(orderIdState.Item1, "LOAD", "PLATE", operatorName, priority, description);
            }
            else
            {
                state = IssueOrder(orderIdState.Item1, "LOAD", "TUBE", operatorName, priority, description);
            }
            if (state.ErrorNumber != 0) { return state; }
            state = AddPlateMethod(orderIdState.Item1, plateType.ToString(), "NO_LID", PlateTubeDictionary[plateType]);
            if (state.ErrorNumber != 0) { return state; }
            foreach (string tray in trays.Keys)
            {
                state = AddOrderTray(orderIdState.Item1, tray, trays[tray]);
                if (state.ErrorNumber != 0) { return state; }
            }
            foreach (string plate in plates)
            {
                state = AddOrderPlate(orderIdState.Item1, plate);
                if (state.ErrorNumber != 0) { return state; }
            }
            if (vialScan)
            {
                state = AddOrderOption(orderIdState.Item1, "NO_1D_ON_LOAD_RACK");
                if (state.ErrorNumber != 0) { return state; }
            }
            state = AddOrderPlateWells(orderIdState.Item1, plateWells);
            if (state.ErrorNumber != 0) { return state; }
            return SendOrder(orderIdState.Item1);
        }

        /// <summary>
        /// Create remove order for plates.
        /// </summary>
        /// <returns>State object</returns>
        public BrooksOrderState RemovePlates(List<string> plates, string operatorName, int priority, string description)
        {
            if (plates.Count == 0) { return new BrooksOrderState(); }
            Tuple<int, BrooksOrderState> orderIdState = GetNextOrderId();
            if (orderIdState.Item2.ErrorNumber != 0) { return orderIdState.Item2; }
            BrooksOrderState state = IssueOrder(orderIdState.Item1, "REMOVE", "PLATE", operatorName, priority, description);
            if (state.ErrorNumber != 0) { return state; }
            foreach (string plate in plates)
            {
                state = AddOrderPlate(orderIdState.Item1, plate);
                if (state.ErrorNumber != 0) { return state; }
            }
            return SendOrder(orderIdState.Item1);
        }

        /// <summary>
        /// Create remove order for plates.
        /// </summary>
        /// <returns>State object</returns>
        public Tuple<int,BrooksOrderState> RemovePKUPlates(List<string> plates, string operatorName, int priority, string description)
        {
            if (plates.Count == 0) {
                return new Tuple<int, BrooksOrderState>(plates.Count,new BrooksOrderState());
            }
            Tuple<int, BrooksOrderState> orderIdState = GetNextOrderId();
            if (orderIdState.Item2.ErrorNumber != 0) { return new Tuple<int, BrooksOrderState>(0,orderIdState.Item2); }
            BrooksOrderState state = IssueOrder(orderIdState.Item1, "REMOVE", "PLATE", operatorName, priority, description);
            if (state.ErrorNumber != 0) { return new Tuple<int, BrooksOrderState>(0, state); }
            foreach (string plate in plates)
            {
                state = AddOrderPlate(orderIdState.Item1, plate);
                if (state.ErrorNumber != 0) { return new Tuple<int, BrooksOrderState>(0, state); }
            }
            return new Tuple<int, BrooksOrderState>(orderIdState.Item1, SendOrder(orderIdState.Item1));
        }
        /// <summary>
        /// Create remove order for tubes.
        /// </summary>
        /// <returns>State object</returns>
        public BrooksOrderState RemoveTubes(List<string> tubes, BrooksPlateType plateType, string operatorName, int priority, string description)
        {
            Tuple<int, BrooksOrderState> orderIdState = GetNextOrderId();
            if (orderIdState.Item2.ErrorNumber != 0) { return orderIdState.Item2; }
            BrooksOrderState state = IssueOrder(orderIdState.Item1, "REMOVE", "TUBE", operatorName, priority, description);
            if (state.ErrorNumber != 0) { return state; }
            foreach (string tube in tubes)
            {
                state = AddOrderTube(orderIdState.Item1, tube);
                if (state.ErrorNumber != 0) { return state; }
            }
            return SendOrder(orderIdState.Item1);
        }

        /// <summary>
        /// Get a list of orders in a certain state.
        /// Valid states are: NEW, SENT, ISSUED, REJECTED, ACCEPTED, PENDING, PROCESSING, COMPLETE, RECEIVED
        /// if state is null - all orders which are not RECEIVED are fetched.
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public Tuple<List<BrooksOrder>, BrooksOrderState> GetOrderList(string state)
        {
            return GetOrders(state);
        }

        /// <summary>
        /// Get list of labware events for a specific order.
        /// </summary>
        public Tuple<List<BrooksLabwareEvent>, BrooksOrderState> GetOrderEvents(int orderId)
        {
            return GetLabwareEvents(orderId);
        }

        /// <summary>
        /// Fail the order before it is accepted.
        /// </summary>
        public BrooksOrderState FailOrder(int orderId)
        {
            return RejectOrder(orderId);
        }

        /// <summary>
        /// Mark the order as received (closed) by Lims
        /// </summary>
        public BrooksOrderState CloseOrder(int orderId)
        {
            Tuple<List<BrooksLabwareEvent>, BrooksOrderState> getState = GetLabwareEvents(orderId);
            if (getState.Item2.ErrorNumber != 0) { return getState.Item2; }
            foreach (BrooksLabwareEvent labwareEvent in getState.Item1)
            {
                BrooksOrderState setState = SetLabwareEventRead(labwareEvent.LabwareEventId);
                if (setState.ErrorNumber != 0) { return setState; }
            }
            return ReceiveOrder(orderId);
        }

        /// <summary>
        /// Get the barcodes of the plates in an order.
        /// </summary>
        public Tuple<List<string>, BrooksOrderState> GetOrderPlates(int orderId)
        {
            return GetPlates(orderId);
        }

        /// <summary>
        /// Get position information for tubes in a remove order.
        /// </summary>
        public Tuple<List<BrooksPlateWell>, BrooksOrderState> GetOrderTubes(int orderId)
        {
            return GetRemovedPlateWells(orderId);
        }

        /// <summary>
        /// Get a list of names of all the trays in the buffer
        /// </summary>
        public Tuple<List<BrooksTray>, BrooksOrderState> GetBufferTrays()
        {
            return GetBufferInformation();
        }

        /// <summary>
        /// Get platetype from barcode.
        /// </summary>
        public BrooksPlateType GetPlateType(string plateBarcode)
        {
            if (plateBarcode.StartsWith("CR")) { return BrooksPlateType.RACK_CR; }
            else if (plateBarcode.StartsWith("MR0")) { return BrooksPlateType.RACK_05; }
            else if (plateBarcode.StartsWith("MR1")) { return BrooksPlateType.RACK_075; }
            else if (plateBarcode.StartsWith("OPKU")) { return BrooksPlateType.PKU_OLD; }
            else if (plateBarcode.StartsWith("600")) { return BrooksPlateType.RACK_05; }
            else if (plateBarcode.StartsWith("500")) { return BrooksPlateType.RACK_05; }
            else if (plateBarcode.StartsWith("400")) { return BrooksPlateType.RACK_05; }
            else if (plateBarcode.StartsWith("BS")) { return BrooksPlateType.RACK_05; }
            else { return BrooksPlateType.PKU_NEW; }
        }
        #endregion Interface

        #region Brooks API Functions
        /// <summary>
        /// Create an order.
        /// </summary>
        private BrooksOrderState IssueOrder(int orderId, string orderType, string orderItemType, string operatorName, int priority, string description)
        {
            return ExecuteSQL("IssueOrder('" + orderId + "','" + orderType + "','" + orderItemType + "','" + operatorName + "',errorDetails," + priority + ",'" + description + "');");
        }

        /// <summary>
        /// Define plate type details
        /// </summary>
        private BrooksOrderState AddPlateMethod(int orderId, string plateType, string lidType, string tubeType)
        {
            return ExecuteSQL("AddPlateMethod('" + orderId + "','" + plateType + "','" + lidType + "','" + tubeType + "',errorDetails);");
        }

        /// <summary>
        /// Add a plate to the order.
        /// </summary>
        private BrooksOrderState AddOrderPlate(int orderId, string plateId)
        {
            return ExecuteSQL("AddOrderPlate('" + orderId + "','" + plateId + "',errorDetails);");
        }
        
        /// <summary>
        /// Add a tray to the order.
        /// </summary>
        private BrooksOrderState AddOrderTray(int orderId, string trayId, int plateCount)
        {
            return ExecuteSQL("AddOrderTray('" + orderId + "','" + trayId + "',errorDetails);");
        }

        /// <summary>
        /// Add samples to a load order.
        /// </summary>
        private BrooksOrderState AddOrderPlateWells(int orderId, List<BrooksPlateWell> plateWells)
        {
            return ExecuteSQLPlateWells("AddOrderPlateWells('" + orderId + "',aPlateWellTab,errorDetails);", plateWells);
        }

        /// <summary>
        /// Add a sample to a remove order.
        /// </summary>
        private BrooksOrderState AddOrderTube(int orderId, string tubeId)
        {
            return ExecuteSQL("AddOrderTube('" + orderId + "','" + tubeId + "',errorDetails);");
        }
              
        /// <summary>
        /// Set an order option
        /// </summary>
        private BrooksOrderState AddOrderOption(int orderId, string option)
        {
            return ExecuteSQL("AddOrderOption('" + orderId + "','" + option + "',errorDetails);");
        }

        /// <summary>
        /// Mark the order as ready for handling
        /// </summary>
        private BrooksOrderState SendOrder(int orderId) 
        {
            return ExecuteSQL("SendOrder('" + orderId + "',errorDetails);");
        }

        /// <summary>
        /// Mark the order as rejected (closed) by Lims.
        /// Must be done before the order is accepted.
        /// </summary>
        private BrooksOrderState RejectOrder(int orderId)
        {
            return ExecuteSQL("RejectOrder('" + orderId + "',errorDetails);");
        }

        /// <summary>
        /// Mark the order as received (closed) by Lims
        /// </summary>
        private BrooksOrderState ReceiveOrder(int orderId)
        {
            return ExecuteSQL("SetOrderReceived('" + orderId + "',errorDetails);");
        }

        /// <summary>
        /// Mark labware event as read by Lims
        /// </summary>
        private BrooksOrderState SetLabwareEventRead(string eventId)
        {
            return ExecuteSQL("SetLabwareEventRead('" + eventId + "',errorDetails);");
        }
        #endregion Brooks API Functions

        #region Helper functions
        /// <summary>
        /// Executes the SQL statement (N.B. It wraps up the call to declare the errorDetails first).
        /// </summary>
        private BrooksOrderState ExecuteSQL(string sqlStatement)
        {
            try
            {
                StringBuilder sql = new StringBuilder();
                sql.Append(" DECLARE                                     ");
                sql.Append("   errorDetails IFS_API.ErrorDetailsRecType; ");
                sql.Append(" BEGIN                                       ");
                sql.Append("   IFS_API.");
                sql.Append(sqlStatement);
                sql.Append("   :nError := errorDetails.error_number;      ");
                sql.Append("   :sErrorMsg := errorDetails.error_message;  ");
                sql.Append(" END;                                         ");
                if (_cnnNaut != null)
                {
                    OracleCommand cmd = _cnnNaut.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = sql.ToString();
                    OracleParameter error = cmd.Parameters.Add(":nError", OracleDbType.Int32, DBNull.Value, ParameterDirection.Output);
                    OracleParameter errorMsg = cmd.Parameters.Add(":sErrorMsg", OracleDbType.Varchar2, 32767, DBNull.Value, ParameterDirection.Output);
                    cmd.ExecuteNonQuery();
                    BrooksOrderState state = new BrooksOrderState(int.Parse(error.Value.ToString()), errorMsg.Value.ToString());
                    cmd.Dispose();
                    return state;
                }
                return new BrooksOrderState(-1, "DB connection is null");
            }
            catch (Exception ex)
            {
                SysLog.Log("Brooks ExecuteSQL Error: " + ex.Message, LogLevel.Error);
                return new BrooksOrderState(-1, ex.Message);
            }
        }

        /// <summary>
        /// Executes the SQL statement including plate wells (N.B. It wraps up the call to declare the errorDetails first).
        /// </summary>
        private BrooksOrderState ExecuteSQLPlateWells(string sqlStatement, List<BrooksPlateWell> plateWells)
        {
            try 
            { 
                StringBuilder sql = new StringBuilder();
                sql.Append(" DECLARE                                         ");
                sql.Append("   errorDetails		IFS_API.ErrorDetailsRecType; ");
                sql.Append("   aPlateWellTab	IFS_API.PlateWellTabType;    ");
                sql.Append("   i                INTEGER:=0;                  ");
                sql.Append(" BEGIN                                           ");
                for (int i = 1; i <= plateWells.Count; i++)
                {
                    sql.Append("   aPlateWellTab(" + i + ").tube_id:=           '" + plateWells[i-1].TubeId + "';");
                    sql.Append("   aPlateWellTab(" + i + ").store_plate_id:=    '" + plateWells[i-1].PlateId + "';");
                    sql.Append("   aPlateWellTab(" + i + ").spw_row:=           '" + plateWells[i-1].Row + "'; ");
                    sql.Append("   aPlateWellTab(" + i + ").spw_col:=           '" + plateWells[i-1].Column + "'; ");
                    sql.Append("   aPlateWellTab(" + i + ").compound_id:=       NULL;");
                    sql.Append("   aPlateWellTab(" + i + ").compound_set:=      NULL;");
                    sql.Append("   aPlateWellTab(" + i + ").plate_method_seq:=  NULL;");
                }
                sql.Append("   IFS_API.");
                sql.Append(sqlStatement);
                sql.Append("   :nError := errorDetails.error_number;         ");
                sql.Append("   :sErrorMsg := errorDetails.error_message;     ");
                sql.Append(" END;                                            ");
                if (_cnnNaut != null)
                {
                    OracleCommand cmd = _cnnNaut.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = sql.ToString();
                    OracleParameter error = cmd.Parameters.Add(":nError", OracleDbType.Int32, DBNull.Value, ParameterDirection.Output);
                    OracleParameter errorMsg = cmd.Parameters.Add(":sErrorMsg", OracleDbType.Varchar2, 32767, DBNull.Value, ParameterDirection.Output);
                    cmd.ExecuteNonQuery();
                    BrooksOrderState state = new BrooksOrderState(int.Parse(error.Value.ToString()), errorMsg.Value.ToString());
                    cmd.Dispose();
                    return state;
                }
                return new BrooksOrderState(-1, "DB connection is null");
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Brooks ExecuteSQLPlateWells Error: {0}", ex.Message), LogLevel.Error);
                return new BrooksOrderState(-1, ex.Message);
            }
        }
        #endregion Helper functions

        #region Direct SQL functions
        /// <summary>
        /// Get the next free order Id from the IFS database
        /// </summary>
        private Tuple<int, BrooksOrderState> GetNextOrderId()
        {
            try
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT max(to_number(Cust_Order_Id)) FROM Ifs_Order";
                    int orderId = Convert.ToInt32(cmd.ExecuteScalar());
                    orderId++;
                    return new Tuple<int, BrooksOrderState>(orderId, new BrooksOrderState());
                }
            }
            catch(Exception ex)
            {
                SysLog.Log(string.Format("Brooks GetNextOrderID Error: {0}", ex.Message), LogLevel.Error);
                return new Tuple<int, BrooksOrderState>(-1, new BrooksOrderState(-1, ex.Message));
            }
        }

        /// <summary>
        /// Get a list of open orders from the IFS database
        /// </summary>
        private Tuple<List<BrooksOrder>, BrooksOrderState> GetOrders(string state)
        {
            string stateString = "";
            if(state != null && state != "")
            {
                stateString = " WHERE Order_State='" + state + "'";
            }
            else
            {
                stateString = " WHERE Order_State != 'RECEIVED'";
            }
            try
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT Cust_Order_Id, Priority, Order_State, Order_Type, Order_Item_Type, Start_Date, Completed_date, Order_Description, Customer_Id FROM Ifs_Order" + stateString + " ORDER BY to_number(Cust_Order_Id)";
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        List<BrooksOrder> orderList = new List<BrooksOrder>();
                        while (reader.Read())
                        {
                            string orderId = reader["Cust_Order_Id"].ToString();
                            string priority = reader["Priority"].ToString();
                            string orderState = reader["Order_State"].ToString();
                            string orderType = reader["Order_Type"].ToString();
                            string orderItemType = reader["Order_Item_Type"].ToString();
                            string startDateString = reader["Start_Date"].ToString();
                            DateTime startDate = startDateString != "" ? DateTime.Parse(startDateString) : DateTime.MaxValue;
                            string completedDateString = reader["Completed_date"].ToString();
                            DateTime completedDate = completedDateString != "" ? DateTime.Parse(completedDateString) : DateTime.MaxValue;
                            string description = reader["Order_Description"].ToString();
                            string operatorName = reader["Customer_Id"].ToString();
                            orderList.Add(new BrooksOrder(orderId, priority, orderState, orderType, orderItemType, startDate, completedDate, description, operatorName));
                        }
                        return new Tuple<List<BrooksOrder>, BrooksOrderState>(orderList, new BrooksOrderState());
                    }
                }
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Brooks GetOrders Error: {0}", ex.Message), LogLevel.Error);
                return new Tuple<List<BrooksOrder>, BrooksOrderState>(null, new BrooksOrderState(-1, ex.Message));
            }
        }

        /// <summary>
        /// Get the barcodes of the plates in an order.
        /// </summary>
        private Tuple<List<string>, BrooksOrderState> GetPlates(int orderId)
        {
            try
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT Store_Plate_Id FROM Ifs_Order_Plate WHERE Cust_Order_Id = '" + orderId + "'";
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        List<string> plateList = new List<string>();
                        while (reader.Read())
                        {
                            string plateId = reader["Store_Plate_Id"].ToString();
                            plateList.Add(plateId);
                        }
                        return new Tuple<List<string>, BrooksOrderState>(plateList, new BrooksOrderState());
                    }
                }
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Brooks Get plates Error: {0}", ex.Message), LogLevel.Error);
                return new Tuple<List<string>, BrooksOrderState>(null, new BrooksOrderState(-1, ex.Message));
            }
        }

        /// <summary>
        /// Get list of labware events for a specific order
        /// </summary>
        private Tuple<List<BrooksLabwareEvent>, BrooksOrderState> GetLabwareEvents(int orderId)
        {
            try
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT LABWARE_EVENT_NUMBER,CREATION_DATE,EVENT_TYPE,ITEM_TYPE,ITEM_ID,SIS_ORDER_ID,EVENT_STATE,LABWARE_TYPE FROM IFS_LABWARE_EVENT WHERE Cust_Order_Id='" + orderId + "' ORDER BY to_number(LABWARE_EVENT_NUMBER)";
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        List<BrooksLabwareEvent> eventList = new List<BrooksLabwareEvent>();
                        while (reader.Read())
                        {
                            string labwareEventId = reader["LABWARE_EVENT_NUMBER"].ToString();
                            DateTime creationDate = DateTime.Parse(reader["CREATION_DATE"].ToString());
                            string eventType = reader["EVENT_TYPE"].ToString();
                            string itemType = reader["ITEM_TYPE"].ToString();
                            string itemId = reader["ITEM_ID"].ToString();
                            string sisOrderId = reader["SIS_ORDER_ID"].ToString();
                            string eventState = reader["EVENT_STATE"].ToString();
                            string labwareType = reader["LABWARE_TYPE"].ToString();
                            eventList.Add(new BrooksLabwareEvent(orderId, labwareEventId, creationDate, eventType, itemType, itemId, sisOrderId, eventState, labwareType));
                        }
                        return new Tuple<List<BrooksLabwareEvent>, BrooksOrderState>(eventList, new BrooksOrderState());
                    }
                }
            }
            catch(Exception ex)
            {
                SysLog.Log(string.Format("Brooks GetLabWareEvents Error: {0}", ex.Message), LogLevel.Error);
                return new Tuple<List<BrooksLabwareEvent>, BrooksOrderState>(null, new BrooksOrderState(-1, ex.Message));
            }
        }

        /// <summary>
        /// Get list of trays present in the IO Buffer
        /// </summary>
        private Tuple<List<BrooksTray>, BrooksOrderState> GetBufferInformation()
        {
            try
            {
                Dictionary<string, BrooksTray> trays = new Dictionary<string, BrooksTray>();
                if (_cnnNaut != null)
                {
                    OracleCommand cmd = _cnnNaut.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT tray.tray_id, tray.tray_state, plate.store_plate_id, plate.plate_type " +
                        "FROM sis_store_segment seg " +
                        "JOIN sis_store_shelf shelf " +
                        "ON seg.segment_id=shelf.segment_id " +
                        "JOIN sis_store_tray tray " +
                        "ON shelf.shelf_id=tray.curr_shelf_id " +
                        "JOIN sis_store_plate plate " +
                        "ON tray.tray_id=plate.tray_id " +
                        "WHERE seg.group_name LIKE 'BUFFER%' " +
                        "ORDER BY tray.tray_id, plate.store_plate_id";
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string trayName = reader["tray_id"] == DBNull.Value ? "" : reader["tray_id"].ToString();
                            string traystate = reader["tray_state"] == DBNull.Value ? "" : reader["tray_state"].ToString();
                            string plateName = reader["store_plate_id"] == DBNull.Value ? "" : reader["store_plate_id"].ToString();
                            string plateType = reader["plate_type"] == DBNull.Value ? "" : reader["plate_type"].ToString();
                            if (!trays.ContainsKey(trayName))
                            {
                                trays.Add(trayName, new BrooksTray(trayName, traystate == "AVAILABLE" ? false : true, plateType));
                            }
                            trays[trayName].Plates.Add(plateName);
                        }
                    }
                }
                return new Tuple<List<BrooksTray>, BrooksOrderState>(new List<BrooksTray>(trays.Values), new BrooksOrderState());
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Brooks GetBufferInformation Error: {0}", ex.Message), LogLevel.Error);
                return new Tuple<List<BrooksTray>, BrooksOrderState>(null, new BrooksOrderState(-1, ex.Message));
            }
        }

        /// <summary>
        /// Get position information for tubes in a remove order.
        /// </summary>
        private Tuple<List<BrooksPlateWell>, BrooksOrderState> GetRemovedPlateWells(int orderId)
        {
            try
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT Curr_Store_Plate_Id, Tube_Id, Tbe_Curr_Row, Tbe_Curr_Col FROM Ifs_Removed_Order_Tubes WHERE Cust_Order_Id = '" + orderId + "'";
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        List<BrooksPlateWell> tubeList = new List<BrooksPlateWell>();
                        while (reader.Read())
                        {
                            string plateId = reader["Curr_Store_Plate_Id"].ToString();
                            string tubeId = reader["Tube_Id"].ToString();
                            string row = (reader["Tbe_Curr_Row"].ToString().ToCharArray()[0] - 64).ToString();
                            int column = Convert.ToInt32(reader["Tbe_Curr_Col"].ToString());
                            tubeList.Add(new BrooksPlateWell(tubeId, plateId, row, column.ToString()));
                        }
                        return new Tuple<List<BrooksPlateWell>, BrooksOrderState>(tubeList, new BrooksOrderState());
                    }
                }
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Brooks GetRemovedPlateWells Error: {0}", ex.Message), LogLevel.Error);
                return new Tuple<List<BrooksPlateWell>, BrooksOrderState>(null, new BrooksOrderState(-1, ex.Message));
            }
        }
        #endregion Direct SQL functions
    }
}
