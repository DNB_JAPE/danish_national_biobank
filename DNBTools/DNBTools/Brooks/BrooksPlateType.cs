﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNBTools.Brooks
{
    public enum BrooksPlateType
    {
        PKU_OLD,
        PKU_NEW,
        RACK_CR,
        RACK_075,
        RACK_05
    }
}
