﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNBTools.Brooks
{
    public class BrooksPlateWell
    {
        public string TubeId { get; set; }
        public string PlateId { get; set; }
        public string Row { get; set; }
        public string Column { get; set; }

        public BrooksPlateWell(string tubeId, string plateId, string row, string column)
        {
            TubeId = tubeId;
            PlateId = plateId;
            Row = row;
            Column = column;
        }
    }
}
