﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNBTools.Brooks
{
    public class BrooksLabwareEvent
    {
        public int OrderId { get; set; }
        public string LabwareEventId { get; set; }
        public DateTime CreationDate { get; set; }
        public string EventType { get; set; }
        public string ItemType { get; set; }
        public string ItemId { get; set; }
        public string SisOrderId { get; set; }
        public string EventState { get; set; }
        public string LabwareType { get; set; } 

        public BrooksLabwareEvent(int orderId, string labwareEventId, DateTime creationDate, string eventType, string itemType, string itemId, string sisOrderId, string eventState, string labwareType)
        {
            OrderId = orderId;
            LabwareEventId = labwareEventId;
            CreationDate = creationDate;
            EventType = eventType;
            ItemType = itemType;
            ItemId = itemId;
            SisOrderId = sisOrderId;
            EventState = eventState;
            LabwareType = labwareType;
        }
    }
}
