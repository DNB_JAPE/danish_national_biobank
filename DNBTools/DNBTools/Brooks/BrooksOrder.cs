﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNBTools.Brooks
{
    [JsonObject(MemberSerialization.OptIn)]
    public class BrooksOrder
    {
        [JsonProperty]
        public string OrderId {get;set;}
        [JsonProperty]
        public string Priority {get;set;}
        [JsonProperty]
        public string OrderState {get;set;}
        [JsonProperty]
        public string OrderType {get;set;}
        [JsonProperty]
        public string OrderItemType {get;set;}
        [JsonProperty]
        public DateTime StartDate {get;set;}
        [JsonProperty]
        public DateTime CompletedDate {get;set;}
        [JsonProperty]
        public string Description {get;set;}
        [JsonProperty]
        public string OperatorName {get;set;}

        public BrooksOrder(string orderId, string priority, string orderState, string orderType, string orderItemType, DateTime startDate, DateTime completedDate, string description, string operatorName)
        {
            OrderId = orderId;
            Priority = priority;
            OrderState = orderState;
            OrderType = orderType;
            OrderItemType = orderItemType;
            StartDate = startDate;
            CompletedDate = completedDate;
            Description = description;
            OperatorName = operatorName;
        }
    }
}
