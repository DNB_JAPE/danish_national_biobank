﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNBTools.Brooks
{
    public class BrooksOrderState
    {
        public int ErrorNumber { get; set; }
        public string ErrorMessage { get; set; }

        public BrooksOrderState()
        {
            ErrorNumber = 0;
            ErrorMessage = "";
        }

        public BrooksOrderState(int errorNumber, string errorMessage)
        {
            ErrorNumber = errorNumber;
            ErrorMessage = errorMessage;
        }
    }
}
