﻿using DNBTools.DataClasses;

namespace DNBTools
{
    public class EntityListBoxItem
    {
        public BiobankEntity Entity { get; set; }
        public string Barcode { get; set; }

        public EntityListBoxItem(BiobankEntity entity)
        {
            Entity = entity;
            Barcode = entity.LogicalId;
        }

        public override string ToString()
        {
            return Barcode;
        }
    }
}
