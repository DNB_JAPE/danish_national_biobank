﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace DNBTools
//{
//    /// <summary>
//    /// Data class representing a patient
//    /// </summary>
//    public class Patient
//    {
//        /// <summary>
//        /// List of visits for the patient.
//        /// </summary>
//        public List<Visit> Visits { get; set; }

//        /// <summary>
//        /// Internal Biobank Id. This is unique and anonymized.
//        /// </summary>
//        public int BiobankId { get; set; }

//        /// <summary>
//        /// Real-life identification number of a patient. This is unique, but not anonymized.
//        /// Access to this information must be restricted as much as possible.
//        /// </summary>
//        public string CPR { get; set; }

//        /// <summary>
//        /// Id of patient in the studies, where the samples were collected. This is not guaranteed to be unique. Not even in the same study.
//        /// </summary>
//        public Dictionary<string, string> ExternalPatientIds { get; set; }

//        public Patient(string cpr, int biobankId)
//        {
//            CPR = cpr;
//            BiobankId = biobankId;
//            ExternalPatientIds = new Dictionary<string, string>();
//            Visits = new List<Visit>();
//        }
//    }
//}
