﻿using System.Collections.Generic;

namespace DNBTools.DataClasses
{
    public class Sample : BiobankEntity
    {
        public Dictionary<long,string> RowName = new Dictionary<long,string>()
        {{1,"A"},
         {2,"B"},
         {3,"C"},
         {4,"D"},
         {5,"E"},
         {6,"F"},
         {7,"G"},
         {8,"H"}};

        public string Name { get; set; }
        public decimal Amount { get; set; }
        public long PlateId { get; set; }
        public long PlateOrder { get; set; }
        public long PlateRow { get; set; }
        public long PlateColumn { get; set; }
        public string BsgId { get; set; }
        public string SampleId { get; set; }

        public Sample(string limsId, string logicalId)
            : base(limsId, logicalId)
        {
        }

        public string PositionAsString()
        {
            return RowName[PlateRow] + PlateColumn;
        }
    }
}
