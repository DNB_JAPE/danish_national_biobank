﻿using System.Collections.Generic;

namespace DNBTools.DataClasses
{
    public class Plate : BiobankEntity
    {
        public Plate(string limsId, string logicalId)
            : base(limsId, logicalId)
        {
        }
    }
}
