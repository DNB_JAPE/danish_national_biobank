﻿namespace DNBTools.DataClasses
{
    public abstract class BiobankEntity
    {
        public string LimsId { get; set; }
        public string LogicalId { get; set; } // Barcode, BiobankId or similar

        public BiobankEntity(string limsId, string logicalId)
        {
            LimsId = limsId;
            LogicalId = logicalId;
        }
    }
}
