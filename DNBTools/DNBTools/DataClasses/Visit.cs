﻿//using DNBTools;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace DNBTools
//{
//    /// <summary>
//    /// Data class for representing a visit (Nautilus: Sample-table)
//    /// </summary>
//    public class Visit
//    {
//        /// <summary>
//        /// Patient the visit is for.
//        /// </summary>
//        public Patient Patient { get; set; }

//        /// <summary>
//        /// List of samples for the visit.
//        /// </summary>
//        public List<Sample> Samples { get; set; }

//        /// <summary>
//        /// The date the sample was taken.
//        /// </summary>
//        public DateTime SampledOn { get; set; }

//        /// <summary>
//        /// The date the sample was received on SSI.
//        /// </summary>
//        public DateTime ReceivedOn { get; set; }

//        /// <summary>
//        /// Type of the material in the sample (Blood, Serum, Urine...).
//        /// </summary>
//        public MaterialType MaterialType { get; set; }

//        /// <summary>
//        /// Nautilus: Sample_Id
//        /// </summary>
//        public string NautilusId { get; set; }

//        public Visit(DateTime sampledOn)
//        {
//            SampledOn = sampledOn;
//            Samples = new List<Sample>();
//        }
//    }
//}
