﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNBTools
{
    /// <summary>
    /// Helper enum for distinguishing Container types.
    /// </summary>
    public enum ContainerType
    {
        UNKNOWN,
        NPKU,
        OPKU,
        CR,
        MX14,
        MX10,
        MX075,
        MX05,
        FilterPaper
    }
}
