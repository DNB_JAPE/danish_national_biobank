﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DNBTools
{
    public enum PlateType
    {
        UNKNOWN,
        NPKU,
        OPKU,
        CR,
        MX14,  // 0.50mL_Matrix
        MX10,  // 0.75mL_Matrix
        MX075, // 1.00mL_MatrixCap
        MX05   // 1.40mL_Matrix
    }
}
