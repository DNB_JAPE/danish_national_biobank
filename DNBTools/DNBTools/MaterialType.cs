﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNBTools
{
    /// <summary>
    /// Enumeration of the different kinds of sample material in the biobank.
    /// </summary>
    public enum MaterialType
    {
        Blood,
        Plasma,
        Serum,
        Urine,
        DNA,
        BuffyCoat,
        Clot,
        PKU,
        Spinal,
        Umbellical,
        Saliva,
        Amnion,
        CellBuffer,
        RNA,
        Feces
    }
}
