﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNBTools.Nautilus
{
    public static class NautilusTools
    {
        public static bool IsProduction()
        {
            FileInfo nautilusConfig = new FileInfo(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), "Thermo", "Nautilus", "Thermo.config"));
            if(nautilusConfig.Exists)
            {
                using (StreamReader reader = nautilusConfig.OpenText())
                {
                    string configData = reader.ReadToEnd();
                    return configData.Contains("srv-dnb-app03");
                }
            }
            else
            {
                throw new FileNotFoundException("Could not determine if Nautilus is in production mode", nautilusConfig.Name);
            }
        }
    }
}
