﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using System.Text;
//using Thermo.Nautilus.Extensions.Database;
using System.Data;
using DNBTools.DataClasses;
using Oracle.ManagedDataAccess.Client;
using System.Reflection;
using System.IO;
using System.Threading;

namespace DNBTools.Nautilus
{
    /// <summary>
    /// Handles all communication with Lims.
    /// </summary>
    public class LimsCommunication : IDisposable
    {
        private object _lock = new object();

        public static string BrooksLocationName = "A4STORE";
        public static string KiwiLocationName = "Kiwi";
        public static string PickedLocationName = "0 Plukket";

        private Dictionary<ContainerType, string> ContainerTypeDictionary = new Dictionary<ContainerType, string>()
        {
            { ContainerType.MX05, "41" },
            { ContainerType.MX075, "21" },
            { ContainerType.CR, "161" },
            { ContainerType.FilterPaper, "321"}
        };

        private Dictionary<string, ContainerType> ContainerTypeIdDictionary = new Dictionary<string, ContainerType>()
        {
            { "41", ContainerType.MX05 },
            { "21", ContainerType.MX075 },
            { "161", ContainerType.CR },
            { "321", ContainerType.FilterPaper }
        };

        /// <summary>
        /// Connection to the Lims database.
        /// </summary>
        public OracleConnection _cnnNaut; // TODO: Make this private

        /// <summary>
        /// Constructor for unit test.
        /// </summary>
        public LimsCommunication() { }

        /// <summary>
        /// Constructor.
        /// </summary>
        public LimsCommunication(string connectionString)
        {
            _cnnNaut = new OracleConnection(connectionString);
            _cnnNaut.Open();
        }

        /// <summary>
        /// Release all resources.
        /// </summary>
        public void Dispose()
        {
            if (_cnnNaut != null)
            {
                _cnnNaut.Close();
            }
            _cnnNaut = null;
        }

        #region Functions
        #region Plate Functions
        public virtual string PlateGetId(string bc)
        {
            try
            {
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "SELECT Plate_Id FROM Plate WHERE Name='" + bc + "'";
                    command.CommandType = System.Data.CommandType.Text;
                    SysLog.Log("Search for PlateID: " + command.CommandText, LogLevel.Info);
                    object obj = command.ExecuteScalar();
                    SysLog.Log("Search for PlateID Found: " + obj == null ? "Nothing" : obj.ToString(), LogLevel.Info);
                    return obj == null ? string.Empty : obj.ToString();
                }
            }
            catch (Exception ex)
            {
                SysLog.Log("Error when getting PlateID: " + ex.Message, LogLevel.Error);
                return string.Empty;
            }
        }

        public virtual bool PlateExist(string plateBC)
        {
            try
            {
                // Connect to database
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "LIMS.DNB.PLATE_IDS_FROM_REFS";
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    OracleParameter inputParameter = new OracleParameter();
                    inputParameter.ParameterName = "plateids";
                    inputParameter.Value = plateBC;
                    inputParameter.OracleDbType = OracleDbType.NVarchar2;
                    inputParameter.Direction = System.Data.ParameterDirection.Input;
                    command.Parameters.Add(inputParameter);
                    OracleParameter outputParameter = new OracleParameter();
                    outputParameter.ParameterName = "c_result";
                    outputParameter.OracleDbType = OracleDbType.RefCursor;
                    outputParameter.Direction = System.Data.ParameterDirection.Output;
                    command.Parameters.Add(outputParameter);
                    SysLog.Log("Search for PlateBC: " + plateBC, LogLevel.Info);
                    using (OracleDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            SysLog.Log("Found PlateBC: " + plateBC, LogLevel.Info);
                            return true;
                        }
                        SysLog.Log("Did not find PlateBC: " + plateBC, LogLevel.Info);
                    }
                }
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Error when checking if plate [{0}] exists: {1}", plateBC , ex.Message), LogLevel.Error);
            }
            return false;
        }

        public virtual string PlateGetBCFromSample(string id)
        {
            try
            {

                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "SELECT External_Reference FROM Plate WHERE Plate_Id = (SELECT Plate_Id FROM Aliquot WHERE Aliquot_Id = '##ID##')";
                    command.CommandText = command.CommandText.Replace("##ID##", id);
                    command.CommandType = System.Data.CommandType.Text;
                    SysLog.Log("Search for Aliquot-ID: " + command.CommandText, LogLevel.Info);
                    object obj = command.ExecuteScalar();
                    SysLog.Log("Search for Aliquot-ID Found: " + obj == null ? "Nothing" : obj.ToString(), LogLevel.Info);
                    return obj == null ? string.Empty : obj.ToString();
                }
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Error when checking if Aliquot-id [{0}] exists in sample: {1}", id, ex.Message), LogLevel.Error);
                return string.Empty;
            }
        }

        public virtual bool PlateIsEmpty(string barcode)
        {
            try
            {
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "SELECT COUNT(*) FROM Aliquot WHERE Plate_Id = (SELECT Plate_Id FROM Plate WHERE EXTERNAL_REFERENCE = '##BARCODE##')";
                    command.CommandText = command.CommandText.Replace("##BARCODE##", barcode);
                    command.CommandType = System.Data.CommandType.Text;
                    SysLog.Log("Search for no of aliquot's with external ref: " + command.CommandText, LogLevel.Info);
                    decimal count = (decimal)command.ExecuteScalar();
                    SysLog.Log("Search for no of aliquot's with external ref. Found: " + count, LogLevel.Info);
                    return count == 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Error when getting no. of Aliquot via external ref. [{0}] exists in sample: {1}", barcode, ex.Message), LogLevel.Error);
                return false;
            }
        }

        public virtual void PlateDispose(string barcode, string user, string timestamp, string comment, bool removeSamplesFromPlate)
        {
            try
            {
                long plate_Id = -1;
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "SELECT Plate_Id FROM Plate WHERE EXTERNAL_REFERENCE = '##BARCODE##'";
                    command.CommandText = command.CommandText.Replace("##BARCODE##", barcode);
                    command.CommandType = System.Data.CommandType.Text;
                    SysLog.Log("Search for Plate with external ref.: " + command.CommandText, LogLevel.Info);
                    plate_Id = (long)command.ExecuteScalar();
                    SysLog.Log("Search for Plate with external ref. Found: " + plate_Id , LogLevel.Info);
                }
                using (OracleTransaction transaction = _cnnNaut.BeginTransaction())
                {
                    using (OracleCommand command = new OracleCommand())
                    {
                        command.Connection = _cnnNaut;
                        command.CommandText = "UPDATE Plate SET Location_Id='5948' WHERE Plate_Id='##PLATE_ID##'";
                        command.CommandText = command.CommandText.Replace("##PLATE_ID##", plate_Id.ToString());
                        command.CommandType = System.Data.CommandType.Text;
                        SysLog.Log("Update Plate: " + command.CommandText, LogLevel.Info);
                        command.ExecuteNonQuery();
                        SysLog.Log("Done with Plate update", LogLevel.Info);
                    }
                    using (OracleCommand command = new OracleCommand())
                    {
                        command.Connection = _cnnNaut;
                        command.CommandText = "UPDATE Plate_User SET U_Disposed='T' WHERE Plate_Id='##PLATE_ID##'";
                        command.CommandText = command.CommandText.Replace("##PLATE_ID##", plate_Id.ToString());
                        command.CommandType = System.Data.CommandType.Text;
                        SysLog.Log("Update disposed Plate: " + command.CommandText, LogLevel.Info);
                        command.ExecuteNonQuery();
                        SysLog.Log("Done with disposed Plate update", LogLevel.Info);
                    }
                    transaction.Commit();
                }
                List<string> sampleIds = SampleGetIdsFromPlate(barcode);
                foreach (string sampleId in sampleIds)
                {
                    SampleDispose(sampleId, user, timestamp, comment, removeSamplesFromPlate);
                }
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Error when disposing Plate [{0}]: {1} ", barcode,  ex.Message), LogLevel.Error);
            }
        }

        public string GetPlateLocationName(string plateID)
        {
            string locationName = string.Empty;
            try
            {
                if (plateID != string.Empty)
                {
                    using (OracleCommand command = new OracleCommand())
                    {
                        command.Connection = _cnnNaut;
                        command.CommandText = "select L.Name LocationName from LIMS.plate p INNER JOIN LIMS.Location L on L.Location_ID = p.Location_ID where p.plate_id = '##PLATEID##'";
                        command.CommandText = command.CommandText.Replace("##PLATEID##", plateID);
                        command.CommandType = System.Data.CommandType.Text;
                        SysLog.Log("Search for Plate location name with Plate ID.: " + command.CommandText, LogLevel.Info);
                        using (OracleDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                locationName = reader["LocationName"].ToString();
                                LimsCommunication.Log(string.Format("Found Location Name [{0}] for plate {1}", locationName, plateID));
                            }
                        }
                    }
                }
                else
                {
                    LimsCommunication.Log("PlateID is empty. Can't find location name...");
                }
            }
            catch (Exception ex)
            {
                LimsCommunication.Log(string.Format("Searching for Location Name for plate {0} gave an error: {1}", plateID, ex.Message));
            }
            return locationName;
        }

        #endregion Plate Functions

        #region Sample Functions
        public virtual List<string> SampleGetIdsFromPlate(string barcode)
        {
            lock (_lock)
            {
                string sql = "SELECT Aliquot_Id FROM Aliquot WHERE Plate_Id = (SELECT Plate_Id FROM Plate WHERE EXTERNAL_REFERENCE = '##BARCODE##')".Replace("##BARCODE##", barcode);
                return SampleGetIdsFromSql(sql);
            }
        }

        public virtual List<string> SampleGetIdsFromSql(string sql)
        {
            lock (_lock)
            {
                List<string> retval = new List<string>();
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = sql;
                    command.CommandType = System.Data.CommandType.Text;
                    SysLog.Log("Get ID's from sample: " + command.CommandText, LogLevel.Info);
                    using (OracleDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string id = reader["Aliquot_Id"].ToString();
                            SysLog.Log("Get ID's from sample. Found: " + id, LogLevel.Info);
                            retval.Add(id);
                        }
                    }
                }
                return retval;
            }
        }

        public virtual void SampleDispose(string id, string user, string timestamp, string comment, bool removeFromPlate)
        {
            lock (_lock)
            {
                using (OracleTransaction transaction = _cnnNaut.BeginTransaction())
                {
                    string storageString;
                    using (OracleCommand command = new OracleCommand())
                    {
                        command.Connection = _cnnNaut;
                        command.CommandText = "SELECT Storage FROM Aliquot WHERE Aliquot_Id='" + id + "'";
                        command.CommandType = System.Data.CommandType.Text;
                        SysLog.Log("Dispose samples. Get storage: " + command.CommandText, LogLevel.Info);
                        object obj = command.ExecuteScalar();
                        storageString = obj == null ? string.Empty : obj.ToString();
                        SysLog.Log("Dispose samples. Get storage. Found: " + storageString, LogLevel.Info);
                    }
                    storageString = storageString + Environment.NewLine + "[" + user + " - " + timestamp + "] " + comment;
                    using (OracleCommand command = new OracleCommand())
                    {
                        command.Connection = _cnnNaut;
                        command.CommandText = "UPDATE Aliquot SET Storage='" + storageString + "' WHERE Aliquot_Id='" + id + "'";
                        command.CommandType = System.Data.CommandType.Text;
                        SysLog.Log("Dispose samples. Update aliquot: " + command.CommandText, LogLevel.Info);
                        command.ExecuteNonQuery();
                        SysLog.Log("Dispose samples. Update aliquot done", LogLevel.Info);
                    }
                    if (removeFromPlate)
                    {
                        using (OracleCommand command = new OracleCommand())
                        {
                            command.Connection = _cnnNaut;
                            command.CommandText = "UPDATE Aliquot SET Location_Id='5948' WHERE Aliquot_Id='" + id + "' AND Location_Id not like '5948'";
                            command.CommandType = System.Data.CommandType.Text;
                            SysLog.Log("Dispose samples. Update aliquots: " + command.CommandText, LogLevel.Info);
                            command.ExecuteNonQuery();
                            SysLog.Log("Dispose samples. Update aliquots done", LogLevel.Info);
                        }
                    }
                    using (OracleCommand command = new OracleCommand())
                    {
                        command.Connection = _cnnNaut;
                        command.CommandText = "UPDATE Aliquot_User SET U_Disposed='T' WHERE Aliquot_Id='" + id + "'";
                        command.CommandType = System.Data.CommandType.Text;
                        SysLog.Log("Dispose samples. Dispose aliquots: " + command.CommandText, LogLevel.Info);
                        command.ExecuteNonQuery();
                        SysLog.Log("Dispose samples. Dispose aliquots done ", LogLevel.Info);
                    }
                    transaction.Commit();
                }
            }
        }

        public string SampleGetContainerTypeID(string id)
        {
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = _cnnNaut;
                command.CommandText = "SELECT Container_Type_Id FROM Aliquot WHERE Aliquot_Id = '" + id + "'";
                command.CommandType = System.Data.CommandType.Text;
                SysLog.Log("Samples - Get container type: " + command.CommandText, LogLevel.Info);
                using (OracleDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string containerTypeId = reader["Container_Type_Id"] == DBNull.Value ? "" : reader["Container_Type_Id"].ToString();
                        SysLog.Log("Samples - Get container typeid. Found: " + containerTypeId, LogLevel.Info);
                        return containerTypeId;
                    }
                }
            }
            return ContainerType.UNKNOWN.ToString();
        }

        public ContainerType SampleGetContainerType(string id)
        {
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = _cnnNaut;
                command.CommandText = "SELECT Container_Type_Id FROM Aliquot WHERE Aliquot_Id = '" + id + "'";
                command.CommandType = System.Data.CommandType.Text;
                SysLog.Log("Samples - Get container type: " + command.CommandText, LogLevel.Info);
                using (OracleDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string containerTypeId = reader["Container_Type_Id"] == DBNull.Value ? "" : reader["Container_Type_Id"].ToString();
                        SysLog.Log("Samples - Get container type. Found: " + containerTypeId, LogLevel.Info);
                        if (ContainerTypeIdDictionary.ContainsKey(containerTypeId))
                        {
                            return ContainerTypeIdDictionary[containerTypeId];
                        }
                        else 
                        {
                            return ContainerType.UNKNOWN;
                        }
                    }
                }
            }
            return ContainerType.UNKNOWN;
        }

        // Rename
        public string GetSampleTypeId(string sampleTypeName)
        {
            lock (_lock)
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT phrase_name FROM phrase_entry WHERE phrase_name='" + sampleTypeName + "' AND phrase_id=29";
                    SysLog.Log("Samples. Get sample type id: " + cmd.CommandText, LogLevel.Info);
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string phraseName = reader["phrase_name"].ToString();
                            SysLog.Log("Samples. Get sample type id. Found: " + phraseName, LogLevel.Info);
                            return phraseName;
                        }
                    }
                }
            }
            return null;
        }

        public List<Sample> SamplesGet(List<string> ids, string orderBy)
        {
            lock (_lock)
            {
                List<Sample> retval = new List<Sample>();
                foreach(string id in ids)
                {
                    retval.Add(SampleGet(id, orderBy));
                }
                return retval;
            }
        }

        public Sample SampleGet(string id)
        {
            lock (_lock)
            {
                return SampleGet(id, "Plate_Order");
            }
        }

        private Sample SampleGet(string id, string orderBy)
        {
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = _cnnNaut;
                command.CommandText = "SELECT a.Aliquot_Id, Name, External_Reference, Amount, Plate_Id, Plate_Order, Plate_Row, Plate_Column, U_Bsg_Id, Sample_Id FROM LIMS.Aliquot a JOIN LIMS.Aliquot_User u ON a.Aliquot_Id=u.Aliquot_Id WHERE a.Aliquot_Id = '" + id + "' ORDER BY " + orderBy;
                command.CommandType = System.Data.CommandType.Text;
                SysLog.Log("Samples. Get sample: " + command.CommandText, LogLevel.Info);
                using (OracleDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string external_reference = reader["External_Reference"] == DBNull.Value ? "" : reader["External_Reference"].ToString();
                        Sample sample = new Sample(reader["Aliquot_Id"].ToString(), external_reference);
                        if (reader["Name"] != DBNull.Value) { sample.Name = reader["Name"].ToString(); }
                        if (reader["Amount"] != DBNull.Value) { sample.Amount = (decimal)reader["Amount"]; }
                        if (reader["Plate_Id"] != DBNull.Value) { sample.PlateId = (long)reader["Plate_Id"]; }
                        if (reader["Plate_Order"] != DBNull.Value) { sample.PlateOrder = (long)reader["Plate_Order"]; }
                        if (reader["Plate_Row"] != DBNull.Value) { sample.PlateRow = (long)reader["Plate_Row"]; }
                        if (reader["Plate_Column"] != DBNull.Value) { sample.PlateColumn = (long)reader["Plate_Column"]; }
                        if (reader["U_Bsg_Id"] != DBNull.Value) { sample.BsgId = reader["U_Bsg_Id"].ToString(); }
                        if (reader["Sample_Id"] != DBNull.Value) { sample.SampleId = reader["Sample_Id"].ToString(); }
                        return sample;
                    }
                }
            }
            return null;
        }

        public void SampleSetVolume(string id, long volume)
        {
            lock (_lock)
            {
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "UPDATE Aliquot SET Amount='" + volume + "' WHERE Aliquot_Id='" + id + "'";
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
            }
        }

        public void SampleSetVolumeToMax(string id)
        {
            lock (_lock)
            {
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "UPDATE Aliquot SET Amount= (select max_volume from aliquot a join container_type c on a.container_type_id = c.container_type_id WHERE Aliquot_Id='" + id + "') WHERE Aliquot_Id='" + id + "'";
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
            }
        }

        public void SampleSetReceivedOn(string id, DateTime timestamp)
        {
            lock (_lock)
            {
                if (timestamp == DateTime.MinValue)
                {
                    using (OracleCommand command = new OracleCommand())
                    {
                        command.Connection = _cnnNaut;
                        command.CommandText = "UPDATE Aliquot SET Received_On='' WHERE Aliquot_Id='" + id + "'";
                        command.CommandType = System.Data.CommandType.Text;
                        command.ExecuteNonQuery();
                    }
                }
                else
                {
                    string receivedOnString = timestamp.ToString("dd-MM-yyyy HH:mm:ss");
                    using (OracleCommand command = new OracleCommand())
                    {
                        command.Connection = _cnnNaut;
                        command.CommandText = "UPDATE Aliquot SET Received_On=TO_DATE('" + receivedOnString + "','dd-MM-yyyy hh24:mi:ss') WHERE Aliquot_Id='" + id + "'";
                        command.CommandType = System.Data.CommandType.Text;
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        public void SampleSetOperator(string id, string operatorId)
        {
            lock (_lock)
            {
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "UPDATE Aliquot SET Operator_Id='" + operatorId + "' WHERE Aliquot_Id='" + id + "'";
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
            }
        }

        public void SampleSetSampleId(string id, string sampleId)
        {
            lock (_lock)
            {
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "UPDATE Aliquot SET Sample_Id='" + sampleId + "' WHERE Aliquot_Id='" + id + "'";
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
            }
        }
        
        public void SampleSetBsgId(string id, string bsgId)
        {
            lock (_lock)
            {
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "UPDATE Aliquot_User SET U_Bsg_Id='" + bsgId + "' WHERE Aliquot_Id='" + id + "'";
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
            }
        }

        public void SampleCreateFormulation(string parentId, string childId)
        {
            lock (_lock)
            {
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "INSERT INTO Aliquot_Formulation (Parent_Aliquot_Id,Child_Aliquot_Id) VALUES ('" + parentId + "','" + childId + "')";
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
            }
        }

        public void SampleDeleteFormulation(string parentId, string childId)
        {
            lock (_lock)
            {
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "DELETE FROM Aliquot_Formulation WHERE Child_Aliquot_Id='" + childId + "'";
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
            }
        }

        public void SamplePKURemove(string id)
        {
            lock(_lock)
            {
                using (OracleTransaction transaction = _cnnNaut.BeginTransaction())
                {
                    List<Sample> samples = new List<Sample>();
                    using (OracleCommand command = new OracleCommand())
                    {
                        command.Connection = _cnnNaut;
                        StringBuilder sb = new StringBuilder();
                        sb.Append("SELECT Aliquot_Id, Plate_Order, Plate_Row, Plate_Column ");
                        sb.Append("FROM Aliquot ");
                        sb.Append("WHERE Plate_Id = (SELECT Plate_Id FROM Aliquot WHERE  Aliquot_Id = ##ALIQUOT_ID##) ");
                        sb.Append("  AND Plate_Order > (SELECT Plate_Order FROM Aliquot WHERE Aliquot_Id = ##ALIQUOT_ID##) ");
                        sb.Append("  AND Plate_Column = (SELECT Plate_Column FROM Aliquot WHERE Aliquot_Id = ##ALIQUOT_ID##) ");
                        sb.Append("ORDER BY Plate_Order ");
                        sb.Replace("##ALIQUOT_ID##", id);
                        command.CommandText = sb.ToString();
                        command.CommandType = System.Data.CommandType.Text;
                        using (OracleDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Sample sample = new Sample(reader["Aliquot_Id"].ToString(), "");
                                if (reader["Plate_Order"] != DBNull.Value) { sample.PlateOrder = (long)reader["Plate_Order"]; }
                                if (reader["Plate_Row"] != DBNull.Value) { sample.PlateRow = (long)reader["Plate_Row"]; }
                                samples.Add(sample);
                            }
                        }
                    }
                    using (OracleCommand cmd = _cnnNaut.CreateCommand())
                    {
                        cmd.Parameters.Clear();
                        cmd.Connection = _cnnNaut;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "lims.lims_location_mgmt_rw.remove_aliquot_from_plate";
                        cmd.Parameters.Add("AliquotID", OracleDbType.Int32).Value = id;
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                    foreach (Sample sample in samples)
                    {
                        using (OracleCommand command = new OracleCommand())
                        {
                            command.Connection = _cnnNaut;
                            StringBuilder sb = new StringBuilder();
                            sb.Append("UPDATE Aliquot SET Plate_Order = ##PLATE_ORDER##, Plate_Row = ##PLATE_ROW## ");
                            sb.Append("WHERE Aliquot_Id = ##ALIQUOT_ID## ");
                            sb.Replace("##PLATE_ORDER##", (sample.PlateOrder - 1).ToString());
                            sb.Replace("##PLATE_ROW##", (sample.PlateRow - 1).ToString());
                            sb.Replace("##ALIQUOT_ID##", sample.LimsId);
                            command.CommandText = sb.ToString();
                            command.CommandType = System.Data.CommandType.Text;
                            command.ExecuteNonQuery();
                        }
                    }
                    transaction.Commit();
                }
            }
        }

        public void SampleSetField(string id, string field, string value)
        {
            lock (_lock)
            {
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "UPDATE Aliquot SET " + field + "='" + value + "' WHERE Aliquot_Id='" + id + "'";
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
            }
        }

        public void SampleUserSetField(string id, string field, string value)
        {
            lock (_lock)
            {
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "UPDATE Aliquot_User SET " + field + "='" + value + "' WHERE Aliquot_Id='" + id + "'";
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
            }
        }

        public bool SampleExist(string barcode, string tubeTypeName)
        {
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = _cnnNaut;
                command.CommandText = "SELECT count(*) FROM Aliquot a JOIN Container_type c ON a.container_type_id=c.container_type_Id WHERE a.external_reference='" + barcode + "' AND c.name='" + tubeTypeName + "'";
                command.CommandType = System.Data.CommandType.Text;
                int sampleCount = Convert.ToInt32(command.ExecuteScalar());
                return sampleCount > 0;
            }
        }

        public string SampleGetPosition(string aliquotId)
        {
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = _cnnNaut;
                command.CommandText = "select chr( plate_row + 64) ||  plate_column position from lims.plate p join lims.aliquot a on a.plate_id = p.plate_id where a.aliquot_id='" + aliquotId + "'";
                command.CommandType = System.Data.CommandType.Text;
                using (OracleDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        return reader["position"].ToString();
                    }
                }
            }
            return null;
        }
        
        public string SampleGetParentBarcode(string aliquotId)
        {
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = _cnnNaut;
                command.CommandText = "select external_reference from lims.aliquot_formulation af join lims.aliquot a on af.parent_aliquot_id = a.aliquot_id where af.child_aliquot_id='" + aliquotId + "'";
                command.CommandType = System.Data.CommandType.Text;
                using (OracleDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        return reader["external_reference"].ToString();
                    }
                }
            }
            return null;
        }

        public string SampleGetMasterBarcode(string aliquotId)
        {
            if (aliquotId == null || aliquotId == string.Empty)
            {
                return null;
            }
            else
            {
                string parentID = GetParentID(aliquotId);
                LimsCommunication.Log(string.Format("Looking for Parent of {0} which is {1}", aliquotId, parentID));
                if (parentID != null)
                {
                    using (OracleCommand command = new OracleCommand())
                    {
                        command.Connection = _cnnNaut;
                        //command.CommandText = string.Format("SELECT a.external_reference masterbarcode FROM lims.aliquot a " +
                        //                      "LEFT JOIN lims.aliquot_formulation af ON af.child_aliquot_id=a.aliquot_id " +
                        //                      "WHERE sample_id IN ( SELECT sample_id FROM lims.aliquot a " +
                        //                      "WHERE ALIQUOT_ID = {0}) AND af.parent_aliquot_id is null", aliquotId);
                        command.CommandText = string.Format("SELECT a.external_reference masterbarcode FROM lims.aliquot a " +
                          "WHERE ALIQUOT_ID = {0} ", parentID);

                        command.CommandType = System.Data.CommandType.Text;
                        using (OracleDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                return reader["masterbarcode"].ToString();
                            }
                        }
                    }
                }
            }
            return null;
        }

        public string GetParentID(string aliquotId)
        {
            string ParentAliquotID = string.Empty;
            LimsCommunication.Log(string.Format("Looking for {0}", aliquotId));

            if (aliquotId == null || aliquotId == string.Empty)
            {
                return null;
            }
            else
            {
                try
                {

                    using (OracleCommand command = new OracleCommand())
                    {
                        command.Connection = _cnnNaut;
                        command.CommandText = string.Format("SELECT af.PARENT_ALIQUOT_ID Parent FROM lims.aliquot_formulation af  " +
                                              "WHERE af.CHILD_ALIQUOT_ID = {0} ", aliquotId);
                        command.CommandType = System.Data.CommandType.Text;
                        using (OracleDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ParentAliquotID = reader["Parent"].ToString();
                                LimsCommunication.Log(string.Format("Found Parentid for {0}", ParentAliquotID));
                            }
                        }
                    }

                    if (ParentAliquotID == null || ParentAliquotID == string.Empty)
                    {
                        LimsCommunication.Log(string.Format("No Parentid for {0}", aliquotId));
                        return aliquotId;
                    }
                    else
                        ParentAliquotID = GetParentID(ParentAliquotID);
                }
                catch (Exception ex)
                {
                    LimsCommunication.Log(string.Format("Searching for Parent ID {0} gave an error: {1}", aliquotId, ex.Message));
                }
            }
            return ParentAliquotID;
        }
        
        #endregion Sample Functions

        #region Patient Functions
        public virtual void PatientCreate(string patientCPR, string clientProjectId, string client, string clientProject, Operator operatorObj, bool addCPRToSDGTAble)
        {
            lock(_lock)
            {
                using (OracleTransaction transaction = _cnnNaut.BeginTransaction())
                {
                    string biobankId = null;
                    biobankId = PatientGetBiobankId(patientCPR);
                    if(biobankId == null)
                    {
                        PatientCreateBiobankId(patientCPR);
                        biobankId = PatientGetBiobankId(patientCPR);
                        if (biobankId == null)
                        {
                            throw new Exception("Failed to create patient");
                        }
                        string cprString = addCPRToSDGTAble ? "<CPR> " + patientCPR : "";
                        using (OracleCommand cmd = _cnnNaut.CreateCommand())
                        {
                            cmd.Connection = _cnnNaut;
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "INSERT INTO Sdg (Sdg_Id,Group_Id,Archived_Child_Complete,Sdg_Template_Id,Workflow_Node_Id,Name,Description,Status,Created_On,Events,Has_Audits,External_Reference,Created_By)" +
                                              "VALUES (Lims.Sq_Sdg.NEXTVAL,'25','F','1','73',Concat('Patient.',to_char(lims.sq_sdg.CURRVAL)),'" + cprString + "','V',SYSDATE,'(Q-Remove Consent,12,#361,F,T)','T','" + biobankId + "'," + operatorObj.LimsId + ")";
                            cmd.ExecuteNonQuery();
                        }
                        using (OracleCommand cmd = _cnnNaut.CreateCommand())
                        {
                            cmd.Connection = _cnnNaut;
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "INSERT INTO Sdg_User (Sdg_Id,U_Patient,U_Consent) VALUES (Lims.Sq_Sdg.CURRVAL, 'T', 'T')";
                            cmd.ExecuteNonQuery();
                        }
                        using (OracleCommand cmd = _cnnNaut.CreateCommand())
                        {
                            cmd.Connection = _cnnNaut;
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "UPDATE Sdg SET Created_By=" + operatorObj.LimsId + " WHERE External_Reference='" + biobankId + "'";
                            cmd.ExecuteNonQuery();
                        }
                    }
                    if(!string.IsNullOrEmpty(clientProjectId) && !PatientExistClientProjectId(biobankId, clientProjectId, client, clientProject))
                    {
                        PatientCreateClientProjectId(biobankId, clientProjectId, client, clientProject);
                    }
                    transaction.Commit();
                }
            }
        }

        public virtual string PatientGetBiobankId(string patientCPR)
        {
            string patientId = null;
            lock (_lock)
            {
                using (OracleCommand lookupCmd = _cnnNaut.CreateCommand())
                {
                    lookupCmd.Connection = _cnnNaut;
                    lookupCmd.CommandType = System.Data.CommandType.Text;
                    lookupCmd.CommandText = "SELECT BB_PID FROM intfc.BIOBANK_PID WHERE CPR='" + patientCPR + "'";
                    using (OracleDataReader lookupReader = lookupCmd.ExecuteReader())
                    {
                        while (lookupReader.Read())
                        {
                            patientId = lookupReader["BB_PID"] == DBNull.Value ? null : lookupReader["BB_PID"].ToString();
                        }
                    }
                }
            }
            return patientId;
        }

        public virtual void PatientCreateBiobankId(string patientCPR)
        {
            lock (_lock)
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "INSERT INTO intfc.BIOBANK_PID (BB_PID, CPR) VALUES (intfc.BB_PID_SEQ.NEXTVAL, '" + patientCPR + "')";
                    int retval = cmd.ExecuteNonQuery();
                }
            }
        }

        public virtual bool PatientExistClientProjectId(string biobankId, string clientProjectId, string client, string clientProject)
        {
            lock (_lock)
            {
                using (OracleCommand command = _cnnNaut.CreateCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "SELECT COUNT(*) FROM intfc.BIOBANK_CLIENT WHERE BB_PID='" + biobankId + "' AND CLIENT_PROJECT_PID='" + clientProjectId + "' AND CLIENT='" + client + "' AND CLIENT_PROJECT='" + clientProject + "'";
                    decimal count = (decimal)command.ExecuteScalar();
                    return count == 0 ? false : true;
                }
            }
        }

        public virtual void PatientCreateClientProjectId(string biobankId, string clientProjectId, string client, string clientProject)
        {
            lock (_lock)
            {
                using (OracleCommand createCmd = _cnnNaut.CreateCommand())
                {
                    createCmd.Connection = _cnnNaut;
                    createCmd.CommandType = System.Data.CommandType.Text;
                    createCmd.CommandText = "INSERT INTO intfc.BIOBANK_CLIENT (BB_PID, CLIENT_PROJECT_PID, CLIENT, CLIENT_PROJECT) VALUES ('" + biobankId + "','" + clientProjectId + "','" + client + "','" + clientProject + "')";
                    createCmd.ExecuteNonQuery();
                }
            }
        }

        public virtual string PatientGetNautilusId(string patientCPR)
        {
            string patientId = null;
            lock (_lock)
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT sdg_id FROM sdg WHERE external_reference = TO_CHAR((SELECT bb_pid FROM intfc.biobank_pid WHERE cpr='" + patientCPR + "'))";
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            patientId = reader["sdg_id"] == DBNull.Value ? null : reader["sdg_id"].ToString();
                        }
                    }
                }
            }
            return patientId;
        }

        public void PatientSetBsgId(string id, string bsgId)
        {
            lock (_lock)
            {
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "UPDATE sdg_user SET U_Bsg_Id='" + bsgId + "' WHERE sdg_id='" + id + "'";
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
            }
        }
        #endregion Patient Functions
        
        #region Study Functions
        public virtual string StudyGetNautilusId(string studyName)
        {
            lock (_lock)
            {
                using (OracleCommand command = _cnnNaut.CreateCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "SELECT study_id FROM study WHERE name='" + studyName + "'";
                    object o = command.ExecuteScalar();
                    return o == DBNull.Value ? null : o.ToString();
                }
            }
        }
        #endregion Study Functions

        #region Visit Functions
        public bool VisitExist(string patientCPR, DateTime date, string studyName)
        {
            lock (_lock)
            {
                string biobankId = null;
                biobankId = PatientGetNautilusId(patientCPR);
                if (biobankId == null)
                {
                    return false;
                }
                string studyId = null;
                studyId = StudyGetNautilusId(studyName);
                if (studyId == null)
                {
                    return false;
                }
                return VisitExistForNautilusId(biobankId, date, studyId);
            }
        }

        public string VisitGet(string patientCPR, DateTime date, string studyName)
        {
            lock (_lock)
            {
                string biobankId = null;
                biobankId = PatientGetNautilusId(patientCPR);
                if (biobankId == null)
                {
                    throw new Exception("Cannot find visit for unknown patient: " + patientCPR);
                }
                string studyId = null;
                studyId = StudyGetNautilusId(studyName);
                if (studyId == null)
                {
                    throw new Exception("Cannot find visit for unknown study: " + studyName);
                }
                return VisitGetFromNautilusId(biobankId, date, studyId);
            }
        }

        public void VisitCreate(string patientCPR, DateTime date, string studyName)
        {
            lock (_lock)
            {
                string biobankId = null;
                biobankId = PatientGetNautilusId(patientCPR);
                if (biobankId == null)
                {
                    throw new Exception("Cannot create visit for unknown patient: " + patientCPR);
                }
                string studyId = null;
                studyId = StudyGetNautilusId(studyName);
                if (studyId == null)
                {
                    throw new Exception("Cannot create visit for unknown study: " + studyName);
                }
                VisitCreateFromNautilusId(biobankId, date, studyId);
            }
        }

        #region Visit Private Methods
        private bool VisitExistForNautilusId(string patientId, DateTime date, string studyId)
        {
            lock (_lock)
            {
                decimal count;
                using (OracleCommand command = _cnnNaut.CreateCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "SELECT COUNT(*) FROM sample WHERE study_id='" + studyId + "' AND sdg_id='" + patientId + "' AND TRUNC(sampled_on)=TO_DATE('" + date.ToString("yyyy-MM-dd") + "','yyyy-mm-dd')";
                    count = (decimal)command.ExecuteScalar();
                }
                return count == 0 ? false : true;
            }
        }

        private string VisitGetFromNautilusId(string patientId, DateTime date, string studyId)
        {
            lock (_lock)
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT sample_id FROM sample WHERE sdg_id='" + patientId + "' AND TRUNC(sampled_on)=TO_DATE('" + date.ToString("yyyy-MM-dd") + "','yyyy-mm-dd') AND study_id='" + studyId + "'";
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            return reader["sample_id"] == DBNull.Value ? null : reader["sample_id"].ToString();
                        }
                    }
                }
            }
            return null;
        }

        private void VisitCreateFromNautilusId(string patientId, DateTime date, string studyId)
        {
            lock (_lock)
            {
                if (VisitExistForNautilusId(patientId, date, studyId))
                {
                    return;
                }
                int increment = -1;
                using (OracleTransaction transaction = _cnnNaut.BeginTransaction())
                {
                    string sampledOnString = date.ToString("dd-MM-yyyy HH:mm:ss");
                    string sampledOnDateString = date.ToString("yyyy-MM-dd");
                    using (OracleCommand cmd = _cnnNaut.CreateCommand())
                    {
                        cmd.Connection = _cnnNaut;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "lims.lims_function.get_increment";
                        cmd.Parameters.Add("returnVal", OracleDbType.Int32, ParameterDirection.ReturnValue);
                        cmd.Parameters["returnVal"].Direction = ParameterDirection.ReturnValue;
                        cmd.Parameters.Add(new OracleParameter("tab_name", "Increment"));
                        cmd.Parameters.Add(new OracleParameter("field_name", sampledOnDateString));
                        cmd.Parameters.Add(new OracleParameter("increment_num", 1));
                        cmd.ExecuteNonQuery();
                        increment = int.Parse(cmd.Parameters["returnVal"].Value.ToString());
                    }
                    string receivedOnString = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
                    using (OracleCommand cmd = _cnnNaut.CreateCommand())
                    {
                        cmd.Connection = _cnnNaut;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = "INSERT INTO Sample " +
                            "(Sample_Id,Priority,Archived_Child_Complete,Group_Id,Sdg_Id,Sampled_On,External_Reference,Conclusion,Sample_Template_Id,Workflow_Node_Id,Name,Status,Created_On,Events,Blind_Sample,Needs_Review,Study_Id,Created_By,Received_On)" +
                            "VALUES " +
                            "(lims.sq_sample.NEXTVAL,1,'F',25," + patientId + ",TO_DATE('" + sampledOnString + "','dd-MM-yyyy hh24:mi:ss'),lims.sq_sample.CURRVAL,'N',22,196,'Visit." + sampledOnDateString + " (" + increment + ")','V',SYSDATE,'(Q-Update Field,80,#217,F,T)','F','F'," + studyId + ",383,TO_DATE('" + receivedOnString + "','dd-MM-yyyy hh24:mi:ss'))";
                        cmd.ExecuteNonQuery();
                    }
                    using (OracleCommand cmd = _cnnNaut.CreateCommand())
                    {
                        cmd.Connection = _cnnNaut;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = "INSERT INTO Sample_User " +
                            "(Sample_Id)" +
                            "VALUES " +
                            "(lims.sq_sample.CURRVAL)";
                        cmd.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
            }
        }
        #endregion Visit Private Methods

        [Obsolete("Possibly")]
        public virtual string VisitGetNautilusIdForNautilusId(string patientId, DateTime date)
        {
            lock (_lock)
            {
                string biobankId = null;
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT sample_id FROM sample WHERE sdg_id='" + patientId + "' AND TRUNC(sampled_on)=TO_DATE('" + date.ToString("yyyy-MM-dd") + "','yyyy-mm-dd')";
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            biobankId = reader["sample_id"] == DBNull.Value ? null : reader["sample_id"].ToString();
                        }
                    }
                }
                return biobankId;
            }
        }

        [Obsolete("Possibly")]
        public virtual string VisitGetNautilusId(string patientCPR, DateTime date)
        {
            lock (_lock)
            {
                string patientBiobankId = null;
                patientBiobankId = PatientGetNautilusId(patientCPR);
                if (patientBiobankId == null)
                {
                    return null;
                }
                return VisitGetNautilusIdForNautilusId(patientBiobankId, date);
            }
        }

        [Obsolete("Possibly")]
        public virtual string VisitGetExternalReference(string patientCPR, DateTime date)
        {
            lock (_lock)
            {
                string patientBiobankId = null;
                patientBiobankId = PatientGetNautilusId(patientCPR);
                if (patientBiobankId == null)
                {
                    return null;
                }
                string externalReference = null;
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT external_reference FROM sample WHERE sdg_id='" + patientBiobankId + "' AND TRUNC(sampled_on)=TO_DATE('" + date.ToString("yyyy-MM-dd") + "','yyyy-mm-dd')";
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            externalReference = reader["external_reference"] == DBNull.Value ? null : reader["external_reference"].ToString();
                        }
                    }
                }
                return externalReference;
            }
        }
        #endregion Visit Functions

        #region Operator Functions
        public Operator OperatorGet(string operatorId)
        {
            lock (_lock)
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT database_name FROM operator WHERE operator_id='" + operatorId + "'";
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string initials = reader["database_name"] == DBNull.Value ? null : reader["database_name"].ToString();
                            Operator Operator = new Operator(operatorId, initials);
                            return Operator;
                        }
                    }
                }
                return null;
            }
        }

        // Deprecated
        public string OperatorGetInitials(int operatorId)
        {
            lock (_lock)
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT database_name FROM operator WHERE operator_id='" + operatorId + "'";
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            return reader["database_name"] == DBNull.Value ? null : reader["database_name"].ToString();
                        }
                    }
                }
                return null;
            }
        }

        public virtual string OperatorGetId(string operatorName)
        {
            lock (_lock)
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT operator_id FROM operator WHERE UPPER(database_name)=UPPER('" + operatorName + "')";
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            return reader["operator_id"] == DBNull.Value ? null : reader["operator_id"].ToString();
                        }
                    }
                }
                return null;
            }
        }
        #endregion Operator Functions

        #region Location Functions
        public virtual string LocationGetId(string locationName)
        {
            lock (_lock)
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT location_id FROM location WHERE name='" + locationName + "'";
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            return reader["location_id"].ToString();
                        }
                    }
                }
            }
            return null;
        }

        public void LocationPlate(string plateId, string locationId) 
        {
            lock (_lock)
            {
                using (OracleCommand updateCmd = _cnnNaut.CreateCommand())
                {
                    updateCmd.Connection = _cnnNaut;
                    updateCmd.CommandType = System.Data.CommandType.Text;
                    updateCmd.CommandText = "UPDATE PLATE SET Location_Id='" + locationId + "' WHERE plate_id='" + plateId + "'";
                    updateCmd.ExecuteNonQuery();
                    updateCmd.Dispose();

                }
            }            
        }

        public void LocationPlateRemoveSamples(string plateBC) 
        {
            lock (_lock)
            {
                string plateId = PlateGetId(plateBC);
                using (OracleCommand updateCmd = _cnnNaut.CreateCommand())
                {
                    updateCmd.Connection = _cnnNaut;
                    updateCmd.CommandType = System.Data.CommandType.Text;
                    updateCmd.CommandText = "UPDATE Aliquot SET plate_id=NULL, plate_order=NULL, plate_row=NULL, plate_column=NULL WHERE plate_id='" + plateId + "'";
                    updateCmd.ExecuteNonQuery();
                    updateCmd.Dispose();

                }
            }
        }

        public void LocationPlateAddSample(string plateBc, string sampleBc, string row, string column)
        {
            ContainerType containerType = containerType = ContainerType.MX075;
            if (plateBc.StartsWith("CR")) { containerType = ContainerType.CR; }
            else if (plateBc.StartsWith("MR0")) { containerType = ContainerType.MX05; }
            else if (plateBc.StartsWith("600")) { containerType = ContainerType.MX05; }
            else if (plateBc.StartsWith("500")) { containerType = ContainerType.MX05; }
            else if (plateBc.StartsWith("400")) { containerType = ContainerType.MX05; }
            else if (plateBc.StartsWith("BS")) { containerType = ContainerType.MX05; }


            using (OracleTransaction transaction = _cnnNaut.BeginTransaction())
            {
                string plateId = null;
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT Plate_Id FROM Plate WHERE External_Reference = '" + plateBc + "'";
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            plateId = reader["Plate_id"].ToString();
                        }
                        reader.Dispose();
                    }
                    cmd.Dispose();
                }
                string aliquotId = null;
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT Aliquot_Id FROM Aliquot WHERE External_Reference = '" + sampleBc + "' AND Container_Type_Id = '" + ContainerTypeDictionary[containerType] + "'";
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            aliquotId = reader["Aliquot_Id"].ToString();
                        }
                        reader.Dispose();
                    }
                    cmd.Dispose();
                }
                if (plateId != null && aliquotId != null)
                {
                    using (OracleCommand cmd = _cnnNaut.CreateCommand())
                    {
                        cmd.Connection = _cnnNaut;
                        cmd.CommandType = System.Data.CommandType.Text;
                        int plateOrder;
                        if (containerType == ContainerType.CR)
                        {
                            plateOrder = Convert.ToInt32(row) + (Convert.ToInt32(column) -1) * 6;
                        }
                        else
                        {
                            plateOrder = Convert.ToInt32(row) + (Convert.ToInt32(column) -1) * 8;
                        }
                        cmd.CommandText = "UPDATE Aliquot SET plate_id='" + plateId + "', plate_order='" + plateOrder + "' , plate_row='" + row + "', plate_column='" + column + "' WHERE aliquot_id='" + aliquotId + "'";
                        cmd.ExecuteNonQuery();
                    }
                }
                else 
                {
                    throw new Exception("Could not update location for sample: " + plateBc + ":" + sampleBc);
                }
                transaction.Commit();
            }
        }

        public virtual List<string> LocationGetPlates(string locationId)
        {
            lock (_lock)
            {
                List<string> retval = new List<string>();
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "SELECT External_Reference FROM Plate WHERE Location_Id='" + locationId + "'";
                    command.CommandType = System.Data.CommandType.Text;
                    using (OracleDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string plateBC = reader["External_Reference"].ToString();
                            retval.Add(plateBC);
                        }
                    }
                }
                return retval;
            }
        }

        #endregion Location Functions

        #region Tube Type Functions
        public virtual string TubeTypeGetId(string tubeTypeName)
        {
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = _cnnNaut;
                command.CommandText = "select container_type_id from container_type where UPPER(name)='" + tubeTypeName.ToUpper() + "'";
                command.CommandType = System.Data.CommandType.Text;
                using (OracleDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        return reader["container_type_id"].ToString();
                    }
                }
            }
            return null;
        }

        public virtual string GetTubeComment(string tubeExRef)
        {
            string Comment = string.Empty;
            try
            {
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "SELECT Storage FROM Aliquot WHERE EXTERNAL_REFERENCE = '##BARCODE##'";
                    command.CommandText = command.CommandText.Replace("##BARCODE##", tubeExRef);
                    command.CommandType = System.Data.CommandType.Text;
                    SysLog.Log("Search for Aliquot comment with external ref.: " + command.CommandText, LogLevel.Info);
                    var res = command.ExecuteScalar();
                    if (res != null && res.GetType() != typeof(DBNull))
                        Comment = (string)res;
                    SysLog.Log("Search for Aliquot comment with external ref. Found: " + Comment, LogLevel.Info);
                }
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Error when finding comment [{0}]: {1} ", tubeExRef, ex.Message), LogLevel.Error);
            }
            return Comment;
        }

        public virtual string UpdateTubeWithComment(string tubeExRef, string comment, bool addComment)
        {
            string Comment = string.Empty;
            try
            {
                if (addComment)
                    Comment = string.Format("{0}#[{1}] {2}", GetTubeComment(tubeExRef), DateTime.Now, comment);
                else
                    Comment = comment;

                SysLog.Log(string.Format("{0} kommentar", addComment ? "Tilføjer " : "Erstatter "), LogLevel.Info);

                using (OracleTransaction transaction = _cnnNaut.BeginTransaction())
                {
                    using (OracleCommand command = new OracleCommand())
                    {
                        command.Connection = _cnnNaut;
                        command.CommandText = "UPDATE Aliquot SET Storage='##Comment##' WHERE EXTERNAL_REFERENCE = '##BARCODE##'";
                        command.CommandText = command.CommandText.Replace("##BARCODE##", tubeExRef);
                        command.CommandText = command.CommandText.Replace("##Comment##", Comment);
                        command.CommandType = System.Data.CommandType.Text;
                        SysLog.Log("Update Aliquot: " + command.CommandText, LogLevel.Info);
                        command.ExecuteNonQuery();
                        SysLog.Log("Done with Aliquot update", LogLevel.Info);
                    }
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Error when adding comment [{0}]: {1} ", tubeExRef, ex.Message), LogLevel.Error);
            }
            return Comment;
        }

        private int maxTubeCount = 800;

        public virtual bool UpdateManyTubesWithComments(List<BiobankEntity> data, string comment, bool addComment, string operatorID, Label statusText, ProgressBar pbStatus)
        {
            bool l_Reply = false;
            string newComment = string.Empty;
            try
            {
                if (comment.Trim() != string.Empty)
                    newComment = string.Format("#[{0} - by {1}] {2}", DateTime.Now, operatorID, comment);

                string IdList = string.Empty;
                int lastRow = 0;
                pbStatus.Maximum = data.Count;
                pbStatus.Value = 0;
                //UpdateProgressbarAndStatus("Data indllæst", 1, statusText, pbStatus);
                // Too many records for one call?
                if (data.Count > maxTubeCount)
                {
                    for (int i = 0; i < data.Count; i+= maxTubeCount)
                    {
                        // Breakdown the list in smaller pieces...
                        IdList = string.Empty;
                        try
                        {

                            for (int y = 0; y < maxTubeCount; y++)
                            {
                                lastRow = i + y;
                                if (lastRow >= data.Count)
                                    break;
                                else
                                    IdList += data[lastRow].LimsId + ",";
                            }
                            //UpdateProgressbarAndStatus("Starter opdatering", 50, statusText, pbStatus);
                        }
                        catch (Exception y)
                        {
                            SysLog.Log(string.Format("Still Index problems [{0}] data count: {1} Error {2}", lastRow, data.Count, y.Message), LogLevel.Error);
                        }

                        IdList = IdList.Substring(0, IdList.Length - 1);
                        try
                        {
                            UpdateTubes(IdList, newComment, addComment);
                            SysLog.Log(string.Format("{0} comment [{1}] for {2} of {3}", (addComment ? "Added" : "Replaced"), newComment, lastRow, data.Count), LogLevel.Info);
                            UpdateProgressbarAndStatus(string.Format("{0} kommentar for {1} ud af {2}", (addComment ? "Tilføjer" : "Udskifter"), lastRow, data.Count), lastRow, statusText, pbStatus);
                            l_Reply = true;
                        }
                        catch (Exception x)
                        {
                            MessageBox.Show(string.Format("List [{0}] gave an error when adding comments: {1} Try to continue...", IdList, x.Message));
                            SysLog.Log(string.Format("List [{0}] gave an error when adding comments: {1} Try to continue...", IdList, x.Message), LogLevel.Error);
                            l_Reply = false;
                        }
                    }
                }
                else
                {
                    foreach (BiobankEntity b in data)
                    {
                        IdList += b.LimsId + ",";
                    }
                    // Remove last ','
                    IdList = IdList.Substring(0, IdList.Length - 1);
                    UpdateProgressbarAndStatus("Starter opdatering", 10, statusText, pbStatus);
                    UpdateTubes(IdList, newComment, addComment);
                    UpdateProgressbarAndStatus(string.Format("Opdaterer {0} aliquots", data.Count), data.Count, statusText, pbStatus);
                    l_Reply = true;
                }
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("Error when adding comments: {0} ", ex.Message), LogLevel.Error);
                MessageBox.Show(string.Format("Error when adding comments: {0} ", ex.Message));
                statusText.Text = string.Format("Fejl ved opdatering af aliquots: {0}", ex.Message);
                statusText.Refresh();
                l_Reply = false;
            }
            return l_Reply;
        }

        private void UpdateProgressbarAndStatus(string statusMessage, int progressvalue, Label statusText, ProgressBar pbStatus)
        {
            //this.Invoke(new MethodInvoker(delegate
            //{
            //    textBox1.Text = (int.Parse(textBox1.Text) + 1).ToString();
            //    textBox1.Refresh();
            //    progressBar1.Value += 10;
            //    progressBar1.Refresh();

            //}));
            statusText.Text = statusMessage;
            statusText.Refresh();
            // ESAT 2019-05-15 Fails when value is outside progressbar max-min values
            if (progressvalue > pbStatus.Maximum) progressvalue = pbStatus.Maximum;
            if (progressvalue < pbStatus.Minimum) progressvalue = pbStatus.Minimum;
            pbStatus.Value = progressvalue;
            pbStatus.Update();
            
            //pbStatus.Refresh();
            Application.DoEvents();
        }

        private void UpdateTubes(string IdList, string newComment, bool addComment)
        {
            using (OracleTransaction transaction = _cnnNaut.BeginTransaction())
            {
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    if (addComment)
                        command.CommandText = "UPDATE LIMS.Aliquot SET Storage=Storage||'##Comment##' WHERE Aliquot_Id in (##BARCODE##)";
                    else
                        command.CommandText = "UPDATE LIMS.Aliquot SET Storage='##Comment##' WHERE Aliquot_Id in (##BARCODE##)";
                    command.CommandText = command.CommandText.Replace("##Comment##", newComment);
                    command.CommandText = command.CommandText.Replace("##BARCODE##", IdList);
                    command.CommandType = System.Data.CommandType.Text;
                    //SysLog.Log("Update Aliquot: " + command.CommandText, LogLevel.Debug);
                    command.ExecuteNonQuery();
                    SysLog.Log("Done with Aliquot updates", LogLevel.Info);
                }
                transaction.Commit();
            }
        }

        public string GetTubeLocationName(string aliquotId)
        {
            string locationName = string.Empty;
            try
            {
                if (aliquotId != string.Empty)
                {
                    using (OracleCommand command = new OracleCommand())
                    {
                        command.Connection = _cnnNaut;
                        command.CommandText = "select L.Name LocationName from LIMS.aliquot a INNER JOIN LIMS.Location L on L.Location_ID = a.Location_ID where a.aliquot_id = '##ALIQUOTID##'";
                        command.CommandText = command.CommandText.Replace("##ALIQUOTID##", aliquotId);
                        command.CommandType = System.Data.CommandType.Text;
                        SysLog.Log("Search for Aliquot location name with external ref.: " + command.CommandText, LogLevel.Info);
                        using (OracleDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                locationName = reader["LocationName"].ToString();
                                LimsCommunication.Log(string.Format("Found Location Name [{0}] for {1}", locationName, aliquotId));
                            }
                        }
                    }
                }
                else
                {
                    LimsCommunication.Log("AliquotID is empty. Can't find location name...");
                }
            }
            catch (Exception ex)
            {
                LimsCommunication.Log(string.Format("Searching for Location Name for {0} gave an error: {1}", aliquotId, ex.Message));
            }
            return locationName;
        }

        #endregion Tube Type Functions

        #region Unit Functions
        public virtual string UnitGetName(string unitid)
        {
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = _cnnNaut;
                command.CommandText = "select name from unit where unit_id='" + unitid + "'";
                command.CommandType = System.Data.CommandType.Text;
                using (OracleDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        return reader["name"].ToString();
                    }
                }
            }
            return string.Empty;
        }
        #endregion Unit Functions

        #region Test
        /// <summary>
        /// Get concentration results.
        /// </summary>
        /// <param name="aliquotId"></param>
        /// <returns>Concentration, A260/A230, A260/A280</returns>
        public Tuple<string, string, string> TestGetConcentrationResults(string aliquotId)
        {
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = _cnnNaut;
                command.CommandText = "select r.calculated_numeric_result Concentration, r2.original_result A260A230, r3.original_result A260A280 from lims.aliquot a left join ( select aliquot_id, max(test_id) as test_id from lims.test where status = 'C' and name = 'DNA Concentration Measurement' group by aliquot_id ) t on t.aliquot_id = a.aliquot_id join lims.test_user tu on tu.test_id = t.test_id left join lims.result r on r.test_id = t.test_id and r.name = 'concentration' left join lims.result r2 on r2.test_id = t.test_id and r2.name = 'A260/A230' left join lims.result r3 on r3.test_id = t.test_id and r3.name = 'A260/A280' where a.aliquot_id='" + aliquotId + "'";
                command.CommandType = System.Data.CommandType.Text;
                using (OracleDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string concentration = reader["Concentration"].ToString();
                        string a230 = reader["A260A230"].ToString();
                        string a280 = reader["A260A280"].ToString();
                        return new Tuple<string, string, string>(concentration, a230, a280);
                    }
                }
            }
            return null;
        }

        public Tuple<string, string, string> TestGetParentConcentrationResults(string aliquotId)
        {
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = _cnnNaut;
                command.CommandText = "select r.calculated_numeric_result Concentration, r2.original_result A260A230, r3.original_result A260A280 from lims.aliquot_formulation af join lims.aliquot a on a.aliquot_id = af.parent_aliquot_id left join ( select aliquot_id, max(test_id) as test_id from lims.test where status = 'C' and name = 'DNA Concentration Measurement' group by aliquot_id ) t on t.aliquot_id = a.aliquot_id join lims.test_user tu on tu.test_id = t.test_id left join lims.result r on r.test_id = t.test_id and r.name = 'concentration' left join lims.result r2 on r2.test_id = t.test_id and r2.name = 'A260/A230' left join lims.result r3 on r3.test_id = t.test_id and r3.name = 'A260/A280' where af.child_aliquot_id= '" + aliquotId + "'";
                command.CommandType = System.Data.CommandType.Text;
                using (OracleDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string concentration = reader["Concentration"].ToString();
                        string a230 = reader["A260A230"].ToString();
                        string a280 = reader["A260A280"].ToString();
                        return new Tuple<string, string, string>(concentration, a230, a280);
                    }
                }
            }
            return null;
        }
        #endregion Test

        #region INTFC
        public bool DnbOrderAliquotListEqualsOrder(List<string> aliquotIds, string order)
        {
            lock (_lock)
            {
                string aliquotString = "(";
                foreach(string aliquot in aliquotIds)
                {
                    aliquotString = aliquotString + aliquot + ",";
                }
                aliquotString = aliquotString.TrimEnd(',');
                aliquotString = aliquotString + ")";
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "SELECT Count(*) FROM aliquot_user WHERE aliquot_id NOT IN " + aliquotString + " AND u_dnb_order_nr='" + order + "'";
                    command.CommandType = System.Data.CommandType.Text;
                    int count = Convert.ToInt32(command.ExecuteScalar());
                    return count == 0;
                }
            }
        }

        public void DnbOrderSetState(string project, string state, DateTime shipmentDate)
        {
            lock (_lock)
            {
                string shippedString = shipmentDate.ToString("dd-MM-yyyy HH:mm:ss");
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "UPDATE intfc.dnb_orders SET status=" + state + ", shipped_date=TO_DATE('" + shippedString + "','dd-MM-yyyy hh24:mi:ss') WHERE project_nr='" + project + "'";
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
            }
        }

        public string BiobankPidGetCPR(string bbPid)
        {
            lock (_lock)
            {
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "SELECT cpr FROM intfc.biobank_pid WHERE bb_pid='" + bbPid + "'";
                    command.CommandType = System.Data.CommandType.Text;
                    using (OracleDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            return(reader["cpr"].ToString());
                        }
                    }
                }
                return "";
            }
        }
        
        public string BiobankClientGetClientProjectPid(string bbPid, string studyid)
        {
            lock (_lock)
            {
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = _cnnNaut;
                    command.CommandText = "SELECT client_project_pid FROM intfc.biobank_client WHERE bb_pid='" + bbPid + "' and client_project=(select name from lims.study where study_id = to_number('" + studyid + "'))";
                    command.CommandType = System.Data.CommandType.Text;
                    using (OracleDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            return(reader["client_project_pid"].ToString());
                        }
                    }
                }
                return "";
            }
        }
        
        
        #endregion INTFC

        #region Shared
        /// <summary>
        /// Get data for the samples on the given carriers.
        /// Tuples of (aliquotId, aliquotRef, carrierRef, row, column).
        /// </summary>
        /// <param name="carrierIds"></param>
        /// <returns></returns>
        public List<Tuple<int, string, string, string, string>> LookupSampleDataForCarriers(List<string> carrierIds)
        {
            List<Tuple<int, string, string, string, string>> datas = new List<Tuple<int, string, string, string, string>> { };
            lock(_lock)
            {
                foreach (string carrierId in carrierIds)
                {
                    List<Tuple<int, string, string, string, string>> data = LookupSampleDataForCarrier(carrierId);
                    if (data != null)
                    {
                        datas.AddRange(data);
                    }
                    else 
                    {
                        return null;
                    }
                }
            }
            return datas;
        }

        /// <summary>
        /// Get data for the samples on the given carrier.
        /// Tuple of (aliquotId, aliquotRef, carrierRef, row, column).
        /// </summary>
        /// <param name="carrierId"></param>
        /// <returns></returns>
        public List<Tuple<int, string, string, string, string>> LookupSampleDataForCarrier(string carrierId)
        {
            List<Tuple<int, string, string, string, string>> datas = new List<Tuple<int, string, string, string, string>> { };
            lock (_lock)
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT a.aliquot_id, a.external_reference AS aliquotref, a.plate_row, a.plate_column, p.external_reference AS plateref FROM aliquot a JOIN plate p ON a.plate_id=p.plate_id WHERE a.plate_id='" + carrierId + "'ORDER BY a.plate_order ASC";
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int aliquotId = Convert.ToInt32(reader["aliquot_id"].ToString(), CultureInfo.InvariantCulture);
                            string row = Convert.ToChar(Int32.Parse(reader["plate_row"].ToString(), CultureInfo.InvariantCulture) + 96).ToString().ToUpper(CultureInfo.InvariantCulture);
                            string column = Convert.ToInt32(reader["plate_column"], CultureInfo.InvariantCulture).ToString("00", CultureInfo.InvariantCulture);
                            string aliquotref = reader["aliquotref"].ToString();
                            string carrierref = reader["plateref"].ToString();
                            if (string.IsNullOrWhiteSpace(aliquotref))
                            {
                                return null;
                            }
                            datas.Add(new Tuple<int, string, string, string, string>(aliquotId, aliquotref, carrierref, row, column));
                        }
                    }
                }
            }
            return datas;
        }

        /// <summary>
        /// Get a dictionary of carrier id and carrier external reference.
        /// </summary>
        /// <param name="sampleIds"></param>
        /// <returns></returns>
        public Dictionary<string, string> LookupCarrierIdsAndRefsForSamples(List<string> sampleIds)
        {
            Dictionary<string, string> retval = new Dictionary<string, string>();
            string sef = string.Empty;
            foreach (string sampleId in sampleIds)
            {
                sef += "'" + sampleId + "',";
            }
            sef = sef.Substring(0, sef.Length - 1);
            lock (_lock)
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT p.plate_id, p.external_reference FROM aliquot a JOIN plate p ON a.plate_id=p.plate_id WHERE a.aliquot_id IN (" + sef + ")";
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string plateId = reader["plate_id"].ToString();
                            string plateRef = reader["external_reference"].ToString();
                            if (!retval.ContainsKey(plateId))
                            {
                                retval.Add(plateId, plateRef);
                            }
                        }

                    }
                }
            }
            return retval;
        }

        /// <summary>
        /// Updates the location of a carrier.
        /// </summary>
        /// <param name="carrierId"></param>
        /// <param name="locationId"></param>
        // Rename
        public void UpdateLocationForCarrier(string carrierId, string locationId)
        {
            lock (_lock)
            {
                using (OracleCommand updateCmd = _cnnNaut.CreateCommand())
                {
                    updateCmd.Connection = _cnnNaut;
                    updateCmd.CommandType = System.Data.CommandType.Text;
                    updateCmd.CommandText = "UPDATE PLATE SET Location_Id='" + locationId + "' WHERE external_reference='" + carrierId + "'";
                    updateCmd.ExecuteNonQuery();
                    updateCmd.Dispose();

                }
            }
        }

        /// <summary>
        /// Updates the location of a sample.
        /// </summary>
        /// <param name="sampleId"></param>
        /// <param name="locationId"></param>
        // Rename
        public void UpdateLocationForSample(string sampleId, string locationId)
        {
            OracleCommand updateCmd = _cnnNaut.CreateCommand();
            updateCmd.Connection = _cnnNaut;
            updateCmd.CommandType = System.Data.CommandType.Text;
            updateCmd.CommandText = "UPDATE ALIQUOT SET Location_Id='" + locationId + "' WHERE aliquot_id='" + sampleId + "'";
            updateCmd.ExecuteNonQuery();
            updateCmd.Dispose();
        }

        /// <summary>
        /// Remove a sample from its carrier.
        /// </summary>
        /// <param name="sampleId"></param>
        public void RemoveSampleFromCarrier(string sampleId)
        {
            lock (_lock)
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Parameters.Clear();
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "lims.lims_location_mgmt_rw.remove_aliquot_from_plate";
                    cmd.Parameters.Add("AliquotID", OracleDbType.Int32).Value = sampleId;
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
            }
        }

        /// <summary>
        /// Place a sample on a carrier.
        /// </summary>
        // Rename
        public void MoveSampleToCarrier(string sampleId, string carrierExternalReference, string row, string column)
        {
            string carrierId = string.Empty;
            lock (_lock)
            {
                using (OracleCommand carrierCmd = _cnnNaut.CreateCommand())
                {
                    carrierCmd.Connection = _cnnNaut;
                    carrierCmd.CommandType = System.Data.CommandType.Text;
                    carrierCmd.CommandText = "SELECT plate_id, external_reference FROM plate WHERE plate.external_reference='" + carrierExternalReference + "'";
                    SysLog.Log(string.Format("Selecting ID: {0}", carrierCmd.CommandText), LogLevel.Debug);
                    using (OracleDataReader carrierReader = carrierCmd.ExecuteReader())
                    {
                        while (carrierReader.Read())
                        {
                            carrierId = carrierReader["plate_id"].ToString();
                        }
                    }
                }
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    using (OracleCommand cmd1 = _cnnNaut.CreateCommand())
                    {
                        try
                        {
                            cmd.Parameters.Clear();
                            cmd.Connection = _cnnNaut;
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "SELECT plate_aliquot_type FROM lims.aliquot WHERE aliquot_id='" + sampleId + "'";
                            SysLog.Log(string.Format("Selecting Type: {0}", cmd.CommandText), LogLevel.Debug);
                            var obj = cmd.ExecuteScalar();
                            string PlateAliquotTypeId = obj == null ? "13" : obj.ToString();
                            // Move sample
                            SysLog.Log(string.Format("AliquotID: {0}", Convert.ToInt32(sampleId)), LogLevel.Debug);
                            SysLog.Log(string.Format("PlateID: {0}", Convert.ToInt32(carrierId)), LogLevel.Debug);
                            SysLog.Log(string.Format("PlateRow: {0}", CalculateNumber(row)), LogLevel.Debug);
                            SysLog.Log(string.Format("PlateColumn: {0}", CalculateNumber(column)), LogLevel.Debug);
                            SysLog.Log(string.Format("PlateOrder: {0}", CalculatePlateOrder(row,column)), LogLevel.Debug);
                            SysLog.Log(string.Format("PlateAliquotTypeID: {0}", Convert.ToInt32(PlateAliquotTypeId)), LogLevel.Debug);

                            cmd1.Parameters.Clear();
                            cmd1.Connection = _cnnNaut;
                            cmd1.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd1.CommandText = "lims.lims_location_mgmt_rw.move_aliquot_to_plate";
                            // ESAT 20181120 Changed string values to be converted to int32
                            cmd1.Parameters.Add("AliquotID", OracleDbType.Int32).Value = Convert.ToInt32(sampleId);
                            cmd1.Parameters.Add("PlateID", OracleDbType.Int32).Value = Convert.ToInt32(carrierId);
                            cmd1.Parameters.Add("PlateRow", OracleDbType.Int32).Value = CalculateNumber(row);
                            cmd1.Parameters.Add("PlateColumn", OracleDbType.Int32).Value = CalculateNumber(column);
                            cmd1.Parameters.Add("PlateOrder", OracleDbType.Int32).Value = CalculatePlateOrder(row, column);
                            cmd1.Parameters.Add("PlateAliquotTypeID", OracleDbType.Int32).Value = Convert.ToInt32(PlateAliquotTypeId);
                            // ESAT 20181120
                            SysLog.Log(string.Format("SP: {0}", cmd1.CommandText), LogLevel.Debug);
                            cmd1.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            // ESAT 20181120
                            SysLog.Log(string.Format("Error when saving: {0}, Inner Exception: {1}", ex.Message, ex.InnerException), LogLevel.Error);
                        }

                    }
                }
            }
        }

        public void SaveData()
        {
            string fileHeader = "RecordId,TRackBC,Tube_Type,TPositionId,TPositionBC,TStatusSummary,TSumStateDescription,TVolume,SRackBC,Sample_Type,SPositionId,SPositionBC,ActionDateTime,UserName,Location_Id,Description";

        }

        #endregion Shared

        #region DiagnosticData
        /// <summary>
        /// Set sample id for a master aliquot.
        /// </summary>
        /// <param name="sampleId"></param>
        /// <param name="visitId"></param>
        public void ConnectVisitAndMaster(string sampleId, string visitId, string specimenComment)
        {
            lock (_lock)
            {
                using (OracleTransaction transaction = _cnnNaut.BeginTransaction())
                {
                    using (OracleCommand cmd = _cnnNaut.CreateCommand())
                    {
                        cmd.Connection = _cnnNaut;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = "UPDATE aliquot SET sample_id='" + visitId + "' WHERE aliquot_id='" + sampleId + "'";
                        cmd.ExecuteNonQuery();
                    }
                    using (OracleCommand cmd = _cnnNaut.CreateCommand())
                    {
                        cmd.Connection = _cnnNaut;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = "UPDATE aliquot_user SET u_specimen_comment='" + specimenComment + "' WHERE aliquot_id='" + sampleId + "'";
                        cmd.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
            }
        }

        /// <summary>
        /// Get the number of lines in the INTFC.Flexilab table.
        /// </summary>
        /// <returns></returns>
        public int CountLinesInFlexilab()
        {
            int lineCount = -1;
            lock (_lock)
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT COUNT(*) FROM intfc.flexilab";
                    lineCount = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
            return lineCount;
        }

        /// <summary>
        /// Get the number of lines in the INTFC.Flexilab_DP_Archive table.
        /// </summary>
        /// <returns></returns>
        public int CountLinesInFlexilabDPArchive()
        {
            int lineCount = -1;
            lock (_lock)
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT COUNT(*) FROM intfc.flexilab_dp_archive";
                    lineCount = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
            return lineCount;
        }

        /// <summary>
        /// Get the number of lines in the table.
        /// </summary>
        /// <returns></returns>
        public int CountLinesInTable(string table)
        {
            int lineCount = -1;
            lock (_lock)
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT COUNT(*) FROM " + table;
                    lineCount = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
            return lineCount;
        }

        /// <summary>
        /// Get information for samples from the INTFC.Flexilab table which is matched with aliquots.
        /// </summary>
        /// <param name="workflowName"></param>
        /// <param name="interval"></param>
        /// <returns>PID, FCID, collected, received, analysis, specimen, aliquot_id, record_id</returns>
        public List<List<string>> GetFlexilabDPMatchedLines(string workflowName, string interval)
        {
            List<List<string>> retval = new List<List<string>>();
            lock (_lock)
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "  SELECT f.fcid, f.collected, f.received, f.analyses, specimen, f.aliquotes, f.pid, f.accnumber, f.specimen_code, f.record_id, a.aliquot_id " +
                                      "  FROM intfc.flexilab f " +
                                      "  JOIN lims.aliquot a " +
                                      "    ON f.FCID=a.external_reference " +
                                      "  JOIN lims.workflow_node wn " +
                                      "    ON a.workflow_node_id=wn.workflow_node_id " +
                                      "  JOIN lims.workflow w " +
                                      "    ON wn.workflow_id=w.workflow_id " +
                                      "  WHERE a.created_on BETWEEN f.received AND f.received + INTERVAL '" + interval + "' DAY " +
                                      "    AND w.name='" + workflowName + "'" +
                                      "UNION " +
                                      "  SELECT f.fcid, f.collected, f.received, f.analyses, specimen, f.aliquotes, f.pid, f.accnumber, f.specimen_code, f.record_id, a.aliquot_id " +
                                      "  FROM intfc.flexilab f " +
                                      "  JOIN lims.aliquot a " +
                                      "    ON f.aliquotes=a.external_reference " +
                                      "  JOIN lims.workflow_node wn " +
                                      "    ON a.workflow_node_id=wn.workflow_node_id " +
                                      "  JOIN lims.workflow w " +
                                      "    ON wn.workflow_id=w.workflow_id " +
                                      "  WHERE a.created_on BETWEEN f.received AND f.received + INTERVAL '" + interval + "' DAY " +
                                      "    AND w.name='" + workflowName + "'" +
                                      "    AND f.aliquotes LIKE 'L%'";
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string fcid = reader["fcid"].ToString();                   // FCID (HIS_ORDER_NUM)
                            string collected = reader["collected"].ToString();         // Collection date
                            string received = reader["received"].ToString();           // SSI receive date
                            string analysis = reader["analyses"].ToString();           // Diagnostic Analysis
                            string specimen = reader["specimen"].ToString();           // Material type (serum, blood, ...)
                            string aliquotes = reader["aliquotes"].ToString();         // 
                            string pid = reader["pid"].ToString();                     // CPR
                            string accnumber = reader["accnumber"].ToString();         // Accesion number
                            string specimen_code = reader["specimen_code"].ToString(); // Material type code
                            string recordId = reader["record_id"].ToString();          // Flexilab table record id
                            string aliquotId = reader["aliquot_id"].ToString();        // Nautilus id of the master aliquot.
                            retval.Add(new List<string> { fcid, collected, received, analysis, specimen, aliquotes, pid, accnumber, specimen_code, recordId, aliquotId });
                        }
                    }
                }
            }
            return retval;
        }

        /// <summary>
        /// Get information for matched samples from the INTFC.Flexilab_Historic table.
        /// </summary>
        /// <returns>PID, FCID, collected, received, analysis, specimen, null, aliquot_id</returns>
        public List<List<string>> GetFlexilabHistoricMatchedLinesFCID()
        {
            List<List<string>> retval = new List<List<string>>();
            lock (_lock)
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT f.fcid, f.collected, f.received, f.analyses, f.specimen, f.aliquotes, f.pid, f.accnumber, f.specimen_code, a.aliquot_id " +
                                      "FROM intfc.unmatched_cid_samples a " +
                                      "JOIN intfc.flexilab_historic f " +
                                      "  ON f.FCID=a.external_reference ";
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string fcid = reader["fcid"].ToString();                   // FCID (HIS_ORDER_NUM)
                            string collected = reader["collected"].ToString();         // Collection date
                            string received = reader["received"].ToString();           // SSI receive date
                            string analysis = reader["analyses"].ToString();           // Diagnostic Analysis
                            string specimen = reader["specimen"].ToString();           // Material type (serum, blood, ...)
                            string aliquotes = reader["aliquotes"].ToString();         // 
                            string pid = reader["pid"].ToString();                     // CPR
                            string accnumber = reader["accnumber"].ToString();         // Accession number
                            string specimen_code = reader["specimen_code"].ToString(); // Material type code
                            string aliquotId = reader["aliquot_id"].ToString();        // Nautilus id of the master aliquot.
                            retval.Add(new List<string> { fcid, collected, received, analysis, specimen, aliquotes, pid, accnumber, specimen_code, null, aliquotId });
                        }
                    }
                }
            }
            return retval;
        }

        /// <summary>
        /// Get information for matched samples from the INTFC.Flexilab_Historic table.
        /// </summary>
        /// <returns>PID, FCID, collected, received, analysis, specimen, null, aliquot_id</returns>
        public List<List<string>> GetFlexilabHistoricMatchedLinesCID()
        {
            List<List<string>> retval = new List<List<string>>();
            lock (_lock)
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT f.fcid, f.collected, f.received, f.analyses, f.specimen, f.aliquotes, f.pid, f.accnumber, f.specimen_code, a.aliquot_id " +
                                      "FROM intfc.unmatched_cid_samples a " +
                                      "JOIN intfc.flexilab_historic f " +
                                      "  ON f.aliquotes=a.external_reference ";
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string fcid = reader["fcid"].ToString();                   // FCID (HIS_ORDER_NUM)
                            string collected = reader["collected"].ToString();         // Collection date
                            string received = reader["received"].ToString();           // SSI receive date
                            string analysis = reader["analyses"].ToString();           // Diagnostic Analysis
                            string specimen = reader["specimen"].ToString();           // Material type (serum, blood, ...)
                            string aliquotes = reader["aliquotes"].ToString();         // 
                            string pid = reader["pid"].ToString();                     // CPR
                            string accnumber = reader["accnumber"].ToString();         // Accession number
                            string specimen_code = reader["specimen_code"].ToString(); // Material type code
                            string aliquotId = reader["aliquot_id"].ToString();        // Nautilus id of the master aliquot.
                            retval.Add(new List<string> { fcid, collected, received, analysis, specimen, aliquotes, pid, accnumber, specimen_code, null, aliquotId });
                        }
                    }
                }
            }
            return retval;
        }

        /// <summary>
        /// Get information for matched samples from the INTFC.Flexilab_Historic table.
        /// </summary>
        /// <returns>PID, FCID, collected, received, analysis, specimen, null, aliquot_id</returns>
        public List<List<string>> GetFlexilabHistoricMatchedLinesACC(decimal interval)
        {
            List<List<string>> retval = new List<List<string>>();
            lock (_lock)
            {
                using (OracleCommand cmd = _cnnNaut.CreateCommand())
                {
                    cmd.Connection = _cnnNaut;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "SELECT f.fcid, f.collected, f.received, f.analyses, f.specimen, f.aliquotes, f.pid, f.accnumber, f.specimen_code, a.aliquot_id " +
                                      "FROM intfc.unmatched_acc_samples a " +
                                      "JOIN intfc.flexilab_historic f " +
                                      "  ON f.accnumber=a.external_reference " + 
                                      "WHERE TO_DATE(f.received) > TO_DATE(a.startdate) - " + interval + 
                                      "  AND TO_DATE(f.received) < TO_DATE(a.enddate) + " + interval;
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string fcid = reader["fcid"].ToString();                   // FCID (HIS_ORDER_NUM)
                            string collected = reader["collected"].ToString();         // Collection date
                            string received = reader["received"].ToString();           // SSI receive date
                            string analysis = reader["analyses"].ToString();           // Diagnostic Analysis
                            string specimen = reader["specimen"].ToString();           // Material type (serum, blood, ...)
                            string aliquotes = reader["aliquotes"].ToString();         // 
                            string pid = reader["pid"].ToString();                     // CPR
                            string accnumber = reader["accnumber"].ToString();         // Accession number
                            string specimen_code = reader["specimen_code"].ToString(); // Material type code
                            string aliquotId = reader["aliquot_id"].ToString();        // Nautilus id of the master aliquot.
                            retval.Add(new List<string> { fcid, collected, received, analysis, specimen, aliquotes, pid, accnumber, specimen_code, null, aliquotId });
                        }
                    }
                }
            }
            return retval;
        }

        /// <summary>
        /// Move the row from INTFC.Flexilab to INTFC.Flexilab_DP_Archive.
        /// </summary>
        public void MoveLineFromFlexilabDPToArchive(string recordId)
        {
            lock (_lock)
            {
                using (OracleTransaction transaction = _cnnNaut.BeginTransaction())
                {
                    using (OracleCommand cmd = _cnnNaut.CreateCommand())
                    {
                        cmd.Connection = _cnnNaut;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = "INSERT INTO intfc.flexilab_dp_archive (fcid, collected, received, analyses, specimen, aliquotes, pid, accnumber, specimen_code, importtodb, importtotable, record_id) SELECT fcid, collected, received, analyses, specimen, aliquotes, pid, accnumber, specimen_code, importtodb, (SELECT CURRENT_TIMESTAMP FROM dual) as importtotable, record_id FROM intfc.flexilab WHERE record_id='" + recordId + "'";
                        cmd.ExecuteNonQuery();
                    }
                    using (OracleCommand cmd = _cnnNaut.CreateCommand())
                    {
                        cmd.Connection = _cnnNaut;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = "DELETE FROM intfc.flexilab WHERE record_id='" + recordId + "'";
                        cmd.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
            }
        }

        /// <summary>
        /// Move rows older than interval days, to the INTFC.Flexilab_DP_Unmatched.
        /// </summary>
        /// <param name="interval"></param>
        public int MoveUnmatchedFromFlexilabDPToUnmatched(string interval)
        {
            int lineCount = -1;
            lock (_lock)
            {
                using (OracleTransaction transaction = _cnnNaut.BeginTransaction())
                {
                    using (OracleCommand cmd = _cnnNaut.CreateCommand())
                    {
                        cmd.Connection = _cnnNaut;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = "INSERT INTO intfc.flexilab_dp_unmatched (fcid, collected, received, analyses, specimen, aliquotes, pid, accnumber, specimen_code, importtodb, importtotable, record_id) SELECT fcid, collected, received, analyses, specimen, aliquotes, pid, accnumber, specimen_code, importtodb, (SELECT CURRENT_TIMESTAMP FROM dual) as importtotable, record_id FROM intfc.flexilab WHERE importtodb<(SELECT CURRENT_TIMESTAMP FROM dual) - INTERVAL '" + interval + "' DAY";
                        cmd.ExecuteNonQuery();
                    }
                    using (OracleCommand cmd = _cnnNaut.CreateCommand())
                    {
                        cmd.Connection = _cnnNaut;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = "DELETE FROM intfc.flexilab WHERE importtodb<(SELECT CURRENT_TIMESTAMP FROM dual) - INTERVAL '" + interval + "' DAY";
                        lineCount = cmd.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
            }
            return lineCount;
        }

        /// <summary>
        /// Move the row from INTFC.Flexilab_Historic to INTFC.Flexilab_Historic_Archive.
        /// </summary>
        public void MoveLineFromFlexilabHistoricToArchive(string fcid, string collected, string received, string analyses, string pid, string accnumber, string specimen_code)
        {
            lock (_lock)
            {
                using (OracleTransaction transaction = _cnnNaut.BeginTransaction())
                {
                    using (OracleCommand cmd = _cnnNaut.CreateCommand())
                    {
                        cmd.Connection = _cnnNaut;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = "INSERT INTO intfc.flexilab_historic_archive (fcid, collected, received, analyses, specimen, aliquotes, pid, accnumber, specimen_code, importtodb, importtotable) SELECT fcid, collected, received, analyses, specimen, aliquotes, pid, accnumber, specimen_code, importtodb, (SELECT CURRENT_TIMESTAMP FROM dual) as importtotable FROM intfc.flexilab_historic WHERE pid='" + pid + "' AND fcid='" + fcid + "' AND collected=TO_DATE('" + collected + "','dd-mm-yyyy hh24:mi:ss') AND received=TO_DATE('" + received + "','dd-mm-yyyy hh24:mi:ss') AND analyses='" + analyses + "' AND specimen_code='" + specimen_code + "'";
                        cmd.ExecuteNonQuery();
                    }
                    using (OracleCommand cmd = _cnnNaut.CreateCommand())
                    {
                        cmd.Connection = _cnnNaut;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = "DELETE FROM intfc.flexilab_historic WHERE pid='" + pid + "' AND fcid='" + fcid + "' AND collected=TO_DATE('" + collected + "','dd-mm-yyyy hh24:mi:ss') AND received=TO_DATE('" + received + "','dd-mm-yyyy hh24:mi:ss') AND analyses='" + analyses + "' AND specimen_code='" + specimen_code + "'";
                        cmd.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
            }
        }
        #endregion DiagnosticData

        #region LHReader
        /// <summary>
        /// Lookup container type ids from container type names.
        /// </summary>
        /// <param name="containerTypeNames"></param>
        /// <returns></returns>
        public Dictionary<string, string> FindContainerTypeIds(List<string> containerTypeNames)
        {
            Dictionary<string, string> retval = new Dictionary<string,string>();
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = _cnnNaut;
                string referenceString = string.Empty;
                foreach (string containerTypeName in containerTypeNames)
                {
                    referenceString = referenceString + containerTypeName.ToUpper() + ",";
                }
                referenceString = referenceString.TrimEnd(',');
                using (OracleDataReader reader = command.ExecuteReader())
                {
                    command.CommandText = "select name, container_type_id from container_type where UPPER(name) in '" + referenceString + "'";
                    command.CommandType = System.Data.CommandType.Text;
                    while (reader.Read())
                    {
                        string name = reader["name"].ToString();
                        string id = reader["container_type_id"].ToString();
                    }
                }
            }
            return retval;
        }

        /// <summary>
        /// Lookup operator ids from operator names.
        /// </summary>
        /// <param name="operatorNames"></param>
        /// <returns></returns>
        public Dictionary<string, string> FindOperatorNameIds(List<string> operatorNames)
        {
            Dictionary<string, string> retval = new Dictionary<string, string>();
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = _cnnNaut;
                string referenceString = string.Empty;
                foreach (string operatorName in operatorNames)
                {
                    referenceString = referenceString + operatorName.ToUpper() + ",";
                }
                referenceString = referenceString.TrimEnd(',');
                using (OracleDataReader reader = command.ExecuteReader())
                {
                    command.CommandText = "select name, operator_ID from operator where UPPER(database_name) in '" + referenceString + "'";
                    command.CommandType = System.Data.CommandType.Text;
                    while (reader.Read())
                    {
                        string name = reader["name"].ToString();
                        string id = reader["operator_ID"].ToString();
                    }
                }
            }
            return retval;
        }

        public Tuple<string, string> FindAliquotIdAndRef(string sql)
        {
            using(OracleCommand command = new OracleCommand())
            {
                command.Connection = _cnnNaut;
                command.CommandText = sql;
                command.CommandType = System.Data.CommandType.Text;
                using (OracleDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string id = reader["aliquot_id"].ToString();
                        string extref = reader["external_reference"].ToString();
                        return new Tuple<string,string>(id, extref);
                    }
                }
            }
            return null;
        }

        public int CountInstances(string sql)
        {
            int retval = 0;
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = _cnnNaut;
                command.CommandText = sql;
                command.CommandType = System.Data.CommandType.Text;
                using (OracleDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        retval++;
                    }
                }
            }
            return retval;
        }
        #endregion LHReader
        #endregion Functions

        #region Helper functions
        private int CalculateNumber(string str)
        {
            int retval;
            if (Int32.TryParse(str, out retval))
            {
                return retval;
            }
            else
            {
                switch (str)
                {
                    case "A":
                        return 1;
                    case "B":
                        return 2;
                    case "C":
                        return 3;
                    case "D":
                        return 4;
                    case "E":
                        return 5;
                    case "F":
                        return 6;
                    case "G":
                        return 7;
                    case "H":
                        return 8;
                    case "I":
                        return 9;
                    case "J":
                        return 10;
                    case "K":
                        return 11;
                    case "L":
                        return 12;
                    case "M":
                        return 13;
                    case "N":
                        return 14;
                    default:
                        return -1;
                }
            }
        }

        private int CalculatePlateOrder(string row, string column)
        {
            switch (row)
            {
                case "A":
                    return Convert.ToInt32(column);
                case "B":
                    return Convert.ToInt32(column) + 12;
                case "C":
                    return Convert.ToInt32(column) + 24;
                case "D":
                    return Convert.ToInt32(column) + 36;
                case "E":
                    return Convert.ToInt32(column) + 48;
                case "F":
                    return Convert.ToInt32(column) + 60;
                case "G":
                    return Convert.ToInt32(column) + 72;
                case "H":
                    return Convert.ToInt32(column) + 84;
            }
            return -1;
        }
        #endregion Helper functions
        
        /// <summary>
        /// Log message
        /// </summary>
        /// <param name="message"></param>
        internal static void Log(string message)
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            string configurationPath = Path.GetDirectoryName(path);
            string logFile = Path.Combine(configurationPath, (DateTime.Today.ToShortDateString() + ".log"));
            File.AppendAllText(logFile, DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + " " + message + Environment.NewLine);
        }
    }
}
