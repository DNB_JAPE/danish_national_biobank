﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace DNBTools
{
    /// <summary>
    /// Helper class for handling wait cursor.
    /// </summary>
    /// <remarks>
    /// <code>
    /// using(WaitCursor wait = new WaitCursor())
    /// {
    ///     // insert lengthy operation code here
    /// }
    /// </code>
    /// </remarks>
    public class WaitCursor : IDisposable
    {
        private static int refCount;

        /// <summary>
        /// Constructor.
        /// </summary>
        public WaitCursor()
        {
            Interlocked.Increment(ref refCount);
            Cursor.Current = Cursors.WaitCursor;
        }

        /// <summary>
        /// returns an IDisposable WaitCursor instance
        /// </summary>
        /// <returns></returns>
        public static IDisposable Create()
        {
            return new WaitCursor();
        }

        ~WaitCursor()
        {
            this.Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (Interlocked.Decrement(ref refCount) == 0)
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #region IDisposable Members
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
