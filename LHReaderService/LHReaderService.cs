﻿using System;
using System.Configuration;
using System.ServiceProcess;
using System.Timers;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Xml.Schema;
using System.Diagnostics;
using System.Threading;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Oracle.DataAccess.Client;
using System.ComponentModel;

namespace LHReaderService
{
    public partial class LHReaderService : ServiceBase
    {
        private string _connectionString;
        private int _checkInterval;
        private List<Reader> _readers = new List<Reader>();
        private System.Timers.Timer _serviceTimer;

        //private System.Threading.Timer _newServiceTimer;

        private volatile bool _NautilusNotRunningAreLogged;

        private bool ExtendedLogging { get; set; }

        public LHReaderService()
        {
            InitializeComponent();
            
            try
            {
                Logger.SetLogWriter(new LogWriterFactory().Create());
                XmlDocument configurationDoc = new XmlDocument();
                configurationDoc.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Configuration.xml"));
                configurationDoc.Schemas.Add(null, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ConfigurationSchema.xsd"));
                ValidationEventHandler eventHandler = new ValidationEventHandler(ValidationEventHandler);
                configurationDoc.Validate(eventHandler);
                ExtendedLogging = Convert.ToBoolean(configurationDoc.SelectSingleNode("/Configuration/ExtendedLogging").InnerXml);
                _connectionString = Encryption.Decrypt(configurationDoc.SelectSingleNode("/Configuration/ConnectionString").InnerXml);
                _checkInterval = int.Parse(configurationDoc.SelectSingleNode("/Configuration/CheckInterval").InnerXml);
                XmlNodeList readerNodes = configurationDoc.SelectNodes("/Configuration/Readers/Project");
                foreach (XmlNode node in readerNodes)
                {
                    string className = node.SelectSingleNode("./Class").InnerXml;
                    switch(className)
                    {
                        case "ReaderBase":
                            {
                                _readers.Add(new ReaderBase(this, node));
                                break;
                            }
                        case "PlateFillReader":
                            {
                                _readers.Add(new PlateFillReader(this, node));
                                break;
                            }
                        case "TestReader":
                            {
                                _readers.Add(new TestReader(this, node));
                                break;
                            }                            
                    }
                    if (ExtendedLogging) Log("Read config for " + className);
                }
                XmlNodeList disposeReaderNodes = configurationDoc.SelectNodes("/Configuration/DisposeReaders/Project");
                foreach (XmlNode node in disposeReaderNodes)
                {
                    _readers.Add(new DisposeReader(this, node));
                }
                if (ExtendedLogging)
                {
                    Log("Read config finished");
                    Log(string.Format("Timer value: {0}", _checkInterval));
                }

                _serviceTimer = new System.Timers.Timer(_checkInterval);
                //_serviceTimer.SynchronizingObject = (ISynchronizeInvoke)this;

                _serviceTimer.Elapsed += new ElapsedEventHandler(_serviceTimer_Elapsed);
                GC.KeepAlive(_serviceTimer);
                if (ExtendedLogging) Log("Timer initialized");
            }
            catch (Exception ex)
            {
                LogError("Failed to initialize: " + ex.Message);
            }
        }

        static void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            LogError("Failed to validate configuration: " + e.Message);
        }

        protected override void OnStart(string[] args)
        {
            Start();
        }

        public void Start()
        {
            Log("LHReaderService starting.");

            EventLog.WriteEntry("LHReaderService in OnStart.");

            _NautilusNotRunningAreLogged = true;
           // _serviceTimer.Elapsed += new ElapsedEventHandler(_serviceTimer_Elapsed);
            _serviceTimer.Enabled = true;

            //GC.KeepAlive(_serviceTimer);

            //_serviceTimer.Enabled = true;
            _serviceTimer.Start();
            Log("LHReaderService started.");
        }

        protected override void OnStop()
        {
            Log("OnStop");
            if (_serviceTimer != null)
            {
                _serviceTimer.Stop();
            }
            //if (_newServiceTimer != null)
            //{
            //    _newServiceTimer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
            //    _newServiceTimer.Dispose();
            //    _newServiceTimer = null;
            //}
        }

        //void _newServiceTimer_Elapsed(object state)
        //{
        //    try
        //    {
        //        if (ExtendedLogging)
        //        {
        //            Log("New Timer elapsed");
        //        }
        //        OracleConnection connection = null;
        //        try
        //        {
        //            _NautilusNotRunningAreLogged = true;
        //            foreach (Reader reader in _readers)
        //            {
        //                if (reader.Active)
        //                {
        //                    if (connection == null || connection.State != System.Data.ConnectionState.Open)
        //                    {
        //                        if (ExtendedLogging) Log(string.Format("Connecting to DB with: {0}", _connectionString));
        //                        connection = new OracleConnection(_connectionString);
        //                        connection.Open();
        //                    }
        //                    try
        //                    {
        //                        Log("Starting " + reader.Name);
        //                        reader.DoWork(connection);
        //                        Log("Finishing " + reader.Name);
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        LogError(ex.Message + " - Skipping " + reader.Name);
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            LogError(ex.Message);
        //        }
        //        finally
        //        {
        //            try
        //            {
        //                if (connection != null)
        //                {
        //                    connection.Close();
        //                    connection.Dispose();
        //                    connection = null;
        //                }
        //            }
        //            catch { }
        //        }

        //    }
        //    catch (Exception x)
        //    {
        //        LogError(x.Message);
        //    }
        //}

        void _serviceTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                CheckForNewData();
            }
            catch (Exception ex)
            {
                LogError(string.Format("Timer elapsed error: {0}", ex.Message));
            }
        }

        void CheckForNewData()
        {
            try
            {
                _serviceTimer.Stop();

                EventLog.WriteEntry("LHReaderService Timer elapsed");
                if (ExtendedLogging)
                {
                    Log("Timer elapsed");
                }

                OracleConnection connection = null;
                try
                {
                    //ServiceController serviceController = new ServiceController("Nautilus Background");
                    //serviceController.Refresh();
                    //if (serviceController.Status == ServiceControllerStatus.Running)
                    //{
                    _NautilusNotRunningAreLogged = true;
                    foreach (Reader reader in _readers)
                    {
                        if (reader.Active)
                        {
                            if (connection == null || connection.State != System.Data.ConnectionState.Open)
                            {
                                connection = new OracleConnection(_connectionString);
                                connection.Open();
                            }
                            try
                            {
                                Log("Starting " + reader.Name);
                                reader.DoWork(connection);
                                Log("Finishing " + reader.Name);
                            }
                            catch (Exception ex)
                            {
                                LogError(ex.Message + " - Skipping " + reader.Name);
                            }
                        }
                    }
                    //}
                    //else if (_NautilusNotRunningAreLogged == true)
                    //{
                    //    _NautilusNotRunningAreLogged = false;
                    //    LogError("Nautilus Background service is not running!");
                    //}
                }
                catch (Exception ex)
                {
                    LogError(ex.Message);
                }
                finally
                {
                    try
                    {
                        if (connection != null)
                        {
                            connection.Close();
                            connection.Dispose();
                            connection = null;
                        }
                    }
                    catch { }
                }

            }
            catch (Exception x)
            {
                LogError(x.Message);
            }
            //_serviceTimer.Enabled = true;
            _serviceTimer.Start();
        }

        internal bool CreateDirectory(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Log message
        /// </summary>
        /// <param name="message"></param>
        internal static void Log(string message)
        {
            LogEntry entryStart = new LogEntry();
            entryStart.Message = message;
            Logger.Write(entryStart);
        }

        /// <summary>
        /// Extended log including callstack amo.
        /// </summary>
        /// <param name="message"></param>
        internal static void LogError(string message)
        {
            LogEntry entryStart = new LogEntry();
            entryStart.Message = "Error: " + message;
            Logger.Write(entryStart);
        }
    }
}
