﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml;

namespace LHReaderService
{
    internal class PlateFillReader : ReaderBase
    {
        internal PlateFillReader(LHReaderService service, XmlNode readerNode)
            : base(service, readerNode)
        {
        }

        internal override void DoWork(OracleConnection connection)
        {
            // Find source files and convert to csv
            List<Tuple<FileInfo, FileInfo>> inputFilesSets = FindFiles();
            if (inputFilesSets != null)
            {
                foreach (Tuple<FileInfo, FileInfo> inputFileSet in inputFilesSets)
                {
                    if (inputFileSet != null)
                    {
                        try
                        {
                            List<FileInfo> activeFileSet = new List<FileInfo>();
                            if (inputFileSet.Item1 != null && inputFileSet.Item1.Exists)
                            {
                                activeFileSet.Add(inputFileSet.Item1);
                            }
                            if (inputFileSet.Item2 != null && inputFileSet.Item2.Exists)
                            {
                                activeFileSet.Add(inputFileSet.Item2);
                            }
                            // Read robot file
                            List<Plate> plates = ParseInputFile(inputFileSet.Item1, new Dictionary<string, string>());
                            if (plates == null) // Parse failed
                            {
                                MoveFiles(activeFileSet, _configuration.ErrorPath);
                                continue;
                            }
                            // Create build plate files
                            if (!WriteAliquotFiles(connection, plates, inputFileSet.Item1))
                            {
                                MoveFiles(activeFileSet, _configuration.ErrorPath);
                                continue;
                            }
                            List<string> sourcePlates = GetSourcePlates(plates, connection);
                            if (!DisposeEmptyPlates(sourcePlates, connection))
                            {
                                LHReaderService.Log("Failed during dispose plates");
                                continue;
                            }
                            // Cleanup
                            MoveFiles(activeFileSet, _configuration.BackupPath);
                        }
                        catch (Exception ex)
                        {
                            LHReaderService.Log(ex.Message);
                            break;
                        }
                    }
                }
            }
        }

        internal List<string> GetSourcePlates(List<Plate> targetPlates, OracleConnection connection)
        {
            if(targetPlates.Count > 0)
            {
            string aliquotString = string.Empty;
            foreach(Plate plate in targetPlates)
            {
                foreach(Aliquot aliquot in plate.Aliquots)
                {
                    aliquotString = aliquotString + "'" + aliquot.Parts[plate.Columns[_configuration.SourceSampleBCColumnName]] + "',";
                }
            }
            aliquotString = aliquotString.TrimEnd(',');
            List<string> retval = new List<string>();
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = connection;
                command.CommandText = "SELECT UNIQUE(External_Reference) FROM plate WHERE plate_id IN (SELECT plate_id FROM aliquot WHERE aliquot_id IN (" + aliquotString + "))";
                command.CommandType = System.Data.CommandType.Text;
                using (OracleDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string barcode = (string)reader["External_Reference"];
                        retval.Add(barcode);
                    }
                }
            }
            return retval;
            }
            return null;
        }

        /// <summary>
        /// Wait for each of the files to disappear (be removed be Nautilus instruments).
        /// </summary>
        /// <param name="masterFiles"></param>
        /// <returns></returns>
        internal static bool WaitForFilesToBeRemoved(List<FileInfo> files, int waitCount, int waitInterval)
        {
            List<FileInfo> filesLeft = new List<FileInfo>();
            if (files.Count > 0)
            {
                int waited = 0;
                while (waited < waitCount)
                {
                    Thread.Sleep(waitInterval);
                    waited++;
                    filesLeft = new List<FileInfo>();
                    foreach (FileInfo file in files)
                    {
                        file.Refresh();
                        if (file.Exists)
                        {
                            filesLeft.Add(file);
                        }
                        else
                        {
                            LHReaderService.Log("File is removed by instrument: " + file.Name);
                        }
                    }
                    if (filesLeft.Count == 0)
                    {
                        LHReaderService.Log("All files are removed by instrument");
                        break;
                    }
                }
            }
            return filesLeft.Count > 0 ? false : true;
        }

        private bool DisposeEmptyPlates(List<string> plates, OracleConnection connection)
        {
            bool retval = true;
            foreach (string plate in plates)
            {
                if (IsPlateEmpty(plate, connection))
                {
                    try
                    {
                        DisposePlate(plate, connection);
                    }
                    catch (Exception ex)
                    {
                        LHReaderService.Log("Failed to dispose plate: " + plate + " " + ex.Message);
                        retval = false;
                    }
                }
            }
            return retval;
        }

        public bool IsPlateEmpty(string barcode, OracleConnection connection)
        {
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = connection;
                command.CommandText = "SELECT COUNT(*) FROM Aliquot WHERE Plate_Id = (SELECT Plate_Id FROM Plate WHERE EXTERNAL_REFERENCE = '##BARCODE##')";
                command.CommandText = command.CommandText.Replace("##BARCODE##", barcode);
                command.CommandType = System.Data.CommandType.Text;
                decimal count = (decimal)command.ExecuteScalar();
                return count == 0 ? true : false;
            }
        }

        public void DisposePlate(string barcode, OracleConnection connection)
        {
            long plate_Id = -1;
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = connection;
                command.CommandText = "SELECT Plate_Id FROM Plate WHERE EXTERNAL_REFERENCE = '##BARCODE##'";
                command.CommandText = command.CommandText.Replace("##BARCODE##", barcode);
                command.CommandType = System.Data.CommandType.Text;
                plate_Id = (long)command.ExecuteScalar();
            }
            List<long> sampleIds = GetSampleIdsFromPlate(barcode, connection);
            string sampleIdString = string.Empty;
            foreach (long id in sampleIds)
            {
                sampleIdString = sampleIdString + "'" + id + "',";
            }
            sampleIdString = sampleIdString.TrimEnd(',');
            using (OracleTransaction transaction = connection.BeginTransaction())
            {
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "UPDATE Plate SET Location_Id='5948' WHERE Plate_Id='##PLATE_ID##'";
                    command.CommandText = command.CommandText.Replace("##PLATE_ID##", plate_Id.ToString());
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "UPDATE Plate_User SET U_Disposed='T' WHERE Plate_Id='##PLATE_ID##'";
                    command.CommandText = command.CommandText.Replace("##PLATE_ID##", plate_Id.ToString());
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "UPDATE Aliquot SET Location_Id='5948' WHERE Aliquot_Id IN (" + sampleIdString + ")";
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "UPDATE Aliquot_User SET U_Disposed='T' WHERE Aliquot_Id IN (" + sampleIdString + ")";
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
                transaction.Commit();
            }
        }

        public List<long> GetSampleIdsFromPlate(string barcode, OracleConnection connection)
        {
            List<long> retval = new List<long>();
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = connection;
                command.CommandText = "SELECT Aliquot_Id FROM Aliquot WHERE Plate_Id = (SELECT Plate_Id FROM Plate WHERE EXTERNAL_REFERENCE = '##BARCODE##')";
                command.CommandText = command.CommandText.Replace("##BARCODE##", barcode);
                command.CommandType = System.Data.CommandType.Text;
                using (OracleDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        long id = (long)reader["Aliquot_Id"];
                        retval.Add(id);
                    }
                }
            }
            return retval;
        }
    }
}
