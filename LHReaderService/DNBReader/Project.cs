﻿using DNBReader.DataClasses;
using DNBReader.Modules;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;

namespace DNBReader
{
    /// <summary>
    /// Represents a single project. This class contains the module queue.
    /// </summary>
    public class Project
    {
        internal virtual string Name { get; set; }
        internal virtual string Description { get; set; }
        internal virtual string BackupPath { get; set; }
        internal virtual string ErrorPath { get; set; }
        internal virtual List<string> DataFiles { get; set; }
        private List<DNBReader.Modules.Module> _modules = new List<DNBReader.Modules.Module>();

        public Project() { }

        internal Project(XmlNode configuration, XmlNode defaultConfiguration)
        {
            Name = configuration.Attributes["Name"].Value;
            Description = configuration.Attributes["Description"].Value;
            BackupPath = configuration.Attributes["BackupPath"].Value;
            ErrorPath = configuration.Attributes["ErrorPath"].Value;
            DNBLogger.Log("Project", "Initialize");
            Assembly assembly = Assembly.GetExecutingAssembly();
            foreach (XmlNode moduleConfiguration in configuration.ChildNodes)
            {
                Type type = assembly.GetTypes().First(t => t.Name == moduleConfiguration.Name);
                DNBReader.Modules.Module module = (DNBReader.Modules.Module)Activator.CreateInstance(type);
                module.Initialize(moduleConfiguration, defaultConfiguration, this);
                _modules.Add(module);
            }
        }

        internal bool DoStuff()
        {
            DataFiles = new List<string>();
            DataFile file = null;
            foreach (DNBReader.Modules.Module module in _modules)
            {
                file = module.DoStuff(file);
                if(file == null)
                {
                    MoveFiles(ErrorPath);
                    return false;
                }
            }
            MoveFiles(BackupPath);
            return true;
        }

        /// <summary>
        /// Helper function for moving files.
        /// </summary>
        /// <param name="path"></param>
        internal virtual void MoveFiles(string path)
        {
            try
            {
                foreach (string file in DataFiles)
                {
                    FileInfo fileInfo = new FileInfo(file);
                    if (fileInfo.Exists)
                    {
                        fileInfo.MoveTo(Path.Combine(path, fileInfo.Name));
                    }
                }
            }
            catch (Exception ex)
            {
                DNBLogger.Log("Project", "Error moving files for " + Name + " to " + path + ": " + ex.Message);
            }
        }
    }
}
