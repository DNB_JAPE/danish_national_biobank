﻿using System.Collections.Generic;
using System.Linq;

namespace DNBReader.DataClasses
{
    /// <summary>
    /// Data class representing a data file.
    /// </summary>
    public class DataFile
    {
        public string Filename { get; set; }
        public string Header { get; set; }
        public char DataDelimiter { get; set; }
        public List<DataGroup> Groups { get; set; }

        public DataFile()
        {
            Groups = new List<DataGroup>();
        }

        /// <summary>
        /// Adds a column to the header and to the datarows. The rows are given a default value.
        /// </summary>
        public virtual void AddColumn(string column, string columnValue)
        {
            string[] headerParts = Header == null ? new string[]{} : Header.Split(DataDelimiter);
            if(!headerParts.Contains(column))
            {
                List<string> headerPartsList = headerParts.ToList();
                headerPartsList.Add(column);
                Header = string.Join(DataDelimiter.ToString(), headerPartsList);
            }
            foreach(DataGroup group in Groups)
            {
                group.AddColumn(column, columnValue);
            }
        }
    }
}
