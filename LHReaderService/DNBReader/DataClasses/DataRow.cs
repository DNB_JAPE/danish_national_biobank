﻿using System.Collections.Generic;
using System.Linq;

namespace DNBReader.DataClasses
{
    /// <summary>
    /// Data class representing a line in a data file.
    /// </summary>
    public class DataRow
    {
        public DataGroup Group { get; set; }
        public Dictionary<string, string> Cells { get; set; }

        public DataRow(DataGroup group)
        {
            Group = group;
            Cells = new Dictionary<string, string>();
        }

        /// <summary>
        /// Prettyprint values of the row.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Join(Group.File.DataDelimiter.ToString(), Cells.Values);
        }
    }
}
