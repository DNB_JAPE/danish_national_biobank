﻿using System.Collections.Generic;

namespace DNBReader.DataClasses
{
    /// <summary>
    /// Data class representing a logical group of lines in a data file.
    /// The obvious group division is plates.
    /// </summary>
    public class DataGroup
    {
        public DataFile File { get; set; }
        public List<DataRow> DataRows { get; set; }

        public DataGroup(DataFile file)
        {
            File = file;
            DataRows = new List<DataRow>();
        }

        /// <summary>
        /// Adds a column to the datarows. The rows are given a default value.
        /// </summary>
        public virtual void AddColumn(string column, string columnValue)
        {
            foreach(DataRow row in DataRows)
            {
                if (!row.Cells.ContainsKey(column))
                {
                    row.Cells.Add(column, columnValue);
                }
            }
        }
    }
}
