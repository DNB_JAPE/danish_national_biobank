﻿using DNBReader.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Splits data into groups. The obvious use for this class is to group rows into plates.
    /// </summary>
    internal class ByColumnGrouper : Grouper
    {
        internal string Column { get; set; }

        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("ByColumnGrouper", "Initialize");
            project = Project;
            XmlNode columnNode = configuration != null ? configuration.SelectSingleNode("Column") : null;
            Column = columnNode != null ? columnNode.InnerXml : defaultConfiguration.SelectSingleNode("Groupers/ByColumnGrouper/Column").InnerXml;
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("ByColumnGrouper", "DoStuff");
            if (file == null)
            {
                DNBLogger.Log("ByColumnGrouper", "Data is not defined");
                return null;
            }
            if (file.Groups.Count() != 1)
            {
                DNBLogger.Log("ByColumnGrouper", "Wrong number of datagroups in file (" + file.Groups.Count() + ")");
                return null;
            }
            DataFile groupedFile = new DataFile();
            groupedFile.Filename = file.Filename;
            groupedFile.DataDelimiter = file.DataDelimiter;
            groupedFile.Header = file.Header;
            Dictionary<string, DataGroup> groups = new Dictionary<string, DataGroup>();
            foreach (DataRow row in file.Groups[0].DataRows)
            {
                string groupName = row.Cells[Column];
                if(!groups.ContainsKey(groupName))
                {
                    DNBLogger.Log("ByColumnGrouper", "Starting new group: " + groupName);
                    DataGroup newGroup = new DataGroup(groupedFile);
                    groupedFile.Groups.Add(newGroup);
                    groups.Add(groupName, newGroup);
                }
                row.Group = groups[groupName];
                groups[groupName].DataRows.Add(row);
            }
            DNBLogger.Log("ByColumnGrouper", "DoStuff end");
            return groupedFile;
        }
    }
}
