﻿using DNBReader.DataClasses;
using System;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Check that volume is legal.
    /// </summary>
    internal class VolumeFilter : Filter
    {
        public string Column { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }

        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("VolumeFilter", "Initialize");
            Project = project;
            XmlNode columnNode = configuration != null ? configuration.SelectSingleNode("Column") : null;
            Column = columnNode != null ? columnNode.InnerXml : defaultConfiguration.SelectSingleNode("Filters/VolumeFilter/Column").InnerXml;
            XmlNode minNode = configuration != null ? configuration.SelectSingleNode("Min") : null;
            Min = Convert.ToInt32(minNode != null ? minNode.InnerXml : defaultConfiguration.SelectSingleNode("Filters/VolumeFilter/Min").InnerXml);
            XmlNode maxNode = configuration != null ? configuration.SelectSingleNode("Max") : null;
            Max = Convert.ToInt32(maxNode != null ? maxNode.InnerXml : defaultConfiguration.SelectSingleNode("Filters/VolumeFilter/Max").InnerXml);
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("VolumeFilter", "DoStuff");
            if (file == null)
            {
                DNBLogger.Log("VolumeFilter", "Data is not defined");
                return null;
            }
            foreach (DataGroup plate in file.Groups)
            {
                foreach (DataRow row in plate.DataRows)
                {
                    int volume;
                    if (!Int32.TryParse(row.Cells[Column], out volume) || volume < Min || volume > Max)
                    {
                        DNBLogger.Log("VolumeFilter", "Error: Invalid volume (" + row.Cells[Column] + ")");
                        return null;
                    }
                }
            }
            DNBLogger.Log("VolumeFilter", "DoStuff end");
            return file;
        }
    }
}
