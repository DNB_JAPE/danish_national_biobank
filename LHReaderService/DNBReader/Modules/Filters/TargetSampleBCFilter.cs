﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Check if target sample bc is valid
    /// </summary>
    internal class TargetSampleBCFilter : RegexFilter
    {
        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("RegexFilter", "Initialize");
            Project = project;
            Column = "TPositionBC";
            Regex = new Regex(@"(^$|^.*E\+.*$|^----------$|\W)");
            Match = false;
        }
    }
}
