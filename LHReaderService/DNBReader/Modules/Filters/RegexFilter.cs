﻿using DNBReader.DataClasses;
using System;
using System.Text.RegularExpressions;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Check is values follow a regular expression or not.
    /// </summary>
    internal class RegexFilter : Filter
    {
        public string Column { get; set; }
        public Regex Regex { get; set; }
        public bool Match { get; set; }

        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("RegexFilter", "Initialize");
            Project = project;
            XmlNode columnNode = configuration != null ? configuration.SelectSingleNode("Column") : null;
            Column = columnNode != null ? columnNode.InnerXml : defaultConfiguration.SelectSingleNode("Filters/RegexFilter/Column").InnerXml;
            XmlNode regexNode = configuration != null ? configuration.SelectSingleNode("Regex") : null;
            Regex = new Regex(regexNode != null ? regexNode.InnerXml : defaultConfiguration.SelectSingleNode("Filters/RegexFilter/Regex").InnerXml);
            XmlNode matchNode = configuration != null ? configuration.SelectSingleNode("Match") : null;
            Match = Convert.ToBoolean(matchNode != null ? matchNode.InnerXml : defaultConfiguration.SelectSingleNode("Filters/RegexFilter/Match").InnerXml);
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("RegexFilter", "DoStuff");
            if (file == null)
            {
                DNBLogger.Log("RegexFilter", "Data is not defined");
                return null;
            }
            foreach (DataGroup plate in file.Groups)
            {
                foreach (DataRow row in plate.DataRows)
                {
                    string data = row.Cells[Column];
                    if (Match != Regex.IsMatch(data))
                    {
                        DNBLogger.Log("RegexFilter", "Line is not valid: " + data);
                        return null;
                    }
                }
            }
            DNBLogger.Log("RegexFilter", "DoStuff end");
            return file;
        }
    }
}
