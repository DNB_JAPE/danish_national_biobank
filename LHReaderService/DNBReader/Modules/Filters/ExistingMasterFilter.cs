﻿using DNBReader.DataClasses;
using System;
using System.Collections.Generic;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Verify that all masters can be looked up for reruns and CreateMaster=Never.
    /// Lookup is based on sql placed in configuration. The following substitution strings are supported in sql:
    /// ##MASTERDESCRIPTION## - The substituted description of the master (of course this gives no meaning if DescriptionMapper and/or DescriptionFileMapper has not previously run).
    /// ##SOURCEALIQUOTBC## - Value of source sample barcode column.
    /// ##SOURCEPLATEBC## - Value of source plate barcode column.
    /// ##PROJECT## - Project name.
    /// ##CONTAINERTYPE## - Value of container type column.
    /// </summary>
    internal class ExistingMasterFilter : Filter
    {
        public string CreateMasters { get; set; }
        public string Sql { get; set; }
        public string SampleBCColumn { get; set; }
        public string PlateBCColumn { get; set; }
        public string DescriptionColumn { get; set; }
        public string ProjectColumn { get; set; }
        public string ContainerTypeColumn { get; set; }

        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("ExistingMasterFilter", "Initialize");
            Project = project;
            XmlNode createMastersNode = configuration != null ? configuration.SelectSingleNode("CreateMasters") : null;
            CreateMasters = createMastersNode != null ? createMastersNode.InnerXml.ToUpper() : defaultConfiguration.SelectSingleNode("Filters/ExistingMasterFilter/CreateMasters").InnerXml.ToUpper();
            XmlNode sqlNode = configuration != null ? configuration.SelectSingleNode("Sql") : null;
            Sql = sqlNode != null ? sqlNode.InnerXml.ToUpper() : defaultConfiguration.SelectSingleNode("Filters/ExistingMasterFilter/Sql").InnerXml.ToUpper();
            XmlNode sampleBCColumnNode = configuration != null ? configuration.SelectSingleNode("SampleBCColumn") : null;
            SampleBCColumn = sampleBCColumnNode != null ? sampleBCColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Filters/ExistingMasterFilter/SampleBCColumn").InnerXml;
            XmlNode plateBCColumnNode = configuration != null ? configuration.SelectSingleNode("PlateBCColumn") : null;
            PlateBCColumn = plateBCColumnNode != null ? plateBCColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Filters/ExistingMasterFilter/PlateBCColumn").InnerXml;
            XmlNode descriptionColumnNode = configuration != null ? configuration.SelectSingleNode("DescriptionColumn") : null;
            DescriptionColumn = descriptionColumnNode != null ? descriptionColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Filters/ExistingMasterFilter/DescriptionColumn").InnerXml;
            XmlNode projectColumnNode = configuration != null ? configuration.SelectSingleNode("ProjectColumn") : null;
            ProjectColumn = projectColumnNode != null ? projectColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Filters/ExistingMasterFilter/ProjectColumn").InnerXml;
            XmlNode containerTypeColumnNode = configuration != null ? configuration.SelectSingleNode("ContainerTypeColumn") : null;
            ContainerTypeColumn = containerTypeColumnNode != null ? containerTypeColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Filters/ExistingMasterFilter/ContainerTypeColumn").InnerXml;
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("ExistingMasterFilter", "DoStuff");
            if (file == null)
            {
                DNBLogger.Log("ExistingMasterFilter", "Data is not defined");
                return null;
            }
            bool error = false;
            file.AddColumn("CreateMaster", "true");
            if (file.Filename.ToUpper().Contains(".RERUN") || CreateMasters.ToUpper() == "NEVER" || CreateMasters.ToUpper() == "IFNOTEXIST")
            {
                foreach (DataGroup plate in file.Groups)
                {
                    foreach (DataRow row in plate.DataRows)
                    {
                        string sql = Sql;
                        if (sql.Contains("##MASTERDESCRIPTION##"))
                        {
                            if (row.Cells.ContainsKey(DescriptionColumn))
                            {
                                sql = sql.Replace("##MASTERDESCRIPTION##", row.Cells[DescriptionColumn]);
                            }
                            else
                            {
                                DNBLogger.Log("ExistingMasterFilter", "Description column (" + DescriptionColumn + ") missing in data");
                                return null;
                            }
                        }
                        if (sql.Contains("##SOURCEALIQUOTBC##"))
                        {
                            if (row.Cells.ContainsKey(SampleBCColumn))
                            {
                                sql = sql.Replace("##SOURCEALIQUOTBC##", row.Cells[SampleBCColumn]);
                            }
                            else
                            {
                                DNBLogger.Log("ExistingMasterFilter", "Source sample barcode column (" + SampleBCColumn + ") missing in data");
                                return null;
                            }
                        }
                        if (sql.Contains("##SOURCEPLATEBC##"))
                        {
                            if (row.Cells.ContainsKey(PlateBCColumn))
                            {
                                sql = sql.Replace("##SOURCEPLATEBC##", row.Cells[PlateBCColumn]);
                            }
                            else
                            {
                                DNBLogger.Log("ExistingMasterFilter", "Source plate barcode column (" + PlateBCColumn + ") missing in data");
                                return null;
                            }
                        }
                        if (sql.Contains("##PROJECT##"))
                        {
                            if (row.Cells.ContainsKey(ProjectColumn))
                            {
                                sql = sql.Replace("##PROJECT##", row.Cells[ProjectColumn]);
                            }
                            else
                            {
                                DNBLogger.Log("ExistingMasterFilter", "Project column (" + ProjectColumn + ") missing in data");
                                return null;
                            }
                        }
                        if (sql.Contains("##CONTAINERTYPE##"))
                        {
                            if (row.Cells.ContainsKey(ContainerTypeColumn))
                            {
                                sql = sql.Replace("##CONTAINERTYPE##", row.Cells[ContainerTypeColumn]);
                            }
                            else
                            {
                                DNBLogger.Log("ExistingMasterFilter", "Container type column (" + ContainerTypeColumn + ") missing in data");
                                return null;
                            }
                        }
                        DNBLogger.Log("ExistingMasterFilter", sql);
                        List<String> ids = Service.LimsCommunication.SampleGetIdsFromSql(sql);
                        if (ids == null)
                        {
                            DNBLogger.Log("ExistingMasterFilter", "Failed to look up ids in Nautilus");
                            return null;
                        }
                        int count = ids.Count;
                        if (count == 0)
                        {
                            if (CreateMasters.ToUpper() == "IFNOTEXIST")
                            {
                                row.Cells["CreateMaster"] = "true";
                            }
                            else
                            {
                                error = true;
                            }
                        }
                        else if (count == 1)
                        {
                            if (CreateMasters.ToUpper() == "IFNOTEXIST")
                            {
                                row.Cells["CreateMaster"] = "false";
                            }
                        }
                        else
                        {
                            DNBLogger.Log("ExistingMasterFilter", "Found " + count + " masters for " + row.Cells[SampleBCColumn]);
                            error = true;
                        }
                    }
                }
            }
            DNBLogger.Log("ExistingMasterFilter", "DoStuff end");
            return error ? null : file;
        }
    }
}
