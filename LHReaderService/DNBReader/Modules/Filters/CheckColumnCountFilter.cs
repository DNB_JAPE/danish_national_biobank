﻿using DNBReader.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Verifies that all lines have the correct number of columns.
    /// </summary>
    internal class CheckColumnCountFilter : Filter
    {
        internal int ColumnCount { get; set; }

        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {            
            DNBLogger.Log("CheckColumnCountFilter", "Initialize");
            project = Project;
            XmlNode columnCountNode = configuration != null ? configuration.SelectSingleNode("CheckColumnCount") : null;
            ColumnCount = Convert.ToInt32(columnCountNode != null ? columnCountNode.InnerXml : defaultConfiguration.SelectSingleNode("Filters/CheckColumnCountFilter/CheckColumnCount").InnerXml);
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("CheckColumnCountFilter", "DoStuff");
            if (file == null)
            {
                DNBLogger.Log("CheckColumnCountFilter", "Data is not defined");
                return null;
            }
            string[] headerParts = file.Header.Split(file.DataDelimiter);
            if(headerParts.Count() != ColumnCount)
            {
                DNBLogger.Log("CheckColumnCountFilter", "Invalid number of columns in header");
                return null;
            }
            bool error = false;
            foreach(DataGroup group in file.Groups)
            {
                foreach(DataRow row in group.DataRows)
                {
                    if (row.Cells.Count() != ColumnCount)
                    {
                        error = true;
                        DNBLogger.Log("CheckColumnCountFilter", "Invalid number of columns in line: " + row.ToString());
                    }
                }
            }
            DNBLogger.Log("CheckColumnCountFilter", "DoStuff end");
            return error ? null : file;
        }
    }
}
