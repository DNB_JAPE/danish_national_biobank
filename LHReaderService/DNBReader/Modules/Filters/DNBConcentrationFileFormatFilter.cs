﻿using DNBReader.DataClasses;
using System;
using System.Collections.Generic;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Check that data contains all columns expected in the DNBConcentration file format.
    /// 
    /// Format:
    ///   0     RecordId              - Rownumber in file
    ///   1     TRackBC	              - Plate barcode (Output)
    ///   2     Tube_Type	          - Sample tube type (Output)
    ///   3     TPositionId           - Position (Output)
    ///   4     TPositionBC           - Sample barcode (Output)
    ///   5     TStatusSummary        - Robot error code
    ///   6     TSumStateDescription  - Robot error description
    ///   7     TVolume               - Volume (Output)
    ///   8     SRackBC               - Plate barcode (Input)
    ///   9     Sample_Type           - Material type (Output)
    ///   10    SPositionId           - Position (Input)
    ///   11    SPositionBC           - Sample barcode (Input)
    ///   12    ActionDateTime        - Timestamp for sample processing
    ///   13    UserName              - Operator name
    ///   14    Instrument            - Robot name                       
    ///   15    Project               - Project name
    ///   16    UserComment           - Operator comment
    ///   17	230nm	
    ///   18	260nm	
    ///   19	280nm;	
    ///   20    Concentration	      - DNA Concentration
    ///   21	A260/A230	          - Ratio
    ///   22	A260/A280             - Ratio
    /// </summary>
    internal class DNBConcentrationFileFormatFilter : Filter
    {
        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("DNBConcentrationFileFormatFilter", "Initialize");
            project = Project;
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("DNBConcentrationFileFormatFilter", "DoStuff");
            if (file == null)
            {
                DNBLogger.Log("DNBConcentrationFileFormatFilter", "Data is not defined");
                return null;
            }
            if (file.Groups.Count == 0)
            {
                DNBLogger.Log("DNBConcentrationFileFormatFilter", "No plates in data file: " + file.Filename);
                return file;
            }
            foreach (DataGroup group in file.Groups)
            {
                foreach (DataRow row in group.DataRows)
                {
                    if (!row.Cells.ContainsKey("RecordId") ||
                        !row.Cells.ContainsKey("TRackBC") ||
                        !row.Cells.ContainsKey("Tube_Type") ||
                        !row.Cells.ContainsKey("TPositionId") ||
                        !row.Cells.ContainsKey("TPositionBC") ||
                        !row.Cells.ContainsKey("TStatusSummary") ||
                        !row.Cells.ContainsKey("TSumStateDescription") ||
                        !row.Cells.ContainsKey("TVolume") ||
                        !row.Cells.ContainsKey("SRackBC") ||
                        !row.Cells.ContainsKey("Sample_Type") ||
                        !row.Cells.ContainsKey("SPositionId") ||
                        !row.Cells.ContainsKey("SPositionBC") ||
                        !row.Cells.ContainsKey("ActionDateTime") ||
                        !row.Cells.ContainsKey("UserName") ||
                        !row.Cells.ContainsKey("Instrument") ||
                        !row.Cells.ContainsKey("Project") ||
                        !row.Cells.ContainsKey("UserComment") ||
                        !row.Cells.ContainsKey("230nm") ||
                        !row.Cells.ContainsKey("260nm") ||
                        !row.Cells.ContainsKey("280nm") ||
                        !row.Cells.ContainsKey("Concentration") ||
                        !row.Cells.ContainsKey("A260/A230") ||
                        !row.Cells.ContainsKey("A260/A280"))
                    {
                        DNBLogger.Log("DNBConcentrationFileFormatFilter", "Data does not follow standard. Not all columns are present.");
                        return null;
                    }
                }
            }
            DNBLogger.Log("DNBConcentrationFileFormatFilter", "DoStuff end");
            return file;
        }
    }
}
