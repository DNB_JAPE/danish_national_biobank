﻿using DNBReader.DataClasses;
using System;
using System.Collections.Generic;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Check that data contains all columns expected in the DNBConcentration file format.
    /// 
    /// Format:
    ///   0     POS_IN	              - Sample position (input)
    ///   1	    BC_IN	              - Plate barcode (input)
    ///   2	    TRANSFER_IN	
    ///   3	    PROTEASE_ADDED
    ///   4	    LYSIS_ADDED
    ///   5	    WASH_3_ADDED
    ///   6	    WASH_4_ADDED
    ///   7	    WASH_5_ADDED
    ///   8	    WASH_6_ADDED
    ///   9	    BIND_BUFFER_ADDED
    ///   10	BEADS_ADDED
    ///   11	ELUTION_BUFF_ADDED
    ///   12	VOL	                  - Volume (output)
    ///   13	TRANSFER_OUT
    ///   14	POS_OUT	              - Sample Position (output)
    ///   15	BC_RACK_OUT	          - Plate barcode (output)
    ///   16	BC_OUT	              - Sample barcode (output)
    ///   17	ActionDateTime	      - Timestamp of sample processing
    ///   18	UserName	          - Operator name (text)
    ///   19	Instrument	          - Robot name (optional)
    ///   20	Project 	          - Project name
    ///   21	Kit_Lot	              - Kit Lot
    ///   22	Tube_Type             - Sample tube type (output)
    ///   23	Sample_Type           - Material type (output)
    /// </summary>
    internal class DNBExtractionFileFormatFilter : Filter
    {
        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("DNBExtractionFileFormatFilter", "Initialize");
            project = Project;
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("DNBExtractionFileFormatFilter", "DoStuff");
            if (file == null)
            {
                DNBLogger.Log("DNBExtractionFileFormatFilter", "Data is not defined");
                return null;
            }
            if (file.Groups.Count == 0)
            {
                DNBLogger.Log("DNBExtractionFileFormatFilter", "No plates in data file: " + file.Filename);
                return file;
            }
            foreach (DataGroup group in file.Groups)
            {
                foreach (DataRow row in group.DataRows)
                {
                    if (!row.Cells.ContainsKey("POS_IN") ||
                        !row.Cells.ContainsKey("BC_IN") ||
                        !row.Cells.ContainsKey("TRANSFER_IN") ||
                        !row.Cells.ContainsKey("PROTEASE_ADDED") ||
                        !row.Cells.ContainsKey("LYSIS_ADDED") ||
                        !row.Cells.ContainsKey("WASH_3_ADDED") ||
                        !row.Cells.ContainsKey("WASH_4_ADDED") ||
                        !row.Cells.ContainsKey("WASH_5_ADDED") ||
                        !row.Cells.ContainsKey("WASH_6_ADDED") ||
                        !row.Cells.ContainsKey("BIND_BUFFER_ADDED") ||
                        !row.Cells.ContainsKey("BEADS_ADDED") ||
                        !row.Cells.ContainsKey("ELUTION_BUFF_ADDED") ||
                        !row.Cells.ContainsKey("VOL") ||
                        !row.Cells.ContainsKey("TRANSFER_OUT") ||
                        !row.Cells.ContainsKey("POS_OUT") ||
                        !row.Cells.ContainsKey("BC_RACK_OUT") ||
                        !row.Cells.ContainsKey("BC_OUT") ||
                        !row.Cells.ContainsKey("ActionDateTime") ||
                        !row.Cells.ContainsKey("UserName") ||
                        !row.Cells.ContainsKey("Instrument") ||
                        !row.Cells.ContainsKey("Project") ||
                        !row.Cells.ContainsKey("Kit_Lot") ||
                        !row.Cells.ContainsKey("Tube_Type") ||
                        !row.Cells.ContainsKey("Sample_Type"))
                    {
                        DNBLogger.Log("DNBExtractionFileFormatFilter", "Data does not follow standard. Not all columns are present.");
                        return null;
                    }
                }
            }
            DNBLogger.Log("DNBExtractionFileFormatFilter", "DoStuff end");
            return file;
        }
    }
}
