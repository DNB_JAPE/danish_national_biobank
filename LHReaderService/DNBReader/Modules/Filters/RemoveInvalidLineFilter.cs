﻿using DNBReader.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Removes empty lines and lines where both source and target barcode is Hamilton ignore.
    /// </summary>
    internal class RemoveInvalidLineFilter : Filter
    {
        public string TargetSampleBCColumn { get; set; }
        public string SourceSampleBCColumn { get; set; }

        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("RemoveInvalidLineFilter", "Initialize");
            Project = project;
            XmlNode targetSampleBCColumnNode = configuration != null ? configuration.SelectSingleNode("TargetSampleBCColumn") : null;
            TargetSampleBCColumn = targetSampleBCColumnNode != null ? targetSampleBCColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Filters/RemoveInvalidLineFilter/TargetSampleBCColumn").InnerXml;
            XmlNode sourceSampleBCColumnNode = configuration != null ? configuration.SelectSingleNode("SourceSampleBCColumn") : null;
            SourceSampleBCColumn = sourceSampleBCColumnNode != null ? sourceSampleBCColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Filters/RemoveInvalidLineFilter/SourceSampleBCColumn").InnerXml;
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("RemoveInvalidLineFilter", "DoStuff");
            if (file == null)
            {
                DNBLogger.Log("RemoveInvalidLineFilter", "Data is not defined");
                return null;
            }
            foreach (DataGroup plate in file.Groups)
            {
                plate.DataRows.RemoveAll(x => (x.Cells[TargetSampleBCColumn] == "" && x.Cells[SourceSampleBCColumn] == "") ||
                                              (x.Cells[TargetSampleBCColumn] == "----------" && x.Cells[SourceSampleBCColumn] == "----------"));
            }
            DNBLogger.Log("RemoveInvalidLineFilter", "DoStuff end");
            return file;
        }
    }
}
