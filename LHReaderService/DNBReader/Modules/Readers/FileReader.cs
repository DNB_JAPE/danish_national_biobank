﻿using DNBReader.DataClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Reads data into a DataFile object.
    /// </summary>
    internal class FileReader : Reader
    {
        internal string Dir { get; set; }
        internal string Pattern { get; set; }
        internal char Delimiter { get; set; }

        public override void Initialize(System.Xml.XmlNode configuration, System.Xml.XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("FileReader", "Initialize");
            Project = project;
            XmlNode dirNode = configuration != null ? configuration.SelectSingleNode("Dir") : null;
            Dir = dirNode != null ? dirNode.InnerXml : defaultConfiguration.SelectSingleNode("Readers/FileReader/Dir").InnerXml;
            XmlNode patternNode = configuration != null ? configuration.SelectSingleNode("Pattern") : null;
            Pattern = patternNode != null ? patternNode.InnerXml : defaultConfiguration.SelectSingleNode("Readers/FileReader/Pattern").InnerXml;
            XmlNode delimiterNode = configuration != null ? configuration.SelectSingleNode("Delimiter") : null;
            Delimiter = Convert.ToChar(delimiterNode != null ? delimiterNode.InnerXml : defaultConfiguration.SelectSingleNode("Readers/FileReader/Delimiter").InnerXml);
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("FileReader", "DoStuff");
            if (file != null)
            {
                DNBLogger.Log("FileReader", "Data exist before file is read");
                return null;
            }
            DirectoryInfo dir = new DirectoryInfo(Dir);
            if (!dir.Exists)
            {
                DNBLogger.Log("FileReader", "Input directory does not exist: " + Dir);
                return null;
            }
            FileInfo[] files = dir.GetFiles(Pattern);
            if (files.Count() > 0)
            {
                DNBLogger.Log("FileReader", "Found " + files.Count() + " files");
                // Select one random file for processing.
                DNBLogger.Log("FileReader", "Parsing " + files[0].Name);
                Project.DataFiles.Add(files[0].FullName);
                try
                {
                    DataFile dataFile = new DataFile();
                    dataFile.Filename = files[0].Name;
                    dataFile.DataDelimiter = Delimiter;
                    DataGroup dataGroup = new DataGroup(dataFile);
                    dataFile.Groups.Add(dataGroup);
                    List<string> readRows = new List<string>();
                    using (StreamReader reader = new StreamReader(files[0].OpenRead()))
                    {
                        string[] headerparts = null;
                        while (true)
                        {
                            string row = reader.ReadLine();
                            if (row == null)
                            {
                                break;
                            }
                            if(readRows.Contains(row))
                            {
                                DNBLogger.Log("FileReader", "Ignoring repeated row: " + row);
                                continue;
                            }
                            row = row.Replace("\"", string.Empty);
                            if (dataFile.Header == null)
                            {
                                dataFile.Header = row;
                                headerparts = row.Split(Delimiter);
                            }
                            else if (row != dataFile.Header)
                            {
                                string[] rowparts = row.Split(Delimiter);
                                if(rowparts.Count() != headerparts.Count())
                                {
                                    DNBLogger.Log("FileReader", "Number of columns does not match header: " + row);
                                    return null;
                                }
                                DataRow dataRow = new DataRow(dataGroup);
                                for(int i=0;i<headerparts.Count();i++)
                                {
                                    dataRow.Cells.Add(headerparts[i], rowparts[i]);
                                }
                                dataGroup.DataRows.Add(dataRow);
                            }
                        }
                    }
                    DNBLogger.Log("FileReader", "DoStuff end");
                    return dataFile;
                }
                catch (Exception ex)
                {
                    DNBLogger.Log("FileReader", "Failed to read file: " + ex.Message);
                    return null;
                }
            }
            DNBLogger.Log("FileReader", "DoStuff end - no files");
            return null;
        }
    }
}