﻿namespace DNBReader.Modules
{
    /// <summary>
    /// Filters remove unwanted data from a Datafile object.
    /// Examples could be duplicate or invalid lines.
    /// Filters can be used to validate and streamline data.
    /// 
    /// Filters should only remove, not change data.
    /// 
    /// Filter names should be postfixed with 'Filter'
    /// </summary>
    public abstract class Filter : Module
    {
    }
}
