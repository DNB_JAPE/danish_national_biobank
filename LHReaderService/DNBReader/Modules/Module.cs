﻿using DNBReader.DataClasses;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// This is the base class for modules.
    /// A module is a logical step in data processing. 
    /// Modules should be kept as simple and generic as possible.
    /// </summary>
    public abstract class Module
    {
        internal Project Project { get; set; }

        /// <summary>
        /// Configuration is divided into two parts.
        /// A default section where settings are specified on class basis.
        /// And a project section where settings are specified on project basis. Only values that differ from default, are specified in this section.
        /// 
        /// Project settings take preceedings over class settings.
        /// </summary>
        /// <param name="configuration">Contains the project specific configuration for this type.</param>
        /// <param name="defaultConfiguration">Contains default configurations for all types.</param>
        /// <param name="project">Reference to the project. Used for adding files to after process copying.</param>
        public virtual void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
        }

        /// <summary>
        /// This is the work function of the module.
        /// It takes a datafile object, does 'stuff' to it and returns a datafile object.
        /// 
        /// If the module returns null, the project will abandon further processing.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public virtual DataFile DoStuff(DataFile file)
        {
            return null;
        }
    }
}
