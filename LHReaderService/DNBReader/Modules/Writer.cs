﻿namespace DNBReader.Modules
{
    /// <summary>
    /// Writers send data to the Lims system - typically be writing a file to a specific folder.
    /// Other types of writers will add the data directly to the Lims database.
    /// 
    /// Writers should not change data. When data gets to a writer, it should be processed.
    /// The writer selects the relevant data from the Datafile object format it and sends it to Lims.
    /// 
    /// A writer is not necessarily the last module in the module chain.
    /// 
    /// Writer names should be postfixed with 'Writer'.
    /// </summary>
    public abstract class Writer : Module
    {
    }
}
