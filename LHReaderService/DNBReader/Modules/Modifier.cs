﻿using DNBReader.DataClasses;
using System.Collections.Generic;
using System.Linq;

namespace DNBReader.Modules
{
    /// <summary>
    /// Modifiers change data. Preferrably they should do this by adding new columns instead of changing existing data.
    /// This will keep the restrictions on module sequence to a minimum.
    /// 
    /// Modifier names shold be postfixed with 'Modifier'.
    /// </summary>
    public abstract class Modifier : Module
    {
        public virtual string[] SourceColumns { get; set; }
        public virtual string TargetColumn { get; set; }

        public override DataFile DoStuff(DataFile datafile)
        {
            DNBLogger.Log("Modifier", "DoStuff (" + Project.Name + ")");
            if (datafile == null)
            {
                DNBLogger.Log("Modifier", "Data is not defined");
                return null;
            }
            bool error = false;
            if (datafile.Groups.Count > 0)
            {
                datafile.AddColumn(TargetColumn, "");
                foreach (DataGroup group in datafile.Groups)
                {
                    foreach (DataRow row in group.DataRows)
                    {
                        List<string> data = row.Cells.Where(x => SourceColumns.ToList().Contains(x.Key)).Select(x => x.Value).ToList();
                        string target = Modify(data);
                        if (target == null)
                        {
                            DNBLogger.Log("Modifier", "Value could not be handled");
                            error = true;
                        }
                        row.Cells[TargetColumn] = target;
                    }
                }
            }
            DNBLogger.Log("Modifier", "DoStuff end (" + Project.Name + ")");
            return error ? null : datafile;
        }

        /// <summary>
        /// Simple modifiers can override this function to lookup data.
        /// </summary>
        /// <returns></returns>
        protected virtual string Modify(List<string> sources) 
        {
            return null; 
        }
    }
}
