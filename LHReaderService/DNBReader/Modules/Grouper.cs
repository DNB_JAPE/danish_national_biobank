﻿namespace DNBReader.Modules
{
    /// <summary>
    /// Groupers take a Datafile object and reorganize the rows into groups.
    /// An obvious example, groupes samples into plates.
    /// 
    /// Groupers should not do other processing.
    /// 
    /// Grouper names should be postfixed with 'Grouper'.
    /// </summary>
    public abstract class Grouper : Module
    {
    }
}
