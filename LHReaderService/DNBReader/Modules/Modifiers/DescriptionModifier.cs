﻿using DNBReader.DataClasses;
using System;
using System.Collections.Generic;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// The following substitutions are made in the description pattern:
    /// ##PROJECT## -> Project name
    /// ##FILENAME## -> Data file name
    /// </summary>
    internal class DescriptionModifier : Modifier
    {
        public string Pattern { get; set; }
        public string Filename { get; set; }

        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("DescriptionModifier", "Initialize");
            Project = project;
            SourceColumns = new string[] { };
            XmlNode sourceNode = configuration != null ? configuration.SelectSingleNode("Pattern") : null;
            Pattern = sourceNode != null ? sourceNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/DescriptionModifier/Pattern").InnerXml;
            XmlNode targetNode = configuration != null ? configuration.SelectSingleNode("TargetColumn") : null;
            TargetColumn = targetNode != null ? targetNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/DescriptionModifier/TargetColumn").InnerXml;
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("DescriptionModifier", "DoStuff (" + Project.Name + ")");
            Filename = file.Filename;
            file = base.DoStuff(file);
            DNBLogger.Log("DescriptionModifier", "DoStuff end (" + Project.Name + ")");
            return file;
        }

        protected override string Modify(List<string> sources)
        {
            string pattern = Pattern.Replace("##PROJECT##", Project.Name);
            pattern = pattern.Replace("##PROJECTDESCRIPTION##", Project.Description);
            pattern = pattern.Replace("##FILENAME##", Filename);
            DNBLogger.Log("DescriptionModifier", "Change description: " + pattern);
            return pattern;
        }
    }
}
