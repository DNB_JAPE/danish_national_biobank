﻿using DNBReader.DataClasses;
using System;
using System.Collections.Generic;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Lookup user name and replace it by the nautilus id.
    /// </summary>
    internal class UserNameModifier : Modifier
    {
        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("UserNameModifier", "Initialize");
            Project = project;
            XmlNode sourceNode = configuration != null ? configuration.SelectSingleNode("SourceColumn") : null;
            SourceColumns = new string[] { sourceNode != null ? sourceNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/UserNameModifier/SourceColumn").InnerXml };
            XmlNode targetNode = configuration != null ? configuration.SelectSingleNode("TargetColumn") : null;
            TargetColumn = targetNode != null ? targetNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/UserNameModifier/TargetColumn").InnerXml;
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("UserNameModifier", "DoStuff (" + Project.Name + ")");
            file = base.DoStuff(file);
            DNBLogger.Log("UserNameModifier", "DoStuff end (" + Project.Name + ")");
            return file;
        }

        protected override string Modify(List<string> sources)
        {
            DNBLogger.Log("UserNameModifier", "Lookup user: " + sources[0]);
            return Service.LimsCommunication.OperatorGetId(sources[0]);
        }
    }
}
