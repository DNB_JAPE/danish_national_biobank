﻿using DNBReader.DataClasses;
using System;
using System.Collections.Generic;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Verify that ActionDateTime is set.
    /// If not and volume = 0, set actionDateTime = Today
    /// </summary>
    internal class ActionDateTimeModifier : Modifier
    {
        public string Undefined { get; set; }

        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("ActionDateTimeModifier", "Initialize");
            Project = project;
            XmlNode actionDateTimeColumnNode = configuration != null ? configuration.SelectSingleNode("ActionDateTimeColumn") : null;
            string actionDateTimeColumn = actionDateTimeColumnNode != null ? actionDateTimeColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/ActionDateTimeModifier/ActionDateTimeColumn").InnerXml;
            TargetColumn = actionDateTimeColumn;
            XmlNode volumeColumnNode = configuration != null ? configuration.SelectSingleNode("VolumeColumn") : null;
            string volumeColumn = volumeColumnNode != null ? volumeColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/ActionDateTimeModifier/VolumeColumn").InnerXml;
            SourceColumns = new string[] { actionDateTimeColumn, volumeColumn };
            XmlNode undefinedNode = configuration != null ? configuration.SelectSingleNode("Undefined") : null;
            Undefined = undefinedNode != null ? undefinedNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/ActionDateTimeModifier/Undefined").InnerXml;
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("ActionDateTimeModifier", "DoStuff (" + Project.Name + ")");
            file = base.DoStuff(file);
            DNBLogger.Log("ActionDateTimeModifier", "DoStuff end (" + Project.Name + ")");
            return file;
        }

        protected override string Modify(List<string> sources)
        {
            if (sources[0] == Undefined)
            {
                if (sources[1] == "0")
                {
                    DNBLogger.Log("ActionDateTimeModifier", "ActionDateTime set to Today");
                    return DateTime.Today.ToString("dd-MM-yyyy HH:mm:ss");
                }
                else
                {
                    DNBLogger.Log("ActionDateTimeModifier", "Undefined actionDateTime and volume != 0");
                    return null;
                }
            }
            return sources[0];
        }
    }
}
