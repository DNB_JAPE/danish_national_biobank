﻿using DNBReader.DataClasses;
using DNBReader.Modules;
using System;
using System.Collections.Generic;
using System.Xml;

namespace DNBReader
{
    /// <summary>
    /// Lookup aliquot and insert the nautilus id.
    /// </summary>
    internal class AliquotIdModifier : Modifier
    {
        public string Sql { get; set; }

        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("AliquotIdModifier", "Initialize");
            Project = project;
            XmlNode idColumnNode = configuration != null ? configuration.SelectSingleNode("IDColumn") : null;
            TargetColumn = idColumnNode != null ? idColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/AliquotIdModifier/IDColumn").InnerXml;
            XmlNode descriptionColumnNode = configuration != null ? configuration.SelectSingleNode("DescriptionColumn") : null;
            string descriptionColumn = descriptionColumnNode != null ? descriptionColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/AliquotIdModifier/DescriptionColumn").InnerXml;
            XmlNode sourceSampleBCColumnNode = configuration != null ? configuration.SelectSingleNode("SourceSampleBCColumn") : null;
            string sourceSampleBCColumn = sourceSampleBCColumnNode != null ? sourceSampleBCColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/AliquotIdModifier/SourceSampleBCColumn").InnerXml;
            XmlNode sourcePlateBCColumnNode = configuration != null ? configuration.SelectSingleNode("SourcePlateBCColumn") : null;
            string sourcePlateBCColumn = sourcePlateBCColumnNode != null ? sourcePlateBCColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/AliquotIdModifier/SourcePlateBCColumn").InnerXml;
            XmlNode containerTypeColumnNode = configuration != null ? configuration.SelectSingleNode("ContainerTypeColumn") : null;
            string containerTypeColumn = containerTypeColumnNode != null ? containerTypeColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/AliquotIdModifier/ContainerTypeColumn").InnerXml;
            XmlNode projectColumnNode = configuration != null ? configuration.SelectSingleNode("ProjectColumn") : null;
            string projectColumn = projectColumnNode != null ? projectColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/AliquotIdModifier/ProjectColumn").InnerXml;
            SourceColumns = new string[] { descriptionColumn, sourceSampleBCColumn, sourcePlateBCColumn, containerTypeColumn, projectColumn };
            XmlNode sqlNode = configuration != null ? configuration.SelectSingleNode("Sql") : null;
            Sql = sqlNode != null ? sqlNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/AliquotIdModifier/Sql").InnerXml;
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("AliquotIdModifier", "DoStuff (" + Project.Name + ")");
            file = base.DoStuff(file);
            DNBLogger.Log("AliquotIdModifier", "DoStuff end (" + Project.Name + ")");
            return file;
        }

        protected override string Modify(List<string> sources)
        {
            string sql = Sql.Replace("##MASTERDESCRIPTION##", sources[0]);
            sql = sql.Replace("##SOURCESAMPLEBC##", sources[1]);
            sql = sql.Replace("##SOURCEPLATEBC##", sources[2]);
            sql = sql.Replace("##CONTAINERTYPE##", sources[3]);
            sql = sql.Replace("##PROJECT##", sources[4]);
            DNBLogger.Log("AliquotIdModifier", "Lookup aliquotId: " + sql);
            List<string> ids = Service.LimsCommunication.SampleGetIdsFromSql(sql);
            if (ids == null)
            {
                DNBLogger.Log("AliquotIdModifier", "Aliquot not found in Nautilus");
                return null;
            }
            else if (ids.Count == 1)
            {
                return ids[0];
            }
            else
            {
                DNBLogger.Log("AliquotIdModifier", "Wrong number of aliquots found (" + ids.Count + ")");
                return null;
            }
        }
    }
}
