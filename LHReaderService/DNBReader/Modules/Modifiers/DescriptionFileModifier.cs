﻿using DNBReader.DataClasses;
using System;
using System.IO;
using System.Linq;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Injects description into data.
    /// Description data is read from description files.
    /// The following substitutions are made in the description pattern:
    /// ##DESCFILENAME## -> Description file name
    /// ##***## -> Value of column *** in the description file
    /// </summary>
    internal class DescriptionFileModifier : Modifier
    {
        public string InputDir;
        public string Pattern;
        public char FileDelimiter;
        public string DataSampleBCColumn;
        public string DescriptionSampleBCColumn;
        public string DescriptionColumn;

        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("DescriptionFileModifier", "Initialize");
            project = Project;
            XmlNode inputDirNode = configuration != null ? configuration.SelectSingleNode("Dir") : null;
            InputDir = inputDirNode != null ? inputDirNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/DescriptionFileModifier/Dir").InnerXml;
            XmlNode patternNode = configuration != null ? configuration.SelectSingleNode("Pattern") : null;
            Pattern = patternNode != null ? patternNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/DescriptionFileModifier/Pattern").InnerXml;
            XmlNode fileDelimiterNode = configuration != null ? configuration.SelectSingleNode("Delimiter") : null;
            FileDelimiter = Convert.ToChar(fileDelimiterNode != null ? fileDelimiterNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/DescriptionFileModifier/Delimiter").InnerXml);
            XmlNode dataSampleBCColumnNode = configuration != null ? configuration.SelectSingleNode("SourceSampleBCColumn") : null;
            DataSampleBCColumn = dataSampleBCColumnNode != null ? dataSampleBCColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/DescriptionFileModifier/SourceSampleBCColumn").InnerXml;
            XmlNode descriptionSampleBCColumnNode = configuration != null ? configuration.SelectSingleNode("DescriptionSampleBCColumn") : null;
            DescriptionSampleBCColumn = descriptionSampleBCColumnNode != null ? descriptionSampleBCColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/DescriptionFileModifier/DescriptionSampleBCColumn").InnerXml;
            XmlNode descriptionColumnNode = configuration != null ? configuration.SelectSingleNode("DescriptionColumn") : null;
            DescriptionColumn = descriptionColumnNode != null ? descriptionColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/DescriptionFileModifier/DescriptionColumn").InnerXml;
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("DescriptionFileModifier", "DoStuff");
            if (file == null)
            {
                DNBLogger.Log("DescriptionFileModifier", "Data is not defined");
                return null;
            }
            string lnr = file.Filename.Substring(file.Filename.IndexOf("C") + 1, file.Filename.IndexOf(".") - (file.Filename.IndexOf("C") + 1));
            DNBLogger.Log("DescriptionFileModifier", "Parsing description file " + lnr + ".csv");
            FileReader reader = new FileReader();
            reader.Dir = InputDir;
            reader.Pattern = lnr + ".csv";
            reader.Delimiter = FileDelimiter;
            reader.Project = Project;
            DataFile descriptionFile = reader.DoStuff(null);
            if(descriptionFile == null)
            {
                DNBLogger.Log("DescriptionFileModifier", "Failed to read " + lnr + ".csv");
                return null;
            }
            DNBLogger.Log("DescriptionFileModifier", "File parsed - substituting");
            file.AddColumn(DescriptionColumn, Pattern);
            foreach (DataGroup plate in file.Groups)
            {
                foreach (DataRow row in plate.DataRows)
                {
                    row.Cells[DescriptionColumn] = row.Cells[DescriptionColumn].Replace("##DESCFILENAME##", descriptionFile.Filename);
                    if (row.Cells[DescriptionColumn].Contains("##"))
                    {
                        DataRow descriptionRow = descriptionFile.Groups[0].DataRows.Where(x => x.Cells[DescriptionSampleBCColumn] == row.Cells[DataSampleBCColumn]).First();
                        while (row.Cells[DescriptionColumn].Contains("##"))
                        {
                            int first = row.Cells[DescriptionColumn].IndexOf("##");
                            int second = row.Cells[DescriptionColumn].IndexOf("##", first + 2);
                            string substitutionColumn = row.Cells[DescriptionColumn].Substring(first + 2, second - first - 2);
                            row.Cells[DescriptionColumn] = row.Cells[DescriptionColumn].Replace("##" + substitutionColumn + "##", descriptionRow.Cells[substitutionColumn]);
                        }
                    }
                }
            }
            DNBLogger.Log("DescriptionFileModifier", "DoStuff end");
            return file;
        }
    }
}
