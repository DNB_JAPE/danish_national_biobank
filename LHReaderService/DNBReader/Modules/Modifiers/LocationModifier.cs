﻿using DNBReader.DataClasses;
using System;
using System.Collections.Generic;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Lookup location name and insert the nautilus id.
    /// </summary>
    internal class LocationModifier : Modifier
    {
        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("LocationModifier", "Initialize");
            Project = project;
            XmlNode sourceNode = configuration != null ? configuration.SelectSingleNode("SourceColumn") : null;
            SourceColumns = new string[] { sourceNode != null ? sourceNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/LocationModifier/SourceColumn").InnerXml };
            XmlNode targetNode = configuration != null ? configuration.SelectSingleNode("TargetColumn") : null;
            TargetColumn = targetNode != null ? targetNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/LocationModifier/TargetColumn").InnerXml;
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("LocationModifier", "DoStuff (" + Project.Name + ")");
            file = base.DoStuff(file);
            DNBLogger.Log("LocationModifier", "DoStuff end (" + Project.Name + ")");
            return file;
        }

        protected override string Modify(List<string> sources)
        {
            DNBLogger.Log("LocationModifier", "Lookup location: " + sources[0]);
            return Service.LimsCommunication.LocationGetId(sources[0]);
        }
    }
}
