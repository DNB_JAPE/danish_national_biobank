﻿using DNBReader.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Empty cells are given the empty string as value.
    /// </summary>
    internal class EmptyCellsModifier : Modifier
    {
        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("EmptyCellsModifier", "Initialize");
            Project = project;
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("EmptyCellsModifier", "DoStuff");
            if (file == null)
            {
                DNBLogger.Log("EmptyCellsModifier", "Data is not defined");
                return null;
            }
            foreach (DataGroup group in file.Groups)
            {
                foreach (DataRow row in group.DataRows)
                {
                    List<string> keys = row.Cells.Keys.ToList();
                    foreach(string key in keys)
                    {
                        if(String.IsNullOrEmpty(row.Cells[key]))
                        {
                            row.Cells[key] = "\"\"";
                        }
                    }
                }
            }
            DNBLogger.Log("EmptyCellsModifier", "DoStuff end");
            return file;
        }
    }
}
