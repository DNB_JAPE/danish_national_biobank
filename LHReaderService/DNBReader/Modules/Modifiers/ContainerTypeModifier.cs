﻿using DNBReader.DataClasses;
using System;
using System.Collections.Generic;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Lookup container type and replace it by the nautilus id.
    /// </summary>
    internal class ContainerTypeModifier : Modifier
    {
        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("ContainerTypeModifier", "Initialize");
            Project = project;
            XmlNode sourceNode = configuration != null ? configuration.SelectSingleNode("SourceColumn") : null;
            SourceColumns = new string[] { sourceNode != null ? sourceNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/ContainerTypeModifier/SourceColumn").InnerXml };
            XmlNode targetNode = configuration != null ? configuration.SelectSingleNode("TargetColumn") : null;
            TargetColumn = targetNode != null ? targetNode.InnerXml : defaultConfiguration.SelectSingleNode("Modifiers/ContainerTypeModifier/TargetColumn").InnerXml;
        }


        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("ContainerTypeModifier", "DoStuff (" + Project.Name + ")");
            file = base.DoStuff(file);
            DNBLogger.Log("ContainerTypeModifier", "DoStuff end (" + Project.Name + ")");
            return file;
        }

        protected override string Modify(List<string> sources)
        {
            DNBLogger.Log("ContainerTypeModifier", "Lookup tube type: " + sources[0]);
            return Service.LimsCommunication.TubeTypeGetId(sources[0]);
        }
    }
}
