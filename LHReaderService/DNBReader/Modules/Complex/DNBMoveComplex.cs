﻿using DNBReader.DataClasses;
using DNBReader.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Complex module for projects previously implemented by PlateFillReader.
    /// </summary>
    internal class DNBMoveComplex : Complex
    {
        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            Project = project;
            DNBLogger.Log("DNBMoveComplex", "Initialize");
            Modules = new List<Module>();
            Modules.Add(new ByColumnGrouper());
            Modules.Last().Initialize(configuration.SelectSingleNode("ByColumnGrouper"), defaultConfiguration, project);
            Modules.Add(new CheckColumnCountFilter());
            Modules.Last().Initialize(configuration.SelectSingleNode("CheckColumnCountFilter"), defaultConfiguration, project);
            Modules.Add(new DNBStandardFileFormatFilter());
            Modules.Last().Initialize(configuration.SelectSingleNode("DNBStandardFileFormatFilter"), defaultConfiguration, project);
            Modules.Add(new RemoveInvalidLineFilter());
            Modules.Last().Initialize(configuration.SelectSingleNode("RemoveInvalidLineFilter"), defaultConfiguration, project);
            Modules.Add(new TargetSampleBCFilter());
            Modules.Last().Initialize(configuration.SelectSingleNode("TargetSampleBCFilter"), defaultConfiguration, project);
            Modules.Add(new VolumeFilter());
            Modules.Last().Initialize(configuration.SelectSingleNode("VolumeFilter"), defaultConfiguration, project);
            Modules.Add(new EmptyCellsModifier());
            Modules.Last().Initialize(configuration.SelectSingleNode("EmptyCellsModifier"), defaultConfiguration, project);
            Modules.Add(new AliquotIdModifier());
            Modules.Last().Initialize(configuration.SelectSingleNode("AliquotIdModifier"), defaultConfiguration, project);
            Modules.Add(new UserNameModifier());
            Modules.Last().Initialize(configuration.SelectSingleNode("UserNameModifier"), defaultConfiguration, project);
            Modules.Add(new ContainerTypeModifier());
            Modules.Last().Initialize(configuration.SelectSingleNode("ContainerTypeModifier"), defaultConfiguration, project);
            Modules.Add(new LocationModifier());
            Modules.Last().Initialize(configuration.SelectSingleNode("LocationModifier"), defaultConfiguration, project);
            Modules.Add(new ActionDateTimeModifier());
            Modules.Last().Initialize(configuration.SelectSingleNode("ActionDateTimeModifier"), defaultConfiguration, project);
            DNBLogger.Log("DNBMoveComplex", "Initialize end");
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("DNBMoveComplex", "DoStuff");
            foreach(Module module in Modules)
            {
                file = module.DoStuff(file);
                if (file == null)
                {
                    return null;
                }
            }
            DNBLogger.Log("DNBMoveComplex", "DoStuff end");
            return file;
        }
    }
}
