﻿namespace DNBReader.Modules
{
    /// <summary>
    /// Readers read data from an outside source - typically a file - and orders the data into a Datafile Object.
    /// Typically, a reader will receive null as input, and return a Datafile object with one group.
    /// 
    /// Readers should only process data enough to be able to put it in a Datafile object.
    /// Further processing should be done by groupers, filters and modifiers.
    /// 
    /// A reader is not necessarily the first module in the module chain.
    ///
    /// Reader names should be postfixed with 'Reader'.
    /// </summary>
    public abstract class Reader : Module
    {
    }
}
