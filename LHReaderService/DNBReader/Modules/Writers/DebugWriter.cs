﻿using DNBReader.DataClasses;
using System;
using System.IO;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Writes the data in its current form without changing it. Used for debugging.
    /// </summary>
    internal class DebugWriter : Writer
    {
        internal string Dir { get; set; }

        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("DebugWriter", "Initialize");
            project = Project;
            XmlNode dirNode = configuration != null ? configuration.SelectSingleNode("Dir") : null;
            Dir = dirNode != null ? dirNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/DebugWriter/Dir").InnerXml;
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("DebugWriter", "DoStuff");
            if (file == null)
            {
                DNBLogger.Log("DebugWriter", "Data is not defined");
                return null;
            }
            DirectoryInfo dir = new DirectoryInfo(Dir);
            if (!dir.Exists)
            {
                DNBLogger.Log("DebugWriter", "Directory does not exist: " + Dir);
                return null;
            }
            try
            {
                string filename = Project.Name + "_Raw_" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff") + ".csv";
                bool first = true;
                using (StreamWriter writer = new StreamWriter(Path.Combine(Dir, filename)))
                {
                    foreach (DataGroup plate in file.Groups)
                    {
                        foreach (DataRow row in plate.DataRows)
                        {
                            if(first)
                            {
                                string headerString = string.Empty;
                                foreach (string cell in row.Cells.Keys)
                                {
                                    headerString = headerString + cell + file.DataDelimiter;
                                }
                                headerString = headerString.TrimEnd(file.DataDelimiter);
                                writer.WriteLine(headerString);
                                first = false;
                            }
                            string rowString = string.Empty;
                            foreach (string cell in row.Cells.Values)
                            {
                                rowString = rowString + cell + file.DataDelimiter;
                            }
                            rowString = rowString.TrimEnd(file.DataDelimiter);
                            writer.WriteLine(rowString);
                        }
                    }
                }
                DNBLogger.Log("DebugWriter", "File written: " + filename);
            }
            catch (Exception ex)
            {
                DNBLogger.Log("DebugWriter", "Failed to write file: " + ex.Message);
                return null;
            }
            DNBLogger.Log("DebugWriter", "DoStuff end");
            return file;
        }
    }
}