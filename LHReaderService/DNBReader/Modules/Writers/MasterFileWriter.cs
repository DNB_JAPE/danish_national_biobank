﻿using DNBReader.DataClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Writes master aliquot files.
    /// </summary>
    internal class MasterFileWriter : Writer
    {
        public string Dir { get; set; }
        public string TmpDir { get; set; }
        public string MasterWorkflow { get; set; }
        public string MasterSampleType { get; set; }
        public int WaitInterval { get; set; }
        public int WaitCount { get; set; }
        public string UserNameColumn { get; set; }
        public string SourceSampleBCColumn { get; set; }
        public string SourcePlateBCColumn { get; set; }
        public string TargetPlateBCColumn { get; set; }
        public string RobotColumn { get; set; }
        public string ProjectColumn { get; set; }
        public string ActionDateTimeColumn { get; set; }
        public string MasterContainerTypeColumn { get; set; }
        public string LocationColumn { get; set; }
        public string DescriptionColumn { get; set; }


        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("MasterFileWriter", "Initialize");
            Project = project;
            XmlNode dirNode = configuration != null ? configuration.SelectSingleNode("Dir") : null;
            Dir = dirNode != null ? dirNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/MasterFileWriter/Dir").InnerXml;
            XmlNode tmpDirNode = configuration != null ? configuration.SelectSingleNode("TmpDir") : null;
            TmpDir = tmpDirNode != null ? tmpDirNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/MasterFileWriter/TmpDir").InnerXml;
            XmlNode masterWorkflowNode = configuration != null ? configuration.SelectSingleNode("MasterWorkflow") : null;
            MasterWorkflow = masterWorkflowNode != null ? masterWorkflowNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/MasterFileWriter/MasterWorkflow").InnerXml;
            XmlNode masterSampleTypeNode = configuration != null ? configuration.SelectSingleNode("MasterSampleType") : null;
            MasterSampleType = masterSampleTypeNode != null ? masterSampleTypeNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/MasterFileWriter/MasterSampleType").InnerXml;
            XmlNode waitIntervalNode = configuration != null ? configuration.SelectSingleNode("WaitInterval") : null;
            WaitInterval = Convert.ToInt32(waitIntervalNode != null ? waitIntervalNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/MasterFileWriter/WaitInterval").InnerXml);
            XmlNode waitCountNode = configuration != null ? configuration.SelectSingleNode("WaitCount") : null;
            WaitCount = Convert.ToInt32(waitCountNode != null ? waitCountNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/MasterFileWriter/WaitCount").InnerXml);
            XmlNode userColumnNode = configuration != null ? configuration.SelectSingleNode("UserNameColumn") : null;
            UserNameColumn = userColumnNode != null ? userColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/MasterFileWriter/UserNameColumn").InnerXml;
            XmlNode sourceSampleBCColumnNode = configuration != null ? configuration.SelectSingleNode("SourceSampleBCColumn") : null;
            SourceSampleBCColumn = sourceSampleBCColumnNode != null ? sourceSampleBCColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/MasterFileWriter/SourceSampleBCColumn").InnerXml;
            XmlNode sourcePlateBCColumnNode = configuration != null ? configuration.SelectSingleNode("SourcePlateBCColumn") : null;
            SourcePlateBCColumn = sourcePlateBCColumnNode != null ? sourcePlateBCColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/MasterFileWriter/SourcePlateBCColumn").InnerXml;
            XmlNode targetPlateBCColumnNode = configuration != null ? configuration.SelectSingleNode("TargetPlateBCColumn") : null;
            TargetPlateBCColumn = targetPlateBCColumnNode != null ? targetPlateBCColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/MasterFileWriter/TargetPlateBCColumn").InnerXml;
            XmlNode robotColumnNode = configuration != null ? configuration.SelectSingleNode("InstrumentColumn") : null;
            RobotColumn = robotColumnNode != null ? robotColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/MasterFileWriter/InstrumentColumn").InnerXml;
            XmlNode projectColumnNode = configuration != null ? configuration.SelectSingleNode("ProjectColumn") : null;
            ProjectColumn = projectColumnNode != null ? projectColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/MasterFileWriter/ProjectColumn").InnerXml;
            XmlNode actionDateTimeColumnNode = configuration != null ? configuration.SelectSingleNode("ActionDateTimeColumn") : null;
            ActionDateTimeColumn = actionDateTimeColumnNode != null ? actionDateTimeColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/MasterFileWriter/ActionDateTimeColumn").InnerXml;
            XmlNode containerTypeColumnNode = configuration != null ? configuration.SelectSingleNode("MasterContainerTypeColumn") : null;
            MasterContainerTypeColumn = containerTypeColumnNode != null ? containerTypeColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/MasterFileWriter/MasterContainerTypeColumn").InnerXml;
            XmlNode locationColumnNode = configuration != null ? configuration.SelectSingleNode("LocationColumn") : null;
            LocationColumn = locationColumnNode != null ? locationColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/MasterFileWriter/LocationColumn").InnerXml;
            XmlNode descriptionColumnNode = configuration != null ? configuration.SelectSingleNode("DescriptionColumn") : null;
            DescriptionColumn = descriptionColumnNode != null ? descriptionColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/MasterFileWriter/DescriptionColumn").InnerXml;
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("MasterFileWriter", "DoStuff");
            if (file == null)
            {
                DNBLogger.Log("MasterFileWriter", "Data is not defined");
                return null;
            }
            if (file.Groups.Count == 0)
            {
                DNBLogger.Log("MasterFileWriter", "No plates in data file: " + file.Filename);
                return file;
            }
            foreach(DataGroup plate in file.Groups)
            {
                if(plate.DataRows.Count == 0)
                {
                    DNBLogger.Log("MasterFileWriter", "No rows in plate");
                    continue;
                }
                string plateBC = plate.DataRows[0].Cells[SourcePlateBCColumn];
                if(plateBC.Contains("DW"))
                {
                    DNBLogger.Log("MasterFileWriter", "No masters are created for Deepwell Plates: " + plateBC);
                    continue;
                }
                DNBLogger.Log("MasterFileWriter", "Writing master file for plate: " + plateBC);
                string masterFileName = file.Filename.Substring(0, file.Filename.Length - 4) + "_(" + plate.DataRows[0].Cells[TargetPlateBCColumn] + ").csv";
                FileInfo tmpMasterAliquotFile = new FileInfo(Path.Combine(TmpDir, masterFileName));
                using (StreamWriter writer = new StreamWriter(tmpMasterAliquotFile.OpenWrite()))
                {
                    writer.WriteLine("Begin Aliquot");
                    writer.WriteLine("\"External_Ref\",\"Workflow_Name\",\"Sample_Ref\",\"Description\",\"U_Log_Robot\",\"U_Projectnr\",\"Container_Type_Id\",\"Matrix_Type\",\"Operator_Id\",\"U_ActionDateTime\",\"Location_Id\"");
                    List<string> writtenMasters = new List<string>();
                    foreach (DataRow row in plate.DataRows)
                    {
                        if (row.Cells["CreateMaster"] == "true")
                        {
                            string sourceSampleBC = row.Cells[SourceSampleBCColumn];
                            if (writtenMasters.Contains(sourceSampleBC))
                            {
                                DNBLogger.Log("MasterFileWriter", "Master is duplicate in plate - ignored");
                                continue;
                            }
                            else
                            {
                                writtenMasters.Add(sourceSampleBC);
                                writer.WriteLine("\"" + sourceSampleBC +
                                    "\",\"" + MasterWorkflow +
                                    "\",\"" +
                                    "\",\"" + row.Cells[DescriptionColumn] +
                                    "\",\"" + row.Cells[RobotColumn] +
                                    "\",\"" + row.Cells[ProjectColumn] +
                                    "\",\"" + row.Cells[MasterContainerTypeColumn] +
                                    "\",\"" + MasterSampleType +
                                    "\",\"" + row.Cells[UserNameColumn] +
                                    "\",\"" + row.Cells[ActionDateTimeColumn] +
                                    "\",\"" + row.Cells[LocationColumn] + "\"");
                            }
                        }
                    }
                    writer.WriteLine("End Aliquot");
                }
                DNBLogger.Log("MasterFileWriter", "File written");
                FileInfo masterFile = new FileInfo(Path.Combine(Dir, masterFileName));
                tmpMasterAliquotFile.MoveTo(masterFile.FullName);
                DNBLogger.Log("MasterFileWriter", "Wait for Nautilus");
                int waited = 0;
                while (waited < WaitCount)
                {
                    Thread.Sleep(WaitInterval);
                    waited++;
                    masterFile.Refresh();
                    if (!masterFile.Exists)
                    {
                        DNBLogger.Log("MasterFileWriter", "File is removed by instrument: " + masterFile.Name);
                        break;
                    }
                }
                masterFile.Refresh();
                if (masterFile.Exists)
                {
                    DNBLogger.Log("MasterFileWriter", "Done waiting, but file is still present: " + masterFile.Name);
                    return null;
                }
            }
            DNBLogger.Log("MasterFileWriter", "Done");
            return file;
        }
    }
}
