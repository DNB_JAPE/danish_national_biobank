﻿using DNBReader.DataClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Writes Build Plate files
    /// </summary>
    internal class BuildPlateFileWriter : Writer
    {
        public string TargetPlateBCColumn { get; set; }
        public string TargetSampleBCColumn { get; set; }
        public string SourceSampleBCColumn { get; set; }
        public string SourcePlateBCColumn { get; set; }
        public string MaterialTypeColumn { get; set; }
        public string DescriptionColumn { get; set; }
        public string PositionColumn { get; set; }
        public int WaitInterval { get; set; }
        public int WaitCount { get; set; }
        public string DirExisting { get; set; }
        public string DirNonexisting { get; set; }
        public string TmpDir { get; set; }
        public string LookupDeepwellMasterSql { get; set; }
        public string MasterWorkflow { get; set; }

        public Dictionary<string, int> RowNumber = new Dictionary<string, int>()
        {{"A",1},
         {"B",2},
         {"C",3},
         {"D",4},
         {"E",5},
         {"F",6},
         {"G",7},
         {"H",8}};

        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("BuildPlateFileWriter", "Initialize");
            project = Project;
            XmlNode dirExistingNode = configuration != null ? configuration.SelectSingleNode("DirExisting") : null;
            DirExisting = dirExistingNode != null ? dirExistingNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/BuildPlateFileWriter/DirExisting").InnerXml;
            XmlNode dirNonexistingNode = configuration != null ? configuration.SelectSingleNode("DirNonexisting") : null;
            DirNonexisting = dirNonexistingNode != null ? dirNonexistingNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/BuildPlateFileWriter/DirNonexisting").InnerXml;
            XmlNode tmpDirNode = configuration != null ? configuration.SelectSingleNode("TmpDir") : null;
            TmpDir = tmpDirNode != null ? tmpDirNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/BuildPlateFileWriter/TmpDir").InnerXml;
            XmlNode lookupDeepwellMasterSqlNode = configuration != null ? configuration.SelectSingleNode("LookupDeepwellMasterSql") : null;
            LookupDeepwellMasterSql = lookupDeepwellMasterSqlNode != null ? lookupDeepwellMasterSqlNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/BuildPlateFileWriter/LookupDeepwellMasterSql").InnerXml;
            XmlNode waitIntervalNode = configuration != null ? configuration.SelectSingleNode("WaitInterval") : null;
            WaitInterval = Convert.ToInt32(waitIntervalNode != null ? waitIntervalNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/BuildPlateFileWriter/WaitInterval").InnerXml);
            XmlNode waitCountNode = configuration != null ? configuration.SelectSingleNode("WaitCount") : null;
            WaitCount = Convert.ToInt32(waitCountNode != null ? waitCountNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/BuildPlateFileWriter/WaitCount").InnerXml);
            XmlNode masterWorkflowNode = configuration != null ? configuration.SelectSingleNode("MasterWorkflow") : null;
            MasterWorkflow = masterWorkflowNode != null ? masterWorkflowNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/BuildPlateFileWriter/MasterWorkflow").InnerXml;
            XmlNode targetPlateBCColumnNode = configuration != null ? configuration.SelectSingleNode("TargetPlateBCColumn") : null;
            TargetPlateBCColumn = targetPlateBCColumnNode != null ? targetPlateBCColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/BuildPlateFileWriter/TargetPlateBCColumn").InnerXml;
            XmlNode targetSampleBCColumnNode = configuration != null ? configuration.SelectSingleNode("TargetSampleBCColumn") : null;
            TargetSampleBCColumn = targetSampleBCColumnNode != null ? targetSampleBCColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/BuildPlateFileWriter/TargetSampleBCColumn").InnerXml;
            XmlNode sourceSampleBCColumnNode = configuration != null ? configuration.SelectSingleNode("SourceSampleBCColumn") : null;
            SourceSampleBCColumn = sourceSampleBCColumnNode != null ? sourceSampleBCColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/BuildPlateFileWriter/SourceSampleBCColumn").InnerXml;
            XmlNode sourcePlateBCColumnNode = configuration != null ? configuration.SelectSingleNode("SourcePlateBCColumn") : null;
            SourcePlateBCColumn = sourcePlateBCColumnNode != null ? sourcePlateBCColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/BuildPlateFileWriter/SourcePlateBCColumn").InnerXml;
            XmlNode materialTypeColumnNode = configuration != null ? configuration.SelectSingleNode("MaterialTypeColumn") : null;
            MaterialTypeColumn = materialTypeColumnNode != null ? materialTypeColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/BuildPlateFileWriter/MaterialTypeColumn").InnerXml;
            XmlNode descriptionColumnNode = configuration != null ? configuration.SelectSingleNode("DescriptionColumn") : null;
            DescriptionColumn = descriptionColumnNode != null ? descriptionColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/BuildPlateFileWriter/DescriptionColumn").InnerXml;
            XmlNode positionColumnNode = configuration != null ? configuration.SelectSingleNode("PositionColumn") : null;
            PositionColumn = positionColumnNode != null ? positionColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/BuildPlateFileWriter/PositionColumn").InnerXml;
        }

        public override DataFile DoStuff(DataClasses.DataFile file)
        {
            DNBLogger.Log("BuildPlateFileWriter", "DoStuff");
            if (file == null)
            {
                DNBLogger.Log("BuildPlateFileWriter", "Data is not defined");
                return null;
            }
            if (file.Groups.Count == 0)
            {
                DNBLogger.Log("BuildPlateFileWriter", "No plates in data file: " + file.Filename);
                return file;
            }
            foreach (DataGroup plate in file.Groups)
            {
                DNBLogger.Log("BuildPlateFileWriter", "Processing plate");
                List<DataRow> normalRows = new List<DataRow>();
                List<DataRow> deepwellRows = new List<DataRow>();
                foreach(DataRow row in plate.DataRows)
                {
                    string sql = LookupDeepwellMasterSql.Replace("##SOURCEPLATEBC##", row.Cells[SourcePlateBCColumn]);
                    sql = sql.Replace("##SOURCESAMPLEBC##", row.Cells[SourceSampleBCColumn]);
                    if (row.Cells[SourcePlateBCColumn].Contains("DW") && row.Cells[MaterialTypeColumn].Equals("DNA") && Service.LimsCommunication.SampleGetIdsFromSql(sql).Count()==0)
                    {
                        deepwellRows.Add(row);
                    }
                    else
                    {
                        normalRows.Add(row);
                    }
                }
                DNBLogger.Log("BuildPlateFileWriter", "Samples divided: " + normalRows.Count() + " normal and " + deepwellRows.Count() + " parentless DW");
                WriteNormalSamples(normalRows, file.Filename, plate.File.DataDelimiter);
                WriteDWSamples(deepwellRows);
            }
            DNBLogger.Log("BuildPlateFileWriter", "DoStuff end");
            return file;
        }

        private void WriteNormalSamples(List<DataRow> rows, string file, char headerDelimiter)
        {
            if (rows.Count > 0)
            {
                DNBLogger.Log("BuildPlateFileWriter", "Writing build plate file for " + rows[0].Cells[TargetPlateBCColumn]);
                string fileName = file.Substring(0, file.Length - 4) + "_(" + rows[0].Cells[TargetPlateBCColumn] + ").csv";
                FileInfo tmpFile = new FileInfo(Path.Combine(TmpDir, fileName));
                using (StreamWriter writer = new StreamWriter(tmpFile.OpenWrite()))
                {
                    writer.WriteLine("RecordId" + headerDelimiter +
                                     "TRackBC" + headerDelimiter +
                                     "Tube_Type" + headerDelimiter +
                                     "TPositionId" + headerDelimiter +
                                     "TPositionBC" + headerDelimiter +
                                     "TStatusSummary" + headerDelimiter +
                                     "TSumStateDescription" + headerDelimiter +
                                     "TVolume" + headerDelimiter +
                                     "Sample_Type" + headerDelimiter +
                                     "SPositionId" + headerDelimiter +
                                     "SPositionBC" + headerDelimiter +
                                     "ActionDateTime" + headerDelimiter +
                                     "UserName" + headerDelimiter +
                                     "Instrument" + headerDelimiter +
                                     "Project" + headerDelimiter +
                                     "UserComment" + headerDelimiter +
                                     "MasterContainer" + headerDelimiter +
                                     "LocationId" + headerDelimiter +
                                     "Description");
                    foreach(DataRow row in rows)
                    {
                        writer.WriteLine(row.Cells["RecordId"] + headerDelimiter +
                                         row.Cells["TRackBC"] + headerDelimiter +
                                         row.Cells["Tube_Type"] + headerDelimiter +
                                         row.Cells["TPositionId"] + headerDelimiter +
                                         row.Cells["TPositionBC"] + headerDelimiter +
                                         row.Cells["TStatusSummary"] + headerDelimiter +
                                         row.Cells["TSumStateDescription"] + headerDelimiter +
                                         row.Cells["TVolume"] + headerDelimiter +
                                         row.Cells["SRackBC"] + headerDelimiter +
                                         row.Cells["Sample_Type"] + headerDelimiter +
                                         row.Cells["SPositionId"] + headerDelimiter +
                                         row.Cells["SPositionBC"] + headerDelimiter +
                                         row.Cells["ActionDateTime"] + headerDelimiter +
                                         row.Cells["UserNameId"] + headerDelimiter +
                                         row.Cells["Instrument"] + headerDelimiter +
                                         row.Cells["Project"] + headerDelimiter +
                                         (row.Cells.ContainsKey("UserComment") ? row.Cells["UserComment"] : "") + headerDelimiter +
                                         (row.Cells.ContainsKey("MasterContainer") ? row.Cells["MasterContainer"] : "") + headerDelimiter +
                                         row.Cells["LocationId"] + headerDelimiter +
                                         row.Cells["Description"] + headerDelimiter); 
                    }
                }
                DNBLogger.Log("BuildPlateFileWriter", "File written");
                FileInfo buildPlateFile;
                if (Service.LimsCommunication.PlateExist(rows[0].Cells[TargetPlateBCColumn]))
                {
                    buildPlateFile = new FileInfo(Path.Combine(DirExisting, fileName));
                }
                else
                {
                    buildPlateFile = new FileInfo(Path.Combine(DirNonexisting, fileName));
                }
                tmpFile.MoveTo(buildPlateFile.FullName);
                DNBLogger.Log("BuildPlateFileWriter", "Wait for Nautilus");
                int waited = 0;
                while (waited < WaitCount)
                {
                    Thread.Sleep(WaitInterval);
                    waited++;
                    buildPlateFile.Refresh();
                    if (!buildPlateFile.Exists)
                    {
                        DNBLogger.Log("BuildPlateFileWriter", "File is removed by instrument: " + buildPlateFile.Name);
                        return;
                    }
                }
                DNBLogger.Log("BuildPlateFileWriter", "Done waiting, but file is still present: " + buildPlateFile.Name);
                throw new Exception("BuildPlateFileWriter: Done waiting, but file is still present " + buildPlateFile.Name);
            }
        }

        private void WriteDWSamples(List<DataRow> rows)
        {
            if (rows.Count > 0)
            {
                DNBLogger.Log("BuildPlateFileWriter", "Writing 'Begin Aliquot' file for " + rows[0].Cells[TargetPlateBCColumn]);
                if (Service.LimsCommunication.PlateExist(rows[0].Cells[TargetPlateBCColumn]))
                {
                    DNBLogger.Log("BuildPlateFileWriter", "Plate already exist");
                    throw new Exception("BuildPlateFileWriter: Plate already exist");
                }
                foreach (DataRow row in rows)
                {
                    string position = row.Cells[PositionColumn];
                    string targetSampleBC = row.Cells[TargetSampleBCColumn];
                    string fileName = targetSampleBC + "_" + position + ".csv";
                    FileInfo tmpFile = new FileInfo(Path.Combine(TmpDir, fileName));
                    string plateId = Service.LimsCommunication.PlateGetId(row.Cells[TargetPlateBCColumn]);
                    int plateRow = RowNumber[position.Substring(0, 1)];
                    int plateColumn = Int32.Parse(position.Substring(1, position.Count() - 1));
                    int plateOrder = (plateRow - 1) * 8 + plateColumn;
                    using (StreamWriter writer = new StreamWriter(tmpFile.OpenWrite()))
                    {
                        writer.WriteLine("Begin Aliquot");
                        writer.WriteLine("\"External_Ref\",\"Workflow_Name\",\"Sample_Ref\",\"Description\",\"Matrix_Type\",\"Plate_Id\",\"Plate_Order\",\"Plate_Column\",\"Plate_Row\"");
                        writer.WriteLine("\"" + targetSampleBC + "\",\"" + MasterWorkflow + "\",,\"" + row.Cells[DescriptionColumn] + "\",\"NoDNA\",\"" + plateId + "\",\"" + plateOrder + "\",\"" + plateColumn + "\",\"" + plateRow.ToString() + "\"");
                        writer.WriteLine("End Aliquot");
                    }
                    DNBLogger.Log("BuildPlateFileWriter", "File written");
                    FileInfo aliquotFile;
                    if (Service.LimsCommunication.PlateExist(rows[0].Cells[TargetPlateBCColumn]))
                    {
                        aliquotFile = new FileInfo(Path.Combine(DirExisting, fileName));
                    }
                    else
                    {
                        aliquotFile = new FileInfo(Path.Combine(DirNonexisting, fileName));
                    }
                    tmpFile.MoveTo(aliquotFile.FullName);
                    DNBLogger.Log("BuildPlateFileWriter", "Wait for Nautilus");
                    int waited = 0;
                    while (waited < WaitCount)
                    {
                        Thread.Sleep(WaitInterval);
                        waited++;
                        aliquotFile.Refresh();
                        if (!aliquotFile.Exists)
                        {
                            DNBLogger.Log("BuildPlateFileWriter", "File is removed by instrument: " + aliquotFile.Name);
                            break;
                        }
                    }
                    aliquotFile.Refresh();
                    if (aliquotFile.Exists)
                    {
                        DNBLogger.Log("BuildPlateFileWriter", "Done waiting, but file is still present: " + aliquotFile.Name);
                        throw new Exception("BuildPlateFileWriter: Done waiting, but file is still present " + aliquotFile.Name);
                    }
                }
            }
        }
    }
}
