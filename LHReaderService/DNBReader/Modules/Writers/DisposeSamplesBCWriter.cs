﻿using DNBReader.DataClasses;
using System;
using System.Collections.Generic;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Disposes samples listed by barcode.
    /// </summary>
    internal class DisposeSamplesBCWriter : Writer
    {
        public string SampleBcColumn;
        public string UserNameColumn;
        public string ActionDateTimeColumn;
        public string CommentColumn;
        public string LookupIdSql;

        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("DisposeSamplesBCWriter", "Initialize");
            project = Project;
            XmlNode sampleBcColumnNode = configuration != null ? configuration.SelectSingleNode("SampleBCColumn") : null;
            SampleBcColumn = sampleBcColumnNode != null ? sampleBcColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/DisposeSamplesBCWriter/SampleBCColumn").InnerXml;
            XmlNode userColumnNode = configuration != null ? configuration.SelectSingleNode("UserNameColumn") : null;
            UserNameColumn = userColumnNode != null ? userColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/DisposeSamplesBCWriter/UserNameColumn").InnerXml;
            XmlNode actionDateTimeColumnNode = configuration != null ? configuration.SelectSingleNode("ActionDateTimeColumn") : null;
            ActionDateTimeColumn = actionDateTimeColumnNode != null ? actionDateTimeColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/DisposeSamplesBCWriter/ActionDateTimeColumn").InnerXml;
            XmlNode commentColumnNode = configuration != null ? configuration.SelectSingleNode("CommentColumn") : null;
            CommentColumn = commentColumnNode != null ? commentColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/DisposeSamplesBCWriter/CommentColumn").InnerXml;
            XmlNode lookupIdSqlNode = configuration != null ? configuration.SelectSingleNode("Sql") : null;
            LookupIdSql = lookupIdSqlNode != null ? lookupIdSqlNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/DisposeSamplesBCWriter/Sql").InnerXml;
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("DisposeSamplesBCWriter", "DoStuff");
            if (file == null)
            {
                DNBLogger.Log("DisposeSamplesBCWriter", "Data is not defined");
                return null;
            }
            foreach (DataGroup group in file.Groups)
            {
                if (group.DataRows.Count > 0)
                {
                    foreach (DataRow row in group.DataRows)
                    {
                        string sql = LookupIdSql.Replace("##BC##", row.Cells[SampleBcColumn]);
                        DNBLogger.Log("DisposeSamplesBCWriter", "Lookup sql: " + sql);
                        List<string> ids = Service.LimsCommunication.SampleGetIdsFromSql(sql);
                        if (ids== null || ids.Count != 1)
                        {
                            DNBLogger.Log("DisposeSamplesBCWriter", "Invalid number of ids found (" + (ids == null ? "null" : ids.Count.ToString()) + ")");
                            return null;
                        } 
                        else
                        {
                            DNBLogger.Log("DisposeSamplesBCWriter", "Disposing sample: " + ids[0]);
                            Service.LimsCommunication.SampleDispose(ids[0], row.Cells[UserNameColumn], row.Cells[ActionDateTimeColumn], row.Cells[CommentColumn], true);
                        }
                    }
                }
            }
            DNBLogger.Log("DisposeSamplesBCWriter", "DoStuff end");
            return file;
        }
    }
}
