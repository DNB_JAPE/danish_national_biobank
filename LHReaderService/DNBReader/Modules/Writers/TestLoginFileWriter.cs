﻿using DNBReader.DataClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Writes Test Login files (For DNA)
    /// </summary>
    internal class TestLoginFileWriter : Writer
    {
        public string TargetPlateBCColumn { get; set; }
        public string SourceSampleBCColumn { get; set; }
        public string SourcePlateBCColumn { get; set; }
        public string DescriptionColumn { get; set; }
        public int WaitInterval { get; set; }
        public int WaitCount { get; set; }
        public string Dir { get; set; }
        public string TmpDir { get; set; }
        public string LookupMasterSql { get; set; }
        public string TestWorkflow { get; set; }

        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("TestLoginFileWriter", "Initialize");
            Project = project;
            XmlNode dirNode = configuration != null ? configuration.SelectSingleNode("Dir") : null;
            Dir = dirNode != null ? dirNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/TestLoginFileWriter/Dir").InnerXml;
            XmlNode tmpDirNode = configuration != null ? configuration.SelectSingleNode("TmpDir") : null;
            TmpDir = tmpDirNode != null ? tmpDirNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/TestLoginFileWriter/TmpDir").InnerXml;
            XmlNode lookupMasterSqlNode = configuration != null ? configuration.SelectSingleNode("LookupMasterSql") : null;
            LookupMasterSql = lookupMasterSqlNode != null ? lookupMasterSqlNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/TestLoginFileWriter/LookupMasterSql").InnerXml;
            XmlNode waitIntervalNode = configuration != null ? configuration.SelectSingleNode("WaitInterval") : null;
            WaitInterval = Convert.ToInt32(waitIntervalNode != null ? waitIntervalNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/TestLoginFileWriter/WaitInterval").InnerXml);
            XmlNode waitCountNode = configuration != null ? configuration.SelectSingleNode("WaitCount") : null;
            WaitCount = Convert.ToInt32(waitCountNode != null ? waitCountNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/TestLoginFileWriter/WaitCount").InnerXml);
            XmlNode testWorkflowNode = configuration != null ? configuration.SelectSingleNode("TestWorkflow") : null;
            TestWorkflow = testWorkflowNode != null ? testWorkflowNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/TestLoginFileWriter/TestWorkflow").InnerXml;
            XmlNode targetPlateBCColumnNode = configuration != null ? configuration.SelectSingleNode("TargetPlateBCColumn") : null;
            TargetPlateBCColumn = targetPlateBCColumnNode != null ? targetPlateBCColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/TestLoginFileWriter/TargetPlateBCColumn").InnerXml;
            XmlNode sourceSampleBCColumnNode = configuration != null ? configuration.SelectSingleNode("SourceSampleBCColumn") : null;
            SourceSampleBCColumn = sourceSampleBCColumnNode != null ? sourceSampleBCColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/TestLoginFileWriter/SourceSampleBCColumn").InnerXml;
            XmlNode sourcePlateBCColumnNode = configuration != null ? configuration.SelectSingleNode("SourcePlateBCColumn") : null;
            SourcePlateBCColumn = sourcePlateBCColumnNode != null ? sourcePlateBCColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/TestLoginFileWriter/SourcePlateBCColumn").InnerXml;
            XmlNode descriptionColumnNode = configuration != null ? configuration.SelectSingleNode("DescriptionColumn") : null;
            DescriptionColumn = descriptionColumnNode != null ? descriptionColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/TestLoginFileWriter/DescriptionColumn").InnerXml;
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("TestLoginFileWriter", "DoStuff");
            if (file == null)
            {
                DNBLogger.Log("TestLoginFileWriter", "Data is not defined");
                return null;
            }
            if (file.Groups.Count == 0)
            {
                DNBLogger.Log("TestLoginFileWriter", "No plates in data file: " + file.Filename);
                return file;
            }
            foreach (DataGroup plate in file.Groups)
            {
                DNBLogger.Log("TestLoginFileWriter", "Processing plate");
                if(plate.DataRows.Count > 0)
                {
                    StringBuilder a = new StringBuilder();
                    a.AppendLine("Begin Aliquot");
                    a.AppendLine("\"External Ref\",\"Aliquot Id\",\"Sample Ref\",\"Description\"");
                    StringBuilder t = new StringBuilder();
                    t.AppendLine("Begin Test");
                    t.AppendLine("\"External Ref\",\"Workflow Name\",\"Aliquot Id\",\"Description\"");
                    foreach (DataRow row in plate.DataRows)
                    {
                        // Replace aliquot ref with aliquot id
                        string aliquotId = "";
                        if (row.Cells[SourceSampleBCColumn].StartsWith("ALIQUOT_ID:"))
                        {
                            DNBLogger.Log("TestLoginFileWriter", "Using aliquot id from data file: " + row.Cells[SourceSampleBCColumn].Substring(11));
                            aliquotId = row.Cells[SourceSampleBCColumn].Substring(11);
                        }
                        else
                        {
                            string sql = LookupMasterSql.Replace("##MASTERDESCRIPTION##", row.Cells[DescriptionColumn]);
                            sql = sql.Replace("##SOURCEALIQUOTBC##", row.Cells[SourceSampleBCColumn]);
                            sql = sql.Replace("##SOURCEPLATEBC##", row.Cells[SourcePlateBCColumn]);
                            DNBLogger.Log("TestLoginFileWriter", "Lookup aliquot id: " + sql);
                            List<string> ids = Service.LimsCommunication.SampleGetIdsFromSql(sql);
                            if (ids == null)
                            {
                                DNBLogger.Log("TestLoginFileWriter", "Failed to create aliquot file: Failed to look up aliquot id: " + sql);
                                return null;
                            }
                            if (ids.Count == 1)
                            {
                                aliquotId = ids[0];
                            }
                            else
                            {
                                DNBLogger.Log("TestLoginFileWriter", "Wrong number of aliquots found (" + ids.Count + ")");
                                return null;
                            }
                        }
                        a.AppendLine(",\"" + aliquotId + "\",,");
                        t.AppendLine(",\"" + TestWorkflow + "\",\"" + aliquotId + "\",");
                    }
                    a.AppendLine("End Aliquot");
                    t.AppendLine("End Test");
                    string fileName = file.Filename.Substring(0, file.Filename.Length - 4) + "_(" + plate.DataRows[0].Cells[TargetPlateBCColumn] + ").csv";
                    FileInfo tmpFile = new FileInfo(Path.Combine(TmpDir, fileName));
                    using (StreamWriter writer = new StreamWriter(tmpFile.OpenWrite()))
                    {
                        writer.Write(a.ToString());
                        writer.WriteLine();
                        writer.Write(t.ToString());
                    }
                    DNBLogger.Log("TestLoginFileWriter", "File written");
                    FileInfo testFile = new FileInfo(Path.Combine(Dir, fileName));
                    tmpFile.MoveTo(testFile.FullName);
                    DNBLogger.Log("TestLoginFileWriter", "Wait for Nautilus");
                    int waited = 0;
                    while (waited < WaitCount)
                    {
                        Thread.Sleep(WaitInterval);
                        waited++;
                        testFile.Refresh();
                        if (!testFile.Exists)
                        {
                            DNBLogger.Log("TestLoginFileWriter", "File is removed by instrument: " + testFile.Name);
                            break;
                        }
                    }
                    testFile.Refresh();
                    if (testFile.Exists)
                    {
                        DNBLogger.Log("TestLoginFileWriter", "Done waiting, but file is still present: " + testFile.Name);
                        return null;
                    }
                }
            }
            DNBLogger.Log("TestLoginFileWriter", "Done");
            return file;
        }
    }
}
