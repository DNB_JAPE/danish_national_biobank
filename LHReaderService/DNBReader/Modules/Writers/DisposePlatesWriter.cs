﻿using DNBReader.DataClasses;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Disposes all plates represented by their barcode in the file.
    /// </summary>
    internal class DisposePlatesWriter : Writer
    {
        public string PlateBCColumn { get; set; }
        public string UserNameColumn { get; set; }
        public string ActionDateTimeColumn { get; set; }
        public string CommentColumn { get; set; }

        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("DisposePlatesWriter", "Initialize");
            project = Project;
            XmlNode plateBcColumnNode = configuration != null ? configuration.SelectSingleNode("PlateBCColumn") : null;
            PlateBCColumn = plateBcColumnNode != null ? plateBcColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/DisposePlatesWriter/PlateBCColumn").InnerXml;
            XmlNode userColumnNode = configuration != null ? configuration.SelectSingleNode("UserNameColumn") : null;
            UserNameColumn = userColumnNode != null ? userColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/DisposePlatesWriter/UserNameColumn").InnerXml;
            XmlNode actionDateTimeColumnNode = configuration != null ? configuration.SelectSingleNode("ActionDateTimeColumn") : null;
            ActionDateTimeColumn = actionDateTimeColumnNode != null ? actionDateTimeColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/DisposePlatesWriter/ActionDateTimeColumn").InnerXml;
            XmlNode commentColumnNode = configuration != null ? configuration.SelectSingleNode("CommentColumn") : null;
            CommentColumn = commentColumnNode != null ? commentColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/DisposePlatesWriter/CommentColumn").InnerXml;
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("DisposePlatesWriter", "DoStuff");
            if (file == null)
            {
                DNBLogger.Log("DisposePlatesWriter", "Data is not defined");
                return null;
            }
            foreach (DataGroup group in file.Groups)
            {
                foreach (DataRow row in group.DataRows)
                {
                    DNBLogger.Log("DisposePlatesWriter", "Disposing plate " + row.Cells[PlateBCColumn]);
                    Service.LimsCommunication.PlateDispose(row.Cells[PlateBCColumn], row.Cells[UserNameColumn], row.Cells[ActionDateTimeColumn], row.Cells[CommentColumn], true);
                }
            }
            DNBLogger.Log("DisposePlatesWriter", "DoStuff end");
            return file;
        }
    }
}
