﻿using DNBReader.DataClasses;
using System;
using System.Collections.Generic;
using System.Xml;

namespace DNBReader.Modules
{
    /// <summary>
    /// Disposes samples listed by id.
    /// </summary>
    internal class DisposeSamplesIDWriter : Writer
    {
        public string SampleIdColumn { get; set; }
        public string UserNameColumn { get; set; }
        public string ActionDateTimeColumn { get; set; }
        public string CommentColumn { get; set; }

        public override void Initialize(XmlNode configuration, XmlNode defaultConfiguration, Project project)
        {
            DNBLogger.Log("DisposeSamplesIDWriter", "Initialize");
            project = Project;
            XmlNode sampleIdColumnNode = configuration != null ? configuration.SelectSingleNode("SampleIDColumn") : null;
            SampleIdColumn = sampleIdColumnNode != null ? sampleIdColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/DisposeSamplesIDWriter/SampleIDColumn").InnerXml;
            XmlNode userColumnNode = configuration != null ? configuration.SelectSingleNode("UserNameColumn") : null;
            UserNameColumn = userColumnNode != null ? userColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/DisposeSamplesIDWriter/UserNameColumn").InnerXml;
            XmlNode actionDateTimeColumnNode = configuration != null ? configuration.SelectSingleNode("ActionDateTimeColumn") : null;
            ActionDateTimeColumn = actionDateTimeColumnNode != null ? actionDateTimeColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/DisposeSamplesIDWriter/ActionDateTimeColumn").InnerXml;
            XmlNode commentColumnNode = configuration != null ? configuration.SelectSingleNode("CommentColumn") : null;
            CommentColumn = commentColumnNode != null ? commentColumnNode.InnerXml : defaultConfiguration.SelectSingleNode("Writers/DisposeSamplesIDWriter/CommentColumn").InnerXml;
        }

        public override DataFile DoStuff(DataFile file)
        {
            DNBLogger.Log("DisposeSamplesIDWriter", "DoStuff");
            if (file == null)
            {
                DNBLogger.Log("DisposeSamplesIDWriter", "Data is not defined");
                return null;
            }
            foreach (DataGroup group in file.Groups)
            {
                if (group.DataRows.Count > 0)
                {
                    foreach (DataRow row in group.DataRows)
                    {
                        DNBLogger.Log("DisposeSamplesIDWriter", "Disposing sample: " + row.Cells[SampleIdColumn]);
                        Service.LimsCommunication.SampleDispose(row.Cells[SampleIdColumn], row.Cells[UserNameColumn], row.Cells[ActionDateTimeColumn], row.Cells[CommentColumn], true);
                    }
                }
            }
            DNBLogger.Log("DisposeSamplesIDWriter", "DoStuff end");
            return file;
        }
    }
}
