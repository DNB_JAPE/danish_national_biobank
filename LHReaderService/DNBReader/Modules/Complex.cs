﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNBReader.Modules
{
    /// <summary>
    /// A complex module represents a commonly used group of modules.
    /// It can be seen as a shorthand for writing multiple steps.
    /// 
    /// Calling DoStuff on a complex module, will trigger the submodules one after the other.
    /// 
    /// Ideally, the submodules require only deafult values for initialization, but this is not mandatory.
    /// Implementation of complex modules, can handle nondefault submodule configuration wtihout breaking the design.
    /// 
    /// Complex module names should be postfixed with 'Complex'.
    /// </summary>
    public abstract class Complex : Module
    {
        public List<Module> Modules { get; set; }
    }
}
