﻿using DNBReader.DataClasses;
using System.Collections.Generic;
using System.Xml;

namespace DNBReader.Modules
{
    internal class DNBStandardFileFormatParser : IModule
    {
        private List<IModule> _modules = new List<IModule>();

        public void Initialize(XmlNode configuration, XmlNode defaultConfiguration)
        {
            FileReader fileReader = new FileReader();
            fileReader.Initialize(configuration.SelectSingleNode("FileReader"), defaultConfiguration);
            _modules.Add(fileReader);
            DNBStandardFileFormatMapper dnbStandardFileFormatMapper = new DNBStandardFileFormatMapper();
            dnbStandardFileFormatMapper.Initialize(null, defaultConfiguration);
            _modules.Add(dnbStandardFileFormatMapper);
            PlateSplitter plateSplitter = new PlateSplitter();
            plateSplitter.Initialize(null, defaultConfiguration);
            _modules.Add(plateSplitter);
            SourceSampleBCValidator sourceSampleBCValidator = new SourceSampleBCValidator();
            sourceSampleBCValidator.Initialize(null, defaultConfiguration);
            _modules.Add(sourceSampleBCValidator);
            TargetSampleBCValidator targetSampleBCValidator = new TargetSampleBCValidator();
            targetSampleBCValidator.Initialize(null, defaultConfiguration);
            _modules.Add(targetSampleBCValidator);
            ExistingMasterLookup existingMasterLookup = new ExistingMasterLookup();
            existingMasterLookup.Initialize(null, defaultConfiguration);
            _modules.Add(existingMasterLookup);
            UserNameMapper userNameMapper = new UserNameMapper();
            userNameMapper.Initialize(null, defaultConfiguration);
            _modules.Add(userNameMapper);
            TargetContainerTypeMapper targetContainerTypeMapper = new TargetContainerTypeMapper();
            targetContainerTypeMapper.Initialize(null, defaultConfiguration);
            _modules.Add(targetContainerTypeMapper);
            SourceContainerTypeMapper sourceContainerTypeMapper = new SourceContainerTypeMapper();
            sourceContainerTypeMapper.Initialize(null, defaultConfiguration);
            _modules.Add(sourceContainerTypeMapper);
            LocationMapper locationMapper = new LocationMapper();
            locationMapper.Initialize(null, defaultConfiguration);
            _modules.Add(locationMapper);
            ActionDateTimeMapper actionDateTimeMapper = new ActionDateTimeMapper();
            actionDateTimeMapper.Initialize(null, defaultConfiguration);
            _modules.Add(actionDateTimeMapper);
            VolumeValidator volumeValidator = new VolumeValidator();
            volumeValidator.Initialize(null, defaultConfiguration);
            _modules.Add(volumeValidator);
        }

        public DataFile DoStuff(string projectName, DataFile file)
        {
            DNBLogger.Log("DNBStandardFileFormatParser", "DoStuff");
            DataFile tmpFile = file;
            foreach(IModule module in _modules)
            {
                tmpFile = module.DoStuff(projectName, tmpFile);
                if(tmpFile==null)
                {
                    DNBLogger.Log("DNBStandardFileFormatParser", "Failed");
                    return null;
                }
            }
            DNBLogger.Log("DNBStandardFileFormatParser", "Done");
            return tmpFile;
        }
    }
}
