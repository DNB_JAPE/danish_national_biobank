﻿using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace DNBReader
{
    /// <summary>
    /// Logging class. Uses EnterpriseLibrary for logging. 
    /// The program must call Init before the first call to Log in order to start the EnterpriseLibrary.
    /// </summary>
    internal class DNBLogger
    {
        static DNBLogger()
        {
            Enable = true;
        }

        public static bool Enable { get; set; }
        public static string LastLog { get; set; }

        public static void Init()
        {
            if (Enable)
            {
                Logger.SetLogWriter(new LogWriterFactory().Create());
            }
        }

        public static void Log(string componentName, string message)
        {
            LastLog = componentName + ": " + message;
            if (Enable)
            {
                LogEntry entryStart = new LogEntry();
                entryStart.Message = componentName + ": " + message;
                Logger.Write(entryStart);
            }
        }
    }
}
