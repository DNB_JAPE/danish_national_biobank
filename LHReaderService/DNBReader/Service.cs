﻿using DNBTools;
using DNBTools.Nautilus;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.ServiceProcess;
using System.Timers;
using System.Xml;

namespace DNBReader
{
    public partial class Service : ServiceBase
    {
        private System.Timers.Timer _timer;
        private List<Project> _projects = new List<Project>();
        internal static LimsCommunication LimsCommunication { get; set; } 
        
        public Service()
        {
            InitializeComponent();
            _timer = new Timer(Convert.ToInt32(ConfigurationManager.AppSettings["IdleTime"]));
            _timer.AutoReset = false;
            _timer.Elapsed += new ElapsedEventHandler(timer_elasped);
        }

        public void Start()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                DNBLogger.Init();
                XmlDocument configurationDoc = new XmlDocument();
                configurationDoc.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Configuration.xml"));
                XmlNode defaultConfiguration = configurationDoc.SelectSingleNode("/Configuration/Defaults");
                XmlNodeList projectConfigurations = configurationDoc.SelectNodes("/Configuration/Projects/Project[@Active='True']");
                foreach (XmlNode projectConfiguration in projectConfigurations)
                {
                    try
                    {
                        Project project = new Project(projectConfiguration, defaultConfiguration);
                        _projects.Add(project);
                    }
                    catch(Exception ex)
                    {
                        DNBLogger.Log("Service", "Failed to initialize project (" + projectConfiguration.Attributes["Name"].Value + "): " + ex.Message);
                        return;
                    }
                }
                _timer.Start();
            }
            catch (Exception ex)
            {
                DNBLogger.Log("Service", "Failed to start: " + ex.Message);
                return;
            }
        }

        protected override void OnStop()
        {
            _timer.Stop();
            if(LimsCommunication != null)
            {
                LimsCommunication.Dispose();
            }
        }

        private void timer_elasped(object sender, ElapsedEventArgs e)
        {
            bool didStuff = true;
            while (didStuff)
            {
                didStuff = false;
                try
                {
                    LimsCommunication = new LimsCommunication(ConfigurationManager.ConnectionStrings["Nautilus"].ConnectionString);
                }
                catch (Exception ex)
                {
                    DNBLogger.Log("Service", "Failed to initialize Nautilus: " + ex.Message);
                    return;
                }
                foreach (Project project in _projects)
                {
                    try
                    {
                        if (project.DoStuff())
                        {
                            DNBLogger.Log("Service", project.Name + " Did some work");
                            didStuff = true;
                        }
                    }
                    catch(Exception ex)
                    {
                        DNBLogger.Log("Service", project.Name + " failed to do stuff: " + ex.Message);
                    }
                }
                LimsCommunication.Dispose();
            }
            _timer.Start();
        }
    }
}
