﻿using System;
using System.Configuration.Install;
using System.Reflection;
using System.ServiceProcess;
using System.Threading;

namespace DNBReader
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            try 
            {
                if (args[0] == "debug")
                {
                    Service service = new Service();
                    service.Start();
                    while (true) { Thread.Sleep(10); }
                }
                else
                {
                    if (Environment.UserInteractive)
                    {
                        ManagedInstallerClass.InstallHelper(new string[] { Assembly.GetExecutingAssembly().Location });
                    }
                    else
                    {
                        ServiceBase.Run(new Service());
                    }
                }
            }
            catch
            {
                // TODO: Log
            }
        }
    }
}
