﻿using DNBTools;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace LHReaderService
{
    internal class DisposeReader : Reader
    {
        internal LHReaderService _service = null;
        internal string _dataType;
        internal string _idColumn;
        internal string _bcColumn;
        internal string _userNameColumn;
        internal string _userCommentColumn;
        internal string _actionDateTimeColumn;
        internal bool _extended;
        internal string _inputPath;
        internal string _backupPath;
        internal string _errorPath;
        internal string _inputFilePattern;
        internal string _sampleBCFindSql;
        internal string _skipLineText;
        internal string _containerTypeColumn;
        internal string _containerTypeSQL;

        internal DisposeReader(LHReaderService service, XmlNode readerNode)
        {
            _service = service;
            _active = bool.Parse(readerNode.SelectSingleNode(".//Active").InnerXml);
            _name = readerNode.SelectSingleNode(".//Name").InnerXml;
            _dataType = readerNode.SelectSingleNode(".//DisposeType").InnerXml;
            _idColumn = readerNode.SelectSingleNode(".//IdColumn").InnerXml;
            _bcColumn = readerNode.SelectSingleNode(".//BCColumn").InnerXml;
            _userNameColumn = readerNode.SelectSingleNode(".//UserNameColumn").InnerXml;
            _userCommentColumn = readerNode.SelectSingleNode(".//UserCommentColumn").InnerXml;
            _actionDateTimeColumn = readerNode.SelectSingleNode(".//ActionDateTimeColumn").InnerXml;
            _extended = bool.Parse(readerNode.SelectSingleNode(".//Extended").InnerXml);
            _inputPath = readerNode.SelectSingleNode("./InputPath").InnerXml;
            _backupPath = readerNode.SelectSingleNode("./BackupPath").InnerXml;
            _errorPath = readerNode.SelectSingleNode("./ErrorPath").InnerXml;
            _inputFilePattern = readerNode.SelectSingleNode("./InputFilePattern").InnerXml;
            _sampleBCFindSql = readerNode.SelectSingleNode("./FindSampleIdFromBCSQL").InnerXml;
            _skipLineText = readerNode.SelectSingleNode("./SkipLineText").InnerXml;
            _containerTypeColumn = readerNode.SelectSingleNode("./ContainerTypeColumn").InnerXml;
            _containerTypeSQL = readerNode.SelectSingleNode("./ContainerTypeSQL").InnerXml;
        }

        internal override void DoWork(OracleConnection connection)
        {
            FileInfo[] files;
            try
            {
                DirectoryInfo inputDirectory = new DirectoryInfo(_inputPath);
                if (inputDirectory.Exists)
                {
                    files = inputDirectory.GetFiles(_inputFilePattern);
                }
                else
                {
                    LHReaderService.Log(string.Format("Can't find directory [{0}] for {1}: ", _inputPath, Name));
                    return;
                }
            }
            catch (Exception ex)
            {
                LHReaderService.Log("Error finding files for " + Name + ": " + ex.Message);
                return;
            }
            foreach (FileInfo file in files)
            {
                LHReaderService.Log("Handling input file: " + file.Name);
                switch (_dataType)
                {
                    case "Plate":
                        {
                            if (DisposePlates(file, connection))
                            {
                                file.MoveTo(Path.Combine(_backupPath, file.Name));
                                LHReaderService.Log("Input file moved to backup folder");
                            }
                            else
                            {
                                file.MoveTo(Path.Combine(_errorPath, file.Name));
                                LHReaderService.Log("Input file moved to error folder");
                            }
                            break;
                        }
                    case "SampleById":
                        {
                            if (DisposeSamplesById(file, connection))
                            {
                                file.MoveTo(Path.Combine(_backupPath, file.Name));
                                LHReaderService.Log("Input file moved to backup folder");
                            }
                            else
                            {
                                file.MoveTo(Path.Combine(_errorPath, file.Name));
                                LHReaderService.Log("Input file moved to error folder");
                            }
                            break;
                        }
                    case "SampleByBC":
                        {
                            if (DisposeSamplesByBC(file, connection))
                            {
                                file.MoveTo(Path.Combine(_backupPath, file.Name));
                                LHReaderService.Log("Input file moved to backup folder");
                            }
                            else
                            {
                                file.MoveTo(Path.Combine(_errorPath, file.Name));
                                LHReaderService.Log("Input file moved to error folder");
                            }
                            break;
                        }
                }
            }
        }

        internal List<DisposeItem> Parse(FileInfo file)
        {
            try 
            {
                List<DisposeItem> items = new List<DisposeItem>();
                using (StreamReader reader = new StreamReader(file.OpenRead()))
                {
                    Dictionary<string, int> columns = new Dictionary<string, int>();
                    while (true)
                    {
                        string line = reader.ReadLine();
                        if (line == null)
                        {
                            break;
                        }
                        line = line.Replace("\"", string.Empty).ToUpper();
                        string[] parts = line.Split(';');
                        if (columns.Keys.Count == 0)
                        {
                            for (int i = 0; i < parts.Length; i++)
                            {
                                columns.Add(parts[i], i);
                            }
                        }
                        else
                        {
                            //if(parts[columns[_bcColumn]] != _skipLineText)
                            if (parts[columns[_idColumn]] != _skipLineText)
                            {
                                DisposeItem item = new DisposeItem();
                                item.Id = _idColumn == "" ? "" : parts[columns[_idColumn]];
                                item.Bc = _bcColumn == "" ? "" : parts[columns[_bcColumn]];
                                item.UserName = _userNameColumn == "" ? "" : parts[columns[_userNameColumn]];
                                item.UserComment = _userCommentColumn == "" ? "" : parts[columns[_userCommentColumn]];
                                item.ActionDateTime = _actionDateTimeColumn == "" ? "" : parts[columns[_actionDateTimeColumn]];
                                item.ContainerType = _containerTypeColumn == "" ? "" : parts[columns[_containerTypeColumn]];
                                items.Add(item);
                            }
                        }
                    }
                }
                if(items.Count > 0 && items.Any(x => x.ContainerType != items[0].ContainerType))
                {
                    LHReaderService.Log("Error parsing file: Container types are not identical for all samples");
                    return null;
                }
                return items;
            }
            catch (Exception ex)
            {
                LHReaderService.Log("Error parsing file: " + file.Name + ": " + ex.Message);
                return null;
            }
        }

        internal bool DisposePlates(FileInfo file, OracleConnection connection)
        {
            try
            {
                List<DisposeItem> items = Parse(file);
                if(items != null)
                {
                    bool success = true;
                    foreach (DisposeItem item in items)
                    {
                        if (_extended || IsPlateEmpty(item.Bc, connection))
                        {
                            DisposePlate(item.Bc, connection);
                            LHReaderService.Log("Plate disposed: " + item.Bc);
                        }
                        else
                        {
                            LHReaderService.Log("Plate is not empty: " + item.Bc);
                            success = false;
                        }
                    }
                    return success;
                }
            }
            catch(Exception ex)
            {
                LHReaderService.Log("Error disposing file: " + file.Name + ": " + ex.Message);
            }
            return false;
        }

        internal bool DisposeSamplesById(FileInfo file, OracleConnection connection)
        {
            try
            {
                List<DisposeItem> items = Parse(file);
                if(items != null)
                {
                    return DisposeSamples(items, _extended, connection);
                }
            }
            catch(Exception ex)
            {
                LHReaderService.Log("Error disposing file: " + file.Name + ": " + ex.Message);
            }
            return false;
        }

        internal bool DisposeSamplesByBC(FileInfo file, OracleConnection connection)
        {
            try
            {
                List<DisposeItem> filteredItems = new List<DisposeItem>();
                List<DisposeItem> items = Parse(file);
                if (items != null)
                {
                    string barcodeString = string.Empty;
                    foreach (DisposeItem item in items)
                    {
                        barcodeString = barcodeString + "'" + item.Bc + "',";
                    }
                    barcodeString = barcodeString.TrimEnd(',');
                    string sql = _sampleBCFindSql.Replace("##SAMPLEIDS##", barcodeString);
                    string containerTypeId = FindContainerTypeId(connection, items[0].ContainerType);
                    sql = sql.Replace("##CONTAINERTYPEID##", containerTypeId);
                    using (OracleCommand command = new OracleCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = sql;
                        command.CommandType = System.Data.CommandType.Text;
                        using (OracleDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string id = ((long)reader["Aliquot_Id"]).ToString();
                                string bc = reader["External_Reference"].ToString();
                                DisposeItem item = items.FirstOrDefault(x => x.Bc == bc);
                                if (item != null)
                                {
                                    item.Id = id;
                                    filteredItems.Add(item);
                                    items.Remove(item);
                                }
                                else
                                {
                                    LHReaderService.Log("Error: Found too many samples in DB (" + bc + ")");
                                }
                            }
                        }
                    }
                    if(items.Count > 0)
                    {
                        string notFoundString = string.Empty;
                        foreach (DisposeItem item in items)
                        {
                            notFoundString = notFoundString + "'" + item.Bc + "',";
                        }
                        notFoundString = notFoundString.TrimEnd(',');
                        LHReaderService.Log("Error failed to find samples: " + notFoundString);
                        return false;
                    }
                    return DisposeSamples(filteredItems, _extended, connection);
                }
            }
            catch(Exception ex)
            {
                LHReaderService.Log("Error disposing file: " + file.Name + ": " + ex.Message);
            }
            return false;
        }

        private bool DisposeSamples(List<DisposeItem> items, bool extended, OracleConnection connection)
        {
            List<string> plateBarcodes = new List<string>();
            if (extended)
            {
                foreach (DisposeItem item in items)
                {
                    string plateBarcode = GetPlateBCFromSample(item.Id, connection);
                    if (plateBarcode != string.Empty && !plateBarcodes.Contains(plateBarcode))
                    {
                        plateBarcodes.Add(plateBarcode);
                    }
                }
            }
            bool success = true;
            foreach (DisposeItem item in items)
            {
                DisposeSample(item, connection);
                LHReaderService.Log("Sample disposed: " + item.Id);
            }
            foreach (string plateBarcode in plateBarcodes)
            {
                if (IsPlateEmpty(plateBarcode, connection))
                {
                    DisposePlate(plateBarcode, connection);
                }
            }
            return success;
        }

        private bool IsPlateEmpty(string barcode, OracleConnection connection)
        {
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = connection;
                command.CommandText = "SELECT COUNT(*) FROM Aliquot WHERE Plate_Id = (SELECT Plate_Id FROM Plate WHERE EXTERNAL_REFERENCE = '##BARCODE##')";
                command.CommandText = command.CommandText.Replace("##BARCODE##", barcode);
                command.CommandType = System.Data.CommandType.Text;
                decimal count = (decimal)command.ExecuteScalar();
                return count == 0 ? true : false;
            }
        }

        private void DisposePlate(string barcode, OracleConnection connection)
        {
            long plate_Id = -1;
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = connection;
                command.CommandText = "SELECT Plate_Id FROM Plate WHERE EXTERNAL_REFERENCE = '##BARCODE##'";
                command.CommandText = command.CommandText.Replace("##BARCODE##", barcode);
                command.CommandType = System.Data.CommandType.Text;
                plate_Id = (long)command.ExecuteScalar();
            }
            List<string> sampleIds = GetSampleIdsFromPlate(barcode, connection);
            string sampleIdString = string.Empty;
            foreach (string id in sampleIds)
            {
                sampleIdString = sampleIdString + "'" + id + "',";
            }
            sampleIdString = sampleIdString.TrimEnd(',');
            using (OracleTransaction transaction = connection.BeginTransaction())
            {
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "UPDATE Plate SET Location_Id='5948' WHERE Plate_Id='##PLATE_ID##'";
                    command.CommandText = command.CommandText.Replace("##PLATE_ID##", plate_Id.ToString());
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "UPDATE Plate_User SET U_Disposed='T' WHERE Plate_Id='##PLATE_ID##'";
                    command.CommandText = command.CommandText.Replace("##PLATE_ID##", plate_Id.ToString());
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
                if (sampleIdString != string.Empty)
                {
                    using (OracleCommand command = new OracleCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "UPDATE Aliquot SET Location_Id='5948' WHERE Aliquot_Id IN (" + sampleIdString + ")";
                        command.CommandType = System.Data.CommandType.Text;
                        command.ExecuteNonQuery();
                    }
                    using (OracleCommand command = new OracleCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "UPDATE Aliquot_User SET U_Disposed='T' WHERE Aliquot_Id IN (" + sampleIdString + ")";
                        command.CommandType = System.Data.CommandType.Text;
                        command.ExecuteNonQuery();
                    }
                }
                transaction.Commit();
            }
        }

        private void DisposeSample(DisposeItem item, OracleConnection connection)
        {
            using (OracleTransaction transaction = connection.BeginTransaction())
            {
                string storageString;
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = connection;
                    // 20200116 ESAT Added AND Location_Id not like '5948' because if file exists in error map, this will repeat itself until string is too long and oracle fails.
                    command.CommandText = "SELECT Storage FROM Aliquot WHERE Aliquot_Id='" + item.Id.ToString() + "' AND Location_Id not like '5948'";
                    command.CommandType = System.Data.CommandType.Text;
                    SysLog.Log("Dispose samples. Get storage: " + command.CommandText, LogLevel.Info);
                    object obj = command.ExecuteScalar();
                    storageString = obj == null ? string.Empty : obj.ToString();
                    SysLog.Log("Dispose samples. Get storage. Found: " + storageString, LogLevel.Info);
                }
                // 20200116 ESAT Added AND Location_Id not like '5948' because if file exists in error map, this will repeat itself until string is too long and oracle fails.
                string tmpComment = "[" + item.UserName + " - " + item.ActionDateTime + "] " + item.UserComment;
                if (tmpComment != "[ - ] ")
                {
                    storageString = storageString + Environment.NewLine + "[" + item.UserName + " - " + item.ActionDateTime + "] " + item.UserComment;
                }
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "UPDATE Aliquot SET Location_Id='5948', Storage='" + storageString + "' WHERE Aliquot_Id='" + item.Id.ToString() + "' AND Location_Id not like '5948'";
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
                using (OracleCommand command = new OracleCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "UPDATE Aliquot_User SET U_Disposed='T' WHERE Aliquot_Id='" + item.Id.ToString() + "'";
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
                transaction.Commit();
            }
        }

        private string GetPlateBCFromSample(string id, OracleConnection connection)
        {
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = connection;
                command.CommandText = "SELECT External_Reference FROM Plate WHERE Plate_Id = (SELECT Plate_Id FROM Aliquot WHERE Aliquot_Id = '##ID##')";
                command.CommandText = command.CommandText.Replace("##ID##", id.ToString());
                command.CommandType = System.Data.CommandType.Text;
                object obj = command.ExecuteScalar();
                return obj == null ? string.Empty : obj.ToString();
            }
        }

        public List<string> GetSampleIdsFromPlate(string barcode, OracleConnection connection)
        {
            string sql = "SELECT Aliquot_Id FROM Aliquot WHERE Plate_Id = (SELECT Plate_Id FROM Plate WHERE EXTERNAL_REFERENCE = '##BARCODE##')".Replace("##BARCODE##", barcode);
            List<string> retval = new List<string>();
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = connection;
                command.CommandText = sql;
                command.CommandType = System.Data.CommandType.Text;
                using (OracleDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string id = ((long)reader["Aliquot_Id"]).ToString();
                        retval.Add(id);
                    }
                }
            }
            return retval;
        }

        /// <summary>
        /// Lookup container type id from container type name.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="aliquot"></param>
        /// <returns></returns>
        internal virtual string FindContainerTypeId(OracleConnection connection, string containerTypeName)
        {
            string retval = string.Empty;
            OracleCommand command = new OracleCommand();
            command.Connection = connection;
            string commandText = _containerTypeSQL;
            if (commandText.Contains("##CONTAINERTYPE##"))
            {
                commandText = commandText.Replace("##CONTAINERTYPE##", containerTypeName);
            }
            command.CommandText = commandText;
            command.CommandType = System.Data.CommandType.Text;
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                retval = reader["container_type_id"].ToString();
            }
            reader.Close();
            command.Dispose();
            return retval;
        }
    }
}

