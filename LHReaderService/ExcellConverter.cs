﻿using System;
using System.Data;
using Microsoft.Office.Interop.Excel;
using System.IO;

namespace LHReaderService
{
    public class ExcellConverter
    {
        public bool ConvertExcelToCSV(string sourceExcelPathAndName, string targetCSVPathAndName)
        {
            LHReaderService.Log("Converting Excell file " + sourceExcelPathAndName);
            Microsoft.Office.Interop.Excel.Application oXL = null;
            Workbook mWorkBook = null;
            Sheets mWorkSheets = null;
            Worksheet mWSheet1 = null;
            try
            {
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = false;
                oXL.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Workbooks workbooks = oXL.Workbooks;
                mWorkBook = workbooks.Open(sourceExcelPathAndName, 0, false, 5, "", "", false, XlPlatform.xlWindows, "", true, false, 0, true, false, false);
                //Get all the sheets in the workbook
                mWorkSheets = mWorkBook.Worksheets;
                //Get the specified sheet
                mWSheet1 = (Worksheet)mWorkSheets[1];
                Microsoft.Office.Interop.Excel.Range range = mWSheet1.UsedRange;
                //replacing ENTER with a space
                range.Replace("\n", " ", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                //replacing COMMA with the column delimeter
                range.Replace(",", @"|#|", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                mWorkBook.SaveAs(targetCSVPathAndName, XlFileFormat.xlTextMSDOS, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, false);
                mWSheet1 = null; 
                mWorkBook.Close(Type.Missing, Type.Missing, Type.Missing);
                mWorkBook = null;
                oXL.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oXL);
                oXL = null;
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                using (StreamReader reader = new StreamReader(targetCSVPathAndName))
                {
                    using (StreamWriter writer = new StreamWriter(targetCSVPathAndName + ".tmp"))
                    {
                        while (true)
                        {
                            string line = reader.ReadLine();
                            if (line == null)
                            {
                                break;
                            }
                            line = line.Replace('\t', ';');
                            writer.WriteLine(line);
                        }
                    }
                }
                File.Delete(targetCSVPathAndName);
                File.Move(targetCSVPathAndName + ".tmp", targetCSVPathAndName);
                return true;
            }
            catch (Exception ex)
            {
                LHReaderService.Log("Error: Failed to convert " + sourceExcelPathAndName + ": " + ex.Message);
                if (mWSheet1 != null) { mWSheet1 = null; }
                if (mWorkBook != null) { mWorkBook.Close(Type.Missing, Type.Missing, Type.Missing); }
                if (mWorkBook != null) { mWorkBook = null; }
                if (oXL != null) { oXL.Quit(); }
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oXL);
                if (oXL != null) { oXL = null; }
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                return false;
            }
        }
    }
}
