﻿
using System.Collections.Generic;
namespace LHReaderService
{
    internal class Aliquot
    {
        internal string Description { get; set; }
        internal List<string> Parts { get; set; }

        internal Aliquot(string description, List<string> parts)
        {
            Description = description;
            Parts = parts;
        }
    }
}
