﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Linq;

namespace LHReaderService
{
    internal class Plate
    {
        internal List<string> Header { get; private set; }
        internal string Barcode { get; private set; }
        internal List<Aliquot> Aliquots { get; set; }
        internal Dictionary<string, int> Columns { get; private set; }

        internal Plate(List<string> header, string barcode, Dictionary<string, int> columns)
        {
            Header = header;
            Barcode = barcode;
            Aliquots = new List<Aliquot>();
            Columns = columns;
        }
    }
}
