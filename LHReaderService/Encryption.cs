﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace LHReaderService
{
    internal static class Encryption
    {
        private static string key = "ZPvTwJ2IB7cjgwphs0NERxJo76cCeiPZqvHpJg7qUW8=";
        private static string iv = "zqtmhQqauea5ylSP2yuXsg==";

        internal static string Encrypt(string plainText)
        {
            return EncryptStringToBytes_Aes(plainText, Convert.FromBase64String(key), Convert.FromBase64String(iv));
        }

        internal static string Decrypt(string encryptedText)
        {
            return DecryptStringFromBytes_Aes(encryptedText, Convert.FromBase64String(key), Convert.FromBase64String(iv));
        }

        private static string EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (plainText == null || plainText.Length <= 0)
            {
                return string.Empty;
            }
            if (Key == null || Key.Length <= 0)
            {
                throw new ArgumentNullException("Key");
            }
            if (IV == null || IV.Length <= 0)
            {
                throw new ArgumentNullException("Key");
            }
            byte[] encrypted;
            // Create an AesManaged object with the specified key and IV.  
            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;
                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            // Return the encrypted string from the memory stream. 
            return Convert.ToBase64String(encrypted);
        }

        private static string DecryptStringFromBytes_Aes(string encryptedText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (String.IsNullOrEmpty(encryptedText))
            {
                return string.Empty;
            }
            if (Key == null || Key.Length <= 0)
            {
                throw new ArgumentNullException("Key");
            }
            if (IV == null || IV.Length <= 0)
            {
                throw new ArgumentNullException("Key");
            }
            byte[] cipherText = Convert.FromBase64String(encryptedText);
            // Declare the string used to hold the decrypted text.
            string plaintext = null;
            // Create an AesManaged object with the specified key and IV. 
            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;
                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                // Create the streams used for decryption. 
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            // Read the decrypted bytes from the decrypting stream and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }
    }
}
