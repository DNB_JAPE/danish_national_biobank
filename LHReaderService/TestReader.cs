﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace LHReaderService
{
    internal class TestReader : ReaderBase
    {
        internal TestReader(LHReaderService service, XmlNode readerNode)
            : base(service, readerNode)
        {}

        internal override void DoWork(Oracle.DataAccess.Client.OracleConnection connection)
        {
            // Find source files and convert to csv
            List<Tuple<FileInfo, FileInfo>> inputFilesSets = FindFiles();
            if (inputFilesSets != null)
            {
                foreach (Tuple<FileInfo, FileInfo> inputFileSet in inputFilesSets)
                {
                    if (inputFileSet != null)
                    {
                        try
                        {
                            List<FileInfo> activeFileSet = new List<FileInfo>();
                            if (inputFileSet.Item1 != null && inputFileSet.Item1.Exists)
                            {
                                activeFileSet.Add(inputFileSet.Item1);
                            }
                            // Read robot file
                            List<Plate> plates = ParseInputFile(inputFileSet.Item1, new Dictionary<string, string>());
                            if (plates == null) // Parse failed
                            {
                                MoveFiles(activeFileSet, _configuration.ErrorPath);
                                continue;
                            }
                            // Write test login file
                            if (!WriteTestLoginFile(connection, plates, inputFileSet.Item1))
                            {
                                MoveFiles(activeFileSet, _configuration.ErrorPath);
                                continue;
                            }
                            // Create build plate files
                            if (!WriteAliquotFiles(connection, plates, inputFileSet.Item1))
                            {
                                MoveFiles(activeFileSet, _configuration.ErrorPath);
                                continue;
                            }
                            // Cleanup
                            MoveFiles(activeFileSet, _configuration.BackupPath);
                        }
                        catch (Exception ex)
                        {
                            LHReaderService.Log(ex.Message);
                            break;
                        }
                    }
                }
            }
        }

        private bool WriteTestLoginFile(Oracle.DataAccess.Client.OracleConnection connection, List<Plate> plates, FileInfo inputFile)
        {
            if (plates == null)
            {
                return false;
            }
            else
            {
                LHReaderService.Log(_configuration.Name + "Writing test login files: " + inputFile.Name);
                try
                {
                    foreach (Plate plate in plates)
                    {
                        if (plate.Aliquots.Count > 0)
                        {
                            StringBuilder a = new StringBuilder();
                            a.AppendLine("Begin Aliquot");
                            a.AppendLine("\"External_Ref\",\"Aliquot_Id\",\"Sample_Ref\",\"Description\"");
                            StringBuilder t = new StringBuilder();
                            t.AppendLine("Begin Test");
                            t.AppendLine("\"External_Ref\",\"Workflow_Name\",\"Aliquot_Id\",\"Description\"");
                            bool foundAll = true;
                            foreach (Aliquot aliquot in plate.Aliquots)
                            {
                                // Replace aliquot ref with aliquot id
                                try
                                {
                                    string aliquotId = string.Empty;
                                    string aliquotRef = aliquot.Parts[plate.Columns[_configuration.SourceSampleBCColumnName]];
                                    aliquotId = FindAliquotId(connection, aliquot, plate);
                                    if (string.IsNullOrEmpty(aliquotId))
                                    {
                                        LHReaderService.Log("Error: Failed to look up aliquot id: " + aliquotRef);
                                        foundAll = false;
                                        continue;
                                    }
                                    a.AppendLine(",\"" + aliquotId + "\",,");
                                    t.AppendLine(",\"" + _configuration.TestWorkflow + "\",\"" + aliquotId + "\",");
                                }
                                catch (Exception ex)
                                {
                                    LHReaderService.Log("Error: Failed to look up aliquot id: " + ex.Message + Environment.NewLine + ex.StackTrace);
                                    foundAll = false;
                                    continue;
                                }
                            }
                            if(!foundAll)
                            {
                                LHReaderService.Log("Error: Failed to look up data. Stopping.");
                                return false;
                            }
                            a.AppendLine("End Aliquot");
                            t.AppendLine("End Test");                            
                            string fileName = inputFile.Name.Substring(0, inputFile.Name.Length - 4) + "_(" + plate.Barcode + ").csv";
                            FileInfo tmpFile = new FileInfo(Path.Combine(_configuration.TmpPath, fileName));
                            using (StreamWriter writer = new StreamWriter(tmpFile.OpenWrite()))
                            {
                                writer.Write(a.ToString());
                                writer.WriteLine();
                                writer.Write(t.ToString());
                            }
                            string fileString = Path.Combine(_configuration.TestFilePath, fileName);
                            tmpFile.MoveTo(fileString);
                            // Wait for instrument to process aliquot files.
                            if (!WaitForInstrument(new FileInfo(fileString)))
                            {
                                return false;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LHReaderService.Log("Error: Failed to create test login file: " + ex.Message + Environment.NewLine + ex.StackTrace);
                    return false;
                }
                return true;
            }
        }
    }
}
