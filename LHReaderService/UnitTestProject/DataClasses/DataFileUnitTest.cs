﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DNBReader.DataClasses;
using Moq;

namespace UnitTestProject.DataClasses
{
    [TestClass]
    public class DataFileUnitTest
    {
        [TestMethod]
        public void Test_DataFile_Constructor()
        {
            DataFile datafile = new DataFile();
            Assert.IsNull(datafile.Filename);
            Assert.IsNull(datafile.Header);
            Assert.IsNotNull(datafile.Groups);
        }

        [TestMethod]
        public void Test_DataFile_AddColumn()
        {
            DataFile datafile = new DataFile();
            datafile.AddColumn("a", "");
            Assert.AreEqual("a", datafile.Header);

            datafile = new DataFile();
            datafile.DataDelimiter = ';';
            datafile.AddColumn("a", "");
            datafile.AddColumn("b", "");
            Assert.AreEqual("a;b", datafile.Header);

            datafile = new DataFile();
            Mock<DataGroup> oGroup = new Mock<DataGroup>(datafile);
            oGroup.Setup(t => t.AddColumn(It.IsAny<string>(), It.IsAny<string>()));
            datafile.Groups.Add(oGroup.Object);
            datafile.AddColumn("a", "b");
            oGroup.Verify(c => c.AddColumn("a", "b"), Times.Once);
        }
    }
}
