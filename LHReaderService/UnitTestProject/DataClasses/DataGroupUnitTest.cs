﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DNBReader.DataClasses;
using Moq;

namespace UnitTestProject.DataClasses
{
    [TestClass]
    public class DataGroupUnitTest
    {
        [TestMethod]
        public void Test_DataGroup_Constructor()
        {
            DataFile datafile = new DataFile();
            DataGroup datagroup = new DataGroup(datafile);
            Assert.AreEqual(datafile, datagroup.File);
            Assert.IsNotNull(datagroup.DataRows);
        }

        [TestMethod]
        public void Test_DataGroup_AddColumn()
        {
            DataGroup group = new DataGroup(null);
            group.AddColumn("a", "");

            group = new DataGroup(null);
            group.AddColumn("a", "");

            group = new DataGroup(null);
            DataRow row = new DataRow(group);
            group.DataRows.Add(row);
            group.AddColumn("a", "b");
            Assert.IsTrue(row.Cells.ContainsKey("a"));
            Assert.AreEqual("b", row.Cells["a"]);
            Assert.AreEqual(1, row.Cells.Count);
            group.AddColumn("a", "c");
            Assert.IsTrue(row.Cells.ContainsKey("a"));
            Assert.AreEqual("b", row.Cells["a"]);
            Assert.AreEqual(1, row.Cells.Count);
        }
    }
}
