﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DNBReader.DataClasses;

namespace UnitTestProject.DataClasses
{
    [TestClass]
    public class DataRowUnitTest
    {
        [TestMethod]
        public void Test_DataRow_Constructor()
        {
            DataGroup group = new DataGroup(null);
            DataRow row = new DataRow(group);
            Assert.AreEqual(group, row.Group);
            Assert.IsNotNull(row.Cells);
        }

        [TestMethod]
        public void Test_DataRow_ToString()
        {
            DataFile file = new DataFile();
            file.DataDelimiter = ',';
            DataGroup group = new DataGroup(file);
            DataRow row = new DataRow(group);
            Assert.AreEqual("", row.ToString());
            row.Cells.Add("a", "b");
            row.Cells.Add("c", "d");
            Assert.AreEqual("b,d", row.ToString());
        }
    }
}
