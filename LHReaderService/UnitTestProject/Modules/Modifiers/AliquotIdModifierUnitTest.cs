﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using DNBReader;
using DNBReader.DataClasses;
using DNBTools.Nautilus;
using Moq;
using System.Collections.Generic;

namespace UnitTestProject.Modules.Modifiers
{
    [TestClass]
    public class AliquotIdModifierUnitTest
    {
        public string _configDefault = "<Defaults><Modifiers><AliquotIdModifier><IDColumn>IDColumn</IDColumn><DescriptionColumn>DescriptionColumn</DescriptionColumn><SourceSampleBCColumn>SourceSampleBCColumn</SourceSampleBCColumn><SourcePlateBCColumn>SourcePlateBCColumn</SourcePlateBCColumn><ContainerTypeColumn>ContainerTypeColumn</ContainerTypeColumn><ProjectColumn>ProjectColumn</ProjectColumn><Sql>Sql</Sql></AliquotIdModifier></Modifiers></Defaults>";
        public string _configEmpty = "<AliquotIdModifier></AliquotIdModifier>";
        public string _configFull = "<AliquotIdModifier><IDColumn>IDColumn</IDColumn><DescriptionColumn>DescriptionColumn</DescriptionColumn><SourceSampleBCColumn>SourceSampleBCColumn</SourceSampleBCColumn><SourcePlateBCColumn>SourcePlateBCColumn</SourcePlateBCColumn><ContainerTypeColumn>ContainerTypeColumn</ContainerTypeColumn><ProjectColumn>ProjectColumn</ProjectColumn><Sql>Sql</Sql></AliquotIdModifier>";

        private XmlNode DefaultConfig
        {
            get
            {
                XmlDocument defaultConfig = new XmlDocument();
                defaultConfig.LoadXml(_configDefault);
                return defaultConfig.FirstChild;
            }
        }
        private XmlNode ConfigEmpty
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configEmpty);
                return config.FirstChild;
            }
        }
        private XmlNode ConfigFull
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configFull);
                return config.FirstChild;
            }
        }

        [TestMethod]
        public void Test_AliquotIdModifier_Initialize()
        {
            DNBLogger.Enable = false;

            AliquotIdModifier modifier = new AliquotIdModifier();
            modifier.Initialize(ConfigEmpty, DefaultConfig, null);
            Assert.AreEqual(modifier.TargetColumn, "IDColumn");
            Assert.AreEqual(modifier.SourceColumns[0], "DescriptionColumn");
            Assert.AreEqual(modifier.SourceColumns[1], "SourceSampleBCColumn");
            Assert.AreEqual(modifier.SourceColumns[2], "SourcePlateBCColumn");
            Assert.AreEqual(modifier.SourceColumns[3], "ContainerTypeColumn");
            Assert.AreEqual(modifier.SourceColumns[4], "ProjectColumn");
            Assert.AreEqual(modifier.Sql, "Sql");

            modifier = new AliquotIdModifier();
            modifier.Initialize(ConfigFull, DefaultConfig, null);
            Assert.AreEqual(modifier.TargetColumn, "IDColumn");
            Assert.AreEqual(modifier.SourceColumns[0], "DescriptionColumn");
            Assert.AreEqual(modifier.SourceColumns[1], "SourceSampleBCColumn");
            Assert.AreEqual(modifier.SourceColumns[2], "SourcePlateBCColumn");
            Assert.AreEqual(modifier.SourceColumns[3], "ContainerTypeColumn");
            Assert.AreEqual(modifier.SourceColumns[4], "ProjectColumn");
            Assert.AreEqual(modifier.Sql, "Sql");
        }

        [TestMethod]
        public void Test_AliquotIdModifier_DoStuff()
        {
            DNBLogger.Enable = false;

            Mock<LimsCommunication> oLims = CreateLims(-1);
            Service.LimsCommunication = oLims.Object;
            AliquotIdModifier modifier = new AliquotIdModifier();
            modifier.Initialize(ConfigFull, DefaultConfig, CreateProject().Object);
            modifier.Sql = "##MASTERDESCRIPTION##,##SOURCESAMPLEBC##,##SOURCEPLATEBC##,##CONTAINERTYPE##,##PROJECT##";
            DataFile dataFile = CreateDataFile();
            DataFile retval = modifier.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("AliquotIdModifier: DoStuff end (Blah)", DNBLogger.LastLog);
            oLims.Verify(c => c.SampleGetIdsFromSql("DescriptionColumn,SourceSampleBCColumn,SourcePlateBCColumn,ContainerTypeColumn,ProjectColumn"), Times.Once);

            oLims = CreateLims(1);
            Service.LimsCommunication = oLims.Object;
            modifier = new AliquotIdModifier();
            modifier.Initialize(ConfigFull, DefaultConfig, CreateProject().Object);
            modifier.Sql = "##MASTERDESCRIPTION##,##SOURCESAMPLEBC##,##SOURCEPLATEBC##,##CONTAINERTYPE##,##PROJECT##";
            dataFile = CreateDataFile();
            retval = modifier.DoStuff(dataFile);
            Assert.IsNotNull(retval);
            Assert.AreEqual("AliquotIdModifier: DoStuff end (Blah)", DNBLogger.LastLog);
            Assert.IsTrue(retval.Header.Contains("IDColumn"));
            oLims.Verify(c => c.SampleGetIdsFromSql("DescriptionColumn,SourceSampleBCColumn,SourcePlateBCColumn,ContainerTypeColumn,ProjectColumn"), Times.Once);
            Assert.AreEqual("a", retval.Groups[0].DataRows[0].Cells["IDColumn"]);
        }

        private Mock<LimsCommunication> CreateLims(int times)
        {
            Mock<LimsCommunication> oLims = new Mock<LimsCommunication>();
            if (times == -1) { oLims.Setup(t => t.SampleGetIdsFromSql(It.IsAny<string>())).Returns((List<string>)null); }
            if (times == 1) { oLims.Setup(t => t.SampleGetIdsFromSql(It.IsAny<string>())).Returns(new List<string>{"a"}); }
            return oLims;
        }

        private Mock<Project> CreateProject()
        {
            Mock<Project> oProject = new Mock<Project>();
            oProject.SetupAllProperties();
            oProject.Object.Name = "Blah";
            return oProject;
        }

        private DataFile CreateDataFile()
        {
            DataFile dataFile = new DataFile();
            dataFile.Header = "DescriptionColumn,SourceSampleBCColumn,SourcePlateBCColumn,ContainerTypeColumn,ProjectColumn";
            dataFile.DataDelimiter = ';';
            DataGroup group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            DataRow row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("DescriptionColumn", "DescriptionColumn");
            row.Cells.Add("SourceSampleBCColumn", "SourceSampleBCColumn");
            row.Cells.Add("SourcePlateBCColumn", "SourcePlateBCColumn");
            row.Cells.Add("ContainerTypeColumn", "ContainerTypeColumn");
            row.Cells.Add("ProjectColumn", "ProjectColumn");
            return dataFile;
        }
    }
}
