﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DNBReader;
using DNBReader.Modules;
using DNBReader.DataClasses;

namespace UnitTestProject.Modules.Modifiers
{
    [TestClass]
    public class EmptyCellsModifierUnitTest
    {
        [TestMethod]
        public void Test_EmptyCellsModifier_DoStuff()
        {
            DNBLogger.Enable = false;

            EmptyCellsModifier modifier = new EmptyCellsModifier();
            object retval = modifier.DoStuff(null);
            Assert.IsNull(retval);
            Assert.AreEqual("EmptyCellsModifier: Data is not defined", DNBLogger.LastLog);

            modifier = new EmptyCellsModifier();
            DataFile datafile = new DataFile();
            datafile.Groups.Add(new DataGroup(datafile));
            datafile.Groups[0].DataRows.Add(new DataRow(datafile.Groups[0]));
            datafile.Groups[0].DataRows[0].Cells.Add("a", "a");
            datafile.Groups[0].DataRows[0].Cells.Add("b", "");
            DataFile datafileRetval = modifier.DoStuff(datafile);
            Assert.IsNotNull(datafileRetval);
            Assert.AreEqual("EmptyCellsModifier: DoStuff end", DNBLogger.LastLog);
            Assert.AreEqual("a", datafileRetval.Groups[0].DataRows[0].Cells["a"]);
            Assert.AreEqual("\"\"", datafileRetval.Groups[0].DataRows[0].Cells["b"]);
        }
    }
}
