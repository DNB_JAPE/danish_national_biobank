﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using DNBReader;
using DNBReader.Modules;
using DNBReader.DataClasses;
using Moq;

namespace UnitTestProject.Modules.Modifiers
{
    [TestClass]
    public class DescriptionModifierUnitTest
    {
        public string _configDefault = "<Defaults><Modifiers><DescriptionModifier><Pattern>Pattern</Pattern><TargetColumn>TargetColumn</TargetColumn></DescriptionModifier></Modifiers></Defaults>";
        public string _configEmpty = "<DescriptionModifier></DescriptionModifier>";
        public string _configFull = "<DescriptionModifier><Pattern>Pattern</Pattern><TargetColumn>TargetColumn</TargetColumn></DescriptionModifier>";

        private XmlNode DefaultConfig
        {
            get
            {
                XmlDocument defaultConfig = new XmlDocument();
                defaultConfig.LoadXml(_configDefault);
                return defaultConfig.FirstChild;
            }
        }
        private XmlNode ConfigEmpty
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configEmpty);
                return config.FirstChild;
            }
        }
        private XmlNode ConfigFull
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configFull);
                return config.FirstChild;
            }
        }

        [TestMethod]
        public void Test_DescriptionModifier_Initialize()
        {
            DNBLogger.Enable = false;

            DescriptionModifier modifier = new DescriptionModifier();
            modifier.Initialize(ConfigEmpty, DefaultConfig, CreateProject().Object);
            Assert.AreEqual(modifier.SourceColumns.Length, 0);
            Assert.AreEqual(modifier.TargetColumn, "TargetColumn");

            modifier = new DescriptionModifier();
            modifier.Initialize(ConfigFull, DefaultConfig, CreateProject().Object);
            Assert.AreEqual(modifier.SourceColumns.Length, 0);
            Assert.AreEqual(modifier.TargetColumn, "TargetColumn");
        }

        [TestMethod]
        public void Test_DescriptionModifier_DoStuff()
        {
            DNBLogger.Enable = false;

            DescriptionModifier modifier = new DescriptionModifier();
            modifier.Initialize(ConfigFull, DefaultConfig, CreateProject().Object);
            modifier.Pattern = "Pattern";
            DataFile datafile = CreateDataFile();
            datafile.Filename = "filename";
            DataFile datafileRetval = modifier.DoStuff(datafile);
            Assert.IsNotNull(datafileRetval);
            Assert.AreEqual("DescriptionModifier: DoStuff end (Blah)", DNBLogger.LastLog);
            Assert.AreEqual("SomeColumn;TargetColumn", datafileRetval.Header);
            Assert.AreEqual("Pattern", datafileRetval.Groups[0].DataRows[0].Cells["TargetColumn"]);

            modifier = new DescriptionModifier();
            modifier.Initialize(ConfigFull, DefaultConfig, CreateProject().Object);
            modifier.Pattern = "Pattern: ##PROJECT##";
            datafile = CreateDataFile();
            datafile.Filename = "filename";
            datafileRetval = modifier.DoStuff(datafile);
            Assert.IsNotNull(datafileRetval);
            Assert.AreEqual("DescriptionModifier: DoStuff end (Blah)", DNBLogger.LastLog);
            Assert.AreEqual("SomeColumn;TargetColumn", datafileRetval.Header);
            Assert.AreEqual("Pattern: Blah", datafileRetval.Groups[0].DataRows[0].Cells["TargetColumn"]);

            modifier = new DescriptionModifier();
            modifier.Initialize(ConfigFull, DefaultConfig, CreateProject().Object);
            modifier.Pattern = "Pattern: ##FILENAME##";
            datafile = CreateDataFile();
            datafile.Filename = "filename";
            datafileRetval = modifier.DoStuff(datafile);
            Assert.IsNotNull(datafileRetval);
            Assert.AreEqual("DescriptionModifier: DoStuff end (Blah)", DNBLogger.LastLog);
            Assert.AreEqual("SomeColumn;TargetColumn", datafileRetval.Header);
            Assert.AreEqual("Pattern: filename", datafileRetval.Groups[0].DataRows[0].Cells["TargetColumn"]);
        }

        private Mock<Project> CreateProject()
        {
            Mock<Project> oProject = new Mock<Project>();
            oProject.SetupAllProperties();
            oProject.Object.Name = "Blah";
            return oProject;
        }

        private DataFile CreateDataFile()
        {
            DataFile dataFile = new DataFile();
            dataFile.Header = "SomeColumn";
            dataFile.DataDelimiter = ';';
            dataFile.Filename = "datafile.csv";
            DataGroup group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            DataRow row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("SampleBCColumn", "1");
            row.Cells.Add("SomeColumn", "SomeData");
            return dataFile;
        }
    }
}
