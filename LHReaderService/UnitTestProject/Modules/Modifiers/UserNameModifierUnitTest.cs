﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using DNBReader;
using DNBReader.Modules;
using DNBReader.DataClasses;
using Moq;
using DNBTools.Nautilus;

namespace UnitTestProject.Modules.Modifiers
{
    [TestClass]
    public class UserNameModifierUnitTest
    {
        public string _configDefault = "<Defaults><Modifiers><UserNameModifier><SourceColumn>SourceColumn</SourceColumn><TargetColumn>TargetColumn</TargetColumn></UserNameModifier></Modifiers></Defaults>";
        public string _configEmpty = "<UserNameModifier></UserNameModifier>";
        public string _configFull = "<UserNameModifier><SourceColumn>SourceColumn</SourceColumn><TargetColumn>TargetColumn</TargetColumn></UserNameModifier>";

        private XmlNode DefaultConfig
        {
            get
            {
                XmlDocument defaultConfig = new XmlDocument();
                defaultConfig.LoadXml(_configDefault);
                return defaultConfig.FirstChild;
            }
        }
        private XmlNode ConfigEmpty
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configEmpty);
                return config.FirstChild;
            }
        }
        private XmlNode ConfigFull
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configFull);
                return config.FirstChild;
            }
        }

        [TestMethod]
        public void Test_UserNameModifier_Initialize()
        {
            DNBLogger.Enable = false;

            UserNameModifier modifier = new UserNameModifier();
            modifier.Initialize(ConfigEmpty, DefaultConfig, null);
            Assert.AreEqual("SourceColumn", modifier.SourceColumns[0]);
            Assert.AreEqual("TargetColumn", modifier.TargetColumn);

            modifier = new UserNameModifier();
            modifier.Initialize(ConfigFull, DefaultConfig, null);
            Assert.AreEqual("SourceColumn", modifier.SourceColumns[0]);
            Assert.AreEqual("TargetColumn", modifier.TargetColumn);
        }

        [TestMethod]
        public void Test_UserNameModifier_DoStuff()
        {
            DNBLogger.Enable = false;

            Mock<LimsCommunication> oLims = CreateLims(-1);
            Service.LimsCommunication = oLims.Object;
            UserNameModifier modifier = new UserNameModifier();
            modifier.Initialize(ConfigFull, DefaultConfig, CreateProject().Object);
            DataFile dataFile = CreateDataFile();
            DataFile retval = modifier.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("UserNameModifier: DoStuff end (Blah)", DNBLogger.LastLog);
            oLims.Verify(c => c.OperatorGetId("SourceColumn"), Times.Once);

            oLims = CreateLims(1);
            Service.LimsCommunication = oLims.Object;
            modifier = new UserNameModifier();
            modifier.Initialize(ConfigFull, DefaultConfig, CreateProject().Object);
            dataFile = CreateDataFile();
            retval = modifier.DoStuff(dataFile);
            Assert.IsNotNull(retval);
            Assert.AreEqual("UserNameModifier: DoStuff end (Blah)", DNBLogger.LastLog);
            Assert.IsTrue(retval.Header.Contains("TargetColumn"));
            oLims.Verify(c => c.OperatorGetId("SourceColumn"), Times.Once);
            Assert.AreEqual("a", retval.Groups[0].DataRows[0].Cells["TargetColumn"]);
        }

        private Mock<LimsCommunication> CreateLims(int times)
        {
            Mock<LimsCommunication> oLims = new Mock<LimsCommunication>();
            if (times == -1) { oLims.Setup(t => t.OperatorGetId(It.IsAny<string>())).Returns((string)null); }
            if (times == 1) { oLims.Setup(t => t.OperatorGetId(It.IsAny<string>())).Returns("a"); }
            return oLims;
        }

        private Mock<Project> CreateProject()
        {
            Mock<Project> oProject = new Mock<Project>();
            oProject.SetupAllProperties();
            oProject.Object.Name = "Blah";
            return oProject;
        }

        private DataFile CreateDataFile()
        {
            DataFile dataFile = new DataFile();
            dataFile.Header = "SourceColumn";
            dataFile.DataDelimiter = ';';
            DataGroup group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            DataRow row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("SourceColumn", "SourceColumn");
            return dataFile;
        }
    }
}
