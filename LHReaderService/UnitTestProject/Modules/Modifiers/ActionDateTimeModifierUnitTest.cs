﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using DNBReader;
using DNBReader.Modules;
using DNBReader.DataClasses;
using Moq;

namespace UnitTestProject.Modules.Modifiers
{
    [TestClass]
    public class ActionDateTimeModifierUnitTest
    {
        public string _configDefault = "<Defaults><Modifiers><ActionDateTimeModifier><ActionDateTimeColumn>ActionDateTimeColumn</ActionDateTimeColumn><VolumeColumn>VolumeColumn</VolumeColumn><Undefined>Undefined</Undefined></ActionDateTimeModifier></Modifiers></Defaults>";
        public string _configEmpty = "<ActionDateTimeModifier></ActionDateTimeModifier>";
        public string _configFull = "<ActionDateTimeModifier><ActionDateTimeColumn>ActionDateTimeColumn</ActionDateTimeColumn><VolumeColumn>VolumeColumn</VolumeColumn><Undefined>Undefined</Undefined></ActionDateTimeModifier>";

        private XmlNode DefaultConfig
        {
            get
            {
                XmlDocument defaultConfig = new XmlDocument();
                defaultConfig.LoadXml(_configDefault);
                return defaultConfig.FirstChild;
            }
        }
        private XmlNode ConfigEmpty
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configEmpty);
                return config.FirstChild;
            }
        }
        private XmlNode ConfigFull
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configFull);
                return config.FirstChild;
            }
        }

        [TestMethod]
        public void Test_ActionDateTimeModifier_Initialize()
        {
            DNBLogger.Enable = false;

            ActionDateTimeModifier modifier = new ActionDateTimeModifier();
            modifier.Initialize(ConfigEmpty, DefaultConfig, null);
            Assert.AreEqual(modifier.TargetColumn, "ActionDateTimeColumn");
            Assert.AreEqual(modifier.SourceColumns[0], "ActionDateTimeColumn");
            Assert.AreEqual(modifier.SourceColumns[1], "VolumeColumn");
            Assert.AreEqual(modifier.Undefined, "Undefined");

            modifier = new ActionDateTimeModifier();
            modifier.Initialize(ConfigFull, DefaultConfig, null);
            Assert.AreEqual(modifier.TargetColumn, "ActionDateTimeColumn");
            Assert.AreEqual(modifier.SourceColumns[0], "ActionDateTimeColumn");
            Assert.AreEqual(modifier.SourceColumns[1], "VolumeColumn");
            Assert.AreEqual(modifier.Undefined, "Undefined");
        }

        [TestMethod]
        public void Test_ActionDateTimeModifier_DoStuff()
        {
            DNBLogger.Enable = false;

            DataFile dataFile = new DataFile();
            DataGroup group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            DataRow row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("ActionDateTimeColumn", (DateTime.Today - new TimeSpan(1, 0, 0, 0)).ToString("dd-MM-yyyy HH:mm:ss"));
            row.Cells.Add("VolumeColumn", "0");
            row.Cells.Add("Undefined", "Not Undefined");
            ActionDateTimeModifier modifier = new ActionDateTimeModifier();
            modifier.Initialize(ConfigFull, DefaultConfig, CreateProject().Object);
            DataFile retval = modifier.DoStuff(dataFile);
            Assert.IsNotNull(retval);
            Assert.AreEqual("ActionDateTimeModifier: DoStuff end (Blah)", DNBLogger.LastLog);

            dataFile = new DataFile();
            group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("ActionDateTimeColumn", "Undefined");
            row.Cells.Add("VolumeColumn", "0");
            row.Cells.Add("Undefined", "Undefined");
            modifier = new ActionDateTimeModifier();
            modifier.Initialize(ConfigFull, DefaultConfig, CreateProject().Object);
            DataFile dataFileRetval = modifier.DoStuff(dataFile);
            Assert.IsNotNull(dataFileRetval);
            Assert.AreEqual("ActionDateTimeModifier: DoStuff end (Blah)", DNBLogger.LastLog);
            Assert.AreEqual(DateTime.Today.ToString("dd-MM-yyyy HH:mm:ss"), dataFileRetval.Groups[0].DataRows[0].Cells["ActionDateTimeColumn"]);

            dataFile = new DataFile();
            group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("ActionDateTimeColumn", "Undefined");
            row.Cells.Add("VolumeColumn", "10");
            row.Cells.Add("Undefined", "Undefined");
            modifier = new ActionDateTimeModifier();
            modifier.Initialize(ConfigFull, DefaultConfig, CreateProject().Object);
            retval = modifier.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("ActionDateTimeModifier: DoStuff end (Blah)", DNBLogger.LastLog);
        }

        private Mock<Project> CreateProject()
        {
            Mock<Project> oProject = new Mock<Project>();
            oProject.SetupAllProperties();
            oProject.Object.Name = "Blah";
            return oProject;
        }
    }
}
