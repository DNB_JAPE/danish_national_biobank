﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using DNBReader;
using DNBReader.Modules;
using DNBReader.DataClasses;
using Moq;
using System.Collections.Generic;

namespace UnitTestProject.Modules.Modifiers
{
    [TestClass]
    public class DescriptionFileModifierUnitTest
    {
        public string _configDefault = "<Defaults><Modifiers><DescriptionFileModifier><Dir>Dir</Dir><Pattern>Pattern</Pattern><Delimiter>;</Delimiter><SourceSampleBCColumn>SourceSampleBCColumn</SourceSampleBCColumn><DescriptionSampleBCColumn>DescriptionSampleBCColumn</DescriptionSampleBCColumn><DescriptionColumn>DescriptionColumn</DescriptionColumn></DescriptionFileModifier></Modifiers></Defaults>";
        public string _configEmpty = "<DescriptionFileModifier></DescriptionFileModifier>";
        public string _configFull = "<DescriptionFileModifier><Dir>Dir</Dir><Pattern>Pattern</Pattern><Delimiter>;</Delimiter><SourceSampleBCColumn>SourceSampleBCColumn</SourceSampleBCColumn><DescriptionSampleBCColumn>DescriptionSampleBCColumn</DescriptionSampleBCColumn><DescriptionColumn>DescriptionColumn</DescriptionColumn></DescriptionFileModifier>";

        private XmlNode DefaultConfig
        {
            get
            {
                XmlDocument defaultConfig = new XmlDocument();
                defaultConfig.LoadXml(_configDefault);
                return defaultConfig.FirstChild;
            }
        }
        private XmlNode ConfigEmpty
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configEmpty);
                return config.FirstChild;
            }
        }
        private XmlNode ConfigFull
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configFull);
                return config.FirstChild;
            }
        }

        [TestMethod]
        public void Test_DescriptionFileModifier_Initialize()
        {
            DNBLogger.Enable = false;

            DescriptionFileModifier filter = new DescriptionFileModifier();
            filter.Initialize(ConfigEmpty, DefaultConfig, null);
            Assert.AreEqual(filter.InputDir, "Dir");
            Assert.AreEqual(filter.Pattern, "Pattern");
            Assert.AreEqual(filter.FileDelimiter, ';');
            Assert.AreEqual(filter.DataSampleBCColumn, "SourceSampleBCColumn");
            Assert.AreEqual(filter.DescriptionSampleBCColumn, "DescriptionSampleBCColumn");
            Assert.AreEqual(filter.DescriptionColumn, "DescriptionColumn");

            filter = new DescriptionFileModifier();
            filter.Initialize(ConfigFull, DefaultConfig, null);
            Assert.AreEqual(filter.InputDir, "Dir");
            Assert.AreEqual(filter.Pattern, "Pattern");
            Assert.AreEqual(filter.FileDelimiter, ';');
            Assert.AreEqual(filter.DataSampleBCColumn, "SourceSampleBCColumn");
            Assert.AreEqual(filter.DescriptionSampleBCColumn, "DescriptionSampleBCColumn");
            Assert.AreEqual(filter.DescriptionColumn, "DescriptionColumn");
        }

        [TestMethod]
        [DeploymentItem(@"TestFiles/001.csv")]
        public void Test_DescriptionFileModifier_DoStuff()
        {
            DNBLogger.Enable = false;

            DescriptionFileModifier modifier = new DescriptionFileModifier();
            modifier.Project = CreateProject().Object;
            object retval = modifier.DoStuff(null);
            Assert.IsNull(retval);
            Assert.AreEqual("DescriptionFileModifier: Data is not defined", DNBLogger.LastLog);

            modifier = new DescriptionFileModifier();
            modifier.Project = CreateProject().Object;
            modifier.InputDir = "c:\\NONEXISTINGDIR";
            modifier.FileDelimiter = ';';
            DataFile dataFile = new DataFile();
            dataFile.Filename = "DescriptionFileModifierC001.csv";
            retval = modifier.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DescriptionFileModifier: Failed to read 001.csv", DNBLogger.LastLog);

            modifier = new DescriptionFileModifier();
            modifier.Project = CreateProject().Object;
            modifier.InputDir = ".";
            modifier.FileDelimiter = ';';
            modifier.Pattern = "Pattern";
            modifier.DescriptionColumn = "DescriptionColumn";
            dataFile = CreateDataFile();
            DataFile dataFileRetval = modifier.DoStuff(dataFile);
            Assert.IsNotNull(dataFileRetval);
            Assert.AreEqual("DescriptionFileModifier: DoStuff end", DNBLogger.LastLog);
            Assert.AreEqual("Pattern", dataFileRetval.Groups[0].DataRows[0].Cells["DescriptionColumn"]);
            Assert.AreEqual("SomeData", dataFileRetval.Groups[0].DataRows[0].Cells["SomeColumn"]);

            modifier = new DescriptionFileModifier();
            modifier.Project = CreateProject().Object;
            modifier.InputDir = ".";
            modifier.FileDelimiter = ';';
            modifier.Pattern = "Pattern: ##DESCFILENAME##";
            modifier.DescriptionColumn = "DescriptionColumn";
            dataFile = CreateDataFile();
            dataFileRetval = modifier.DoStuff(dataFile);
            Assert.IsNotNull(dataFileRetval);
            Assert.AreEqual("DescriptionFileModifier: DoStuff end", DNBLogger.LastLog);
            Assert.AreEqual("Pattern: 001.csv", dataFileRetval.Groups[0].DataRows[0].Cells["DescriptionColumn"]);
            Assert.AreEqual("SomeData", dataFileRetval.Groups[0].DataRows[0].Cells["SomeColumn"]);

            modifier = new DescriptionFileModifier();
            modifier.Project = CreateProject().Object;
            modifier.InputDir = ".";
            modifier.FileDelimiter = ';';
            modifier.Pattern = "Pattern: ##Column1##";
            modifier.DescriptionColumn = "DescriptionColumn";
            modifier.DataSampleBCColumn = "SampleBCColumn";
            modifier.DescriptionSampleBCColumn = "SampleBCColumn";
            dataFile = CreateDataFile();
            dataFileRetval = modifier.DoStuff(dataFile);
            Assert.IsNotNull(dataFileRetval);
            Assert.AreEqual("DescriptionFileModifier: DoStuff end", DNBLogger.LastLog);
            Assert.AreEqual("Pattern: Row1C1", dataFileRetval.Groups[0].DataRows[0].Cells["DescriptionColumn"]);
            Assert.AreEqual("SomeData", dataFileRetval.Groups[0].DataRows[0].Cells["SomeColumn"]);
            Assert.AreEqual("Pattern: ", dataFileRetval.Groups[0].DataRows[1].Cells["DescriptionColumn"]);
        }

        private Mock<Project> CreateProject()
        {
            Mock<Project> oProject = new Mock<Project>();
            oProject.SetupAllProperties();
            oProject.Object.Name = "Blah";
            oProject.Object.DataFiles = new List<string>();
            return oProject;
        }

        private DataFile CreateDataFile()
        {
            DataFile dataFile = new DataFile();
            dataFile.Header = "SampleBCColumn;SomeColumn";
            dataFile.DataDelimiter = ';';
            dataFile.Filename = "DescriptionFileModifierC001.csv";
            DataGroup group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            DataRow row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("SampleBCColumn", "1");
            row.Cells.Add("SomeColumn", "SomeData");
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("SampleBCColumn", "2");
            row.Cells.Add("SomeColumn", "SomeData");
            return dataFile;
        }
    }
}
