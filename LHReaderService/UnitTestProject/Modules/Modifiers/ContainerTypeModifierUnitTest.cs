﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using DNBReader;
using DNBReader.Modules;
using DNBTools.Nautilus;
using Moq;
using DNBReader.DataClasses;

namespace UnitTestProject.Modules.Modifiers
{
    [TestClass]
    public class ContainerTypeModifierUnitTest
    {
        public string _configDefault = "<Defaults><Modifiers><ContainerTypeModifier><SourceColumn>SourceColumn</SourceColumn><TargetColumn>TargetColumn</TargetColumn></ContainerTypeModifier></Modifiers></Defaults>";
        public string _configEmpty = "<ContainerTypeModifier></ContainerTypeModifier>";
        public string _configFull = "<ContainerTypeModifier><SourceColumn>SourceColumn</SourceColumn><TargetColumn>TargetColumn</TargetColumn></ContainerTypeModifier>";

        private XmlNode DefaultConfig
        {
            get
            {
                XmlDocument defaultConfig = new XmlDocument();
                defaultConfig.LoadXml(_configDefault);
                return defaultConfig.FirstChild;
            }
        }
        private XmlNode ConfigEmpty
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configEmpty);
                return config.FirstChild;
            }
        }
        private XmlNode ConfigFull
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configFull);
                return config.FirstChild;
            }
        }

        [TestMethod]
        public void Test_ContainerTypeModifier_Initialize()
        {
            DNBLogger.Enable = false;

            ContainerTypeModifier modifier = new ContainerTypeModifier();
            modifier.Initialize(ConfigEmpty, DefaultConfig, CreateProject().Object);
            Assert.AreEqual(modifier.SourceColumns[0], "SourceColumn");
            Assert.AreEqual(modifier.TargetColumn, "TargetColumn");

            modifier = new ContainerTypeModifier();
            modifier.Initialize(ConfigFull, DefaultConfig, CreateProject().Object);
            Assert.AreEqual(modifier.SourceColumns[0], "SourceColumn");
            Assert.AreEqual(modifier.TargetColumn, "TargetColumn");
        }

        [TestMethod]
        public void Test_ContainerTypeModifier_DoStuff()
        {
            DNBLogger.Enable = false;

            Mock<LimsCommunication> oLims = CreateLims(-1);
            Service.LimsCommunication = oLims.Object;
            ContainerTypeModifier modifier = new ContainerTypeModifier();
            modifier.Initialize(ConfigFull, DefaultConfig, CreateProject().Object);
            DataFile dataFile = CreateDataFile();
            DataFile retval = modifier.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("ContainerTypeModifier: DoStuff end (Blah)", DNBLogger.LastLog);
            oLims.Verify(c => c.TubeTypeGetId("SourceColumn"), Times.Once);

            oLims = CreateLims(1);
            Service.LimsCommunication = oLims.Object;
            modifier = new ContainerTypeModifier();
            modifier.Initialize(ConfigFull, DefaultConfig, CreateProject().Object);
            dataFile = CreateDataFile();
            retval = modifier.DoStuff(dataFile);
            Assert.IsNotNull(retval);
            Assert.AreEqual("ContainerTypeModifier: DoStuff end (Blah)", DNBLogger.LastLog);
            Assert.IsTrue(retval.Header.Contains("TargetColumn"));
            oLims.Verify(c => c.TubeTypeGetId("SourceColumn"), Times.Once);
            Assert.AreEqual("a", retval.Groups[0].DataRows[0].Cells["TargetColumn"]);
        }

        private Mock<LimsCommunication> CreateLims(int times)
        {
            Mock<LimsCommunication> oLims = new Mock<LimsCommunication>();
            if (times == -1) { oLims.Setup(t => t.TubeTypeGetId(It.IsAny<string>())).Returns((string)null); }
            if (times == 1) { oLims.Setup(t => t.TubeTypeGetId(It.IsAny<string>())).Returns("a"); }
            return oLims;
        }

        private Mock<Project> CreateProject() 
        {
            Mock<Project> oProject = new Mock<Project>();
            oProject.SetupAllProperties();
            oProject.Object.Name = "Blah";
            return oProject;
        }

        private DataFile CreateDataFile()
        {
            DataFile dataFile = new DataFile();
            dataFile.Header = "SourceColumn";
            dataFile.DataDelimiter = ';';
            DataGroup group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            DataRow row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("SourceColumn", "SourceColumn");
            return dataFile;
        }
    }
}
