﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using DNBReader;
using DNBReader.Modules;
using DNBReader.DataClasses;
using DNBTools.Nautilus;
using Moq;
using System.Collections.Generic;
using System.IO;

namespace UnitTestProject.Modules.Writers
{
    [TestClass]
    public class TestLoginFileWriterUnitTest
    {
        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        public string _configDefault = "<Defaults><Writers><TestLoginFileWriter><Dir>Dir</Dir><TmpDir>TmpDir</TmpDir><LookupMasterSql>LookupMasterSql</LookupMasterSql><WaitInterval>10</WaitInterval><WaitCount>60</WaitCount><TestWorkflow>TestWorkflow</TestWorkflow><TargetPlateBCColumn>TargetPlateBCColumn</TargetPlateBCColumn><SourceSampleBCColumn>SourceSampleBCColumn</SourceSampleBCColumn><SourcePlateBCColumn>SourcePlateBCColumn</SourcePlateBCColumn><DescriptionColumn>DescriptionColumn</DescriptionColumn></TestLoginFileWriter></Writers></Defaults>";
        public string _configEmpty = "<TestLoginFileWriter></TestLoginFileWriter>";
        public string _configFull = "<TestLoginFileWriter><Dir>Dir</Dir><TmpDir>TmpDir</TmpDir><LookupMasterSql>LookupMasterSql</LookupMasterSql><WaitInterval>10</WaitInterval><WaitCount>60</WaitCount><TestWorkflow>TestWorkflow</TestWorkflow><TargetPlateBCColumn>TargetPlateBCColumn</TargetPlateBCColumn><SourceSampleBCColumn>SourceSampleBCColumn</SourceSampleBCColumn><SourcePlateBCColumn>SourcePlateBCColumn</SourcePlateBCColumn><DescriptionColumn>DescriptionColumn</DescriptionColumn></TestLoginFileWriter>";

        private XmlNode DefaultConfig
        {
            get
            {
                XmlDocument defaultConfig = new XmlDocument();
                defaultConfig.LoadXml(_configDefault);
                return defaultConfig.FirstChild;
            }
        }
        private XmlNode ConfigEmpty
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configEmpty);
                return config.FirstChild;
            }
        }
        private XmlNode ConfigFull
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configFull);
                return config.FirstChild;
            }
        }

        [TestMethod]
        public void Test_TestLoginFileWriter_Initialize()
        {
            DNBLogger.Enable = false;

            TestLoginFileWriter writer = new TestLoginFileWriter();
            writer.Initialize(ConfigEmpty, DefaultConfig, null);
            Assert.AreEqual(writer.Dir, "Dir");
            Assert.AreEqual(writer.TmpDir, "TmpDir");
            Assert.AreEqual(writer.LookupMasterSql, "LookupMasterSql");
            Assert.AreEqual(writer.WaitInterval, 10);
            Assert.AreEqual(writer.WaitCount, 60);
            Assert.AreEqual(writer.TestWorkflow, "TestWorkflow");
            Assert.AreEqual(writer.TargetPlateBCColumn, "TargetPlateBCColumn");
            Assert.AreEqual(writer.SourceSampleBCColumn, "SourceSampleBCColumn");
            Assert.AreEqual(writer.SourcePlateBCColumn, "SourcePlateBCColumn");
            Assert.AreEqual(writer.DescriptionColumn, "DescriptionColumn");

            writer = new TestLoginFileWriter();
            writer.Initialize(ConfigFull, DefaultConfig, null);
            Assert.AreEqual(writer.Dir, "Dir");
            Assert.AreEqual(writer.TmpDir, "TmpDir");
            Assert.AreEqual(writer.LookupMasterSql, "LookupMasterSql");
            Assert.AreEqual(writer.WaitInterval, 10);
            Assert.AreEqual(writer.WaitCount, 60);
            Assert.AreEqual(writer.TestWorkflow, "TestWorkflow");
            Assert.AreEqual(writer.TargetPlateBCColumn, "TargetPlateBCColumn");
            Assert.AreEqual(writer.SourceSampleBCColumn, "SourceSampleBCColumn");
            Assert.AreEqual(writer.SourcePlateBCColumn, "SourcePlateBCColumn");
            Assert.AreEqual(writer.DescriptionColumn, "DescriptionColumn");
        }

        [TestMethod]
        public void Test_TestLoginFileWriter_DoStuff()
        {
            DNBLogger.Enable = false;

            TestLoginFileWriter writer = new TestLoginFileWriter();
            object retval = writer.DoStuff(null);
            Assert.IsNull(retval);
            Assert.AreEqual("TestLoginFileWriter: Data is not defined", DNBLogger.LastLog);

            writer = new TestLoginFileWriter();
            DataFile datafile = new DataFile();
            datafile.Filename = "filename";
            DataFile datafileRetval = writer.DoStuff(datafile);
            Assert.IsNotNull(datafileRetval);
            Assert.AreEqual("TestLoginFileWriter: No plates in data file: filename", DNBLogger.LastLog);

            Mock<LimsCommunication> oLims = CreateLims(-1);
            Service.LimsCommunication = oLims.Object;
            writer = new TestLoginFileWriter();
            writer.TargetPlateBCColumn = "TargetPlateBCColumn";
            writer.SourceSampleBCColumn = "SourceSampleBCColumn";
            writer.SourcePlateBCColumn = "SourcePlateBCColumn";
            writer.DescriptionColumn = "DescriptionColumn";
            writer.LookupMasterSql = "Sql";
            retval = writer.DoStuff(CreateDataFile());
            Assert.IsNull(retval);
            Assert.AreEqual("TestLoginFileWriter: Failed to create aliquot file: Failed to look up aliquot id: Sql", DNBLogger.LastLog);

            oLims = CreateLims(0);
            Service.LimsCommunication = oLims.Object;
            writer = new TestLoginFileWriter();
            writer.TargetPlateBCColumn = "TargetPlateBCColumn";
            writer.SourceSampleBCColumn = "SourceSampleBCColumn";
            writer.SourcePlateBCColumn = "SourcePlateBCColumn";
            writer.DescriptionColumn = "DescriptionColumn";
            writer.LookupMasterSql = "Sql";
            retval = writer.DoStuff(CreateDataFile());
            Assert.IsNull(retval);
            Assert.AreEqual("TestLoginFileWriter: Wrong number of aliquots found (0)", DNBLogger.LastLog);

            oLims = CreateLims(1);
            Service.LimsCommunication = oLims.Object;
            writer = new TestLoginFileWriter();
            writer.TargetPlateBCColumn = "TargetPlateBCColumn";
            writer.SourceSampleBCColumn = "SourceSampleBCColumn";
            writer.SourcePlateBCColumn = "SourcePlateBCColumn";
            writer.DescriptionColumn = "DescriptionColumn";
            writer.LookupMasterSql = "Sql: ##MASTERDESCRIPTION##, ##SOURCEALIQUOTBC##, ##SOURCEPLATEBC##";
            writer.WaitCount = 1;
            writer.WaitInterval = 1;
            writer.TmpDir = TestContext.DeploymentDirectory;
            writer.Dir = TestContext.DeploymentDirectory;
            writer.TestWorkflow = "Workflow";
            retval = writer.DoStuff(CreateDataFile());
            Assert.IsNull(retval);
            Assert.AreEqual("TestLoginFileWriter: Done waiting, but file is still present: TestLoginFileWriter_(TargetPlateBCColumn).csv", DNBLogger.LastLog);
            oLims.Verify(c => c.SampleGetIdsFromSql("Sql: DescriptionColumn, SourceSampleBCColumn, SourcePlateBCColumn"), Times.Once);
            string outputFile = Directory.GetFiles(TestContext.DeploymentDirectory, "TestLoginFileWriter_(*.csv")[0];
            TestContext.AddResultFile(outputFile);
            Assert.AreEqual(          "Begin Aliquot" + 
                Environment.NewLine + "\"External Ref\",\"Aliquot Id\",\"Sample Ref\",\"Description\"" + 
                Environment.NewLine + ",\"a\",," + 
                Environment.NewLine + ",\"aliquot_id\",," + 
                Environment.NewLine + "End Aliquot" + 
                Environment.NewLine + 
                Environment.NewLine + "Begin Test" + 
                Environment.NewLine + "\"External Ref\",\"Workflow Name\",\"Aliquot Id\",\"Description\"" +
                Environment.NewLine + ",\"Workflow\",\"a\"," +
                Environment.NewLine + ",\"Workflow\",\"aliquot_id\"," + 
                Environment.NewLine + "End Test" + 
                Environment.NewLine, 
                File.ReadAllText(outputFile));
        }

        private Mock<LimsCommunication> CreateLims(int count)
        {
            Mock<LimsCommunication> oLims = new Mock<LimsCommunication>();
            if (count == -1) { oLims.Setup(t => t.SampleGetIdsFromSql(It.IsAny<string>())).Returns((List<string>)null); }
            else if (count == 0) { oLims.Setup(t => t.SampleGetIdsFromSql(It.IsAny<string>())).Returns(new List<string>()); }
            else { oLims.Setup(t => t.SampleGetIdsFromSql(It.IsAny<string>())).Returns(new List<string>(){"a"}); }
            return oLims;
        }

        private DataFile CreateDataFile()
        {
            DataFile dataFile = new DataFile();
            dataFile.Header = "TargetPlateBCColumn;SourceSampleBCColumn;SourcePlateBCColumn;DescriptionColumn";
            dataFile.DataDelimiter = ';';
            dataFile.Filename = "TestLoginFileWriter.csv";
            DataGroup group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            DataRow row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("TargetPlateBCColumn", "TargetPlateBCColumn");
            row.Cells.Add("SourceSampleBCColumn", "SourceSampleBCColumn");
            row.Cells.Add("SourcePlateBCColumn", "SourcePlateBCColumn");
            row.Cells.Add("DescriptionColumn", "DescriptionColumn");
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("TargetPlateBCColumn", "TargetPlateBCColumn");
            row.Cells.Add("SourceSampleBCColumn", "ALIQUOT_ID:aliquot_id");
            row.Cells.Add("SourcePlateBCColumn", "SourcePlateBCColumn1");
            row.Cells.Add("DescriptionColumn", "DescriptionColumn1");
            return dataFile;
        }
    }
}
