﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using DNBReader;
using DNBReader.Modules;
using DNBReader.DataClasses;
using System.IO;

namespace UnitTestProject.Modules.Writers
{
    [TestClass]
    public class MasterFileWriterUnitTest
    {
        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        public string _configDefault = "<Defaults><Writers><MasterFileWriter><Dir>Dir</Dir><TmpDir>TmpDir</TmpDir><MasterWorkflow>MasterWorkflow</MasterWorkflow><MasterSampleType>MasterSampleType</MasterSampleType><WaitInterval>10</WaitInterval><WaitCount>60</WaitCount><UserNameColumn>UserNameColumn</UserNameColumn><SourceSampleBCColumn>SourceSampleBCColumn</SourceSampleBCColumn><SourcePlateBCColumn>SourcePlateBCColumn</SourcePlateBCColumn><TargetPlateBCColumn>TargetPlateBCColumn</TargetPlateBCColumn><InstrumentColumn>InstrumentColumn</InstrumentColumn><ProjectColumn>ProjectColumn</ProjectColumn><ActionDateTimeColumn>ActionDateTimeColumn</ActionDateTimeColumn><MasterContainerTypeColumn>MasterContainerTypeColumn</MasterContainerTypeColumn><LocationColumn>LocationColumn</LocationColumn><DescriptionColumn>DescriptionColumn</DescriptionColumn></MasterFileWriter></Writers></Defaults>";
        public string _configEmpty = "<MasterFileWriter></MasterFileWriter>";
        public string _configFull = "<MasterFileWriter><Dir>Dir</Dir><TmpDir>TmpDir</TmpDir><MasterWorkflow>MasterWorkflow</MasterWorkflow><MasterSampleType>MasterSampleType</MasterSampleType><WaitInterval>10</WaitInterval><WaitCount>60</WaitCount><UserNameColumn>UserNameColumn</UserNameColumn><SourceSampleBCColumn>SourceSampleBCColumn</SourceSampleBCColumn><SourcePlateBCColumn>SourcePlateBCColumn</SourcePlateBCColumn><TargetPlateBCColumn>TargetPlateBCColumn</TargetPlateBCColumn><InstrumentColumn>InstrumentColumn</InstrumentColumn><ProjectColumn>ProjectColumn</ProjectColumn><ActionDateTimeColumn>ActionDateTimeColumn</ActionDateTimeColumn><MasterContainerTypeColumn>MasterContainerTypeColumn</MasterContainerTypeColumn><LocationColumn>LocationColumn</LocationColumn><DescriptionColumn>DescriptionColumn</DescriptionColumn></MasterFileWriter>";

        private XmlNode DefaultConfig
        {
            get
            {
                XmlDocument defaultConfig = new XmlDocument();
                defaultConfig.LoadXml(_configDefault);
                return defaultConfig.FirstChild;
            }
        }
        private XmlNode ConfigEmpty
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configEmpty);
                return config.FirstChild;
            }
        }
        private XmlNode ConfigFull
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configFull);
                return config.FirstChild;
            }
        }

        [TestMethod]
        public void Test_MasterFileWriter_Initialize()
        {
            DNBLogger.Enable = false;

            MasterFileWriter writer = new MasterFileWriter();
            writer.Initialize(ConfigEmpty, DefaultConfig, null);
            Assert.AreEqual(writer.Dir, "Dir");
            Assert.AreEqual(writer.TmpDir, "TmpDir");
            Assert.AreEqual(writer.MasterWorkflow, "MasterWorkflow");
            Assert.AreEqual(writer.MasterSampleType, "MasterSampleType");
            Assert.AreEqual(writer.WaitInterval, 10);
            Assert.AreEqual(writer.WaitCount, 60);
            Assert.AreEqual(writer.UserNameColumn, "UserNameColumn");
            Assert.AreEqual(writer.SourceSampleBCColumn, "SourceSampleBCColumn");
            Assert.AreEqual(writer.SourcePlateBCColumn, "SourcePlateBCColumn");
            Assert.AreEqual(writer.RobotColumn, "InstrumentColumn");
            Assert.AreEqual(writer.ActionDateTimeColumn, "ActionDateTimeColumn");
            Assert.AreEqual(writer.MasterContainerTypeColumn, "MasterContainerTypeColumn");
            Assert.AreEqual(writer.LocationColumn, "LocationColumn");
            Assert.AreEqual(writer.DescriptionColumn, "DescriptionColumn");

            writer = new MasterFileWriter();
            writer.Initialize(ConfigFull, DefaultConfig, null);
            Assert.AreEqual(writer.Dir, "Dir");
            Assert.AreEqual(writer.TmpDir, "TmpDir");
            Assert.AreEqual(writer.MasterWorkflow, "MasterWorkflow");
            Assert.AreEqual(writer.MasterSampleType, "MasterSampleType");
            Assert.AreEqual(writer.WaitInterval, 10);
            Assert.AreEqual(writer.WaitCount, 60);
            Assert.AreEqual(writer.UserNameColumn, "UserNameColumn");
            Assert.AreEqual(writer.SourceSampleBCColumn, "SourceSampleBCColumn");
            Assert.AreEqual(writer.SourcePlateBCColumn, "SourcePlateBCColumn");
            Assert.AreEqual(writer.RobotColumn, "InstrumentColumn");
            Assert.AreEqual(writer.ActionDateTimeColumn, "ActionDateTimeColumn");
            Assert.AreEqual(writer.MasterContainerTypeColumn, "MasterContainerTypeColumn");
            Assert.AreEqual(writer.LocationColumn, "LocationColumn");
            Assert.AreEqual(writer.DescriptionColumn, "DescriptionColumn");
        }

        [TestMethod]
        public void Test_MasterFileWriter_DoStuff()
        {
            DNBLogger.Enable = false;

            MasterFileWriter writer = new MasterFileWriter();
            object retval = writer.DoStuff(null);
            Assert.IsNull(retval);
            Assert.AreEqual("MasterFileWriter: Data is not defined", DNBLogger.LastLog);

            writer = new MasterFileWriter();
            DataFile datafile = new DataFile();
            datafile.Filename = "filename";
            DataFile datafileRetval = writer.DoStuff(datafile);
            Assert.IsNotNull(datafileRetval);
            Assert.AreEqual("MasterFileWriter: No plates in data file: filename", DNBLogger.LastLog);

            writer = new MasterFileWriter();
            datafile = new DataFile();
            datafile.Filename = "filename";
            DataGroup datagroup = new DataGroup(datafile);
            datafile.Groups.Add(datagroup);
            datafileRetval = writer.DoStuff(datafile);
            Assert.IsNotNull(datafileRetval);
            Assert.AreEqual("MasterFileWriter: Done", DNBLogger.LastLog);
            string[] outputFiles = Directory.GetFiles(TestContext.DeploymentDirectory, "filename_*.csv");
            Assert.AreEqual(0, outputFiles.Length);

            writer = new MasterFileWriter();
            datafile = new DataFile();
            datafile.Filename = "filenameDW";
            datagroup = new DataGroup(datafile);
            datafile.Groups.Add(datagroup);
            DataRow dataRow = new DataRow(datagroup);
            dataRow.Cells.Add("SourceSampleBCColumn", "DWnoget");
            datafileRetval = writer.DoStuff(datafile);
            Assert.IsNotNull(datafileRetval);
            Assert.AreEqual("MasterFileWriter: Done", DNBLogger.LastLog);
            outputFiles = Directory.GetFiles(TestContext.DeploymentDirectory, "filenameDW_*.csv");
            Assert.AreEqual(0, outputFiles.Length);

            writer = new MasterFileWriter();
            writer.UserNameColumn = "UserNameColumn";
            writer.SourceSampleBCColumn = "SourceSampleBCColumn";
            writer.SourcePlateBCColumn = "SourcePlateBCColumn";
            writer.TargetPlateBCColumn = "TargetPlateBCColumn";
            writer.RobotColumn = "RobotColumn";
            writer.ProjectColumn = "ProjectColumn";
            writer.ActionDateTimeColumn = "ActionDateTimeColumn";
            writer.MasterContainerTypeColumn = "MasterContainerTypeColumn";
            writer.LocationColumn = "LocationColumn";
            writer.DescriptionColumn = "DescriptionColumn";
            writer.WaitCount = 1;
            writer.WaitInterval = 1;
            writer.TmpDir = TestContext.DeploymentDirectory;
            writer.Dir = TestContext.DeploymentDirectory;
            writer.MasterWorkflow = "Workflow";
            writer.MasterSampleType = "MasterSampleType";
            datafile = CreateDataFile();
            datafileRetval = writer.DoStuff(datafile);
            Assert.IsNull(datafileRetval);
            Assert.AreEqual("MasterFileWriter: Done waiting, but file is still present: MasterFileWriter_(TargetPlateBCColumn).csv", DNBLogger.LastLog);
            outputFiles = Directory.GetFiles(TestContext.DeploymentDirectory, "MasterFileWriter*.csv");
            Assert.AreEqual(1, outputFiles.Length);
            TestContext.AddResultFile(outputFiles[0]);
            Assert.AreEqual("Begin Aliquot" +
                Environment.NewLine + "\"External_Ref\",\"Workflow_Name\",\"Sample_Ref\",\"Description\",\"U_Log_Robot\",\"U_Projectnr\",\"Container_Type_Id\",\"Matrix_Type\",\"Operator_Id\",\"U_ActionDateTime\",\"Location_Id\"" +
                Environment.NewLine + "\"SourceSampleBCColumn\",\"Workflow\",\"\",\"DescriptionColumn\",\"RobotColumn\",\"ProjectColumn\",\"MasterContainerTypeColumn\",\"MasterSampleType\",\"UserNameColumn\",\"ActionDateTimeColumn\",\"LocationColumn\"" +
                Environment.NewLine + "\"SourceSampleBCColumn1\",\"Workflow\",\"\",\"DescriptionColumn1\",\"RobotColumn1\",\"ProjectColumn1\",\"MasterContainerTypeColumn1\",\"MasterSampleType\",\"UserNameColumn1\",\"ActionDateTimeColumn1\",\"LocationColumn1\"" +
                Environment.NewLine + "End Aliquot" + Environment.NewLine,
                File.ReadAllText(outputFiles[0]));
        }

        private DataFile CreateDataFile()
        {
            DataFile dataFile = new DataFile();
            dataFile.Header = "UserNameColumn;SourceSampleBCColumn;SourcePlateBCColumn;TargetPlateBCColumn;RobotColumn;ProjectColumn;ActionDateTimeColumn;MasterContainerTypeColumn;LocationColumn;DescriptionColumn;CreateMaster";
            dataFile.DataDelimiter = ';';
            dataFile.Filename = "MasterFileWriter.csv";
            DataGroup group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            DataRow row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("UserNameColumn", "UserNameColumn");
            row.Cells.Add("SourceSampleBCColumn", "SourceSampleBCColumn");
            row.Cells.Add("SourcePlateBCColumn", "SourcePlateBCColumn");
            row.Cells.Add("TargetPlateBCColumn", "TargetPlateBCColumn");
            row.Cells.Add("RobotColumn", "RobotColumn");
            row.Cells.Add("ProjectColumn", "ProjectColumn");
            row.Cells.Add("ActionDateTimeColumn", "ActionDateTimeColumn");
            row.Cells.Add("MasterContainerTypeColumn", "MasterContainerTypeColumn");
            row.Cells.Add("LocationColumn", "LocationColumn");
            row.Cells.Add("DescriptionColumn", "DescriptionColumn");
            row.Cells.Add("CreateMaster", "true");
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("UserNameColumn", "UserNameColumn");
            row.Cells.Add("SourceSampleBCColumn", "SourceSampleBCColumn");
            row.Cells.Add("SourcePlateBCColumn", "SourcePlateBCColumn");
            row.Cells.Add("TargetPlateBCColumn", "TargetPlateBCColumn");
            row.Cells.Add("RobotColumn", "RobotColumn");
            row.Cells.Add("ProjectColumn", "ProjectColumn");
            row.Cells.Add("ActionDateTimeColumn", "ActionDateTimeColumn");
            row.Cells.Add("MasterContainerTypeColumn", "MasterContainerTypeColumn");
            row.Cells.Add("LocationColumn", "LocationColumn");
            row.Cells.Add("DescriptionColumn", "DescriptionColumn");
            row.Cells.Add("CreateMaster", "true");
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("UserNameColumn", "UserNameColumn1");
            row.Cells.Add("SourceSampleBCColumn", "SourceSampleBCColumn1");
            row.Cells.Add("SourcePlateBCColumn", "SourcePlateBCColumn");
            row.Cells.Add("TargetPlateBCColumn", "TargetPlateBCColumn");
            row.Cells.Add("RobotColumn", "RobotColumn1");
            row.Cells.Add("ProjectColumn", "ProjectColumn1");
            row.Cells.Add("ActionDateTimeColumn", "ActionDateTimeColumn1");
            row.Cells.Add("MasterContainerTypeColumn", "MasterContainerTypeColumn1");
            row.Cells.Add("LocationColumn", "LocationColumn1");
            row.Cells.Add("DescriptionColumn", "DescriptionColumn1");
            row.Cells.Add("CreateMaster", "true");
            return dataFile;
        }
    }
}
