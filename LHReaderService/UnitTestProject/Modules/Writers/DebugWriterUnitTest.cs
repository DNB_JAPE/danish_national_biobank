﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using DNBReader;
using DNBReader.Modules;
using DNBReader.DataClasses;
using System.IO;
using Moq;

namespace UnitTestProject.Modules.Writers
{
    [TestClass]
    public class DebugWriterUnitTest
    {
        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        public string _configDefault = "<Defaults><Writers><DebugWriter><Dir>DirectoryDefault</Dir></DebugWriter></Writers></Defaults>";
        public string _configEmpty = "<DebugWriter></DebugWriter>";
        public string _configFull = "<DebugWriter><Dir>Directory</Dir></DebugWriter>";

        private XmlNode DefaultConfig
        {
            get
            {
                XmlDocument defaultConfig = new XmlDocument();
                defaultConfig.LoadXml(_configDefault);
                return defaultConfig.FirstChild;
            }
        }
        private XmlNode ConfigEmpty
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configEmpty);
                return config.FirstChild;
            }
        }
        private XmlNode ConfigFull
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configFull);
                return config.FirstChild;
            }
        }

        [TestMethod]
        public void Test_DebugWriter_Initialize()
        {
            DNBLogger.Enable = false;

            DebugWriter writer = new DebugWriter();
            writer.Initialize(ConfigEmpty, DefaultConfig, null);
            Assert.AreEqual(writer.Dir, "DirectoryDefault");

            writer = new DebugWriter();
            writer.Initialize(ConfigFull, DefaultConfig, null);
            Assert.AreEqual(writer.Dir, "Directory");
        }

        [TestMethod]
        public void Test_DebugWriter_DoStuff()
        {
            DNBLogger.Enable = false;

            DebugWriter writer = new DebugWriter();
            writer.Project = CreateProject().Object;
            object retval = writer.DoStuff(null);
            Assert.IsNull(retval);
            Assert.AreEqual("DebugWriter: Data is not defined", DNBLogger.LastLog);

            writer = new DebugWriter();
            writer.Project = CreateProject().Object;
            writer.Dir = "c:\\NONEXISTINGDIR";
            retval = writer.DoStuff(new DataFile());
            Assert.IsNull(retval);
            Assert.AreEqual("DebugWriter: Directory does not exist: c:\\NONEXISTINGDIR", DNBLogger.LastLog);

            writer = new DebugWriter();
            writer.Project = CreateProject().Object;
            writer.Dir = TestContext.DeploymentDirectory;
            retval = writer.DoStuff(new DataFile());
            Assert.IsNotNull(retval);
            Assert.AreEqual("DebugWriter: DoStuff end", DNBLogger.LastLog);

            writer = new DebugWriter();
            writer.Project = CreateProject().Object;
            writer.Dir = TestContext.DeploymentDirectory;
            DataFile dataFile = new DataFile();
            dataFile.DataDelimiter = ';';
            dataFile.Header = "Column1;Column2";
            dataFile.Groups.Add(new DataGroup(dataFile));
            dataFile.Groups[0].DataRows.Add(new DataRow(dataFile.Groups[0]));
            dataFile.Groups[0].DataRows[0].Cells.Add("Column1", "Row1Column1Group1");
            dataFile.Groups[0].DataRows[0].Cells.Add("Column2", "Row1Column2Group1");
            dataFile.Groups.Add(new DataGroup(dataFile));
            dataFile.Groups[1].DataRows.Add(new DataRow(dataFile.Groups[0]));
            dataFile.Groups[1].DataRows[0].Cells.Add("Column1", "Row1Column1Group2");
            dataFile.Groups[1].DataRows[0].Cells.Add("Column2", "Row1Column2Group2");
            retval = writer.DoStuff(dataFile);
            Assert.IsNotNull(retval);
            Assert.AreEqual("DebugWriter: DoStuff end", DNBLogger.LastLog);
            string outputFile = Directory.GetFiles(TestContext.DeploymentDirectory, "Blah_Raw_*.csv")[0];
            TestContext.AddResultFile(outputFile);
            Assert.AreEqual("Column1;Column2" + Environment.NewLine + "Row1Column1Group1;Row1Column2Group1" + Environment.NewLine + "Row1Column1Group2;Row1Column2Group2" + Environment.NewLine, File.ReadAllText(outputFile));
        }

        private Mock<Project> CreateProject()
        {
            Mock<Project> oProject = new Mock<Project>();
            oProject.SetupAllProperties();
            oProject.Object.Name = "Blah";
            return oProject;
        }
    }
}
