﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using DNBReader;
using DNBReader.Modules;
using Moq;
using DNBTools.Nautilus;
using DNBReader.DataClasses;
using System.Collections.Generic;

namespace UnitTestProject.Modules.Writers
{
    [TestClass]
    public class DisposeSamplesBCWriterUnitTest
    {
        public string _configDefault = "<Defaults><Writers><DisposeSamplesBCWriter><SampleBCColumn>SampleBCColumn</SampleBCColumn><UserNameColumn>UserNameColumn</UserNameColumn><ActionDateTimeColumn>ActionDateTimeColumn</ActionDateTimeColumn><CommentColumn>CommentColumn</CommentColumn><Sql>Sql</Sql></DisposeSamplesBCWriter></Writers></Defaults>";
        public string _configEmpty = "<DisposeSamplesBCWriter></DisposeSamplesBCWriter>";
        public string _configFull = "<DisposeSamplesBCWriter><SampleBCColumn>SampleBCColumn</SampleBCColumn><UserNameColumn>UserNameColumn</UserNameColumn><ActionDateTimeColumn>ActionDateTimeColumn</ActionDateTimeColumn><CommentColumn>CommentColumn</CommentColumn><Sql>Sql</Sql></DisposeSamplesBCWriter>";

        private XmlNode DefaultConfig
        {
            get
            {
                XmlDocument defaultConfig = new XmlDocument();
                defaultConfig.LoadXml(_configDefault);
                return defaultConfig.FirstChild;
            }
        }
        private XmlNode ConfigEmpty
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configEmpty);
                return config.FirstChild;
            }
        }
        private XmlNode ConfigFull
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configFull);
                return config.FirstChild;
            }
        }

        [TestMethod]
        public void Test_DisposeSamplesBCWriter_Initialize()
        {
            DNBLogger.Enable = false;

            DisposeSamplesBCWriter writer = new DisposeSamplesBCWriter();
            writer.Initialize(ConfigEmpty, DefaultConfig, null);
            Assert.AreEqual(writer.SampleBcColumn, "SampleBCColumn");
            Assert.AreEqual(writer.UserNameColumn, "UserNameColumn");
            Assert.AreEqual(writer.ActionDateTimeColumn, "ActionDateTimeColumn");
            Assert.AreEqual(writer.CommentColumn, "CommentColumn");
            Assert.AreEqual(writer.LookupIdSql, "Sql");

            writer = new DisposeSamplesBCWriter();
            writer.Initialize(ConfigFull, DefaultConfig, null);
            Assert.AreEqual(writer.SampleBcColumn, "SampleBCColumn");
            Assert.AreEqual(writer.UserNameColumn, "UserNameColumn");
            Assert.AreEqual(writer.ActionDateTimeColumn, "ActionDateTimeColumn");
            Assert.AreEqual(writer.CommentColumn, "CommentColumn");
            Assert.AreEqual(writer.LookupIdSql, "Sql");
        }

        [TestMethod]
        public void Test_DisposeSamplesBCWriter_DoStuff()
        {
            DNBLogger.Enable = false;

            DisposeSamplesBCWriter writer = new DisposeSamplesBCWriter();
            object retval = writer.DoStuff(null);
            Assert.IsNull(retval);
            Assert.AreEqual("DisposeSamplesBCWriter: Data is not defined", DNBLogger.LastLog);

            DataFile dataFile = new DataFile();
            dataFile.Header = "SampleBCColumn;UserNameColumn;ActionDateTimeColumn;CommentColumn";
            dataFile.DataDelimiter = ';';
            DataFile datafileRetval = writer.DoStuff(dataFile);
            Assert.IsNotNull(datafileRetval);
            Assert.AreEqual("DisposeSamplesBCWriter: DoStuff end", DNBLogger.LastLog);

            Mock<LimsCommunication> oLims = CreateLims(0);
            Service.LimsCommunication = oLims.Object;
            writer = new DisposeSamplesBCWriter();
            writer.SampleBcColumn = "SampleBCColumn";
            writer.ActionDateTimeColumn = "ActionDateTimeColumn";
            writer.CommentColumn = "CommentColumn";
            writer.UserNameColumn = "UserNameColumn";
            writer.LookupIdSql = "sql ##BC##";
            dataFile = CreateDataFile();
            datafileRetval = writer.DoStuff(dataFile);
            Assert.IsNull(datafileRetval);
            Assert.AreEqual("DisposeSamplesBCWriter: Invalid number of ids found (null)", DNBLogger.LastLog);
            oLims.Verify(c => c.SampleGetIdsFromSql("sql SampleBCColumn"), Times.Once);

            oLims = CreateLims(2);
            Service.LimsCommunication = oLims.Object;
            writer = new DisposeSamplesBCWriter();
            writer.SampleBcColumn = "SampleBCColumn";
            writer.ActionDateTimeColumn = "ActionDateTimeColumn";
            writer.CommentColumn = "CommentColumn";
            writer.UserNameColumn = "UserNameColumn";
            writer.LookupIdSql = "sql ##BC##";
            dataFile = CreateDataFile();
            datafileRetval = writer.DoStuff(dataFile);
            Assert.IsNull(datafileRetval);
            Assert.AreEqual("DisposeSamplesBCWriter: Invalid number of ids found (2)", DNBLogger.LastLog);
            oLims.Verify(c => c.SampleGetIdsFromSql("sql SampleBCColumn"), Times.Once);

            oLims = CreateLims(1);
            Service.LimsCommunication = oLims.Object;
            writer = new DisposeSamplesBCWriter();
            writer.SampleBcColumn = "SampleBCColumn";
            writer.ActionDateTimeColumn = "ActionDateTimeColumn";
            writer.CommentColumn = "CommentColumn";
            writer.UserNameColumn = "UserNameColumn";
            writer.LookupIdSql = "sql ##BC##";
            dataFile = CreateDataFile();
            datafileRetval = writer.DoStuff(dataFile);
            Assert.IsNotNull(datafileRetval);
            Assert.AreEqual("DisposeSamplesBCWriter: DoStuff end", DNBLogger.LastLog);
            oLims.Verify(c => c.SampleGetIdsFromSql("sql SampleBCColumn"), Times.Once);
            oLims.Verify(c => c.SampleGetIdsFromSql("sql SampleBCColumn1"), Times.Once);
            oLims.Verify(c => c.SampleDispose("a", "UserNameColumn", "ActionDateTimeColumn", "CommentColumn", true), Times.Once);
            oLims.Verify(c => c.SampleDispose("a", "UserNameColumn1", "ActionDateTimeColumn1", "CommentColumn1", true), Times.Once);
        }

        private Mock<LimsCommunication> CreateLims(int SampleGetIdsFromSql)
        {
            Mock<LimsCommunication> oLims = new Mock<LimsCommunication>();
            if (SampleGetIdsFromSql == -1) { oLims.Setup(t => t.SampleGetIdsFromSql(It.IsAny<string>())).Returns((List<string>)null); }
            if (SampleGetIdsFromSql == 1) { oLims.Setup(t => t.SampleGetIdsFromSql(It.IsAny<string>())).Returns(new List<string>() { "a" }); }
            if (SampleGetIdsFromSql == 2) { oLims.Setup(t => t.SampleGetIdsFromSql(It.IsAny<string>())).Returns(new List<string>() { "a", "b" }); }
            oLims.Setup(t => t.SampleDispose(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>()));
            return oLims;
        }

        private DataFile CreateDataFile()
        {
            DataFile dataFile = new DataFile();
            dataFile.Header = "SampleBCColumn;UserNameColumn;ActionDateTimeColumn;CommentColumn";
            dataFile.DataDelimiter = ';';
            DataGroup group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            DataRow row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("SampleBCColumn", "SampleBCColumn");
            row.Cells.Add("UserNameColumn", "UserNameColumn");
            row.Cells.Add("ActionDateTimeColumn", "ActionDateTimeColumn");
            row.Cells.Add("CommentColumn", "CommentColumn");
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("SampleBCColumn", "SampleBCColumn1");
            row.Cells.Add("UserNameColumn", "UserNameColumn1");
            row.Cells.Add("ActionDateTimeColumn", "ActionDateTimeColumn1");
            row.Cells.Add("CommentColumn", "CommentColumn1");
            return dataFile;
        }
    }
}