﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using DNBReader;
using DNBReader.Modules;
using DNBTools.Nautilus;
using Moq;
using DNBReader.DataClasses;

namespace UnitTestProject.Modules.Writers
{
    [TestClass]
    public class DisposePlatesWriterUnitTest
    {
        public string _configDefault = "<Defaults><Writers><DisposePlatesWriter><PlateBCColumn>PlateBCColumn</PlateBCColumn><UserNameColumn>UserNameColumn</UserNameColumn><ActionDateTimeColumn>ActionDateTimeColumn</ActionDateTimeColumn><CommentColumn>CommentColumn</CommentColumn></DisposePlatesWriter></Writers></Defaults>";
        public string _configEmpty = "<DisposePlatesWriter></DisposePlatesWriter>";
        public string _configFull = "<DisposePlatesWriter><PlateBCColumn>PlateBCColumn</PlateBCColumn><UserNameColumn>UserNameColumn</UserNameColumn><ActionDateTimeColumn>ActionDateTimeColumn</ActionDateTimeColumn><CommentColumn>CommentColumn</CommentColumn></DisposePlatesWriter>";

        private XmlNode DefaultConfig
        {
            get
            {
                XmlDocument defaultConfig = new XmlDocument();
                defaultConfig.LoadXml(_configDefault);
                return defaultConfig.FirstChild;
            }
        }
        private XmlNode ConfigEmpty
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configEmpty);
                return config.FirstChild;
            }
        }
        private XmlNode ConfigFull
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configFull);
                return config.FirstChild;
            }
        }

        [TestMethod]
        public void Test_DisposePlatesWriter_Initialize()
        {
            DNBLogger.Enable = false;

            DisposePlatesWriter writer = new DisposePlatesWriter();
            writer.Initialize(ConfigEmpty, DefaultConfig, null);
            Assert.AreEqual(writer.PlateBCColumn, "PlateBCColumn");
            Assert.AreEqual(writer.UserNameColumn, "UserNameColumn");
            Assert.AreEqual(writer.ActionDateTimeColumn, "ActionDateTimeColumn");
            Assert.AreEqual(writer.CommentColumn, "CommentColumn");

            writer = new DisposePlatesWriter();
            writer.Initialize(ConfigFull, DefaultConfig, null);
            Assert.AreEqual(writer.PlateBCColumn, "PlateBCColumn");
            Assert.AreEqual(writer.UserNameColumn, "UserNameColumn");
            Assert.AreEqual(writer.ActionDateTimeColumn, "ActionDateTimeColumn");
            Assert.AreEqual(writer.CommentColumn, "CommentColumn");
        }

        [TestMethod]
        public void Test_DisposePlatesWriter_DoStuff()
        {
            DNBLogger.Enable = false;

            DisposePlatesWriter writer = new DisposePlatesWriter();
            object retval = writer.DoStuff(null);
            Assert.IsNull(retval);
            Assert.AreEqual("DisposePlatesWriter: Data is not defined", DNBLogger.LastLog);

            Mock<LimsCommunication> oLims = CreateLims();
            Service.LimsCommunication = oLims.Object;
            writer = new DisposePlatesWriter();
            writer.PlateBCColumn = "PlateBCColumn";
            writer.UserNameColumn = "UserNameColumn";
            writer.ActionDateTimeColumn = "ActionDateTimeColumn";
            writer.CommentColumn = "CommentColumn";
            DataFile dataFile = CreateDataFile();
            DataFile datafileRetval = writer.DoStuff(dataFile);
            Assert.IsNotNull(datafileRetval);
            Assert.AreEqual("DisposePlatesWriter: DoStuff end", DNBLogger.LastLog);
            oLims.Verify(c => c.PlateDispose("PlateBCColumn", "UserNameColumn", "ActionDateTimeColumn", "CommentColumn", true), Times.Once);
            oLims.Verify(c => c.PlateDispose("PlateBCColumn1", "UserNameColumn1", "ActionDateTimeColumn1", "CommentColumn1", true), Times.Once);
        }

        private Mock<LimsCommunication> CreateLims()
        {
            Mock<LimsCommunication> oLims = new Mock<LimsCommunication>();
            oLims.Setup(t => t.PlateDispose(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>()));
            return oLims;
        }

        private DataFile CreateDataFile()
        {
            DataFile dataFile = new DataFile();
            dataFile.Header = "PlateBCColumn;UserNameColumn;ActionDateTimeColumn;CommentColumn";
            dataFile.DataDelimiter = ';';
            DataGroup group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            DataRow row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("PlateBCColumn", "PlateBCColumn");
            row.Cells.Add("UserNameColumn", "UserNameColumn");
            row.Cells.Add("ActionDateTimeColumn", "ActionDateTimeColumn");
            row.Cells.Add("CommentColumn", "CommentColumn");
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("PlateBCColumn", "PlateBCColumn1");
            row.Cells.Add("UserNameColumn", "UserNameColumn1");
            row.Cells.Add("ActionDateTimeColumn", "ActionDateTimeColumn1");
            row.Cells.Add("CommentColumn", "CommentColumn1");
            return dataFile;
        }
    }
}
