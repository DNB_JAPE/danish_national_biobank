﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using DNBReader;
using DNBReader.Modules;
using DNBReader.DataClasses;
using Moq;
using DNBTools.Nautilus;

namespace UnitTestProject.Modules.Writers
{
    [TestClass]
    public class DisposeSamplesIDWriterUnitTest
    {
        public string _configDefault = "<Defaults><Writers><DisposeSamplesIDWriter><SampleIDColumn>SampleIDColumn</SampleIDColumn><UserNameColumn>UserNameColumn</UserNameColumn><ActionDateTimeColumn>ActionDateTimeColumn</ActionDateTimeColumn><CommentColumn>CommentColumn</CommentColumn></DisposeSamplesIDWriter></Writers></Defaults>";
        public string _configEmpty = "<DisposeSamplesIDWriter></DisposeSamplesIDWriter>";
        public string _configFull = "<DisposeSamplesIDWriter><SampleIDColumn>SampleIDColumn</SampleIDColumn><UserNameColumn>UserNameColumn</UserNameColumn><ActionDateTimeColumn>ActionDateTimeColumn</ActionDateTimeColumn><CommentColumn>CommentColumn</CommentColumn></DisposeSamplesIDWriter>";

        private XmlNode DefaultConfig
        {
            get
            {
                XmlDocument defaultConfig = new XmlDocument();
                defaultConfig.LoadXml(_configDefault);
                return defaultConfig.FirstChild;
            }
        }
        private XmlNode ConfigEmpty
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configEmpty);
                return config.FirstChild;
            }
        }
        private XmlNode ConfigFull
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configFull);
                return config.FirstChild;
            }
        }

        [TestMethod]
        public void Test_DisposeSamplesIDWriter_Initialize()
        {
            DNBLogger.Enable = false;

            DisposeSamplesIDWriter writer = new DisposeSamplesIDWriter();
            writer.Initialize(ConfigEmpty, DefaultConfig, null);
            Assert.AreEqual(writer.SampleIdColumn, "SampleIDColumn");
            Assert.AreEqual(writer.UserNameColumn, "UserNameColumn");
            Assert.AreEqual(writer.ActionDateTimeColumn, "ActionDateTimeColumn");
            Assert.AreEqual(writer.CommentColumn, "CommentColumn");

            writer = new DisposeSamplesIDWriter();
            writer.Initialize(ConfigFull, DefaultConfig, null);
            Assert.AreEqual(writer.SampleIdColumn, "SampleIDColumn");
            Assert.AreEqual(writer.UserNameColumn, "UserNameColumn");
            Assert.AreEqual(writer.ActionDateTimeColumn, "ActionDateTimeColumn");
            Assert.AreEqual(writer.CommentColumn, "CommentColumn");
        }

        [TestMethod]
        public void Test_DisposeSamplesIDWriter_DoStuff()
        {
            DNBLogger.Enable = false;

            DisposeSamplesIDWriter writer = new DisposeSamplesIDWriter();
            object retval = writer.DoStuff(null);
            Assert.IsNull(retval);
            Assert.AreEqual("DisposeSamplesIDWriter: Data is not defined", DNBLogger.LastLog);

            DataFile dataFile = new DataFile();
            dataFile.Header = "SampleIDColumn;UserNameColumn;ActionDateTimeColumn;CommentColumn";
            dataFile.DataDelimiter = ';';
            DataFile datafileRetval = writer.DoStuff(dataFile);
            Assert.IsNotNull(datafileRetval);
            Assert.AreEqual("DisposeSamplesIDWriter: DoStuff end", DNBLogger.LastLog);

            Mock<LimsCommunication> oLims = CreateLims();
            Service.LimsCommunication = oLims.Object;
            writer = new DisposeSamplesIDWriter();
            writer.SampleIdColumn = "SampleIDColumn";
            writer.ActionDateTimeColumn = "ActionDateTimeColumn";
            writer.CommentColumn = "CommentColumn";
            writer.UserNameColumn = "UserNameColumn";
            dataFile = CreateDataFile();
            datafileRetval = writer.DoStuff(dataFile);
            Assert.IsNotNull(datafileRetval);
            Assert.AreEqual("DisposeSamplesIDWriter: DoStuff end", DNBLogger.LastLog);
            oLims.Verify(c => c.SampleDispose("SampleIDColumn", "UserNameColumn", "ActionDateTimeColumn", "CommentColumn", true), Times.Once);
            oLims.Verify(c => c.SampleDispose("SampleIDColumn1", "UserNameColumn1", "ActionDateTimeColumn1", "CommentColumn1", true), Times.Once);
        }

        private Mock<LimsCommunication> CreateLims()
        {
            Mock<LimsCommunication> oLims = new Mock<LimsCommunication>();
            oLims.Setup(t => t.SampleDispose(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>()));
            return oLims;
        }

        private DataFile CreateDataFile()
        {
            DataFile dataFile = new DataFile();
            dataFile.Header = "SampleIDColumn;UserNameColumn;ActionDateTimeColumn;CommentColumn";
            dataFile.DataDelimiter = ';';
            DataGroup group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            DataRow row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("SampleIDColumn", "SampleIDColumn");
            row.Cells.Add("UserNameColumn", "UserNameColumn");
            row.Cells.Add("ActionDateTimeColumn", "ActionDateTimeColumn");
            row.Cells.Add("CommentColumn", "CommentColumn");
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("SampleIDColumn", "SampleIDColumn1");
            row.Cells.Add("UserNameColumn", "UserNameColumn1");
            row.Cells.Add("ActionDateTimeColumn", "ActionDateTimeColumn1");
            row.Cells.Add("CommentColumn", "CommentColumn1");
            return dataFile;
        }
    }
}
