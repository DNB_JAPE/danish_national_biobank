﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using DNBReader.Modules;
using System.Xml.Linq;
using DNBReader;
using DNBReader.DataClasses;
using Moq;
using System.Collections.Generic;

namespace UnitTestProject
{
    [TestClass]
    public class FileReaderUnitTest
    {
        public string _configDefault = "<Defaults><Readers><FileReader><Dir>DirectoryDefault</Dir><Pattern>PatternDefault</Pattern><Delimiter>;</Delimiter></FileReader></Readers></Defaults>";
        public string _configEmpty = "<FileReader></FileReader>";
        public string _configFull = "<FileReader><Dir>Directory</Dir><Pattern>Pattern</Pattern><Delimiter>,</Delimiter></FileReader>";

        private XmlNode DefaultConfig
        {
            get
            {
                XmlDocument defaultConfig = new XmlDocument();
                defaultConfig.LoadXml(_configDefault);
                return defaultConfig.FirstChild;
            }
        }

        private XmlNode ConfigEmpty
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configEmpty);
                return config.FirstChild;
            }
        }

        private XmlNode ConfigFull
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configFull);
                return config.FirstChild;
            }
        }

        [TestMethod]
        public void Test_FileReader_Initialize()
        {
            DNBLogger.Enable = false;

            FileReader reader = new FileReader();
            reader.Initialize(ConfigEmpty, DefaultConfig, null);
            Assert.AreEqual(reader.Dir, "DirectoryDefault");
            Assert.AreEqual(reader.Pattern, "PatternDefault");
            Assert.AreEqual(reader.Delimiter, ';');

            reader = new FileReader();
            reader.Initialize(ConfigFull, DefaultConfig, null);
            Assert.AreEqual(reader.Dir, "Directory");
            Assert.AreEqual(reader.Pattern, "Pattern");
            Assert.AreEqual(reader.Delimiter, ',');
        }

        [TestMethod]
        [DeploymentItem(@"TestFiles/FileReader.csv")]
        [DeploymentItem(@"TestFiles/FileReaderColumns.csv")]
        public void Test_FileReader_DoStuff()
        {
            DNBLogger.Enable = false;

            FileReader reader = new FileReader();
            reader.Project = CreateProject().Object;
            object retval = reader.DoStuff(new DataFile());
            Assert.IsNull(retval);
            Assert.AreEqual("FileReader: Data exist before file is read", DNBLogger.LastLog);

            reader = new FileReader();
            reader.Project = CreateProject().Object;
            reader.Dir = "c:\\NONEXISTINGDIR";
            retval = reader.DoStuff(null);
            Assert.IsNull(retval);
            Assert.AreEqual("FileReader: Input directory does not exist: c:\\NONEXISTINGDIR", DNBLogger.LastLog);

            reader = new FileReader();
            reader.Project = CreateProject().Object;
            reader.Dir = ".";
            reader.Pattern = "NonExistingFileReader.csv";
            reader.Delimiter = ';';
            DataFile dataFile = reader.DoStuff(null);
            Assert.IsNull(dataFile);
            Assert.AreEqual("FileReader: DoStuff end - no files", DNBLogger.LastLog);

            reader = new FileReader();
            reader.Project = CreateProject().Object;
            reader.Dir = ".";
            reader.Pattern = "FileReaderColumns.csv";
            reader.Delimiter = ';';
            dataFile = reader.DoStuff(null);
            Assert.IsNull(dataFile);
            Assert.AreEqual("FileReader: Number of columns does not match header: Row3C1", DNBLogger.LastLog);

            reader = new FileReader();
            reader.Project = CreateProject().Object;
            reader.Dir = ".";
            reader.Pattern = "FileReader.csv";
            reader.Delimiter = ';';
            dataFile = reader.DoStuff(null);
            Assert.IsNotNull(dataFile);
            Assert.AreEqual("FileReader: DoStuff end", DNBLogger.LastLog);
            Assert.AreEqual("FileReader.csv", dataFile.Filename);
            Assert.AreEqual(';', dataFile.DataDelimiter);
            Assert.AreEqual("Column1;Column2", dataFile.Header);
            Assert.AreEqual(1, dataFile.Groups.Count);
            Assert.AreEqual(3, dataFile.Groups[0].DataRows.Count);
            Assert.AreEqual(2, dataFile.Groups[0].DataRows[0].Cells.Count);
            Assert.AreEqual("Row1C1", dataFile.Groups[0].DataRows[0].Cells["Column1"]);
            Assert.AreEqual("", dataFile.Groups[0].DataRows[1].Cells["Column1"]);
            Assert.AreEqual("Row2C2", dataFile.Groups[0].DataRows[1].Cells["Column2"]);
            Assert.AreEqual("", dataFile.Groups[0].DataRows[2].Cells["Column1"]);
            Assert.AreEqual("Row3C2", dataFile.Groups[0].DataRows[2].Cells["Column2"]);
        }

        private Mock<Project> CreateProject()
        {
            Mock<Project> oProject = new Mock<Project>();
            oProject.SetupAllProperties();
            oProject.Object.Name = "Blah";
            oProject.Object.DataFiles = new List<string>();
            return oProject;
        }
    }
}
