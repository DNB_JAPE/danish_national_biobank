﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DNBReader.Modules;
using DNBReader.DataClasses;
using DNBReader;
using Moq;

namespace UnitTestProject.Modules
{
    [TestClass]
    public class ModifierUnitTest
    {
        [TestMethod]
        public void Test_Modifier_DoStuff()
        {
            DNBLogger.Enable = false;

            Modifier modifier = new TestNullModifier();
            modifier.Project = CreateProject().Object;
            DataFile retval = modifier.DoStuff(null);
            Assert.IsNull(retval);
            Assert.AreEqual("Modifier: Data is not defined", DNBLogger.LastLog);

            modifier = new TestNullModifier();
            modifier.TargetColumn = "Target";
            modifier.SourceColumns = new string[] { "Source1" };
            modifier.Project = CreateProject().Object;
            retval = modifier.DoStuff(CreateDataFile());
            Assert.IsNull(retval);
            Assert.AreEqual("Modifier: DoStuff end (Blah)", DNBLogger.LastLog);

            modifier = new TestNotNullModifier();
            modifier.TargetColumn = "Target";
            modifier.SourceColumns = new string[] { "Source1" };
            modifier.Project = CreateProject().Object;
            retval = modifier.DoStuff(CreateDataFile());
            Assert.IsNotNull(retval);
            Assert.AreEqual("Modifier: DoStuff end (Blah)", DNBLogger.LastLog);
            Assert.AreEqual(3, retval.Groups[0].DataRows[0].Cells.Count);
            Assert.AreEqual("A", retval.Groups[0].DataRows[0].Cells["Target"]);
        }

        private Mock<Project> CreateProject()
        {
            Mock<Project> oProject = new Mock<Project>();
            oProject.SetupAllProperties();
            oProject.Object.Name = "Blah";
            return oProject;
        }

        private DataFile CreateDataFile()
        {
            DataFile dataFile = new DataFile();
            dataFile.DataDelimiter = ';';
            dataFile.Header = "Source1;Column2";
            dataFile.Groups.Add(new DataGroup(dataFile));
            dataFile.Groups[0].DataRows.Add(new DataRow(dataFile.Groups[0]));
            dataFile.Groups[0].DataRows[0].Cells.Add("Source1", "Plate1");
            dataFile.Groups[0].DataRows[0].Cells.Add("Column2", "Dummy");
            return dataFile;
        }
    }

    public class TestNullModifier : Modifier { }

    public class TestNotNullModifier : Modifier 
    {
        protected override string Modify(System.Collections.Generic.List<string> sources)
        {
            return "A";
        }
    }
}
