﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DNBReader;
using DNBReader.DataClasses;
using DNBReader.Modules;

namespace UnitTestProject.Modules.Filters
{
    [TestClass]
    public class DNBStandardInstrumentFileFormatFilterUnitTest
    {
        [TestMethod]
        public void Test_DNBStandardInstrumentFileFormatFilter_Initialize()
        {
            DNBLogger.Enable = false;
            DNBStandardInstrumentFileFormatFilter filter = new DNBStandardInstrumentFileFormatFilter();
            filter.Initialize(null, null, null);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Initialize", DNBLogger.LastLog);
        }

        [TestMethod]
        public void Test_DNBStandardInstrumentFileFormatFilter_DoStuff()
        {
            DNBLogger.Enable = false;

            DNBStandardInstrumentFileFormatFilter filter = new DNBStandardInstrumentFileFormatFilter();
            object retval = filter.DoStuff(null);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data is not defined", DNBLogger.LastLog);

            filter = new DNBStandardInstrumentFileFormatFilter();
            DataFile dataFile = new DataFile();
            dataFile.Filename = "Filename";
            retval = filter.DoStuff(dataFile);
            Assert.IsNotNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: No plates in data file: Filename", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("RecordId");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("TRackBC");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("Tube_Type");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("TPositionId");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("TPositionBC");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("TStatusSummary");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("TSumStateDescription");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("TVolume");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("SRackBC");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("Sample_Type");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("SPositionId");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("SPositionBC");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("ActionDateTime");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("UserName");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("Instrument");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("Project");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("UserComment");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("MasterContainer");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("LocationId");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("Description");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            retval = filter.DoStuff(dataFile);
            Assert.IsNotNull(retval);
            Assert.AreEqual("DNBStandardInstrumentFileFormatFilter: DoStuff end", DNBLogger.LastLog);
        }

        private DataFile CreateDataFile()
        {
            DataFile dataFile = new DataFile();
            dataFile.Groups.Add(new DataGroup(dataFile));
            dataFile.Groups[0].DataRows.Add(new DataRow(dataFile.Groups[0]));
            dataFile.Groups[0].DataRows[0].Cells.Add("RecordId", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("TRackBC", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("Tube_Type", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("TPositionId", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("TPositionBC", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("TStatusSummary", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("TSumStateDescription", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("TVolume", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("SRackBC", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("Sample_Type", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("SPositionId", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("SPositionBC", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("ActionDateTime", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("UserName", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("Instrument", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("Project", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("UserComment", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("MasterContainer", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("LocationId", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("Description", "Data");
            return dataFile;
        }
    }
}
