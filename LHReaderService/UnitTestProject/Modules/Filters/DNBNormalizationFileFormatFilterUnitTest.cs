﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DNBReader.DataClasses;
using DNBReader;
using DNBReader.Modules;

namespace UnitTestProject.Modules.Filters
{
    [TestClass]
    public class DNBNormalizationFileFormatFilterUnitTest
    {
        [TestMethod]
        public void Test_DNBNormalizationFileFormatFilter_Initialize()
        {
            DNBLogger.Enable = false;
            DNBNormalizationFileFormatFilter filter = new DNBNormalizationFileFormatFilter();
            filter.Initialize(null, null, null);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Initialize", DNBLogger.LastLog);
        }

        [TestMethod]
        public void Test_DNBNormalizationFileFormatFilter_DoStuff()
        {
            DNBLogger.Enable = false;

            DNBNormalizationFileFormatFilter filter = new DNBNormalizationFileFormatFilter();
            object retval = filter.DoStuff(null);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data is not defined", DNBLogger.LastLog);

            filter = new DNBNormalizationFileFormatFilter();
            DataFile dataFile = new DataFile();
            dataFile.Filename = "Filename";
            retval = filter.DoStuff(dataFile);
            Assert.IsNotNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: No plates in data file: Filename", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("RecordId");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("TRackBC");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("Tube_Type");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("TPositionId");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("TPositionBC");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("TStatusSummary");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("TSumStateDescription");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("TVolume");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("SRackBC");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("Sample_Type");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("SPositionId");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("SPositionBC");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("ActionDateTime");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("UserName");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("Instrument");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("Project");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("UserComment");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("230nm");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("260nm");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("280nm");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("Concentration");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("A260/A230");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("A260/A280");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("SVolume");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            retval = filter.DoStuff(dataFile);
            Assert.IsNotNull(retval);
            Assert.AreEqual("DNBNormalizationFileFormatFilter: DoStuff end", DNBLogger.LastLog);
        }

        private DataFile CreateDataFile()
        {
            DataFile dataFile = new DataFile();
            dataFile.Groups.Add(new DataGroup(dataFile));
            dataFile.Groups[0].DataRows.Add(new DataRow(dataFile.Groups[0]));
            dataFile.Groups[0].DataRows[0].Cells.Add("RecordId", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("TRackBC", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("Tube_Type", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("TPositionId", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("TPositionBC", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("TStatusSummary", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("TSumStateDescription", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("TVolume", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("SRackBC", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("Sample_Type", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("SPositionId", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("SPositionBC", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("ActionDateTime", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("UserName", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("Instrument", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("Project", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("UserComment", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("230nm", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("260nm", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("280nm", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("Concentration", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("A260/A230", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("A260/A280", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("SVolume", "Data");
            return dataFile;
        }
    }
}
