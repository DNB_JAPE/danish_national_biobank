﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using DNBReader;
using DNBReader.Modules;
using DNBReader.DataClasses;
using Moq;
using DNBTools.Nautilus;
using System.Collections.Generic;

namespace UnitTestProject.Modules.Filters
{
    [TestClass]
    public class ExistingMasterFilterUnitTest
    {
        public string _configDefault = "<Defaults><Filters><ExistingMasterFilter><CreateMasters>CreateMasters</CreateMasters><Sql>Sql</Sql><SampleBCColumn>SampleBCColumn</SampleBCColumn><PlateBCColumn>PlateBCColumn</PlateBCColumn><DescriptionColumn>DescriptionColumn</DescriptionColumn><ProjectColumn>ProjectColumn</ProjectColumn><ContainerTypeColumn>ContainerTypeColumn</ContainerTypeColumn></ExistingMasterFilter></Filters></Defaults>";
        public string _configEmpty = "<ExistingMasterFilter></ExistingMasterFilter>";
        public string _configFull = "<ExistingMasterFilter><CreateMasters>CreateMasters</CreateMasters><Sql>Sql</Sql><SampleBCColumn>SampleBCColumn</SampleBCColumn><PlateBCColumn>PlateBCColumn</PlateBCColumn><DescriptionColumn>DescriptionColumn</DescriptionColumn><ProjectColumn>ProjectColumn</ProjectColumn><ContainerTypeColumn>ContainerTypeColumn</ContainerTypeColumn></ExistingMasterFilter>";

        private XmlNode DefaultConfig
        {
            get
            {
                XmlDocument defaultConfig = new XmlDocument();
                defaultConfig.LoadXml(_configDefault);
                return defaultConfig.FirstChild;
            }
        }
        private XmlNode ConfigEmpty
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configEmpty);
                return config.FirstChild;
            }
        }
        private XmlNode ConfigFull
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configFull);
                return config.FirstChild;
            }
        }

        [TestMethod]
        public void Test_ExistingMasterFilter_Initialize()
        {
            DNBLogger.Enable = false;

            ExistingMasterFilter filter = new ExistingMasterFilter();
            filter.Initialize(ConfigEmpty, DefaultConfig, null);
            Assert.AreEqual(filter.CreateMasters, "CREATEMASTERS");
            Assert.AreEqual(filter.Sql, "SQL");
            Assert.AreEqual(filter.SampleBCColumn, "SampleBCColumn");
            Assert.AreEqual(filter.PlateBCColumn, "PlateBCColumn");
            Assert.AreEqual(filter.DescriptionColumn, "DescriptionColumn");
            Assert.AreEqual(filter.ProjectColumn, "ProjectColumn");
            Assert.AreEqual(filter.ContainerTypeColumn, "ContainerTypeColumn");

            filter = new ExistingMasterFilter();
            filter.Initialize(ConfigFull, DefaultConfig, null);
            Assert.AreEqual(filter.CreateMasters, "CREATEMASTERS");
            Assert.AreEqual(filter.Sql, "SQL");
            Assert.AreEqual(filter.SampleBCColumn, "SampleBCColumn");
            Assert.AreEqual(filter.PlateBCColumn, "PlateBCColumn");
            Assert.AreEqual(filter.DescriptionColumn, "DescriptionColumn");
            Assert.AreEqual(filter.ProjectColumn, "ProjectColumn");
            Assert.AreEqual(filter.ContainerTypeColumn, "ContainerTypeColumn");
        }

        [TestMethod]
        public void Test_ExistingMasterFilter_DoStuff()
        {
            DNBLogger.Enable = false;

            ExistingMasterFilter filter = new ExistingMasterFilter();
            object retval = filter.DoStuff(null);
            Assert.IsNull(retval);
            Assert.AreEqual("ExistingMasterFilter: Data is not defined", DNBLogger.LastLog);

            filter = new ExistingMasterFilter();
            filter.CreateMasters = "ALLWAYS";
            DataFile dataFile = new DataFile();
            dataFile.Filename = "Noget.csv";
            retval = filter.DoStuff(dataFile);
            Assert.IsNotNull(retval);
            Assert.AreEqual("ExistingMasterFilter: DoStuff end", DNBLogger.LastLog);

            Mock<LimsCommunication> oLims = CreateLims(1);
            Service.LimsCommunication = oLims.Object;
            filter = new ExistingMasterFilter();
            filter.Sql = "sql";
            filter.CreateMasters = "ALWAYS";
            dataFile = CreateDataFile();
            dataFile.Filename = "Noget_Rerun.csv";
            retval = filter.DoStuff(dataFile);
            Assert.IsNotNull(retval);
            Assert.AreEqual("ExistingMasterFilter: DoStuff end", DNBLogger.LastLog);

            oLims = CreateLims(1);
            Service.LimsCommunication = oLims.Object;
            filter = new ExistingMasterFilter();
            filter.Sql = "sql";
            filter.CreateMasters = "NEVER";
            dataFile = CreateDataFile();
            dataFile.Filename = "Noget.csv";
            retval = filter.DoStuff(dataFile);
            Assert.IsNotNull(retval);
            Assert.AreEqual("ExistingMasterFilter: DoStuff end", DNBLogger.LastLog);

            oLims = CreateLims(1);
            Service.LimsCommunication = oLims.Object;
            filter = new ExistingMasterFilter();
            filter.Sql = "##MASTERDESCRIPTION##";
            filter.CreateMasters = "NEVER";
            filter.DescriptionColumn = "DescriptionColumn";
            dataFile = CreateDataFile();
            dataFile.Filename = "Noget.csv";
            retval = filter.DoStuff(dataFile);
            Assert.IsNotNull(retval);
            Assert.AreEqual("ExistingMasterFilter: DoStuff end", DNBLogger.LastLog);
            oLims.Verify(c => c.SampleGetIdsFromSql("DescriptionColumn"), Times.Once);

            oLims = CreateLims(1);
            Service.LimsCommunication = oLims.Object;
            filter = new ExistingMasterFilter();
            filter.Sql = "##MASTERDESCRIPTION##";
            filter.CreateMasters = "NEVER";
            filter.DescriptionColumn = "DescriptionColumn";
            dataFile = CreateDataFile();
            dataFile.Filename = "Noget.csv";
            dataFile.Groups[0].DataRows[0].Cells.Remove("DescriptionColumn");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("ExistingMasterFilter: Description column (DescriptionColumn) missing in data", DNBLogger.LastLog);
            oLims.Verify(c => c.SampleGetIdsFromSql("DescriptionColumn"), Times.Never);

            oLims = CreateLims(1);
            Service.LimsCommunication = oLims.Object;
            filter = new ExistingMasterFilter();
            filter.Sql = "##SOURCEALIQUOTBC##";
            filter.CreateMasters = "NEVER";
            filter.SampleBCColumn = "SampleBCColumn";
            dataFile = CreateDataFile();
            dataFile.Filename = "Noget.csv";
            retval = filter.DoStuff(dataFile);
            Assert.IsNotNull(retval);
            Assert.AreEqual("ExistingMasterFilter: DoStuff end", DNBLogger.LastLog);
            oLims.Verify(c => c.SampleGetIdsFromSql("SampleBCColumn"), Times.Once);

            oLims = CreateLims(1);
            Service.LimsCommunication = oLims.Object;
            filter = new ExistingMasterFilter();
            filter.Sql = "##SOURCEALIQUOTBC##";
            filter.CreateMasters = "NEVER";
            filter.SampleBCColumn = "SampleBCColumn";
            dataFile = CreateDataFile();
            dataFile.Filename = "Noget.csv";
            dataFile.Groups[0].DataRows[0].Cells.Remove("SampleBCColumn");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("ExistingMasterFilter: Source sample barcode column (SampleBCColumn) missing in data", DNBLogger.LastLog);
            oLims.Verify(c => c.SampleGetIdsFromSql("SampleBCColumn"), Times.Never);

            oLims = CreateLims(1);
            Service.LimsCommunication = oLims.Object;
            filter = new ExistingMasterFilter();
            filter.Sql = "##SOURCEPLATEBC##";
            filter.CreateMasters = "NEVER";
            filter.PlateBCColumn = "PlateBCColumn";
            dataFile = CreateDataFile();
            dataFile.Filename = "Noget.csv";
            retval = filter.DoStuff(dataFile);
            Assert.IsNotNull(retval);
            Assert.AreEqual("ExistingMasterFilter: DoStuff end", DNBLogger.LastLog);
            oLims.Verify(c => c.SampleGetIdsFromSql("PlateBCColumn"), Times.Once);

            oLims = CreateLims(1);
            Service.LimsCommunication = oLims.Object;
            filter = new ExistingMasterFilter();
            filter.Sql = "##SOURCEPLATEBC##";
            filter.CreateMasters = "NEVER";
            filter.PlateBCColumn = "PlateBCColumn";
            dataFile = CreateDataFile();
            dataFile.Filename = "Noget.csv";
            dataFile.Groups[0].DataRows[0].Cells.Remove("PlateBCColumn");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("ExistingMasterFilter: Source plate barcode column (PlateBCColumn) missing in data", DNBLogger.LastLog);
            oLims.Verify(c => c.SampleGetIdsFromSql("PlateBCColumn"), Times.Never);

            oLims = CreateLims(1);
            Service.LimsCommunication = oLims.Object;
            filter = new ExistingMasterFilter();
            filter.Sql = "##PROJECT##";
            filter.CreateMasters = "NEVER";
            filter.ProjectColumn = "ProjectColumn";
            dataFile = CreateDataFile();
            dataFile.Filename = "Noget.csv";
            retval = filter.DoStuff(dataFile);
            Assert.IsNotNull(retval);
            Assert.AreEqual("ExistingMasterFilter: DoStuff end", DNBLogger.LastLog);
            oLims.Verify(c => c.SampleGetIdsFromSql("ProjectColumn"), Times.Once);

            oLims = CreateLims(1);
            Service.LimsCommunication = oLims.Object;
            filter = new ExistingMasterFilter();
            filter.Sql = "##PROJECT##";
            filter.CreateMasters = "NEVER";
            filter.ProjectColumn = "ProjectColumn";
            dataFile = CreateDataFile();
            dataFile.Filename = "Noget.csv";
            dataFile.Groups[0].DataRows[0].Cells.Remove("ProjectColumn");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("ExistingMasterFilter: Project column (ProjectColumn) missing in data", DNBLogger.LastLog);
            oLims.Verify(c => c.SampleGetIdsFromSql("ProjectColumn"), Times.Never);

            oLims = CreateLims(1);
            Service.LimsCommunication = oLims.Object;
            filter = new ExistingMasterFilter();
            filter.Sql = "##CONTAINERTYPE##";
            filter.CreateMasters = "NEVER";
            filter.ContainerTypeColumn = "ContainerTypeColumn";
            dataFile = CreateDataFile();
            dataFile.Filename = "Noget.csv";
            retval = filter.DoStuff(dataFile);
            Assert.IsNotNull(retval);
            Assert.AreEqual("ExistingMasterFilter: DoStuff end", DNBLogger.LastLog);
            oLims.Verify(c => c.SampleGetIdsFromSql("ContainerTypeColumn"), Times.Once);

            oLims = CreateLims(1);
            Service.LimsCommunication = oLims.Object;
            filter = new ExistingMasterFilter();
            filter.Sql = "##CONTAINERTYPE##";
            filter.CreateMasters = "NEVER";
            filter.ContainerTypeColumn = "ContainerTypeColumn";
            dataFile = CreateDataFile();
            dataFile.Filename = "Noget.csv";
            dataFile.Groups[0].DataRows[0].Cells.Remove("ContainerTypeColumn");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("ExistingMasterFilter: Container type column (ContainerTypeColumn) missing in data", DNBLogger.LastLog);
            oLims.Verify(c => c.SampleGetIdsFromSql("ContainerTypeColumn"), Times.Never);

            oLims = CreateLims(0);
            Service.LimsCommunication = oLims.Object;
            filter = new ExistingMasterFilter();
            filter.Sql = "sql";
            filter.CreateMasters = "NEVER";
            dataFile = CreateDataFile();
            dataFile.Filename = "Noget.csv";
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("ExistingMasterFilter: Failed to look up ids in Nautilus", DNBLogger.LastLog);
            oLims.Verify(c => c.SampleGetIdsFromSql("sql"), Times.Once);

            oLims = CreateLims(2);
            Service.LimsCommunication = oLims.Object;
            filter = new ExistingMasterFilter();
            filter.Sql = "sql";
            filter.CreateMasters = "NEVER";
            filter.SampleBCColumn = "SampleBCColumn";
            dataFile = CreateDataFile();
            dataFile.Filename = "Noget.csv";
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.IsTrue(DNBLogger.LastLog.StartsWith("ExistingMasterFilter: DoStuff end"));
            oLims.Verify(c => c.SampleGetIdsFromSql("sql"), Times.Once);
        }

        private Mock<LimsCommunication> CreateLims(int times)
        {
            Mock<LimsCommunication> oLims = new Mock<LimsCommunication>();
            if (times == -1) { oLims.Setup(t => t.SampleGetIdsFromSql(It.IsAny<string>())).Returns((List<string>)null); }
            if (times == 1) { oLims.Setup(t => t.SampleGetIdsFromSql(It.IsAny<string>())).Returns(new List<string>() { "a" }); }
            if (times == 2) { oLims.Setup(t => t.SampleGetIdsFromSql(It.IsAny<string>())).Returns(new List<string>() { "a", "b" }); }
            
            return oLims;
        }

        private DataFile CreateDataFile()
        {
            DataFile dataFile = new DataFile();
            DataGroup group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            DataRow row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("DescriptionColumn", "DescriptionColumn");
            row.Cells.Add("SampleBCColumn", "SampleBCColumn");
            row.Cells.Add("PlateBCColumn", "PlateBCColumn");
            row.Cells.Add("ProjectColumn", "ProjectColumn");
            row.Cells.Add("ContainerTypeColumn", "ContainerTypeColumn");
            return dataFile;
        }
    }
}
