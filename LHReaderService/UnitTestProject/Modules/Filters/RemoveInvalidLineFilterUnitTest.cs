﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DNBReader;
using System.Xml;
using DNBReader.Modules;
using DNBReader.DataClasses;

namespace UnitTestProject.Modules.Filters
{
    [TestClass]
    public class RemoveInvalidLineFilterUnitTest
    {
        public string _configDefault = "<Defaults><Filters><RemoveInvalidLineFilter><TargetSampleBCColumn>TargetSampleBCColumn</TargetSampleBCColumn><SourceSampleBCColumn>SourceSampleBCColumn</SourceSampleBCColumn></RemoveInvalidLineFilter></Filters></Defaults>";
        public string _configEmpty = "<RemoveInvalidLineFilter></RemoveInvalidLineFilter>";
        public string _configFull = "<RemoveInvalidLineFilter><TargetSampleBCColumn>TargetSampleBCColumn</TargetSampleBCColumn><SourceSampleBCColumn>SourceSampleBCColumn</SourceSampleBCColumn></RemoveInvalidLineFilter>";

        private XmlNode DefaultConfig
        {
            get
            {
                XmlDocument defaultConfig = new XmlDocument();
                defaultConfig.LoadXml(_configDefault);
                return defaultConfig.FirstChild;
            }
        }
        private XmlNode ConfigEmpty
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configEmpty);
                return config.FirstChild;
            }
        }
        private XmlNode ConfigFull
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configFull);
                return config.FirstChild;
            }
        }

        [TestMethod]
        public void Test_RemoveInvalidLineFilter_Initialize()
        {
            DNBLogger.Enable = false;

            RemoveInvalidLineFilter filter = new RemoveInvalidLineFilter();
            filter.Initialize(ConfigEmpty, DefaultConfig, null);
            Assert.AreEqual(filter.TargetSampleBCColumn, "TargetSampleBCColumn");
            Assert.AreEqual(filter.SourceSampleBCColumn, "SourceSampleBCColumn");

            filter = new RemoveInvalidLineFilter();
            filter.Initialize(ConfigFull, DefaultConfig, null);
            Assert.AreEqual(filter.TargetSampleBCColumn, "TargetSampleBCColumn");
            Assert.AreEqual(filter.SourceSampleBCColumn, "SourceSampleBCColumn");
        }

        [TestMethod]
        public void Test_RemoveInvalidLineFilter_DoStuff()
        {
            DNBLogger.Enable = false;

            RemoveInvalidLineFilter filter = new RemoveInvalidLineFilter();
            object retval = filter.DoStuff(null);
            Assert.IsNull(retval);
            Assert.AreEqual("RemoveInvalidLineFilter: Data is not defined", DNBLogger.LastLog);

            DataFile dataFile = new DataFile();
            DataGroup group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            DataRow row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("TargetSampleBCColumn", "target1");
            row.Cells.Add("SourceSampleBCColumn", "source1");
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("TargetSampleBCColumn", "");
            row.Cells.Add("SourceSampleBCColumn", "");
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("TargetSampleBCColumn", "target3");
            row.Cells.Add("SourceSampleBCColumn", "source3");
            filter = new RemoveInvalidLineFilter();
            filter.TargetSampleBCColumn = "TargetSampleBCColumn";
            filter.SourceSampleBCColumn = "SourceSampleBCColumn";
            DataFile dataFileRetval = filter.DoStuff(dataFile);
            Assert.IsNotNull(dataFileRetval);
            Assert.AreEqual("RemoveInvalidLineFilter: DoStuff end", DNBLogger.LastLog);
            Assert.AreEqual(2, dataFileRetval.Groups[0].DataRows.Count);
            Assert.AreEqual("target1", dataFileRetval.Groups[0].DataRows[0].Cells["TargetSampleBCColumn"]);
            Assert.AreEqual("target3", dataFileRetval.Groups[0].DataRows[1].Cells["TargetSampleBCColumn"]);

            dataFile = new DataFile();
            group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("TargetSampleBCColumn", "target1");
            row.Cells.Add("SourceSampleBCColumn", "source1");
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("TargetSampleBCColumn", "----------");
            row.Cells.Add("SourceSampleBCColumn", "----------");
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("TargetSampleBCColumn", "target3");
            row.Cells.Add("SourceSampleBCColumn", "source3");
            filter = new RemoveInvalidLineFilter();
            filter.TargetSampleBCColumn = "TargetSampleBCColumn";
            filter.SourceSampleBCColumn = "SourceSampleBCColumn";
            dataFileRetval = filter.DoStuff(dataFile);
            Assert.IsNotNull(dataFileRetval);
            Assert.AreEqual("RemoveInvalidLineFilter: DoStuff end", DNBLogger.LastLog);
            Assert.AreEqual(2, dataFileRetval.Groups[0].DataRows.Count);
            Assert.AreEqual("target1", dataFileRetval.Groups[0].DataRows[0].Cells["TargetSampleBCColumn"]);
            Assert.AreEqual("target3", dataFileRetval.Groups[0].DataRows[1].Cells["TargetSampleBCColumn"]);

            dataFile = new DataFile();
            group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("TargetSampleBCColumn", "target1");
            row.Cells.Add("SourceSampleBCColumn", "source1");
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("TargetSampleBCColumn", "");
            row.Cells.Add("SourceSampleBCColumn", "----------");
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("TargetSampleBCColumn", "target3");
            row.Cells.Add("SourceSampleBCColumn", "source3");
            filter = new RemoveInvalidLineFilter();
            filter.TargetSampleBCColumn = "TargetSampleBCColumn";
            filter.SourceSampleBCColumn = "SourceSampleBCColumn";
            dataFileRetval = filter.DoStuff(dataFile);
            Assert.IsNotNull(dataFileRetval);
            Assert.AreEqual("RemoveInvalidLineFilter: DoStuff end", DNBLogger.LastLog);
            Assert.AreEqual(3, dataFileRetval.Groups[0].DataRows.Count);
            Assert.AreEqual("target1", dataFileRetval.Groups[0].DataRows[0].Cells["TargetSampleBCColumn"]);
            Assert.AreEqual("", dataFileRetval.Groups[0].DataRows[1].Cells["TargetSampleBCColumn"]);
            Assert.AreEqual("target3", dataFileRetval.Groups[0].DataRows[2].Cells["TargetSampleBCColumn"]);
        }
    }
}
