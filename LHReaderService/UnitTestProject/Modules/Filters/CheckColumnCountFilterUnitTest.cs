﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using DNBReader;
using DNBReader.Modules;
using DNBReader.DataClasses;

namespace UnitTestProject.Modules.Filters
{
    [TestClass]
    public class CheckColumnCountFilterUnitTest
    {
        public string _configDefault = "<Defaults><Filters><CheckColumnCountFilter><CheckColumnCount>3</CheckColumnCount></CheckColumnCountFilter></Filters></Defaults>";
        public string _configEmpty = "<CheckColumnCountFilter></CheckColumnCountFilter>";
        public string _configFull = "<CheckColumnCountFilter><CheckColumnCount>2</CheckColumnCount></CheckColumnCountFilter>";

        private XmlNode DefaultConfig
        {
            get
            {
                XmlDocument defaultConfig = new XmlDocument();
                defaultConfig.LoadXml(_configDefault);
                return defaultConfig.FirstChild;
            }
        }
        private XmlNode ConfigEmpty
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configEmpty);
                return config.FirstChild;
            }
        }
        private XmlNode ConfigFull
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configFull);
                return config.FirstChild;
            }
        }

        [TestMethod]
        public void Test_CheckColumnCountFilter_Initialize()
        {
            DNBLogger.Enable = false;

            CheckColumnCountFilter filter = new CheckColumnCountFilter();
            filter.Initialize(ConfigEmpty, DefaultConfig, null);
            Assert.AreEqual(filter.ColumnCount, 3);

            filter = new CheckColumnCountFilter();
            filter.Initialize(ConfigFull, DefaultConfig, null);
            Assert.AreEqual(filter.ColumnCount, 2);
        }

        [TestMethod]
        public void Test_CheckColumnCountFilter_DoStuff()
        {
            DNBLogger.Enable = false;

            CheckColumnCountFilter filter = new CheckColumnCountFilter();
            object retval = filter.DoStuff(null);
            Assert.IsNull(retval);
            Assert.AreEqual("CheckColumnCountFilter: Data is not defined", DNBLogger.LastLog);

            filter = new CheckColumnCountFilter();
            filter.ColumnCount = 2;
            DataFile dataFile = new DataFile();
            dataFile.DataDelimiter = ';';
            dataFile.Header = "Column1";
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("CheckColumnCountFilter: Invalid number of columns in header", DNBLogger.LastLog);

            filter = new CheckColumnCountFilter();
            filter.ColumnCount = 2;
            dataFile = new DataFile();
            dataFile.DataDelimiter = ';';
            dataFile.Header = "Column1;Column2";
            dataFile.Groups.Add(new DataGroup(dataFile));
            dataFile.Groups[0].DataRows.Add(new DataRow(dataFile.Groups[0]));
            dataFile.Groups[0].DataRows[0].Cells.Add("Column1", "Plate1");
            dataFile.Groups[0].DataRows[0].Cells.Add("Column2", "Data1");
            dataFile.Groups[0].DataRows.Add(new DataRow(dataFile.Groups[0]));
            dataFile.Groups[0].DataRows[1].Cells.Add("Column1", "Plate1");
            dataFile.Groups[0].DataRows[1].Cells.Add("Column2", "Data2");
            dataFile.Groups[0].DataRows.Add(new DataRow(dataFile.Groups[0]));
            dataFile.Groups[0].DataRows[2].Cells.Add("Column1", "Plate2");
            dataFile.Groups[0].DataRows[2].Cells.Add("Column2", "Data3");
            dataFile.Groups[0].DataRows.Add(new DataRow(dataFile.Groups[0]));
            dataFile.Groups[0].DataRows[3].Cells.Add("Column1", "Plate1");
            DataFile dataFileRetval = filter.DoStuff(dataFile);
            Assert.IsNull(dataFileRetval);
            Assert.AreEqual("CheckColumnCountFilter: DoStuff end", DNBLogger.LastLog);

            filter = new CheckColumnCountFilter();
            filter.ColumnCount = 2;
            dataFile = new DataFile();
            dataFile.DataDelimiter = ';';
            dataFile.Header = "Column1;Column2";
            dataFile.Groups.Add(new DataGroup(dataFile));
            dataFile.Groups[0].DataRows.Add(new DataRow(dataFile.Groups[0]));
            dataFile.Groups[0].DataRows[0].Cells.Add("Column1", "Plate1");
            dataFile.Groups[0].DataRows[0].Cells.Add("Column2", "Data1");
            dataFile.Groups[0].DataRows.Add(new DataRow(dataFile.Groups[0]));
            dataFile.Groups[0].DataRows[1].Cells.Add("Column1", "Plate1");
            dataFile.Groups[0].DataRows[1].Cells.Add("Column2", "Data2");
            dataFile.Groups[0].DataRows.Add(new DataRow(dataFile.Groups[0]));
            dataFile.Groups[0].DataRows[2].Cells.Add("Column1", "Plate2");
            dataFile.Groups[0].DataRows[2].Cells.Add("Column2", "Data3");
            dataFile.Groups[0].DataRows.Add(new DataRow(dataFile.Groups[0]));
            dataFile.Groups[0].DataRows[3].Cells.Add("Column1", "Plate1");
            dataFile.Groups[0].DataRows[3].Cells.Add("Column2", "Data4");
            dataFileRetval = filter.DoStuff(dataFile);
            Assert.IsNotNull(dataFileRetval);
            Assert.AreEqual("CheckColumnCountFilter: DoStuff end", DNBLogger.LastLog);
        }
    }
}
