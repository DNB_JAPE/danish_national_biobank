﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using DNBReader;
using DNBReader.Modules;

namespace UnitTestProject.Modules.Filters
{
    [TestClass]
    public class TargetSampleBCFilterUnitTest
    {
        [TestMethod]
        public void Test_TargetSampleBCFilter_Initialize()
        {
            DNBLogger.Enable = false;

            TargetSampleBCFilter filter = new TargetSampleBCFilter();
            filter.Initialize(null, null, null);
            Assert.AreEqual(filter.Column, "TPositionBC");
            Assert.AreEqual(filter.Regex.ToString(), @"(^$|^.*E\+.*$|^----------$|\W)");
            Assert.AreEqual(filter.Match, false);
        }
    }
}
