﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DNBReader.DataClasses;
using DNBReader;
using DNBReader.Modules;

namespace UnitTestProject.Modules.Filters
{
    [TestClass]
    public class DNBExtractionFileFormatFilterUnitTest
    {
        [TestMethod]
        public void Test_DNBExtractionFileFormatFilter_Initialize()
        {
            DNBLogger.Enable = false;
            DNBExtractionFileFormatFilter filter = new DNBExtractionFileFormatFilter();
            filter.Initialize(null, null, null);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Initialize", DNBLogger.LastLog);
        }

        [TestMethod]
        public void Test_DNBExtractionFileFormatFilter_DoStuff()
        {
            DNBLogger.Enable = false;

            DNBExtractionFileFormatFilter filter = new DNBExtractionFileFormatFilter();
            object retval = filter.DoStuff(null);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data is not defined", DNBLogger.LastLog);

            filter = new DNBExtractionFileFormatFilter();
            DataFile dataFile = new DataFile();
            dataFile.Filename = "Filename";
            retval = filter.DoStuff(dataFile);
            Assert.IsNotNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: No plates in data file: Filename", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("POS_IN");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("BC_IN");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("TRANSFER_IN");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("PROTEASE_ADDED");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("LYSIS_ADDED");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("WASH_3_ADDED");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("WASH_4_ADDED");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("WASH_5_ADDED");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("WASH_6_ADDED");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("BIND_BUFFER_ADDED");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("BEADS_ADDED");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);
            
            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("ELUTION_BUFF_ADDED");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("VOL");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("TRANSFER_OUT");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("POS_OUT");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("BC_RACK_OUT");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("BC_OUT");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("ActionDateTime");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("UserName");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("Instrument");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("Project");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("Kit_Lot");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("Tube_Type");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            dataFile.Groups[0].DataRows[0].Cells.Remove("Sample_Type");
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: Data does not follow standard. Not all columns are present.", DNBLogger.LastLog);

            dataFile = CreateDataFile();
            retval = filter.DoStuff(dataFile);
            Assert.IsNotNull(retval);
            Assert.AreEqual("DNBExtractionFileFormatFilter: DoStuff end", DNBLogger.LastLog);
        }

        private DataFile CreateDataFile()
        {
            DataFile dataFile = new DataFile();
            dataFile.Groups.Add(new DataGroup(dataFile));
            dataFile.Groups[0].DataRows.Add(new DataRow(dataFile.Groups[0]));
            dataFile.Groups[0].DataRows[0].Cells.Add("POS_IN", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("BC_IN", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("TRANSFER_IN", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("PROTEASE_ADDED", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("LYSIS_ADDED", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("WASH_3_ADDED", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("WASH_4_ADDED", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("WASH_5_ADDED", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("WASH_6_ADDED", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("BIND_BUFFER_ADDED", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("BEADS_ADDED", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("ELUTION_BUFF_ADDED", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("VOL", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("TRANSFER_OUT", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("POS_OUT", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("BC_RACK_OUT", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("BC_OUT", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("ActionDateTime", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("UserName", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("Instrument", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("Project", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("Kit_Lot", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("Tube_Type", "Data");
            dataFile.Groups[0].DataRows[0].Cells.Add("Sample_Type", "Data");
            return dataFile;
        }
    }
}
