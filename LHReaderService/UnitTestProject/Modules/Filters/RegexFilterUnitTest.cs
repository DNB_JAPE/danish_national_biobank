﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using DNBReader;
using DNBReader.Modules;
using DNBReader.DataClasses;
using System.Text.RegularExpressions;

namespace UnitTestProject.Modules.Filters
{
    [TestClass]
    public class RegexFilterUnitTest
    {
        public string _configDefault = "<Defaults><Filters><RegexFilter><Column>Column</Column><Regex>Regex</Regex><Match>True</Match></RegexFilter></Filters></Defaults>";
        public string _configEmpty = "<RegexFilter></RegexFilter>";
        public string _configFull = "<RegexFilter><Column>Column</Column><Regex>Regex</Regex><Match>True</Match></RegexFilter>";

        private XmlNode DefaultConfig
        {
            get
            {
                XmlDocument defaultConfig = new XmlDocument();
                defaultConfig.LoadXml(_configDefault);
                return defaultConfig.FirstChild;
            }
        }
        private XmlNode ConfigEmpty
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configEmpty);
                return config.FirstChild;
            }
        }
        private XmlNode ConfigFull
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configFull);
                return config.FirstChild;
            }
        }

        [TestMethod]
        public void Test_RegexFilter_Initialize()
        {
            DNBLogger.Enable = false;

            RegexFilter filter = new RegexFilter();
            filter.Initialize(ConfigEmpty, DefaultConfig, null);
            Assert.AreEqual(filter.Column, "Column");
            Assert.AreEqual(filter.Regex.ToString(), "Regex");
            Assert.AreEqual(filter.Match, true);

            filter = new RegexFilter();
            filter.Initialize(ConfigFull, DefaultConfig, null);
            Assert.AreEqual(filter.Column, "Column");
            Assert.AreEqual(filter.Regex.ToString(), "Regex");
            Assert.AreEqual(filter.Match, true);
        }

        [TestMethod]
        public void Test_RegexFilter_DoStuff()
        {
            DNBLogger.Enable = false;

            RegexFilter filter = new RegexFilter();
            object retval = filter.DoStuff(null);
            Assert.IsNull(retval);
            Assert.AreEqual("RegexFilter: Data is not defined", DNBLogger.LastLog);

            filter = new RegexFilter();
            filter.Column = "Column";
            filter.Regex = new Regex(".NotMatch.");
            filter.Match = true;
            retval = filter.DoStuff(CreateDataFile());
            Assert.IsNull(retval);
            Assert.AreEqual("RegexFilter: Line is not valid: Row1", DNBLogger.LastLog);

            filter = new RegexFilter();
            filter.Column = "Column";
            filter.Regex = new Regex("Row1");
            filter.Match = true;
            retval = filter.DoStuff(CreateDataFile());
            Assert.IsNull(retval);
            Assert.AreEqual("RegexFilter: Line is not valid: Row2", DNBLogger.LastLog);

            filter = new RegexFilter();
            filter.Column = "Column";
            filter.Regex = new Regex("Row.");
            filter.Match = true;
            retval = filter.DoStuff(CreateDataFile());
            Assert.IsNotNull(retval);
            Assert.AreEqual("RegexFilter: DoStuff end", DNBLogger.LastLog);
        }

        private DataFile CreateDataFile()
        {
            DataFile dataFile = new DataFile();
            DataGroup group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            DataRow row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("Column", "Row1");
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("Column", "Row2");
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("Column", "Row3");
            return dataFile;
        }
    }
}
