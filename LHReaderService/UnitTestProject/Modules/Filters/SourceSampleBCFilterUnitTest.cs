﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using DNBReader;
using DNBReader.Modules;

namespace UnitTestProject.Modules.Filters
{
    [TestClass]
    public class SourceSampleBCFilterUnitTest
    {
        [TestMethod]
        public void Test_SourceSampleBCFilter_Initialize()
        {
            DNBLogger.Enable = false;

            SourceSampleBCFilter filter = new SourceSampleBCFilter();
            filter.Initialize(null, null, null);
            Assert.AreEqual(filter.Column, "SPositionBC");
            Assert.AreEqual(filter.Regex.ToString(), @"(^$|^.*E\+.*$|^----------$|\W)");
            Assert.AreEqual(filter.Match, false);
        }
    }
}
