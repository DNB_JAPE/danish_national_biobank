﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using DNBReader;
using DNBReader.Modules;
using DNBReader.DataClasses;

namespace UnitTestProject.Modules.Filters
{
    [TestClass]
    public class VolumeFilterUnitTest
    {
        public string _configDefault = "<Defaults><Filters><VolumeFilter><Column>Column</Column><Min>0</Min><Max>1000</Max></VolumeFilter></Filters></Defaults>";
        public string _configEmpty = "<VolumeFilter></VolumeFilter>";
        public string _configFull = "<VolumeFilter><Column>Column</Column><Min>0</Min><Max>1000</Max></VolumeFilter>";

        private XmlNode DefaultConfig
        {
            get
            {
                XmlDocument defaultConfig = new XmlDocument();
                defaultConfig.LoadXml(_configDefault);
                return defaultConfig.FirstChild;
            }
        }
        private XmlNode ConfigEmpty
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configEmpty);
                return config.FirstChild;
            }
        }
        private XmlNode ConfigFull
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configFull);
                return config.FirstChild;
            }
        }

        [TestMethod]
        public void Test_VolumeFilter_Initialize()
        {
            DNBLogger.Enable = false;

            VolumeFilter filter = new VolumeFilter();
            filter.Initialize(ConfigEmpty, DefaultConfig, null);
            Assert.AreEqual(filter.Column, "Column");
            Assert.AreEqual(filter.Min, 0);
            Assert.AreEqual(filter.Max, 1000);

            filter = new VolumeFilter();
            filter.Initialize(ConfigFull, DefaultConfig, null);
            Assert.AreEqual(filter.Column, "Column");
            Assert.AreEqual(filter.Min, 0);
            Assert.AreEqual(filter.Max, 1000);
        }

        [TestMethod]
        public void Test_VolumeFilter_DoStuff()
        {
            DNBLogger.Enable = false;

            VolumeFilter filter = new VolumeFilter();
            object retval = filter.DoStuff(null);
            Assert.IsNull(retval);
            Assert.AreEqual("VolumeFilter: Data is not defined", DNBLogger.LastLog);

            DataFile dataFile = new DataFile();
            DataGroup group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            DataRow row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("Column", "a");
            filter = new VolumeFilter();
            filter.Column = "Column";
            filter.Min = 0;
            filter.Max = 1000;
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("VolumeFilter: Error: Invalid volume (a)", DNBLogger.LastLog);

            dataFile = new DataFile();
            group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("Column", "1");
            filter = new VolumeFilter();
            filter.Column = "Column";
            filter.Min = 5;
            filter.Max = 1000;
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("VolumeFilter: Error: Invalid volume (1)", DNBLogger.LastLog);

            dataFile = new DataFile();
            group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("Column", "10000");
            filter = new VolumeFilter();
            filter.Column = "Column";
            filter.Min = 0;
            filter.Max = 1000;
            retval = filter.DoStuff(dataFile);
            Assert.IsNull(retval);
            Assert.AreEqual("VolumeFilter: Error: Invalid volume (10000)", DNBLogger.LastLog);

            dataFile = new DataFile();
            group = new DataGroup(dataFile);
            dataFile.Groups.Add(group);
            row = new DataRow(group);
            group.DataRows.Add(row);
            row.Cells.Add("Column", "100");
            filter = new VolumeFilter();
            filter.Column = "Column";
            filter.Min = 0;
            filter.Max = 1000;
            retval = filter.DoStuff(dataFile);
            Assert.IsNotNull(retval);
            Assert.AreEqual("VolumeFilter: DoStuff end", DNBLogger.LastLog);
        }
    }
}
