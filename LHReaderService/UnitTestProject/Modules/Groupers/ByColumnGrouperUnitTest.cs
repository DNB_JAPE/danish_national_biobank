﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using DNBReader;
using DNBReader.Modules;
using DNBReader.DataClasses;

namespace UnitTestProject.Modules.Groupers
{
    [TestClass]
    public class ByColumnGrouperUnitTest
    {
        public string _configDefault = "<Defaults><Groupers><ByColumnGrouper><Column>Column1</Column></ByColumnGrouper></Groupers></Defaults>";
        public string _configEmpty = "<ByColumnGrouper></ByColumnGrouper>";
        public string _configFull = "<ByColumnGrouper><Column>Column1</Column></ByColumnGrouper>";

        private XmlNode DefaultConfig
        {
            get
            {
                XmlDocument defaultConfig = new XmlDocument();
                defaultConfig.LoadXml(_configDefault);
                return defaultConfig.FirstChild;
            }
        }
        private XmlNode ConfigEmpty
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configEmpty);
                return config.FirstChild;
            }
        }
        private XmlNode ConfigFull
        {
            get
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(_configFull);
                return config.FirstChild;
            }
        }

        [TestMethod]
        public void Test_ByColumnGrouper_Initialize()
        {
            DNBLogger.Enable = false;

            ByColumnGrouper grouper = new ByColumnGrouper();
            grouper.Initialize(ConfigEmpty, DefaultConfig, null);
            Assert.AreEqual(grouper.Column, "Column1");

            grouper = new ByColumnGrouper();
            grouper.Initialize(ConfigFull, DefaultConfig, null);
            Assert.AreEqual(grouper.Column, "Column1");
        }

        [TestMethod]
        public void Test_ByColumnGrouper_DoStuff()
        {
            DNBLogger.Enable = false;

            ByColumnGrouper grouper = new ByColumnGrouper();
            object retval = grouper.DoStuff(null);
            Assert.IsNull(retval);
            Assert.AreEqual("ByColumnGrouper: Data is not defined", DNBLogger.LastLog);

            grouper = new ByColumnGrouper();
            retval = grouper.DoStuff(new DataFile());
            Assert.IsNull(retval);
            Assert.AreEqual("ByColumnGrouper: Wrong number of datagroups in file (0)", DNBLogger.LastLog);

            grouper = new ByColumnGrouper();
            grouper.Column = "Column1";
            DataFile dataFile = new DataFile();
            dataFile.DataDelimiter = ';';
            dataFile.Header = "Column1;Column2";
            dataFile.Groups.Add(new DataGroup(dataFile));
            dataFile.Groups[0].DataRows.Add(new DataRow(dataFile.Groups[0]));
            dataFile.Groups[0].DataRows[0].Cells.Add("Column1", "Plate1");
            dataFile.Groups[0].DataRows[0].Cells.Add("Column2", "Data1");
            dataFile.Groups[0].DataRows.Add(new DataRow(dataFile.Groups[0]));
            dataFile.Groups[0].DataRows[1].Cells.Add("Column1", "Plate1");
            dataFile.Groups[0].DataRows[1].Cells.Add("Column2", "Data2");
            dataFile.Groups[0].DataRows.Add(new DataRow(dataFile.Groups[0]));
            dataFile.Groups[0].DataRows[2].Cells.Add("Column1", "Plate2");
            dataFile.Groups[0].DataRows[2].Cells.Add("Column2", "Data3");
            dataFile.Groups[0].DataRows.Add(new DataRow(dataFile.Groups[0]));
            dataFile.Groups[0].DataRows[3].Cells.Add("Column1", "Plate1");
            dataFile.Groups[0].DataRows[3].Cells.Add("Column2", "Data4");
            DataFile groupedDataFile = grouper.DoStuff(dataFile);
            Assert.IsNotNull(groupedDataFile);
            Assert.AreEqual("ByColumnGrouper: DoStuff end", DNBLogger.LastLog);
            Assert.AreEqual(2, groupedDataFile.Groups.Count);
            Assert.AreEqual(3, groupedDataFile.Groups[0].DataRows.Count);
            Assert.AreEqual(1, groupedDataFile.Groups[1].DataRows.Count);
        }
    }
}
