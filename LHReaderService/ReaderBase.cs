﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;

namespace LHReaderService
{
    internal class ReaderBase : Reader
    {
        internal LHReaderService _service = null;
        internal ReaderConfiguration _configuration = null;

        internal ReaderBase(LHReaderService service, XmlNode readerNode)
        {
            _service = service;
            _configuration = new ReaderConfiguration(readerNode);
            _active = _configuration.Active;
            _name = _configuration.Name;
        }

        /// <summary>
        /// Main worker function. Most readers will not need to overwrite this implementation.
        /// </summary>
        /// <param name="connection"></param>
        internal override void DoWork(OracleConnection connection)
        {
            // Find source files and convert to csv
            List<Tuple<FileInfo, FileInfo>> inputFilesSets = FindFiles();
            if (inputFilesSets != null)
            {
                foreach (Tuple<FileInfo, FileInfo> inputFileSet in inputFilesSets)
                {
                    try
                    {
                        if (inputFileSet != null)
                        {
                            List<FileInfo> activeFileSet = new List<FileInfo>();
                            if (inputFileSet.Item1 != null && inputFileSet.Item1.Exists)
                            {
                                activeFileSet.Add(inputFileSet.Item1);
                            }
                            if (inputFileSet.Item2 != null && inputFileSet.Item2.Exists)
                            {
                                activeFileSet.Add(inputFileSet.Item2);
                            }
                            // Parse description file
                            Dictionary<string, string> descriptions = ParseAliquotDescriptions(inputFileSet.Item2);
                            if (descriptions == null) // Parse failed
                            {
                                MoveFiles(activeFileSet, _configuration.ErrorPath);
                                continue;
                            }
                            // Read robot file
                            List<Plate> plates = ParseInputFile(inputFileSet.Item1, descriptions);
                            if (plates == null) // Parse failed
                            {
                                MoveFiles(activeFileSet, _configuration.ErrorPath);
                                continue;
                            }
                            // Create all masters with description info as comment
                            if (!WriteMasterAliquotFiles(connection, plates, inputFileSet.Item1))
                            {
                                MoveFiles(activeFileSet, _configuration.ErrorPath);
                                continue;
                            }
                            // Create build plate files
                            if (!WriteAliquotFiles(connection, plates, inputFileSet.Item1))
                            {
                                MoveFiles(activeFileSet, _configuration.ErrorPath);
                                continue;
                            }
                            // ESAT 2018-08-15 Enabled functionality
                            WriteDisposeFiles(connection, plates, inputFileSet.Item1);
                            // Cleanup
                            MoveFiles(activeFileSet, _configuration.BackupPath);
                        }
                    }
                    catch (Exception ex)
                    {
                        LHReaderService.Log(ex.Message);
                        break;
                    }
                }
            }
        }

        private void WriteDisposeFiles(OracleConnection connection, List<Plate> plates, FileInfo inputFile)
        {
            LHReaderService.Log(_configuration.Name + "Writing dispose files: " + inputFile.Name);
            try
            {
                foreach (Plate plate in plates)
                {
                    if (plate.Aliquots.Count > 0)
                    {
                        int num = 1;
                        List<string> ids = new List<string>();
                        foreach(Aliquot aliquot in plate.Aliquots)
                        {
                            // ESAT 2018-11-13 Changed so all Master aliquots with disposed flag get disposed.

                            //if(aliquot.Parts[plate.Columns[_configuration.ActionDateTimeColumnName]] == "----------" && 
                            //   aliquot.Parts[plate.Columns[_configuration.VolumeColumnName]] == "0" &&
                            //   aliquot.Parts[plate.Columns["Dispose_Input"]] == "1")
                            if (aliquot.Parts[plate.Columns["Dispose_Input"]] == "1")
                            {
                                using (OracleCommand cmd = connection.CreateCommand())
                                {
                                    cmd.Connection = connection;
                                    cmd.CommandType = System.Data.CommandType.Text;
                                    // ESAT 2018-11-13 Changed so only Master aliquots from same project get disposed.
                                    cmd.CommandText = "select A.Aliquot_ID from aliquot A JOIN Aliquot_User U on A.Aliquot_ID = U.Aliquot_ID where U.U_PROJECTNR = '" +
                                        aliquot.Parts[plate.Columns[_configuration.ProjectColumnName]] + "' AND A.Aliquot_ID='" +
                                        aliquot.Parts[plate.Columns[_configuration.SourceSampleBCColumnName]] + "'";
                                    using (OracleDataReader reader = cmd.ExecuteReader())
                                    {
                                        while (reader.Read())
                                        {
                                            ids.Add(reader["aliquot_id"].ToString());
                                        }
                                    }
                                    if (aliquot.Parts[plate.Columns[_configuration.VolumeColumnName]] == "0" && _configuration.DisposeEmptyTargetAliquots && aliquot.Parts[plate.Columns[_configuration.MaterialTypeColumnName]] == "Serum")
                                    {
                                        // ESAT 2019-12-19 Dispose empty target tubes
                                        cmd.CommandText = "select aliquot_id from aliquot a join plate p on a.plate_id=p.plate_id where P.EXTERNAL_REFERENCE='" + plate.Barcode + "' and A.EXTERNAL_REFERENCE='" + aliquot.Parts[plate.Columns[_configuration.TargetSampleBCColumnName]] + "'";
                                        using (OracleDataReader reader = cmd.ExecuteReader())
                                        {
                                            while (reader.Read())
                                            {
                                                string a_ID = reader["aliquot_id"].ToString();
                                                
                                                if (!ids.Contains(a_ID))
                                                {
                                                    ids.Add(a_ID);
                                                    LHReaderService.Log(string.Format("Disposing {0} aliquot no.{1} BC: {2}", a_ID, num, aliquot.Parts[plate.Columns[_configuration.TargetSampleBCColumnName]]));
                                                    num++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if(ids.Count > 0)
                        {
                            string disposeFileName = "DispSampleIdExtLNR" + plate.Barcode + ".csv";
                            FileInfo tmpDisposeFile = new FileInfo(Path.Combine(_configuration.TmpPath, disposeFileName));
                            using (StreamWriter writer = new StreamWriter(tmpDisposeFile.OpenWrite()))
                            {
                                writer.WriteLine("Id");
                                foreach(string id in ids)
                                {
                                    writer.WriteLine(id);
                                }
                            }
                            string disposeFileString = Path.Combine(_configuration.DisposeFilePath, disposeFileName);
                            tmpDisposeFile.MoveTo(disposeFileString);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LHReaderService.Log("Error: Failed to create dispose file: " + ex.Message);
            }
        }

        /// <summary>
        /// Convert xls/xlsx files to csv.
        /// The original file is moved to the backupfolder.
        /// </summary>
        /// <param name="file"></param>
        /// <returns>FileInfo object of the converted file. Or null if conversion failed.</returns>
        internal virtual FileInfo ConvertFileToCSV(FileInfo file)
        {
            if (file.Extension.ToLower().Equals(".csv"))
            {
                return file;
            }
            else if (file.Extension.ToLower().Equals(".xls") || file.Extension.ToLower().Equals(".xlsx"))
            {
                // Convert to csv (OBS! Excell must be installed)
                ExcellConverter excell = new ExcellConverter();
                string csvFileName = file.FullName.Substring(0, file.FullName.Length - 3) + "csv";
                if (excell.ConvertExcelToCSV(file.FullName, csvFileName))
                {
                    file.MoveTo(Path.Combine(_configuration.BackupPath, file.Name));
                    return new FileInfo(csvFileName);
                }
            }
            return null;
        }

        /// <summary>
        /// Prepare list of input and description files.
        /// </summary>
        /// <returns></returns>
        internal virtual List<Tuple<FileInfo, FileInfo>> FindFiles()
        {
            try
            {
                DirectoryInfo inputDirectory = new DirectoryInfo(_configuration.InputPath);
                if (inputDirectory.Exists)
                {
                    List<Tuple<FileInfo, FileInfo>> retval = new List<Tuple<FileInfo, FileInfo>>();
                    FileInfo[] inputFiles = inputDirectory.GetFiles(_configuration.InputFilePattern);
                    foreach (FileInfo inputFile in inputFiles)
                    {
                        FileInfo csvInputFile = ConvertFileToCSV(inputFile);
                        if (csvInputFile == null)
                        {
                            // Ignore this file of unknown type
                            LHReaderService.Log("Ignored file: " + inputFile.Name);
                            continue;
                        }
                        // Search for description file
                        string descriptionFilenameWithoutExtension = Path.Combine(_configuration.DescriptionPath, csvInputFile.Name.Substring(csvInputFile.Name.IndexOf("LNR") + 3, csvInputFile.Name.IndexOf(".") - (csvInputFile.Name.IndexOf("LNR") + 3)));
                        FileInfo descriptionFileCSV = new FileInfo(descriptionFilenameWithoutExtension + ".csv");
                        FileInfo descriptionFileXLS = new FileInfo(descriptionFilenameWithoutExtension + ".xls");
                        FileInfo descriptionFileXLSX = new FileInfo(descriptionFilenameWithoutExtension + ".xlsx");
                        if (descriptionFileCSV.Exists)
                        {
                            retval.Add(new Tuple<FileInfo, FileInfo>(csvInputFile, descriptionFileCSV));
                        }
                        else if (descriptionFileXLS.Exists && _configuration.ConvertToCSV)
                        {
                            descriptionFileCSV = ConvertFileToCSV(descriptionFileXLS);
                            if (descriptionFileCSV == null)
                            {
                                LHReaderService.Log("Failed to convert description file: " + descriptionFileXLS.Name);
                                continue;
                            }
                            retval.Add(new Tuple<FileInfo, FileInfo>(csvInputFile, descriptionFileCSV));
                        }
                        else if (descriptionFileXLSX.Exists && _configuration.ConvertToCSV)
                        {
                            descriptionFileCSV = ConvertFileToCSV(descriptionFileXLSX);
                            if (descriptionFileCSV == null)
                            {
                                LHReaderService.Log("Failed to convert description file: " + descriptionFileXLSX.Name);
                                continue;
                            }
                            retval.Add(new Tuple<FileInfo, FileInfo>(csvInputFile, descriptionFileCSV));
                        }
                        else
                        {
                            retval.Add(new Tuple<FileInfo, FileInfo>(csvInputFile, null));
                        }
                    }
                    return retval;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                LHReaderService.Log("Error finding files for " + _configuration.Name + ": " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Moves a set of files to a given path.
        /// </summary>
        /// <param name="files"></param>
        /// <param name="path"></param>
        internal virtual void MoveFiles(List<FileInfo> files, string path)
        {
            try
            {
                foreach (FileInfo file in files)
                {
                    if (file.Exists)
                    {
                        file.MoveTo(Path.Combine(path, file.Name));
                    }
                }
            }
            catch (Exception ex)
            {
                LHReaderService.Log("Error moving files for " + _configuration.Name + " to " + path + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Create a dictionary of descriptions for aliquot barcodes.
        /// The descriptions are made by substituting in the project description string.
        /// #&FileName&# - will be substituted by the description file name.
        /// ##COLUMN_NAME## - will be substituted by the value of the column COLUMN_NAME from the description file.
        /// </summary>
        /// <param name="descriptionFile"></param>
        /// <returns></returns>
        internal virtual Dictionary<string, string> ParseAliquotDescriptions(FileInfo descriptionFile)
        {
            Dictionary<string, string> descriptions = new Dictionary<string, string>();
            if (descriptionFile == null)
            {
                return descriptions;
            }
            else
            {
                LHReaderService.Log(_configuration.Name + " Handling description file: " + descriptionFile.Name);
                try
                {
                    using (StreamReader reader = new StreamReader(descriptionFile.OpenRead(), System.Text.Encoding.GetEncoding(1252)))
                    {
                        Dictionary<string, int> columns = new Dictionary<string, int>();
                        bool firstLine = true;
                        while (true)
                        {
                            string line = reader.ReadLine();
                            if (line == null)
                            {
                                break;
                            }
                            string[] parts = line.Split(new char[] { _configuration.DescriptionDelimiter });
                            for (int i = 0; i < parts.Length; i++) // Trim "s
                            {
                                parts[i] = parts[i].TrimStart('"').TrimEnd('"');
                            }
                            if (firstLine)
                            {
                                for (int i = 0; i < parts.Count(); i++)
                                {
                                    if (parts[i] != string.Empty) // Skip empty columns
                                    {
                                        columns.Add(parts[i], i);
                                    }
                                }
                                firstLine = false;
                            }
                            else
                            {
                                string aliquotId = parts[columns[_configuration.DescriptionIdColumnName]];
                                if (aliquotId != string.Empty && !descriptions.ContainsKey(aliquotId))
                                {
                                    string descriptionString = _configuration.DescriptionTextFilter;
                                    if (descriptionString.Contains("#&FILENAME&#"))
                                    {
                                        descriptionString = descriptionString.Replace("#&FILENAME&#", descriptionFile.Name);
                                    }
                                    while (descriptionString.Contains("##"))
                                    {
                                        // Replace with value of specified column (e.g. box number: ##Kasse## -> 70)
                                        int first = descriptionString.IndexOf("##");
                                        int second = descriptionString.IndexOf("##", first + 2);
                                        string substitutionstring = descriptionString.Substring(first + 2, second - first - 2);
                                        if (columns.ContainsKey(substitutionstring))
                                        {
                                            descriptionString = descriptionString.Replace("##" + substitutionstring + "##", parts[columns[substitutionstring]]);
                                        }
                                        else
                                        {
                                            LHReaderService.Log("Failed to parse description file: " + descriptionFile.Name + " - Invalid column: " + substitutionstring);
                                            return null;
                                        }
                                    }
                                    descriptions.Add(aliquotId, descriptionString);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LHReaderService.Log("Error: Parsing description file: " + descriptionFile.Name + " - " + ex.Message);
                    return null;
                }
            }
            return descriptions;
        }

        /// <summary>
        /// Create sample/plate object hierarky from input file.
        /// </summary>
        /// <param name="inputFile"></param>
        /// <param name="descriptionDictionary"></param>
        /// <returns></returns>
        internal virtual List<Plate> ParseInputFile(FileInfo inputFile, Dictionary<string, string> descriptions)
        {
            Dictionary<string, Plate> plates = new Dictionary<string, Plate>();
            if (inputFile == null)
            {
                return new List<Plate>();
            }
            else
            {
                LHReaderService.Log(_configuration.Name + " Handling input file: " + inputFile.Name);
                try
                {
                    using (StreamReader reader = new StreamReader(inputFile.OpenRead()))
                    {
                        Dictionary<string, int> columns = new Dictionary<string, int>();
                        List<string> headerParts = null;
                        string header = null;
                        while (true)
                        {
                            string line = reader.ReadLine();
                            if (line == null)
                            {
                                break;
                            }
                            line = line.Replace("\"", string.Empty);
                            if (!line.Equals(header))
                            {
                                string[] parts = line.Split(';');
                                if(parts.Count() != _configuration.InputColumnCount)
                                {
                                    LHReaderService.Log("Error: Wrong number of columns in line: " + line);
                                    return null;
                                }
                                if (header == null) // The first line of the file consist of column names. This line should be at the beginning of each file.
                                {
                                    header = line;
                                    for (int i = 0; i < parts.Count(); i++)
                                    {
                                        if (parts[i] != string.Empty) // Skip empty columns
                                        {
                                            columns.Add(parts[i], i);
                                        }
                                    }
                                    headerParts = new List<string>(parts);
                                    if(!_configuration.CheckColumnNames(columns))
                                    {
                                        LHReaderService.Log("Error: Required column names are not present in data file");
                                        return null;
                                    }
                                }
                                else
                                {
                                    string targetPlateId = parts[columns[_configuration.TargetPlateBCColumnName]];
                                    if (!plates.ContainsKey(targetPlateId))
                                    {
                                        LHReaderService.Log("Started new plate: " + targetPlateId);
                                        plates.Add(targetPlateId, new Plate(headerParts, targetPlateId, columns));
                                    }
                                    string sourceSampleId = parts[columns[_configuration.SourceSampleBCColumnName]];
                                    Regex masterBCRegex = new Regex(@".*E\+.*");
                                    if (masterBCRegex.IsMatch(sourceSampleId))
                                    {
                                        LHReaderService.Log("Error: Master BC not vaild: " + sourceSampleId);
                                        return null;
                                    }
                                    string targetSampleId = parts[columns[_configuration.TargetSampleBCColumnName]];
                                    if(targetSampleId == string.Empty)
                                    {
                                        LHReaderService.Log("Error: Sample BC is empty");
                                        return null;
                                    }
                                    string volumeStr = parts[columns[_configuration.VolumeColumnName]];
                                    int volume = -1;
                                    if(!int.TryParse(volumeStr, out volume) || volume > 10000)
                                    {
                                        LHReaderService.Log("Error: Volume not vaild: " + volumeStr);
                                        return null;
                                    }

                                    if(sourceSampleId.Equals("") || targetSampleId.Equals("")) // Case B, D, E, F, H
                                    {
                                        LHReaderService.Log("Error: Source or target barcode is empty: '" + sourceSampleId + "' / '" + targetSampleId + "'");
                                        return null;
                                    }
                                    else if (sourceSampleId.Equals(_configuration.SkipLineText) && targetSampleId.Equals(_configuration.SkipLineText)) // Case A
                                    {
                                        // Ignore
                                    }
                                    else if ((sourceSampleId.Equals(_configuration.SkipLineText) || targetSampleId.Equals(_configuration.SkipLineText)) && !(this is PlateFillReader)) // Case C, G
                                    {
                                        LHReaderService.Log("Error: Source or target barcode is 'SkipLineText': '" + sourceSampleId + "' / '" + targetSampleId + "'");
                                        return null;
                                    }
                                    else if (!sourceSampleId.Any(char.IsLetterOrDigit) || !targetSampleId.Any(char.IsLetterOrDigit)) // Case J
                                    {
                                        LHReaderService.Log("Error: Source or target barcode consists of illegal characters: '" + sourceSampleId + "' / '" + targetSampleId + "'");
                                        return null;
                                    }
                                    else // Case I
                                    {
                                        string description = string.Empty;
                                        if (descriptions != null && descriptions.ContainsKey(sourceSampleId))
                                        {
                                            description = descriptions[sourceSampleId];
                                        }
                                        else if (descriptions.Count == 0 && !string.IsNullOrEmpty(_configuration.DescriptionTextFilter))
                                        {
                                            if (_configuration.DescriptionTextFilter.Contains("#&FILENAME&#"))
                                            {
                                                description = _configuration.DescriptionTextFilter.Replace("#&FILENAME&#", inputFile.Name);
                                            }
                                            else
                                            {
                                                description = _configuration.DescriptionTextFilter;
                                            }
                                        }
                                        Aliquot aliquot = new Aliquot(description, new List<string>(parts));
                                        plates[targetPlateId].Aliquots.Add(aliquot);
                                        LHReaderService.Log("Read aliquot with external reference: " + sourceSampleId);
                                    }
                                }
                            }
                        }
                        // Remove doubles
                        List<string> comparisonColumns = new List<string>();
                        comparisonColumns.AddRange(_configuration.MasterDoubleLineCompareColumns);
                        comparisonColumns.AddRange(_configuration.SourceDoubleLineCompareColumns);
                        foreach(Plate plate in plates.Values)
                        {
                            plate.Aliquots = RemoveDoubles(plate.Aliquots, plate.Columns, comparisonColumns);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LHReaderService.Log("Error: Parsing file: " + inputFile.Name + ": " + ex.Message);
                    return null;
                }
                return new List<Plate>(plates.Values);
            }
        }

        /// <summary>
        /// Check the Nautilus DB to see if any plates exist with any of the given barcodes.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="plates"></param>
        /// <returns></returns>
        internal virtual bool ExistPlates(OracleConnection connection, List<Plate> plates)
        {
            LHReaderService.Log("Check existence of " + plates.Count + " plates ");
            try
            {
                string referenceString = string.Empty;
                foreach (Plate plate in plates)
                {
                    referenceString = referenceString + plate.Barcode + ",";
                }
                referenceString = referenceString.TrimEnd(',');
                // Connect to database
                OracleCommand command = new OracleCommand();
                command.Connection = connection;
                command.CommandText = "LIMS.DNB.PLATE_IDS_FROM_REFS";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                OracleParameter inputParameter = new OracleParameter();
                inputParameter.ParameterName = "plateids";
                inputParameter.Value = referenceString;
                inputParameter.OracleDbType = OracleDbType.NVarchar2;
                inputParameter.Direction = System.Data.ParameterDirection.Input;
                command.Parameters.Add(inputParameter);
                OracleParameter outputParameter = new OracleParameter();
                outputParameter.ParameterName = "c_result";
                outputParameter.OracleDbType = OracleDbType.RefCursor;
                outputParameter.Direction = System.Data.ParameterDirection.Output;
                command.Parameters.Add(outputParameter);
                OracleDataReader reader = command.ExecuteReader();
                bool found = false;
                while (reader.Read())
                {
                    found = true;
                    LHReaderService.Log("Plate with external reference already exist: " + reader["external_reference"].ToString());
                }
                reader.Close();
                command.Dispose();
                if (found)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                LHReaderService.Log("Failed to check if plates exist: " + ex.Message);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Create instrument files for master aliquot creation.
        /// </summary>
        internal virtual bool WriteMasterAliquotFiles(OracleConnection connection, List<Plate> plates, FileInfo inputFile)
        {
            if (plates == null)
            {
                return false;
            }
            else
            {
                LHReaderService.Log(_configuration.Name + "Writing master files: " + inputFile.Name);
                try
                {
                    bool foundAll = true;
                    foreach (Plate plate in plates)
                    {
                        if (plate.Aliquots.Count > 0 && !plate.Aliquots[0].Parts[plate.Columns[_configuration.SourcePlateBCColumnName]].Contains("DW"))
                        {
                            foreach (Aliquot aliquot in plate.Aliquots)
                            {
                                // Replace user name with user id
                                string userId = string.Empty;
                                try
                                {
                                    if (!string.IsNullOrEmpty(_configuration.UserNameColumnName))
                                    {
                                        string userName = aliquot.Parts[plate.Columns[_configuration.UserNameColumnName]];
                                        userId = FindUserId(connection, aliquot, plate);
                                        if (string.IsNullOrEmpty(userId))
                                        {
                                            LHReaderService.Log("Error: Failed to create master file: Failed to look up user id: " + userName);
                                            foundAll = false;
                                            continue;
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LHReaderService.Log("Error: Failed to create master file: Failed to look up user id: " + ex.Message + Environment.NewLine + ex.StackTrace);
                                    return false;
                                }
                                // Replace container type name with container type id
                                try
                                {
                                    if (!string.IsNullOrEmpty(_configuration.MasterContainerTypeColumnName))
                                    {
                                        string containerTypeId = string.Empty;
                                        string containerTypeName = aliquot.Parts[plate.Columns[_configuration.MasterContainerTypeColumnName]];
                                        containerTypeId = FindContainerTypeId(connection, containerTypeName, plate);
                                        if (string.IsNullOrEmpty(containerTypeId))
                                        {
                                            LHReaderService.Log("Error: Failed to create master file: Failed to look up container type id: " + containerTypeName);
                                            foundAll = false;
                                            continue;
                                        }
                                        aliquot.Parts[plate.Columns[_configuration.MasterContainerTypeColumnName]] = containerTypeId;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LHReaderService.Log("Error: Failed to create master file: Failed to look up container id: " + ex.Message + Environment.NewLine + ex.StackTrace);
                                    return false;
                                }
                                // Replace location name with location id
                                string location_id;
                                try
                                {
                                    string robotName = aliquot.Parts[plate.Columns[_configuration.RobotColumnName]];
                                    location_id = LookupLocationId(connection, robotName);
                                    if (string.IsNullOrEmpty(location_id))
                                    {
                                        LHReaderService.Log("Error: Failed to look up location id: " + robotName);
                                        foundAll = false;
                                        continue;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LHReaderService.Log("Error: Failed to lookup location id: " + ex.Message + Environment.NewLine + ex.StackTrace);
                                    return false;
                                }
                            }
                        }
                    }
                    if (!foundAll)
                    {
                        LHReaderService.Log("Error: Failed to look up data. Stopping.");
                        return false;
                    }
                    foreach (Plate plate in plates)
                    {
                        if (plate.Aliquots.Count > 0 && !plate.Aliquots[0].Parts[plate.Columns[_configuration.SourcePlateBCColumnName]].Contains("DW"))
                        {
                            string masterFileName = inputFile.Name.Substring(0, inputFile.Name.Length - 4) + "_(" + plate.Barcode + ").csv";
                            FileInfo tmpMasterAliquotFile = new FileInfo(Path.Combine(_configuration.TmpPath, masterFileName));
                            List<Aliquot> aliquots = RemoveDoubles(plate.Aliquots, plate.Columns, _configuration.MasterDoubleLineCompareColumns);
                            List<Aliquot> masterAliquots = new List<Aliquot>();
                            if (inputFile.Name.Contains(".RERUN.") || _configuration.CreateMaster == "Never")
                            {
                                masterAliquots = FindAliquotsWithoutMasters(connection, aliquots, plate);
                                if (masterAliquots.Count != 0)
                                {
                                    foreach (Aliquot masterAliquot in masterAliquots)
                                    {
                                        int aliquotRefColumnIndex = plate.Columns[_configuration.SourceSampleBCColumnName];
                                        string aliquotRef = masterAliquot.Parts[aliquotRefColumnIndex];
                                        LHReaderService.Log("Did not find master: " + aliquotRef);
                                    }
                                    throw new Exception("Not all masters exist!");
                                }
                            }
                            else
                            {
                                masterAliquots = FindAliquotsWithoutMasters(connection, aliquots, plate);
                            }
                            if (masterAliquots.Count > 0)
                            {
                                using (StreamWriter writer = new StreamWriter(tmpMasterAliquotFile.OpenWrite()))
                                {
                                    writer.WriteLine("Begin Aliquot");
                                    writer.WriteLine("External_Ref,Workflow_Name,Sample_Ref,Description,U_Log_Robot,U_Projectnr,Container_Type_Id,Matrix_Type,Operator_Id,U_ActionDateTime,Location_Id");
                                    foreach (Aliquot aliquot in masterAliquots)
                                    {
                                        // if actiondatetime is empty - replace with now
                                        string actionDateTime = aliquot.Parts[plate.Columns[_configuration.ActionDateTimeColumnName]];
                                        actionDateTime = actionDateTime == "----------" && aliquot.Parts[plate.Columns[_configuration.VolumeColumnName]] == "0" ? DateTime.Today.ToString("dd-MM-yyyy HH:mm:ss") : DateTime.Parse(actionDateTime).ToString("dd-MM-yyyy HH:mm:ss");
                                        string masterContainerType = _configuration.MasterContainerTypeColumnName == "" ? "" : aliquot.Parts[plate.Columns[_configuration.MasterContainerTypeColumnName]];
                                        writer.WriteLine(aliquot.Parts[plate.Columns[_configuration.SourceSampleBCColumnName]] +
                                                   "," + _configuration.MasterWorkflow +
                                                   "," +
                                                   "," + aliquot.Description +
                                                   "," + aliquot.Parts[plate.Columns[_configuration.RobotColumnName]] +
                                                   "," + aliquot.Parts[plate.Columns[_configuration.ProjectColumnName]] +
                                                   "," + masterContainerType +
                                                   "," + _configuration.MasterSampleType +
                                                   "," + FindUserId(connection, aliquot, plate) +
                                                   "," + actionDateTime +
                                                   "," + LookupLocationId(connection, aliquot.Parts[plate.Columns[_configuration.RobotColumnName]]));
                                    }
                                    writer.WriteLine("End Aliquot");
                                }
                                string masterAliquotFileString = Path.Combine(_configuration.MasterFilePath, masterFileName);
                                tmpMasterAliquotFile.MoveTo(masterAliquotFileString);
                                if (!WaitForInstrument(new FileInfo(masterAliquotFileString)))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LHReaderService.Log("Error: Failed to create master file: " + ex.Message);
                    return false;
                }
                return true;
            }
        }

        internal virtual string LookupLocationId(OracleConnection connection, string locationName)
        {
            string locationId = "";
            using (OracleCommand cmd = connection.CreateCommand())
            {
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "SELECT location_id FROM location WHERE UPPER(name)='" + locationName.ToUpper().TrimStart(' ').TrimEnd(' ').Replace("LH0", "LH") + "'";
                using (OracleDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        locationId = reader["location_id"].ToString();
                    }
                }
            }
            return locationId;
        }

        /// <summary>
        /// Return aliquots that do not already have a master.
        /// Some projects will lookup this in the DB and must therefore override this function.
        /// </summary>
        internal virtual List<Aliquot> FindAliquotsWithoutMasters(OracleConnection connection, List<Aliquot> aliquots, Plate plate)
        {
            List<Aliquot> retval = new List<Aliquot>();
            foreach (Aliquot aliquot in aliquots)
            {
                string found = string.Empty;
                OracleCommand command = new OracleCommand();
                command.Connection = connection;
                string commandText = _configuration.CreateMasterSQL.Replace("##MASTERDESCRIPTION##", aliquot.Description);
                int aliquotRefColumnIndex = plate.Columns[_configuration.SourceSampleBCColumnName];
                string aliquotRef = aliquot.Parts[aliquotRefColumnIndex];
                commandText = commandText.Replace("##SOURCEALIQUOTREF##", aliquotRef);
                int plateBCColumnIndex = plate.Columns[_configuration.SourcePlateBCColumnName];
                string plateBC = aliquot.Parts[plateBCColumnIndex];
                commandText = commandText.Replace("##SOURCEPLATEBC##", plateBC);
                int containerTypeColumnIndex = plate.Columns[_configuration.ContainerTypeColumnName];
                string containerType = aliquot.Parts[containerTypeColumnIndex];
                commandText = commandText.Replace("##CONTAINERTYPE##", containerType);
                command.CommandText = commandText;
                command.CommandType = System.Data.CommandType.Text;
                OracleDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    found = reader["aliquot_id"].ToString();
                }
                reader.Close();
                command.Dispose();
                if (string.IsNullOrEmpty(found))
                {
                    retval.Add(aliquot);
                }
            }
            return retval;
        }

        /// <summary>
        /// Wait for the file to disappear from the folder.
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        internal virtual bool WaitForInstrument(FileInfo file)
        {
            if (file != null)
            {
                int waited = 0;
                while (waited < _configuration.WaitCount)
                {
                    Thread.Sleep(_configuration.WaitInterval);
                    waited++;
                    file.Refresh();
                    if (!file.Exists)
                    {
                        LHReaderService.Log("File is removed by instrument: " + file.Name);
                        return true;
                    }
                }
            }
            LHReaderService.Log("File was not removed by instrument: " + file.Name);
            return false;
        }


        /// <summary>
        /// Create instrument files for aliquot creation.
        /// </summary>
        internal virtual bool WriteAliquotFiles(OracleConnection connection, List<Plate> plates, FileInfo inputFile)
        {
            if (plates == null)
            {
                return false;
            }
            else
            {
                LHReaderService.Log(_configuration.Name + "Writing aliquot files: " + inputFile.Name);
                try
                {
                    bool foundAll = true;
                    // <plate<normalaliquots, aliquotswithoutdeepwellparents>>
                    Dictionary<Plate, Tuple<List<Aliquot>, List<Aliquot>>> entities = new Dictionary<Plate, Tuple<List<Aliquot>, List<Aliquot>>>();
                    foreach(Plate plate in plates)
                    {
                        entities.Add(plate, new Tuple<List<Aliquot>,List<Aliquot>>(new List<Aliquot>(), new List<Aliquot>()));
                        foreach(Aliquot aliquot in plate.Aliquots)
                        {
                            if(aliquot.Parts[plate.Columns[_configuration.SourcePlateBCColumnName]].Contains("DW") &&
                               aliquot.Parts[plate.Columns[_configuration.MaterialTypeColumnName]].Equals("DNA") && 
                               !ExistMasterDWSample(connection, aliquot, plate))
                            {
                                entities[plate].Item2.Add(aliquot);
                            }
                            else
                            {
                                // Replace aliquot ref with aliquot id
                                try
                                {
                                    string aliquotId = string.Empty;
                                    string aliquotRef = aliquot.Parts[plate.Columns[_configuration.SourceSampleBCColumnName]];
                                    if (aliquotRef.StartsWith("ALIQUOT_ID:"))
                                    {
                                        aliquotId = aliquotRef.Substring(11);
                                    }
                                    else
                                    {
                                        aliquotId = FindAliquotId(connection, aliquot, plate);
                                    }
                                    if (string.IsNullOrEmpty(aliquotId))
                                    {
                                        LHReaderService.Log("Error: Failed to look up aliquot id: " + aliquotRef);
                                        foundAll = false;
                                        continue;
                                    }
                                    aliquot.Parts[plate.Columns[_configuration.SourceSampleBCColumnName]] = aliquotId;
                                }
                                catch (Exception ex)
                                {
                                    LHReaderService.Log("Error: Failed to look up aliquot id: " + ex.Message + Environment.NewLine + ex.StackTrace);
                                    foundAll = false;
                                    continue;
                                }
                                // Replace user name with user id
                                try
                                {
                                    if (!string.IsNullOrEmpty(_configuration.UserNameColumnName))
                                    {
                                        string userId = string.Empty;
                                        string userName = aliquot.Parts[plate.Columns[_configuration.UserNameColumnName]];
                                        userId = FindUserId(connection, aliquot, plate);
                                        if (string.IsNullOrEmpty(userId))
                                        {
                                            LHReaderService.Log("Error: Failed to look up user id: " + userName);
                                            foundAll = false;
                                            continue;
                                        }
                                        aliquot.Parts[plate.Columns[_configuration.UserNameColumnName]] = userId;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LHReaderService.Log("Error: Failed to look up user id: " + ex.Message + Environment.NewLine + ex.StackTrace);
                                    foundAll = false;
                                    continue;
                                }
                                // Replace container type name with container type id
                                try
                                {
                                    if (!string.IsNullOrEmpty(_configuration.ContainerTypeColumnName))
                                    {
                                        string containerTypeId = string.Empty;
                                        string containerTypeName = aliquot.Parts[plate.Columns[_configuration.ContainerTypeColumnName]];
                                        containerTypeId = FindContainerTypeId(connection, containerTypeName, plate);
                                        if (string.IsNullOrEmpty(containerTypeId))
                                        {
                                            LHReaderService.Log("Error: Failed to look up container type id: " + containerTypeName);
                                            foundAll = false;
                                            continue;
                                        }
                                        aliquot.Parts[plate.Columns[_configuration.ContainerTypeColumnName]] = containerTypeId;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LHReaderService.Log("Error: Failed to look up container id: " + ex.Message + Environment.NewLine + ex.StackTrace);
                                    foundAll = false;
                                    continue;
                                }
                                // Replace location name with location id
                                try
                                {
                                    string robotName = aliquot.Parts[plate.Columns[_configuration.RobotColumnName]];
                                    string location_id = LookupLocationId(connection, robotName);
                                    if(string.IsNullOrEmpty(location_id))
                                    {
                                        LHReaderService.Log("Error: Failed to look up location id: " + robotName);
                                        foundAll = false;
                                        continue;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LHReaderService.Log("Error: Failed to lookup location id: " + ex.Message + Environment.NewLine + ex.StackTrace);
                                    return false;
                                }

                                // ESAT 2018-03-14
                                // Replace fixed project name with master project name
                                if (_configuration.UseProjectNameFromSource)
                                {
                                    try
                                    {
                                        string aliquotID = aliquot.Parts[plate.Columns[_configuration.SourceSampleBCColumnName]];
                                        string projectName = FindProjectName(connection, aliquotID);
                                        if (string.IsNullOrEmpty(projectName))
                                        {
                                            LHReaderService.Log("Error: Failed to look up project name: " + projectName);
                                            foundAll = false;
                                            continue;
                                        }
                                        aliquot.Parts[plate.Columns[_configuration.ProjectColumnName]] = projectName;
                                    }
                                    catch (Exception ex)
                                    {
                                        LHReaderService.Log("Error: Failed to look up project name: " + ex.Message + Environment.NewLine + ex.StackTrace);
                                        foundAll = false;
                                        continue;
                                    }
                                }
                                entities[plate].Item1.Add(aliquot);
                            }
                        }
                    }
                    if(!foundAll)
                    {
                        LHReaderService.Log("Error: Failed to look up data. Stopping.");
                        return false;
                    }
                    foreach (Plate plate in entities.Keys)
                    {
                        if(entities[plate].Item1.Count > 0)
                        {
                            // Write aliquot file
                            string aliquotFileName = inputFile.Name.Substring(0, inputFile.Name.Length - 4) + "_(" + plate.Barcode + ").csv";
                            FileInfo tmpAliquotFile = new FileInfo(Path.Combine(_configuration.TmpPath, aliquotFileName));
                            using (StreamWriter writer = new StreamWriter(tmpAliquotFile.OpenWrite()))
                            {
                                bool firstColumn = true;
                                foreach(int index in _configuration.AliquotFileColumns)
                                {
                                    if (firstColumn)
                                    {
                                        firstColumn = false;
                                    }
                                    else 
                                    {
                                        writer.Write(",");
                                    }
                                    writer.Write(plate.Header[index]);
                                }
                                writer.Write(",Location_Id");
                                writer.Write(",Description");
                                writer.Write(Environment.NewLine);
                                foreach (Aliquot aliquot in entities[plate].Item1)
                                {
                                    // Write aliquot to file
                                    // if actiondatetime is empty - replace with today
                                    string actionDateTime = aliquot.Parts[plate.Columns[_configuration.ActionDateTimeColumnName]];
                                    actionDateTime = actionDateTime == "----------" && aliquot.Parts[plate.Columns[_configuration.VolumeColumnName]] == "0" ? DateTime.Today.ToString("dd-MM-yyyy HH:mm:ss") : DateTime.Parse(actionDateTime).ToString("dd-MM-yyyy HH:mm:ss");
                                    string line = string.Empty;
                                    foreach (int index in _configuration.AliquotFileColumns)
                                    {
                                        string partstr = aliquot.Parts[index].Replace(',', ' ');
                                        if (index == plate.Columns[_configuration.ActionDateTimeColumnName])
                                        {
                                            partstr = actionDateTime;
                                        }
                                        partstr = "\"" + partstr + "\"";
                                        line = line + partstr + ",";
                                    }
                                    string robotName = aliquot.Parts[plate.Columns[_configuration.RobotColumnName]];
                                    line = line + "\"" + LookupLocationId(connection, robotName) + "\",";
                                    line = line + "\"" + aliquot.Description + "\"";
                                    writer.WriteLine(line);
                                }
                            }
                            string aliquotFileString;
                            if(ExistPlates(connection, new List<Plate>{plate}))
                            {
                                aliquotFileString = Path.Combine(_configuration.AliquotFilePath2, aliquotFileName);
                            }
                            else
                            {
                                aliquotFileString = Path.Combine(_configuration.AliquotFilePath, aliquotFileName);
                            }
                            tmpAliquotFile.MoveTo(aliquotFileString);
                            // Wait for instrument to process aliquot files.
                            if (!WaitForInstrument(new FileInfo(aliquotFileString)))
                            {
                                return false;
                            }
                        }
                        if (entities[plate].Item2.Count > 0)
                        {
                            if (!WriteBeginLoginAliquotFile(connection, inputFile.Name, entities[plate].Item2, plate))
                            {
                                return false;
                            }
                        }
                        else
                        {
                            LHReaderService.Log("No samples found for test login file - skipping step");
                        }
                    }
                }
                catch (Exception ex)
                {
                    LHReaderService.Log("Error: Failed to create aliquot file: " + ex.Message + Environment.NewLine + ex.StackTrace);
                    return false;
                }
                return true;
            }
        }

        private bool WriteBeginLoginAliquotFile(OracleConnection connection, string inputFileName, List<Aliquot> aliquots, Plate plate)
        {
            if(!ExistPlates(connection, new List<Plate>{plate}))
            {
                LHReaderService.Log("Error: Failed to create login aliquot file because plate " + plate.Barcode + " does not exist");
                return false;
            }
            string fileName = inputFileName.Substring(0, inputFileName.Length - 4) + "_" + plate.Barcode + "_noDNA.csv";
            FileInfo tmpAliquotFile = new FileInfo(Path.Combine(_configuration.TmpPath, fileName));
            using (StreamWriter writer = new StreamWriter(tmpAliquotFile.OpenWrite()))
            {
                writer.WriteLine("Begin Aliquot");
                writer.WriteLine("\"External_Ref\",\"Workflow_Name\",\"Sample_Ref\",\"Description\",\"Matrix_Type\",\"Plate_Id\",\"Plate_Order\",\"Plate_Column\",\"Plate_Row\",\"U_Projectnr\",\"Container_Type_Id\",\"Operator_Id\",\"Location_Id\",\"Amount\",\"U_ActionDateTime\",\"U_Log_Robot\"");
                foreach(Aliquot aliquot in aliquots)
                {
                    string aliquotBC = aliquot.Parts[plate.Columns[_configuration.TargetSampleBCColumnName]];
                    string aliquotWF = _configuration.AliquotWorkflow;
                    string position = aliquot.Parts[plate.Columns[_configuration.TargetSamplePositionId]];
                    int plateRow = RowNumber[position.Substring(0, 1)];
                    int plateColumn = Int32.Parse(position.Substring(1, position.Count() - 1));
                    int plateOrder = (plateColumn - 1) * 8 + plateRow;
                    int plateId = FindPlateId(connection, plate);
                    // Replace user name with user id
                    try
                    {
                        if (!string.IsNullOrEmpty(_configuration.UserNameColumnName))
                        {
                            string userId = string.Empty;
                            string userName = aliquot.Parts[plate.Columns[_configuration.UserNameColumnName]];
                            userId = FindUserId(connection, aliquot, plate);
                            if (string.IsNullOrEmpty(userId))
                            {
                                LHReaderService.Log("Error: Failed to create aliquot file: Failed to look up user id: " + userName);
                                return false;
                            }
                            aliquot.Parts[plate.Columns[_configuration.UserNameColumnName]] = userId;
                        }
                    }
                    catch (Exception ex)
                    {
                        LHReaderService.Log("Error: Failed to create aliquot file: Failed to look up user id: " + ex.Message + Environment.NewLine + ex.StackTrace);
                        return false;
                    }
                    // Replace container type name with container type id
                    try
                    {
                        if (!string.IsNullOrEmpty(_configuration.ContainerTypeColumnName))
                        {
                            string containerTypeId = string.Empty;
                            string containerTypeName = aliquot.Parts[plate.Columns[_configuration.ContainerTypeColumnName]];
                            containerTypeId = FindContainerTypeId(connection, containerTypeName, plate);
                            if (string.IsNullOrEmpty(containerTypeId))
                            {
                                LHReaderService.Log("Error: Failed to create aliquot file: Failed to look up container type id: " + containerTypeName);
                                return false;
                            }
                            aliquot.Parts[plate.Columns[_configuration.ContainerTypeColumnName]] = containerTypeId;
                        }
                    }
                    catch (Exception ex)
                    {
                        LHReaderService.Log("Error: Failed to create aliquot file: Failed to look up container id: " + ex.Message + Environment.NewLine + ex.StackTrace);
                        return false;
                    }
                    // Replace location name with location id
                    string location_id = "";
                    try
                    {
                        string robotName = aliquot.Parts[plate.Columns[_configuration.RobotColumnName]];
                        location_id = LookupLocationId(connection, robotName);
                    }
                    catch (Exception ex)
                    {
                        LHReaderService.Log("Error: Failed to lookup location id: " + ex.Message + Environment.NewLine + ex.StackTrace);
                        return false;
                    }
                    // if actiondatetime is empty - replace with today
                    string actionDateTime = aliquot.Parts[plate.Columns[_configuration.ActionDateTimeColumnName]];
                    actionDateTime = actionDateTime == "----------" && aliquot.Parts[plate.Columns[_configuration.VolumeColumnName]] == "0" ? DateTime.Today.ToString("dd-MM-yyyy HH:mm:ss") : DateTime.Parse(actionDateTime).ToString("dd-MM-yyyy HH:mm:ss");
                    writer.WriteLine("\"" + aliquotBC + "\",\"" + aliquotWF + "\",\"\",\"" + aliquot.Description + "\",\"noDNA\",\"" + plateId.ToString() + "\",\"" + plateOrder + "\",\"" + plateColumn + "\",\"" + plateRow.ToString() + "\",\"" + aliquot.Parts[plate.Columns[_configuration.ProjectColumnName]] + "\",\"" + aliquot.Parts[plate.Columns[_configuration.ContainerTypeColumnName]] + "\",\"" + aliquot.Parts[plate.Columns[_configuration.UserNameColumnName]] + "\",\"" + location_id + "\",\"" + aliquot.Parts[plate.Columns[_configuration.VolumeColumnName]] + "\",\"" + actionDateTime + "\",\"" + aliquot.Parts[plate.Columns[_configuration.RobotColumnName]] + "\"");
                }                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
                writer.WriteLine("End Aliquot");
            }
            string aliquotFileString = Path.Combine(_configuration.MasterFilePath, fileName);
            tmpAliquotFile.MoveTo(aliquotFileString);
            if (!WaitForInstrument(new FileInfo(aliquotFileString)))
            {
                return false;
            }
            return true;
        }

        public Dictionary<string, int> RowNumber = new Dictionary<string, int>()
        {{"A",1},
         {"B",2},
         {"C",3},
         {"D",4},
         {"E",5},
         {"F",6},
         {"G",7},
         {"H",8}};

        private bool ExistMasterDWSample(OracleConnection connection, Aliquot aliquot, Plate plate)
        {
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = connection;
                string commandText = _configuration.FindMasterSQL.Replace("##MASTERDESCRIPTION##", aliquot.Description);
                if (commandText.Contains("##SOURCEALIQUOTREF##"))
                {
                    int aliquotBCColumnIndex = plate.Columns[_configuration.SourceSampleBCColumnName];
                    string aliquotRef = aliquot.Parts[aliquotBCColumnIndex];
                    commandText = commandText.Replace("##SOURCEALIQUOTREF##", aliquotRef);
                }
                if (commandText.Contains("##SOURCEPLATEBC##"))
                {
                    int plateBCColumnIndex = plate.Columns[_configuration.SourcePlateBCColumnName];
                    string plateBC = aliquot.Parts[plateBCColumnIndex];
                    commandText = commandText.Replace("##SOURCEPLATEBC##", plateBC);
                }
                if (commandText.Contains("##CONTAINERTYPE##"))
                {
                    int containerTypeColumnIndex = plate.Columns[_configuration.ContainerTypeColumnName];
                    string containerType = aliquot.Parts[containerTypeColumnIndex];
                    commandText = commandText.Replace("##CONTAINERTYPE##", containerType);
                }
                if (commandText.Contains("##PROJECT##"))
                {
                    int projectColumnIndex = plate.Columns[_configuration.ProjectColumnName];
                    string containerType = aliquot.Parts[projectColumnIndex];
                    commandText = commandText.Replace("##PROJECT##", containerType);
                }
                command.CommandText = commandText;
                object retval = command.ExecuteScalar();
                return retval != null;
            }
        }

        /// <summary>
        /// Lookup aliquot id of master.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="aliquot"></param>
        /// <returns></returns>
        internal virtual string FindAliquotId(OracleConnection connection, Aliquot aliquot, Plate plate)
        {
            string retval = string.Empty;
            // Replace aliquot ext.ref. by aliquot id found by ext.ref + description
            OracleCommand command = new OracleCommand();
            command.Connection = connection;
            string commandText = _configuration.FindMasterSQL.Replace("##MASTERDESCRIPTION##", aliquot.Description);
            if (commandText.Contains("##SOURCEALIQUOTREF##"))
            {
                int aliquotBCColumnIndex = plate.Columns[_configuration.SourceSampleBCColumnName];
                string aliquotRef = aliquot.Parts[aliquotBCColumnIndex];
                commandText = commandText.Replace("##SOURCEALIQUOTREF##", aliquotRef);
            }
            if (commandText.Contains("##SOURCEPLATEBC##"))
            {
                int plateBCColumnIndex = plate.Columns[_configuration.SourcePlateBCColumnName];
                string plateBC = aliquot.Parts[plateBCColumnIndex];
                commandText = commandText.Replace("##SOURCEPLATEBC##", plateBC);
            }
            if (commandText.Contains("##CONTAINERTYPE##"))
            {
                int containerTypeColumnIndex = plate.Columns[_configuration.ContainerTypeColumnName];
                string containerType = aliquot.Parts[containerTypeColumnIndex];
                commandText = commandText.Replace("##CONTAINERTYPE##", containerType);
            }
            if (commandText.Contains("##PROJECT##"))
            {
                int projectColumnIndex = plate.Columns[_configuration.ProjectColumnName];
                string containerType = aliquot.Parts[projectColumnIndex];
                commandText = commandText.Replace("##PROJECT##", containerType);
            }
            command.CommandText = commandText;
            command.CommandType = System.Data.CommandType.Text;
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                retval = reader["aliquot_id"].ToString();
            }
            if (string.IsNullOrEmpty(retval))
            {
                LHReaderService.Log("Aliquot id not found in DB: " + commandText);
            }
            reader.Close();
            command.Dispose();
            return retval;
        }

        /// <summary>
        /// Lookup user id from user name.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="aliquot"></param>
        /// <returns></returns>
        internal virtual string FindUserId(OracleConnection connection, Aliquot aliquot, Plate plate)
        {
            string retval = string.Empty;
            OracleCommand command = new OracleCommand();
            command.Connection = connection;
            string commandText = _configuration.UserNameSQL;
            if (commandText.Contains("##USERNAME##"))
            {
                string userName = aliquot.Parts[plate.Columns[_configuration.UserNameColumnName]];
                commandText = commandText.Replace("##USERNAME##", userName);
            }
            command.CommandText = commandText;
            command.CommandType = System.Data.CommandType.Text;
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                retval = reader["operator_id"].ToString();
            }
            reader.Close();
            command.Dispose();
            return retval;
        }

        /// <summary>
        /// Lookup container type id from container type name.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="aliquot"></param>
        /// <returns></returns>
        internal virtual string FindContainerTypeId(OracleConnection connection, string containerTypeName, Plate plate)
        {
            string retval = string.Empty;
            OracleCommand command = new OracleCommand();
            command.Connection = connection;
            string commandText = _configuration.ContainerTypeSQL;
            if (commandText.Contains("##CONTAINERTYPE##"))
            {
                commandText = commandText.Replace("##CONTAINERTYPE##", containerTypeName);
            }
            command.CommandText = commandText;
            command.CommandType = System.Data.CommandType.Text;
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                retval = reader["container_type_id"].ToString();
            }
            reader.Close();
            command.Dispose();
            return retval;
        }

        /// <summary>
        /// Returns a list of aliquots cleansed of doubles based on a list of columns to compare.
        /// </summary>
        /// <param name="fullList"></param>
        /// <param name="comparisonColumns"></param>
        /// <returns></returns>
        internal virtual List<Aliquot> RemoveDoubles(List<Aliquot> fullList, Dictionary<string, int> plateColumns, List<string> comparisonColumns)
        {
            List<Aliquot> aliquots = new List<Aliquot>();
            List<string> doubles = new List<string>();
            foreach (Aliquot aliquot in fullList)
            {
                string doubleString = string.Empty;
                foreach (string column in comparisonColumns)
                {
                    doubleString = doubleString + aliquot.Parts[plateColumns[column]];
                }
                if (!doubles.Contains(doubleString))
                {
                    doubles.Add(doubleString);
                    aliquots.Add(aliquot);
                }
                else
                {
                    LHReaderService.Log("Ignored double line: " + doubleString);
                }
            }
            return aliquots;
        }

        internal int FindPlateId(OracleConnection connection, Plate plate)
        {
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = connection;
                command.CommandText = "SELECT plate_id FROM plate WHERE external_reference='" + plate.Barcode + "'";
                return Convert.ToInt32(command.ExecuteScalar());
            }
        }

        internal string FindProjectName(OracleConnection connection, string AliquotID)
        {
            using (OracleCommand command = new OracleCommand())
            {
                command.Connection = connection;
                command.CommandText = "select pr.name from " +
                                      "lims.aliquot a " +
                                      "left join " +
                                      "lims.sample s on s.sample_id = a.sample_id " +
                                      "left join " +
                                      "lims.project_study prs on s.study_id = prs.study_id " +
                                      "left join " +
                                      "lims.project pr on pr.project_id = prs.project_id " +
                                      "where a.aliquot_id = " + AliquotID;

                return (string)(command.ExecuteScalar());
            }
        }
    }
}
