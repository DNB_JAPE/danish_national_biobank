﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace LHReaderService
{
    internal class ReaderConfiguration
    {
        internal ReaderConfiguration(XmlNode configurationNode)
        {
            Active = bool.Parse(configurationNode.SelectSingleNode(".//Active").InnerXml);
            Name = configurationNode.SelectSingleNode(".//Name").InnerXml;
            MasterWorkflow = configurationNode.SelectSingleNode("./MasterWorkflow").InnerXml;
            InputPath = configurationNode.SelectSingleNode("./InputPath").InnerXml;
            BackupPath = configurationNode.SelectSingleNode("./BackupPath").InnerXml;
            ErrorPath = configurationNode.SelectSingleNode("./ErrorPath").InnerXml;
            TmpPath = configurationNode.SelectSingleNode("./TmpPath").InnerXml;
            MasterFilePath = configurationNode.SelectSingleNode("./MasterFilePath").InnerXml;
            AliquotFilePath = configurationNode.SelectSingleNode("./AliquotFilePath").InnerXml;
            AliquotFilePath2 = configurationNode.SelectSingleNode("./AliquotFilePath2").InnerXml;
            DescriptionPath = configurationNode.SelectSingleNode("./DescriptionPath").InnerXml;
            InputFilePattern = configurationNode.SelectSingleNode("./InputFilePattern").InnerXml;
            TargetPlateBCColumnName = configurationNode.SelectSingleNode("./TargetPlateBCColumnName").InnerXml;
            TargetSampleBCColumnName = configurationNode.SelectSingleNode("./TargetSampleBCColumnName").InnerXml;
            TargetSamplePositionId = configurationNode.SelectSingleNode("./TargetSamplePositionId").InnerXml;
            SourcePlateBCColumnName = configurationNode.SelectSingleNode("./SourcePlateBCColumnName").InnerXml;
            SourcePlateLabwareIdColumnName = configurationNode.SelectSingleNode("./SourcePlateLabwareIdColumnName").InnerXml;
            SourceSampleBCColumnName = configurationNode.SelectSingleNode("./SourceSampleBCColumnName").InnerXml;
            SourceSamplePositionId = configurationNode.SelectSingleNode("./SourceSamplePositionId").InnerXml;
            SkipLineText = configurationNode.SelectSingleNode("./SkipLineText").InnerXml;
            MasterDoubleLineCompareColumns = new List<string>();
            string MasterDoubleLineCompareColumnsString = configurationNode.SelectSingleNode("./MasterDoubleLineCompareColumns").InnerXml;
            string[] masterDoubleLineCompareColumnsParts = string.IsNullOrEmpty(MasterDoubleLineCompareColumnsString) ? new string[]{} : MasterDoubleLineCompareColumnsString.Split(',');
            MasterDoubleLineCompareColumns.AddRange(masterDoubleLineCompareColumnsParts);
            SourceDoubleLineCompareColumns = new List<string>();
            string SourceDoubleLineCompareColumnsString = configurationNode.SelectSingleNode("./SourceDoubleLineCompareColumns").InnerXml;
            string[] sourceDoubleLineCompareColumnsParts = string.IsNullOrEmpty(SourceDoubleLineCompareColumnsString) ? new string[] { } : SourceDoubleLineCompareColumnsString.Split(',');
            SourceDoubleLineCompareColumns.AddRange(sourceDoubleLineCompareColumnsParts);
            WaitCount = int.Parse(configurationNode.SelectSingleNode("./WaitCount").InnerXml);
            WaitInterval = int.Parse(configurationNode.SelectSingleNode("./WaitInterval").InnerXml);
            CreateMaster = configurationNode.SelectSingleNode("./CreateMaster").InnerXml;
            CreateMasterSQL = configurationNode.SelectSingleNode("./CreateMasterSQL").InnerXml;
            FindMasterSQL = configurationNode.SelectSingleNode("./FindMasterSQL").InnerXml;
            string[] aliquotFileColumnStrings = configurationNode.SelectSingleNode("./AliquotFileColumns").InnerXml.Split(',');
            List<int> aliquotFileColumns = new List<int>();
            foreach (string aliquotFileColumn in aliquotFileColumnStrings)
            {
                aliquotFileColumns.Add(Convert.ToInt32(aliquotFileColumn));
            }
            AliquotFileColumns = aliquotFileColumns;
            ConvertToCSV = bool.Parse(configurationNode.SelectSingleNode("./ConvertToCSV").InnerXml);
            string descriptionDelimiterString = configurationNode.SelectSingleNode("./DescriptionDelimiter").InnerXml;
            DescriptionDelimiter = string.IsNullOrEmpty(descriptionDelimiterString) ? ';' : descriptionDelimiterString.ToCharArray()[0];
            DescriptionIdColumnName = configurationNode.SelectSingleNode("./DescriptionIdColumnName").InnerXml;
            DescriptionTextFilter = configurationNode.SelectSingleNode("./DescriptionTextFilter").InnerText;
            UserNameColumnName = configurationNode.SelectSingleNode("./UserNameColumnName").InnerXml;
            UserNameSQL = configurationNode.SelectSingleNode("./UserNameSQL").InnerXml;
            MasterContainerTypeColumnName = configurationNode.SelectSingleNode("./MasterContainerTypeColumnName").InnerXml;
            ContainerTypeColumnName = configurationNode.SelectSingleNode("./ContainerTypeColumnName").InnerXml;
            ContainerTypeSQL = configurationNode.SelectSingleNode("./ContainerTypeSQL").InnerXml;
            RobotColumnName = configurationNode.SelectSingleNode("./RobotColumnName").InnerXml;
            ProjectColumnName = configurationNode.SelectSingleNode("./ProjectColumnName").InnerXml;
            MaterialTypeColumnName = configurationNode.SelectSingleNode("./MaterialTypeColumnName").InnerXml;
            ActionDateTimeColumnName = configurationNode.SelectSingleNode("./ActionDateTimeColumnName").InnerXml;
            VolumeColumnName = configurationNode.SelectSingleNode("./VolumeColumnName").InnerXml;
            DisposeFilePath = configurationNode.SelectSingleNode("./DisposeFilePath").InnerXml;
            XmlNode masterSampleTypeNode = configurationNode.SelectSingleNode("./MasterSampleType");
            MasterSampleType = masterSampleTypeNode == null ? "" : masterSampleTypeNode.InnerXml;
            // Only used in DNAConcentrationReader
            XmlNode testWorkflowNode = configurationNode.SelectSingleNode("./TestWorkflow");
            TestWorkflow = testWorkflowNode == null ? "" : testWorkflowNode.InnerXml;
            XmlNode testFilePathNode = configurationNode.SelectSingleNode("./TestFilePath");
            TestFilePath = testFilePathNode == null ? "" : testFilePathNode.InnerXml;
            XmlNode aliquotWorkflowNode = configurationNode.SelectSingleNode("./AliquotWorkflow");
            AliquotWorkflow = aliquotWorkflowNode == null ? "" : aliquotWorkflowNode.InnerXml;

            XmlNode inputColumnCountNode = configurationNode.SelectSingleNode("./InputColumnCount");
            InputColumnCount = Convert.ToInt32(inputColumnCountNode.InnerXml);

            XmlNode useProjectNameFromSourceNode = configurationNode.SelectSingleNode("./UseProjectNameFromSource");
            UseProjectNameFromSource = useProjectNameFromSourceNode == null ? false : Convert.ToBoolean(useProjectNameFromSourceNode.InnerXml);

            XmlNode disposeEmptyTargetAliquots= configurationNode.SelectSingleNode("./DisposeEmptyTargetAliquots");
            DisposeEmptyTargetAliquots = disposeEmptyTargetAliquots == null ? false : Convert.ToBoolean(disposeEmptyTargetAliquots.InnerXml);
        }

        /// <summary>
        /// 
        /// </summary>
        internal string DisposeFilePath { get; set; }

        /// <summary>
        /// 
        /// </summary>
        internal string VolumeColumnName { get; set; }

        /// <summary>
        /// Material Type (e.g. Serum)
        /// </summary>
        internal string MaterialTypeColumnName { get; set; }
        
        /// <summary>
        /// Time of processing on robot.
        /// </summary>
        internal string ActionDateTimeColumnName { get; set; }

        /// <summary>
        /// Is the reader active?
        /// </summary>
        internal bool Active { get; set; }

        /// <summary>
        /// Name of the project (e.g. DNB-1)
        /// </summary>
        internal string Name { get; set; }

        /// <summary>
        /// Name of the master workflow for this project.
        /// </summary>
        internal string MasterWorkflow { get; set; }

        /// <summary>
        /// Name of the aliquot workflow for this project.
        /// </summary>
        internal string AliquotWorkflow { get; set; }

        /// <summary>
        /// Input path for the reader.
        /// </summary>
        internal string InputPath { get; set; }

        /// <summary>
        /// Processed files are moved to this location.
        /// </summary>
        internal string BackupPath { get; set; }

        /// <summary>
        /// In case of errors, files are moved to this location.
        /// </summary>
        internal string ErrorPath { get; set; }

        /// <summary>
        /// Working folder for the reader.
        /// </summary>
        internal string TmpPath { get; set; }

        /// <summary>
        /// Input folder for the master instrument.
        /// </summary>
        internal string MasterFilePath { get; set; }

        /// <summary>
        /// Input folder for the build plate/aliquot instrument.
        /// </summary>
        internal string AliquotFilePath { get; set; }

        /// <summary>
        /// Input folder for the build plate/aliquot instrument for existing plates.
        /// </summary>
        internal string AliquotFilePath2 { get; set; }

        /// <summary>
        /// Input path for description files.
        /// </summary>
        internal string DescriptionPath { get; set; }

        /// <summary>
        /// Pattern of input files. Used to identify input files for this project.
        /// </summary>
        internal string InputFilePattern { get; set; }

        /// <summary>
        /// Name of the column containing target plate barcodes in the input files.
        /// </summary>
        internal string TargetPlateBCColumnName { get; set; }

        /// <summary>
        /// Name of the column containing target sample barcodes in the input files.
        /// </summary>
        internal string TargetSampleBCColumnName { get; set; }

        /// <summary>
        /// Name of the column containing target sample position in the input files.
        /// </summary>
        internal string TargetSamplePositionId { get; set; }

        /// <summary>
        /// Name of the column containing source plate barcodes in the input files.
        /// </summary>
        internal string SourcePlateBCColumnName { get; set; }

        /// <summary>
        /// Name of the column containing source plate labware in the input files.
        /// </summary>
        internal string SourcePlateLabwareIdColumnName { get; set; }

        /// <summary>
        /// Name of the column containing source sample barcode in the input files.
        /// </summary>
        internal string SourceSampleBCColumnName { get; set; }

        /// <summary>
        /// Name of the column containing source sample position in the input files.
        /// </summary>
        internal string SourceSamplePositionId { get; set; }

        /// <summary>
        /// Lines in the input file, where the source sample barcode has this value, will be ignored.
        /// </summary>
        internal string SkipLineText { get; set; }

        /// <summary>
        /// List of names of the columns, which makes an input line unique.
        /// This is used for removing doublet lines in the input files.
        /// </summary>
        internal List<string> MasterDoubleLineCompareColumns { get; set; }

        /// <summary>
        /// List of names of the columns, which makes an input line unique.
        /// This is used for removing doublet lines in the input files.
        /// </summary>
        internal List<string> SourceDoubleLineCompareColumns { get; set; }

        /// <summary>
        /// Number of times, the reader should wait for the instrument to handle the master files.
        /// </summary>
        internal int WaitCount { get; set; }

        /// <summary>
        /// Time interval between checks of master file handling by the instrument.
        /// </summary>
        internal int WaitInterval { get; set; }

        /// <summary>
        ///  Always - create master - don't try to find existing.
        ///  Never - create master - if the master is not found using sql, raise an error.
        ///  IfNotExist - If the master is not found using sql, create it.
        /// </summary>
        internal string CreateMaster { get; set; }

        /// <summary>
        ///  If this node has a value, the SQL will be run. 
        ///  If it returns an id, no master will be created for this line.
        /// </summary>
        internal string CreateMasterSQL { get; set; }

        /// <summary>
        /// SQL statement for finding master of a specific aliquot.
        /// In the base implementation, ##MASTERDESCRIPTION## will be exchanged for the master aliquot description
        /// and ##aliquotref## will be exchanged for the master barcode.
        /// </summary>
        internal string FindMasterSQL { get; set; }
        
        /// <summary>
        /// List of columns specifying the order of input file columns in the aliquot file.
        /// </summary>
        internal List<int> AliquotFileColumns { get; set; }

        /// <summary>
        /// Should XLS/XLXS files be converted to CSV? Othervise they will be ignored.
        /// </summary>
        internal bool ConvertToCSV { get; set; }

        /// <summary>
        /// Delimeter of columns in description files.
        /// </summary>
        internal char DescriptionDelimiter { get; set; }

        /// <summary>
        /// Name of the aliquot id column of the description files.
        /// </summary>
        internal string DescriptionIdColumnName { get; set; }

        /// <summary>
        /// Template of the master descriptions. Values from the description files will be substituted into this template.
        /// </summary>
        internal string DescriptionTextFilter { get; set; }

        /// <summary>
        /// Name of the user name column of the data files.
        /// </summary>
        internal string UserNameColumnName { get; set; }

        /// <summary>
        /// SQL used for lookup of operator id.
        /// </summary>
        internal string UserNameSQL { get; set; }

        /// <summary>
        /// Name of the master container type column of the data files.
        /// </summary>
        internal string MasterContainerTypeColumnName { get; set; }

        /// <summary>
        /// Name of the container type column of the data files.
        /// </summary>
        internal string ContainerTypeColumnName { get; set; }

        /// <summary>
        /// SQL used for lookup of container type id.
        /// </summary>
        internal string ContainerTypeSQL { get; set; }

        /// <summary>
        /// Name of the robot column of the data files.
        /// </summary>
        internal string RobotColumnName { get; set; }

        /// <summary>
        /// Name of the project column of the data files.
        /// </summary>
        internal string ProjectColumnName { get; set; }

        /// <summary>
        /// Master Sample Type.
        /// </summary>
        internal string MasterSampleType { get; set; }

        /// <summary>
        /// Only used in DNAConcentrationReader
        /// </summary>
        internal string TestWorkflow { get; set; }
        internal string TestFilePath { get; set; }

        internal int InputColumnCount { get; set; }        

        /// <summary>
        /// Uses the projectname from the source as projectname
        /// </summary>
        internal bool UseProjectNameFromSource { get; set; }

        internal bool DisposeEmptyTargetAliquots { get; set; }


        internal bool CheckColumnNames(Dictionary<string, int> columns)
        {
            bool retval = true;
            retval = CheckColumnName("ProjectColumnName", ProjectColumnName, columns) == false ? false : retval;
            retval = CheckColumnName("RobotColumnName", RobotColumnName, columns) == false ? false : retval;
            retval = CheckColumnName("ContainerTypeColumnName", ContainerTypeColumnName, columns) == false ? false : retval;
            retval = CheckColumnName("MasterContainerTypeColumnName", MasterContainerTypeColumnName, columns) == false ? false : retval;
            retval = CheckColumnName("UserNameColumnName", UserNameColumnName, columns) == false ? false : retval;
            retval = CheckColumnName("SourceSampleBCColumnName", SourceSampleBCColumnName, columns) == false ? false : retval;
            retval = CheckColumnName("SourcePlateLabwareIdColumnName", SourcePlateLabwareIdColumnName, columns) == false ? false : retval;
            retval = CheckColumnName("SourcePlateBCColumnName", SourcePlateBCColumnName, columns) == false ? false : retval;
            retval = CheckColumnName("TargetSampleBCColumnName", TargetSampleBCColumnName, columns) == false ? false : retval;
            retval = CheckColumnName("TargetPlateBCColumnName", TargetPlateBCColumnName, columns) == false ? false : retval;
            retval = CheckColumnName("ActionDateTimeColumnName", ActionDateTimeColumnName, columns) == false ? false : retval;
            retval = CheckColumnName("MaterialTypeColumnName", MaterialTypeColumnName, columns) == false ? false : retval;
            retval = CheckColumnName("VolumeColumnName", VolumeColumnName, columns) == false ? false : retval;
            return retval;
        }

        private bool CheckColumnName(string variable, string columnName, Dictionary<string, int> columns)
        {
            if(columnName != string.Empty && !columns.ContainsKey(columnName))
            {
                LHReaderService.Log("Column not found in data file: " + variable + " = " + columnName);
                return false;
            }
            return true;
        }
    }
}
