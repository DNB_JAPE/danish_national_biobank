﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LHReaderService
{
    internal class Reader
    {
        protected bool _active;
        protected string _name;

        internal bool Active
        {
            get
            {
                return _active;
            }
        }

        internal string Name
        {
            get
            {
                return _name;
            }
        }

        internal virtual void DoWork(OracleConnection connection)
        {
        }
    }
}
