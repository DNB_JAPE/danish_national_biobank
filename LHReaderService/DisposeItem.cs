﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LHReaderService
{
    internal class DisposeItem
    {
        internal string Id { get; set; }
        internal string Bc { get; set; }
        internal string UserName { get; set; }
        internal string UserComment { get; set; }
        internal string ActionDateTime { get; set; }
        internal string ContainerType { get; set; }
    }
}
