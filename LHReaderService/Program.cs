﻿using System;
using System.ServiceProcess;
using System.Threading;

namespace LHReaderService
{
    static class Program
    {
        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        private static extern bool AllocConsole();

        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        private static extern bool AttachConsole(int pid);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                // Command line given, display console
                if (!AttachConsole(-1))
                { // Attach to an parent process console
                    AllocConsole(); // Alloc a new console
                }
                if (args[0].ToLower() == "/help")
                {
                    Console.WriteLine("Useage: LHReaderService");
                    Console.WriteLine("        LHReaderService [option] [argument]");
                    Console.WriteLine();
                    Console.WriteLine("No option is used for running as a windows service.");
                    Console.WriteLine();
                    Console.WriteLine("Options:");
                    Console.WriteLine("  /help");
                    Console.WriteLine("    This menu");
                    Console.WriteLine("  /console");
                    Console.WriteLine("    Run as console application");
                }
                else if (args[0].ToLower() == "/console")
                {
                    Console.WriteLine("Running as console application. To stop the program use the Task Manager...");
                    LHReaderService service = new LHReaderService();
                    service.Start();
                    while (true)
                    {
                        Thread.Sleep(100);
                    }
                }
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
                { 
                    new LHReaderService() 
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
